module.exports = {
  moduleNameMapper: {
    '\\.(css|scss)$': 'identity-obj-proxy',
  },
  preset: 'ts-jest',
  setupFiles: ['<rootDir>__test__/setup.ts'],
  testEnvironment: 'jsdom',
  transform: {
    '.+\\.(j|t)sx?$': 'ts-jest',
  },
  transformIgnorePatterns: [
    'node_modules/(?!(lodash|lodash-es|@react-hook/copy|array-move)/.*)',
  ],
}
