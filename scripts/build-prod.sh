#!/bin/bash

# exit script if any commands fail
set -e

printf "\033[0;36m \n=== Cleaning Up Old Files === \n \033[0m"
rm -rf lib css;

printf "\033[0;36m \n=== Bundling Code === \n \033[0m"
rollup -c --bundleConfigAsCjs;

printf "\033[0;36m \n=== Moving Standalone CSS to Root === \n \033[0m"
mv lib/css ./css;
