#!/bin/bash

# exit script if any commands fail
set -e

printf "\033[0;36m \n=== Cleaning Up Old Files === \n \033[0m"
rm -rf lib;

printf "\033[0;36m \n=== Bundling Code & Listening for Changes === \n \033[0m"
rollup -c --bundleConfigAsCjs --watch;
