module.exports = {
  plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography')],

  content: ['{src,styleguide}/**/*.{css,md,ts,tsx}'],

  theme: {
    screens: {
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px',
      xxl: '1536px',
    },

    extend: {
      borderWidth: {
        none: 'var(--border-none)',
        DEFAULT: 'var(--border-base)',
        md: 'var(--border-md)',
        lg: 'var(--border-lg)',
        xl: 'var(--border-xl)',
      },

      borderRadius: {
        none: 'var(--border-radius-none)',
        sm: 'var(--border-radius-sm)',
        DEFAULT: 'var(--border-radius-base)',
        md: 'var(--border-radius-md)',
        lg: 'var(--border-radius-lg)',
        xl: 'var(--border-radius-xl)',
        xxl: 'var(--border-radius-xxl)',
      },

      colors: {
        'brand-accent': 'var(--color-brand-accent)',
        'brand-primary': 'var(--color-brand-primary)',
        'brand-secondary': 'var(--color-brand-secondary)',

        background: 'var(--color-background)',

        border: 'var(--color-border)',

        overlay: 'var(--color-overlay)',

        surface: {
          subtle: 'var(--color-surface-subtle)',
          DEFAULT: 'var(--color-surface)',
          strong: 'var(--color-surface-strong)',
        },

        input: {
          background: {
            DEFAULT: 'var(--color-input-background)',
            disabled: 'var(--color-input-background-disabled)',
          },
        },

        main: {
          dark: 'var(--color-main-dark)',
          DEFAULT: 'var(--color-main)',
          light: 'var(--color-main-light)',
          faded: 'var(--color-main-faded)',
        },

        neutral: {
          dark: 'var(--color-neutral-dark)',
          DEFAULT: 'var(--color-neutral)',
          light: 'var(--color-neutral-light)',
          faded: 'var(--color-neutral-faded)',
        },

        positive: {
          dark: 'var(--color-positive-dark)',
          DEFAULT: 'var(--color-positive)',
          light: 'var(--color-positive-light)',
          faded: 'var(--color-positive-faded)',
        },

        caution: {
          dark: 'var(--color-caution-dark)',
          DEFAULT: 'var(--color-caution)',
          light: 'var(--color-caution-light)',
          faded: 'var(--color-caution-faded)',
        },

        critical: {
          dark: 'var(--color-critical-dark)',
          DEFAULT: 'var(--color-critical)',
          light: 'var(--color-critical-light)',
          faded: 'var(--color-critical-faded)',
        },

        info: {
          dark: 'var(--color-info-dark)',
          DEFAULT: 'var(--color-info)',
          light: 'var(--color-info-light)',
          faded: 'var(--color-info-faded)',
        },
      },

      spacing: {
        xxs: 'var(--spacing-xxs)',
        xs: 'var(--spacing-xs)',
        sm: 'var(--spacing-sm)',
        base: 'var(--spacing-base)',
        lg: 'var(--spacing-lg)',
        xl: 'var(--spacing-xl)',
        xxl: 'var(--spacing-xxl)',
      },

      fontFamily: {
        // sans is tailwinds default in @base layer
        sans: [
          'system-ui',
          'sans-serif',
          '-apple-system',
          'BlinkMacSystemFont',
          'Segoe UI',
          'Roboto',
          'Helvetica Neue',
          'Arial',
          'Noto Sans',
        ],
      },

      fontSize: {
        sm: ['var(--size-text-sm)', 'var(--line-height-text-sm)'],
        base: ['var(--size-text)', 'var(--line-height-text)'],
        md: ['var(--size-text-md)', 'var(--line-height-text-md)'],
        lg: ['var(--size-text-lg)', 'var(--line-height-text-lg)'],
        xl: ['var(--size-text-xl)', 'var(--line-height-text-xl)'],
        xxl: ['var(--size-text-xxl)', 'var(--line-height-text-xxl)'],
      },

      textColor: {
        color: {
          subtle: 'var(--color-text-subtle)',
          DEFAULT: 'var(--color-text)',
          strong: 'var(--color-text-strong)',
          semantic: 'var(--color-text-semantic)',
          main: {
            DEFAULT: 'var(--color-main)',
            dark: 'var(--color-main-dark)',
            faded: 'var(--color-main-faded)',
            light: 'var(--color-main-light)',
          },
          neutral: {
            DEFAULT: 'var(--color-neutral)',
            dark: 'var(--color-neutral-dark)',
            faded: 'var(--color-neutral-faded)',
            light: 'var(--color-neutral-light)',
          },
          positive: {
            DEFAULT: 'var(--color-positive)',
            dark: 'var(--color-positive-dark)',
            faded: 'var(--color-positive-faded)',
            light: 'var(--color-positive-light)',
          },
          caution: {
            DEFAULT: 'var(--color-caution)',
            dark: 'var(--color-caution-dark)',
            faded: 'var(--color-caution-faded)',
            light: 'var(--color-caution-light)',
          },
          critical: {
            DEFAULT: 'var(--color-critical)',
            dark: 'var(--color-critical-dark)',
            faded: 'var(--color-critical-faded)',
            light: 'var(--color-critical-light)',
          },
          info: {
            DEFAULT: 'var(--color-info)',
            dark: 'var(--color-info-dark)',
            faded: 'var(--color-info-faded)',
            light: 'var(--color-info-light)',
          },
        },
      },

      zIndex: {
        'modal-backdrop': 'var(--z-modal-backdrop)',
        backdrop: 'var(--z-backdrop)',
        dropdown: 'var(--z-dropdown)',
        fixed: 'var(--z-fixed)',
        modal: 'var(--z-modal)',
        offcanvas: 'var(--z-offcanvas)',
        popover: 'var(--z-popover)',
        sticky: 'var(--z-sticky)',
        tooltip: 'var(--z-tooltip)',
      },

      typography: {
        DEFAULT: {
          css: {
            '--tw-prose-body': 'var(--color-text)',
            '--tw-prose-headings': 'var(--color-text-strong)',
            '--tw-prose-lead': 'var(--color-text-strong)',
            '--tw-prose-links': 'var(--color-info)',
            '--tw-prose-bold': 'var(--color-text-strong)',
            '--tw-prose-counters': 'var(--color-neutral)',
            '--tw-prose-bullets': 'var(--color-neutral)',
            '--tw-prose-hr': 'var(--color-border)',
            '--tw-prose-quotes': 'var(--color-neutral)',
            '--tw-prose-quote-borders': 'var(--color-neutral-dark)',
            '--tw-prose-captions': 'var(--color-text-subtle)',
            '--tw-prose-code': 'var(--color-main)',
            '--tw-prose-pre-code': 'var(--color-main-faded)',
            '--tw-prose-pre-bg': 'var(--color-surface-subtle)',
            '--tw-prose-th-borders': 'var(--color-border)',
            '--tw-prose-td-borders': 'var(--color-border)',
          },
        },
      },
    },
  },
}
