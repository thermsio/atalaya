/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path')
const pkg = require('./package.json')

module.exports = {
  ignore: [
    // .fixture.tsx files are used for react-cosmos
    '**/*.fixture.*',
    '**/*.test.*',
  ],

  pagePerSection: true,

  propsParser: require('react-docgen-typescript').parse,

  skipComponentsWithoutExample: true,

  sections: [
    { name: 'Versions', content: 'styleguide/docs/versions.md' },
    { name: 'Changelog', content: 'styleguide/docs/changelog.md' },
    { name: 'Guiding Principles', content: 'styleguide/docs/principles.md' },
    {
      name: 'Foundations',
      content: 'styleguide/docs/foundations.md',
      sections: [
        {
          name: 'Colors',
          content: 'styleguide/docs/colors.md',
        },
        {
          name: 'Spacing',
          content: 'styleguide/docs/spacing.md',
        },
        {
          name: 'Typography',
          content: 'styleguide/docs/typography.md',
        },
        { name: 'Icons', content: 'styleguide/docs/icons.md' },
      ],
      sectionDepth: 2,
    },
    {
      name: 'Layout',
      content: 'styleguide/docs/layout.md',
      components: 'src/layout/**/*.tsx',
      sectionDepth: 2,
    },
    {
      name: 'ReactComponents',
      content: 'styleguide/docs/components.md',
      components: 'src/components/**/*.tsx',
      sectionDepth: 2,
    },
  ],

  styleguideComponents: {
    PlaygroundRenderer: path.join(
      __dirname,
      'styleguide/styleguidistWrappers/PlaygroundRendererWrapper.tsx',
    ),
    SectionsRenderer: path.join(
      __dirname,
      'styleguide/styleguidistWrappers/SectionsRendererWrapper.tsx',
    ),
    StyleGuideRenderer: path.join(
      __dirname,
      'styleguide/styleguidistWrappers/StyleGuideRendererWrapper.tsx',
    ),
  },

  styles: 'styleguide/styles/styles.js',
  theme: 'styleguide/styles/theme.js',

  template: {
    head: {
      raw: [
        '<script async src="https://www.googletagmanager.com/gtag/js?id=G-S0WR5MGQHS"></script>',
        "<script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'G-S0WR5MGQHS');</script>",
      ],
    },
  },

  version: pkg.version,

  webpackConfig: {
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: 'ts-loader',
          exclude: /node_modules/,
        },

        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader', 'postcss-loader'],
        },
        {
          test: /\.(png|jp(e*)g|gif)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: 'images/[hash]-[name].[ext]',
              },
            },
          ],
        },
        {
          test: /\.svg$/,
          use: ['@svgr/webpack'],
        },
      ],
    },
  },
}
