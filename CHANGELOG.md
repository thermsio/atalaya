# [9.21.0](http://bitbucket.org/thermsio/atalaya/compare/v9.20.0...v9.21.0) (2025-02-05)


### Features

* **CORE-1983:** Create backgroundColor prop for ButtonGroupToggle. ([0882f09](http://bitbucket.org/thermsio/atalaya/commits/0882f09ce899fa1f86f9698abb608bd99661f7fa))

# [9.20.0](http://bitbucket.org/thermsio/atalaya/compare/v9.19.0...v9.20.0) (2025-01-23)


### Bug Fixes

* docs build react-select fucking Abdulahi nigerian fucker ([015281a](http://bitbucket.org/thermsio/atalaya/commits/015281aeb670adbecbcbab7761ff520b3081c998))


### Features

* **CORE-2459 CORE-3620:** update react-toastify to v11 ([90324d6](http://bitbucket.org/thermsio/atalaya/commits/90324d69c6bfcefeb1cb9c3a7a69ca364b10cee6))
* **CORE-3620:** Add width to Stack in Toast and Columns in ToastWithAvatar ([f1ba622](http://bitbucket.org/thermsio/atalaya/commits/f1ba6221b0ca3acf69a6f4a1c7b6a05f04b21a72))
* **CORE-3620:** Add width="content" to children Column in Toast ([87f6541](http://bitbucket.org/thermsio/atalaya/commits/87f65416d62e1fd18d46e6869f99900286b11c8d))
* **CORE-3620:** Bump react-toastify from 8.2.0 to 11.0.2. Update types and imports for Toasts ([193d661](http://bitbucket.org/thermsio/atalaya/commits/193d6616933755b79ea1eca37e012e8b8d8b1657))
* **CORE-3623:** Refactor Columns to divs in Toast and ToastWithAvatar. ([5d09ce5](http://bitbucket.org/thermsio/atalaya/commits/5d09ce588ce174b6ccbc7f1ee3b54e6ba8ac7496))

# [9.19.0](http://bitbucket.org/thermsio/atalaya/compare/v9.18.0...v9.19.0) (2024-12-31)


### Features

* **CORE-3620:** Remove propTypes and defaultProps from components. Add interface in SectionTitle. Change const to function throughout. ([3990120](http://bitbucket.org/thermsio/atalaya/commits/399012098c53ed44a3cfafed66161ddb568d815d))

# [9.18.0](http://bitbucket.org/thermsio/atalaya/compare/v9.17.5...v9.18.0) (2024-12-18)


### Features

* limit max size of DropdownContainer ([73a9484](http://bitbucket.org/thermsio/atalaya/commits/73a94848abaf54c6d58504a36c92ce47797a5582))

## [9.17.5](http://bitbucket.org/thermsio/atalaya/compare/v9.17.4...v9.17.5) (2024-12-17)


### Bug Fixes

* update deps ([267fc95](http://bitbucket.org/thermsio/atalaya/commits/267fc95f04c11007a9f374b2f19a49fa73113872))

## [9.17.4](http://bitbucket.org/thermsio/atalaya/compare/v9.17.3...v9.17.4) (2024-11-22)


### Bug Fixes

* update deps ([8a3e541](http://bitbucket.org/thermsio/atalaya/commits/8a3e541adc34c7604b40ed5c8ed22f24c64dce52))

## [9.17.3](http://bitbucket.org/thermsio/atalaya/compare/v9.17.2...v9.17.3) (2024-10-29)


### Bug Fixes

* Pagination, consistent return of block type element; ([1df97a4](http://bitbucket.org/thermsio/atalaya/commits/1df97a4f4b2770a264875f286a96bba499ef4558))

## [9.17.2](http://bitbucket.org/thermsio/atalaya/compare/v9.17.1...v9.17.2) (2024-10-28)


### Bug Fixes

* className not being passed to pagination in some instances; ([211650e](http://bitbucket.org/thermsio/atalaya/commits/211650e65d438cbc614af149d6b4ad6920c1ff72))

## [9.17.1](https://bitbucket.org/thermsio/atalaya/compare/v9.17.0...v9.17.1) (2024-10-17)


### Bug Fixes

* update patch/minor deps, peerDeps react >=17<19, remove prop-types peerDeps ([79bec26](https://bitbucket.org/thermsio/atalaya/commits/79bec26795a699c69521e2d3d8f4a05f861a4ac5))

# [9.17.0](http://bitbucket.org/thermsio/atalaya/compare/v9.16.0...v9.17.0) (2024-10-02)


### Features

* **CORE-3543:** ContextMenu, allow passing a function as controller; ([32a63c7](http://bitbucket.org/thermsio/atalaya/commits/32a63c7aec7a4250de5508dc06c2d2c77c314615))

# [9.16.0](http://bitbucket.org/thermsio/atalaya/compare/v9.15.0...v9.16.0) (2024-09-10)


### Features

* **CORE-3125:** Hide tag color square when no color defined; ([24f904f](http://bitbucket.org/thermsio/atalaya/commits/24f904fec48a72046e47d1bc71bdaea823b0c077))

# [9.15.0](http://bitbucket.org/thermsio/atalaya/compare/v9.14.1...v9.15.0) (2024-09-10)


### Features

* **CORE-3125:** tweak CopyText styling; stop taking up space on document layout when alwaysShow=[secure]; ([c63a8c4](http://bitbucket.org/thermsio/atalaya/commits/c63a8c4dd080720a24729493a55cfdeadb9b827c))

## [9.14.1](http://bitbucket.org/thermsio/atalaya/compare/v9.14.0...v9.14.1) (2024-09-10)


### Bug Fixes

* **CORE-3125:** add subtleBackground to dep index; ([010aa40](http://bitbucket.org/thermsio/atalaya/commits/010aa40fbd1c472e944b29c72a4ed43bb71427dd))

# [9.14.0](http://bitbucket.org/thermsio/atalaya/compare/v9.13.4...v9.14.0) (2024-09-10)


### Features

* **CORE-3125:** add subtle prop to Tag; ([1eaef6b](http://bitbucket.org/thermsio/atalaya/commits/1eaef6b9e65e1bbb9363663cff564eaeabbe268a))

## [9.13.4](http://bitbucket.org/thermsio/atalaya/compare/v9.13.3...v9.13.4) (2024-09-07)


### Bug Fixes

* Lightbox control row being displayed horizontally; ([48ede0d](http://bitbucket.org/thermsio/atalaya/commits/48ede0d9e5795abafd78c26004176d184828e398))
* Lightbox control row being displayed horizontally; ([61015b0](http://bitbucket.org/thermsio/atalaya/commits/61015b0af383dc0c1f7777aefed318e113697049))

## [9.13.3](http://bitbucket.org/thermsio/atalaya/compare/v9.13.2...v9.13.3) (2024-08-27)


### Bug Fixes

* **CORE-3521:** DropdownContainer not flipping correctly on inset mode ([bc75917](http://bitbucket.org/thermsio/atalaya/commits/bc75917b0352396725104c7823813a10a09888a2))

## [9.13.2](http://bitbucket.org/thermsio/atalaya/compare/v9.13.1...v9.13.2) (2024-08-14)


### Bug Fixes

* update deps ([3462801](http://bitbucket.org/thermsio/atalaya/commits/34628013a5a61e448bbbff2c80abcf1cffedf195))

## [9.13.1](http://bitbucket.org/thermsio/atalaya/compare/v9.13.0...v9.13.1) (2024-08-12)


### Bug Fixes

* **CORE-3506:** fix DayPicker range, end timestamp marking start of day instead of end of day; ([e67797b](http://bitbucket.org/thermsio/atalaya/commits/e67797ba383afd489d129278f299725b628468ef))

# [9.13.0](http://bitbucket.org/thermsio/atalaya/compare/v9.12.5...v9.13.0) (2024-07-11)


### Bug Fixes

* update package-lock.json ([13606ca](http://bitbucket.org/thermsio/atalaya/commits/13606ca2793cab66b42e713fd2b43bf2d9ddb8fb))


### Features

* refresh pipeline; ([a0583f6](http://bitbucket.org/thermsio/atalaya/commits/a0583f6669341b295f2b73592e4a76454e7750a3))

## [9.12.5](https://bitbucket.org/thermsio/atalaya/compare/v9.12.4...v9.12.5) (2024-07-08)


### Bug Fixes

* SemanticVariant imports ([6b8af63](https://bitbucket.org/thermsio/atalaya/commits/6b8af630e2def3203380eeb4765748c24f9145a2))
* sourcemaps removed from rollup ([4e164f3](https://bitbucket.org/thermsio/atalaya/commits/4e164f38420b14185e05ee49cbf2571463383ddc))

## [9.12.5](https://bitbucket.org/thermsio/atalaya/compare/v9.12.4...v9.12.5) (2024-07-08)


### Bug Fixes

* sourcemaps removed from rollup ([4e164f3](https://bitbucket.org/thermsio/atalaya/commits/4e164f38420b14185e05ee49cbf2571463383ddc))

## [9.12.4](https://bitbucket.org/thermsio/atalaya/compare/v9.12.3...v9.12.4) (2024-07-08)


### Bug Fixes

* inline sourcemap ([c943a06](https://bitbucket.org/thermsio/atalaya/commits/c943a06b5486b330cc0e537c86b7a3cd3c3a0d71))

## [9.12.3](https://bitbucket.org/thermsio/atalaya/compare/v9.12.2...v9.12.3) (2024-07-08)


### Bug Fixes

* rollup config and outputs ([0b2ef10](https://bitbucket.org/thermsio/atalaya/commits/0b2ef10b072893279299d7d0b3d726a85cc0e59f))

## [9.12.2](https://bitbucket.org/thermsio/atalaya/compare/v9.12.1...v9.12.2) (2024-07-08)


### Bug Fixes

* export typings bundle ([3fe6b75](https://bitbucket.org/thermsio/atalaya/commits/3fe6b75200447d3246116f3f4247adf21e779e8b))

## [9.12.1](http://bitbucket.org/thermsio/atalaya/compare/v9.12.0...v9.12.1) (2024-07-08)


### Bug Fixes

* export typings ([1c90291](http://bitbucket.org/thermsio/atalaya/commits/1c90291502b0fbae10d9677c69186c5b86cf771c))

# [9.12.0](http://bitbucket.org/thermsio/atalaya/compare/v9.11.0...v9.12.0) (2024-07-08)


### Features

* update all deps ([38866b1](http://bitbucket.org/thermsio/atalaya/commits/38866b1bbfc80a75e07cd9226950bda1966f578b))

# [9.11.0](http://bitbucket.org/thermsio/atalaya/compare/v9.10.0...v9.11.0) (2024-06-19)


### Features

* **CORE-2969:** add sideNavPages to Pagination; ([3148f34](http://bitbucket.org/thermsio/atalaya/commits/3148f348dc629777dcf0d5ac4315453f7c92001d))

# [9.10.0](http://bitbucket.org/thermsio/atalaya/compare/v9.9.1...v9.10.0) (2024-06-07)


### Features

* **CORE-3446:** Refactor DateTimeRangePicker wrapping & HourMinutePick width ([0ac4227](http://bitbucket.org/thermsio/atalaya/commits/0ac4227a4857c062bbc36d251f108bd443c15d7c))

## [9.9.1](http://bitbucket.org/thermsio/atalaya/compare/v9.9.0...v9.9.1) (2024-05-10)


### Bug Fixes

* Select going behind sticky items; ([ce6c2fa](http://bitbucket.org/thermsio/atalaya/commits/ce6c2faa9589531427b8617998245b57f49b1a6f))

# [9.9.0](http://bitbucket.org/thermsio/atalaya/compare/v9.8.5...v9.9.0) (2024-05-03)


### Features

* allow Select component to set menu position; ([e32fc89](http://bitbucket.org/thermsio/atalaya/commits/e32fc890523085b115758fad0b6ba8e462e1712c))

## [9.8.5](https://bitbucket.org/thermsio/atalaya/compare/v9.8.4...v9.8.5) (2024-05-03)


### Bug Fixes

* ts error ([67868a2](https://bitbucket.org/thermsio/atalaya/commits/67868a2fb24011dbd29552fddf3ab3091872d1fa))

## [9.8.4](https://bitbucket.org/thermsio/atalaya/compare/v9.8.3...v9.8.4) (2024-05-03)


### Bug Fixes

* Update deps ([75d2c6c](https://bitbucket.org/thermsio/atalaya/commits/75d2c6ca2fcdd2d3c1fc98cd4303bb913dccc40a))

## [9.8.3](https://bitbucket.org/thermsio/atalaya/compare/v9.8.2...v9.8.3) (2024-05-03)


### Bug Fixes

* Update deps ([cccb6eb](https://bitbucket.org/thermsio/atalaya/commits/cccb6eb2577600026e07f2dc4007bcb7776080af))

## [9.8.2](http://bitbucket.org/thermsio/atalaya/compare/v9.8.1...v9.8.2) (2024-04-26)


### Bug Fixes

* **CORE-3409:** pass labelBehavior prop from Input to FormControlWrapper; ([cacf54e](http://bitbucket.org/thermsio/atalaya/commits/cacf54ed9b3eeda71368c5bd8ce46ec53856533b))

## [9.8.1](http://bitbucket.org/thermsio/atalaya/compare/v9.8.0...v9.8.1) (2024-04-25)


### Bug Fixes

* **CORE-3341:** scroll issues on stacked Modals when bottom modal was taller than the one on top; ([8533a61](http://bitbucket.org/thermsio/atalaya/commits/8533a61cb2b7edacecd77a9c6d5f796f58a33660))

# [9.8.0](http://bitbucket.org/thermsio/atalaya/compare/v9.7.3...v9.8.0) (2024-04-25)


### Features

* **CORE-3409:** add floating label to form controls; ([1f1184e](http://bitbucket.org/thermsio/atalaya/commits/1f1184e56ac6bb309f4f4b73fa275ba5a7730165))

## [9.7.3](http://bitbucket.org/thermsio/atalaya/compare/v9.7.2...v9.7.3) (2024-04-23)


### Bug Fixes

* **Select:** padding on select combined ([679b9b4](http://bitbucket.org/thermsio/atalaya/commits/679b9b49157697c05426e2888b6286c58c0a2b4e))

## [9.7.2](http://bitbucket.org/thermsio/atalaya/compare/v9.7.1...v9.7.2) (2024-04-23)


### Bug Fixes

* **ButtonGroupToggle:** types ([82e261c](http://bitbucket.org/thermsio/atalaya/commits/82e261c691ec0df2da11ee36d064baa60f8ff497))

## [9.7.1](http://bitbucket.org/thermsio/atalaya/compare/v9.7.0...v9.7.1) (2024-04-23)


### Bug Fixes

* docs import ([38b4a35](http://bitbucket.org/thermsio/atalaya/commits/38b4a352d5855358fdc10c3c69f48ed86d6d2a73))
* missing export ([99bad56](http://bitbucket.org/thermsio/atalaya/commits/99bad563b75e4d982480a96d402bad592e6aff54))

# [9.7.0](http://bitbucket.org/thermsio/atalaya/compare/v9.6.2...v9.7.0) (2024-04-23)


### Features

* **ButtonGroupToggle:** new component ([27426c3](http://bitbucket.org/thermsio/atalaya/commits/27426c3989d7aa237fc807cfa77f73f8767fb31a))

## [9.6.2](https://bitbucket.org/thermsio/atalaya/compare/v9.6.1...v9.6.2) (2024-04-22)


### Bug Fixes

* **ButtonWithActionModal:** optional children props ([ea36ba3](https://bitbucket.org/thermsio/atalaya/commits/ea36ba30bebc141e408af3640c11112347924736))

## [9.6.1](https://bitbucket.org/thermsio/atalaya/compare/v9.6.0...v9.6.1) (2024-04-22)


### Bug Fixes

* update deps ([045daf6](https://bitbucket.org/thermsio/atalaya/commits/045daf60ef2da4743d21c22dd85f3dd8d6342672))

# [9.6.0](http://bitbucket.org/thermsio/atalaya/compare/v9.5.1...v9.6.0) (2024-04-22)


### Features

* Add title to omitted ButtonProps for ButtonWithActionModal props ([2c23948](http://bitbucket.org/thermsio/atalaya/commits/2c239485b923c430d38c768ad9f3088ca14d5203))

## [9.5.1](http://bitbucket.org/thermsio/atalaya/compare/v9.5.0...v9.5.1) (2024-04-19)


### Bug Fixes

* **CORE-3341:** ButtonWithActionModal, icon prop not working; ([9ff6fe7](http://bitbucket.org/thermsio/atalaya/commits/9ff6fe708c4e81b92f221d3b7aae0bc3b5326a17))

# [9.5.0](http://bitbucket.org/thermsio/atalaya/compare/v9.4.0...v9.5.0) (2024-04-19)


### Features

* **CORE-3341:** ButtonWithActionModal, expand props; ([2d2da3f](http://bitbucket.org/thermsio/atalaya/commits/2d2da3fe0b31878a504700e5c81a7652c1a71e7d))

# [9.4.0](http://bitbucket.org/thermsio/atalaya/compare/v9.3.0...v9.4.0) (2024-04-10)


### Features

* **DateTimeRangePicker:** add onClearValue prop ([1e8aedc](http://bitbucket.org/thermsio/atalaya/commits/1e8aedc7bed181ab0af8470f95df9da71373e001))

# [9.3.0](http://bitbucket.org/thermsio/atalaya/compare/v9.2.1...v9.3.0) (2024-03-26)


### Features

* remove deprecated code; ([15bf17e](http://bitbucket.org/thermsio/atalaya/commits/15bf17e3e81f2535914a88947b86c08ac4d9e942))

## [9.2.1](http://bitbucket.org/thermsio/atalaya/compare/v9.2.0...v9.2.1) (2024-03-26)


### Bug Fixes

* **CORE-3330:** fix errors with DateTimePicker; ([a9f3089](http://bitbucket.org/thermsio/atalaya/commits/a9f3089108b81d27931be03e032f5cc5e41b5ffe))

# [9.2.0](http://bitbucket.org/thermsio/atalaya/compare/v9.1.0...v9.2.0) (2024-03-24)


### Features

* tweak dark colors; ([08738d1](http://bitbucket.org/thermsio/atalaya/commits/08738d1c36b6e588a1841fc4612873209b3cddcb))

# [9.1.0](http://bitbucket.org/thermsio/atalaya/compare/v9.0.1...v9.1.0) (2024-03-23)


### Bug Fixes

* ts error ([6a24b45](http://bitbucket.org/thermsio/atalaya/commits/6a24b4539c011ef26d694e727085c5fd35bf3121))


### Features

* removed deprecated use of 2x classes, dark/light color theme changes ([0b762c9](http://bitbucket.org/thermsio/atalaya/commits/0b762c904138385859d25b4870be64818db16dd4))

## [9.0.1](http://bitbucket.org/thermsio/atalaya/compare/v9.0.0...v9.0.1) (2024-03-22)

### Bug Fixes

- docs crashing; ([cd7b6cd](http://bitbucket.org/thermsio/atalaya/commits/cd7b6cd4aeb3172097d650a3f883e53a705edeee))

# [9.0.0](http://bitbucket.org/thermsio/atalaya/compare/v8.60.0...v9.0.0) (2024-03-22)

### Features

- **CORE-3330:** restore transition icon from TimeRangePicker ([91f40f2](http://bitbucket.org/thermsio/atalaya/commits/91f40f2fafe123baea2c642e90ed1f7747c38030))
- **CORE-3330:** revamp TimePicker -wip; ([2f554b8](http://bitbucket.org/thermsio/atalaya/commits/2f554b82500708d5a1cf105ff77660eb1c598718))
- **CORE-3330:** revamp TimePicker; ([83c16db](http://bitbucket.org/thermsio/atalaya/commits/83c16db111de750312ebb9cc737c4b9556d37304))
- **CORE-3330:** TimePicker center items in mobile bottom sheet; ([06529ee](http://bitbucket.org/thermsio/atalaya/commits/06529ee11cf36e12bda6d6a22d17581a5438907c))
- **CORE-3330:** TimeRangePicker center items in mobile bottom sheet; ([0e9bead](http://bitbucket.org/thermsio/atalaya/commits/0e9beadee477f1a082eb33a3ee6a52e285af4a81))

### BREAKING CHANGES

- **CORE-3330:** TimePicker props have changed;

# [8.60.0](http://bitbucket.org/thermsio/atalaya/compare/v8.59.0...v8.60.0) (2024-03-11)

### Features

- add hint to Checkbox; ([89e8368](http://bitbucket.org/thermsio/atalaya/commits/89e836804c33d950e3d82485b0e400e8ec769308))

# [8.59.0](http://bitbucket.org/thermsio/atalaya/compare/v8.58.2...v8.59.0) (2024-03-06)

### Features

- **CORE-3341:** WeekDays, make onChangeValue optional, add styles for non-interactive presentation; ([e10b90e](http://bitbucket.org/thermsio/atalaya/commits/e10b90e2aa6712dcbfc22208e6f4faca161ef5c8))

## [8.58.2](http://bitbucket.org/thermsio/atalaya/compare/v8.58.1...v8.58.2) (2024-02-23)

### Bug Fixes

- TimePicker max and min numbers being wrong; ([35646d5](http://bitbucket.org/thermsio/atalaya/commits/35646d5e77d338d3b4f349bd29c8c67cb440414d))

## [8.58.1](http://bitbucket.org/thermsio/atalaya/compare/v8.58.0...v8.58.1) (2024-02-09)

### Bug Fixes

- TableRow duplicating row selection; ([285e0ca](http://bitbucket.org/thermsio/atalaya/commits/285e0ca60ec25b1e8646b7c0428845a101581faf))

# [8.58.0](http://bitbucket.org/thermsio/atalaya/compare/v8.57.0...v8.58.0) (2024-02-08)

### Features

- ToggleSwitch add toggleFirst & subTextInline; ([f6300f0](http://bitbucket.org/thermsio/atalaya/commits/f6300f025315df1e377c44a8fad52b1be2ad8cb7))

# [8.57.0](http://bitbucket.org/thermsio/atalaya/compare/v8.56.1...v8.57.0) (2024-01-18)

### Features

- **CORE-3297:** hide tooltip if it's falsy; ([72424ef](http://bitbucket.org/thermsio/atalaya/commits/72424ef8a9f3a3e580025794dc5f02c1b2c3ab4d))

## [8.56.1](http://bitbucket.org/thermsio/atalaya/compare/v8.56.0...v8.56.1) (2024-01-17)

### Bug Fixes

- build crash; ([71eed26](http://bitbucket.org/thermsio/atalaya/commits/71eed26b2b236eb32cbacc803f4afe033e36c683))
- FormControls, "0" appearing on valid/error place; ([b0ff9bc](http://bitbucket.org/thermsio/atalaya/commits/b0ff9bc1975da627f0f3c43008ecea7e5863fb30))

# [8.56.0](http://bitbucket.org/thermsio/atalaya/compare/v8.55.0...v8.56.0) (2024-01-16)

### Features

- **CORE-3299:** TimePicker, fix default value for timestampEnd; ([99569bd](http://bitbucket.org/thermsio/atalaya/commits/99569bd321e29e7a4c4a655cb5de1daa589b0d7e))

# [8.55.0](http://bitbucket.org/thermsio/atalaya/compare/v8.54.0...v8.55.0) (2024-01-16)

### Features

- **CORE-3299:** add hours/minutesEnabled prop to HoursMinuteSelect; ([08b5458](http://bitbucket.org/thermsio/atalaya/commits/08b5458530fb4f2ca8aa75de9b9a15f5b78b9399))

# [8.54.0](http://bitbucket.org/thermsio/atalaya/compare/v8.53.0...v8.54.0) (2024-01-09)

### Features

- **CORE-3299:** tweak WeekDaysPicker height to align with other FormControls; ([dd3a8fb](http://bitbucket.org/thermsio/atalaya/commits/dd3a8fb17b2856402bdb40be8942c293eb05a93c))

# [8.53.0](http://bitbucket.org/thermsio/atalaya/compare/v8.52.0...v8.53.0) (2024-01-09)

### Features

- **CORE-3299:** tweak FormLayout spacing; ([edf7dce](http://bitbucket.org/thermsio/atalaya/commits/edf7dceb40f2f0b2fad53f574fcb678456d8003e))

# [8.52.0](http://bitbucket.org/thermsio/atalaya/compare/v8.51.0...v8.52.0) (2024-01-08)

### Features

- **CORE-3299:** Add WeekDaysPicker; ([e6151f3](http://bitbucket.org/thermsio/atalaya/commits/e6151f32941276feac370c2bc9f382834da33907))

# [8.51.0](http://bitbucket.org/thermsio/atalaya/compare/v8.50.3...v8.51.0) (2023-12-26)

### Features

- **CORE-3275:** dropdown change strategy to fixed; ([5d93fde](http://bitbucket.org/thermsio/atalaya/commits/5d93fde8475ece3bb933d6a33fd7e55e400f1f7d))

## [8.50.3](http://bitbucket.org/thermsio/atalaya/compare/v8.50.2...v8.50.3) (2023-12-25)

### Bug Fixes

- **Slider:** add realtive position to track ([646cba8](http://bitbucket.org/thermsio/atalaya/commits/646cba82e09dc6949be5fc21458d5b770d80eef2))

## [8.50.2](http://bitbucket.org/thermsio/atalaya/compare/v8.50.1...v8.50.2) (2023-12-05)

### Bug Fixes

- remove !; ([be9c71e](http://bitbucket.org/thermsio/atalaya/commits/be9c71e7d7ae1b8f590bfd28ac973f1b0b59a67d))

## [8.50.1](https://bitbucket.org/thermsio/atalaya/compare/v8.50.0...v8.50.1) (2023-12-05)

### Bug Fixes

- Date Picker blocking incorrect dates; ([e9031da](https://bitbucket.org/thermsio/atalaya/commits/e9031daf2e582d539a8af0d8e16f12c7e98c2687))

# [8.50.0](http://bitbucket.org/thermsio/atalaya/compare/v8.49.3...v8.50.0) (2023-12-04)

### Features

- DatePicker disableDatesAfterDate, disableDatesBeforeDate; ([4b9708a](http://bitbucket.org/thermsio/atalaya/commits/4b9708af8494614e8cdd5afbaea08d2ce442df6e))

## [8.49.3](http://bitbucket.org/thermsio/atalaya/compare/v8.49.2...v8.49.3) (2023-11-27)

### Bug Fixes

- **CORE-3165:** fix DateTimePicker re-opening after closing on mobile; ([4cc0f08](http://bitbucket.org/thermsio/atalaya/commits/4cc0f08b42125eee138473de7284dda2d512dae5))

## [8.49.2](http://bitbucket.org/thermsio/atalaya/compare/v8.49.1...v8.49.2) (2023-11-24)

### Bug Fixes

- **CORE-3098:** Modal, prevent overlays from going outside children; ([b1138ab](http://bitbucket.org/thermsio/atalaya/commits/b1138ab10f4caece6f18e3eec7d347fa13d0b63e))

## [8.49.1](http://bitbucket.org/thermsio/atalaya/compare/v8.49.0...v8.49.1) (2023-11-22)

### Bug Fixes

- Modal, keep padding consistent if header is not rendered; ([de74c5f](http://bitbucket.org/thermsio/atalaya/commits/de74c5f9be3bd243a5d95088e6c5763db9747f94))

# [8.49.0](http://bitbucket.org/thermsio/atalaya/compare/v8.48.0...v8.49.0) (2023-11-21)

### Features

- **CORE-3260:** improve DateTimeRangePicker logic; ([5171fb6](http://bitbucket.org/thermsio/atalaya/commits/5171fb69dddb463f0b842e9ea0f33eff4d24102b))

# [8.48.0](http://bitbucket.org/thermsio/atalaya/compare/v8.47.2...v8.48.0) (2023-11-17)

### Features

- **CORE-3164:** remove ={true} no Select.captureMenuScroll; ([bbf5946](http://bitbucket.org/thermsio/atalaya/commits/bbf594669b0f35435bd3b14d4fda6c7ba3136fc3))

## [8.47.2](http://bitbucket.org/thermsio/atalaya/compare/v8.47.1...v8.47.2) (2023-11-17)

### Bug Fixes

- **CORE-3164:** use a non hacky way of fixing onMouseScrollToBottom callback; ([a126926](http://bitbucket.org/thermsio/atalaya/commits/a12692636c12dc3e6e6acfa1de73b3acafa6a19a))

## [8.47.1](http://bitbucket.org/thermsio/atalaya/compare/v8.47.0...v8.47.1) (2023-11-17)

### Bug Fixes

- **CORE-3164:** fix onMouseScrollToBottom not working on mobile; ([5b731e2](http://bitbucket.org/thermsio/atalaya/commits/5b731e2bd088f528aeadd00e69f6ab9edec19bc2))

# [8.47.0](http://bitbucket.org/thermsio/atalaya/compare/v8.46.0...v8.47.0) (2023-11-15)

### Features

- **CORE-3164:** fix Select component not showing options inside bottom sheet; ([0639af5](http://bitbucket.org/thermsio/atalaya/commits/0639af5e6d1d4e4b0e4cba7a5c3920c5221c0883))

# [8.46.0](http://bitbucket.org/thermsio/atalaya/compare/v8.45.0...v8.46.0) (2023-11-15)

### Features

- **Hoverable:** add hoverStyle prop ([0cb1019](http://bitbucket.org/thermsio/atalaya/commits/0cb10198d779453eccd2ca3e389cd0b5cc31a404))

# [8.45.0](http://bitbucket.org/thermsio/atalaya/compare/v8.44.0...v8.45.0) (2023-11-14)

### Features

- **Hoverable:** add onHoverStateChange prop ([91f8cb1](http://bitbucket.org/thermsio/atalaya/commits/91f8cb133e39c3f379242ad0a05ecf327f92846a))

# [8.44.0](http://bitbucket.org/thermsio/atalaya/compare/v8.43.0...v8.44.0) (2023-11-14)

### Features

- **Hoverable:** rename Hoverable.Content to .HoverContent add .TextEllipse ([e3e8166](http://bitbucket.org/thermsio/atalaya/commits/e3e81663db013577ecd1d900deeb9a9e9db9f450))

# [8.43.0](http://bitbucket.org/thermsio/atalaya/compare/v8.42.3...v8.43.0) (2023-11-14)

### Features

- **Hoverable:** add new component ([ecfd77e](http://bitbucket.org/thermsio/atalaya/commits/ecfd77ee004c9385d1cedac19746aae369bd5fac))

## [8.42.3](https://bitbucket.org/thermsio/atalaya/compare/v8.42.2...v8.42.3) (2023-11-13)

### Bug Fixes

- major fuckup because Tom didn't have his coffee ([ef0de7a](https://bitbucket.org/thermsio/atalaya/commits/ef0de7ad1550fba4b5d20ea02343aae6cb481467))

## [8.42.2](https://bitbucket.org/thermsio/atalaya/compare/v8.42.1...v8.42.2) (2023-11-13)

### Bug Fixes

- ButtonWithModal modalComponent propTypes ts error ([df8b169](https://bitbucket.org/thermsio/atalaya/commits/df8b1691016df4e681b679ffd58af26ee7c37adf))

## [8.42.1](https://bitbucket.org/thermsio/atalaya/compare/v8.42.0...v8.42.1) (2023-11-13)

### Bug Fixes

- ts error ([ce6e494](https://bitbucket.org/thermsio/atalaya/commits/ce6e494c39f34f728d76c879871954ef11cfe4c6))

# [8.42.0](http://bitbucket.org/thermsio/atalaya/compare/v8.41.1...v8.42.0) (2023-11-13)

### Features

- **ButtonWithModal:** allow modalComponent ([8b77396](http://bitbucket.org/thermsio/atalaya/commits/8b773966096f05cb5b92f3b04251af9407a59394))

## [8.41.1](http://bitbucket.org/thermsio/atalaya/compare/v8.41.0...v8.41.1) (2023-11-09)

### Bug Fixes

- TS2590 error due to React.memo wrappers ([2e4ea10](http://bitbucket.org/thermsio/atalaya/commits/2e4ea10449793dea2285a1570cf6fd4b1787e2de))

# [8.41.0](http://bitbucket.org/thermsio/atalaya/compare/v8.40.0...v8.41.0) (2023-11-07)

### Features

- **CORE-3216:** Tweak dayPicker range pick logic to enable picking the same day; ([80e62b8](http://bitbucket.org/thermsio/atalaya/commits/80e62b8eecf8129bbf625ce20fb2c198beba88c5))

# [8.40.0](https://bitbucket.org/thermsio/atalaya/compare/v8.39.0...v8.40.0) (2023-10-17)

### Bug Fixes

- update deps ([09418fd](https://bitbucket.org/thermsio/atalaya/commits/09418fdfd6ffb4d2088f7cb104d4546c6279e9b8))

### Features

- **Slider:** add onChangeImmediate() ([9405a59](https://bitbucket.org/thermsio/atalaya/commits/9405a5931b9d407f1b14bffff1d946c61d462a52))
- **Slider:** add renderThumb prop ([020ca2e](https://bitbucket.org/thermsio/atalaya/commits/020ca2e96a8ebf3f5c1adeec0d08d8dcf9bc347a))

# [8.39.0](http://bitbucket.org/thermsio/atalaya/compare/v8.38.0...v8.39.0) (2023-10-13)

### Features

- **CORE-2608:** TimelineItem remove extra padding; ([ecd45e2](http://bitbucket.org/thermsio/atalaya/commits/ecd45e20d5f4a3a0cff2b9c59f91fdbca6ed76b7))

# [8.38.0](http://bitbucket.org/thermsio/atalaya/compare/v8.37.2...v8.38.0) (2023-10-12)

### Features

- **CORE-2608:** TimelineItem make timestamp optional; ([2278378](http://bitbucket.org/thermsio/atalaya/commits/227837870ceb76929b79c1550c56cfd992d87240))

## [8.37.2](http://bitbucket.org/thermsio/atalaya/compare/v8.37.1...v8.37.2) (2023-09-15)

### Bug Fixes

- **CORE-3216:** trigger release; ([8259387](http://bitbucket.org/thermsio/atalaya/commits/825938795abd281065e340c1d00a95ab95b304f0))

## [8.37.1](https://bitbucket.org/thermsio/atalaya/compare/v8.37.0...v8.37.1) (2023-09-15)

### Bug Fixes

- **CORE-3216:** HorizontalScroll, fix left items not being accessible on narrow viewports; ([9afdb0a](https://bitbucket.org/thermsio/atalaya/commits/9afdb0ab538efa8d9886167dfcd763eb81feee02))

# [8.37.0](http://bitbucket.org/thermsio/atalaya/compare/v8.36.1...v8.37.0) (2023-09-13)

### Features

- **CORE-3215:** Removed default 100 height/width from Image Skeleton ([1707e49](http://bitbucket.org/thermsio/atalaya/commits/1707e494c48fa17e5f4905dd529b5ddf38092499))

## [8.36.1](http://bitbucket.org/thermsio/atalaya/compare/v8.36.0...v8.36.1) (2023-09-08)

### Bug Fixes

- **CORE-3209 WEB-APP-OPS-41:** Modal fix .at() not being defined on old browsers; ([d68dcef](http://bitbucket.org/thermsio/atalaya/commits/d68dcef1aeaaaa7056adb557c986a08d8ba13bfb))

# [8.36.0](http://bitbucket.org/thermsio/atalaya/compare/v8.35.0...v8.36.0) (2023-09-07)

### Features

- **CORE-3208:** add isOpen prop to tooltip; ([71c5d12](http://bitbucket.org/thermsio/atalaya/commits/71c5d1238fa283d6de01d1f01d1bef9416d050b2))

# [8.35.0](http://bitbucket.org/thermsio/atalaya/compare/v8.34.1...v8.35.0) (2023-09-05)

### Features

- **CORE-3208:** Pagination, center pagination if perPagePicker and description is hidden ([b8a47c8](http://bitbucket.org/thermsio/atalaya/commits/b8a47c80035613d8c198cb322ecfeb7df986f57f))

## [8.34.1](http://bitbucket.org/thermsio/atalaya/compare/v8.34.0...v8.34.1) (2023-08-31)

### Bug Fixes

- FormikRichTextEditor initialHTML ([44cdfe0](http://bitbucket.org/thermsio/atalaya/commits/44cdfe0718c4d7432c8b6cef818e1d9f5c2b6185))

# [8.34.0](http://bitbucket.org/thermsio/atalaya/compare/v8.33.0...v8.34.0) (2023-08-30)

### Features

- **CORE-3208:** use prose on ReactTextEditorDisplayContent; ([7bd57c6](http://bitbucket.org/thermsio/atalaya/commits/7bd57c6de2f9adc472bd5806ba5868e5b798d8c8))

# [8.33.0](http://bitbucket.org/thermsio/atalaya/compare/v8.32.2...v8.33.0) (2023-08-29)

### Features

- **CORE-3208:** add children to RichTextEditorDisplayContent; ([2b3349f](http://bitbucket.org/thermsio/atalaya/commits/2b3349f95a83cf2e6278b2dc1c5d2b7f75ef2dac))

## [8.32.2](http://bitbucket.org/thermsio/atalaya/compare/v8.32.1...v8.32.2) (2023-08-29)

### Reverts

- Revert "feat(): Annul Tailwind reset for lists and menu elements;" ([7e571c0](http://bitbucket.org/thermsio/atalaya/commits/7e571c07364b1af74a02c9524edc9c4ad7097e95))
- Revert "chore(release): 8.32.0 [skip ci]" ([95a6f4d](http://bitbucket.org/thermsio/atalaya/commits/95a6f4d71d32f35b024d2f4807668a31c9e84c5d))
- Revert "fix(): move css list reset to base.css;" ([8b1e684](http://bitbucket.org/thermsio/atalaya/commits/8b1e6848ab108d8b367d1b5d58ca15d20bf67ce8))
- Revert "chore(release): 8.32.1 [skip ci]" ([acf7fc9](http://bitbucket.org/thermsio/atalaya/commits/acf7fc9b648a6928b23d88b4f71a1f173ffdab9d))

# [8.31.0](http://bitbucket.org/thermsio/atalaya/compare/v8.30.0...v8.31.0) (2023-08-23)

### Features

- **CORE-2684:** Added default height & width to loading Skeletons if none are given ([4cfd52e](http://bitbucket.org/thermsio/atalaya/commits/4cfd52e12611447acecda920db267cb250cf572b))

# [8.30.0](http://bitbucket.org/thermsio/atalaya/compare/v8.29.0...v8.30.0) (2023-08-22)

### Features

- **CORE-2684:** Added maxHeight & maxWidth to Image props ([1ec10ca](http://bitbucket.org/thermsio/atalaya/commits/1ec10ca98f9d4fe1c7c10bde090e77f291605eab))

# [8.29.0](http://bitbucket.org/thermsio/atalaya/compare/v8.28.0...v8.29.0) (2023-08-22)

### Features

- **Dropdown:** add useDropdownContext() hook ([0b3ab8d](http://bitbucket.org/thermsio/atalaya/commits/0b3ab8d00b5de8a643d0e1c2c42ad4b067f41a51))

# [8.28.0](http://bitbucket.org/thermsio/atalaya/compare/v8.27.0...v8.28.0) (2023-08-22)

### Features

- update floating-ui to next major version; ([8816ede](http://bitbucket.org/thermsio/atalaya/commits/8816eded14bc7ed7d11fdcfd69235b21755aafcf))

# [8.27.0](http://bitbucket.org/thermsio/atalaya/compare/v8.26.0...v8.27.0) (2023-08-18)

### Features

- **CORE-2728:** Added placeholder-neutral to input.form-field, textarea.form-field class in FormControls.css ([f8cc67f](http://bitbucket.org/thermsio/atalaya/commits/f8cc67fad0ffba7179406cbb84930674b2595713))
- **CORE-2728:** Changed Select placeholder text color ([a849154](http://bitbucket.org/thermsio/atalaya/commits/a849154c996d01ee57ffd6a37f3957797d6134c6))

# [8.26.0](http://bitbucket.org/thermsio/atalaya/compare/v8.25.0...v8.26.0) (2023-08-17)

### Features

- **CORE-3185:** FormControls, add isHorizontal prop; ([5aeaea2](http://bitbucket.org/thermsio/atalaya/commits/5aeaea2ac95940daeef3cfd3d8e0128d3ae3cbaf))

# [8.25.0](http://bitbucket.org/thermsio/atalaya/compare/v8.24.0...v8.25.0) (2023-08-16)

### Features

- **CORE-3091:** Added disabled check to Time input style maxWidth ([53b56e0](http://bitbucket.org/thermsio/atalaya/commits/53b56e013513231ed25fcb23ac11c17561fffd91))

# [8.24.0](http://bitbucket.org/thermsio/atalaya/compare/v8.23.0...v8.24.0) (2023-08-14)

### Bug Fixes

- **CORE-3185:** tests fail after deps update; ([6e562a0](http://bitbucket.org/thermsio/atalaya/commits/6e562a0c720ace4b4c7d0c321cb775bd71e22838))

### Features

- **CORE-3185:** FormControls horizontal layout based on container width instead of `horizontalProp`; ([5faad43](http://bitbucket.org/thermsio/atalaya/commits/5faad433d814882f98e968dc7e1953389aa14d5c))
- FormLayout space prop default to sm; ([68ded32](http://bitbucket.org/thermsio/atalaya/commits/68ded32977918d778a67bbcf43d3191ba98931ea))

# [8.23.0](http://bitbucket.org/thermsio/atalaya/compare/v8.22.1...v8.23.0) (2023-08-10)

### Features

- **CORE-2649:** Lightbox, add left and right arrow navigation; ([1402b0c](http://bitbucket.org/thermsio/atalaya/commits/1402b0cb775d1f1fd485ed3a3df8088ac2548075))

## [8.22.1](http://bitbucket.org/thermsio/atalaya/compare/v8.22.0...v8.22.1) (2023-08-09)

### Bug Fixes

- **CORE-3000:** fix docs build; ([54ef605](http://bitbucket.org/thermsio/atalaya/commits/54ef605cd57078754210e7b30cdd7969fe4ac23b))

# [8.22.0](http://bitbucket.org/thermsio/atalaya/compare/v8.21.1...v8.22.0) (2023-08-09)

### Features

- **CORE-3000:** MultiSelect,add clear selected button; ([e0b1239](http://bitbucket.org/thermsio/atalaya/commits/e0b1239648fcdc85576dac8527d1238f70dca065))

## [8.21.1](http://bitbucket.org/thermsio/atalaya/compare/v8.21.0...v8.21.1) (2023-08-07)

### Bug Fixes

- **CORE-3172:** RichTextEditor, add spell check to rich text editor. ([35ccc21](http://bitbucket.org/thermsio/atalaya/commits/35ccc21ee175735d23b4d3ca4fb85e09c2717f37))

# [8.21.0](http://bitbucket.org/thermsio/atalaya/compare/v8.20.1...v8.21.0) (2023-07-14)

### Features

- **CORE-3134:** Added hover:cursor-pointer class when Dropdown controller is a string ([511f579](http://bitbucket.org/thermsio/atalaya/commits/511f57959fef705c916e63e6d5cf8661b279c844))
- **CORE-3134:** Added ring styling to string controller div in Dropdown index ([66b7ac5](http://bitbucket.org/thermsio/atalaya/commits/66b7ac5e577a2456eec43955d181153d5fba1fcb))
- **CORE-3134:** Removed mt-0.5 styling from controller wrapping divs in Dropdown index ([97b2203](http://bitbucket.org/thermsio/atalaya/commits/97b2203b9169999423473b34fd78030bd9daa1ce))
- **CORE-3134:** Replaced button with div for dropdown controller in ContextMenu index ([bdd55e3](http://bitbucket.org/thermsio/atalaya/commits/bdd55e38d9bb47bc9762a9be8dbed547cbe3dea9))
- **CORE-3134:** Replaced controller wrapping button with div in Dropdown index. Readded button for dropdown controller in ContextMenu index ([33e0d2c](http://bitbucket.org/thermsio/atalaya/commits/33e0d2c545134e737bf0fa58c8ad956d9d3d7900))

## [8.20.1](http://bitbucket.org/thermsio/atalaya/compare/v8.20.0...v8.20.1) (2023-07-14)

### Bug Fixes

- update deps ([d498de3](http://bitbucket.org/thermsio/atalaya/commits/d498de3d7b47d11af9759f42fa07ea344cb775ee))

# [8.20.0](http://bitbucket.org/thermsio/atalaya/compare/v8.19.2...v8.20.0) (2023-07-10)

### Features

- **CORE-3139:** Added color prop to Badge. Updated deps ([c5d5c35](http://bitbucket.org/thermsio/atalaya/commits/c5d5c350ffff55e10fa9a42c1466280ae61738f6))

## [8.19.2](http://bitbucket.org/thermsio/atalaya/compare/v8.19.1...v8.19.2) (2023-07-07)

### Bug Fixes

- **CORE-3053:** Tabs.Tab remove extra bottom padding from tabs; ([70904a3](http://bitbucket.org/thermsio/atalaya/commits/70904a3637d1cbd9abc1ec8f3a48ea73db2b8c24))

## [8.19.1](http://bitbucket.org/thermsio/atalaya/compare/v8.19.0...v8.19.1) (2023-07-06)

### Bug Fixes

- update deps ([8ed731d](http://bitbucket.org/thermsio/atalaya/commits/8ed731d632b59e6a362bf689e91541bd94e832fb))

# [8.19.0](http://bitbucket.org/thermsio/atalaya/compare/v8.18.0...v8.19.0) (2023-06-30)

### Bug Fixes

- **CORE-2966:** Nested ButtonWithModal in Modal breaking parent closeHandler. ([2d2e019](http://bitbucket.org/thermsio/atalaya/commits/2d2e019f51a3cb5460a699b082f22ca0bd96d0e9))

### Features

- **CORE-2666:** Modal, mount it on the natural app flow instead of portaling it; ([bc8e514](http://bitbucket.org/thermsio/atalaya/commits/bc8e514d8369663fda8a55bf01f44374b9d5ed25))

# [8.18.0](http://bitbucket.org/thermsio/atalaya/compare/v8.17.0...v8.18.0) (2023-06-28)

### Features

- **CORE-3051:** Select, add hideValue prop; ([3208d20](http://bitbucket.org/thermsio/atalaya/commits/3208d202e4fa4a02d6efddb71dfab0e67117b48e))

# [8.17.0](http://bitbucket.org/thermsio/atalaya/compare/v8.16.0...v8.17.0) (2023-06-23)

### Features

- **CORE-3064:** Changed lightbox default to [secure] in ImageComponent index. Added lightbox prop to AvatarGroup ([81c9806](http://bitbucket.org/thermsio/atalaya/commits/81c9806da6394037bbdf21db2935af271c4e9c23))
- **CORE-3064:** Removed commented out code ([78c1899](http://bitbucket.org/thermsio/atalaya/commits/78c1899340b8bc83b8952beec99f72240d0da0c8))
- **CORE-3064:** Removed lightbox default in ImageComponent index. ([731cd52](http://bitbucket.org/thermsio/atalaya/commits/731cd5291c351dfc7161497bfabedd850345de0f))

# [8.16.0](http://bitbucket.org/thermsio/atalaya/compare/v8.15.0...v8.16.0) (2023-06-23)

### Bug Fixes

- **CORE-2928:** stop RichTextEditor content from being placed on top of dropdowns; ([931acaf](http://bitbucket.org/thermsio/atalaya/commits/931acaf3083e94b7bb94c95df7dff7e2b2a9b464))

### Features

- **CORE-3058:** make HorizontalScroll buttons more visible; ([9b1534b](http://bitbucket.org/thermsio/atalaya/commits/9b1534b1f4417b3ff0e1ddf3cea3bc27e388e824))

# [8.15.0](http://bitbucket.org/thermsio/atalaya/compare/v8.14.2...v8.15.0) (2023-06-23)

### Features

- **CORE-3124:** Lightbox add download button; ([fb1ea6a](http://bitbucket.org/thermsio/atalaya/commits/fb1ea6a000532e3cbf1c708c2d671c532c05d289))

## [8.14.2](http://bitbucket.org/thermsio/atalaya/compare/v8.14.1...v8.14.2) (2023-06-22)

### Bug Fixes

- **CORE-3020:** add z-dropdown to Select dropdown; ([d2754ce](http://bitbucket.org/thermsio/atalaya/commits/d2754cec9fd2f16047a818c09c1628e8c28aa5a6))

## [8.14.1](http://bitbucket.org/thermsio/atalaya/compare/v8.14.0...v8.14.1) (2023-06-22)

### Bug Fixes

- Image, fix lightbox prop not working on cases where lightboxContext.onClickFile was not defined; ([0ae6ec3](http://bitbucket.org/thermsio/atalaya/commits/0ae6ec383ddc85f25fa3279c3d579be3bd4e8ec7))
- LightboxContextGroup, getting Format Not Supported warning. When url did not have file extension. ([06bf061](http://bitbucket.org/thermsio/atalaya/commits/06bf0614c6e6a6696c501d437cc267f16662b9fb))

# [8.14.0](http://bitbucket.org/thermsio/atalaya/compare/v8.13.5...v8.14.0) (2023-06-21)

### Features

- **CORE-3020:** allow Breadcrumb to accept ReactNodes; ([bbd67ce](http://bitbucket.org/thermsio/atalaya/commits/bbd67cea2f469275bbc2d7b5656df2fa87e3f54c))

## [8.13.5](http://bitbucket.org/thermsio/atalaya/compare/v8.13.4...v8.13.5) (2023-06-17)

### Bug Fixes

- **Modal:** add back relative class ([69198dc](http://bitbucket.org/thermsio/atalaya/commits/69198dc7f37f1cccdddcde7ae8a17dc70f3ec281))

## [8.13.4](http://bitbucket.org/thermsio/atalaya/compare/v8.13.3...v8.13.4) (2023-06-17)

### Bug Fixes

- publish cmd ([e590a0e](http://bitbucket.org/thermsio/atalaya/commits/e590a0ed72adc5115a824f72bb1948bde58d889c))

## [8.13.3](https://bitbucket.org/thermsio/atalaya/compare/v8.13.2...v8.13.3) (2023-06-16)

### Bug Fixes

- **TableGroupContext:** performance memoize ([6d2e98e](https://bitbucket.org/thermsio/atalaya/commits/6d2e98ec94e5d91a93f40de4e7629f8f4b5e352d))

## [8.13.2](https://bitbucket.org/thermsio/atalaya/compare/v8.13.1...v8.13.2) (2023-06-16)

### Bug Fixes

- add logging for prod build testing TableGroup state ([0368ae0](https://bitbucket.org/thermsio/atalaya/commits/0368ae0d2db57cf88b670221776e6b4ff8474fba))

## [8.13.1](http://bitbucket.org/thermsio/atalaya/compare/v8.13.0...v8.13.1) (2023-06-16)

### Bug Fixes

- **TableGroup:** initialize state with getter to avoid global ref pollution ([cb8a6df](http://bitbucket.org/thermsio/atalaya/commits/cb8a6df872598bf9713833b2319f98f8f362c90a))

# [8.13.0](http://bitbucket.org/thermsio/atalaya/compare/v8.12.0...v8.13.0) (2023-06-15)

### Features

- reduce border radius; ([e479e8e](http://bitbucket.org/thermsio/atalaya/commits/e479e8eaf2753ba2f896ab8acf716898ad07a287))

# [8.12.0](http://bitbucket.org/thermsio/atalaya/compare/v8.11.4...v8.12.0) (2023-06-13)

### Features

- **CORE-2996:** AvatarGroup, add xxs and xxl sizes; ([187476f](http://bitbucket.org/thermsio/atalaya/commits/187476f9228ed7eabe0711764c929808a1450eaf))

## [8.11.4](http://bitbucket.org/thermsio/atalaya/compare/v8.11.3...v8.11.4) (2023-06-12)

### Bug Fixes

- **RichTextEditor:** initialHTML bug with no <p> tag ([217d836](http://bitbucket.org/thermsio/atalaya/commits/217d8364c3bd4a42a55354896b3f31a46d28fc1c))

## [8.11.3](http://bitbucket.org/thermsio/atalaya/compare/v8.11.2...v8.11.3) (2023-06-12)

### Bug Fixes

- update deps ([ec278d8](http://bitbucket.org/thermsio/atalaya/commits/ec278d83acb6bff934bdec655893a8ea5d8c9c2f))

## [8.11.2](http://bitbucket.org/thermsio/atalaya/compare/v8.11.1...v8.11.2) (2023-06-08)

### Bug Fixes

- **CORE-3070:** stop passing the click event to close; ([fbc8f8e](http://bitbucket.org/thermsio/atalaya/commits/fbc8f8e1af0ce786c5bf68fdce4648b9f27307aa))

## [8.11.1](http://bitbucket.org/thermsio/atalaya/compare/v8.11.0...v8.11.1) (2023-06-08)

### Bug Fixes

- **CORE-3070:** ButtonWithModal closeHandler not working; ([f98ab20](http://bitbucket.org/thermsio/atalaya/commits/f98ab20146be9b1d22d25521d707677da3d5baf9))
- **CORE-3070:** Modal wrongly trapping focus when there were 2 modal contexts active at the same time; ([537ae33](http://bitbucket.org/thermsio/atalaya/commits/537ae33316701b21fa8eb112cacd85b3641604c2))

# [8.11.0](http://bitbucket.org/thermsio/atalaya/compare/v8.10.0...v8.11.0) (2023-06-07)

### Features

- **CORE-3057:** stop modal from going over toast; ([c1827b8](http://bitbucket.org/thermsio/atalaya/commits/c1827b8e474fd82295f2737baaecf1c2e87d7264))

# [8.10.0](http://bitbucket.org/thermsio/atalaya/compare/v8.9.0...v8.10.0) (2023-06-06)

### Features

- **CORE-3042:** make dark theme the default on all relevant components; ([661e68d](http://bitbucket.org/thermsio/atalaya/commits/661e68de8b431c301052e25ca586171abaf26e23))

# [8.9.0](http://bitbucket.org/thermsio/atalaya/compare/v8.8.0...v8.9.0) (2023-06-06)

### Features

- **CORE-3049:** improve FormControl contrast in light mode; ([262a390](http://bitbucket.org/thermsio/atalaya/commits/262a3902084bf7b10430da4f7a6ff2c3a43de607))

# [8.8.0](http://bitbucket.org/thermsio/atalaya/compare/v8.7.1...v8.8.0) (2023-06-06)

### Features

- **CORE-3050:** fix modal causing screen jump when opening; ([b11210c](http://bitbucket.org/thermsio/atalaya/commits/b11210c7f8279b4503278af018f45a513ba1b9be))

## [8.7.1](http://bitbucket.org/thermsio/atalaya/compare/v8.7.0...v8.7.1) (2023-06-05)

### Bug Fixes

- **CORE-2989:** DateTimeRangePicker fix controller values not updating when getting cleared; ([1032610](http://bitbucket.org/thermsio/atalaya/commits/1032610c95e1812486ced41c606cc2141dc250b7))

# [8.7.0](http://bitbucket.org/thermsio/atalaya/compare/v8.6.0...v8.7.0) (2023-06-02)

### Features

- **CORE-2381:** allow multiple open modals to be closed in order when pressing esc. ([95fadb0](http://bitbucket.org/thermsio/atalaya/commits/95fadb0d68df03c62842313f219850dd000a5a12))

# [8.6.0](http://bitbucket.org/thermsio/atalaya/compare/v8.5.1...v8.6.0) (2023-05-31)

### Features

- **CORE-3000:** Added optionLabelExtractor prop to MultiSelect. Updated deps ([df05d68](http://bitbucket.org/thermsio/atalaya/commits/df05d684d4f5775db7f4febb325d195008e700a0))

## [8.5.1](http://bitbucket.org/thermsio/atalaya/compare/v8.5.0...v8.5.1) (2023-05-23)

### Bug Fixes

- Corrected loadAllOptions prop name in AsyncLoadMissingOptions fixture ([eff28d6](http://bitbucket.org/thermsio/atalaya/commits/eff28d6ccedeea9b92c7f144592002cf5a0a0af2))

# [8.5.0](http://bitbucket.org/thermsio/atalaya/compare/v8.4.2...v8.5.0) (2023-05-23)

### Features

- **CORE-3000:** Refactored handleSelectAll in MultiSelect ([d7c8127](http://bitbucket.org/thermsio/atalaya/commits/d7c81271df56f9e627a7b5ca03a61f10b8c7c191))
- **CORE-3000:** Refactored handleSelectOptionChange in MultiSelect to allow a selected option in the OptionsList to be unselected after all options have been selected. ([e76a2bc](http://bitbucket.org/thermsio/atalaya/commits/e76a2bcca1e3ad80434c28844ebd133c5775ea96))
- **CORE-3000:** Refactored MultiSelect to allow selecting & unselecting all options. ([7a989f2](http://bitbucket.org/thermsio/atalaya/commits/7a989f21a454fad7c369bcdc850553b503a8b72c))
- **CORE-3000:** Renamed fetchAllOptions to loadAllOptions. Moved 'Select All' button out of OptionsLisy & into MultiSelect. ([6c056d6](http://bitbucket.org/thermsio/atalaya/commits/6c056d6ab13bcc542caee784db5856cc904c7da2))
- **CORE-3000:** Replaced useHandleChange with handleChange in MultiSelect. ([10e02a7](http://bitbucket.org/thermsio/atalaya/commits/10e02a724e2e1f14ee0439f5a311b71a5466a65a))
- **CORE-3000:** Updated loadAllOptions explanation text ([47e5608](http://bitbucket.org/thermsio/atalaya/commits/47e5608287019c86d4230cba0edf2509c51b7557))

## [8.4.2](http://bitbucket.org/thermsio/atalaya/compare/v8.4.1...v8.4.2) (2023-05-17)

### Bug Fixes

- stop descender part of letter being cut of on certain font faces in XL text; ([8154772](http://bitbucket.org/thermsio/atalaya/commits/81547729fa26c0b8fa40f98a21da8d21d38853ab))

## [8.4.1](https://bitbucket.org/thermsio/atalaya/compare/v8.4.0...v8.4.1) (2023-05-14)

### Bug Fixes

- useDarkMode and change ls key ([29de329](https://bitbucket.org/thermsio/atalaya/commits/29de329180f20aa4e7dccba9f67fbe90e3f10243))

# [8.4.0](https://bitbucket.org/thermsio/atalaya/compare/v8.3.0...v8.4.0) (2023-05-13)

### Features

- allow defaultTheme prop on AtalayaWrapper ([a5c8bb2](https://bitbucket.org/thermsio/atalaya/commits/a5c8bb25867d645db45de8d5076e8bad5350d802))

# [8.3.0](http://bitbucket.org/thermsio/atalaya/compare/v8.2.1...v8.3.0) (2023-05-11)

### Features

- AtalayaWrapper, add defaultTheme prop; ([76d5370](http://bitbucket.org/thermsio/atalaya/commits/76d53705eeecd2ff01c5bb6af6c1f887fca5fccc))

## [8.2.1](http://bitbucket.org/thermsio/atalaya/compare/v8.2.0...v8.2.1) (2023-05-11)

### Bug Fixes

- **Number:** revert formattedValue prop option ([97c6d13](http://bitbucket.org/thermsio/atalaya/commits/97c6d133ef6a7fd520458094ae50e751b2b38219))

# [8.2.0](http://bitbucket.org/thermsio/atalaya/compare/v8.1.1...v8.2.0) (2023-05-11)

### Features

- **Number:** field allows formattedValue prop ([05b5e4a](http://bitbucket.org/thermsio/atalaya/commits/05b5e4ad2e1a5959f135aad42e9f8a7ce2ebd452))

## [8.1.1](http://bitbucket.org/thermsio/atalaya/compare/v8.1.0...v8.1.1) (2023-05-08)

### Bug Fixes

- **RTE:** background color use field input class ([16b6e2e](http://bitbucket.org/thermsio/atalaya/commits/16b6e2ef274cf7333eb280801e5c84d9889c0588))

# [8.1.0](http://bitbucket.org/thermsio/atalaya/compare/v8.0.2...v8.1.0) (2023-05-05)

### Features

- Added 'maxNotification' prop to Tab to increase Badge max count when needed. ([d9d447e](http://bitbucket.org/thermsio/atalaya/commits/d9d447e9aa4e147907c0539610e3c40cbf8db4ee))

## [8.0.2](http://bitbucket.org/thermsio/atalaya/compare/v8.0.1...v8.0.2) (2023-05-02)

### Bug Fixes

- chore update deps ([918a9e6](http://bitbucket.org/thermsio/atalaya/commits/918a9e65a6d37337440259af70b435fb34792bfb))

## [8.0.1](http://bitbucket.org/thermsio/atalaya/compare/v8.0.0...v8.0.1) (2023-04-24)

### Bug Fixes

- **CORE-2956:** DateTimePicker show empty input value when timestamps get cleared; ([efe8d2a](http://bitbucket.org/thermsio/atalaya/commits/efe8d2af03648eb5ac2042aa9e450f93d01b9b36))

# [8.0.0](https://bitbucket.org/thermsio/atalaya/compare/v7.136.2...v8.0.0) (2023-04-21)

### Features

- update react v18 and all dependencies ([55d046c](https://bitbucket.org/thermsio/atalaya/commits/55d046cc44dfce746621706212e94b4037371fc7))

### BREAKING CHANGES

- React v18 and other dependeny updates can cause unexpected issues in first major release

## [7.136.2](https://bitbucket.org/thermsio/atalaya/compare/v7.136.1...v7.136.2) (2023-04-13)

### Bug Fixes

- **Image:** prevent flashing on mount/remount ([5e185b0](https://bitbucket.org/thermsio/atalaya/commits/5e185b0b51e85f6b59ccb3abbd0b5e6538e4b462))

## [7.136.1](http://bitbucket.org/thermsio/atalaya/compare/v7.136.0...v7.136.1) (2023-04-07)

### Bug Fixes

- **CORE-2932:** Image.lightbox prop not working; ([3d1e62f](http://bitbucket.org/thermsio/atalaya/commits/3d1e62f667a2ba7ff338a6f423d8e482cc34ad7c))

# [7.136.0](http://bitbucket.org/thermsio/atalaya/compare/v7.135.0...v7.136.0) (2023-04-04)

### Features

- **CORE-2922:** Added export for RichTextEditorDisplayContent ([4d4f464](http://bitbucket.org/thermsio/atalaya/commits/4d4f464974e7f83aa21fc51e86c349143f2f2a92))
- **CORE-2922:** Removed [@tailwind](http://bitbucket.org/tailwind) [@base](http://bitbucket.org/base) layer in all.css ([2977a22](http://bitbucket.org/thermsio/atalaya/commits/2977a223268cc68f1d609d102531c6a1f60ad850))
- **RichTextEditorDisplayContent:** add new component for displaying html and avoiding css reset from tailwind ([ef7e67e](http://bitbucket.org/thermsio/atalaya/commits/ef7e67eada42a7262c94bdf016cb556ad3bba6bf))

# [7.135.0](http://bitbucket.org/thermsio/atalaya/compare/v7.134.0...v7.135.0) (2023-04-04)

### Features

- **CORE-2823:** Timeline only show connecting line when there are further events; ([d83cf7f](http://bitbucket.org/thermsio/atalaya/commits/d83cf7f49cb3b8c05acbc7c25f41f4c921e2d5a3))

# [7.134.0](http://bitbucket.org/thermsio/atalaya/compare/v7.133.0...v7.134.0) (2023-04-04)

### Features

- **CORE-2823:** Improve Timeline styling; ([ee36f26](http://bitbucket.org/thermsio/atalaya/commits/ee36f26129df10fc126e31d0ad8e057033f5249b))

# [7.133.0](http://bitbucket.org/thermsio/atalaya/compare/v7.132.1...v7.133.0) (2023-04-04)

### Features

- **CORE-2823:** Add TS generics to Timeline, adds code completion for callbacks; ([15b02aa](http://bitbucket.org/thermsio/atalaya/commits/15b02aa35e2e6ca2ab8076270ad0c06a335ce4d3))

## [7.132.1](http://bitbucket.org/thermsio/atalaya/compare/v7.132.0...v7.132.1) (2023-04-04)

### Bug Fixes

- lint ([82080e0](http://bitbucket.org/thermsio/atalaya/commits/82080e066370cc425c61dc2497d9994bb4b32404))

# [7.132.0](http://bitbucket.org/thermsio/atalaya/compare/v7.131.0...v7.132.0) (2023-04-04)

### Features

- **DateTimeRangePicker:** add prop 'onChangeValueBehavior' for controlling when onChangeValue prop is called ([ef42959](http://bitbucket.org/thermsio/atalaya/commits/ef429593b4a1e2dbc1d1064a4cb8b5292670df50))

# [7.131.0](http://bitbucket.org/thermsio/atalaya/compare/v7.130.0...v7.131.0) (2023-04-03)

### Features

- **CORE-2823:** add children to Button type; ([6c45602](http://bitbucket.org/thermsio/atalaya/commits/6c4560277da11176b00df94a480363dbd110549a))

# [7.130.0](http://bitbucket.org/thermsio/atalaya/compare/v7.129.0...v7.130.0) (2023-04-03)

### Features

- **CORE-2823:** add Medium text sizing; ([6c43fc2](http://bitbucket.org/thermsio/atalaya/commits/6c43fc243844a3d29cb6f5b04b24c59689d7247d))

# [7.129.0](http://bitbucket.org/thermsio/atalaya/compare/v7.128.0...v7.129.0) (2023-03-30)

### Features

- **CORE-2922:** Added Tailwind base layer to revert ol & ul display in all.css ([1927af5](http://bitbucket.org/thermsio/atalaya/commits/1927af50ba8d6f8f1e90805200c6e087b0c03f60))

# [7.128.0](http://bitbucket.org/thermsio/atalaya/compare/v7.127.0...v7.128.0) (2023-03-29)

### Features

- update typescript to v5 and other minor deps ([5df9c92](http://bitbucket.org/thermsio/atalaya/commits/5df9c92faf0340212d08218a393dd86d1c1d6bc1))

# [7.127.0](https://bitbucket.org/thermsio/atalaya/compare/v7.126.0...v7.127.0) (2023-03-21)

### Features

- **MultiSelect:** fix internal logic issues allow loadMissingOptions prop to return Promise with Option[] ([9b32542](https://bitbucket.org/thermsio/atalaya/commits/9b32542b5640235834ccba220e2f767cdf0e400d))

# [7.126.0](https://bitbucket.org/thermsio/atalaya/compare/v7.125.2...v7.126.0) (2023-03-21)

### Features

- **ButtonWithModal:** add loadingMessage prop ([32b71a4](https://bitbucket.org/thermsio/atalaya/commits/32b71a4771b9aab61106bb8187ad3718c1699537))

## [7.125.2](https://bitbucket.org/thermsio/atalaya/compare/v7.125.1...v7.125.2) (2023-03-15)

### Bug Fixes

- resize hook throttle ([59e070e](https://bitbucket.org/thermsio/atalaya/commits/59e070ea00a34fe307f82cdb1286e52544b61d0f))

## [7.125.1](https://bitbucket.org/thermsio/atalaya/compare/v7.125.0...v7.125.1) (2023-03-15)

### Bug Fixes

- wrap ResizeObserver to catch errors ([22da173](https://bitbucket.org/thermsio/atalaya/commits/22da173f029eb290f6a1390cc31563cf3d333634))

# [7.125.0](http://bitbucket.org/thermsio/atalaya/compare/v7.124.6...v7.125.0) (2023-03-02)

### Features

- **CORE-2821:** extend tailwind fontSize, textColor and ZIndex rather than replacing them; ([fff72a8](http://bitbucket.org/thermsio/atalaya/commits/fff72a83fb463c4b5bade876b6246fa689c9b497))

## [7.124.6](http://bitbucket.org/thermsio/atalaya/compare/v7.124.5...v7.124.6) (2023-03-02)

### Bug Fixes

- **ColorPicker:** value non-string won't throw error ([aacea42](http://bitbucket.org/thermsio/atalaya/commits/aacea420c00ef5203341c62a219faa9894442e2b))

## [7.124.5](http://bitbucket.org/thermsio/atalaya/compare/v7.124.4...v7.124.5) (2023-02-22)

### Bug Fixes

- **LightboxContextGroup:** LightboxContextGroup guarantee the order of opening lightbox onClick/open (CORE-2840) ([56802f3](http://bitbucket.org/thermsio/atalaya/commits/56802f381873c2705717813642d0cdfab3d1d8b1))

## [7.124.4](http://bitbucket.org/thermsio/atalaya/compare/v7.124.3...v7.124.4) (2023-02-22)

### Bug Fixes

- **LightboxContextGroup:** LightboxContextProvider will open a fileUrl when it doesn't exist in the files array (CORE-2840) ([e9949ae](http://bitbucket.org/thermsio/atalaya/commits/e9949ae876f8ab4dd76aaaefdc618ea809e6018d))

## [7.124.3](http://bitbucket.org/thermsio/atalaya/compare/v7.124.2...v7.124.3) (2023-02-13)

### Bug Fixes

- **CORE-2693:** simplify FormControlWrapper horizontal logic to be more consistent; ([5a575b3](http://bitbucket.org/thermsio/atalaya/commits/5a575b3bc6254e3aa282c6042fb347aa4db297a9))

## [7.124.2](http://bitbucket.org/thermsio/atalaya/compare/v7.124.1...v7.124.2) (2023-02-10)

### Bug Fixes

- **CORE-2693:** FormLayout.Section fix space not being properly passed; ([cdbfdec](http://bitbucket.org/thermsio/atalaya/commits/cdbfdec925f7bc5cc7014f7582ee0232b66c45cc))

## [7.124.1](https://bitbucket.org/thermsio/atalaya/compare/v7.124.0...v7.124.1) (2023-02-07)

### Bug Fixes

- **CORE-2805:** FormLayout.Section, fix `space` prop not working; ([65a187b](https://bitbucket.org/thermsio/atalaya/commits/65a187b9d8ea3a7a2efef8ea9a260854d1fae3a2))

# [7.124.0](https://bitbucket.org/thermsio/atalaya/compare/v7.123.0...v7.124.0) (2023-02-06)

### Features

- **CORE-2805:** Stepper, add onStepChange Promise support, center nodes, add className to stepper container; ([6c55327](https://bitbucket.org/thermsio/atalaya/commits/6c55327923ed835b5198b05ebbe14b24c3a25173))

# [7.123.0](http://bitbucket.org/thermsio/atalaya/compare/v7.122.0...v7.123.0) (2023-02-06)

### Features

- **CORE-2805:** export Stepper; ([7e5cf64](http://bitbucket.org/thermsio/atalaya/commits/7e5cf64aa04ec24e10b4ae14c4874525988cef73))

# [7.122.0](http://bitbucket.org/thermsio/atalaya/compare/v7.121.1...v7.122.0) (2023-02-03)

### Features

- **CORE-2805:** add Stepper; ([87878de](http://bitbucket.org/thermsio/atalaya/commits/87878de681940b6610ee9083a846657171fd59c8))

## [7.121.1](http://bitbucket.org/thermsio/atalaya/compare/v7.121.0...v7.121.1) (2023-01-29)

### Bug Fixes

- **Tag:** value prop type ReactNode ([b5a1ed0](http://bitbucket.org/thermsio/atalaya/commits/b5a1ed0574f4dac7a84de9ad6fbc61df23664a72))

# [7.121.0](http://bitbucket.org/thermsio/atalaya/compare/v7.120.0...v7.121.0) (2023-01-27)

### Features

- **CORE-2599:** ButtonConfirm tweak default confirmation message styling; ([f7d132b](http://bitbucket.org/thermsio/atalaya/commits/f7d132be7e3ae353c0ab2ddd3fb8158d7195aa78))

# [7.120.0](http://bitbucket.org/thermsio/atalaya/compare/v7.119.5...v7.120.0) (2023-01-26)

### Bug Fixes

- **CORE-2599:** fix import crash; ([261a014](http://bitbucket.org/thermsio/atalaya/commits/261a014d92805ec122c8bf33b8f7dfc51410e2e0))

### Features

- **CORE-2599:** Show ToggleSwitch controller at the end of the row when label is present; ([69425cb](http://bitbucket.org/thermsio/atalaya/commits/69425cb1409061925363d48fa29aabf26cdfedd7))

## [7.119.5](http://bitbucket.org/thermsio/atalaya/compare/v7.119.4...v7.119.5) (2023-01-20)

### Bug Fixes

- **CORE-2765:** Phone not fill in missing numbers with '\_'; ([7a19947](http://bitbucket.org/thermsio/atalaya/commits/7a19947424863d38f35eac77af45616a5fe29e79))

## [7.119.4](http://bitbucket.org/thermsio/atalaya/compare/v7.119.3...v7.119.4) (2023-01-11)

### Bug Fixes

- **CORE-2757:** fix MultiSelect crashing when checking out selected values; ([6bfcec4](http://bitbucket.org/thermsio/atalaya/commits/6bfcec4b1ce0cd44bb74acb1631e77fa45224b38))

## [7.119.3](http://bitbucket.org/thermsio/atalaya/compare/v7.119.2...v7.119.3) (2023-01-11)

### Bug Fixes

- **CORE-2744:** remove console.log; ([27e4ab7](http://bitbucket.org/thermsio/atalaya/commits/27e4ab7217da988d69454f6e56beaf4c3709be04))

## [7.119.2](http://bitbucket.org/thermsio/atalaya/compare/v7.119.1...v7.119.2) (2023-01-11)

### Bug Fixes

- **CORE-2744:** Select, onChangeOptions deleting options that were in value but no in options list; ([318662a](http://bitbucket.org/thermsio/atalaya/commits/318662a8b311db77666af12ffd0b207206b7825b))

## [7.119.1](https://bitbucket.org/thermsio/atalaya/compare/v7.119.0...v7.119.1) (2023-01-11)

### Bug Fixes

- DropdownContainer content width ([875c61f](https://bitbucket.org/thermsio/atalaya/commits/875c61f1aed59cac4f88e6e91b4b79d9507336d2))

# [7.119.0](http://bitbucket.org/thermsio/atalaya/compare/v7.118.2...v7.119.0) (2023-01-09)

### Features

- **CORE-2742:** allow actionModal to use an Element; ([3317c7f](http://bitbucket.org/thermsio/atalaya/commits/3317c7f557e435545f3a93a314452b49a4cff292))

## [7.118.2](http://bitbucket.org/thermsio/atalaya/compare/v7.118.1...v7.118.2) (2023-01-04)

### Bug Fixes

- **CORE-2732:** DateTimePicker fix status icons creating a new line ([bc39292](http://bitbucket.org/thermsio/atalaya/commits/bc3929201a364292b8f7f6eb581797a9d26d1cf1))

## [7.118.1](http://bitbucket.org/thermsio/atalaya/compare/v7.118.0...v7.118.1) (2023-01-03)

### Bug Fixes

- **CORE-2736:** DayPicker fix Monday being shown as the first day of the month if calendar was set to weekStart Monday and sunday was the first day of the month; ([37dfe1c](http://bitbucket.org/thermsio/atalaya/commits/37dfe1c191c33c7f253364b4812683ea1812da4d))

# [7.118.0](http://bitbucket.org/thermsio/atalaya/compare/v7.117.7...v7.118.0) (2022-12-30)

### Features

- add showErrorMessage to FormControls ([e814fb9](http://bitbucket.org/thermsio/atalaya/commits/e814fb9e16b6fe7f564593bce4248a85fa397514))

## [7.117.7](https://bitbucket.org/thermsio/atalaya/compare/v7.117.6...v7.117.7) (2022-12-27)

### Bug Fixes

- Modal spacing between header and body; ([abc4234](https://bitbucket.org/thermsio/atalaya/commits/abc42342e59aace6d5a4bf0ebb61ce870da75318))

## [7.117.6](http://bitbucket.org/thermsio/atalaya/compare/v7.117.5...v7.117.6) (2022-12-24)

### Bug Fixes

- **Select:** return early onCreateOption ([ceaa846](http://bitbucket.org/thermsio/atalaya/commits/ceaa846df4b25fe52f1248b8c3d5dbdc93d7badf))

## [7.117.5](http://bitbucket.org/thermsio/atalaya/compare/v7.117.4...v7.117.5) (2022-12-23)

### Bug Fixes

- **Select:** onCreateOption callback SelectOption when multi ([25891f3](http://bitbucket.org/thermsio/atalaya/commits/25891f3f246671d62965a1ce2e9249ad61d3047e))

## [7.117.4](https://bitbucket.org/thermsio/atalaya/compare/v7.117.3...v7.117.4) (2022-12-20)

### Bug Fixes

- ContainerDimensionsContext fix scrollbars altering dimensions; ([9859d2f](https://bitbucket.org/thermsio/atalaya/commits/9859d2fa8d0cf301c37e30299bc705ee79868386))

## [7.117.3](https://bitbucket.org/thermsio/atalaya/compare/v7.117.2...v7.117.3) (2022-12-20)

### Bug Fixes

- **Select:** onChangeOptions ([3413458](https://bitbucket.org/thermsio/atalaya/commits/341345871fb125ec66f7340ab84e50480772ca33))

## [7.117.2](https://bitbucket.org/thermsio/atalaya/compare/v7.117.1...v7.117.2) (2022-12-19)

### Bug Fixes

- **Select:** props generic default any ([4215ca6](https://bitbucket.org/thermsio/atalaya/commits/4215ca6cade1fe981bf8bd5bb268540d717f1111))
- **Select:** props generic default any ([31e2de0](https://bitbucket.org/thermsio/atalaya/commits/31e2de09a84e0651221bcca3fbb5a96d616da301))

## [7.117.2](https://bitbucket.org/thermsio/atalaya/compare/v7.117.1...v7.117.2) (2022-12-19)

### Bug Fixes

- **Select:** props generic default any ([31e2de0](https://bitbucket.org/thermsio/atalaya/commits/31e2de09a84e0651221bcca3fbb5a96d616da301))

## [7.117.1](https://bitbucket.org/thermsio/atalaya/compare/v7.117.0...v7.117.1) (2022-12-19)

### Bug Fixes

- **Select:** props ([ebb19ed](https://bitbucket.org/thermsio/atalaya/commits/ebb19edb56ad557676becbc20ed52aa2f89dbb37))

# [7.117.0](https://bitbucket.org/thermsio/atalaya/compare/v7.116.0...v7.117.0) (2022-12-19)

### Features

- **Select:** improve typings for options and callback props ([97858db](https://bitbucket.org/thermsio/atalaya/commits/97858db2e48049daa6a3d074baa2f818ab4bffb3))

# [7.116.0](http://bitbucket.org/thermsio/atalaya/compare/v7.115.2...v7.116.0) (2022-12-19)

### Features

- **Select:** add onChangeOption pass selectedOptions ([7ec9266](http://bitbucket.org/thermsio/atalaya/commits/7ec9266d4d6a2647ce0af36ba7b26bd0f8bd39d2))

## [7.115.2](https://bitbucket.org/thermsio/atalaya/compare/v7.115.1...v7.115.2) (2022-12-13)

### Bug Fixes

- **DateTimeRangePicker:** call onChangeValue when both start & end are set CORE-2668 ([7525d93](https://bitbucket.org/thermsio/atalaya/commits/7525d93ef660a17cdd588d7f133455de14323234))

## [7.115.1](https://bitbucket.org/thermsio/atalaya/compare/v7.115.0...v7.115.1) (2022-12-13)

### Bug Fixes

- **DateTimePicker:** show dropdown bottom-left CORE-2638 ([8bc961f](https://bitbucket.org/thermsio/atalaya/commits/8bc961faece5fd391e31135da8665465e92e7351))

# [7.115.0](http://bitbucket.org/thermsio/atalaya/compare/v7.114.0...v7.115.0) (2022-12-06)

### Features

- **CORE-2651:** loose coupling of DateTimePicker subcomponents; ([c7b9d53](http://bitbucket.org/thermsio/atalaya/commits/c7b9d5376fb80658551d0e8674e7810281d5938d))

# [7.114.0](http://bitbucket.org/thermsio/atalaya/compare/v7.113.5...v7.114.0) (2022-12-06)

### Features

- **CORE-2651:** update input background light color. ([ef67f30](http://bitbucket.org/thermsio/atalaya/commits/ef67f305f24777279daeb44af72e76a7be8b9919))

## [7.113.5](https://bitbucket.org/thermsio/atalaya/compare/v7.113.4...v7.113.5) (2022-12-05)

### Bug Fixes

- **TextWithSuggestions:** Suggestions generic extends Array<unknown>Array<unknown> ([44c139f](https://bitbucket.org/thermsio/atalaya/commits/44c139f7a11385e79e089836a9f74bec0e780cb2))

## [7.113.4](https://bitbucket.org/thermsio/atalaya/compare/v7.113.3...v7.113.4) (2022-12-05)

### Bug Fixes

- **TextWithSuggestions:** Suggest type label/value optional ([9eccb26](https://bitbucket.org/thermsio/atalaya/commits/9eccb26914d30d5ddaed231d24bd01963d4b3c4a))

## [7.113.3](https://bitbucket.org/thermsio/atalaya/compare/v7.113.2...v7.113.3) (2022-12-05)

### Bug Fixes

- **TextWithSuggestions:** suggestions TS generic for DX ([aa2e106](https://bitbucket.org/thermsio/atalaya/commits/aa2e10649fb2893009cc94cc7eaeb226bc13952d))

## [7.113.2](http://bitbucket.org/thermsio/atalaya/compare/v7.113.1...v7.113.2) (2022-11-30)

### Bug Fixes

- **CORE-2651:** Lightbox not supporting file type if extension was uppercase. ([037c6a0](http://bitbucket.org/thermsio/atalaya/commits/037c6a027cbec00236810ef3ae8a9e496269c0c5))

## [7.113.1](https://bitbucket.org/thermsio/atalaya/compare/v7.113.0...v7.113.1) (2022-11-25)

### Bug Fixes

- exports mapping ([4a4fc60](https://bitbucket.org/thermsio/atalaya/commits/4a4fc606888731a364b73db3864719b31ead5918))

# [7.113.0](https://bitbucket.org/thermsio/atalaya/compare/v7.112.0...v7.113.0) (2022-11-25)

### Features

- add exports to package.json for better project auto-import support ([d7efa68](https://bitbucket.org/thermsio/atalaya/commits/d7efa688458b8c46a2e48dd86a94b7d235df1d24))

# [7.112.0](http://bitbucket.org/thermsio/atalaya/compare/v7.111.5...v7.112.0) (2022-11-25)

### Features

- **CORE-2530:** add inset to DropdownContainer; ([8735a6c](http://bitbucket.org/thermsio/atalaya/commits/8735a6c7e1035a774dfefb033cec9e3c12645b23))

## [7.111.5](http://bitbucket.org/thermsio/atalaya/compare/v7.111.4...v7.111.5) (2022-11-24)

### Bug Fixes

- **CORE-2609:** Select, show created options on input. ([e3963b7](http://bitbucket.org/thermsio/atalaya/commits/e3963b7637dc71866a9a6b4b0a77ff1560d685c0))

## [7.111.4](https://bitbucket.org/thermsio/atalaya/compare/v7.111.3...v7.111.4) (2022-11-24)

### Bug Fixes

- **Image:** lazye=false default ([671cff6](https://bitbucket.org/thermsio/atalaya/commits/671cff6fcb36d36ddd6983c135812077a99e3461))

## [7.111.3](https://bitbucket.org/thermsio/atalaya/compare/v7.111.2...v7.111.3) (2022-11-19)

### Bug Fixes

- variant typings ([d3d4eb1](https://bitbucket.org/thermsio/atalaya/commits/d3d4eb190535dce2e2f61e462339e8ba129b6ec4))

## [7.111.2](http://bitbucket.org/thermsio/atalaya/compare/v7.111.1...v7.111.2) (2022-11-14)

### Bug Fixes

- **ListCreator:** element key ([fe1c017](http://bitbucket.org/thermsio/atalaya/commits/fe1c0170de646d6c7d4791d677b38e38735ac0ac))

## [7.111.1](http://bitbucket.org/thermsio/atalaya/compare/v7.111.0...v7.111.1) (2022-11-14)

### Reverts

- Revert "feat(CORE-2561): update initialEditing if prop was initially undefined and changed" ([30367e2](http://bitbucket.org/thermsio/atalaya/commits/30367e218c3c54e5341a683f5ce9d73c0a469173))

# [7.111.0](http://bitbucket.org/thermsio/atalaya/compare/v7.110.5...v7.111.0) (2022-11-14)

### Features

- **CORE-2561:** update initialEditing if prop was initially undefined and changed ([c014e2e](http://bitbucket.org/thermsio/atalaya/commits/c014e2e6b12437197b9c9c9aaf710390166bac1a))

## [7.110.5](http://bitbucket.org/thermsio/atalaya/compare/v7.110.4...v7.110.5) (2022-11-12)

### Bug Fixes

- **CORE-2561:** fix tooltip not flipping. ([4347e71](http://bitbucket.org/thermsio/atalaya/commits/4347e71217eb68642154e20a63cff1fdb1eac7a4))

## [7.110.4](http://bitbucket.org/thermsio/atalaya/compare/v7.110.3...v7.110.4) (2022-11-12)

### Bug Fixes

- **ButtonWithActionModal:** use children instead of actionText ([b0b3a61](http://bitbucket.org/thermsio/atalaya/commits/b0b3a61c9ebc56a4ed8b04e9f56731621a71e3da))

## [7.110.3](http://bitbucket.org/thermsio/atalaya/compare/v7.110.2...v7.110.3) (2022-11-11)

### Bug Fixes

- add support for fullscreen mode in browsers ([2d35f8e](http://bitbucket.org/thermsio/atalaya/commits/2d35f8ee1b8d7236654e2ec3baa080655255e6eb))

## [7.110.2](http://bitbucket.org/thermsio/atalaya/compare/v7.110.1...v7.110.2) (2022-11-11)

### Bug Fixes

- **DateTime:** add mode to useMemo deps ([5734bfe](http://bitbucket.org/thermsio/atalaya/commits/5734bfe849e28e6e97eaa8a9b65d14dde7da4430))

## [7.110.1](https://bitbucket.org/thermsio/atalaya/compare/v7.110.0...v7.110.1) (2022-11-10)

### Bug Fixes

- **ListCreator:** regex error ([276b4d1](https://bitbucket.org/thermsio/atalaya/commits/276b4d1c464bb4d2260102a57bb029d2c3ae4f49))

# [7.110.0](http://bitbucket.org/thermsio/atalaya/compare/v7.109.0...v7.110.0) (2022-11-10)

### Bug Fixes

- useContainerDimensionsContext allow optional context ([5249022](http://bitbucket.org/thermsio/atalaya/commits/524902239f926338f1aa3be1aaf3b1932ffae7e7))
- **useContainerDimensionsContext:** optional ctx check ([85a4ad2](http://bitbucket.org/thermsio/atalaya/commits/85a4ad2eae8b7af4f8c21c6dec9df821061ca11f))

### Features

- **useIsBreakpointActive:** respects ContainerDimensionsContextProvider viewport value if ctx is found ([e9dc96f](http://bitbucket.org/thermsio/atalaya/commits/e9dc96fc2f263c0a0f1449e80f8424ece703e816))

# [7.109.0](http://bitbucket.org/thermsio/atalaya/compare/v7.108.0...v7.109.0) (2022-11-10)

### Features

- **CORE-2561:** FormControlDisplayEdit ([438c0d1](http://bitbucket.org/thermsio/atalaya/commits/438c0d1a6bcb5576a905dfb832ba66ec53fefeec))

# [7.108.0](http://bitbucket.org/thermsio/atalaya/compare/v7.107.8...v7.108.0) (2022-11-10)

### Features

- **CORE-2561:** Add FormControlDisplayEdit, ReadOnlyFormControl; ([eee7e32](http://bitbucket.org/thermsio/atalaya/commits/eee7e3213a80e1cb287f050e8e1c700050729879))
- **CORE-2561:** Add FormControlWrapper props to ReadOnlyFormControl ([a99fc7a](http://bitbucket.org/thermsio/atalaya/commits/a99fc7a788713d8b25ed5ce729a7ee82a1d0d4d0))

## [7.107.8](https://bitbucket.org/thermsio/atalaya/compare/v7.107.7...v7.107.8) (2022-11-07)

### Bug Fixes

- remove require line in comment due to breaking turbo_modules ([22171d1](https://bitbucket.org/thermsio/atalaya/commits/22171d1884ffa97cc6babec2a4e489fdbaf78601))

## [7.107.7](http://bitbucket.org/thermsio/atalaya/compare/v7.107.6...v7.107.7) (2022-11-05)

### Bug Fixes

- **CORE-2546:** stop dropdown and tooltip from going outside screen bounds ; ([c89d0ab](http://bitbucket.org/thermsio/atalaya/commits/c89d0ab5993d23d4b8b6e2181441cc7efa61454a))

## [7.107.6](https://bitbucket.org/thermsio/atalaya/compare/v7.107.5...v7.107.6) (2022-11-04)

### Bug Fixes

- **Radio:** boolean Radio options types ([7981069](https://bitbucket.org/thermsio/atalaya/commits/798106988c893fc3fba1fc44a3b95f324db4ffdc))

## [7.107.5](https://bitbucket.org/thermsio/atalaya/compare/v7.107.4...v7.107.5) (2022-11-04)

### Bug Fixes

- **Radio:** allow boolean Radio options ([83cc8aa](https://bitbucket.org/thermsio/atalaya/commits/83cc8aabda47e1e5beaaa84c80c13d93db57fe0f))

## [7.107.4](http://bitbucket.org/thermsio/atalaya/compare/v7.107.3...v7.107.4) (2022-11-01)

### Bug Fixes

- docs ([47f0070](http://bitbucket.org/thermsio/atalaya/commits/47f007070817a4efbbf3b06a33d036f7e7034968))

## [7.107.3](http://bitbucket.org/thermsio/atalaya/compare/v7.107.2...v7.107.3) (2022-11-01)

### Bug Fixes

- docs ([6ded92f](http://bitbucket.org/thermsio/atalaya/commits/6ded92f663a5b3c4deb1009f7a3e01d302b7a7dc))

## [7.107.2](http://bitbucket.org/thermsio/atalaya/compare/v7.107.1...v7.107.2) (2022-11-01)

### Bug Fixes

- docs ([e3d42a7](http://bitbucket.org/thermsio/atalaya/commits/e3d42a7d4420a4d2172be853893290160b080ccb))

## [7.107.1](http://bitbucket.org/thermsio/atalaya/compare/v7.107.0...v7.107.1) (2022-11-01)

### Bug Fixes

- update MultiSelect docs example ([d000a90](http://bitbucket.org/thermsio/atalaya/commits/d000a90dfec6a092ce1b10bde07df6526620dea7))

# [7.107.0](https://bitbucket.org/thermsio/atalaya/compare/v7.106.5...v7.107.0) (2022-11-01)

### Features

- **MultiSelect:** allow user to click load more options when no maxHeight ([17c71e1](https://bitbucket.org/thermsio/atalaya/commits/17c71e1a33399541b754ac417ee42b9ea3766a94))

## [7.106.5](http://bitbucket.org/thermsio/atalaya/compare/v7.106.4...v7.106.5) (2022-10-31)

### Bug Fixes

- **CORE-2524:** Lightbox, prevent videos from overflowing container; ([8e315e0](http://bitbucket.org/thermsio/atalaya/commits/8e315e0a17e7067d9a86a09c33ce92781c4cad17))

## [7.106.4](http://bitbucket.org/thermsio/atalaya/compare/v7.106.3...v7.106.4) (2022-10-28)

### Bug Fixes

- SelectOption type export moved up ([9b17c93](http://bitbucket.org/thermsio/atalaya/commits/9b17c932da41c076662d9ca16ddd2e5e2dc03497))

## [7.106.3](http://bitbucket.org/thermsio/atalaya/compare/v7.106.2...v7.106.3) (2022-10-28)

### Bug Fixes

- SelectOption type ([899a047](http://bitbucket.org/thermsio/atalaya/commits/899a04730323b40a6627efbb64d99724ada4512c))

## [7.106.2](http://bitbucket.org/thermsio/atalaya/compare/v7.106.1...v7.106.2) (2022-10-28)

### Bug Fixes

- Option type ([7475a80](http://bitbucket.org/thermsio/atalaya/commits/7475a80775eb90ec61fbb7332a6cd4a1479207e2))

## [7.106.1](http://bitbucket.org/thermsio/atalaya/compare/v7.106.0...v7.106.1) (2022-10-27)

### Bug Fixes

- **CORE-2219:** Modal, not extending to fill width if content didn't stretch it; ([23c2a5a](http://bitbucket.org/thermsio/atalaya/commits/23c2a5a387fcea67b1cf1dbee4c1acbb42177905))

# [7.106.0](http://bitbucket.org/thermsio/atalaya/compare/v7.105.1...v7.106.0) (2022-10-27)

### Features

- **CORE-2219:** Modal, add ability to add custom ; ([1004bed](http://bitbucket.org/thermsio/atalaya/commits/1004bed8e2da91669a8fdf6a78e95cdf7d773c92))

## [7.105.1](http://bitbucket.org/thermsio/atalaya/compare/v7.105.0...v7.105.1) (2022-10-27)

### Bug Fixes

- **DateTimePicker:** initial onChangeValue 0 sec set ([633cbe9](http://bitbucket.org/thermsio/atalaya/commits/633cbe9fce0a36be06ccabfe9630e83fb3909f8c))

# [7.105.0](https://bitbucket.org/thermsio/atalaya/compare/v7.104.0...v7.105.0) (2022-10-27)

### Features

- **MultiSelect:** add generic types ([5b61e93](https://bitbucket.org/thermsio/atalaya/commits/5b61e9374b370819dc6be66327f553337a2339d9))

# [7.104.0](http://bitbucket.org/thermsio/atalaya/compare/v7.103.2...v7.104.0) (2022-10-26)

### Features

- Added renderOption prop to MultiSelect OptionsList ([c701475](http://bitbucket.org/thermsio/atalaya/commits/c701475d4431ecafc057a362807c252e830b3124))

## [7.103.2](https://bitbucket.org/thermsio/atalaya/compare/v7.103.1...v7.103.2) (2022-10-26)

### Bug Fixes

- **FormControlsWrapper:** render string error ([a80562d](https://bitbucket.org/thermsio/atalaya/commits/a80562ddfea1c48c12754408a3777a4184212b5f))

## [7.103.1](https://bitbucket.org/thermsio/atalaya/compare/v7.103.0...v7.103.1) (2022-10-24)

### Bug Fixes

- **MultiSelect:** text subText ([ce6378e](https://bitbucket.org/thermsio/atalaya/commits/ce6378ee097a49b100e9b2202ad37d9a3f9a4ca9))

# [7.103.0](http://bitbucket.org/thermsio/atalaya/compare/v7.102.4...v7.103.0) (2022-10-24)

### Features

- **CORE-2219:** Modal, make header row sticky; ([72b70ce](http://bitbucket.org/thermsio/atalaya/commits/72b70ceb519d4486174816f1ec20ddf679256e24))

## [7.102.4](http://bitbucket.org/thermsio/atalaya/compare/v7.102.3...v7.102.4) (2022-10-24)

### Bug Fixes

- **MultiSelect:** text search fill space ([4751307](http://bitbucket.org/thermsio/atalaya/commits/47513078225da548ae9a036ea248174bac6669b8))

## [7.102.3](https://bitbucket.org/thermsio/atalaya/compare/v7.102.2...v7.102.3) (2022-10-24)

### Bug Fixes

- **MultiSelect:** loading position, do not call loadOptions on mount, show selected count in tab ([a3a7824](https://bitbucket.org/thermsio/atalaya/commits/a3a782498c4b197dad82227fb5212de8ffd70900))

## [7.102.2](https://bitbucket.org/thermsio/atalaya/compare/v7.102.1...v7.102.2) (2022-10-21)

### Bug Fixes

- **Modal:** header & internalHeader bug when header prop changes ([8bc3552](https://bitbucket.org/thermsio/atalaya/commits/8bc3552a06b85ba85d03124883c28120cbcd7026))

## [7.102.1](http://bitbucket.org/thermsio/atalaya/compare/v7.102.0...v7.102.1) (2022-10-21)

### Bug Fixes

- **MultiSelect:** loading selected options and tab state ([d02edbe](http://bitbucket.org/thermsio/atalaya/commits/d02edbe55bfc81655f2461299e8734512153f42a))

# [7.102.0](http://bitbucket.org/thermsio/atalaya/compare/v7.101.0...v7.102.0) (2022-10-21)

### Features

- **MultiSelect:** new features and functionality added, technically a breaking change due to some removed props but risking this as a feature release ([3ac62e3](http://bitbucket.org/thermsio/atalaya/commits/3ac62e359999bdec6b13e00a392161a733cad14a))

# [7.101.0](http://bitbucket.org/thermsio/atalaya/compare/v7.100.0...v7.101.0) (2022-10-20)

### Features

- **CORE-2219:** update subtle active button text color... again; ([93c9589](http://bitbucket.org/thermsio/atalaya/commits/93c9589f1ee42bbb5301b809ae02be1818288309))

# [7.100.0](http://bitbucket.org/thermsio/atalaya/compare/v7.99.1...v7.100.0) (2022-10-20)

### Features

- **CORE-2219:** update subtle active button text color; ([7cf6167](http://bitbucket.org/thermsio/atalaya/commits/7cf6167730bad0afd41718127344fea8f6126fa2))

## [7.99.1](http://bitbucket.org/thermsio/atalaya/compare/v7.99.0...v7.99.1) (2022-10-20)

### Bug Fixes

- **MultiSelect:** add totalOptions prop ([e9585a9](http://bitbucket.org/thermsio/atalaya/commits/e9585a9f2ff80fb980ac1b9f843e4a9394714b25))
- **MultiSelect:** scrolling jump in options list ([226884c](http://bitbucket.org/thermsio/atalaya/commits/226884c3dfab037b9a076a865de0e204e3949877))

# [7.99.0](http://bitbucket.org/thermsio/atalaya/compare/v7.98.0...v7.99.0) (2022-10-19)

### Features

- **Pagination:** Forgot space between total & record/records ([30a33c7](http://bitbucket.org/thermsio/atalaya/commits/30a33c73d680f2967645994cb85385c44ca21b40))

# [7.98.0](http://bitbucket.org/thermsio/atalaya/compare/v7.97.0...v7.98.0) (2022-10-19)

### Features

- **Pagination:** Added check for displaying record/records with total ([f955407](http://bitbucket.org/thermsio/atalaya/commits/f9554077070945e9a3ebc4e8d45512f523971004))

# [7.97.0](http://bitbucket.org/thermsio/atalaya/compare/v7.96.0...v7.97.0) (2022-10-19)

### Features

- **CORE-2219:** Pagination, add className to container; ([e46afa0](http://bitbucket.org/thermsio/atalaya/commits/e46afa07100e51cb02a6d9a8f2ec63e92b01f755))

# [7.96.0](http://bitbucket.org/thermsio/atalaya/compare/v7.95.1...v7.96.0) (2022-10-13)

### Features

- **CORE-2535:** add viewport to ContainerDimensions and make the ref optional; ([9e421e6](http://bitbucket.org/thermsio/atalaya/commits/9e421e6b4572428a8e9002914f8447189ef67f9b))

## [7.95.1](http://bitbucket.org/thermsio/atalaya/compare/v7.95.0...v7.95.1) (2022-10-12)

### Bug Fixes

- **CORE-2535:** fix crash when using ContainerDimensions, ([01be48e](http://bitbucket.org/thermsio/atalaya/commits/01be48ef8da740ce0d4ba57777f13de8d15e6101))

# [7.95.0](http://bitbucket.org/thermsio/atalaya/compare/v7.94.0...v7.95.0) (2022-10-12)

### Features

- **CORE-2535:** add ContainerDimensions context/hook, ([0bfc548](http://bitbucket.org/thermsio/atalaya/commits/0bfc548a9f665313e44117718f79dcfdcabe7c52))

# [7.94.0](http://bitbucket.org/thermsio/atalaya/compare/v7.93.0...v7.94.0) (2022-10-11)

### Features

- **ButtonWithModal:** add ButtonWithModal components ([7204ce3](http://bitbucket.org/thermsio/atalaya/commits/7204ce32f1b4834091d1793d678650c29c74ec2d))

# [7.93.0](http://bitbucket.org/thermsio/atalaya/compare/v7.92.0...v7.93.0) (2022-10-11)

### Features

- **Pagination:** only show total records when total is <= records per page ([825b51c](http://bitbucket.org/thermsio/atalaya/commits/825b51c5194ef03eecc85e242a9594c864d345e5))

# [7.92.0](http://bitbucket.org/thermsio/atalaya/compare/v7.91.0...v7.92.0) (2022-10-10)

### Features

- **CORE-2219:** HorizontalScroll, add itemWidth, ([c40b0a8](http://bitbucket.org/thermsio/atalaya/commits/c40b0a801abc6cacea4a76eaa6bd6355128b1fcd))

# [7.91.0](http://bitbucket.org/thermsio/atalaya/compare/v7.90.0...v7.91.0) (2022-10-05)

### Features

- **CORE-2219:** export HorizontalScroll, ([71677f8](http://bitbucket.org/thermsio/atalaya/commits/71677f84e36c0405c65312244bfdbc0e1e821a76))

# [7.90.0](http://bitbucket.org/thermsio/atalaya/compare/v7.89.1...v7.90.0) (2022-10-04)

### Features

- **CORE-2219:** add HorizontalScroll ([ceb3951](http://bitbucket.org/thermsio/atalaya/commits/ceb39511afcdc301b96d580e7f36324e53de2597))
- **CORE-2219:** rename gap to space ([3d98578](http://bitbucket.org/thermsio/atalaya/commits/3d985780be0746317f9b66cceef102cdc5bbf9fb))

## [7.89.1](https://bitbucket.org/thermsio/atalaya/compare/v7.89.0...v7.89.1) (2022-10-02)

### Bug Fixes

- **DateTime:** use and apply timezone ([52f977f](https://bitbucket.org/thermsio/atalaya/commits/52f977f28ea5c201ca4679ba403086502bdf5dff))

# [7.89.0](http://bitbucket.org/thermsio/atalaya/compare/v7.88.1...v7.89.0) (2022-09-27)

### Features

- **CORE-2375:** getFileTypeFromUrl explicitly make CORS request; ([7410cb1](http://bitbucket.org/thermsio/atalaya/commits/7410cb1ecfcbd61e3a0462841d57c9a50167f788))
- **CORE-2375:** LightboxFileTypeByExtension, add jfif support; ([c5de202](http://bitbucket.org/thermsio/atalaya/commits/c5de2021ce7dfeaa671e2cffab97f091a1a7b742))
- **CORE-2375:** refactor LightboxContext, add support for nested contexts; ([58b3eff](http://bitbucket.org/thermsio/atalaya/commits/58b3effbdc3dd0ad893c3a65d3b3e0dd2163920f))

## [7.88.1](https://bitbucket.org/thermsio/atalaya/compare/v7.88.0...v7.88.1) (2022-09-27)

### Bug Fixes

- **DateTimePicker:** date/time mode fix regex mode ([b506331](https://bitbucket.org/thermsio/atalaya/commits/b5063317ff4f1b4b4060b6a237daf67ea54afa13))

# [7.88.0](http://bitbucket.org/thermsio/atalaya/compare/v7.87.0...v7.88.0) (2022-09-26)

### Features

- **CORE-2375:** Add lightboxSrc prop to Avatar props; ([d12f26f](http://bitbucket.org/thermsio/atalaya/commits/d12f26fc2388402cdb0a113ced12dcb37c5720a0))

# [7.87.0](http://bitbucket.org/thermsio/atalaya/compare/v7.86.1...v7.87.0) (2022-09-26)

### Features

- **CORE-2375:** Add lightbox prop to Avatar props; ([a5d96b7](http://bitbucket.org/thermsio/atalaya/commits/a5d96b7deda7e24ee551a90d56c8aaff5324d5b7))

## [7.86.1](http://bitbucket.org/thermsio/atalaya/compare/v7.86.0...v7.86.1) (2022-09-23)

### Reverts

- Revert "feat(CORE-2375): Image, open lightbox if lightboxSrc is present but no lightbox;" ([40d522f](http://bitbucket.org/thermsio/atalaya/commits/40d522fad3d6a63941217d21ca5b5ce003683f37))

# [7.86.0](http://bitbucket.org/thermsio/atalaya/compare/v7.85.0...v7.86.0) (2022-09-23)

### Features

- **CORE-2375:** Image, do not add click related styles if image is not clickable; ([1590bc9](http://bitbucket.org/thermsio/atalaya/commits/1590bc9298de0b499336e49cd74c922457b1bd88))

# [7.85.0](http://bitbucket.org/thermsio/atalaya/compare/v7.84.0...v7.85.0) (2022-09-23)

### Features

- **CORE-2375:** Image, open lightbox if lightboxSrc is present but no lightbox; ([8c0aec9](http://bitbucket.org/thermsio/atalaya/commits/8c0aec951419c0bab66e6fbd2163fc069d43f8c4))

# [7.84.0](http://bitbucket.org/thermsio/atalaya/compare/v7.83.0...v7.84.0) (2022-09-23)

### Features

- **CORE-2375:** Image, make lightbox [secure] by default; ([d5c9db9](http://bitbucket.org/thermsio/atalaya/commits/d5c9db983bee86e0c38a69aa9e0a62915087f239))

# [7.83.0](http://bitbucket.org/thermsio/atalaya/compare/v7.82.1...v7.83.0) (2022-09-22)

### Features

- **CORE-2375:** remove console.logs. ([a03224e](http://bitbucket.org/thermsio/atalaya/commits/a03224e9e13cc7d7790536c3afe00f9c2cc4fd55))

## [7.82.1](http://bitbucket.org/thermsio/atalaya/compare/v7.82.0...v7.82.1) (2022-09-22)

### Bug Fixes

- **CORE-2375:** LightboxContext, FileType fallback failing and useReducer. ([ee56042](http://bitbucket.org/thermsio/atalaya/commits/ee56042e792cc757d844dd3f515dfeea6378de33))

# [7.82.0](http://bitbucket.org/thermsio/atalaya/compare/v7.81.0...v7.82.0) (2022-09-22)

### Features

- **CORE-2375:** lightboxContext only fetch new urls. ([9afffab](http://bitbucket.org/thermsio/atalaya/commits/9afffab37dc97c02b08a11cd2ccb4b117ad6ee8c))

# [7.81.0](http://bitbucket.org/thermsio/atalaya/compare/v7.80.1...v7.81.0) (2022-09-21)

### Features

- **CORE-2375:** imageLightbox add loading indicator for images; ([227d5e9](http://bitbucket.org/thermsio/atalaya/commits/227d5e9197517b47129e0df57681cb7fa573a08d))

## [7.80.1](http://bitbucket.org/thermsio/atalaya/compare/v7.80.0...v7.80.1) (2022-09-20)

### Bug Fixes

- **CORE-2375:** crash due to file being undefined; ([bf34fac](http://bitbucket.org/thermsio/atalaya/commits/bf34fac087851933476a80a7419cb33d2d429fe6))

# [7.80.0](http://bitbucket.org/thermsio/atalaya/compare/v7.79.0...v7.80.0) (2022-09-20)

### Features

- **CORE-2375:** add lightbox src; ([19d9a79](http://bitbucket.org/thermsio/atalaya/commits/19d9a79d217838ba56a9927d4da96438acdb6771))

# [7.79.0](http://bitbucket.org/thermsio/atalaya/compare/v7.78.0...v7.79.0) (2022-09-20)

### Features

- **CORE-2375:** revamp lightbox; ([14e0e89](http://bitbucket.org/thermsio/atalaya/commits/14e0e89a6b76340674c7b7c5d86beff2da1bb46c))
- **CORE-2375:** revamp lightbox; ([cc3c701](http://bitbucket.org/thermsio/atalaya/commits/cc3c70195496495cf678bdfb6e0bc9f2731f7683))

# [7.78.0](http://bitbucket.org/thermsio/atalaya/compare/v7.77.5...v7.78.0) (2022-09-19)

### Features

- **CORE-2453:** Refactored Number to handle undefined value from onChangeInteger ([72533a7](http://bitbucket.org/thermsio/atalaya/commits/72533a71f63fab88f6ab52112a7b5d0dd5956ec3))

## [7.77.5](http://bitbucket.org/thermsio/atalaya/compare/v7.77.4...v7.77.5) (2022-09-16)

### Bug Fixes

- Fixed the previous onChangeInteger change because someone forgot something ([61ee567](http://bitbucket.org/thermsio/atalaya/commits/61ee567337bd90f0564960d6c8cdcc237af149b0))

## [7.77.4](https://bitbucket.org/thermsio/atalaya/compare/v7.77.3...v7.77.4) (2022-09-16)

### Bug Fixes

- **CORE-2510:** fix date picker in range mode ([e0cedb6](https://bitbucket.org/thermsio/atalaya/commits/e0cedb63b240216fb2453e4d70a858c1a9f28713))

## [7.77.3](https://bitbucket.org/thermsio/atalaya/compare/v7.77.2...v7.77.3) (2022-09-10)

### Bug Fixes

- force version bump ([685b61b](https://bitbucket.org/thermsio/atalaya/commits/685b61b5180508a2691f1f7e52673b8257cdc43d))

## [7.77.2](https://bitbucket.org/thermsio/atalaya/compare/v7.77.1...v7.77.2) (2022-09-10)

### Bug Fixes

- ghots event listeners in useClickOutside hook ([e224354](https://bitbucket.org/thermsio/atalaya/commits/e22435425bc66c9bca59f8f27d340cb21be0b89c))

## [7.77.1](https://bitbucket.org/thermsio/atalaya/compare/v7.77.0...v7.77.1) (2022-09-10)

### Bug Fixes

- **Image:** wrap Skeleton loading in div for flex parents ([1c4978e](https://bitbucket.org/thermsio/atalaya/commits/1c4978eceb91106cf9cd9b1da35a793b9e95102f))

# [7.77.0](https://bitbucket.org/thermsio/atalaya/compare/v7.76.0...v7.77.0) (2022-09-10)

### Bug Fixes

- hour manual key input empty value ([f263db6](https://bitbucket.org/thermsio/atalaya/commits/f263db6dd5c61382dc51696da9eacc7947452569))

### Features

- **DateTimePicker:** add ability to pick date or time individually, refactor logic for hour/min & positioning ([a2d5aff](https://bitbucket.org/thermsio/atalaya/commits/a2d5aff43369805cbf646ff3d0fdbb4ee2d30095))
- use DateTimeDisplayContext default is24 in DateTimePickerContext ([f28251a](https://bitbucket.org/thermsio/atalaya/commits/f28251aad2d47381de4f69fc99d99d9ae5f007d2))

# [7.76.0](http://bitbucket.org/thermsio/atalaya/compare/v7.75.0...v7.76.0) (2022-09-09)

### Features

- refactor DateTimePickerContext to use valtio for state management ([358fd94](http://bitbucket.org/thermsio/atalaya/commits/358fd94b302048fb5191c120d2767a45474a22e6))

# [7.75.0](http://bitbucket.org/thermsio/atalaya/compare/v7.74.1...v7.75.0) (2022-09-09)

### Features

- ActionModal change texts type to ReactNode ([b37d415](http://bitbucket.org/thermsio/atalaya/commits/b37d41546ced77d28775c04cb46a56b0f5e0e3dd))
- ActionModal change texts type to ReactNode ([488383f](http://bitbucket.org/thermsio/atalaya/commits/488383fa0c5b2d9ec42507588b2ed18b5649ffd5))

## [7.74.1](https://bitbucket.org/thermsio/atalaya/compare/v7.74.0...v7.74.1) (2022-09-09)

### Bug Fixes

- **ActionModal:** allow ReactNode for actionText ([ca36fbf](https://bitbucket.org/thermsio/atalaya/commits/ca36fbff4e437c239b5d0eb30810a45e02341787))

# [7.74.0](http://bitbucket.org/thermsio/atalaya/compare/v7.73.0...v7.74.0) (2022-09-07)

### Features

- Pagination, hide perPagePicker if totalRecords are less than the minimum amount that can be displayed. ([b3d063f](http://bitbucket.org/thermsio/atalaya/commits/b3d063fc485d7dd480037f5715eebd5b35214298))

# [7.73.0](http://bitbucket.org/thermsio/atalaya/compare/v7.72.2...v7.73.0) (2022-09-07)

### Features

- **Pagination:** hide page limit picker when total is less than 10 ([cd408ea](http://bitbucket.org/thermsio/atalaya/commits/cd408ea850bc1fa5c549a46963b2656c966c94f5))

## [7.72.2](http://bitbucket.org/thermsio/atalaya/compare/v7.72.1...v7.72.2) (2022-09-07)

### Bug Fixes

- FormLayout, fix change from default dividers. ([04e6864](http://bitbucket.org/thermsio/atalaya/commits/04e6864610ce13426b5541227f7bc6919ebe7aee))

## [7.72.1](https://bitbucket.org/thermsio/atalaya/compare/v7.72.0...v7.72.1) (2022-09-06)

### Bug Fixes

- **TableGroup:** initialState for context create new object ([b0a9ab7](https://bitbucket.org/thermsio/atalaya/commits/b0a9ab7d5b6daf2b0a5bd48d2a23b2d7d88448a4))

# [7.72.0](http://bitbucket.org/thermsio/atalaya/compare/v7.71.0...v7.72.0) (2022-09-06)

### Bug Fixes

- **CORE-2412:** stop outSideClicks from overlays from triggering on multiple elements. ([d2a49fc](http://bitbucket.org/thermsio/atalaya/commits/d2a49fc070bbebe25103a8798ec78792660cb109))

### Features

- **CORE-2412:** refactor Overlays to use onClick instead of onClick outside. ([721614d](http://bitbucket.org/thermsio/atalaya/commits/721614d7a37106300d6a570228c0dd39c80a21b7))

# [7.71.0](http://bitbucket.org/thermsio/atalaya/compare/v7.70.10...v7.71.0) (2022-09-06)

### Features

- **CORE-2449:** Dropdown fix dropdown position on Controlled Mode; ([af03837](http://bitbucket.org/thermsio/atalaya/commits/af03837b2b05e305147d762b1e72749a8736b82f))
- **CORE-2449:** DropdownContainer now implement react-popper; ([62ca1f5](http://bitbucket.org/thermsio/atalaya/commits/62ca1f506bbbecc88d2b4477df069f655cb778e0))
- **CORE-2457:** Add closeOnClick prop to ContextMenu; ([99d95d3](http://bitbucket.org/thermsio/atalaya/commits/99d95d308fa76941cafbd2009611f93a3f17467e))

## [7.70.10](http://bitbucket.org/thermsio/atalaya/compare/v7.70.9...v7.70.10) (2022-09-03)

### Bug Fixes

- circular Image prop and hidden class on <img /> ([1b1287c](http://bitbucket.org/thermsio/atalaya/commits/1b1287c19f2db17a197bf609c3ccfc717c39b6c8))

## [7.70.9](https://bitbucket.org/thermsio/atalaya/compare/v7.70.8...v7.70.9) (2022-09-03)

### Bug Fixes

- **Image:** retry logic ([b31e0bf](https://bitbucket.org/thermsio/atalaya/commits/b31e0bfa353979e250c29fa6d8875f57535854a3))

## [7.70.8](https://bitbucket.org/thermsio/atalaya/compare/v7.70.7...v7.70.8) (2022-09-03)

### Bug Fixes

- **Modal:** context setHeader ([4e86be0](https://bitbucket.org/thermsio/atalaya/commits/4e86be09bd38385df8b8503aaf56ba2dce5b62d4))

## [7.70.7](https://bitbucket.org/thermsio/atalaya/compare/v7.70.6...v7.70.7) (2022-09-02)

### Bug Fixes

- Dropdown position ([b0d7f27](https://bitbucket.org/thermsio/atalaya/commits/b0d7f2750d95ca7a7d397d7f7e54d9799252fece))

## [7.70.6](https://bitbucket.org/thermsio/atalaya/compare/v7.70.5...v7.70.6) (2022-09-02)

### Bug Fixes

- force bump version ([ebbcf0f](https://bitbucket.org/thermsio/atalaya/commits/ebbcf0f6790b3d585acbda56b24b6d5e8929e27f))

## [7.70.5](https://bitbucket.org/thermsio/atalaya/compare/v7.70.4...v7.70.5) (2022-09-02)

### Bug Fixes

- **Modal:** portal logic prevent duplicate divs being added ([8623f35](https://bitbucket.org/thermsio/atalaya/commits/8623f35b5caeb105469c0573956fd3640224c20a))

## [7.70.4](https://bitbucket.org/thermsio/atalaya/compare/v7.70.3...v7.70.4) (2022-09-01)

### Bug Fixes

- **Dropdown:** reset font in dropdown to avoid parent inherit ([08cdb03](https://bitbucket.org/thermsio/atalaya/commits/08cdb039625ee3164524cac71c2d4c39cb295265))

## [7.70.3](https://bitbucket.org/thermsio/atalaya/compare/v7.70.2...v7.70.3) (2022-09-01)

### Bug Fixes

- **Modal:** portal instance element mount modal portal ([e113d7a](https://bitbucket.org/thermsio/atalaya/commits/e113d7a5022eeb723594c059f45fc00c724b8a82))

## [7.70.2](http://bitbucket.org/thermsio/atalaya/compare/v7.70.1...v7.70.2) (2022-08-30)

### Bug Fixes

- **Icon:** prevent re-render unless url changes ([2c116d8](http://bitbucket.org/thermsio/atalaya/commits/2c116d8a3db44c8fadd3c2f6d2c95bd5397c8ddc))

## [7.70.1](https://bitbucket.org/thermsio/atalaya/compare/v7.70.0...v7.70.1) (2022-08-30)

### Bug Fixes

- stupid fucking Yup validation error passing an object to fields ([502ee53](https://bitbucket.org/thermsio/atalaya/commits/502ee53f15979b562e2a8acf7b869190215d2736))

# [7.70.0](http://bitbucket.org/thermsio/atalaya/compare/v7.69.1...v7.70.0) (2022-08-29)

### Features

- **CORE-2399:** Refactored autoComplete for input in Text to handle Chrome not supporting 'off' ([307cb0b](http://bitbucket.org/thermsio/atalaya/commits/307cb0b6cc6a78e269b6ce6fa34fa53b6536080c))

## [7.69.1](https://bitbucket.org/thermsio/atalaya/compare/v7.69.0...v7.69.1) (2022-08-29)

### Bug Fixes

- **CORE-2400:** add padding to modal; ([a2d8357](https://bitbucket.org/thermsio/atalaya/commits/a2d8357bed610a105dbcc52f048a77bd78488f9a))

# [7.69.0](https://bitbucket.org/thermsio/atalaya/compare/v7.68.1...v7.69.0) (2022-08-29)

### Features

- **CORE-2400:** add relative class to modal body div ([a7b514c](https://bitbucket.org/thermsio/atalaya/commits/a7b514ce09b6862ad0a0ad322ce2e3cdc700bc1a))

## [7.68.1](http://bitbucket.org/thermsio/atalaya/compare/v7.68.0...v7.68.1) (2022-08-29)

### Bug Fixes

- modal header not updating if Modal.header prop changed. ([ab06d80](http://bitbucket.org/thermsio/atalaya/commits/ab06d80c5c28acd6ce0a6cca71a11f5087cc610d))

# [7.68.0](http://bitbucket.org/thermsio/atalaya/compare/v7.67.1...v7.68.0) (2022-08-25)

### Features

- add FormElementLayoutWrapper; ([b3457d8](http://bitbucket.org/thermsio/atalaya/commits/b3457d825b0810ce62f79f091aa717979c50b01e))

## [7.67.1](http://bitbucket.org/thermsio/atalaya/compare/v7.67.0...v7.67.1) (2022-08-25)

### Bug Fixes

- **CORE-2423:** DatePicker, Update timestamps when value changes ; ([98093de](http://bitbucket.org/thermsio/atalaya/commits/98093de519660cce6b83559b48a055435b0b9b34))

# [7.67.0](http://bitbucket.org/thermsio/atalaya/compare/v7.66.1...v7.67.0) (2022-08-24)

### Features

- FormControls keep Cancel button enabled if form is disabled; ([ef8f065](http://bitbucket.org/thermsio/atalaya/commits/ef8f065b2627f4ccbdbc86d2b27d64faffb2db5b))

## [7.66.1](http://bitbucket.org/thermsio/atalaya/compare/v7.66.0...v7.66.1) (2022-08-22)

### Bug Fixes

- **CORE-2378:** ButtonConfirm, make confirmationMessage prop optional; ([f1e23cd](http://bitbucket.org/thermsio/atalaya/commits/f1e23cdc0c5d854e36c578df41876675ceed8b5c))

# [7.66.0](http://bitbucket.org/thermsio/atalaya/compare/v7.65.5...v7.66.0) (2022-08-22)

### Features

- **CORE-2378:** add ConfirmButton ([d460bd6](http://bitbucket.org/thermsio/atalaya/commits/d460bd65169358541afd7f0bcc04854b14509830))

## [7.65.5](https://bitbucket.org/thermsio/atalaya/compare/v7.65.4...v7.65.5) (2022-08-22)

### Bug Fixes

- declaration output by adding DropdownChecklistType ([be50a72](https://bitbucket.org/thermsio/atalaya/commits/be50a72db548aaf662e2ca42d946dad543ac0c5f))

## [7.65.4](https://bitbucket.org/thermsio/atalaya/compare/v7.65.3...v7.65.4) (2022-08-22)

### Bug Fixes

- FormControlCommonProps generic types error ([9ad9749](https://bitbucket.org/thermsio/atalaya/commits/9ad9749fde6441c4bd14ff0e1d1139e4cbf48424))

## [7.65.3](https://bitbucket.org/thermsio/atalaya/compare/v7.65.2...v7.65.3) (2022-08-21)

### Bug Fixes

- package.json types path ([4718e3f](https://bitbucket.org/thermsio/atalaya/commits/4718e3f175adf795d53953b422ee1fbe72bb2498))

## [7.65.2](https://bitbucket.org/thermsio/atalaya/compare/v7.65.1...v7.65.2) (2022-08-21)

### Bug Fixes

- package.json exports field removed ([c2bc6ee](https://bitbucket.org/thermsio/atalaya/commits/c2bc6eeaedcdb4689b169f41679c9ea91aa8ed19))

## [7.65.1](https://bitbucket.org/thermsio/atalaya/compare/v7.65.0...v7.65.1) (2022-08-21)

### Bug Fixes

- rollup builds sourcemaps and bundle esm ([aba11a7](https://bitbucket.org/thermsio/atalaya/commits/aba11a705b29ff1f3956333665d90acf6494effe))

# [7.65.0](http://bitbucket.org/thermsio/atalaya/compare/v7.64.6...v7.65.0) (2022-08-20)

### Features

- add DateTimeDisplayContext ([22e96f7](http://bitbucket.org/thermsio/atalaya/commits/22e96f70dd90cb94204c04f23d2ccfcb273f4f60))

## [7.64.6](http://bitbucket.org/thermsio/atalaya/compare/v7.64.5...v7.64.6) (2022-08-19)

### Bug Fixes

- **CORE-2378:** DateTimeRangePicker, make value prop optional; ([71d2486](http://bitbucket.org/thermsio/atalaya/commits/71d2486f8b23c7006405b208a55f1eccfb08ee3d))

## [7.64.5](https://bitbucket.org/thermsio/atalaya/compare/v7.64.4...v7.64.5) (2022-08-18)

### Bug Fixes

- **DateTimePicker:** controlled component udpate timestamp & mode when props change ([950ad2e](https://bitbucket.org/thermsio/atalaya/commits/950ad2e53740c8de34b59b766d1d77f9a6ff2320))

## [7.64.4](http://bitbucket.org/thermsio/atalaya/compare/v7.64.3...v7.64.4) (2022-08-17)

### Bug Fixes

- **DateTimePicker:** initial timestamp on mount ([6c29a5e](http://bitbucket.org/thermsio/atalaya/commits/6c29a5ec2872cab84a5cce3e96e9050a1c7f9c6b))

## [7.64.3](http://bitbucket.org/thermsio/atalaya/compare/v7.64.2...v7.64.3) (2022-08-17)

### Bug Fixes

- fix regression DateTimePicker when due to changing the mode name from datetime to dateTime; ([3f5bc6a](http://bitbucket.org/thermsio/atalaya/commits/3f5bc6a30ea8ba971d8eef35c02c10c7181f82a7))
- **Modal:** modal portal instances used inside portal container ([004985b](http://bitbucket.org/thermsio/atalaya/commits/004985b146e39e0f5e9af59190efc0e6da6d6390))

## [7.64.2](http://bitbucket.org/thermsio/atalaya/compare/v7.64.1...v7.64.2) (2022-08-17)

### Bug Fixes

- **ToggleSwitch:** disabled prop passed to input element ([7c96860](http://bitbucket.org/thermsio/atalaya/commits/7c968608b14bc3d9cbf9d8133b1b452df39759f5))

## [7.64.1](http://bitbucket.org/thermsio/atalaya/compare/v7.64.0...v7.64.1) (2022-08-17)

### Bug Fixes

- clear time breaking time set; ([3cf10c7](http://bitbucket.org/thermsio/atalaya/commits/3cf10c760a429bdae3724e773ce97d5ec90bbc3b))

# [7.64.0](https://bitbucket.org/thermsio/atalaya/compare/v7.63.3...v7.64.0) (2022-08-16)

### Features

- **DropdownChecklist.Item:** add onClickCheckbox ([00e4ec5](https://bitbucket.org/thermsio/atalaya/commits/00e4ec56f54af64e6292a4beaa6142ce065c3034))

## [7.63.3](http://bitbucket.org/thermsio/atalaya/compare/v7.63.2...v7.63.3) (2022-08-16)

### Bug Fixes

- null check ([07a7fb8](http://bitbucket.org/thermsio/atalaya/commits/07a7fb851d698e11ca6bab44d3301e44c634b58f))

## [7.63.2](http://bitbucket.org/thermsio/atalaya/compare/v7.63.1...v7.63.2) (2022-08-16)

### Bug Fixes

- **Icon:** force-cache option for fetch ([c66c191](http://bitbucket.org/thermsio/atalaya/commits/c66c191dff0ca50b1be19de4c1b429c6357f7822))

## [7.63.1](http://bitbucket.org/thermsio/atalaya/compare/v7.63.0...v7.63.1) (2022-08-16)

### Bug Fixes

- dayjs moved to peerDeps ([2c8a721](http://bitbucket.org/thermsio/atalaya/commits/2c8a72134662b272fa1e0ac982afa8271100977c))

# [7.63.0](http://bitbucket.org/thermsio/atalaya/compare/v7.62.1...v7.63.0) (2022-08-16)

### Features

- add clear button to DateTimeRangePicker; ([7dfdee9](http://bitbucket.org/thermsio/atalaya/commits/7dfdee9a572582118853160fc3961055c9467d57))

## [7.62.1](http://bitbucket.org/thermsio/atalaya/compare/v7.62.0...v7.62.1) (2022-08-15)

### Bug Fixes

- fix Tabbed dropdown not having background; ([65f17c6](http://bitbucket.org/thermsio/atalaya/commits/65f17c61fd2172a278dd4ea6633a1bfe6303c6ef))

# [7.62.0](http://bitbucket.org/thermsio/atalaya/compare/v7.61.0...v7.62.0) (2022-08-12)

### Bug Fixes

- fix Async Select docs value not sticking; ([7b04750](http://bitbucket.org/thermsio/atalaya/commits/7b047508fa611911c272eb1597676b517f9a8d22))

### Features

- **CORE-2378:** add Tooltip, add tooltip prop to Tag; ([b81e9c6](http://bitbucket.org/thermsio/atalaya/commits/b81e9c6905d82873250eaf895765986e557e0eb0))
- remove modal header divider; ([ce3afeb](http://bitbucket.org/thermsio/atalaya/commits/ce3afeb77676e711918951a807cb52f3f7455740))

# [7.61.0](http://bitbucket.org/thermsio/atalaya/compare/v7.60.2...v7.61.0) (2022-08-11)

### Features

- **CORE-2252:** Moved DropdownChecklist from web-app-ops. Update deps ([b3b6bf5](http://bitbucket.org/thermsio/atalaya/commits/b3b6bf55471c0bc652cdd070c1465686f5c3b7f9))
- **DropdownChecklist:** Add new component ([a692215](http://bitbucket.org/thermsio/atalaya/commits/a692215ff045bf4cab128e960bb178024d15c293))

## [7.60.2](http://bitbucket.org/thermsio/atalaya/compare/v7.60.1...v7.60.2) (2022-08-11)

### Bug Fixes

- DatePicker regressions; ([f10391c](http://bitbucket.org/thermsio/atalaya/commits/f10391c1537b66c74645222faf79bd4c83f1b8d6))

## [7.60.1](http://bitbucket.org/thermsio/atalaya/compare/v7.60.0...v7.60.1) (2022-08-10)

### Bug Fixes

- reduce spacing between ButtonGroup Outline buttons ([07e72c3](http://bitbucket.org/thermsio/atalaya/commits/07e72c3ea466befc0a9fd4ea95ceb0ceff858a86))

# [7.60.0](http://bitbucket.org/thermsio/atalaya/compare/v7.59.0...v7.60.0) (2022-08-10)

### Features

- **CORE-2363:** add DateTimePickerDocs ([1d8494c](http://bitbucket.org/thermsio/atalaya/commits/1d8494c87423cac8c0ac84d8d0243b4a84303ff5))
- **CORE-2363:** implement DateTimeRangePicker ([d783035](http://bitbucket.org/thermsio/atalaya/commits/d783035cbdd32abc6a8357acd3635357739f4553))

# [7.59.0](http://bitbucket.org/thermsio/atalaya/compare/v7.58.0...v7.59.0) (2022-08-02)

### Bug Fixes

- **CORE-2278:** fix select dropdown being trapped inside modal; ([0e43141](http://bitbucket.org/thermsio/atalaya/commits/0e4314128598ef43f175445f41a768ffd1b14537))

### Features

- **CORE-2278:** Dropdowns now change position if they go out of bounds of viewport; ([07c0689](http://bitbucket.org/thermsio/atalaya/commits/07c068939571108ffb83079f2be22d1107323d68))
- **CORE-2278:** make react select work properly on modals; ([0fafbdc](http://bitbucket.org/thermsio/atalaya/commits/0fafbdcc1a07b470c1f3b4f752279ff60ddecd98))
- **CORE-2278:** Stop Select's dropdown from being trapped inside modal WIP; ([63b1d30](http://bitbucket.org/thermsio/atalaya/commits/63b1d302847ddc1ad687d1ed33feca4318b029f4))

# [7.58.0](http://bitbucket.org/thermsio/atalaya/compare/v7.57.0...v7.58.0) (2022-07-29)

### Features

- **CORE-2218:** make button x padding the same as y; ([76c6c0c](http://bitbucket.org/thermsio/atalaya/commits/76c6c0c650ec92adfd7bc615f9d5dbb19a3e739c))

# [7.57.0](http://bitbucket.org/thermsio/atalaya/compare/v7.56.1...v7.57.0) (2022-07-27)

### Features

- **CORE-2316:** Modals implement portals; ([7760f53](http://bitbucket.org/thermsio/atalaya/commits/7760f53163dc1bbcbf48dab3ca7867d5493a20b5))

## [7.56.1](http://bitbucket.org/thermsio/atalaya/compare/v7.56.0...v7.56.1) (2022-07-26)

### Bug Fixes

- **CORE-2316:** Fix modals inside SlidePanel being off centered; ([a4775c6](http://bitbucket.org/thermsio/atalaya/commits/a4775c6fdebd035beee1e432b2853f1521a14462))

# [7.56.0](http://bitbucket.org/thermsio/atalaya/compare/v7.55.0...v7.56.0) (2022-07-25)

### Bug Fixes

- **CORE-2315:** Fix selecting tables not being accurate; ([52a67f4](http://bitbucket.org/thermsio/atalaya/commits/52a67f40434492f717c8dfcb617c4aac60b30c83))

### Features

- **CORE-2315:** make selected count be accurate WIP; ([9ae57c9](http://bitbucket.org/thermsio/atalaya/commits/9ae57c9587b0a72c1cf399b4880d67ee11d49025))

# [7.55.0](http://bitbucket.org/thermsio/atalaya/compare/v7.54.1...v7.55.0) (2022-07-22)

### Features

- **CORE-2263:** Icon, do not log abortSignal errors; ([147804b](http://bitbucket.org/thermsio/atalaya/commits/147804b9a3e597c2728383383b15e50040027d29))

## [7.54.1](https://bitbucket.org/thermsio/atalaya/compare/v7.54.0...v7.54.1) (2022-07-15)

### Bug Fixes

- update react-lazy-load ([2cf6baa](https://bitbucket.org/thermsio/atalaya/commits/2cf6baabd783af8a312c744435c197c6b3521882))

# [7.54.0](https://bitbucket.org/thermsio/atalaya/compare/v7.53.0...v7.54.0) (2022-07-14)

### Features

- add toastManager.dismiss func ([4bb1ca9](https://bitbucket.org/thermsio/atalaya/commits/4bb1ca9857c3dbf8841c4747613df1d66c95f910))

# [7.53.0](http://bitbucket.org/thermsio/atalaya/compare/v7.52.1...v7.53.0) (2022-07-14)

### Features

- **CORE-2218:** make disabled fields more noticeable; ([5a7d539](http://bitbucket.org/thermsio/atalaya/commits/5a7d53950c33e7efa57fb87e4cf81e2209da9bdd))

## [7.52.1](http://bitbucket.org/thermsio/atalaya/compare/v7.52.0...v7.52.1) (2022-07-12)

### Bug Fixes

- **Modal:** closeHandler does not call w/ synthetic event ([0bd701e](http://bitbucket.org/thermsio/atalaya/commits/0bd701eacbb0e4c32f194b211be44275e2e55f80))

# [7.52.0](http://bitbucket.org/thermsio/atalaya/compare/v7.51.1...v7.52.0) (2022-07-12)

### Features

- update deps ([4052dd5](http://bitbucket.org/thermsio/atalaya/commits/4052dd5b755502f2caed3babba22100f472b07ec))

## [7.51.1](http://bitbucket.org/thermsio/atalaya/compare/v7.51.0...v7.51.1) (2022-07-08)

### Bug Fixes

- Make menu stay open on Select multi by default, add closeOnSelect prop; ([74e2da5](http://bitbucket.org/thermsio/atalaya/commits/74e2da51b5d9a9d3fb183bf3878d5807169ed0f6))

# [7.51.0](http://bitbucket.org/thermsio/atalaya/compare/v7.50.0...v7.51.0) (2022-07-08)

### Features

- update deps ([89192fa](http://bitbucket.org/thermsio/atalaya/commits/89192fa333516f492894752a76bc3a00c682e481))

# [7.50.0](http://bitbucket.org/thermsio/atalaya/compare/v7.49.1...v7.50.0) (2022-07-04)

### Features

- **CORE-1676:** add subText to radio option; ([89854e8](http://bitbucket.org/thermsio/atalaya/commits/89854e8053ae546fcaf2c33e2f3e2afa260b0c95))

## [7.49.1](https://bitbucket.org/thermsio/atalaya/compare/v7.49.0...v7.49.1) (2022-06-24)

### Bug Fixes

- Toast TS React.ReactNode for labels ([0ad6e6b](https://bitbucket.org/thermsio/atalaya/commits/0ad6e6b1ff14af7cb9edf2451aa06dfbb9701421))

# [7.49.0](http://bitbucket.org/thermsio/atalaya/compare/v7.48.0...v7.49.0) (2022-06-23)

### Features

- add exclusive colors for form input backgrounds; ([e0c5c2d](http://bitbucket.org/thermsio/atalaya/commits/e0c5c2d325dcb8453825446be69599798089eb64))

# [7.48.0](http://bitbucket.org/thermsio/atalaya/compare/v7.47.5...v7.48.0) (2022-06-21)

### Features

- **CORE-2077:** BottomMenu, add BottomMenuItem, tweak BottomMenu stylings; ([38ebd9b](http://bitbucket.org/thermsio/atalaya/commits/38ebd9b6163d0d5bea01bbab1ee02a44d37540c8))

## [7.47.5](http://bitbucket.org/thermsio/atalaya/compare/v7.47.4...v7.47.5) (2022-06-20)

### Bug Fixes

- FormSectionChildren check if child is null or [secure] ([75988ac](http://bitbucket.org/thermsio/atalaya/commits/75988ac03296b5bf7605a5c1a8fcc7ccfb1211d1))

## [7.47.4](http://bitbucket.org/thermsio/atalaya/compare/v7.47.3...v7.47.4) (2022-06-20)

### Bug Fixes

- Icon not removing icon when url becomes undefined after loading; ([c910226](http://bitbucket.org/thermsio/atalaya/commits/c91022640d89728908f011bae7bc4d55313745ba))
- ToggleSwitch not showing background color; ([57e478d](http://bitbucket.org/thermsio/atalaya/commits/57e478d3e1f2fbacb400438fd623ddb25b9ebaaf))

## [7.47.3](http://bitbucket.org/thermsio/atalaya/compare/v7.47.2...v7.47.3) (2022-06-17)

### Bug Fixes

- fix "Andy"'s fuck up with isReactFragment ([ebc381b](http://bitbucket.org/thermsio/atalaya/commits/ebc381b4233b591a9dc5af0c30756e9e8818a6c2))

## [7.47.2](https://bitbucket.org/thermsio/atalaya/compare/v7.47.1...v7.47.2) (2022-06-17)

### Bug Fixes

- cory's fuckup on fragment check ([f36c68e](https://bitbucket.org/thermsio/atalaya/commits/f36c68e583277d54cf7ab4e2822e29be6be55449))

## [7.47.1](https://bitbucket.org/thermsio/atalaya/compare/v7.47.0...v7.47.1) (2022-06-17)

### Bug Fixes

- isReactFragment() logic check ([7e0f752](https://bitbucket.org/thermsio/atalaya/commits/7e0f75297cf089351f181bf5b622894613b68928))

# [7.47.0](https://bitbucket.org/thermsio/atalaya/compare/v7.46.0...v7.47.0) (2022-06-17)

### Features

- fix Tom's fuckup with FormLayout.Section ([18d2661](https://bitbucket.org/thermsio/atalaya/commits/18d266179d614b56e6672e744e0f82aa9f4e231f))

# [7.46.0](https://bitbucket.org/thermsio/atalaya/compare/v7.45.4...v7.46.0) (2022-06-17)

### Bug Fixes

- **Select:** menuOverlay issue with light styles when using menuOpen prop ([221374f](https://bitbucket.org/thermsio/atalaya/commits/221374f0f176d2b2882f37729668ce5330d52589))

### Features

- update deps ([05e359f](https://bitbucket.org/thermsio/atalaya/commits/05e359fa40150293a5900a5f96552cd70794637c))

## [7.45.4](http://bitbucket.org/thermsio/atalaya/compare/v7.45.3...v7.45.4) (2022-06-17)

### Bug Fixes

- **CORE-2119:** FormLayout, fix layout switch not working; ([31ea8e4](http://bitbucket.org/thermsio/atalaya/commits/31ea8e462c8f59ca2d76f64455151da4b2d9044e))

## [7.45.3](http://bitbucket.org/thermsio/atalaya/compare/v7.45.2...v7.45.3) (2022-06-15)

### Bug Fixes

- **FormLayout.Controls:** confirm delete call ([9212b64](http://bitbucket.org/thermsio/atalaya/commits/9212b6415ac8b21656810762de09d783a524357a))

## [7.45.2](http://bitbucket.org/thermsio/atalaya/compare/v7.45.1...v7.45.2) (2022-06-14)

### Bug Fixes

- **CORE-2119:** remove console.log; ([fbca21c](http://bitbucket.org/thermsio/atalaya/commits/fbca21c4b56bca3fa11008820bd8de286bfe71a4))

## [7.45.1](http://bitbucket.org/thermsio/atalaya/compare/v7.45.0...v7.45.1) (2022-06-14)

### Bug Fixes

- **CORE-2119:** fix inconsistent stripes; ([5533619](http://bitbucket.org/thermsio/atalaya/commits/55336195989233f522eb49a80492f9c976783383))

# [7.45.0](http://bitbucket.org/thermsio/atalaya/compare/v7.44.2...v7.45.0) (2022-06-14)

### Features

- **CORE-2119:** add DataDisplay; ([2674466](http://bitbucket.org/thermsio/atalaya/commits/26744661cfa6c29c629f18c0845320fa00decc1a))

## [7.44.2](http://bitbucket.org/thermsio/atalaya/compare/v7.44.1...v7.44.2) (2022-06-10)

### Bug Fixes

- DropDown on mobile going behind MobileNav and Modals; ([3ebb7bf](http://bitbucket.org/thermsio/atalaya/commits/3ebb7bfda0e9b80e414e71d438dc41c2b7222d45))

## [7.44.1](http://bitbucket.org/thermsio/atalaya/compare/v7.44.0...v7.44.1) (2022-06-10)

### Bug Fixes

- remove gray buttons on scrollbar for Windows; ([607a3dc](http://bitbucket.org/thermsio/atalaya/commits/607a3dcb3a6486239dcc34e9c4f34c72273733c3))

# [7.44.0](http://bitbucket.org/thermsio/atalaya/compare/v7.43.2...v7.44.0) (2022-06-10)

### Bug Fixes

- AvatarGroup display error; ([64f915b](http://bitbucket.org/thermsio/atalaya/commits/64f915b160e005056cdb0eef26dd35eb4d3e6f4b))

### Features

- add custom scrollbar styling; ([2713261](http://bitbucket.org/thermsio/atalaya/commits/2713261dd80fa9f935092cd26287f90b04ca3c52))

## [7.43.2](http://bitbucket.org/thermsio/atalaya/compare/v7.43.1...v7.43.2) (2022-06-09)

### Bug Fixes

- Changed ActionModal body prop type to React.ReactNode ([6b58245](http://bitbucket.org/thermsio/atalaya/commits/6b58245579caa6b5f7a2f4d9ddca567178b83411))

## [7.43.1](http://bitbucket.org/thermsio/atalaya/compare/v7.43.0...v7.43.1) (2022-06-08)

### Bug Fixes

- update deps ([b448cc7](http://bitbucket.org/thermsio/atalaya/commits/b448cc7d14cf635abf7559b4aae3b1204e7c42dd))

# [7.43.0](http://bitbucket.org/thermsio/atalaya/compare/v7.42.0...v7.43.0) (2022-06-08)

### Features

- **CORE-2185:** Card, add specific styling for onClick prop ([260fdf8](http://bitbucket.org/thermsio/atalaya/commits/260fdf87c4d11f1d323108cd2f99c03c5ffc413d))

# [7.42.0](http://bitbucket.org/thermsio/atalaya/compare/v7.41.0...v7.42.0) (2022-06-07)

### Features

- **CORE-1993:** Added z-dropdown to TextWithSuggestions suggestion display ([efe1d78](http://bitbucket.org/thermsio/atalaya/commits/efe1d783bc56d025948cdaced03832d2261ff3c7))

# [7.41.0](http://bitbucket.org/thermsio/atalaya/compare/v7.40.0...v7.41.0) (2022-06-07)

### Features

- **CORE-1993:** Added z-dropdown to TextWithSuggestions suggestion display ([346f373](http://bitbucket.org/thermsio/atalaya/commits/346f373045ef225f11a2b369c01695b337c879b5))

# [7.40.0](http://bitbucket.org/thermsio/atalaya/compare/v7.39.2...v7.40.0) (2022-06-07)

### Features

- **CORE-2185:** ColorCircle, ColorSquare add size="sm"; ([47cf984](http://bitbucket.org/thermsio/atalaya/commits/47cf984d9c579bd60a6edf6dd2db3cc3ba3ce685))

## [7.39.2](http://bitbucket.org/thermsio/atalaya/compare/v7.39.1...v7.39.2) (2022-06-06)

### Bug Fixes

- **CORE-2184:** Table overflowing; ([e40ca09](http://bitbucket.org/thermsio/atalaya/commits/e40ca09d439fc31acbe1afcaa2395fafa809cd10))

## [7.39.1](http://bitbucket.org/thermsio/atalaya/compare/v7.39.0...v7.39.1) (2022-06-06)

### Bug Fixes

- **CORE-2184:** visibleSize not working on initial load; ([dc79522](http://bitbucket.org/thermsio/atalaya/commits/dc79522d33a2af8c5bd3530b7d025a15e4396c97))

# [7.39.0](https://bitbucket.org/thermsio/atalaya/compare/v7.38.0...v7.39.0) (2022-06-03)

### Bug Fixes

- FormSection subTitle TS type to React.ReactNode ([bbc49a1](https://bitbucket.org/thermsio/atalaya/commits/bbc49a1ecaefdea2c47aeb11db06a6d7991f4ac9))

### Features

- update deps ([8e4281e](https://bitbucket.org/thermsio/atalaya/commits/8e4281e9cb33374a9676f565230e37880510640a))

# [7.38.0](http://bitbucket.org/thermsio/atalaya/compare/v7.37.0...v7.38.0) (2022-06-03)

### Features

- **CORE-2184:** make hidden columns work properly; ([a2d8a96](http://bitbucket.org/thermsio/atalaya/commits/a2d8a96a63429c7ddb8bc3148196696c55b7d31f))

# [7.37.0](http://bitbucket.org/thermsio/atalaya/compare/v7.36.0...v7.37.0) (2022-06-01)

### Features

- **CORE-2184:** publish new version; ([eb0c06e](http://bitbucket.org/thermsio/atalaya/commits/eb0c06ea7f238ce7ad0546110c896d8fbf7bc059))

# [7.36.0](http://bitbucket.org/thermsio/atalaya/compare/v7.35.1...v7.36.0) (2022-06-01)

### Features

- **CORE-2184:** ButtonGroup, now works with nested buttons; ([59627c3](http://bitbucket.org/thermsio/atalaya/commits/59627c3ed9c000774016927fed432896695b94b4))

## [7.35.1](https://bitbucket.org/thermsio/atalaya/compare/v7.35.0...v7.35.1) (2022-05-29)

### Bug Fixes

- **Modal:** full width on small screens ([c0b81c3](https://bitbucket.org/thermsio/atalaya/commits/c0b81c3a0130092a04590bb9a7f08ed750a83a0e))

# [7.35.0](http://bitbucket.org/thermsio/atalaya/compare/v7.34.1...v7.35.0) (2022-05-27)

### Features

- **CORE-2184:** BottomMenu add Background choice; ([8d056c3](http://bitbucket.org/thermsio/atalaya/commits/8d056c39ebc52e410bf084dae353ed340f9234ce))

## [7.34.1](http://bitbucket.org/thermsio/atalaya/compare/v7.34.0...v7.34.1) (2022-05-27)

### Bug Fixes

- **CORE-2184:** BottomMenu classes; ([fc7c36c](http://bitbucket.org/thermsio/atalaya/commits/fc7c36cb8b45d7c5ac2a66798fd77304b9fc1f19))

# [7.34.0](http://bitbucket.org/thermsio/atalaya/compare/v7.33.0...v7.34.0) (2022-05-27)

### Features

- **CORE-2184:** add BottomMenu ([c522aed](http://bitbucket.org/thermsio/atalaya/commits/c522aed4f91fac413760b9d99db611ba16251ed8))

# [7.33.0](http://bitbucket.org/thermsio/atalaya/compare/v7.32.2...v7.33.0) (2022-05-24)

### Features

- trigger publish; ([3aad32a](http://bitbucket.org/thermsio/atalaya/commits/3aad32a4051f744f304fcdc55cc0e151d502baf2))

## [7.32.2](https://bitbucket.org/thermsio/atalaya/compare/v7.32.1...v7.32.2) (2022-05-24)

### Bug Fixes

- **CORE-2184:** Inline, fix wrap spacing not working on alignX="between". ([04f4f10](https://bitbucket.org/thermsio/atalaya/commits/04f4f107fe2bb0ae59b9e4244a148b9291a9b6b2))

## [7.32.1](http://bitbucket.org/thermsio/atalaya/compare/v7.32.0...v7.32.1) (2022-05-23)

### Bug Fixes

- **CORE-2112:** fix className typo. ([29a1af1](http://bitbucket.org/thermsio/atalaya/commits/29a1af1713c150f37d760a2dc03b714d2fd046a1))
- **CORE-2112:** stop Avatars from compressing when there is not enough room. ([8829eef](http://bitbucket.org/thermsio/atalaya/commits/8829eeff63ee6f36b825fbeaefdda08c09b7d4bf))
- **CORE-2148:** Image, add clickable styling to Image Skeleton. ([821153e](http://bitbucket.org/thermsio/atalaya/commits/821153e5c0d08ee68fef554c9c6aa631b4fc397f))

# [7.32.0](http://bitbucket.org/thermsio/atalaya/compare/v7.31.1...v7.32.0) (2022-05-23)

### Features

- **CORE-2148:** Tabs, check for children change to calculate mobile tabs. ([8b09bf9](http://bitbucket.org/thermsio/atalaya/commits/8b09bf9365c8756f90a5c11ab2799477196dec6b))

## [7.31.1](https://bitbucket.org/thermsio/atalaya/compare/v7.31.0...v7.31.1) (2022-05-21)

### Bug Fixes

- **CORE-2154:** allow FormLayout horizontal prop to override the screen width calculations ([53a657e](https://bitbucket.org/thermsio/atalaya/commits/53a657ecc321d112a2066626a67f0835193b660f))

# [7.31.0](https://bitbucket.org/thermsio/atalaya/compare/v7.30.1...v7.31.0) (2022-05-19)

### Features

- **FormLayout.Controls:** confirmDelete ([c13674e](https://bitbucket.org/thermsio/atalaya/commits/c13674ea7f7e1fe5d7646c1634c39b11ab45f216))

## [7.30.1](https://bitbucket.org/thermsio/atalaya/compare/v7.30.0...v7.30.1) (2022-05-18)

### Bug Fixes

- **Label:** prop types for children ([273c5c3](https://bitbucket.org/thermsio/atalaya/commits/273c5c39e4147ffca7aa11a72c82ee084c212efa))

# [7.30.0](http://bitbucket.org/thermsio/atalaya/compare/v7.29.6...v7.30.0) (2022-05-18)

### Features

- **CORE-1947:** add npm publish script. ([a8fb01e](http://bitbucket.org/thermsio/atalaya/commits/a8fb01e2eb33c46e943e9e55e0534c84e7e46097))
- **CORE-1947:** only show error image after retrying. ([fa8d299](http://bitbucket.org/thermsio/atalaya/commits/fa8d299a995b2bed42300061a5078de46f72badc))

## [7.29.6](http://bitbucket.org/thermsio/atalaya/compare/v7.29.5...v7.29.6) (2022-05-17)

### Bug Fixes

- **CORE-1947:** fix validation jumping around on TextArea. ([510d38a](http://bitbucket.org/thermsio/atalaya/commits/510d38aee24deda3b9db362ff314542fe3c7bf3f))

## [7.29.5](http://bitbucket.org/thermsio/atalaya/compare/v7.29.4...v7.29.5) (2022-05-17)

### Bug Fixes

- **Select:** return null when Select component value has no option in options ([fdb2752](http://bitbucket.org/thermsio/atalaya/commits/fdb27529c379463148e65022d7950d15a07a5b62))

## [7.29.4](http://bitbucket.org/thermsio/atalaya/compare/v7.29.3...v7.29.4) (2022-05-17)

### Bug Fixes

- Select null & undefined value ([bcb3f12](http://bitbucket.org/thermsio/atalaya/commits/bcb3f12dfb76418b6eeefc6a3dc2f101181e8108))

## [7.29.3](https://bitbucket.org/thermsio/atalaya/compare/v7.29.2...v7.29.3) (2022-05-15)

### Bug Fixes

- **Select:** apply optionValueExtractor ([1d59304](https://bitbucket.org/thermsio/atalaya/commits/1d59304dbb70119ef1ce4fb265c9a269c1454299))

## [7.29.2](http://bitbucket.org/thermsio/atalaya/compare/v7.29.1...v7.29.2) (2022-05-14)

### Bug Fixes

- **CORE-2162:** Select update selectedValue by value change ([5b948d8](http://bitbucket.org/thermsio/atalaya/commits/5b948d84cf5202eea7b61f77794a50410bb7a998))

## [7.29.1](http://bitbucket.org/thermsio/atalaya/compare/v7.29.0...v7.29.1) (2022-05-13)

### Bug Fixes

- **Select:** menuOverlay = true by default ([3e08d43](http://bitbucket.org/thermsio/atalaya/commits/3e08d43f8ab128340f41030d65d238833f3c2f01))

# [7.29.0](http://bitbucket.org/thermsio/atalaya/compare/v7.28.0...v7.29.0) (2022-05-11)

### Features

- **CORE-1588:** change Breadcrumbs type to ReactNode ([e439c50](http://bitbucket.org/thermsio/atalaya/commits/e439c505679bdff199c4c835d893aac6d412c69f))

# [7.28.0](http://bitbucket.org/thermsio/atalaya/compare/v7.27.0...v7.28.0) (2022-05-10)

### Features

- **CORE-1588:** Button, add size="xs"; ([785f4ea](http://bitbucket.org/thermsio/atalaya/commits/785f4eaddc782ee7c7ccec5f83fdf893dde618f1))

# [7.27.0](http://bitbucket.org/thermsio/atalaya/compare/v7.26.0...v7.27.0) (2022-05-10)

### Features

- **CORE-1588:** Breadcrumbs, update styling; ([c0fc730](http://bitbucket.org/thermsio/atalaya/commits/c0fc73032e3a806e319829e139f12fdb997e78d5))

# [7.26.0](http://bitbucket.org/thermsio/atalaya/compare/v7.25.1...v7.26.0) (2022-05-09)

### Features

- **CORE-1588:** Breadcrumbs, add backClickIcon/separatorIcon props; ([22e22a8](http://bitbucket.org/thermsio/atalaya/commits/22e22a88c05d275eac59876d6a778cb723a781e0))

## [7.25.1](http://bitbucket.org/thermsio/atalaya/compare/v7.25.0...v7.25.1) (2022-05-09)

### Bug Fixes

- remove postcss deprecation warning; ([0d3452f](http://bitbucket.org/thermsio/atalaya/commits/0d3452ffbab60a8aa97921ac363451e17b760fc9))

# [7.25.0](http://bitbucket.org/thermsio/atalaya/compare/v7.24.3...v7.25.0) (2022-05-04)

### Features

- Avatar, add onClick; ([a9fef0c](http://bitbucket.org/thermsio/atalaya/commits/a9fef0c20dc54fe7517940ba518c81ec2efc38bf))
- Button, add active prop; ([2fb8ec3](http://bitbucket.org/thermsio/atalaya/commits/2fb8ec39c8e9baf2f54d53216995d954a51199a0))

## [7.24.3](http://bitbucket.org/thermsio/atalaya/compare/v7.24.2...v7.24.3) (2022-05-03)

### Bug Fixes

- **Modal:** small screen overflow and padding issues ([ffc902e](http://bitbucket.org/thermsio/atalaya/commits/ffc902ecee2d0316cd61af01a79fd47b8161d3d7))

## [7.24.2](https://bitbucket.org/thermsio/atalaya/compare/v7.24.1...v7.24.2) (2022-05-02)

### Bug Fixes

- Tabs comment out mobile width check ([98f63be](https://bitbucket.org/thermsio/atalaya/commits/98f63be28355d2163b3fb362d79ff3e8084ed976))

## [7.24.1](https://bitbucket.org/thermsio/atalaya/compare/v7.24.0...v7.24.1) (2022-05-02)

### Bug Fixes

- update deps ([b4c772a](https://bitbucket.org/thermsio/atalaya/commits/b4c772a1f04a0ee2b334c87122c86324dac85497))

# [7.24.0](http://bitbucket.org/thermsio/atalaya/compare/v7.23.1...v7.24.0) (2022-05-02)

### Bug Fixes

- **CORE-1523:** Modal moving up and down when scrolling; ([eff2427](http://bitbucket.org/thermsio/atalaya/commits/eff2427120a95e8bf0111691c061b061bec235a0))
- **CORE-1523:** Modal nested modal being bounded by parent Modal; ([211e56a](http://bitbucket.org/thermsio/atalaya/commits/211e56a54236254d4eee6874ed524dd565c82ac8))
- **CORE-1523:** rename usePageScrollFreeze.tsx to usePageScrollLock.ts ([9d3d2a2](http://bitbucket.org/thermsio/atalaya/commits/9d3d2a27f3cc2be493c981d848e653d079cfe163))

### Features

- **CORE-1523:** add usePageScrollFreeze; ([12c1d73](http://bitbucket.org/thermsio/atalaya/commits/12c1d731594ddaa02cbbfdf7221c0c186965fa15))
- **CORE-1523:** SlidePanel, make header optional; ([4ae0ee3](http://bitbucket.org/thermsio/atalaya/commits/4ae0ee3218b88cd0888c5789bfd653e7eb57fb29))

## [7.23.1](http://bitbucket.org/thermsio/atalaya/compare/v7.23.0...v7.23.1) (2022-04-27)

### Bug Fixes

- **CORE-2097:** Image, fix fluid not working; ([8af5c5a](http://bitbucket.org/thermsio/atalaya/commits/8af5c5a09326b5762b46e5d46097532cce76bade))

# [7.23.0](http://bitbucket.org/thermsio/atalaya/compare/v7.22.0...v7.23.0) (2022-04-27)

### Features

- **CORE-2097:** Tag, add auto contrast for custom color; tweak styling; ([b4fed69](http://bitbucket.org/thermsio/atalaya/commits/b4fed69b7828ced96dc10b534822389a0ff74c2c))
- **CORE-2097:** Tag, default to sm tag, rename sm to lg; ([510147e](http://bitbucket.org/thermsio/atalaya/commits/510147ed95df495682db4eb5b11cd88404425166))

# [7.22.0](http://bitbucket.org/thermsio/atalaya/compare/v7.21.1...v7.22.0) (2022-04-26)

### Features

- **CORE-2106:** 0 count no longer shown on Badge ([5593e36](http://bitbucket.org/thermsio/atalaya/commits/5593e36bcd58593007feba6fd17704f2305296c5))
- **CORE-2106:** Refactored Badge displayValue ([7aa28c0](http://bitbucket.org/thermsio/atalaya/commits/7aa28c0427bda0eb2cee9b8d04994ee0d25567fd))

## [7.21.1](http://bitbucket.org/thermsio/atalaya/compare/v7.21.0...v7.21.1) (2022-04-26)

### Bug Fixes

- Radio button styles having less precedence than general input styles ([0f7dd1a](http://bitbucket.org/thermsio/atalaya/commits/0f7dd1ade8df81e18e01a617e7fefdb7a67acb9c))

# [7.21.0](http://bitbucket.org/thermsio/atalaya/compare/v7.20.2...v7.21.0) (2022-04-26)

### Features

- remove webpack-hack.js; ([b91fe74](http://bitbucket.org/thermsio/atalaya/commits/b91fe74f120ac38470da1674a2888d29aab8ce17))

## [7.20.2](http://bitbucket.org/thermsio/atalaya/compare/v7.20.1...v7.20.2) (2022-04-24)

### Bug Fixes

- Modal headline overflow ([3a08284](http://bitbucket.org/thermsio/atalaya/commits/3a08284903353646efdd8cf214eba7c67b88350a))

## [7.20.1](http://bitbucket.org/thermsio/atalaya/compare/v7.20.0...v7.20.1) (2022-04-21)

### Bug Fixes

- html & body font-size use --size-text variable value ([a6e360f](http://bitbucket.org/thermsio/atalaya/commits/a6e360f91081bd83976c3d435f579f1a2e911ea0))

# [7.20.0](http://bitbucket.org/thermsio/atalaya/compare/v7.19.0...v7.20.0) (2022-04-21)

### Bug Fixes

- **CORE-1673:** Image, fix lazy={[secure]} no working; ([bf6eb28](http://bitbucket.org/thermsio/atalaya/commits/bf6eb28a3218d1c308240fe9f3cfc71a9da3deaa))

### Features

- **CORE-1673:** add render props to Lightbox children; ([91a4584](http://bitbucket.org/thermsio/atalaya/commits/91a4584ba93398d5bfdb6ca98dabd861c5e41127))
- **CORE-1673:** add stylings when image has onClick prop; ([7687b32](http://bitbucket.org/thermsio/atalaya/commits/7687b3241491324574b55d8a4aea8fddf4af6ba9))

# [7.19.0](http://bitbucket.org/thermsio/atalaya/compare/v7.18.0...v7.19.0) (2022-04-19)

### Features

- wrap Icon, Image, ColorSquare, ColorCircle in React.memo ([ce300eb](http://bitbucket.org/thermsio/atalaya/commits/ce300eb18ce8bfdd8ac67481fd4b95b510a1ef9f))

# [7.18.0](http://bitbucket.org/thermsio/atalaya/compare/v7.17.0...v7.18.0) (2022-04-19)

### Bug Fixes

- **CORE-2090:** fix toasts going behind modal; ([2a75979](http://bitbucket.org/thermsio/atalaya/commits/2a75979d20c1c66583d72ddaeabafb5a661d5a4b))
- **CORE-2090:** Overlay, fix not working properly. ([03ad7bf](http://bitbucket.org/thermsio/atalaya/commits/03ad7bf7fca9bb846056f73c21f6d4db0c36a3b8))

### Features

- **CORE-2090:** ToastContainer, stop exposing react-toastify props; ([a292285](http://bitbucket.org/thermsio/atalaya/commits/a29228514f422b243fa20745d21e6b02bad8a14e))

# [7.17.0](http://bitbucket.org/thermsio/atalaya/compare/v7.16.2...v7.17.0) (2022-04-18)

### Features

- **CORE-1427:** re implement LightboxViewer; ([2b548de](http://bitbucket.org/thermsio/atalaya/commits/2b548de6fb6a50da76472de0f3b6a211eb06124d))

## [7.16.2](http://bitbucket.org/thermsio/atalaya/compare/v7.16.1...v7.16.2) (2022-04-16)

### Bug Fixes

- **Tabs:** remove w-full on container ([0916d35](http://bitbucket.org/thermsio/atalaya/commits/0916d351246453690ced74ae76fbc434e0cc97e2))

## [7.16.1](http://bitbucket.org/thermsio/atalaya/compare/v7.16.0...v7.16.1) (2022-04-15)

### Bug Fixes

- **Modal:** header full width ([11a8484](http://bitbucket.org/thermsio/atalaya/commits/11a84846aa74a1c457cdf26f1f4e9df9fe7dc7c2))

# [7.16.0](http://bitbucket.org/thermsio/atalaya/compare/v7.15.1...v7.16.0) (2022-04-14)

### Bug Fixes

- **CORE-2055:** fix docs type; update SelectOptions interface, ([658c960](http://bitbucket.org/thermsio/atalaya/commits/658c9608fc427100288d01c29669ef6607036b1c))
- **CORE-2055:** Select, remove totalOptions, ([0437bad](http://bitbucket.org/thermsio/atalaya/commits/0437bad921d5b44c571d3e8dfccdcf89f2fb02f0))

### Features

- **CORE-2055:** Export Select and HTMLSelect Option Types, ([df2ffb7](http://bitbucket.org/thermsio/atalaya/commits/df2ffb7ba7c71fe4aa83216953b89a3c670734ae))
- **CORE-2055:** Select, replace loading skeletons for absolutely positioned spinner, ([7489915](http://bitbucket.org/thermsio/atalaya/commits/7489915f118f26edcda8d4d576336258ee0f137f))

## [7.15.1](http://bitbucket.org/thermsio/atalaya/compare/v7.15.0...v7.15.1) (2022-04-11)

### Bug Fixes

- Tabs overflow from scroll to auto ([0729a2f](http://bitbucket.org/thermsio/atalaya/commits/0729a2face63bd89438cd97597ccc3e47709a73f))

# [7.15.0](http://bitbucket.org/thermsio/atalaya/compare/v7.14.4...v7.15.0) (2022-04-11)

### Features

- add semantic text tones; ([2741663](http://bitbucket.org/thermsio/atalaya/commits/27416633c8bad92546d34091641b5716d896194d))

## [7.14.4](http://bitbucket.org/thermsio/atalaya/compare/v7.14.3...v7.14.4) (2022-04-11)

### Bug Fixes

- **Tabs:** re-calculate width when dynamic Tabs children render ([dce975f](http://bitbucket.org/thermsio/atalaya/commits/dce975f0e7ebf6db66f53cb842d3f778e172e9ae))

## [7.14.3](http://bitbucket.org/thermsio/atalaya/compare/v7.14.2...v7.14.3) (2022-04-10)

### Bug Fixes

- **Modal:** header full width ([30e6986](http://bitbucket.org/thermsio/atalaya/commits/30e6986d0542c3a1194e6745e6fffe4417426705))

## [7.14.2](http://bitbucket.org/thermsio/atalaya/compare/v7.14.1...v7.14.2) (2022-04-10)

### Bug Fixes

- **Tabs:** add overflow to desktop component ([40a0351](http://bitbucket.org/thermsio/atalaya/commits/40a0351a588fa689514e25d31e76d05d8f2220e6))

## [7.14.1](http://bitbucket.org/thermsio/atalaya/compare/v7.14.0...v7.14.1) (2022-04-08)

### Bug Fixes

- **CORE-2055:** Select, only check for totalOptions if it has been defined; ([618b582](http://bitbucket.org/thermsio/atalaya/commits/618b582a1f4805935c3c86198d9a9d56c4c0387e))

# [7.14.0](http://bitbucket.org/thermsio/atalaya/compare/v7.13.0...v7.14.0) (2022-04-08)

### Features

- **CORE-2055:** Select, add totalOptions; ([2231ea9](http://bitbucket.org/thermsio/atalaya/commits/2231ea921b27ef7186d068f1643b45635fce0742))

# [7.13.0](http://bitbucket.org/thermsio/atalaya/compare/v7.12.0...v7.13.0) (2022-04-07)

### Features

- make Select dropdown rounded; ([1154785](http://bitbucket.org/thermsio/atalaya/commits/1154785c08eff5722344058e278ae18ddc1088db))

# [7.12.0](https://bitbucket.org/thermsio/atalaya/compare/v7.11.0...v7.12.0) (2022-04-06)

### Features

- **Select:** RenderCustomMenuList option ([83103be](https://bitbucket.org/thermsio/atalaya/commits/83103bed53755eddf4c942eb1c809400ec4aa8cd))

# [7.11.0](http://bitbucket.org/thermsio/atalaya/compare/v7.10.1...v7.11.0) (2022-04-06)

### Features

- **Select:** improve tabbed select menu UI ([6b37e9e](http://bitbucket.org/thermsio/atalaya/commits/6b37e9ed277be300928492637374b5496322b7b7))

## [7.10.1](http://bitbucket.org/thermsio/atalaya/compare/v7.10.0...v7.10.1) (2022-04-06)

### Bug Fixes

- stop loading overlay from overlapping on modal; ([ca58854](http://bitbucket.org/thermsio/atalaya/commits/ca58854edb974d4c846f1f8f49ddd513545b148d))

# [7.10.0](http://bitbucket.org/thermsio/atalaya/compare/v7.9.1...v7.10.0) (2022-04-06)

### Features

- **CORE-1427:** added className prop ([1454aa6](http://bitbucket.org/thermsio/atalaya/commits/1454aa6c8583095966d6dff11b67ae553fb65863))
- **CORE-1427:** adding asynchronous extracting type of file ([5d11bee](http://bitbucket.org/thermsio/atalaya/commits/5d11beef4887494fc373c85d0dc34de07f92d0b3))
- **CORE-1427:** created LightboxViewer.md ([c3ba2ae](http://bitbucket.org/thermsio/atalaya/commits/c3ba2aee0b4f84fca2e8d3d27560463d864c9002))
- **CORE-1427:** creating lightbox component for images and videos ([77d4476](http://bitbucket.org/thermsio/atalaya/commits/77d4476b6622906e5fd5befec52f2f1a91298088))
- **CORE-1427:** export example video component and cleaned lightboxViewer, LightboxViewerContext.tsx and Image components from logs ([35aed04](http://bitbucket.org/thermsio/atalaya/commits/35aed045108bdc942cd19441b05f3f41d4c8c22e))
- **CORE-1427:** exported default lightbox wrapped by lightbox context provider ([73e07e2](http://bitbucket.org/thermsio/atalaya/commits/73e07e272ee6da4cd5b499cfb40e5d092c3271ea))
- **CORE-1427:** extracting type from the url in Image ([a9ff040](http://bitbucket.org/thermsio/atalaya/commits/a9ff04041cb2c32c1b5a7f88b980ba0473e97859))
- **CORE-1427:** fixed asynchronous load of images and videos ([514e242](http://bitbucket.org/thermsio/atalaya/commits/514e2422aa6a9c2bb18311a2ca1843cc63c7da20))
- **CORE-1427:** refactored structure ([0d5854c](http://bitbucket.org/thermsio/atalaya/commits/0d5854c633296fc11d2ba7d589540306d880781b))
- **CORE-1427:** removed logs and added horizontal displaying ([8a13aef](http://bitbucket.org/thermsio/atalaya/commits/8a13aefe5b788c1d4c2c33c7a560bf08e7f21e98))
- **CORE-1673:** cleaned the code ([6a69a7d](http://bitbucket.org/thermsio/atalaya/commits/6a69a7d965665cdadbb011c3337adb57fa5cfad8))
- **CORE-1673:** fixed lightbox for single image through lightboxUrl prop ([2a182b5](http://bitbucket.org/thermsio/atalaya/commits/2a182b53159848a613b5a98242c54b3a828ada58))

## [7.9.1](http://bitbucket.org/thermsio/atalaya/compare/v7.9.0...v7.9.1) (2022-04-05)

### Bug Fixes

- revert react@17.0.2 ([0d042da](http://bitbucket.org/thermsio/atalaya/commits/0d042dab5eee3049abc6ca537fef53acca6573b7))

# [7.9.0](http://bitbucket.org/thermsio/atalaya/compare/v7.8.0...v7.9.0) (2022-04-05)

### Features

- multi-select using react-select ([47afcca](http://bitbucket.org/thermsio/atalaya/commits/47afccafedab74b9da5830dabae38efd9252ebbe))
- multi-select using react-select ([7dd174e](http://bitbucket.org/thermsio/atalaya/commits/7dd174e00308a51753e7f7effceb1cff7dfa0c11))

# [7.8.0](http://bitbucket.org/thermsio/atalaya/compare/v7.7.2...v7.8.0) (2022-04-04)

### Bug Fixes

- ButtonGroup docs not showing examples; ([9fe6fd8](http://bitbucket.org/thermsio/atalaya/commits/9fe6fd80fe6a5bec285e861c9908ac0615306a5e))

### Features

- update Toasts to react-toastify v8; ([2f34b99](http://bitbucket.org/thermsio/atalaya/commits/2f34b99e09a77bbccdf5b11967d742ccd961aa3e))

## [7.7.2](http://bitbucket.org/thermsio/atalaya/compare/v7.7.1...v7.7.2) (2022-04-03)

### Bug Fixes

- add @therms/icons@0.0.1 back to devDeps for docs site ([4948665](http://bitbucket.org/thermsio/atalaya/commits/4948665278e2bd19f5f9a0e44d0548fc661413ab))

## [7.7.1](http://bitbucket.org/thermsio/atalaya/compare/v7.7.0...v7.7.1) (2022-03-31)

### Bug Fixes

- Number, add and minus buttons not firing onChange event; ([6e443e0](http://bitbucket.org/thermsio/atalaya/commits/6e443e07cd0aed532c4586ae12b37a6cb25029a8))

# [7.7.0](http://bitbucket.org/thermsio/atalaya/compare/v7.6.1...v7.7.0) (2022-03-29)

### Features

- **CORE-1952:** Select, add filterOption, loadingMessage; ([078e54d](http://bitbucket.org/thermsio/atalaya/commits/078e54da9d0829ff6ce4c7b8d639ae46a5cacab3))

## [7.6.1](http://bitbucket.org/thermsio/atalaya/compare/v7.6.0...v7.6.1) (2022-03-25)

### Bug Fixes

- TableCell py-2 ([e267afc](http://bitbucket.org/thermsio/atalaya/commits/e267afcef1b5bc9e2475f65438d0f535df9cc50d))

# [7.6.0](http://bitbucket.org/thermsio/atalaya/compare/v7.5.6...v7.6.0) (2022-03-25)

### Features

- **CORE-1952:** Select, add RenderNoOptions; ([9ba5ccc](http://bitbucket.org/thermsio/atalaya/commits/9ba5ccc9022372e4379119ca156ab77cb02f16bc))

## [7.5.6](https://bitbucket.org/thermsio/atalaya/compare/v7.5.5...v7.5.6) (2022-03-24)

### Bug Fixes

- remove useKeyPress stopPropogation ([b18fdc1](https://bitbucket.org/thermsio/atalaya/commits/b18fdc191fbf2a3c099559bc34519bcd3b65299a))

## [7.5.5](https://bitbucket.org/thermsio/atalaya/compare/v7.5.4...v7.5.5) (2022-03-24)

### Bug Fixes

- **ModalContext:** prevent nested modals from taking full screen height ([5615e66](https://bitbucket.org/thermsio/atalaya/commits/5615e66dcf75b68e113ca5a06a09121053d5040d))

## [7.5.5](https://bitbucket.org/thermsio/atalaya/compare/v7.5.4...v7.5.5) (2022-03-24)

### Bug Fixes

- **ModalContext:** prevent nested modals from taking full screen height ([5615e66](https://bitbucket.org/thermsio/atalaya/commits/5615e66dcf75b68e113ca5a06a09121053d5040d))

## [7.5.4](https://bitbucket.org/thermsio/atalaya/compare/v7.5.3...v7.5.4) (2022-03-24)

### Bug Fixes

- typo ([e65c6e4](https://bitbucket.org/thermsio/atalaya/commits/e65c6e45766fb81f502447cb98b9833507732a8f))

## [7.5.3](https://bitbucket.org/thermsio/atalaya/compare/v7.5.2...v7.5.3) (2022-03-24)

### Bug Fixes

- **Timeline:** return null when no data ([c83e83b](https://bitbucket.org/thermsio/atalaya/commits/c83e83b4d3a7cd637672d480cf98aa4fcafc37ce))

## [7.5.2](https://bitbucket.org/thermsio/atalaya/compare/v7.5.1...v7.5.2) (2022-03-24)

### Bug Fixes

- **ActionModal:** title ([c4b718a](https://bitbucket.org/thermsio/atalaya/commits/c4b718ac48b127b60865353ddf92864ec93d9b66))

## [7.5.1](https://bitbucket.org/thermsio/atalaya/compare/v7.5.0...v7.5.1) (2022-03-24)

### Bug Fixes

- **ActionModal:** body element ([694d840](https://bitbucket.org/thermsio/atalaya/commits/694d840951394041706e249fabfe480b93aded4a))

# [7.5.0](https://bitbucket.org/thermsio/atalaya/compare/v7.4.0...v7.5.0) (2022-03-23)

### Features

- **ActionModal:** add escPressClose and title type ReactNode ([00c9cfb](https://bitbucket.org/thermsio/atalaya/commits/00c9cfb5bebc8a43884456f8ba8572d3c6380ada))

# [7.4.0](https://bitbucket.org/thermsio/atalaya/compare/v7.3.1...v7.4.0) (2022-03-23)

### Features

- Icon add xs size ([73d3130](https://bitbucket.org/thermsio/atalaya/commits/73d31304c17f6d68e44b766dbe1b13c260fd6fd1))

## [7.3.1](https://bitbucket.org/thermsio/atalaya/compare/v7.3.0...v7.3.1) (2022-03-23)

### Bug Fixes

- **Timeline:** iconBgColor ([80690fc](https://bitbucket.org/thermsio/atalaya/commits/80690fcb3a0891c78eb62e02321c6cd698442d13))

# [7.3.0](http://bitbucket.org/thermsio/atalaya/compare/v7.2.2...v7.3.0) (2022-03-23)

### Bug Fixes

- docs ([e190f1f](http://bitbucket.org/thermsio/atalaya/commits/e190f1ffdac9d24c54629f0a059a15c07b55fbbd))

### Features

- **Timeline:** add renderTimestamp & iconBgColor props ([cded9ce](http://bitbucket.org/thermsio/atalaya/commits/cded9ce4210e5226d92565ea7e9b4107a1014222))

## [7.2.2](http://bitbucket.org/thermsio/atalaya/compare/v7.2.1...v7.2.2) (2022-03-21)

### Bug Fixes

- **CORE-1987:** remove src/ from npm publish ([1e98a62](http://bitbucket.org/thermsio/atalaya/commits/1e98a627047ebb0158bd7b9db683fda6ac9bdb9d))

## [7.2.1](http://bitbucket.org/thermsio/atalaya/compare/v7.2.0...v7.2.1) (2022-03-21)

### Bug Fixes

- **Card:** add onClick to prop types ([a99a6cb](http://bitbucket.org/thermsio/atalaya/commits/a99a6cbb281b03f3c9bde679236d0af421bae470))

# [7.2.0](http://bitbucket.org/thermsio/atalaya/compare/v7.1.1...v7.2.0) (2022-03-21)

### Features

- add border and onClick props to Card; ([4718374](http://bitbucket.org/thermsio/atalaya/commits/4718374ee04d6a2187ef148f77e7d3e6c3b19393))

## [7.1.1](https://bitbucket.org/thermsio/atalaya/compare/v7.1.0...v7.1.1) (2022-03-21)

### Bug Fixes

- **Modal:** setHeader syntax err ([1d4cd33](https://bitbucket.org/thermsio/atalaya/commits/1d4cd333146da90097a34be4a8bf644c7a660020))

# [7.1.0](http://bitbucket.org/thermsio/atalaya/compare/v7.0.3...v7.1.0) (2022-03-21)

### Features

- **Modal:** all ModalContextProvider to pass setHeader() to children via context ([569dd0f](http://bitbucket.org/thermsio/atalaya/commits/569dd0fe190238ec03e18f8bc7143f48e97f7bf8))

## [7.0.3](http://bitbucket.org/thermsio/atalaya/compare/v7.0.2...v7.0.3) (2022-03-19)

### Bug Fixes

- toastManager variant & position const add string type ([4a61538](http://bitbucket.org/thermsio/atalaya/commits/4a61538175f5c8c5ad93975a3ccdeaf2d283ee02))

## [7.0.2](http://bitbucket.org/thermsio/atalaya/compare/v7.0.1...v7.0.2) (2022-03-17)

### Bug Fixes

- cancel Icon fetch request if component is unmounted; ([e57643e](http://bitbucket.org/thermsio/atalaya/commits/e57643ea4dc66b253ac634e3c4455e84da9f7c61))

## [7.0.1](http://bitbucket.org/thermsio/atalaya/compare/v7.0.0...v7.0.1) (2022-03-17)

### Bug Fixes

- show colorSquare/Circle if variant is chosen; ([e31610e](http://bitbucket.org/thermsio/atalaya/commits/e31610e352cc599d033c6b8a363db53ffea7ddb9))

# [7.0.0](http://bitbucket.org/thermsio/atalaya/compare/v6.16.0...v7.0.0) (2022-03-17)

### Features

- add ColorCircle ColorSquare; ([8535db6](http://bitbucket.org/thermsio/atalaya/commits/8535db619b0939a989577195ab7f6cbdf21dce84))

### BREAKING CHANGES

- removed PriorityColor, please use ColorSquare instead;

# [6.16.0](http://bitbucket.org/thermsio/atalaya/compare/v6.15.5...v6.16.0) (2022-03-16)

### Features

- **CORE-1628:** created type ScreenSizeName ([9de2c64](http://bitbucket.org/thermsio/atalaya/commits/9de2c6488c3a592fd495c1c08d987c6850c8c0d3))
- **CORE-1628:** fixed type for column for creating column index map ([2f3e124](http://bitbucket.org/thermsio/atalaya/commits/2f3e124959ada865f20611acd6e754e0f6f117d7))
- **CORE-1628:** hiding columns in TableGroup based on visibleSize prop ([68d71c1](http://bitbucket.org/thermsio/atalaya/commits/68d71c19ef06e135d41c58ff934589d69364a1cd))
- **CORE-1628:** hiding columns in TableGroup based on visibleSize prop added ([5ace7f8](http://bitbucket.org/thermsio/atalaya/commits/5ace7f899ef695c6c5236950e6cb5ebdbb4a9d6b))
- **CORE-1628:** hiding columns in TableGroup based on visibleSize prop fixed types and performance ([a71a77c](http://bitbucket.org/thermsio/atalaya/commits/a71a77c8f0fd358e7fd81d65abdce3c551df25b9))
- **CORE-1628:** hiding columns in TableGroup based on visibleSize prop refactor useEffect and added setForceUpdate ([dbfdb25](http://bitbucket.org/thermsio/atalaya/commits/dbfdb2501ef7381f342c221363817e594743ac6e))
- **CORE-1628:** hiding columns in TableGroup based on visibleSize prop refactored ([3903f47](http://bitbucket.org/thermsio/atalaya/commits/3903f47a27c5da3010ce65b90296fc85bc9006b8))

## [6.15.5](http://bitbucket.org/thermsio/atalaya/compare/v6.15.4...v6.15.5) (2022-03-16)

### Bug Fixes

- avoid purging p/m-x/y classes; ([77bf912](http://bitbucket.org/thermsio/atalaya/commits/77bf912eae973419dae69b9b04ebfa8b7adc4ec4))

## [6.15.4](http://bitbucket.org/thermsio/atalaya/compare/v6.15.3...v6.15.4) (2022-03-15)

### Bug Fixes

- Select, firstRender always true; ([ebda201](http://bitbucket.org/thermsio/atalaya/commits/ebda2018e33df2d7a264760af1e367517f890e1f))

## [6.15.3](http://bitbucket.org/thermsio/atalaya/compare/v6.15.2...v6.15.3) (2022-03-15)

### Bug Fixes

- Icon, crashing app when url fetch returned 404; ([56b5ad1](http://bitbucket.org/thermsio/atalaya/commits/56b5ad1b720b8dae31fd13cc48b5b40652a8fb54))
- pipeline crashing on build step; ([02ba841](http://bitbucket.org/thermsio/atalaya/commits/02ba841309d94db4884bfeaed3a491d76863bcee))
- pipeline crashing; ([efe1c03](http://bitbucket.org/thermsio/atalaya/commits/efe1c0392c3ef8d718705ac5f7e8ff1ed6951fad))

## [6.15.2](http://bitbucket.org/thermsio/atalaya/compare/v6.15.1...v6.15.2) (2022-03-15)

### Bug Fixes

- es module build ([f304945](http://bitbucket.org/thermsio/atalaya/commits/f304945a8ac4fa4302150f91fd7ee4a3a965c114))

## [6.15.1](http://bitbucket.org/thermsio/atalaya/compare/v6.15.0...v6.15.1) (2022-03-11)

### Bug Fixes

- Select prop type optionLabelExtractor returns ReactNode ([c7b98c7](http://bitbucket.org/thermsio/atalaya/commits/c7b98c772e3bff0ac1a2a55988ff4d2ffeb940dd))
- ts lint error ([0ae1f5c](http://bitbucket.org/thermsio/atalaya/commits/0ae1f5cff0842783a7773df37a4179d4cceb079e))

# [6.15.0](http://bitbucket.org/thermsio/atalaya/compare/v6.14.3...v6.15.0) (2022-03-08)

### Features

- Icon, support HEX color values; ([dd6a8bf](http://bitbucket.org/thermsio/atalaya/commits/dd6a8bf78f596ceb75db4d12ffb9db483fa3aecd))

## [6.14.3](http://bitbucket.org/thermsio/atalaya/compare/v6.14.2...v6.14.3) (2022-03-05)

### Bug Fixes

- **CORE-1761:** removed blue box from input while focusing in Select component ([d76fc35](http://bitbucket.org/thermsio/atalaya/commits/d76fc3575ad78d9a716dbb642b77fcc1164d7a43))

## [6.14.2](https://bitbucket.org/thermsio/atalaya/compare/v6.14.1...v6.14.2) (2022-03-05)

### Bug Fixes

- **ContextMenu:** css styles MenuOption cursor ([b208d68](https://bitbucket.org/thermsio/atalaya/commits/b208d68bbda45e9d9cf94d766f0de6671d9fa440))

## [6.14.1](https://bitbucket.org/thermsio/atalaya/compare/v6.14.0...v6.14.1) (2022-03-05)

### Bug Fixes

- **ContextMenu:** css styles MenuOption cursor ([e67a62e](https://bitbucket.org/thermsio/atalaya/commits/e67a62eb3130e76995e69d9e24bcfccdc46dbe5d))

# [6.14.0](https://bitbucket.org/thermsio/atalaya/compare/v6.13.2...v6.14.0) (2022-03-05)

### Features

- **ContextMenu:** add 'disabled' prop to MenuOption ([bbfc95f](https://bitbucket.org/thermsio/atalaya/commits/bbfc95f23bde7cc7f46680f2f49f6db2fd9e4ee5))

## [6.13.2](http://bitbucket.org/thermsio/atalaya/compare/v6.13.1...v6.13.2) (2022-03-04)

### Bug Fixes

- Select Multi Items being centered & tweak spacing; ([58f860b](http://bitbucket.org/thermsio/atalaya/commits/58f860b5fdf8b36aed689021ab3f51d24fe4820f))

## [6.13.1](http://bitbucket.org/thermsio/atalaya/compare/v6.13.0...v6.13.1) (2022-03-04)

### Reverts

- Revert "fix: Selected Multi Option no longer compress to minimum size;" ([6e2bc29](http://bitbucket.org/thermsio/atalaya/commits/6e2bc2930ec65fe6231880161368edd1f1cccdf9))

# [6.13.0](http://bitbucket.org/thermsio/atalaya/compare/v6.12.2...v6.13.0) (2022-03-03)

### Bug Fixes

- **CORE-1607:** fixed blinking in CopyText component ([5aa04a4](http://bitbucket.org/thermsio/atalaya/commits/5aa04a477f5cac910e3e2ca3ebc1c20a49594af3))

### Features

- **CORE-1370:** added highlighting the text after clicking in DateTimePicker.tsx based on Number.tsx ([d853a1f](http://bitbucket.org/thermsio/atalaya/commits/d853a1fc238bcdf4b2cb468ccef41bd4ca34b285))

## [6.12.2](http://bitbucket.org/thermsio/atalaya/compare/v6.12.1...v6.12.2) (2022-03-03)

### Bug Fixes

- Selected Multi Option no longer compress to minimum size; ([525598c](http://bitbucket.org/thermsio/atalaya/commits/525598c6f9bf0e1780b4ecdfac9a04bcbaa5f864))

## [6.12.1](http://bitbucket.org/thermsio/atalaya/compare/v6.12.0...v6.12.1) (2022-03-03)

### Bug Fixes

- Remove console.log; ([a4af749](http://bitbucket.org/thermsio/atalaya/commits/a4af7495c79bfa7851e3a39896a24df04eb964ae))
- Select, Stop loadingMore when no new options appear; ([7e7699e](http://bitbucket.org/thermsio/atalaya/commits/7e7699e95f4e937ee943500141586759c16fe9b8))

# [6.12.0](http://bitbucket.org/thermsio/atalaya/compare/v6.11.5...v6.12.0) (2022-03-03)

### Features

- export component props; ([00e0819](http://bitbucket.org/thermsio/atalaya/commits/00e08198fed5d08cf83f8a2769d8b6fac5fae68f))

## [6.11.5](http://bitbucket.org/thermsio/atalaya/compare/v6.11.4...v6.11.5) (2022-03-03)

### Bug Fixes

- DateTimePicker docs not working; ([9f43a4d](http://bitbucket.org/thermsio/atalaya/commits/9f43a4de9410a0ce2bfaf7fac32f3983ed241989))

## [6.11.4](https://bitbucket.org/thermsio/atalaya/compare/v6.11.3...v6.11.4) (2022-03-03)

### Bug Fixes

- bump version ([1ef5215](https://bitbucket.org/thermsio/atalaya/commits/1ef5215d25f24ed4a1fa145c22fb363c91885e4f))

## [6.11.3](https://bitbucket.org/thermsio/atalaya/compare/v6.11.2...v6.11.3) (2022-03-03)

### Bug Fixes

- **Select:** use optionValueExtractor when it is defined for onChangeValue() ([c4111e4](https://bitbucket.org/thermsio/atalaya/commits/c4111e41db95dc15f194b7aea087eb3d360e6336))

## [6.11.2](https://bitbucket.org/thermsio/atalaya/compare/v6.11.1...v6.11.2) (2022-03-01)

### Bug Fixes

- replace ReactElement with ReactNode ([2e3a2c1](https://bitbucket.org/thermsio/atalaya/commits/2e3a2c1ba54862af50029c8109d1e515cce701b8))

## [6.11.1](http://bitbucket.org/thermsio/atalaya/compare/v6.11.0...v6.11.1) (2022-03-01)

### Bug Fixes

- .md ref ([e1d734d](http://bitbucket.org/thermsio/atalaya/commits/e1d734ddea7c1a0a4bc15850286614fdd5b61909))

# [6.11.0](http://bitbucket.org/thermsio/atalaya/compare/v6.10.0...v6.11.0) (2022-03-01)

### Features

- **CORE-1926:** removed show prop from ActionModal.md component and Modal.md ([c0291d5](http://bitbucket.org/thermsio/atalaya/commits/c0291d529de4c63af5203b53762014290327c4c4))

# [6.10.0](http://bitbucket.org/thermsio/atalaya/compare/v6.9.5...v6.10.0) (2022-03-01)

### Features

- **CORE-1926:** remove shop prop from ActionModal.tsx component and Modal.fixture.tsx ([f7ee4e3](http://bitbucket.org/thermsio/atalaya/commits/f7ee4e3e8986901c1627270acd4aa6c790699871))
- **CORE-1926:** remove shop prop from Modal.tsx component ([7558c8c](http://bitbucket.org/thermsio/atalaya/commits/7558c8c52e53135caeeb58334a8e174fbd19d15b))

## [6.9.5](https://bitbucket.org/thermsio/atalaya/compare/v6.9.4...v6.9.5) (2022-02-27)

### Bug Fixes

- **DateTimePicker:** fire onChange on mount, add noInitialValue ([798117c](https://bitbucket.org/thermsio/atalaya/commits/798117c4bccc741ef0a711d9f9eb59fb2e0cd5d6))
- Radio wrap pop ([ec91d61](https://bitbucket.org/thermsio/atalaya/commits/ec91d61074d815d3a5ac64d8db4260f7a601c21e))

## [6.9.4](https://bitbucket.org/thermsio/atalaya/compare/v6.9.3...v6.9.4) (2022-02-25)

### Bug Fixes

- **CORE-1765:** replace p with div in toast ([c473f25](https://bitbucket.org/thermsio/atalaya/commits/c473f25049982f52ab00938ccdab4e3e68c565d7))
- **Stack:** pull wrap from classifyProps util to avoid console error ([289748f](https://bitbucket.org/thermsio/atalaya/commits/289748f8b7598bf0e7783f288076c4871291e184))

## [6.9.3](https://bitbucket.org/thermsio/atalaya/compare/v6.9.2...v6.9.3) (2022-02-25)

### Bug Fixes

- types ([b1e66ed](https://bitbucket.org/thermsio/atalaya/commits/b1e66ed9571db4a2e8831b81f4ea01898aafdc69))

## [6.9.2](https://bitbucket.org/thermsio/atalaya/compare/v6.9.1...v6.9.2) (2022-02-25)

### Bug Fixes

- types ([ee88d2b](https://bitbucket.org/thermsio/atalaya/commits/ee88d2ba3830895abab35ccb2b3aaac3ef5462b9))

## [6.9.1](https://bitbucket.org/thermsio/atalaya/compare/v6.9.0...v6.9.1) (2022-02-24)

### Bug Fixes

- remove border on Avatar ([be663b3](https://bitbucket.org/thermsio/atalaya/commits/be663b3b5812f1466525f25fa7758dfc508a2d23))

# [6.9.0](https://bitbucket.org/thermsio/atalaya/compare/v6.8.5...v6.9.0) (2022-02-24)

### Features

- **Avatar:** add xxs size ([86f17e3](https://bitbucket.org/thermsio/atalaya/commits/86f17e30a058e5b893b94d8cc5a2124ba01a86b8))

## [6.8.5](https://bitbucket.org/thermsio/atalaya/compare/v6.8.4...v6.8.5) (2022-02-24)

### Bug Fixes

- types ([ae0a302](https://bitbucket.org/thermsio/atalaya/commits/ae0a3021a7e5d005c4c4d0c4770cb29837b91313))

## [6.8.4](https://bitbucket.org/thermsio/atalaya/compare/v6.8.3...v6.8.4) (2022-02-23)

### Bug Fixes

- **FormSection:** spread FormLayoutContext isHorizontal value ([0756cdc](https://bitbucket.org/thermsio/atalaya/commits/0756cdc52b694c0b8d546e9d287520b8f62bc907))

## [6.8.3](https://bitbucket.org/thermsio/atalaya/compare/v6.8.2...v6.8.3) (2022-02-23)

### Bug Fixes

- package-lock ([b6decd5](https://bitbucket.org/thermsio/atalaya/commits/b6decd51434f7ff9661bfb12f95afdae3780b306))

## [6.8.2](https://bitbucket.org/thermsio/atalaya/compare/v6.8.1...v6.8.2) (2022-02-23)

### Bug Fixes

- no horizontal layout when no context FormControlsWrapper ([aa05507](https://bitbucket.org/thermsio/atalaya/commits/aa05507b8e1cb13799b64debc4cd4f83fcb9fcd7))

## [6.8.1](http://bitbucket.org/thermsio/atalaya/compare/v6.8.0...v6.8.1) (2022-02-22)

### Bug Fixes

- FormLayout dividers=space default ([0c40134](http://bitbucket.org/thermsio/atalaya/commits/0c401343909965d9ee0c5500f1c0de081af319ed))

# [6.8.0](http://bitbucket.org/thermsio/atalaya/compare/v6.7.0...v6.8.0) (2022-02-22)

### Features

- **Avatar:** add 2xl size ([16856ba](http://bitbucket.org/thermsio/atalaya/commits/16856ba36673cb1a4ad20529e8fdb417f141de91))

# [6.7.0](http://bitbucket.org/thermsio/atalaya/compare/v6.6.0...v6.7.0) (2022-02-21)

### Features

- **Modal:** add ModalContext and utils ([eee5f7b](http://bitbucket.org/thermsio/atalaya/commits/eee5f7bdc76b2994f81775d59e19651e3fe7ae2e))

# [6.6.0](http://bitbucket.org/thermsio/atalaya/compare/v6.5.0...v6.6.0) (2022-02-18)

### Features

- ContextMenu, make children type more flexible ([820a67a](http://bitbucket.org/thermsio/atalaya/commits/820a67ab21c202d2f04e28daa969a8e39f54e34a))

# [6.5.0](http://bitbucket.org/thermsio/atalaya/compare/v6.4.2...v6.5.0) (2022-02-17)

### Features

- Pagination, improve selected page visual feedback; ([d85d4c1](http://bitbucket.org/thermsio/atalaya/commits/d85d4c193d0f50ffb3b47be6379a4c4a011e6605))

## [6.4.2](http://bitbucket.org/thermsio/atalaya/compare/v6.4.1...v6.4.2) (2022-02-17)

### Bug Fixes

- Pagination, add a default to recordsPerPage; ([1c864b3](http://bitbucket.org/thermsio/atalaya/commits/1c864b3883f5ce70af4bcdefe1e36e5dc33aa20d))

## [6.4.1](http://bitbucket.org/thermsio/atalaya/compare/v6.4.0...v6.4.1) (2022-02-17)

### Bug Fixes

- Avatar, background stretching on flex containers; ([a2e4487](http://bitbucket.org/thermsio/atalaya/commits/a2e4487da7b25d7c9cb9135a195b643e102a52f5))

# [6.4.0](http://bitbucket.org/thermsio/atalaya/compare/v6.3.2...v6.4.0) (2022-02-16)

### Bug Fixes

- fix cosmos missing some basic stylings; ([8c485cb](http://bitbucket.org/thermsio/atalaya/commits/8c485cbfa75bd21e5b2a48aa28bc279b7c865bc3))

### Features

- Avatar, change object fit prop to cover; ([92ff166](http://bitbucket.org/thermsio/atalaya/commits/92ff16671f53b2aa83b2dff6b5f01b24b2a14166))

## [6.3.2](http://bitbucket.org/thermsio/atalaya/compare/v6.3.1...v6.3.2) (2022-02-14)

### Bug Fixes

- add css to tailwind content ([ad7a72d](http://bitbucket.org/thermsio/atalaya/commits/ad7a72dd307dffa5181fc6690bd9a38920c3aad4))

## [6.3.1](http://bitbucket.org/thermsio/atalaya/compare/v6.3.0...v6.3.1) (2022-02-14)

### Bug Fixes

- Pagination, do not take extra space when pagination is hidden and above description. ([4b814e1](http://bitbucket.org/thermsio/atalaya/commits/4b814e1900b68fd3b997e2c33862cf881d524d56))
- Pagination, fix width changing when scrolling between single and double-digit pages. ([93848bb](http://bitbucket.org/thermsio/atalaya/commits/93848bb5907daa65bde637f60badb2ca878b3105))

# [6.3.0](http://bitbucket.org/thermsio/atalaya/compare/v6.2.1...v6.3.0) (2022-02-14)

### Features

- FormWrapper, Do not render heading if no content and layout is vertical; Solves extra margin issues ([b97dfd4](http://bitbucket.org/thermsio/atalaya/commits/b97dfd4015c25beeb453b4a9a0b947a0ca09f5a6))

## [6.2.1](http://bitbucket.org/thermsio/atalaya/compare/v6.2.0...v6.2.1) (2022-02-12)

### Bug Fixes

- update deps, tailwind@3.0.22 ([39862ca](http://bitbucket.org/thermsio/atalaya/commits/39862ca40a94dad4b2007e056a6b76507c09530d))

# [6.2.0](http://bitbucket.org/thermsio/atalaya/compare/v6.1.0...v6.2.0) (2022-02-12)

### Features

- FormWrapper, move hint text below label; ([e1bb8cd](http://bitbucket.org/thermsio/atalaya/commits/e1bb8cda411b86ec0017096c941a2d7ef72fad78))

# [6.1.0](http://bitbucket.org/thermsio/atalaya/compare/v6.0.0...v6.1.0) (2022-02-11)

### Features

- add a commit with feat to force publish to npm; ([5e2fa62](http://bitbucket.org/thermsio/atalaya/commits/5e2fa62fe07ddb7c282433fca2bbfeb6d2868fe5))

# [6.0.0](http://bitbucket.org/thermsio/atalaya/compare/v5.27.5...v6.0.0) (2022-02-11)

### Bug Fixes

- documentation examples not having proper styles; ([00bcdd8](http://bitbucket.org/thermsio/atalaya/commits/00bcdd87cdff6cbc7d8981986c7b14f17940ad4d))
- FormContorls, tailwind forms plugins overriding custom styles. ([b605256](http://bitbucket.org/thermsio/atalaya/commits/b60525646ea0f4420559554ae5c5f8daea0975fa))
- stop tailwind styles from being overwritten by consuming apps also using tailwind; ([e9d919d](http://bitbucket.org/thermsio/atalaya/commits/e9d919d66dfab0efbc21b1a65ab9ab2754d949ad))

### Features

- add 0 to spacing types; ([f21a403](http://bitbucket.org/thermsio/atalaya/commits/f21a403b60d41a0f1bb949e516cdcb426735dc48))
- Badge, reduce border radius; ([9d4268b](http://bitbucket.org/thermsio/atalaya/commits/9d4268b91bb6935c1ea6875535cbf512f8ffe481))

### BREAKING CHANGES

- tailwind.js has been removed. Import tailwind.config.js as a preset instead.

## [5.27.5](http://bitbucket.org/thermsio/atalaya/compare/v5.27.4...v5.27.5) (2022-02-10)

### Bug Fixes

- deprecation warnings ([612db42](http://bitbucket.org/thermsio/atalaya/commits/612db429c209c680fa0d7c8b5df1c4bbbac3e027))

## [5.27.4](http://bitbucket.org/thermsio/atalaya/compare/v5.27.3...v5.27.4) (2022-02-09)

### Bug Fixes

- **CORE-1844:** fix tabs content not showing on mobile views; ([6a221ca](http://bitbucket.org/thermsio/atalaya/commits/6a221caf11ef67dacebd983f9c1a1666c2f2aa90))
- **CORE-1844:** fix tabs not turning into select inside a flex container; ([79225bb](http://bitbucket.org/thermsio/atalaya/commits/79225bb9f1748627f6ebc50db335d60c2e77eb6f))

## [5.27.3](http://bitbucket.org/thermsio/atalaya/compare/v5.27.2...v5.27.3) (2022-02-07)

### Bug Fixes

- **CORE-1844:** section heading divider and improve visual distinction between sections; ([dea77c5](http://bitbucket.org/thermsio/atalaya/commits/dea77c56c4ae6dfe83b9f26119d8c1998d96817d))

## [5.27.2](http://bitbucket.org/thermsio/atalaya/compare/v5.27.1...v5.27.2) (2022-02-07)

### Bug Fixes

- **CORE-1844:** improve FormLayout Loading; ([7e0e4f7](http://bitbucket.org/thermsio/atalaya/commits/7e0e4f757c3e09bab7823f1d13150d3470405318))

## [5.27.1](http://bitbucket.org/thermsio/atalaya/compare/v5.27.0...v5.27.1) (2022-02-07)

### Bug Fixes

- **CORE-1844:** fix FormLayout dividers not showing on after section; ([1f79819](http://bitbucket.org/thermsio/atalaya/commits/1f79819b86f48205260c4b02f71a9c43e8d62706))

# [5.27.0](http://bitbucket.org/thermsio/atalaya/compare/v5.26.0...v5.27.0) (2022-02-04)

### Features

- **TableGroupe:** add TableCell ellipse text better than tailwind's jenky ass implementation ([336bf7d](http://bitbucket.org/thermsio/atalaya/commits/336bf7dd9f60cb9795efa248245dd90ea248328e))

# [5.26.0](http://bitbucket.org/thermsio/atalaya/compare/v5.25.1...v5.26.0) (2022-02-04)

### Features

- **TableGroup:** add .breakword css class util and TableCell component accept className prop ([4ea48d3](http://bitbucket.org/thermsio/atalaya/commits/4ea48d3bc7990797bd18689b598fcc59a6207e29))

## [5.25.1](http://bitbucket.org/thermsio/atalaya/compare/v5.25.0...v5.25.1) (2022-02-04)

### Bug Fixes

- revert external css build ([9fc42a7](http://bitbucket.org/thermsio/atalaya/commits/9fc42a737c8ba6eb29a7fb8590f45bba01942cae))

# [5.25.0](http://bitbucket.org/thermsio/atalaya/compare/v5.24.2...v5.25.0) (2022-02-04)

### Features

- refactor build to extract CSS styles BREAKING-CHANGE requires direct import of css in project that is using Atalaya ([47089f5](http://bitbucket.org/thermsio/atalaya/commits/47089f583ff975c24193e1b7046c8356c4a3b61d))

## [5.24.2](http://bitbucket.org/thermsio/atalaya/compare/v5.24.1...v5.24.2) (2022-02-04)

### Bug Fixes

- **CORE-1830:** fix missing modal padding; ([98acb84](http://bitbucket.org/thermsio/atalaya/commits/98acb8485491210df0b37ad0875a22117b6365cc))

## [5.24.1](http://bitbucket.org/thermsio/atalaya/compare/v5.24.0...v5.24.1) (2022-02-04)

### Bug Fixes

- **CORE-1830:** fix modal sizes; ([c4052d0](http://bitbucket.org/thermsio/atalaya/commits/c4052d05bb7f4e02a6bc2fbfaf363afa41e6e5c5))

# [5.24.0](http://bitbucket.org/thermsio/atalaya/compare/v5.23.4...v5.24.0) (2022-02-04)

### Features

- **TableGroup:** allow React.FC or ReactElement as props in TableGroup ([0718ebb](http://bitbucket.org/thermsio/atalaya/commits/0718ebb7f0bc5e5366e10db1e15fcac4c860cbd5))

## [5.23.4](http://bitbucket.org/thermsio/atalaya/compare/v5.23.3...v5.23.4) (2022-02-04)

### Bug Fixes

- **TableGroup:** remove whitespace-nowrap class from table cells that caused horizontal issues CORE-1786 ([35259a2](http://bitbucket.org/thermsio/atalaya/commits/35259a2b10b525625000c09db5767e2b325b4fa0))

## [5.23.3](http://bitbucket.org/thermsio/atalaya/compare/v5.23.2...v5.23.3) (2022-02-03)

### Bug Fixes

- tabs default tabId ([14bda9d](http://bitbucket.org/thermsio/atalaya/commits/14bda9ddedee0038ec9c25a7d06b6e2527173d46))

## [5.23.2](http://bitbucket.org/thermsio/atalaya/compare/v5.23.1...v5.23.2) (2022-02-03)

### Bug Fixes

- FormLayout.Controls, don't show loading on cancel button; ([c34461d](http://bitbucket.org/thermsio/atalaya/commits/c34461d10fdb6152f811aaa377991f1482a5c406))
- FormLayout.Controls, prop change from isSubmitting to submitting; ([9a2c9fc](http://bitbucket.org/thermsio/atalaya/commits/9a2c9fcedb3ae5cba65eb812a701bdc35ba6850b))

## [5.23.1](http://bitbucket.org/thermsio/atalaya/compare/v5.23.0...v5.23.1) (2022-02-03)

### Bug Fixes

- FormLayout.Controls, on submit, only show loading on submit button, disable all the rest; ([a63d370](http://bitbucket.org/thermsio/atalaya/commits/a63d370d83b345a029c85e5505ac158e5d46435e))

# [5.23.0](http://bitbucket.org/thermsio/atalaya/compare/v5.22.3...v5.23.0) (2022-02-02)

### Features

- FormLayout.Controls, deprecate isSaving on favour of isSubmitting; ([c37717b](http://bitbucket.org/thermsio/atalaya/commits/c37717beb3f225058e31c5b00efdfdd12a38a844))

## [5.22.3](http://bitbucket.org/thermsio/atalaya/compare/v5.22.2...v5.22.3) (2022-02-02)

### Bug Fixes

- FormLayout, horizontal layout flicker when loading directly in stacked layout; ([6394cf8](http://bitbucket.org/thermsio/atalaya/commits/6394cf83b9471c9312d2bb832f9f650c74a0dd76))

## [5.22.2](http://bitbucket.org/thermsio/atalaya/compare/v5.22.1...v5.22.2) (2022-02-02)

### Bug Fixes

- FormControl, revert capitalization of error messages; ([57e3780](http://bitbucket.org/thermsio/atalaya/commits/57e3780048c5956406c9b8e1a5ce9a335f9e096e))

## [5.22.1](http://bitbucket.org/thermsio/atalaya/compare/v5.22.0...v5.22.1) (2022-02-01)

### Bug Fixes

- Tabs title props accept ReactElement & export IToastManagerOptions ([d99f2bd](http://bitbucket.org/thermsio/atalaya/commits/d99f2bde2f62e4c1b1afc03a84d603858be0b0c7))

# [5.22.0](http://bitbucket.org/thermsio/atalaya/compare/v5.21.0...v5.22.0) (2022-02-01)

### Features

- FormControl, capitalize error messages; ([0f75ca0](http://bitbucket.org/thermsio/atalaya/commits/0f75ca02adb049c7d52a228e51dd7d832fa097e7))

# [5.21.0](http://bitbucket.org/thermsio/atalaya/compare/v5.20.0...v5.21.0) (2022-02-01)

### Features

- Inputs, remove Field from input names; ([267e36b](http://bitbucket.org/thermsio/atalaya/commits/267e36b6ac4e2199ff95f3d3a45307a4a81fd8ae))

# [5.20.0](http://bitbucket.org/thermsio/atalaya/compare/v5.19.1...v5.20.0) (2022-02-01)

### Features

- **CORE-1609:** add placeholder options; ([fa2b31d](http://bitbucket.org/thermsio/atalaya/commits/fa2b31dbd17b9536cf7b334bca610c754ae36753))
- **CORE-1609:** Icon, add error display and retry logic; ([c1b7aa4](http://bitbucket.org/thermsio/atalaya/commits/c1b7aa4bdc6ad0f7ea0472f419f95825138fd5e5))
- **CORE-1764:** Image, remove white outline when skeleton is being displayed; ([a3e5469](http://bitbucket.org/thermsio/atalaya/commits/a3e54691bfd46a0cfe29aeaffd546c7ff9087d79))
- Icon, rename keepSpot to preserveSpace; ([94417a3](http://bitbucket.org/thermsio/atalaya/commits/94417a36ddfe46a85e70e665c3a94da14cf18723))

## [5.19.1](http://bitbucket.org/thermsio/atalaya/compare/v5.19.0...v5.19.1) (2022-01-29)

### Bug Fixes

- shit, need a version bump 💩🍔 ([7693ad8](http://bitbucket.org/thermsio/atalaya/commits/7693ad8c06bf5ee54961d54ad66bccc65987b585))

# [5.19.0](http://bitbucket.org/thermsio/atalaya/compare/v5.18.1...v5.19.0) (2022-01-28)

### Features

- Icon, set display as inline; ([4ce9fd1](http://bitbucket.org/thermsio/atalaya/commits/4ce9fd19f25badbf2f69b70b928e5019f3524fa0))

## [5.18.1](http://bitbucket.org/thermsio/atalaya/compare/v5.18.0...v5.18.1) (2022-01-27)

### Bug Fixes

- **Select:** add placeholder prop pass-through ([b26b5cf](http://bitbucket.org/thermsio/atalaya/commits/b26b5cf096fd75f93c4380e92204cbe82af9cf54))

# [5.18.0](http://bitbucket.org/thermsio/atalaya/compare/v5.17.1...v5.18.0) (2022-01-26)

### Features

- **CORE-1794:** Update FormLayout.Section docs after changing the default value of horizontal; ([898c4cf](http://bitbucket.org/thermsio/atalaya/commits/898c4cfe3a30b32be0dac7dbff42174bafed1e85))

## [5.17.1](http://bitbucket.org/thermsio/atalaya/compare/v5.17.0...v5.17.1) (2022-01-26)

### Bug Fixes

- **CORE-1794:** default horizontal to true on FormLayout.Section; ([c86be3d](http://bitbucket.org/thermsio/atalaya/commits/c86be3dfff3efefbe2ce92a39794df61e0b4a011))

# [5.17.0](http://bitbucket.org/thermsio/atalaya/compare/v5.16.0...v5.17.0) (2022-01-25)

### Features

- Tweak light theme color values. ([711c137](http://bitbucket.org/thermsio/atalaya/commits/711c1374c61f9f3d63d7628af95db231a127988c))

# [5.16.0](http://bitbucket.org/thermsio/atalaya/compare/v5.15.1...v5.16.0) (2022-01-25)

### Bug Fixes

- FormLayout.FormControls being fullWidth on horizontal. Again. ([c43c39e](http://bitbucket.org/thermsio/atalaya/commits/c43c39e7f17c75c1936836a7bcbe29b8399dc06d))
- FormLayout.FormControls fix buttons being fullWidth on horizontal view ([fb3d799](http://bitbucket.org/thermsio/atalaya/commits/fb3d799f5075d7d5db4d08995d81d5e5da3a513e))

### Features

- **CORE-1794 CORE-1793:** Deprecate horizontal prop. ([f672ed3](http://bitbucket.org/thermsio/atalaya/commits/f672ed3104181fc14555d94238fcf678351d7a6a))
- **CORE-1795:** FormControlWrapper, improve mobile layout adaptability. ([2adc3dc](http://bitbucket.org/thermsio/atalaya/commits/2adc3dc47cb12ec6e0952e2af7495317a6d599ca))
- **CORE-1795:** FormLayout, stop tracking height changes. ([f7b0e2e](http://bitbucket.org/thermsio/atalaya/commits/f7b0e2e9b6d19f2be16805e5d4a834fd4030801f))

## [5.15.1](http://bitbucket.org/thermsio/atalaya/compare/v5.15.0...v5.15.1) (2022-01-22)

### Bug Fixes

- **DateTimePicker:** format ISODateString onChange ([84b6e75](http://bitbucket.org/thermsio/atalaya/commits/84b6e757dcb7c8e8b6092cabf108a3263eddd342))

# [5.15.0](http://bitbucket.org/thermsio/atalaya/compare/v5.14.0...v5.15.0) (2022-01-21)

### Features

- actually export SlidePanel ([8e6a27c](http://bitbucket.org/thermsio/atalaya/commits/8e6a27c8d28981c4a9f8772a29890128c304f0dd))

# [5.14.0](http://bitbucket.org/thermsio/atalaya/compare/v5.13.0...v5.14.0) (2022-01-21)

### Features

- input Text, forwardRef to the underlying input; ([8d3214e](http://bitbucket.org/thermsio/atalaya/commits/8d3214e56a01e4622cf49591f149e4efcf27015e))

# [5.13.0](http://bitbucket.org/thermsio/atalaya/compare/v5.12.0...v5.13.0) (2022-01-21)

### Features

- **CORE-1668:** SlidePanel.fixture move timeout to useEffect; ([6277001](http://bitbucket.org/thermsio/atalaya/commits/6277001b5de53a76c669d8ace39d3cb86a4a24f7))
- **CORE-1763:** add SlidePanel; ([8b6975c](http://bitbucket.org/thermsio/atalaya/commits/8b6975ca6a45d5f9ef2a6d905e0a50f1812251a8))
- **CORE-1763:** remove extra padding on cosmos decorator; ([efba388](http://bitbucket.org/thermsio/atalaya/commits/efba388723152b193280a24915581a37f50965ae))

# [5.12.0](http://bitbucket.org/thermsio/atalaya/compare/v5.11.0...v5.12.0) (2022-01-17)

### Features

- **CORE-1763:** icons will now occupy their place even if no svg is provided, add keepSpot prop to turn the behaviour off; ([07e4b84](http://bitbucket.org/thermsio/atalaya/commits/07e4b84aa3f6aa4a8a3411d0c6ae1d7f1729afa9))

# [5.11.0](http://bitbucket.org/thermsio/atalaya/compare/v5.10.8...v5.11.0) (2022-01-14)

### Features

- **CORE-1763:** Icon, export IconProps. ([2952a52](http://bitbucket.org/thermsio/atalaya/commits/2952a523c22af239a385315fb55ffa78a2cc4c43))

## [5.10.8](http://bitbucket.org/thermsio/atalaya/compare/v5.10.7...v5.10.8) (2022-01-10)

### Bug Fixes

- **CORE-1763:** FormControlWrapper fix taking up space for label cell even if no label is provided. ([326af3f](http://bitbucket.org/thermsio/atalaya/commits/326af3f1a150fc703ce5dae71e315bd4757efff1))
- **CORE-1763:** FormSection horizontal jumping from vertical to horizontal on first render. ([e579fe1](http://bitbucket.org/thermsio/atalaya/commits/e579fe1feba4b75b653cf3bb9fc29f7593a1e7c4))

## [5.10.7](http://bitbucket.org/thermsio/atalaya/compare/v5.10.6...v5.10.7) (2022-01-10)

### Bug Fixes

- **FormLayout.Controls:** only disable cancel btn when saving ([0aa97c3](http://bitbucket.org/thermsio/atalaya/commits/0aa97c37e0c17ade530505d4b15d630cd4ce06e3))

## [5.10.6](http://bitbucket.org/thermsio/atalaya/compare/v5.10.5...v5.10.6) (2022-01-09)

### Bug Fixes

- **Select:** onBlur passes inputId for Formik compat ([dfc6bb1](http://bitbucket.org/thermsio/atalaya/commits/dfc6bb1f1865624d6dbd0cb3ce44f80d8e948e26))

## [5.10.5](http://bitbucket.org/thermsio/atalaya/compare/v5.10.4...v5.10.5) (2022-01-09)

### Bug Fixes

- **Tabs:** margin on tab container children ([01d3ee6](http://bitbucket.org/thermsio/atalaya/commits/01d3ee69703a032762533dd3fdfb98a325ba42c3))

## [5.10.4](http://bitbucket.org/thermsio/atalaya/compare/v5.10.3...v5.10.4) (2022-01-09)

### Bug Fixes

- pass mock react synthetic events for ColorPicker and DateTimePicker ([cce41f6](http://bitbucket.org/thermsio/atalaya/commits/cce41f617eb8be86a0a924910fb0ba7296e8a4b7))

## [5.10.3](http://bitbucket.org/thermsio/atalaya/compare/v5.10.2...v5.10.3) (2022-01-08)

### Bug Fixes

- **Tabs:** refactor TabsLayout component to fix bug with unmounting <Tab /> components ([6908acc](http://bitbucket.org/thermsio/atalaya/commits/6908acc96deed2080431cbb697fe9d1fbc7a1b4e))

## [5.10.2](http://bitbucket.org/thermsio/atalaya/compare/v5.10.1...v5.10.2) (2022-01-08)

### Bug Fixes

- form field components add onBlur onFocus callbacks to non-standard inputs ([d0f55be](http://bitbucket.org/thermsio/atalaya/commits/d0f55be97c24766acf7d2ca8c7d60389f54790e9))

## [5.10.1](http://bitbucket.org/thermsio/atalaya/compare/v5.10.0...v5.10.1) (2022-01-07)

### Bug Fixes

- **Tabs:** active-tab use title when no tab key exists ([8c5ac8d](http://bitbucket.org/thermsio/atalaya/commits/8c5ac8d1953df64c1e0499e05c912f873667f97e))

# [5.10.0](http://bitbucket.org/thermsio/atalaya/compare/v5.9.0...v5.10.0) (2022-01-07)

### Features

- **ListCreator:** add sorting to the options list with drag-and-drop ([60788d5](http://bitbucket.org/thermsio/atalaya/commits/60788d5dc194f49b828889e6cbba2c0bc25f1688))

# [5.9.0](http://bitbucket.org/thermsio/atalaya/compare/v5.8.2...v5.9.0) (2022-01-07)

### Features

- **Layout:** add data-[layout] component name attr to <div />'s for easier HTML debugging ([94d6bdf](http://bitbucket.org/thermsio/atalaya/commits/94d6bdf808371848e96a353b5e8b9aed332272f0))

## [5.8.2](http://bitbucket.org/thermsio/atalaya/compare/v5.8.1...v5.8.2) (2022-01-07)

### Bug Fixes

- **Radio:** add wrap prop to inline to prevent options from overflowing ([4f7c822](http://bitbucket.org/thermsio/atalaya/commits/4f7c822b678678fd1133de4cdd793fe7b322647e))

## [5.8.1](http://bitbucket.org/thermsio/atalaya/compare/v5.8.0...v5.8.1) (2022-01-07)

### Bug Fixes

- **Select:** show selected value as an option when it does not exist in options list ([e585275](http://bitbucket.org/thermsio/atalaya/commits/e5852755dbc85463427e288a9cd9ad8139582b0c))

# [5.8.0](http://bitbucket.org/thermsio/atalaya/compare/v5.7.1...v5.8.0) (2022-01-07)

### Features

- **Select:** allow creating new options in the <Select /> component ([13b6603](http://bitbucket.org/thermsio/atalaya/commits/13b6603a5a1bcc3de071c8fca65e9528a2beba4b))

## [5.7.1](http://bitbucket.org/thermsio/atalaya/compare/v5.7.0...v5.7.1) (2022-01-07)

### Bug Fixes

- **DateTimePicker:** respect 'disabled' prop ([65e2eca](http://bitbucket.org/thermsio/atalaya/commits/65e2ecaf1478b5d3f0dc4ca7a4bde9973d0bb392))

# [5.7.0](http://bitbucket.org/thermsio/atalaya/compare/v5.6.0...v5.7.0) (2022-01-07)

### Features

- **Checkbox:** default label id ([c5c2a32](http://bitbucket.org/thermsio/atalaya/commits/c5c2a320d151ab715cbb8f488cad1973f94daeea))

# [5.6.0](http://bitbucket.org/thermsio/atalaya/compare/v5.5.0...v5.6.0) (2022-01-07)

### Features

- **SkeletonText:** add inline prop for multiple inline Skeleton text components ([60cbf54](http://bitbucket.org/thermsio/atalaya/commits/60cbf543880b772958d2e1751c62bdd2216ae9a5))

# [5.5.0](http://bitbucket.org/thermsio/atalaya/compare/v5.4.0...v5.5.0) (2022-01-06)

### Features

- Dropdowns, use surface-subtle as background color; ([381b46e](http://bitbucket.org/thermsio/atalaya/commits/381b46eaecb3c92a83dc29b8f89c888f93f95603))

# [5.4.0](http://bitbucket.org/thermsio/atalaya/compare/v5.3.0...v5.4.0) (2022-01-05)

### Features

- **useIsBreakpointActive:** add hook to exports ([6b26f28](http://bitbucket.org/thermsio/atalaya/commits/6b26f28dbda813df25009451763a5803740788fb))

# [5.3.0](http://bitbucket.org/thermsio/atalaya/compare/v5.2.3...v5.3.0) (2022-01-04)

### Bug Fixes

- **CORE-1773:** fix pipeline breaking; ([d96a7d3](http://bitbucket.org/thermsio/atalaya/commits/d96a7d3e9b21c87e1ae1a67d2f4f5c59f14217fe))

### Features

- **CORE-1773:** add url prop; ([92cb3ab](http://bitbucket.org/thermsio/atalaya/commits/92cb3abe502e1d38f639c44b7ea793f048661881))
- **CORE-1773:** fix flipX/Y not working; ([bbcce11](http://bitbucket.org/thermsio/atalaya/commits/bbcce1126ed1f53e40228d1ec623c5204d447ff9))
- **CORE-1773:** fix package-lock conflicts; ([b9be75a](http://bitbucket.org/thermsio/atalaya/commits/b9be75af0445241827bcccd3547e8de5b8e34cc3))
- **CORE-1773:** remove console.log; ([1503c50](http://bitbucket.org/thermsio/atalaya/commits/1503c508c93ca7fbb2898c658417ac36d8331e16))

## [5.2.3](http://bitbucket.org/thermsio/atalaya/compare/v5.2.2...v5.2.3) (2022-01-03)

### Bug Fixes

- **ListCreator:** add prop to text input for search field type ([427e4a2](http://bitbucket.org/thermsio/atalaya/commits/427e4a2e24e32c528499deeca3bb7f1c5bad6fc3))

## [5.2.2](http://bitbucket.org/thermsio/atalaya/compare/v5.2.1...v5.2.2) (2022-01-03)

### Bug Fixes

- **ListCreator:** options sort logic ([6506aaa](http://bitbucket.org/thermsio/atalaya/commits/6506aaa841a9e764eec9cdd54c018633b1f9d3f1))

## [5.2.1](http://bitbucket.org/thermsio/atalaya/compare/v5.2.0...v5.2.1) (2022-01-03)

### Bug Fixes

- **Radio:** make RadioOption.value optional and default to use the .label prop ([2a74c8f](http://bitbucket.org/thermsio/atalaya/commits/2a74c8fbfb668f86804b6760c9684cd9a4f80be8))

# [5.2.0](http://bitbucket.org/thermsio/atalaya/compare/v5.1.0...v5.2.0) (2022-01-03)

### Features

- **ListCreator:** add new <ListCreator /> component ([f4a5f39](http://bitbucket.org/thermsio/atalaya/commits/f4a5f397ed3293818b75159f694ab8cd7ad6d2e9))

# [5.1.0](http://bitbucket.org/thermsio/atalaya/compare/v5.0.0...v5.1.0) (2022-01-02)

### Features

- **Text:** add search prop ([e2c8cd4](http://bitbucket.org/thermsio/atalaya/commits/e2c8cd428672143b6b8ff6b990c9a5a5147b3374))
- **TextWithSuggestions:** stubbed out using <Text /> component ([32dac5a](http://bitbucket.org/thermsio/atalaya/commits/32dac5a65b76193e932c6ed54b6447587f2db79c))

# [5.0.0](http://bitbucket.org/thermsio/atalaya/compare/v4.0.0...v5.0.0) (2022-01-02)

### Bug Fixes

- build:dev script calling error ([98a6622](http://bitbucket.org/thermsio/atalaya/commits/98a6622196f6f0e5758ab15b23dcd6655a0f4be8))
- **Checkbox:** onChangeValue(active: boolean) ([b8a536f](http://bitbucket.org/thermsio/atalaya/commits/b8a536f2b4cbafe3e7ea7d386eb7f91a80979708))
- **CORE-1760:** make disabled tabs more distinguishable; ([af4e08c](http://bitbucket.org/thermsio/atalaya/commits/af4e08c9122aadd810e1965f73e14fb667ceb089))
- export SelectProps ([1086ec4](http://bitbucket.org/thermsio/atalaya/commits/1086ec4b7746d647dbf69862d8df7098330c9b9c))
- provide the remaining props that were missing after we stopped passing ...rest props to react-select; ([0f48a59](http://bitbucket.org/thermsio/atalaya/commits/0f48a59dbbeb9da821194c46cb38f9a9eb456d94))
- **Select:** onChangeValue check if array ([8bf2a8e](http://bitbucket.org/thermsio/atalaya/commits/8bf2a8efb33dc97ab6a2337cb61999c53c39e6ef))
- TagGroup, remove default fixed max size; ([c95d8a0](http://bitbucket.org/thermsio/atalaya/commits/c95d8a0f96c364125a808b873d0ddc3b0c2c2eac))

### Features

- **CORE-1675:** Select, add spacing between Selected Multi Options ([612cbbb](http://bitbucket.org/thermsio/atalaya/commits/612cbbb65e1cad79ef82275478041f87271ba65e))
- **CORE-1675:** stop menu opening when clicking multi select item; ([3a2278a](http://bitbucket.org/thermsio/atalaya/commits/3a2278aabb7263aab58e5ffa64ac2d2c13a9f4f1))
- **CORE-1737:** tweak Avatar look; ([63432e7](http://bitbucket.org/thermsio/atalaya/commits/63432e7b59e0d6a196119013194ef92c74a0ac44))
- **CORE-1760:** add Tab children to display content WIP; ([cfc813f](http://bitbucket.org/thermsio/atalaya/commits/cfc813f84d24b8811603055c449302e2ac419c7b))
- **CORE-1760:** add Tab children to display content; ([bf75d01](http://bitbucket.org/thermsio/atalaya/commits/bf75d01081f2abc3ecf3c57be569aab2c77591d3))
- **CORE-1760:** add Tab children to display content; ([ca615f0](http://bitbucket.org/thermsio/atalaya/commits/ca615f0d128b35d494b615a41ed2e52889ed8737))
- **CORE-1760:** Complete refactor ot Tabs component; ([47162f6](http://bitbucket.org/thermsio/atalaya/commits/47162f67d3a34c03ebfd3fb6f1026a40144cc5c9))
- **CORE-1760:** Select, add isOptionDisabled; ([23215e1](http://bitbucket.org/thermsio/atalaya/commits/23215e17b30246a200d9e19936ddd79b3d6eff56))
- **CORE-1760:** Tab, add disabled prop; ([8621fca](http://bitbucket.org/thermsio/atalaya/commits/8621fcad6a741312f53c642ae2048e07cf1f674f))
- **CORE-1760:** Tabs, replace alignX with align and fullWidth props; ([c2135a3](http://bitbucket.org/thermsio/atalaya/commits/c2135a3dc8abe684df0d5eb663378f28c1bfc063))
- **CORE-1760:** total refactor of Tabs WIP; ([978c7d1](http://bitbucket.org/thermsio/atalaya/commits/978c7d103de799fe08a00d89e492d7a886179591))
- **CORE-1760:** update children comment; ([1d46dfc](http://bitbucket.org/thermsio/atalaya/commits/1d46dfcf099f4bf3c24c7f4ab1fb3ea2e6e64a91))
- **CORE-1760:** use tabKey instead of tabIndex; ([a6ff5a8](http://bitbucket.org/thermsio/atalaya/commits/a6ff5a84b58c37ade99475fe9f6a349731be3637))
- **Radio:** add value prop to use as an alternative to 'activeIndex' ([993ee11](http://bitbucket.org/thermsio/atalaya/commits/993ee11a3f5338bc9931efd79b8eb7b4b52dd0b7))
- **Radio:** add vertical prop ([e9f2e46](http://bitbucket.org/thermsio/atalaya/commits/e9f2e467558d608afd6c321a517f8ba85913988e))
- Select, stop accepting extra react-select specific props. ([46f5fca](http://bitbucket.org/thermsio/atalaya/commits/46f5fca8d7f8f6813982f59c98666482d4984caa))
- **Select:** add onChangeOption, improve SelectProps interface types and generics ([185ba4c](http://bitbucket.org/thermsio/atalaya/commits/185ba4cdc00ea315a3d1129f8ba372427dc162c5))
- Tag, add ability to set custom colors; ([0b59079](http://bitbucket.org/thermsio/atalaya/commits/0b59079fbcca665e5c13f7add99643258c7b18ce))
- update Tabs to work with refactored Select ([8499195](http://bitbucket.org/thermsio/atalaya/commits/8499195fdb0fdc2c9a51d3d5209c3a4ecb7d2d1d))

### BREAKING CHANGES

- **CORE-1760:** Tabs alignX prop is no longer supported. Use align=left|right and fullWidth props now instead.
- Select no long passes extra props to react-select.

# [5.0.0-beta.12](http://bitbucket.org/thermsio/atalaya/compare/v5.0.0-beta.11...v5.0.0-beta.12) (2022-01-02)

### Bug Fixes

- **CORE-1760:** make disabled tabs more distinguishable; ([af4e08c](http://bitbucket.org/thermsio/atalaya/commits/af4e08c9122aadd810e1965f73e14fb667ceb089))

### Features

- **CORE-1760:** add Tab children to display content WIP; ([cfc813f](http://bitbucket.org/thermsio/atalaya/commits/cfc813f84d24b8811603055c449302e2ac419c7b))
- **CORE-1760:** add Tab children to display content; ([bf75d01](http://bitbucket.org/thermsio/atalaya/commits/bf75d01081f2abc3ecf3c57be569aab2c77591d3))
- **CORE-1760:** add Tab children to display content; ([ca615f0](http://bitbucket.org/thermsio/atalaya/commits/ca615f0d128b35d494b615a41ed2e52889ed8737))
- **CORE-1760:** Complete refactor ot Tabs component; ([47162f6](http://bitbucket.org/thermsio/atalaya/commits/47162f67d3a34c03ebfd3fb6f1026a40144cc5c9))
- **CORE-1760:** Select, add isOptionDisabled; ([23215e1](http://bitbucket.org/thermsio/atalaya/commits/23215e17b30246a200d9e19936ddd79b3d6eff56))
- **CORE-1760:** Tab, add disabled prop; ([8621fca](http://bitbucket.org/thermsio/atalaya/commits/8621fcad6a741312f53c642ae2048e07cf1f674f))
- **CORE-1760:** Tabs, replace alignX with align and fullWidth props; ([c2135a3](http://bitbucket.org/thermsio/atalaya/commits/c2135a3dc8abe684df0d5eb663378f28c1bfc063))
- **CORE-1760:** total refactor of Tabs WIP; ([978c7d1](http://bitbucket.org/thermsio/atalaya/commits/978c7d103de799fe08a00d89e492d7a886179591))
- **CORE-1760:** update children comment; ([1d46dfc](http://bitbucket.org/thermsio/atalaya/commits/1d46dfcf099f4bf3c24c7f4ab1fb3ea2e6e64a91))
- **CORE-1760:** use tabKey instead of tabIndex; ([a6ff5a8](http://bitbucket.org/thermsio/atalaya/commits/a6ff5a84b58c37ade99475fe9f6a349731be3637))

### BREAKING CHANGES

- **CORE-1760:** Tabs alignX prop is no longer supported. Use align=left|right and fullWidth props now instead.

# [5.0.0-beta.11](http://bitbucket.org/thermsio/atalaya/compare/v5.0.0-beta.10...v5.0.0-beta.11) (2022-01-01)

### Bug Fixes

- **Checkbox:** onChangeValue(active: boolean) ([b8a536f](http://bitbucket.org/thermsio/atalaya/commits/b8a536f2b4cbafe3e7ea7d386eb7f91a80979708))

# [5.0.0-beta.10](http://bitbucket.org/thermsio/atalaya/compare/v5.0.0-beta.9...v5.0.0-beta.10) (2021-12-30)

### Features

- **Radio:** add value prop to use as an alternative to 'activeIndex' ([993ee11](http://bitbucket.org/thermsio/atalaya/commits/993ee11a3f5338bc9931efd79b8eb7b4b52dd0b7))
- **Radio:** add vertical prop ([e9f2e46](http://bitbucket.org/thermsio/atalaya/commits/e9f2e467558d608afd6c321a517f8ba85913988e))

# [5.0.0-beta.9](http://bitbucket.org/thermsio/atalaya/compare/v5.0.0-beta.8...v5.0.0-beta.9) (2021-12-28)

### Bug Fixes

- **Select:** onChangeValue check if array ([8bf2a8e](http://bitbucket.org/thermsio/atalaya/commits/8bf2a8efb33dc97ab6a2337cb61999c53c39e6ef))

# [5.0.0-beta.8](http://bitbucket.org/thermsio/atalaya/compare/v5.0.0-beta.7...v5.0.0-beta.8) (2021-12-28)

### Features

- **Select:** add onChangeOption, improve SelectProps interface types and generics ([185ba4c](http://bitbucket.org/thermsio/atalaya/commits/185ba4cdc00ea315a3d1129f8ba372427dc162c5))

# [5.0.0-beta.7](http://bitbucket.org/thermsio/atalaya/compare/v5.0.0-beta.6...v5.0.0-beta.7) (2021-12-27)

### Bug Fixes

- TagGroup, remove default fixed max size; ([c95d8a0](http://bitbucket.org/thermsio/atalaya/commits/c95d8a0f96c364125a808b873d0ddc3b0c2c2eac))

### Features

- Tag, add ability to set custom colors; ([0b59079](http://bitbucket.org/thermsio/atalaya/commits/0b59079fbcca665e5c13f7add99643258c7b18ce))

# [5.0.0-beta.6](http://bitbucket.org/thermsio/atalaya/compare/v5.0.0-beta.5...v5.0.0-beta.6) (2021-12-24)

### Features

- **CORE-1737:** tweak Avatar look; ([63432e7](http://bitbucket.org/thermsio/atalaya/commits/63432e7b59e0d6a196119013194ef92c74a0ac44))

# [5.0.0-beta.5](http://bitbucket.org/thermsio/atalaya/compare/v5.0.0-beta.4...v5.0.0-beta.5) (2021-12-23)

### Features

- **CORE-1675:** stop menu opening when clicking multi select item; ([3a2278a](http://bitbucket.org/thermsio/atalaya/commits/3a2278aabb7263aab58e5ffa64ac2d2c13a9f4f1))

# [5.0.0-beta.4](http://bitbucket.org/thermsio/atalaya/compare/v5.0.0-beta.3...v5.0.0-beta.4) (2021-12-23)

### Bug Fixes

- build:dev script calling error ([98a6622](http://bitbucket.org/thermsio/atalaya/commits/98a6622196f6f0e5758ab15b23dcd6655a0f4be8))

### Features

- **CORE-1675:** Select, add spacing between Selected Multi Options ([612cbbb](http://bitbucket.org/thermsio/atalaya/commits/612cbbb65e1cad79ef82275478041f87271ba65e))

# [5.0.0-beta.3](http://bitbucket.org/thermsio/atalaya/compare/v5.0.0-beta.2...v5.0.0-beta.3) (2021-12-22)

### Bug Fixes

- export SelectProps ([1086ec4](http://bitbucket.org/thermsio/atalaya/commits/1086ec4b7746d647dbf69862d8df7098330c9b9c))

# [5.0.0-beta.2](http://bitbucket.org/thermsio/atalaya/compare/v5.0.0-beta.1...v5.0.0-beta.2) (2021-12-22)

### Bug Fixes

- provide the remaining props that were missing after we stopped passing ...rest props to react-select; ([0f48a59](http://bitbucket.org/thermsio/atalaya/commits/0f48a59dbbeb9da821194c46cb38f9a9eb456d94))

### Features

- update Tabs to work with refactored Select ([8499195](http://bitbucket.org/thermsio/atalaya/commits/8499195fdb0fdc2c9a51d3d5209c3a4ecb7d2d1d))

# [5.0.0-beta.1](http://bitbucket.org/thermsio/atalaya/compare/v4.0.0...v5.0.0-beta.1) (2021-12-21)

### Features

- Select, stop accepting extra react-select specific props. ([46f5fca](http://bitbucket.org/thermsio/atalaya/commits/46f5fca8d7f8f6813982f59c98666482d4984caa))

### BREAKING CHANGES

- Select no long passes extra props to react-select.

# [4.0.0](http://bitbucket.org/thermsio/atalaya/compare/v3.3.1...v4.0.0) (2021-12-19)

### Bug Fixes

- **CORE-1525:** build failing because emitOnlyDeclarations. ([0270558](http://bitbucket.org/thermsio/atalaya/commits/027055858c5448cd83a0bce6cd69dd9669d01d5b))
- **CORE-1525:** fix skeleton being inline instead of stacked and skeleton circle not being a circle; ([bba231e](http://bitbucket.org/thermsio/atalaya/commits/bba231ed8e2d143d8851182a60ea5bbc13fdee0c))
- **CORE-1525:** revert expanding documentations props by default. ([f79fbcd](http://bitbucket.org/thermsio/atalaya/commits/f79fbcdc628de4226a8589d823bc7780eeb37300))
- **CORE-1525:** temp fix to package lock error that causes react cosmos sandbox to freeze. ([447bbe3](http://bitbucket.org/thermsio/atalaya/commits/447bbe35c4f450473d3f7c9562cf0bb1c7cbd263))
- remove tailwind.config mode: 'jit' ([0eb3830](http://bitbucket.org/thermsio/atalaya/commits/0eb3830aa198654993503d9391c484802807c18f))
- tailwind-config content regex for purge ([35b3730](http://bitbucket.org/thermsio/atalaya/commits/35b37307b5c957d808e9fdb00cbe84d7c9447e12))

### chore

- update deps ([1269517](http://bitbucket.org/thermsio/atalaya/commits/12695175773b99ad5e6aa1dcde83233cd3c35639))

### Features

- **CORE-1525:** add RenderGroupHeading. ([51d7376](http://bitbucket.org/thermsio/atalaya/commits/51d73760bbed5ef420dd15f17a6b1f4f8f3e3448))
- **CORE-1525:** add RenderListItem. ([204093a](http://bitbucket.org/thermsio/atalaya/commits/204093a2f9266180132980238c240027c8edfc2f))
- **CORE-1525:** add RenderSelectedSingle & RenderSelectedMulti. ([3619264](http://bitbucket.org/thermsio/atalaya/commits/3619264957b0ebc0d04a910190de5f15cfc7c843))
- **CORE-1525:** Select add documentation. ([dd4100d](http://bitbucket.org/thermsio/atalaya/commits/dd4100d377d3997d49be4d019a70f7f65ecfeb3b))
- **CORE-1525:** Select add searchText to loadOptions ([6654c74](http://bitbucket.org/thermsio/atalaya/commits/6654c7469c53102cdb524cdb297f374e4ba16309))
- **CORE-1525:** Select, delegate a lot of pagination logic to the consuming component. ([fb755e3](http://bitbucket.org/thermsio/atalaya/commits/fb755e3e20a065af465b307a43cfabf893489197))
- **CORE-1525:** Select, keep loading results until there is at least 10. ([9d1927a](http://bitbucket.org/thermsio/atalaya/commits/9d1927ae4c520d318835dddffa0d51d518657a40))
- **CORE-1525:** Select, prevent loading of new options when input has not changed and last options were the same as before. ([b4325ad](http://bitbucket.org/thermsio/atalaya/commits/b4325ade7ff8df2fa1ea8df30d7cd2f24491fab9))
- **CORE-1525:** Select, replace loading more message with skeleton component. ([8b4b215](http://bitbucket.org/thermsio/atalaya/commits/8b4b215401c32c04926b14ce831e37e02de7e1d2))
- **CORE-1525:** Select, sticky position group heading; ([9f16113](http://bitbucket.org/thermsio/atalaya/commits/9f1611323e82a1087f10c2237b10c8866f8894dc))
- **CORE-1525:** Select, tweak loading skeletons; ([a5ec07b](http://bitbucket.org/thermsio/atalaya/commits/a5ec07bcdb3b4de903fef9c6b080bf42d9a3ebbd))
- update to tailwind 3 ([8847a85](http://bitbucket.org/thermsio/atalaya/commits/8847a85be23ea2a6ab40a6bd04b90267dbea01f7))

### BREAKING CHANGES

- tailwind is now at v3 (adding this because we expect a major version bump from semantic-release)

# [3.4.0-beta.5](http://bitbucket.org/thermsio/atalaya/compare/v3.4.0-beta.4...v3.4.0-beta.5) (2021-12-17)

### Features

- **CORE-1525:** Select, sticky position group heading; ([9f16113](http://bitbucket.org/thermsio/atalaya/commits/9f1611323e82a1087f10c2237b10c8866f8894dc))

# [3.4.0-beta.4](http://bitbucket.org/thermsio/atalaya/compare/v3.4.0-beta.3...v3.4.0-beta.4) (2021-12-17)

### Bug Fixes

- **CORE-1525:** build failing because emitOnlyDeclarations. ([0270558](http://bitbucket.org/thermsio/atalaya/commits/027055858c5448cd83a0bce6cd69dd9669d01d5b))
- **CORE-1525:** fix skeleton being inline instead of stacked and skeleton circle not being a circle; ([bba231e](http://bitbucket.org/thermsio/atalaya/commits/bba231ed8e2d143d8851182a60ea5bbc13fdee0c))
- **CORE-1525:** revert expanding documentations props by default. ([f79fbcd](http://bitbucket.org/thermsio/atalaya/commits/f79fbcdc628de4226a8589d823bc7780eeb37300))
- **CORE-1525:** temp fix to package lock error that causes react cosmos sandbox to freeze. ([447bbe3](http://bitbucket.org/thermsio/atalaya/commits/447bbe35c4f450473d3f7c9562cf0bb1c7cbd263))

### Features

- **CORE-1525:** add RenderGroupHeading. ([51d7376](http://bitbucket.org/thermsio/atalaya/commits/51d73760bbed5ef420dd15f17a6b1f4f8f3e3448))
- **CORE-1525:** add RenderListItem. ([204093a](http://bitbucket.org/thermsio/atalaya/commits/204093a2f9266180132980238c240027c8edfc2f))
- **CORE-1525:** add RenderSelectedSingle & RenderSelectedMulti. ([3619264](http://bitbucket.org/thermsio/atalaya/commits/3619264957b0ebc0d04a910190de5f15cfc7c843))
- **CORE-1525:** Select add documentation. ([dd4100d](http://bitbucket.org/thermsio/atalaya/commits/dd4100d377d3997d49be4d019a70f7f65ecfeb3b))
- **CORE-1525:** Select add searchText to loadOptions ([6654c74](http://bitbucket.org/thermsio/atalaya/commits/6654c7469c53102cdb524cdb297f374e4ba16309))
- **CORE-1525:** Select, delegate a lot of pagination logic to the consuming component. ([fb755e3](http://bitbucket.org/thermsio/atalaya/commits/fb755e3e20a065af465b307a43cfabf893489197))
- **CORE-1525:** Select, keep loading results until there is at least 10. ([9d1927a](http://bitbucket.org/thermsio/atalaya/commits/9d1927ae4c520d318835dddffa0d51d518657a40))
- **CORE-1525:** Select, prevent loading of new options when input has not changed and last options were the same as before. ([b4325ad](http://bitbucket.org/thermsio/atalaya/commits/b4325ade7ff8df2fa1ea8df30d7cd2f24491fab9))
- **CORE-1525:** Select, replace loading more message with skeleton component. ([8b4b215](http://bitbucket.org/thermsio/atalaya/commits/8b4b215401c32c04926b14ce831e37e02de7e1d2))
- **CORE-1525:** Select, tweak loading skeletons; ([a5ec07b](http://bitbucket.org/thermsio/atalaya/commits/a5ec07bcdb3b4de903fef9c6b080bf42d9a3ebbd))

# [3.4.0-beta.3](http://bitbucket.org/thermsio/atalaya/compare/v3.4.0-beta.2...v3.4.0-beta.3) (2021-12-11)

### Bug Fixes

- remove tailwind.config mode: 'jit' ([0eb3830](http://bitbucket.org/thermsio/atalaya/commits/0eb3830aa198654993503d9391c484802807c18f))

# [3.4.0-beta.2](http://bitbucket.org/thermsio/atalaya/compare/v3.4.0-beta.1...v3.4.0-beta.2) (2021-12-11)

### Bug Fixes

- tailwind-config content regex for purge ([35b3730](http://bitbucket.org/thermsio/atalaya/commits/35b37307b5c957d808e9fdb00cbe84d7c9447e12))

# [3.4.0-beta.1](http://bitbucket.org/thermsio/atalaya/compare/v3.3.0...v3.4.0-beta.1) (2021-12-11)

### Features

- update to tailwind 3 ([8847a85](http://bitbucket.org/thermsio/atalaya/commits/8847a85be23ea2a6ab40a6bd04b90267dbea01f7))

## [3.3.1](http://bitbucket.org/thermsio/atalaya/compare/v3.3.0...v3.3.1) (2021-12-09)

### Bug Fixes

- update deps, postcss 8.4.4 ([2de8bb9](http://bitbucket.org/thermsio/atalaya/commits/2de8bb96e8d2894acf9d194cd68b57b38e9b4193))

# [3.3.0](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0...v3.3.0) (2021-12-08)

### Bug Fixes

- **CORE-1731:** Colors.md, fix displaying reverse colors values on documentation; ([ae75357](http://bitbucket.org/thermsio/atalaya/commits/ae75357d78e53871118362501c6898d307a6cdd6))

### Features

- **CORE-1732:** add useStylesContext; ([df5b3cb](http://bitbucket.org/thermsio/atalaya/commits/df5b3cbaa7994dafde58a094dbfc385f3b5cbc3d))

# [3.2.0](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0...v3.2.0) (2021-11-29)

### Bug Fixes

- .reduce arg order ([78564e0](http://bitbucket.org/thermsio/atalaya/commits/78564e07dafa8e07a3c09518ebbac1bae32ee2de))
- add disabled prop to FormLayout.Controls ([1553a47](http://bitbucket.org/thermsio/atalaya/commits/1553a477e314a2e2f5c60e6e95ad6d0bd4102911))
- add TextField export ([2f1c758](http://bitbucket.org/thermsio/atalaya/commits/2f1c758f32d1de0a5daa94c6dc4f8b28742d88a0))
- **Button:** fullWidth prop class add width:100% ([077fa45](http://bitbucket.org/thermsio/atalaya/commits/077fa457fcdbc86dbdf752522d4dd79cba224378))
- **ColorPicker:** onChange() mock synthetic event ([3859131](http://bitbucket.org/thermsio/atalaya/commits/3859131ac9d5fc734d55a0cbe10cdf3cf389782a))
- **CopyText:** add displayValue prop ([fdcb279](http://bitbucket.org/thermsio/atalaya/commits/fdcb27923c5d2787d039875948b8985feebd85a5))
- **CopyText:** children optional prop ([59a95fd](http://bitbucket.org/thermsio/atalaya/commits/59a95fd225ec3753d9b569248e58a15070f31e9d))
- **CopyText:** e.stopPropagation() ([5960791](http://bitbucket.org/thermsio/atalaya/commits/596079162f612ddd4c66a4bf56c9e343f5dc744a))
- **CopyText:** no value return children ([1aa4604](http://bitbucket.org/thermsio/atalaya/commits/1aa4604cf2e85f9cc05ef620482459796511bd95))
- **CopyText:** typo ([a3173a2](http://bitbucket.org/thermsio/atalaya/commits/a3173a2da3deaf0835ce2c9ed9de49a84c6f5e82))
- **CopyText:** typo children value ([d157b1e](http://bitbucket.org/thermsio/atalaya/commits/d157b1e5b38241a59a2e88eade4e95f12714487a))
- **CORE-1333:** escape quote; (glad the pipeline caught that, society would have collapsed otherwise) ([6834e09](http://bitbucket.org/thermsio/atalaya/commits/6834e09838e56365aeeaaae254608dd15d6a3966))
- **CORE-1333:** prevent form input state being lost between Horizontal and Stacked display change ([55fdc93](http://bitbucket.org/thermsio/atalaya/commits/55fdc9306264e42900ab1987d0a2b3081fce0979))
- **CORE-1333:** remove divider lines after form title ([6d91b88](http://bitbucket.org/thermsio/atalaya/commits/6d91b88ddfcd4f24a635c012a43baa5b4cb3d7fa))
- **CORE-1613:** move styles.css to project root; ([d7e248b](http://bitbucket.org/thermsio/atalaya/commits/d7e248bdd2c734a3a4f92aab7375b46175a04830))
- **CORE-1613:** remove components css files from tailwind components to prevent purging and specificity issues; ([9d26ec8](http://bitbucket.org/thermsio/atalaya/commits/9d26ec8ed2bb218c0e87b2616d1dd67254bea12a))
- **CORE-1667:** fix alignment issuesb etween AvatarImage and AvatarSymbol ([d457594](http://bitbucket.org/thermsio/atalaya/commits/d457594bd538de4aaee84e4afba9ce332c8e2401))
- **CORE-1667:** fix Image height/width prop not working; ([f738c1a](http://bitbucket.org/thermsio/atalaya/commits/f738c1a3730843dfcfd1e1eacc4d9daa5cadec85))
- **CORE-1705:** FormControls, only render childs if the are valid elements. ([fae13d3](http://bitbucket.org/thermsio/atalaya/commits/fae13d3bca54c31e70cd14b0525163be7695821c))
- **CORE-1705:** FormControls, prevent extra spacing when missing extraEnd and onDelete props. ([3b63e44](http://bitbucket.org/thermsio/atalaya/commits/3b63e444107936b2846716f65353f4a1da0648de))
- **CORE-1705:** FormSection, fix spacing not working inside form sections; ([3693d12](http://bitbucket.org/thermsio/atalaya/commits/3693d128e301a94a14ae36745380536ee87f1255))
- **CORE-1705:** Modal, add more spacing between header and body; ([55590b8](http://bitbucket.org/thermsio/atalaya/commits/55590b83645e2a043ee881e5f1ae2b419efc6ce1))
- cosmos not working with new StylesContext shape ([0fdffc0](http://bitbucket.org/thermsio/atalaya/commits/0fdffc0574315b42d21bda1a3fa7fd39bcbd5ab7))
- css comment type ([60f52a1](http://bitbucket.org/thermsio/atalaya/commits/60f52a1a983fb32c3a863f4ec0d6b9c2923892f0))
- don't render Breadcrumbs if supplied empty array ([8677059](http://bitbucket.org/thermsio/atalaya/commits/867705946a4f293f27b3e3c01a20c9ecc0749429))
- export CopyText component ([6c79189](http://bitbucket.org/thermsio/atalaya/commits/6c79189c99d3f854d7be08785616f2f971c473b5))
- fix badge styles not working; ([e79554c](http://bitbucket.org/thermsio/atalaya/commits/e79554c9ca92eaa41d8a5c4dcb7177fcc9e59f8a))
- fix slider range being compressed; ([259cf99](http://bitbucket.org/thermsio/atalaya/commits/259cf996f8f271cf330fdf2c0a341c652f684b70))
- fixture component name ([8f4c59f](http://bitbucket.org/thermsio/atalaya/commits/8f4c59ff4cffdb253b81627849fe57e90b8a92f5))
- FormLayout Text import for styleguide docs ([1a3f2f7](http://bitbucket.org/thermsio/atalaya/commits/1a3f2f7a89e7d865eb17a03297a5afcfa0e9aca3))
- **Modal:** positioned items being hidden inside modal when overflowing. ([b19162a](http://bitbucket.org/thermsio/atalaya/commits/b19162a0cd8d8a652cd7fdbe3b948404f0b19308))
- **Overlay:** add .z-tooltip to children ([5ab0e15](http://bitbucket.org/thermsio/atalaya/commits/5ab0e15cfac30d53ab1800cb8c1d5cad08e93680))
- remove min width/height from buttons: ([d34ff55](http://bitbucket.org/thermsio/atalaya/commits/d34ff5570fd9b0c051b8225ea71fb060aa3970aa))
- remove outdated code from tailwind.config ([50bff7a](http://bitbucket.org/thermsio/atalaya/commits/50bff7a5731560c2540af6668435f6693ea3d599))
- **Select:** onChange optional prop ([2cf1657](http://bitbucket.org/thermsio/atalaya/commits/2cf16577678f885322770d8cf8ef4974f00fc2a3))
- **Select:** parse value prop and map to option in list ([b61d697](http://bitbucket.org/thermsio/atalaya/commits/b61d6977e6d46a1911cc66d1dc8ee2d7d74a19fc))
- TableGroup controls section remove margin css ([a3d91a6](http://bitbucket.org/thermsio/atalaya/commits/a3d91a62d65d0b975f3ad7b0e2509beab46051fa))
- TableGroup export ([8fb12a5](http://bitbucket.org/thermsio/atalaya/commits/8fb12a5b34d9dc2e72b072e6b15e12ca56b763e6))
- **TableGroup:** onSetSkippedRecords calc ([5f4088f](http://bitbucket.org/thermsio/atalaya/commits/5f4088f1db2b69ac22f4b014e8940afbd696071e))
- **TableGroup:** pagination onSetSkippedRecords prop value ([83489cc](http://bitbucket.org/thermsio/atalaya/commits/83489cc52a4060fdc57f77dec8a6aeb0611b4852))
- tailwind .css JIT mode requirements, add '[@tailwind](http://bitbucket.org/tailwind) components' where '[@layer](http://bitbucket.org/layer) components' is used ([0650e46](http://bitbucket.org/thermsio/atalaya/commits/0650e461b4ca1e1ab317b479e0c264e8339c7fca))
- **Toast:** children type allow element or string ([75b3aad](http://bitbucket.org/thermsio/atalaya/commits/75b3aad772a15d0eda58b2bb3c819506c9965e65))
- tsconfig ([ff62c4a](http://bitbucket.org/thermsio/atalaya/commits/ff62c4aff6addfd1de466806a1676f12a923c22f))

### Features

- **ActionModal:** add slow loading logic and UI ([801b5d6](http://bitbucket.org/thermsio/atalaya/commits/801b5d69b648cdc888cf45781a29d875f25517a5))
- **CORE-1333:** add dividers prop to FormSection ([c50a30a](http://bitbucket.org/thermsio/atalaya/commits/c50a30aa836b75dd3e9aba611a6c7bdb21e71ae1))
- **CORE-1333:** add FormSections component; ([babaaa5](http://bitbucket.org/thermsio/atalaya/commits/babaaa5a6bb98f944552479a1a4ab96b2bd89101))
- **CORE-1333:** add Sections examples; ([a173ae5](http://bitbucket.org/thermsio/atalaya/commits/a173ae56fc6a4c79b9fafaaaea5bc73350aae95f))
- **CORE-1333:** remove form heading components in favor of heading props; ([7c3f78e](http://bitbucket.org/thermsio/atalaya/commits/7c3f78eb25a7161eb0125e13f97e9347bc86fd20))
- **CORE-1519:** add AvatarGroup; ([115838e](http://bitbucket.org/thermsio/atalaya/commits/115838e17ba7fc7f72851a0f27b767028c96a52c))
- **CORE-1520:** add alignX options; ([5c530f0](http://bitbucket.org/thermsio/atalaya/commits/5c530f03c9da69bdf2201744057b996c23fe8daf))
- **CORE-1520:** add noBottomBorder and pills; ([2b10357](http://bitbucket.org/thermsio/atalaya/commits/2b10357830a51e77f944b2ce91d973dcd03f69c7))
- **CORE-1558:** add AtalayaStyle wrapper ([d3437d3](http://bitbucket.org/thermsio/atalaya/commits/d3437d35bccc13e77d5fa5e8e0722c11e990f02b))
- **CORE-1558:** add StyleWrapper component; ([4d2b769](http://bitbucket.org/thermsio/atalaya/commits/4d2b769a63d72cf0e0f8b4f9901c8aed3dcdfb00))
- **CORE-1580:** maintain a consistent button size across different button types; ([bcbd188](http://bitbucket.org/thermsio/atalaya/commits/bcbd188dbb35ae6457d20ebca134da2d282e6306))
- **CORE-1584:** expand avatar so it can use custom colors; ([8b6ac9d](http://bitbucket.org/thermsio/atalaya/commits/8b6ac9d1e5f4afc426251bbcc1aaeb316a47db4b))
- **CORE-1584:** expand avatar so it can use custom colors; ([22d77d5](http://bitbucket.org/thermsio/atalaya/commits/22d77d5a607097f8514a5cbde89272f20385e64a))
- **CORE-1612:** add loading message; ([4be8855](http://bitbucket.org/thermsio/atalaya/commits/4be88551c89fbfc871bf02993bbaeac2801b41c6))
- **CORE-1613:** add standalone css exports; ([55d1307](http://bitbucket.org/thermsio/atalaya/commits/55d130794265db7bdcfd2853b5c24cc6f79d4eaf))
- **CORE-1613:** export css styles as a separate style sheet; ([4b16ef4](http://bitbucket.org/thermsio/atalaya/commits/4b16ef4a32c8717b1d534e33277d2b72f0cd4dbb))
- **CORE-1613:** update readme with atalaya import changes; ([2e9d9ce](http://bitbucket.org/thermsio/atalaya/commits/2e9d9ce79f05fe74acec0e1a0741ecc9854a155f))
- **CORE-1616:** add toastId to documentation. ([18ed36b](http://bitbucket.org/thermsio/atalaya/commits/18ed36b536087e831c6bb2de604c3fcf706ba304))
- **CORE-1617:** add max/min width prop to FormControlWrapper; ([042f33b](http://bitbucket.org/thermsio/atalaya/commits/042f33ba947b5297d9c40351d7d749030a127615))
- **CORE-1667:** add documentation about height and width changes for Image component; ([68c769a](http://bitbucket.org/thermsio/atalaya/commits/68c769a782d22712f2c22c1fe05c10b806441557))
- **CORE-1667:** add symbol option to Avatar; ([ef97b97](http://bitbucket.org/thermsio/atalaya/commits/ef97b97e3724d2ac0f7eb48e57110ca30402ab25))
- **CORE-1705:** FormLayout, add space prop ([df00eaa](http://bitbucket.org/thermsio/atalaya/commits/df00eaa933038f89a62a5bb62e8628207be00177))
- expand styleguide props & methods by default; ([971ae63](http://bitbucket.org/thermsio/atalaya/commits/971ae63fb27e1f06d8699603440c0cb2924bb156))
- export Breadcrumbs ([6c91963](http://bitbucket.org/thermsio/atalaya/commits/6c919635b359a80020ea942820778cea4d577aec))
- **Loading:** add hidden & visible props ([78dd95d](http://bitbucket.org/thermsio/atalaya/commits/78dd95d68adad58fabc417a942d31fb217a7fbd6))
- **Modal:** add width prop for sizing modals ([622b15b](http://bitbucket.org/thermsio/atalaya/commits/622b15bae5e8deef1d7adfdcad67c032c1cc33dc))
- **Overlay:** a layover component for utility use ([0af59e5](http://bitbucket.org/thermsio/atalaya/commits/0af59e54afae7cfa0967d439151f64909a8490e3))
- **TableGroup:** added new props onClickRow and passed props to TableRow component ([b499398](http://bitbucket.org/thermsio/atalaya/commits/b499398cba4e02e0faddf7ee8632732fe802ad93))
- **TableGroup:** pass clearSelectedRows to table Controls component ([dfa4c58](http://bitbucket.org/thermsio/atalaya/commits/dfa4c58219ec9c1d92ac59fe1ce597c93894690d))
- TableRow HTMLElement props ([bd8a8cc](http://bitbucket.org/thermsio/atalaya/commits/bd8a8cc31adfcfda685f776ee9a6c44913092bd1))

# [3.2.0-beta.41](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.40...v3.2.0-beta.41) (2021-11-29)

### Bug Fixes

- **CORE-1613:** move styles.css to project root; ([d7e248b](http://bitbucket.org/thermsio/atalaya/commits/d7e248bdd2c734a3a4f92aab7375b46175a04830))
- **CORE-1613:** remove components css files from tailwind components to prevent purging and specificity issues; ([9d26ec8](http://bitbucket.org/thermsio/atalaya/commits/9d26ec8ed2bb218c0e87b2616d1dd67254bea12a))
- remove min width/height from buttons: ([d34ff55](http://bitbucket.org/thermsio/atalaya/commits/d34ff5570fd9b0c051b8225ea71fb060aa3970aa))
- remove outdated code from tailwind.config ([50bff7a](http://bitbucket.org/thermsio/atalaya/commits/50bff7a5731560c2540af6668435f6693ea3d599))

### Features

- **CORE-1613:** add standalone css exports; ([55d1307](http://bitbucket.org/thermsio/atalaya/commits/55d130794265db7bdcfd2853b5c24cc6f79d4eaf))
- **CORE-1613:** export css styles as a separate style sheet; ([4b16ef4](http://bitbucket.org/thermsio/atalaya/commits/4b16ef4a32c8717b1d534e33277d2b72f0cd4dbb))
- **CORE-1613:** update readme with atalaya import changes; ([2e9d9ce](http://bitbucket.org/thermsio/atalaya/commits/2e9d9ce79f05fe74acec0e1a0741ecc9854a155f))

# [3.2.0-beta.40](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.39...v3.2.0-beta.40) (2021-11-29)

### Bug Fixes

- **CORE-1705:** FormSection, fix spacing not working inside form sections; ([3693d12](http://bitbucket.org/thermsio/atalaya/commits/3693d128e301a94a14ae36745380536ee87f1255))

# [3.2.0-beta.39](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.38...v3.2.0-beta.39) (2021-11-26)

### Bug Fixes

- **CORE-1705:** FormControls, only render childs if the are valid elements. ([fae13d3](http://bitbucket.org/thermsio/atalaya/commits/fae13d3bca54c31e70cd14b0525163be7695821c))
- **CORE-1705:** FormControls, prevent extra spacing when missing extraEnd and onDelete props. ([3b63e44](http://bitbucket.org/thermsio/atalaya/commits/3b63e444107936b2846716f65353f4a1da0648de))
- **CORE-1705:** Modal, add more spacing between header and body; ([55590b8](http://bitbucket.org/thermsio/atalaya/commits/55590b83645e2a043ee881e5f1ae2b419efc6ce1))

### Features

- **CORE-1705:** FormLayout, add space prop ([df00eaa](http://bitbucket.org/thermsio/atalaya/commits/df00eaa933038f89a62a5bb62e8628207be00177))

# [3.2.0-beta.38](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.37...v3.2.0-beta.38) (2021-11-22)

### Bug Fixes

- **CORE-1667:** fix alignment issuesb etween AvatarImage and AvatarSymbol ([d457594](http://bitbucket.org/thermsio/atalaya/commits/d457594bd538de4aaee84e4afba9ce332c8e2401))

### Features

- **CORE-1519:** add AvatarGroup; ([115838e](http://bitbucket.org/thermsio/atalaya/commits/115838e17ba7fc7f72851a0f27b767028c96a52c))
- **CORE-1667:** add symbol option to Avatar; ([ef97b97](http://bitbucket.org/thermsio/atalaya/commits/ef97b97e3724d2ac0f7eb48e57110ca30402ab25))
- expand styleguide props & methods by default; ([971ae63](http://bitbucket.org/thermsio/atalaya/commits/971ae63fb27e1f06d8699603440c0cb2924bb156))

# [3.2.0-beta.37](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.36...v3.2.0-beta.37) (2021-11-21)

### Bug Fixes

- fixture component name ([8f4c59f](http://bitbucket.org/thermsio/atalaya/commits/8f4c59ff4cffdb253b81627849fe57e90b8a92f5))

### Features

- **Modal:** add width prop for sizing modals ([622b15b](http://bitbucket.org/thermsio/atalaya/commits/622b15bae5e8deef1d7adfdcad67c032c1cc33dc))

# [3.2.0-beta.36](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.35...v3.2.0-beta.36) (2021-11-18)

### Features

- **CORE-1616:** add toastId to documentation. ([18ed36b](http://bitbucket.org/thermsio/atalaya/commits/18ed36b536087e831c6bb2de604c3fcf706ba304))

# [3.2.0-beta.35](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.34...v3.2.0-beta.35) (2021-11-18)

### Bug Fixes

- **Modal:** positioned items being hidden inside modal when overflowing. ([b19162a](http://bitbucket.org/thermsio/atalaya/commits/b19162a0cd8d8a652cd7fdbe3b948404f0b19308))

# [3.2.0-beta.34](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.33...v3.2.0-beta.34) (2021-11-18)

### Features

- **CORE-1612:** add loading message; ([4be8855](http://bitbucket.org/thermsio/atalaya/commits/4be88551c89fbfc871bf02993bbaeac2801b41c6))

# [3.2.0-beta.33](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.32...v3.2.0-beta.33) (2021-11-17)

### Bug Fixes

- **CORE-1667:** fix Image height/width prop not working; ([f738c1a](http://bitbucket.org/thermsio/atalaya/commits/f738c1a3730843dfcfd1e1eacc4d9daa5cadec85))

### Features

- **CORE-1667:** add documentation about height and width changes for Image component; ([68c769a](http://bitbucket.org/thermsio/atalaya/commits/68c769a782d22712f2c22c1fe05c10b806441557))

# [3.2.0-beta.32](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.31...v3.2.0-beta.32) (2021-11-16)

### Bug Fixes

- **ColorPicker:** onChange() mock synthetic event ([3859131](http://bitbucket.org/thermsio/atalaya/commits/3859131ac9d5fc734d55a0cbe10cdf3cf389782a))

### Features

- **TableGroup:** pass clearSelectedRows to table Controls component ([dfa4c58](http://bitbucket.org/thermsio/atalaya/commits/dfa4c58219ec9c1d92ac59fe1ce597c93894690d))

# [3.2.0-beta.31](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.30...v3.2.0-beta.31) (2021-11-16)

### Features

- **TableGroup:** added new props onClickRow and passed props to TableRow component ([b499398](http://bitbucket.org/thermsio/atalaya/commits/b499398cba4e02e0faddf7ee8632732fe802ad93))

# [3.2.0-beta.30](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.29...v3.2.0-beta.30) (2021-11-15)

### Bug Fixes

- add disabled prop to FormLayout.Controls ([1553a47](http://bitbucket.org/thermsio/atalaya/commits/1553a477e314a2e2f5c60e6e95ad6d0bd4102911))

# [3.2.0-beta.29](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.28...v3.2.0-beta.29) (2021-10-31)

### Bug Fixes

- **Button:** fullWidth prop class add width:100% ([077fa45](http://bitbucket.org/thermsio/atalaya/commits/077fa457fcdbc86dbdf752522d4dd79cba224378))
- css comment type ([60f52a1](http://bitbucket.org/thermsio/atalaya/commits/60f52a1a983fb32c3a863f4ec0d6b9c2923892f0))

# [3.2.0-beta.28](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.27...v3.2.0-beta.28) (2021-10-26)

### Bug Fixes

- don't render Breadcrumbs if supplied empty array ([8677059](http://bitbucket.org/thermsio/atalaya/commits/867705946a4f293f27b3e3c01a20c9ecc0749429))

# [3.2.0-beta.27](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.26...v3.2.0-beta.27) (2021-10-26)

### Features

- export Breadcrumbs ([6c91963](http://bitbucket.org/thermsio/atalaya/commits/6c919635b359a80020ea942820778cea4d577aec))

# [3.2.0-beta.26](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.25...v3.2.0-beta.26) (2021-10-22)

### Bug Fixes

- fix slider range being compressed; ([259cf99](http://bitbucket.org/thermsio/atalaya/commits/259cf996f8f271cf330fdf2c0a341c652f684b70))

### Features

- **CORE-1617:** add max/min width prop to FormControlWrapper; ([042f33b](http://bitbucket.org/thermsio/atalaya/commits/042f33ba947b5297d9c40351d7d749030a127615))

# [3.2.0-beta.25](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.24...v3.2.0-beta.25) (2021-10-22)

### Features

- **CORE-1584:** expand avatar so it can use custom colors; ([8b6ac9d](http://bitbucket.org/thermsio/atalaya/commits/8b6ac9d1e5f4afc426251bbcc1aaeb316a47db4b))
- **CORE-1584:** expand avatar so it can use custom colors; ([22d77d5](http://bitbucket.org/thermsio/atalaya/commits/22d77d5a607097f8514a5cbde89272f20385e64a))

# [3.2.0-beta.24](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.23...v3.2.0-beta.24) (2021-10-22)

### Bug Fixes

- .reduce arg order ([78564e0](http://bitbucket.org/thermsio/atalaya/commits/78564e07dafa8e07a3c09518ebbac1bae32ee2de))

# [3.2.0-beta.23](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.22...v3.2.0-beta.23) (2021-10-22)

### Bug Fixes

- **Select:** parse value prop and map to option in list ([b61d697](http://bitbucket.org/thermsio/atalaya/commits/b61d6977e6d46a1911cc66d1dc8ee2d7d74a19fc))

# [3.2.0-beta.22](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.21...v3.2.0-beta.22) (2021-10-21)

### Features

- **ActionModal:** add slow loading logic and UI ([801b5d6](http://bitbucket.org/thermsio/atalaya/commits/801b5d69b648cdc888cf45781a29d875f25517a5))

# [3.2.0-beta.21](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.20...v3.2.0-beta.21) (2021-10-21)

### Bug Fixes

- **CopyText:** children optional prop ([59a95fd](http://bitbucket.org/thermsio/atalaya/commits/59a95fd225ec3753d9b569248e58a15070f31e9d))

# [3.2.0-beta.20](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.19...v3.2.0-beta.20) (2021-10-21)

### Bug Fixes

- **CopyText:** no value return children ([1aa4604](http://bitbucket.org/thermsio/atalaya/commits/1aa4604cf2e85f9cc05ef620482459796511bd95))

# [3.2.0-beta.19](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.18...v3.2.0-beta.19) (2021-10-20)

### Bug Fixes

- **CopyText:** typo ([a3173a2](http://bitbucket.org/thermsio/atalaya/commits/a3173a2da3deaf0835ce2c9ed9de49a84c6f5e82))
- **CopyText:** typo children value ([d157b1e](http://bitbucket.org/thermsio/atalaya/commits/d157b1e5b38241a59a2e88eade4e95f12714487a))

# [3.2.0-beta.18](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.17...v3.2.0-beta.18) (2021-10-20)

### Bug Fixes

- **CopyText:** add displayValue prop ([fdcb279](http://bitbucket.org/thermsio/atalaya/commits/fdcb27923c5d2787d039875948b8985feebd85a5))
- **CopyText:** e.stopPropagation() ([5960791](http://bitbucket.org/thermsio/atalaya/commits/596079162f612ddd4c66a4bf56c9e343f5dc744a))

# [3.2.0-beta.17](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.16...v3.2.0-beta.17) (2021-10-20)

### Bug Fixes

- fix badge styles not working; ([e79554c](http://bitbucket.org/thermsio/atalaya/commits/e79554c9ca92eaa41d8a5c4dcb7177fcc9e59f8a))

# [3.2.0-beta.16](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.15...v3.2.0-beta.16) (2021-10-20)

### Bug Fixes

- export CopyText component ([6c79189](http://bitbucket.org/thermsio/atalaya/commits/6c79189c99d3f854d7be08785616f2f971c473b5))

# [3.2.0-beta.15](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.14...v3.2.0-beta.15) (2021-10-20)

### Bug Fixes

- **Toast:** children type allow element or string ([75b3aad](http://bitbucket.org/thermsio/atalaya/commits/75b3aad772a15d0eda58b2bb3c819506c9965e65))

# [3.2.0-beta.14](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.13...v3.2.0-beta.14) (2021-10-20)

### Bug Fixes

- **TableGroup:** pagination onSetSkippedRecords prop value ([83489cc](http://bitbucket.org/thermsio/atalaya/commits/83489cc52a4060fdc57f77dec8a6aeb0611b4852))

# [3.2.0-beta.13](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.12...v3.2.0-beta.13) (2021-10-18)

### Bug Fixes

- **CORE-1333:** escape quote; (glad the pipeline caught that, society would have collapsed otherwise) ([6834e09](http://bitbucket.org/thermsio/atalaya/commits/6834e09838e56365aeeaaae254608dd15d6a3966))
- **CORE-1333:** prevent form input state being lost between Horizontal and Stacked display change ([55fdc93](http://bitbucket.org/thermsio/atalaya/commits/55fdc9306264e42900ab1987d0a2b3081fce0979))
- **CORE-1333:** remove divider lines after form title ([6d91b88](http://bitbucket.org/thermsio/atalaya/commits/6d91b88ddfcd4f24a635c012a43baa5b4cb3d7fa))
- cosmos not working with new StylesContext shape ([0fdffc0](http://bitbucket.org/thermsio/atalaya/commits/0fdffc0574315b42d21bda1a3fa7fd39bcbd5ab7))

### Features

- **CORE-1333:** add dividers prop to FormSection ([c50a30a](http://bitbucket.org/thermsio/atalaya/commits/c50a30aa836b75dd3e9aba611a6c7bdb21e71ae1))
- **CORE-1333:** add FormSections component; ([babaaa5](http://bitbucket.org/thermsio/atalaya/commits/babaaa5a6bb98f944552479a1a4ab96b2bd89101))
- **CORE-1333:** add Sections examples; ([a173ae5](http://bitbucket.org/thermsio/atalaya/commits/a173ae56fc6a4c79b9fafaaaea5bc73350aae95f))
- **CORE-1333:** remove form heading components in favor of heading props; ([7c3f78e](http://bitbucket.org/thermsio/atalaya/commits/7c3f78eb25a7161eb0125e13f97e9347bc86fd20))

# [3.2.0-beta.12](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.11...v3.2.0-beta.12) (2021-10-17)

### Bug Fixes

- **TableGroup:** onSetSkippedRecords calc ([5f4088f](http://bitbucket.org/thermsio/atalaya/commits/5f4088f1db2b69ac22f4b014e8940afbd696071e))

# [3.2.0-beta.11](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.10...v3.2.0-beta.11) (2021-10-12)

### Bug Fixes

- **Overlay:** add .z-tooltip to children ([5ab0e15](http://bitbucket.org/thermsio/atalaya/commits/5ab0e15cfac30d53ab1800cb8c1d5cad08e93680))

# [3.2.0-beta.10](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.9...v3.2.0-beta.10) (2021-10-12)

### Features

- **Overlay:** a layover component for utility use ([0af59e5](http://bitbucket.org/thermsio/atalaya/commits/0af59e54afae7cfa0967d439151f64909a8490e3))

# [3.2.0-beta.9](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.8...v3.2.0-beta.9) (2021-10-11)

### Features

- **CORE-1558:** add AtalayaStyle wrapper ([d3437d3](http://bitbucket.org/thermsio/atalaya/commits/d3437d35bccc13e77d5fa5e8e0722c11e990f02b))
- **CORE-1558:** add StyleWrapper component; ([4d2b769](http://bitbucket.org/thermsio/atalaya/commits/4d2b769a63d72cf0e0f8b4f9901c8aed3dcdfb00))

# [3.2.0-beta.8](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.7...v3.2.0-beta.8) (2021-10-05)

### Bug Fixes

- tailwind .css JIT mode requirements, add '[@tailwind](http://bitbucket.org/tailwind) components' where '[@layer](http://bitbucket.org/layer) components' is used ([0650e46](http://bitbucket.org/thermsio/atalaya/commits/0650e461b4ca1e1ab317b479e0c264e8339c7fca))

### Features

- **Loading:** add hidden & visible props ([78dd95d](http://bitbucket.org/thermsio/atalaya/commits/78dd95d68adad58fabc417a942d31fb217a7fbd6))

# [3.2.0-beta.7](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.6...v3.2.0-beta.7) (2021-10-05)

### Bug Fixes

- **Select:** onChange optional prop ([2cf1657](http://bitbucket.org/thermsio/atalaya/commits/2cf16577678f885322770d8cf8ef4974f00fc2a3))

# [3.2.0-beta.6](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.5...v3.2.0-beta.6) (2021-10-03)

### Features

- TableRow HTMLElement props ([bd8a8cc](http://bitbucket.org/thermsio/atalaya/commits/bd8a8cc31adfcfda685f776ee9a6c44913092bd1))

# [3.2.0-beta.5](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.4...v3.2.0-beta.5) (2021-10-03)

### Bug Fixes

- TableGroup controls section remove margin css ([a3d91a6](http://bitbucket.org/thermsio/atalaya/commits/a3d91a62d65d0b975f3ad7b0e2509beab46051fa))
- tsconfig ([ff62c4a](http://bitbucket.org/thermsio/atalaya/commits/ff62c4aff6addfd1de466806a1676f12a923c22f))

# [3.2.0-beta.4](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.3...v3.2.0-beta.4) (2021-10-03)

### Bug Fixes

- TableGroup export ([8fb12a5](http://bitbucket.org/thermsio/atalaya/commits/8fb12a5b34d9dc2e72b072e6b15e12ca56b763e6))

# [3.2.0-beta.3](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.2...v3.2.0-beta.3) (2021-10-01)

### Bug Fixes

- add TextField export ([2f1c758](http://bitbucket.org/thermsio/atalaya/commits/2f1c758f32d1de0a5daa94c6dc4f8b28742d88a0))

# [3.2.0-beta.2](http://bitbucket.org/thermsio/atalaya/compare/v3.2.0-beta.1...v3.2.0-beta.2) (2021-09-29)

### Features

- **CORE-1520:** add alignX options; ([5c530f0](http://bitbucket.org/thermsio/atalaya/commits/5c530f03c9da69bdf2201744057b996c23fe8daf))
- **CORE-1520:** add noBottomBorder and pills; ([2b10357](http://bitbucket.org/thermsio/atalaya/commits/2b10357830a51e77f944b2ce91d973dcd03f69c7))

# [3.2.0-beta.1](http://bitbucket.org/thermsio/atalaya/compare/v3.1.1-beta.1...v3.2.0-beta.1) (2021-09-29)

### Features

- **CORE-1580:** maintain a consistent button size across different button types; ([bcbd188](http://bitbucket.org/thermsio/atalaya/commits/bcbd188dbb35ae6457d20ebca134da2d282e6306))

## [3.1.1-beta.1](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0...v3.1.1-beta.1) (2021-09-25)

### Bug Fixes

- FormLayout Text import for styleguide docs ([1a3f2f7](http://bitbucket.org/thermsio/atalaya/commits/1a3f2f7a89e7d865eb17a03297a5afcfa0e9aca3))

# [3.1.0](http://bitbucket.org/thermsio/atalaya/compare/v3.0.0...v3.1.0) (2021-09-25)

### Bug Fixes

- add tailwind in dependencies list ([2425796](http://bitbucket.org/thermsio/atalaya/commits/2425796b46c02341e845cd24a2f3039d93f717c5))
- build remove inject env vars for NODE_ENV to remove \_virtual dir file with no extension ([ede2d8e](http://bitbucket.org/thermsio/atalaya/commits/ede2d8e71c3453e2d783eb393f2a2dd5f7cddbb8))
- **CORE-1356:** make ButtonGroup more lenient about it's children ([0e16438](http://bitbucket.org/thermsio/atalaya/commits/0e16438f596450992e957661a3effde2f433b51b))
- **CORE-1356:** make HTML Select work with numbers ([7dfa2f5](http://bitbucket.org/thermsio/atalaya/commits/7dfa2f5e2d05d45d1bb1e001e72f6f28ca2a90f6))
- **CORE-1356:** make Inline width work with wrap ([d198678](http://bitbucket.org/thermsio/atalaya/commits/d1986789ffe4de2975448e22344754d49c6b4f0e))
- **CORE-1357:** fix RichTextEditor not loading toolbar icons, for real. ([3de317b](http://bitbucket.org/thermsio/atalaya/commits/3de317b1c1aa7efecd0299a2037f93fd799dc598))
- **CORE-1357:** fix toolbar icons not showing; ([140a2d0](http://bitbucket.org/thermsio/atalaya/commits/140a2d035eae46cd0a031c974dca69c47f71741c))
- **CORE-1358:** change active option logic; use the index instead of the option itself for tracking it ([c1b17c9](http://bitbucket.org/thermsio/atalaya/commits/c1b17c9a155e43bcf13519c6f3764a7d99d9a4d6))
- **CORE-1358:** make fullWidth tabs only turning into select when minimum size has been reached ([9ff9a18](http://bitbucket.org/thermsio/atalaya/commits/9ff9a18cc182fa25ce1a3e2d4aed27f18dbe3217))
- **CORE-1358:** remove console.logs ([1bcc222](http://bitbucket.org/thermsio/atalaya/commits/1bcc2222e066eb5efcfe1aac972352e1076ba573))
- **CORE-1358:** Showing Select on first render regardless of component width ([ab75272](http://bitbucket.org/thermsio/atalaya/commits/ab75272498f24b728056d45e0e2552b15864b8fc))
- **CORE-1358:** Stop Select custom css from getting purged ([59e12d3](http://bitbucket.org/thermsio/atalaya/commits/59e12d356ca56b8c0073fce8f72d0d06d18ec554))
- **CORE-1358:** tailwind config ([9c799e0](http://bitbucket.org/thermsio/atalaya/commits/9c799e043eea265253ab924458744138e69d6a6f))
- **CORE-1361:** add custom classes to Icons ([7f89fbf](http://bitbucket.org/thermsio/atalaya/commits/7f89fbf510f5158387d65f191c902fc1b7e2da12))
- **CORE-1524:** simplify ComponentsWrapper Props ([d562a4a](http://bitbucket.org/thermsio/atalaya/commits/d562a4a87e8af906c2110c5ec8aa1879145c680f))
- **CORE-1547:** fix build errors ([1097bf0](http://bitbucket.org/thermsio/atalaya/commits/1097bf020736ea0ddda4e26529345c9e632db501))
- exports added useDarkModeTheme ([35152af](http://bitbucket.org/thermsio/atalaya/commits/35152afd7be8550bfabebfd58dda0cd180937210))
- files deployed w/ npm package to include patches/ dir ([c5c55f6](http://bitbucket.org/thermsio/atalaya/commits/c5c55f67bea7efbbbf3834a098445a77df7c3204))
- fix incorrect icon size ([19560c0](http://bitbucket.org/thermsio/atalaya/commits/19560c091031ddbf4db24565ecc23d43c6ebd002))
- readme and bumping for react-draft-wysiwyg patch version publish ([e9eac1f](http://bitbucket.org/thermsio/atalaya/commits/e9eac1f085268c4918bfaf1216a913119cc2f818))
- remove .env file ([bf69f5e](http://bitbucket.org/thermsio/atalaya/commits/bf69f5ea30ca006146e2949bd1dc2ac70936d891))
- **Skeleton.Text:** don't wrap <div> if no lines prop passed ([9e14ad9](http://bitbucket.org/thermsio/atalaya/commits/9e14ad9803c5d1992b3ecc701846f392dae6e183))
- **Skeleton.Text:** use <Inline/> wrapper ([130d13e](http://bitbucket.org/thermsio/atalaya/commits/130d13ee9ca6c22356b92d33efc92e4fb49fe10d))
- **TableGroup:** docs example component ([e45b07a](http://bitbucket.org/thermsio/atalaya/commits/e45b07ae90cef01514ae2aaa6b6dfaf15036cc38))
- text color not being subtle ([3b3fd1a](http://bitbucket.org/thermsio/atalaya/commits/3b3fd1ab9bf6517bcf4043185aacc508248e69ef))
- **ToggleSwitch:** active={undefined | boolean} ([1d9b62c](http://bitbucket.org/thermsio/atalaya/commits/1d9b62c4f62eef367487b509521fea1325102780))
- tweak button minimum widths ([b7992cb](http://bitbucket.org/thermsio/atalaya/commits/b7992cb6587b9d64c149afeaa3b14149bf8a9aa6))

### Features

- **CORE-1356:** add extra props to Pagination and docs ([488090c](http://bitbucket.org/thermsio/atalaya/commits/488090c2de94d15907ad2001594a70f024fa93b5))
- **CORE-1356:** add pagination logic ([bd78a89](http://bitbucket.org/thermsio/atalaya/commits/bd78a89c1b8790207492d85c18ccb17178954be9))
- **CORE-1356:** add perPagesPicker to Pagination ([5a5c89f](http://bitbucket.org/thermsio/atalaya/commits/5a5c89f8a8dd8c94e46792c28051739cd9996aa4))
- **CORE-1356:** add the ability for Buttons to define their type if Group has not defined it ([769572b](http://bitbucket.org/thermsio/atalaya/commits/769572ba1bcb49a7a5824c586708deb50a7e81bb))
- **CORE-1356:** change pagination logic ([9da935e](http://bitbucket.org/thermsio/atalaya/commits/9da935ebc72f670f876caf71e11ee58bd3728ace))
- **CORE-1356:** change the number of pages displayed depending on isMobile ([c3a29d4](http://bitbucket.org/thermsio/atalaya/commits/c3a29d4dc412f9c3bf61f1bd9d43b83409f5002b))
- **CORE-1356:** make Icon work with a wider kind of svgs ([e72a33d](http://bitbucket.org/thermsio/atalaya/commits/e72a33dae424d45512ebe9dfe3e4688ee3fbccb3))
- **CORE-1356:** upgrade Pagination as an organisms; Export it! ([1a865fd](http://bitbucket.org/thermsio/atalaya/commits/1a865fd94c77c2cbda09d4bacd1b1b22c03a1fc0))
- **CORE-1357:** add autofocus ([756bd8b](http://bitbucket.org/thermsio/atalaya/commits/756bd8bb66d5c68de906e5c122aec2e8ccffda6a))
- **CORE-1357:** add form control wrapper to RichTextEditor ([f92d3bd](http://bitbucket.org/thermsio/atalaya/commits/f92d3bdd0dd2a4a3fb8f61eacfd4892ede2e210a))
- **CORE-1357:** add onChangeValue ([ea2d4c3](http://bitbucket.org/thermsio/atalaya/commits/ea2d4c3a8efcd90885bbbfc2f621be28680f86eb))
- **CORE-1357:** add RichTextEditor ([3b52c77](http://bitbucket.org/thermsio/atalaya/commits/3b52c77ec457f9fa03a64f6286701e62a66088e7))
- **CORE-1358:** add fullWidth prop ([f350fa7](http://bitbucket.org/thermsio/atalaya/commits/f350fa755006b2f6d2d27ec2a51c4e0842ab58a5))
- **CORE-1358:** add internal state management; Flesh out the docs. ([1f71f87](http://bitbucket.org/thermsio/atalaya/commits/1f71f875cf375922b28e0a135ac3a2dd4a6d1920))
- **CORE-1358:** add subtle Badge version ([c0ff9f4](http://bitbucket.org/thermsio/atalaya/commits/c0ff9f4a09ddf92b43aa4b7ba22cab1ef7942829))
- **CORE-1358:** add tabs ([ce4c254](http://bitbucket.org/thermsio/atalaya/commits/ce4c2541d597778a834a6d2a21cf45644eac02ca))
- **CORE-1358:** allow Select to be customized via react-select props ([cc0f70a](http://bitbucket.org/thermsio/atalaya/commits/cc0f70a2884ac522b0c4f07507f6033444d097bc))
- **CORE-1358:** change mobile detection logic; added breakpoint hook and isMobileDevice hook ([9b09715](http://bitbucket.org/thermsio/atalaya/commits/9b09715324c9ad14b760318ad31f4871eb4d18cd))
- **CORE-1358:** export breakpoints ([c0849c5](http://bitbucket.org/thermsio/atalaya/commits/c0849c5abbb219fb2f23cff1a729497060eee6ec))
- **CORE-1358:** export tabs ([8dc38cf](http://bitbucket.org/thermsio/atalaya/commits/8dc38cf87e7260efab1c7c8cb0fc380cac63daf4))
- **CORE-1358:** memoize activeIndex ([e19e159](http://bitbucket.org/thermsio/atalaya/commits/e19e159037db69000ca9505742fd3eb8877960ac))
- **CORE-1358:** use HTML Select if isMobile ([a6c11be](http://bitbucket.org/thermsio/atalaya/commits/a6c11beaaa7883685dd2dee368c52a1dc1671f97))
- **CORE-1358:** use Tab as children instead of the options prop ([49e97a5](http://bitbucket.org/thermsio/atalaya/commits/49e97a5ad6867661e2d1c608bdb26565c305e605))
- **CORE-1358:** when tabs are overflowing, show Select ([155d71d](http://bitbucket.org/thermsio/atalaya/commits/155d71d99757f6c53bea3b354984bf89c7f52d7c))
- **CORE-1361:** add alwaysShow prop ([8370d73](http://bitbucket.org/thermsio/atalaya/commits/8370d736860ebba9edb88494c04680a4943e12fd))
- **CORE-1361:** add CopyText ([6e374bb](http://bitbucket.org/thermsio/atalaya/commits/6e374bbe2ffaaf3004f6e9d116d085b2b593fef2))
- **CORE-1361:** add CopyText docs ([0ad2b52](http://bitbucket.org/thermsio/atalaya/commits/0ad2b52ad1735433270c884a4e0501b8b9f58982))
- **CORE-1508:** make breakpoints extendable for height ([f723756](http://bitbucket.org/thermsio/atalaya/commits/f7237563e111376d89909ddc009c56c24ff99fe6))
- **CORE-1524:** add ComponentsWrapper with theme switcher ([4359828](http://bitbucket.org/thermsio/atalaya/commits/4359828c85b09df00187f63b8d5c47b65a100651))
- **CORE-1524:** add label docs ([1f40fb8](http://bitbucket.org/thermsio/atalaya/commits/1f40fb8c5e1739da383b60fd593d2dc1560c7550))
- **CORE-1524:** add label docs & component; ([2147c8a](http://bitbucket.org/thermsio/atalaya/commits/2147c8ab83add6fdbfb59619f552e959561c5c78))
- **CORE-1542:** add breadcrumbs component ([52e4142](http://bitbucket.org/thermsio/atalaya/commits/52e41420b6747d62512b38e94567aa38aff943e5))
- **CORE-1547:** export Colors constant ([ea6a91f](http://bitbucket.org/thermsio/atalaya/commits/ea6a91f9a3e97bd78038ec58c890e550a9a45438))
- **CORE-1547:** export Constants ([eac05ac](http://bitbucket.org/thermsio/atalaya/commits/eac05ac8b6103e93233943ef30d8e23debd0d229))
- **CORE-1547:** update tailwind theme with xxs/xxl scale ([ad548f9](http://bitbucket.org/thermsio/atalaya/commits/ad548f927c43b4689c0ed731270cb173e888a092))
- new tailwind.js file with published npm package ([13efa58](http://bitbucket.org/thermsio/atalaya/commits/13efa581f3f1ade8c7356d23df71becb28e035e5))
- **TableGroup:** add select checkbox logic and TableGroupContext ([b42d5f4](http://bitbucket.org/thermsio/atalaya/commits/b42d5f44e3dbfbd4c3196625e6e18b7dd0037f4e))
- **TableGroup:** add TableGroup components ([bfc5021](http://bitbucket.org/thermsio/atalaya/commits/bfc5021d54c3110107f3d254d3ec9dbef44626ba))
- **Text:** change TextField to Text export w/ deprecation comment ([a13d10c](http://bitbucket.org/thermsio/atalaya/commits/a13d10c3782f771c2e02e586761f04abf588bba1))

# [3.1.0-beta.27](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.26...v3.1.0-beta.27) (2021-09-25)

### Bug Fixes

- **CORE-1361:** add custom classes to Icons ([7f89fbf](http://bitbucket.org/thermsio/atalaya/commits/7f89fbf510f5158387d65f191c902fc1b7e2da12))

### Features

- **CORE-1361:** add alwaysShow prop ([8370d73](http://bitbucket.org/thermsio/atalaya/commits/8370d736860ebba9edb88494c04680a4943e12fd))
- **CORE-1361:** add CopyText ([6e374bb](http://bitbucket.org/thermsio/atalaya/commits/6e374bbe2ffaaf3004f6e9d116d085b2b593fef2))
- **CORE-1361:** add CopyText docs ([0ad2b52](http://bitbucket.org/thermsio/atalaya/commits/0ad2b52ad1735433270c884a4e0501b8b9f58982))

# [3.1.0-beta.26](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.25...v3.1.0-beta.26) (2021-09-24)

### Features

- **Text:** change TextField to Text export w/ deprecation comment ([a13d10c](http://bitbucket.org/thermsio/atalaya/commits/a13d10c3782f771c2e02e586761f04abf588bba1))

# [3.1.0-beta.25](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.24...v3.1.0-beta.25) (2021-09-24)

### Bug Fixes

- **Skeleton.Text:** use <Inline/> wrapper ([130d13e](http://bitbucket.org/thermsio/atalaya/commits/130d13ee9ca6c22356b92d33efc92e4fb49fe10d))

# [3.1.0-beta.24](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.23...v3.1.0-beta.24) (2021-09-24)

### Bug Fixes

- **Skeleton.Text:** don't wrap <div> if no lines prop passed ([9e14ad9](http://bitbucket.org/thermsio/atalaya/commits/9e14ad9803c5d1992b3ecc701846f392dae6e183))

# [3.1.0-beta.23](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.22...v3.1.0-beta.23) (2021-09-21)

### Features

- **CORE-1542:** add breadcrumbs component ([52e4142](http://bitbucket.org/thermsio/atalaya/commits/52e41420b6747d62512b38e94567aa38aff943e5))

# [3.1.0-beta.22](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.21...v3.1.0-beta.22) (2021-09-21)

### Features

- **CORE-1508:** make breakpoints extendable for height ([f723756](http://bitbucket.org/thermsio/atalaya/commits/f7237563e111376d89909ddc009c56c24ff99fe6))

# [3.1.0-beta.21](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.20...v3.1.0-beta.21) (2021-09-20)

### Bug Fixes

- **CORE-1547:** fix build errors ([1097bf0](http://bitbucket.org/thermsio/atalaya/commits/1097bf020736ea0ddda4e26529345c9e632db501))

### Features

- **CORE-1547:** export Colors constant ([ea6a91f](http://bitbucket.org/thermsio/atalaya/commits/ea6a91f9a3e97bd78038ec58c890e550a9a45438))
- **CORE-1547:** export Constants ([eac05ac](http://bitbucket.org/thermsio/atalaya/commits/eac05ac8b6103e93233943ef30d8e23debd0d229))
- **CORE-1547:** update tailwind theme with xxs/xxl scale ([ad548f9](http://bitbucket.org/thermsio/atalaya/commits/ad548f927c43b4689c0ed731270cb173e888a092))

# [3.1.0-beta.20](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.19...v3.1.0-beta.20) (2021-09-20)

### Bug Fixes

- **ToggleSwitch:** active={undefined | boolean} ([1d9b62c](http://bitbucket.org/thermsio/atalaya/commits/1d9b62c4f62eef367487b509521fea1325102780))

# [3.1.0-beta.19](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.18...v3.1.0-beta.19) (2021-09-18)

### Features

- new tailwind.js file with published npm package ([13efa58](http://bitbucket.org/thermsio/atalaya/commits/13efa581f3f1ade8c7356d23df71becb28e035e5))

# [3.1.0-beta.18](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.17...v3.1.0-beta.18) (2021-09-17)

### Bug Fixes

- files deployed w/ npm package to include patches/ dir ([c5c55f6](http://bitbucket.org/thermsio/atalaya/commits/c5c55f67bea7efbbbf3834a098445a77df7c3204))

# [3.1.0-beta.17](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.16...v3.1.0-beta.17) (2021-09-17)

### Bug Fixes

- remove .env file ([bf69f5e](http://bitbucket.org/thermsio/atalaya/commits/bf69f5ea30ca006146e2949bd1dc2ac70936d891))

# [3.1.0-beta.16](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.15...v3.1.0-beta.16) (2021-09-17)

### Bug Fixes

- readme and bumping for react-draft-wysiwyg patch version publish ([e9eac1f](http://bitbucket.org/thermsio/atalaya/commits/e9eac1f085268c4918bfaf1216a913119cc2f818))

# [3.1.0-beta.15](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.14...v3.1.0-beta.15) (2021-09-14)

### Bug Fixes

- fix incorrect icon size ([19560c0](http://bitbucket.org/thermsio/atalaya/commits/19560c091031ddbf4db24565ecc23d43c6ebd002))

# [3.1.0-beta.14](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.13...v3.1.0-beta.14) (2021-09-07)

### Bug Fixes

- add tailwind in dependencies list ([2425796](http://bitbucket.org/thermsio/atalaya/commits/2425796b46c02341e845cd24a2f3039d93f717c5))

# [3.1.0-beta.13](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.12...v3.1.0-beta.13) (2021-08-26)

### Bug Fixes

- **CORE-1357:** fix RichTextEditor not loading toolbar icons, for real. ([3de317b](http://bitbucket.org/thermsio/atalaya/commits/3de317b1c1aa7efecd0299a2037f93fd799dc598))
- **CORE-1524:** simplify ComponentsWrapper Props ([d562a4a](http://bitbucket.org/thermsio/atalaya/commits/d562a4a87e8af906c2110c5ec8aa1879145c680f))

# [3.1.0-beta.12](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.11...v3.1.0-beta.12) (2021-08-26)

### Features

- **CORE-1524:** add ComponentsWrapper with theme switcher ([4359828](http://bitbucket.org/thermsio/atalaya/commits/4359828c85b09df00187f63b8d5c47b65a100651))
- **CORE-1524:** add label docs ([1f40fb8](http://bitbucket.org/thermsio/atalaya/commits/1f40fb8c5e1739da383b60fd593d2dc1560c7550))
- **CORE-1524:** add label docs & component; ([2147c8a](http://bitbucket.org/thermsio/atalaya/commits/2147c8ab83add6fdbfb59619f552e959561c5c78))

# [3.1.0-beta.11](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.10...v3.1.0-beta.11) (2021-08-26)

### Bug Fixes

- **CORE-1357:** fix toolbar icons not showing; ([140a2d0](http://bitbucket.org/thermsio/atalaya/commits/140a2d035eae46cd0a031c974dca69c47f71741c))

# [3.1.0-beta.10](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.9...v3.1.0-beta.10) (2021-08-05)

### Bug Fixes

- exports added useDarkModeTheme ([35152af](http://bitbucket.org/thermsio/atalaya/commits/35152afd7be8550bfabebfd58dda0cd180937210))
- **TableGroup:** docs example component ([e45b07a](http://bitbucket.org/thermsio/atalaya/commits/e45b07ae90cef01514ae2aaa6b6dfaf15036cc38))

# [3.1.0-beta.9](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.8...v3.1.0-beta.9) (2021-08-05)

### Features

- **TableGroup:** add select checkbox logic and TableGroupContext ([b42d5f4](http://bitbucket.org/thermsio/atalaya/commits/b42d5f44e3dbfbd4c3196625e6e18b7dd0037f4e))

# [3.1.0-beta.8](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.7...v3.1.0-beta.8) (2021-08-05)

### Features

- **TableGroup:** add TableGroup components ([bfc5021](http://bitbucket.org/thermsio/atalaya/commits/bfc5021d54c3110107f3d254d3ec9dbef44626ba))

# [3.1.0-beta.7](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.6...v3.1.0-beta.7) (2021-06-07)

### Bug Fixes

- **CORE-1358:** make fullWidth tabs only turning into select when minimum size has been reached ([9ff9a18](http://bitbucket.org/thermsio/atalaya/commits/9ff9a18cc182fa25ce1a3e2d4aed27f18dbe3217))
- **CORE-1358:** remove console.logs ([1bcc222](http://bitbucket.org/thermsio/atalaya/commits/1bcc2222e066eb5efcfe1aac972352e1076ba573))

# [3.1.0-beta.6](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.5...v3.1.0-beta.6) (2021-06-07)

### Bug Fixes

- **CORE-1358:** Stop Select custom css from getting purged ([59e12d3](http://bitbucket.org/thermsio/atalaya/commits/59e12d356ca56b8c0073fce8f72d0d06d18ec554))
- **CORE-1358:** tailwind config ([9c799e0](http://bitbucket.org/thermsio/atalaya/commits/9c799e043eea265253ab924458744138e69d6a6f))

# [3.1.0-beta.5](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.4...v3.1.0-beta.5) (2021-06-05)

### Bug Fixes

- build remove inject env vars for NODE_ENV to remove \_virtual dir file with no extension ([ede2d8e](http://bitbucket.org/thermsio/atalaya/commits/ede2d8e71c3453e2d783eb393f2a2dd5f7cddbb8))

# [3.1.0-beta.4](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.3...v3.1.0-beta.4) (2021-06-04)

### Bug Fixes

- **CORE-1358:** change active option logic; use the index instead of the option itself for tracking it ([c1b17c9](http://bitbucket.org/thermsio/atalaya/commits/c1b17c9a155e43bcf13519c6f3764a7d99d9a4d6))
- **CORE-1358:** Showing Select on first render regardless of component width ([ab75272](http://bitbucket.org/thermsio/atalaya/commits/ab75272498f24b728056d45e0e2552b15864b8fc))

### Features

- **CORE-1358:** add fullWidth prop ([f350fa7](http://bitbucket.org/thermsio/atalaya/commits/f350fa755006b2f6d2d27ec2a51c4e0842ab58a5))
- **CORE-1358:** add internal state management; Flesh out the docs. ([1f71f87](http://bitbucket.org/thermsio/atalaya/commits/1f71f875cf375922b28e0a135ac3a2dd4a6d1920))
- **CORE-1358:** add subtle Badge version ([c0ff9f4](http://bitbucket.org/thermsio/atalaya/commits/c0ff9f4a09ddf92b43aa4b7ba22cab1ef7942829))
- **CORE-1358:** add tabs ([ce4c254](http://bitbucket.org/thermsio/atalaya/commits/ce4c2541d597778a834a6d2a21cf45644eac02ca))
- **CORE-1358:** allow Select to be customized via react-select props ([cc0f70a](http://bitbucket.org/thermsio/atalaya/commits/cc0f70a2884ac522b0c4f07507f6033444d097bc))
- **CORE-1358:** change mobile detection logic; added breakpoint hook and isMobileDevice hook ([9b09715](http://bitbucket.org/thermsio/atalaya/commits/9b09715324c9ad14b760318ad31f4871eb4d18cd))
- **CORE-1358:** export breakpoints ([c0849c5](http://bitbucket.org/thermsio/atalaya/commits/c0849c5abbb219fb2f23cff1a729497060eee6ec))
- **CORE-1358:** export tabs ([8dc38cf](http://bitbucket.org/thermsio/atalaya/commits/8dc38cf87e7260efab1c7c8cb0fc380cac63daf4))
- **CORE-1358:** memoize activeIndex ([e19e159](http://bitbucket.org/thermsio/atalaya/commits/e19e159037db69000ca9505742fd3eb8877960ac))
- **CORE-1358:** use HTML Select if isMobile ([a6c11be](http://bitbucket.org/thermsio/atalaya/commits/a6c11beaaa7883685dd2dee368c52a1dc1671f97))
- **CORE-1358:** use Tab as children instead of the options prop ([49e97a5](http://bitbucket.org/thermsio/atalaya/commits/49e97a5ad6867661e2d1c608bdb26565c305e605))
- **CORE-1358:** when tabs are overflowing, show Select ([155d71d](http://bitbucket.org/thermsio/atalaya/commits/155d71d99757f6c53bea3b354984bf89c7f52d7c))

# [3.1.0-beta.3](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.2...v3.1.0-beta.3) (2021-06-02)

### Features

- **CORE-1356:** make Icon work with a wider kind of svgs ([e72a33d](http://bitbucket.org/thermsio/atalaya/commits/e72a33dae424d45512ebe9dfe3e4688ee3fbccb3))
- **CORE-1357:** add autofocus ([756bd8b](http://bitbucket.org/thermsio/atalaya/commits/756bd8bb66d5c68de906e5c122aec2e8ccffda6a))
- **CORE-1357:** add form control wrapper to RichTextEditor ([f92d3bd](http://bitbucket.org/thermsio/atalaya/commits/f92d3bdd0dd2a4a3fb8f61eacfd4892ede2e210a))
- **CORE-1357:** add onChangeValue ([ea2d4c3](http://bitbucket.org/thermsio/atalaya/commits/ea2d4c3a8efcd90885bbbfc2f621be28680f86eb))
- **CORE-1357:** add RichTextEditor ([3b52c77](http://bitbucket.org/thermsio/atalaya/commits/3b52c77ec457f9fa03a64f6286701e62a66088e7))

# [3.1.0-beta.2](http://bitbucket.org/thermsio/atalaya/compare/v3.1.0-beta.1...v3.1.0-beta.2) (2021-05-26)

### Features

- **CORE-1356:** upgrade Pagination as an organisms; Export it! ([1a865fd](http://bitbucket.org/thermsio/atalaya/commits/1a865fd94c77c2cbda09d4bacd1b1b22c03a1fc0))

# [3.1.0-beta.1](http://bitbucket.org/thermsio/atalaya/compare/v3.0.0...v3.1.0-beta.1) (2021-05-26)

### Bug Fixes

- **CORE-1356:** make ButtonGroup more lenient about it's children ([0e16438](http://bitbucket.org/thermsio/atalaya/commits/0e16438f596450992e957661a3effde2f433b51b))
- **CORE-1356:** make HTML Select work with numbers ([7dfa2f5](http://bitbucket.org/thermsio/atalaya/commits/7dfa2f5e2d05d45d1bb1e001e72f6f28ca2a90f6))
- **CORE-1356:** make Inline width work with wrap ([d198678](http://bitbucket.org/thermsio/atalaya/commits/d1986789ffe4de2975448e22344754d49c6b4f0e))
- text color not being subtle ([3b3fd1a](http://bitbucket.org/thermsio/atalaya/commits/3b3fd1ab9bf6517bcf4043185aacc508248e69ef))
- tweak button minimum widths ([b7992cb](http://bitbucket.org/thermsio/atalaya/commits/b7992cb6587b9d64c149afeaa3b14149bf8a9aa6))

### Features

- **CORE-1356:** add extra props to Pagination and docs ([488090c](http://bitbucket.org/thermsio/atalaya/commits/488090c2de94d15907ad2001594a70f024fa93b5))
- **CORE-1356:** add pagination logic ([bd78a89](http://bitbucket.org/thermsio/atalaya/commits/bd78a89c1b8790207492d85c18ccb17178954be9))
- **CORE-1356:** add perPagesPicker to Pagination ([5a5c89f](http://bitbucket.org/thermsio/atalaya/commits/5a5c89f8a8dd8c94e46792c28051739cd9996aa4))
- **CORE-1356:** add the ability for Buttons to define their type if Group has not defined it ([769572b](http://bitbucket.org/thermsio/atalaya/commits/769572ba1bcb49a7a5824c586708deb50a7e81bb))
- **CORE-1356:** change pagination logic ([9da935e](http://bitbucket.org/thermsio/atalaya/commits/9da935ebc72f670f876caf71e11ee58bd3728ace))
- **CORE-1356:** change the number of pages displayed depending on isMobile ([c3a29d4](http://bitbucket.org/thermsio/atalaya/commits/c3a29d4dc412f9c3bf61f1bd9d43b83409f5002b))

# [3.0.0](http://bitbucket.org/thermsio/atalaya/compare/v2.2.0...v3.0.0) (2021-05-24)

### Bug Fixes

- DateTimePicker fields not filling their container; ([0c40af0](http://bitbucket.org/thermsio/atalaya/commits/0c40af04d312310aa834e080563a2f6a4e04996c))
- make Inline actually be inline ([95af9aa](http://bitbucket.org/thermsio/atalaya/commits/95af9aa739cb5c02d38c949df7f8f57e6e4d4190))
- make Inline wrap work as intended ([8e86cdb](http://bitbucket.org/thermsio/atalaya/commits/8e86cdbf2f2a4ced084527cdeb600bf990073a2a))
- properly group components inside atomic design categories; Makes alphabetical sorting work. ([26a21b1](http://bitbucket.org/thermsio/atalaya/commits/26a21b178e66e8ea54f57ab66f5e043b0c1225b5))
- tests ([df9abf8](http://bitbucket.org/thermsio/atalaya/commits/df9abf8d7d0bb976419d79cebfada017bef6d8a7))
- tests ([c26b0dd](http://bitbucket.org/thermsio/atalaya/commits/c26b0dda740eea985d24a9767f40f18ba5bf91ec))
- wrongly reverting children spacing if alignY === end ([62e2760](http://bitbucket.org/thermsio/atalaya/commits/62e2760da0300eef387fedfc894ba8ad31562af8))

### Features

- Card component uses now Stack under the hood; update Badge and Card docs ([a7ba8f3](http://bitbucket.org/thermsio/atalaya/commits/a7ba8f38394b4e653722194ecc6ef2a4673266c6))
- directly export modules from index ([968ddb2](http://bitbucket.org/thermsio/atalaya/commits/968ddb21e6a1d31341d58137390ff95bb4f13e70))
- enable declaring divProps directly in layout component. ([0505a96](http://bitbucket.org/thermsio/atalaya/commits/0505a965611f2141d416c5e99075d6bc99af5b85))
- persist theme choice between sessions ([ab73153](http://bitbucket.org/thermsio/atalaya/commits/ab7315378ab90a8e9415677605eea779c201f459))
- remove default exports ([03a7a5c](http://bitbucket.org/thermsio/atalaya/commits/03a7a5c2877be72143a46b6d4a96da224a5ec4bc))
- Update ActionModal; update ActionModal docs ([748ae74](http://bitbucket.org/thermsio/atalaya/commits/748ae747e30d87bb193ec5bec54f8a79d9b6fb27))
- update color docs ([c8c7145](http://bitbucket.org/thermsio/atalaya/commits/c8c7145f1cdb82f92b4b26ee8d0088c308f3efd3))
- update Columns doc ([c681ef3](http://bitbucket.org/thermsio/atalaya/commits/c681ef30011cc7f916fa4fdc20e148f776ff3a72))
- update Icon docs ([6e895bc](http://bitbucket.org/thermsio/atalaya/commits/6e895bcd74d1e02e36e6942d240095a293c48b63))
- update icons docs ([24a9073](http://bitbucket.org/thermsio/atalaya/commits/24a9073268b1fb2ffcb2b6aa25fc2acadc6c30ed))
- update Image docs tweak Image component behaviour ([6a8efd0](http://bitbucket.org/thermsio/atalaya/commits/6a8efd0c551248d042ed51441d1ccb6efd0e2741))
- update Inline and Stack documentation ([1854c99](http://bitbucket.org/thermsio/atalaya/commits/1854c996dbccfd0776784f00b04b9fd8b09c6696))
- update Inline doc ([29bd030](http://bitbucket.org/thermsio/atalaya/commits/29bd030a3e05440ab0f7376d65ff042aa0d50a67))
- update layout prop description ([3a4e450](http://bitbucket.org/thermsio/atalaya/commits/3a4e450e4dde8a133ece018cab3a99aa084a4623))
- Update Modal; update Modal docs ([2c8845f](http://bitbucket.org/thermsio/atalaya/commits/2c8845ffafb0240307e2402cca1fa9ba25e94b33))
- update spacing docs ([44633ec](http://bitbucket.org/thermsio/atalaya/commits/44633ecea61b90e2496ec1e7dc5c46647d519968))
- Update TagGroup logic; update Tag docs ([573acd1](http://bitbucket.org/thermsio/atalaya/commits/573acd1033829178556f4b406a8970256d3ad440))
- update TextArea docs ([3ad66cd](http://bitbucket.org/thermsio/atalaya/commits/3ad66cd0913b56a782434b198261fbd7e53183d7))
- Update Toasts docs; ([defb737](http://bitbucket.org/thermsio/atalaya/commits/defb737e8a88a8b31b695224ace63ed7062ed1d6))
- update typography docs ([9f98c05](http://bitbucket.org/thermsio/atalaya/commits/9f98c059147165e63444f27bfb845015bfdfa568))

### BREAKING CHANGES

- Layout components no longer use the divProps prop; Now those props can be declared on the component as you would any other prop.

# [3.0.0-beta.1](http://bitbucket.org/thermsio/atalaya/compare/v2.2.0...v3.0.0-beta.1) (2021-05-24)

### Bug Fixes

- DateTimePicker fields not filling their container; ([0c40af0](http://bitbucket.org/thermsio/atalaya/commits/0c40af04d312310aa834e080563a2f6a4e04996c))
- make Inline actually be inline ([95af9aa](http://bitbucket.org/thermsio/atalaya/commits/95af9aa739cb5c02d38c949df7f8f57e6e4d4190))
- make Inline wrap work as intended ([8e86cdb](http://bitbucket.org/thermsio/atalaya/commits/8e86cdbf2f2a4ced084527cdeb600bf990073a2a))
- properly group components inside atomic design categories; Makes alphabetical sorting work. ([26a21b1](http://bitbucket.org/thermsio/atalaya/commits/26a21b178e66e8ea54f57ab66f5e043b0c1225b5))
- tests ([df9abf8](http://bitbucket.org/thermsio/atalaya/commits/df9abf8d7d0bb976419d79cebfada017bef6d8a7))
- tests ([c26b0dd](http://bitbucket.org/thermsio/atalaya/commits/c26b0dda740eea985d24a9767f40f18ba5bf91ec))
- wrongly reverting children spacing if alignY === end ([62e2760](http://bitbucket.org/thermsio/atalaya/commits/62e2760da0300eef387fedfc894ba8ad31562af8))

### Features

- Card component uses now Stack under the hood; update Badge and Card docs ([a7ba8f3](http://bitbucket.org/thermsio/atalaya/commits/a7ba8f38394b4e653722194ecc6ef2a4673266c6))
- directly export modules from index ([968ddb2](http://bitbucket.org/thermsio/atalaya/commits/968ddb21e6a1d31341d58137390ff95bb4f13e70))
- enable declaring divProps directly in layout component. ([0505a96](http://bitbucket.org/thermsio/atalaya/commits/0505a965611f2141d416c5e99075d6bc99af5b85))
- persist theme choice between sessions ([ab73153](http://bitbucket.org/thermsio/atalaya/commits/ab7315378ab90a8e9415677605eea779c201f459))
- remove default exports ([03a7a5c](http://bitbucket.org/thermsio/atalaya/commits/03a7a5c2877be72143a46b6d4a96da224a5ec4bc))
- Update ActionModal; update ActionModal docs ([748ae74](http://bitbucket.org/thermsio/atalaya/commits/748ae747e30d87bb193ec5bec54f8a79d9b6fb27))
- update color docs ([c8c7145](http://bitbucket.org/thermsio/atalaya/commits/c8c7145f1cdb82f92b4b26ee8d0088c308f3efd3))
- update Columns doc ([c681ef3](http://bitbucket.org/thermsio/atalaya/commits/c681ef30011cc7f916fa4fdc20e148f776ff3a72))
- update Icon docs ([6e895bc](http://bitbucket.org/thermsio/atalaya/commits/6e895bcd74d1e02e36e6942d240095a293c48b63))
- update icons docs ([24a9073](http://bitbucket.org/thermsio/atalaya/commits/24a9073268b1fb2ffcb2b6aa25fc2acadc6c30ed))
- update Image docs tweak Image component behaviour ([6a8efd0](http://bitbucket.org/thermsio/atalaya/commits/6a8efd0c551248d042ed51441d1ccb6efd0e2741))
- update Inline and Stack documentation ([1854c99](http://bitbucket.org/thermsio/atalaya/commits/1854c996dbccfd0776784f00b04b9fd8b09c6696))
- update Inline doc ([29bd030](http://bitbucket.org/thermsio/atalaya/commits/29bd030a3e05440ab0f7376d65ff042aa0d50a67))
- update layout prop description ([3a4e450](http://bitbucket.org/thermsio/atalaya/commits/3a4e450e4dde8a133ece018cab3a99aa084a4623))
- Update Modal; update Modal docs ([2c8845f](http://bitbucket.org/thermsio/atalaya/commits/2c8845ffafb0240307e2402cca1fa9ba25e94b33))
- update spacing docs ([44633ec](http://bitbucket.org/thermsio/atalaya/commits/44633ecea61b90e2496ec1e7dc5c46647d519968))
- Update TagGroup logic; update Tag docs ([573acd1](http://bitbucket.org/thermsio/atalaya/commits/573acd1033829178556f4b406a8970256d3ad440))
- update TextArea docs ([3ad66cd](http://bitbucket.org/thermsio/atalaya/commits/3ad66cd0913b56a782434b198261fbd7e53183d7))
- Update Toasts docs; ([defb737](http://bitbucket.org/thermsio/atalaya/commits/defb737e8a88a8b31b695224ace63ed7062ed1d6))
- update typography docs ([9f98c05](http://bitbucket.org/thermsio/atalaya/commits/9f98c059147165e63444f27bfb845015bfdfa568))

### BREAKING CHANGES

- Layout components no longer use the divProps prop; Now those props can be declared on the component as you would any other prop.

# [2.2.0](http://bitbucket.org/thermsio/atalaya/compare/v2.1.1...v2.2.0) (2021-05-19)

### Bug Fixes

- **CORE-1347:** stop Skeleton from breaking the layout on Image ([4951495](http://bitbucket.org/thermsio/atalaya/commits/4951495a0d8fcc9a74e64e08979248f3c87c9825))
- **CORE-1347:** update layout fontSize options ([4d09e65](http://bitbucket.org/thermsio/atalaya/commits/4d09e65eac6d59f192053ef9175e4010aa031f44))
- getResponsiveClasses error logging ([4c0d9c3](http://bitbucket.org/thermsio/atalaya/commits/4c0d9c32572baa06455e33d457dc7c7c58a241b1))

### Features

- **CORE-1347:** add chars prop to Skeleton.Text ([d2b7a0f](http://bitbucket.org/thermsio/atalaya/commits/d2b7a0fb47e54a237cc7282c6988f84a5a1e1e28))
- **CORE-1347:** add Skeleton components ([6e2a689](http://bitbucket.org/thermsio/atalaya/commits/6e2a6897df966d17f88a87f7568a489d454eb915))
- **CORE-1347:** add Skeleton support to Image ([32b4bed](http://bitbucket.org/thermsio/atalaya/commits/32b4bedc6044786b1fc1904b0541cb9dc95d3f10))

# [2.2.0-beta.2](http://bitbucket.org/thermsio/atalaya/compare/v2.2.0-beta.1...v2.2.0-beta.2) (2021-05-19)

### Features

- **CORE-1347:** add chars prop to Skeleton.Text ([d2b7a0f](http://bitbucket.org/thermsio/atalaya/commits/d2b7a0fb47e54a237cc7282c6988f84a5a1e1e28))

# [2.2.0-beta.1](http://bitbucket.org/thermsio/atalaya/compare/v2.1.1...v2.2.0-beta.1) (2021-05-18)

### Bug Fixes

- **CORE-1347:** stop Skeleton from breaking the layout on Image ([4951495](http://bitbucket.org/thermsio/atalaya/commits/4951495a0d8fcc9a74e64e08979248f3c87c9825))
- **CORE-1347:** update layout fontSize options ([4d09e65](http://bitbucket.org/thermsio/atalaya/commits/4d09e65eac6d59f192053ef9175e4010aa031f44))
- getResponsiveClasses error logging ([4c0d9c3](http://bitbucket.org/thermsio/atalaya/commits/4c0d9c32572baa06455e33d457dc7c7c58a241b1))

### Features

- **CORE-1347:** add Skeleton components ([6e2a689](http://bitbucket.org/thermsio/atalaya/commits/6e2a6897df966d17f88a87f7568a489d454eb915))
- **CORE-1347:** add Skeleton support to Image ([32b4bed](http://bitbucket.org/thermsio/atalaya/commits/32b4bedc6044786b1fc1904b0541cb9dc95d3f10))

## [2.1.1](http://bitbucket.org/thermsio/atalaya/compare/v2.1.0...v2.1.1) (2021-05-17)

### Bug Fixes

- getResponsiveClasses crashing if wrong input type is summited ([c457eaf](http://bitbucket.org/thermsio/atalaya/commits/c457eafb81ef68019759d050d492db1facefd167))
- pipeline ([a540729](http://bitbucket.org/thermsio/atalaya/commits/a54072926e7481426a6f813db485951002c77a43))

# [2.1.0](http://bitbucket.org/thermsio/atalaya/compare/v2.0.1...v2.1.0) (2021-05-17)

### Features

- force publish ([1679a07](http://bitbucket.org/thermsio/atalaya/commits/1679a07f4c122a890b9947041ab0c39fbb3e5bd9))

## [2.0.1](http://bitbucket.org/thermsio/atalaya/compare/v2.0.0...v2.0.1) (2021-05-17)

### Bug Fixes

- install not working with npm 7 ([1e4d73c](http://bitbucket.org/thermsio/atalaya/commits/1e4d73c59a53413cdcc5ddc5c0bea7be71dbae25))

# [2.0.0](http://bitbucket.org/thermsio/atalaya/compare/v1.5.2...v2.0.0) (2021-05-17)

### Bug Fixes

- **CORE-1286:** fix ButtonGroup docs ([137ba22](http://bitbucket.org/thermsio/atalaya/commits/137ba2260fcfe4fc861a3fdc3c238df9a92ff899))
- **CORE-1286:** fix Columns not working with conditional expressions ([335d850](http://bitbucket.org/thermsio/atalaya/commits/335d8501959866eda762526109cf5ca7a20eb61d))
- **CORE-1286:** fix toast not having default text colors ([53fafeb](http://bitbucket.org/thermsio/atalaya/commits/53fafeb312cb6afd7f0993afa8eb817361507406))
- **CORE-1286:** mobile toast being too short ([fc522b9](http://bitbucket.org/thermsio/atalaya/commits/fc522b9eaf7bd4c0e3507d8a94c1a15d0460e69b))
- **CORE-1286:** publish Avatar. ([263f120](http://bitbucket.org/thermsio/atalaya/commits/263f1202e03b178f90ec8dd4032ae18d49b52037))
- **CORE-1286:** test failing ([c0c9cae](http://bitbucket.org/thermsio/atalaya/commits/c0c9caea389a4cbd28530caad70b34bac12bd80e))
- **CORE-1337:** fix toast default style css breaking bundle ([dfc036c](http://bitbucket.org/thermsio/atalaya/commits/dfc036c1a7cdbb208f8e529234200d8134bace7e))
- make ColorCard.tsx full width ([57f8dfa](http://bitbucket.org/thermsio/atalaya/commits/57f8dfa94509d978197b367488d4b625bb9d28df))
- zIndices name; ([86d6e37](http://bitbucket.org/thermsio/atalaya/commits/86d6e3723f69ba4bf26c55f7c86797de60edd054))
- **CORE-1337:** undo extra spin class for icon ([283a886](http://bitbucket.org/thermsio/atalaya/commits/283a8860accb9ac3ebf2670e88b7798456d4784f))

### Features

- **CORE-1286:** add NotificationToast and documentation ([f2d604b](http://bitbucket.org/thermsio/atalaya/commits/f2d604b67aea0149371da549a4aef2af8681f7d6))
- **CORE-1286:** add title prop ([dcf1c39](http://bitbucket.org/thermsio/atalaya/commits/dcf1c39f99714636c2bb6af6c5b31ce031b6fc58))
- **CORE-1286:** Add toast with Avatar component. ([fa94210](http://bitbucket.org/thermsio/atalaya/commits/fa942101a6c94c027a6fe2799e38d744c958d578))
- **CORE-1286:** add ToastContainer ([dfbad7a](http://bitbucket.org/thermsio/atalaya/commits/dfbad7a4e7de6e71b27aeb96ae4cef894d633651))
- **CORE-1286:** add ToastContainer and toastManager docs ([fd8d575](http://bitbucket.org/thermsio/atalaya/commits/fd8d5759cda0a6659d0465648a6a25be3eaceba2))
- **CORE-1286:** add toastManager ([09f7c26](http://bitbucket.org/thermsio/atalaya/commits/09f7c26a7468a243744c1b21d54c381374be53d3))
- **CORE-1286:** add toasts.css ([e498d56](http://bitbucket.org/thermsio/atalaya/commits/e498d56ae5c3bd25964c467740fc911eafbfde1c))
- **CORE-1286:** Add warning when trying to display a toast with no ToastContainer. ([69ac765](http://bitbucket.org/thermsio/atalaya/commits/69ac7659dd861ae3b189c4f9d63ebc46ab698be7))
- **CORE-1286:** export config options ([82f838b](http://bitbucket.org/thermsio/atalaya/commits/82f838b1ca93e6acaec29b61dce10cc443f0d333))
- **CORE-1286:** make ToastContainer optional ([eb03075](http://bitbucket.org/thermsio/atalaya/commits/eb03075b788b424f52ae07cd5dbdb4cc5f45bafa))
- **CORE-1286:** Make toasts wider. ([2a3129f](http://bitbucket.org/thermsio/atalaya/commits/2a3129f5dfe4f25e5084f2d02308a9c9ebb79dbf))
- **CORE-1286:** Publish Toasts ([03d7107](http://bitbucket.org/thermsio/atalaya/commits/03d7107b643be664e1cf432062eac149f147811d))
- **CORE-1286:** rename NotificationToast to Toast, update docs. ([77bb039](http://bitbucket.org/thermsio/atalaya/commits/77bb039ee59f322489a61348de102c6025383b1c))
- **CORE-1337:** add color prop to Loading ([3ce1562](http://bitbucket.org/thermsio/atalaya/commits/3ce15624461408ab5bf89101a7ad7f0c74755558))
- **CORE-1337:** add ease spin animation ([b09ec37](http://bitbucket.org/thermsio/atalaya/commits/b09ec378bf1d9b38f49e072594c5df0634f00905))
- **CORE-1337:** add Loading component ([65d672f](http://bitbucket.org/thermsio/atalaya/commits/65d672fa57db0de794e03319e275dbe5631a81e5))
- **CORE-1337:** add loading prop to button ([7d6fa85](http://bitbucket.org/thermsio/atalaya/commits/7d6fa85bf4bff3a3b803d7b4600713962888936d))
- **CORE-1337:** add loading prop to FormLayout ([f2856d7](http://bitbucket.org/thermsio/atalaya/commits/f2856d70d400e5d1bf8ada4d019263a3304d821b))
- **CORE-1337:** export Loading component ([28f8e0f](http://bitbucket.org/thermsio/atalaya/commits/28f8e0f5346a7d6ff087365f85cc87c83eddb7cd))
- **CORE-1337:** let people know they control spinner color ([e8adc83](http://bitbucket.org/thermsio/atalaya/commits/e8adc8338bd0190023c1f63e62ab6f1eda711d23))
- **CORE-1337:** update FormLayout with new Loading API ([7de88db](http://bitbucket.org/thermsio/atalaya/commits/7de88dbbfd5ed712b2c2f47903f074a705ec9689))
- **CORE-1337:** update spinner icon ([8d89ebb](http://bitbucket.org/thermsio/atalaya/commits/8d89ebba683fe3c264a69120763b03b9aee95f25))
- **CORE-1346:** add height and width properties to layout components; ([84f9f5e](http://bitbucket.org/thermsio/atalaya/commits/84f9f5ec25077daeb44e0dd92d9a4ef11f2085c2))
- update light theme colors ([ec79677](http://bitbucket.org/thermsio/atalaya/commits/ec7967788e870fab362273210405424a7a7f68b2))

### BREAKING CHANGES

- **CORE-1346:** replaced fillY prop from Column and Stack with height:"full"

# [2.0.0-beta.1](http://bitbucket.org/thermsio/atalaya/compare/v1.6.0-beta.4...v2.0.0-beta.1) (2021-05-17)

### Features

- **CORE-1346:** add height and width properties to layout components; ([84f9f5e](http://bitbucket.org/thermsio/atalaya/commits/84f9f5ec25077daeb44e0dd92d9a4ef11f2085c2))

### BREAKING CHANGES

- **CORE-1346:** replaced fillY prop from Column and Stack with height:"full"

# [1.6.0-beta.4](http://bitbucket.org/thermsio/atalaya/compare/v1.6.0-beta.3...v1.6.0-beta.4) (2021-05-17)

### Bug Fixes

- **CORE-1337:** fix toast default style css breaking bundle ([dfc036c](http://bitbucket.org/thermsio/atalaya/commits/dfc036c1a7cdbb208f8e529234200d8134bace7e))

# [1.6.0-beta.3](http://bitbucket.org/thermsio/atalaya/compare/v1.6.0-beta.2...v1.6.0-beta.3) (2021-05-17)

### Bug Fixes

- **CORE-1286:** fix ButtonGroup docs ([137ba22](http://bitbucket.org/thermsio/atalaya/commits/137ba2260fcfe4fc861a3fdc3c238df9a92ff899))
- **CORE-1286:** fix Columns not working with conditional expressions ([335d850](http://bitbucket.org/thermsio/atalaya/commits/335d8501959866eda762526109cf5ca7a20eb61d))
- **CORE-1286:** fix toast not having default text colors ([53fafeb](http://bitbucket.org/thermsio/atalaya/commits/53fafeb312cb6afd7f0993afa8eb817361507406))
- **CORE-1286:** mobile toast being too short ([fc522b9](http://bitbucket.org/thermsio/atalaya/commits/fc522b9eaf7bd4c0e3507d8a94c1a15d0460e69b))
- **CORE-1286:** publish Avatar. ([263f120](http://bitbucket.org/thermsio/atalaya/commits/263f1202e03b178f90ec8dd4032ae18d49b52037))
- **CORE-1286:** test failing ([c0c9cae](http://bitbucket.org/thermsio/atalaya/commits/c0c9caea389a4cbd28530caad70b34bac12bd80e))

### Features

- **CORE-1286:** add NotificationToast and documentation ([f2d604b](http://bitbucket.org/thermsio/atalaya/commits/f2d604b67aea0149371da549a4aef2af8681f7d6))
- **CORE-1286:** add title prop ([dcf1c39](http://bitbucket.org/thermsio/atalaya/commits/dcf1c39f99714636c2bb6af6c5b31ce031b6fc58))
- **CORE-1286:** Add toast with Avatar component. ([fa94210](http://bitbucket.org/thermsio/atalaya/commits/fa942101a6c94c027a6fe2799e38d744c958d578))
- **CORE-1286:** add ToastContainer ([dfbad7a](http://bitbucket.org/thermsio/atalaya/commits/dfbad7a4e7de6e71b27aeb96ae4cef894d633651))
- **CORE-1286:** add ToastContainer and toastManager docs ([fd8d575](http://bitbucket.org/thermsio/atalaya/commits/fd8d5759cda0a6659d0465648a6a25be3eaceba2))
- **CORE-1286:** add toastManager ([09f7c26](http://bitbucket.org/thermsio/atalaya/commits/09f7c26a7468a243744c1b21d54c381374be53d3))
- **CORE-1286:** add toasts.css ([e498d56](http://bitbucket.org/thermsio/atalaya/commits/e498d56ae5c3bd25964c467740fc911eafbfde1c))
- **CORE-1286:** Add warning when trying to display a toast with no ToastContainer. ([69ac765](http://bitbucket.org/thermsio/atalaya/commits/69ac7659dd861ae3b189c4f9d63ebc46ab698be7))
- **CORE-1286:** export config options ([82f838b](http://bitbucket.org/thermsio/atalaya/commits/82f838b1ca93e6acaec29b61dce10cc443f0d333))
- **CORE-1286:** make ToastContainer optional ([eb03075](http://bitbucket.org/thermsio/atalaya/commits/eb03075b788b424f52ae07cd5dbdb4cc5f45bafa))
- **CORE-1286:** Make toasts wider. ([2a3129f](http://bitbucket.org/thermsio/atalaya/commits/2a3129f5dfe4f25e5084f2d02308a9c9ebb79dbf))
- **CORE-1286:** Publish Toasts ([03d7107](http://bitbucket.org/thermsio/atalaya/commits/03d7107b643be664e1cf432062eac149f147811d))
- **CORE-1286:** rename NotificationToast to Toast, update docs. ([77bb039](http://bitbucket.org/thermsio/atalaya/commits/77bb039ee59f322489a61348de102c6025383b1c))

# [1.6.0-beta.2](http://bitbucket.org/thermsio/atalaya/compare/v1.6.0-beta.1...v1.6.0-beta.2) (2021-05-14)

### Bug Fixes

- make ColorCard.tsx full width ([57f8dfa](http://bitbucket.org/thermsio/atalaya/commits/57f8dfa94509d978197b367488d4b625bb9d28df))
- zIndices name; ([86d6e37](http://bitbucket.org/thermsio/atalaya/commits/86d6e3723f69ba4bf26c55f7c86797de60edd054))

### Features

- update light theme colors ([ec79677](http://bitbucket.org/thermsio/atalaya/commits/ec7967788e870fab362273210405424a7a7f68b2))

# [1.6.0-beta.1](http://bitbucket.org/thermsio/atalaya/compare/v1.5.2...v1.6.0-beta.1) (2021-05-11)

### Bug Fixes

- **CORE-1337:** undo extra spin class for icon ([283a886](http://bitbucket.org/thermsio/atalaya/commits/283a8860accb9ac3ebf2670e88b7798456d4784f))

### Features

- **CORE-1337:** add color prop to Loading ([3ce1562](http://bitbucket.org/thermsio/atalaya/commits/3ce15624461408ab5bf89101a7ad7f0c74755558))
- **CORE-1337:** add ease spin animation ([b09ec37](http://bitbucket.org/thermsio/atalaya/commits/b09ec378bf1d9b38f49e072594c5df0634f00905))
- **CORE-1337:** add Loading component ([65d672f](http://bitbucket.org/thermsio/atalaya/commits/65d672fa57db0de794e03319e275dbe5631a81e5))
- **CORE-1337:** add loading prop to button ([7d6fa85](http://bitbucket.org/thermsio/atalaya/commits/7d6fa85bf4bff3a3b803d7b4600713962888936d))
- **CORE-1337:** add loading prop to FormLayout ([f2856d7](http://bitbucket.org/thermsio/atalaya/commits/f2856d70d400e5d1bf8ada4d019263a3304d821b))
- **CORE-1337:** export Loading component ([28f8e0f](http://bitbucket.org/thermsio/atalaya/commits/28f8e0f5346a7d6ff087365f85cc87c83eddb7cd))
- **CORE-1337:** let people know they control spinner color ([e8adc83](http://bitbucket.org/thermsio/atalaya/commits/e8adc8338bd0190023c1f63e62ab6f1eda711d23))
- **CORE-1337:** update FormLayout with new Loading API ([7de88db](http://bitbucket.org/thermsio/atalaya/commits/7de88dbbfd5ed712b2c2f47903f074a705ec9689))
- **CORE-1337:** update spinner icon ([8d89ebb](http://bitbucket.org/thermsio/atalaya/commits/8d89ebba683fe3c264a69120763b03b9aee95f25))

## [1.5.2](http://bitbucket.org/thermsio/atalaya/compare/v1.5.1...v1.5.2) (2021-05-11)

### Bug Fixes

- Button className not being passed ([584fadf](http://bitbucket.org/thermsio/atalaya/commits/584fadf3ab623cd6337226675afc751b9f6c970d))
- Button className not in dep array ([50cd4a7](http://bitbucket.org/thermsio/atalaya/commits/50cd4a7e2c6d4779c7afd351bb3ffe5a5c09a740))
- fix pseudo class selectors not being included into final bundle ([8b06a7a](http://bitbucket.org/thermsio/atalaya/commits/8b06a7a6f0cddb38f7e33d351b50d97dd5190660))
- Prevent Dropdown circular importing ([fa12174](http://bitbucket.org/thermsio/atalaya/commits/fa12174c7a3a5529ee433f17826b7bbfc0af65ff))
- tailwind purge ([056b9bb](http://bitbucket.org/thermsio/atalaya/commits/056b9bbb0878107188a64330a551e3172a644302))
- wrong GA tag ([f0b6167](http://bitbucket.org/thermsio/atalaya/commits/f0b6167508b1c7f0bffe0735c2173ad57266ac0c))

## [1.5.2-beta.2](http://bitbucket.org/thermsio/atalaya/compare/v1.5.2-beta.1...v1.5.2-beta.2) (2021-05-11)

### Bug Fixes

- fix pseudo class selectors not being included into final bundle ([8b06a7a](http://bitbucket.org/thermsio/atalaya/commits/8b06a7a6f0cddb38f7e33d351b50d97dd5190660))
- tailwind purge ([056b9bb](http://bitbucket.org/thermsio/atalaya/commits/056b9bbb0878107188a64330a551e3172a644302))

## [1.5.2-beta.1](http://bitbucket.org/thermsio/atalaya/compare/v1.5.1...v1.5.2-beta.1) (2021-05-10)

### Bug Fixes

- Button className not being passed ([584fadf](http://bitbucket.org/thermsio/atalaya/commits/584fadf3ab623cd6337226675afc751b9f6c970d))
- Button className not in dep array ([50cd4a7](http://bitbucket.org/thermsio/atalaya/commits/50cd4a7e2c6d4779c7afd351bb3ffe5a5c09a740))
- Prevent Dropdown circular importing ([fa12174](http://bitbucket.org/thermsio/atalaya/commits/fa12174c7a3a5529ee433f17826b7bbfc0af65ff))
- wrong GA tag ([f0b6167](http://bitbucket.org/thermsio/atalaya/commits/f0b6167508b1c7f0bffe0735c2173ad57266ac0c))

## [1.5.1](http://bitbucket.org/thermsio/atalaya/compare/v1.5.0...v1.5.1) (2021-05-10)

### Bug Fixes

- add GA info to styleguide config ([923d3cc](http://bitbucket.org/thermsio/atalaya/commits/923d3ccb2a8125c133976e1cfaa797ac05927cdc))
- double declarations from react-icons ([e2376e6](http://bitbucket.org/thermsio/atalaya/commits/e2376e645a16302a96ff24f9748b59672a919c36))

# [1.5.0](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0...v1.5.0) (2021-05-09)

# [1.4.0-beta.51](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.50...v1.4.0-beta.51) (2021-05-10)

### Bug Fixes

- add GA info to styleguide config ([e05cdca](http://bitbucket.org/thermsio/atalaya/commits/e05cdca2af506b5b5e08beec5b6f1a6fd5eeb9e0))
- double declarations from react-icons ([ae16a7a](http://bitbucket.org/thermsio/atalaya/commits/ae16a7ab9e294493b5f7b23dceaaa09280dd1126))

# [1.4.0-beta.50](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.49...v1.4.0-beta.50) (2021-05-09)

### Bug Fixes

- es bundle output for optimal performance ([9198b44](http://bitbucket.org/thermsio/atalaya/commits/9198b449336c08d34d1eb2b338e00ab1cd34f38e))
- move react-autosize-textarea to deps ([43b0361](http://bitbucket.org/thermsio/atalaya/commits/43b036115afb61722d2fce0b3be2885ecfad2409))
- remove tsconfig target/module to fix es bundle output ([996bdb6](http://bitbucket.org/thermsio/atalaya/commits/996bdb6b76acc408e58d1ab5c67f358891d7ac87))
- tsconfig esModuleInterop and remove prettier eslint extend ([ceb1453](http://bitbucket.org/thermsio/atalaya/commits/ceb1453fb5746200834c4b98ac54f1d2f0d83298))
- UniversalDropdown using outdated Icon ([ed3fa6d](http://bitbucket.org/thermsio/atalaya/commits/ed3fa6d35380241d5395285b5866e7822370013f))
- **CORE-1219:** fix button not using new semantic classes ([edd55b7](http://bitbucket.org/thermsio/atalaya/commits/edd55b733e631b70aa22a2566a65652a06e43a54))
- **CORE-1219:** fix margin and padding classes being purged ([55c504f](http://bitbucket.org/thermsio/atalaya/commits/55c504fa6594c48989ba76c3318c33918aed98b2))
- **CORE-1219:** fix purgeCSS ignoring ts files ([7e0de03](http://bitbucket.org/thermsio/atalaya/commits/7e0de030c81aa26b894adc7c9b615a845e59c704))
- **CORE-1219:** fix semantic colors not being displayed ([25d1358](http://bitbucket.org/thermsio/atalaya/commits/25d1358f2e8c9b673805ea4c0dcdc0b634fb647e))
- **CORE-1219:** fix Tag test fail ([9ea4af9](http://bitbucket.org/thermsio/atalaya/commits/9ea4af914c15e93270c7625ee103703d55ce2054))
- **CORE-1219:** make tag semantic background classes purgeable ([6fd7265](http://bitbucket.org/thermsio/atalaya/commits/6fd7265bc5031960dccf27b626ffcbddb752a7b0))
- **CORE-1219:** update Badge test ([572e9d3](http://bitbucket.org/thermsio/atalaya/commits/572e9d391fffa05462c0ab2a654c3a9cd242adf0))
- **CORE-1219:** update borderColor props with semantics ([e0e22b4](http://bitbucket.org/thermsio/atalaya/commits/e0e22b4c4cab6398b324ff0bae80b7d7c9ceefa0))
- **CORE-1244:** make Card background optional prop ([3efc6a0](http://bitbucket.org/thermsio/atalaya/commits/3efc6a02af9524d382355eb5486d9f969f8bdb78))
- **CORE-1249:** make PrioritySquare support margins ([4aba71c](http://bitbucket.org/thermsio/atalaya/commits/4aba71c4eed8d435a466aa6a555321dd29f477bb))
- **CORE-1249:** remove 'undefined' classes from final HTML ([6d2d08c](http://bitbucket.org/thermsio/atalaya/commits/6d2d08c20d1176e73438dbb07716b9fd4e5781dd))
- **CORE-1249:** remove DispatchUnassignedListItem ([940dbb6](http://bitbucket.org/thermsio/atalaya/commits/940dbb67b3b48247b2cdc063e00bb7d5577b22c8))
- **CORE-1249:** stop crash when providing an undefined class ([c84e635](http://bitbucket.org/thermsio/atalaya/commits/c84e6355bfc6f679b650931f5d68e99aa7af5270))
- **CORE-1249:** stop crash when rendering empty child on Inline ([abdd9f8](http://bitbucket.org/thermsio/atalaya/commits/abdd9f8990a650a48a98d3ecb3ae423494bf5394))
- **CORE-1251:** fix negative margin collapsing on Inline ([e9d61ad](http://bitbucket.org/thermsio/atalaya/commits/e9d61ad19a07e21c1d34de575bbd86dc09c73b5b))
- **CORE-1251:** make divide prop optional ([edb3d18](http://bitbucket.org/thermsio/atalaya/commits/edb3d1889d5c87d636d189ebad1bd8a2efe3229e))
- **CORE-1251:** remove className from accepted props ([2a7b5f8](http://bitbucket.org/thermsio/atalaya/commits/2a7b5f8368616726ac49fade0f57ddec6530f8db))
- **CORE-1251:** stop crash when Columns had a null child ([d0f18c8](http://bitbucket.org/thermsio/atalaya/commits/d0f18c8f169384b3a8f01a5e1d45699adb595d36))
- **CORE-1251:** stop double spacing between children when using Inline wrap prop ([a779b50](http://bitbucket.org/thermsio/atalaya/commits/a779b50d3cbfab8767a35e373e6e94ee508f1f60))
- **CORE-1251:** update inline children type ([c0c7d21](http://bitbucket.org/thermsio/atalaya/commits/c0c7d212033b7944d82bddaee7fa0a0f330e91d9))
- **CORE-1252:** remove rounded corners on Image thumbnail ([8ddf603](http://bitbucket.org/thermsio/atalaya/commits/8ddf603f9f13c4ccd49f6ddb7372d39fef3dddc4))
- **CORE-1252:** rename Semantic type to SemanticType; add subset: PriorityType ([0ea6a2c](http://bitbucket.org/thermsio/atalaya/commits/0ea6a2ccff904f21cb0b273e91184750f8762519))
- **CORE-1279:** fix colors documentation ([b0c6823](http://bitbucket.org/thermsio/atalaya/commits/b0c68237724f36ce40ee8002f92c0f291cc2885c))
- **CORE-1279:** show just 2 decimals for rgb colors ([99c7230](http://bitbucket.org/thermsio/atalaya/commits/99c72307dfde96010c9ad8a64bbc72844c2e7009))
- **CORE-1281:** fix stack children props not accepting multiple elements ([c6f87d0](http://bitbucket.org/thermsio/atalaya/commits/c6f87d073050fc8926fa0fc192c0b01d1a03d62a))
- **CORE-1283:** fix pipeline ([4bb1fe6](http://bitbucket.org/thermsio/atalaya/commits/4bb1fe6f566e090838d883c83fd310297d542d26))
- **CORE-1284:** add ButtonGroup ([db04d98](http://bitbucket.org/thermsio/atalaya/commits/db04d98476280f7f8aee341aee196fe3056959a9))
- **CORE-1284:** add Full width example ([247ec00](http://bitbucket.org/thermsio/atalaya/commits/247ec00a0cce00d7abd9f2edfc79df41e00492d0))
- **CORE-13009:** prevent bug when using a 'default' value as the first item of responsiveClasses array ([ee67af0](http://bitbucket.org/thermsio/atalaya/commits/ee67af02c9b60cd00f67def4cd04ae1b15402627))
- **CORE-1305:** fix critical text color not working ([6abf30f](http://bitbucket.org/thermsio/atalaya/commits/6abf30f39c0fb4374a35098d10e19e28d973faab))
- **CORE-1309:** make code blocks easier to read on light mode ([94d6b91](http://bitbucket.org/thermsio/atalaya/commits/94d6b910c552241f2c554be61b69c622e4f05e43))
- **CORE-1312:** add hover pointer to form labels ([ef83f00](http://bitbucket.org/thermsio/atalaya/commits/ef83f005cd498869072dfda16d0520aade551322))
- **CORE-1312:** change transition animation ([e5713df](http://bitbucket.org/thermsio/atalaya/commits/e5713df46de495c46c3e56d7859bd5b6ee754cfb))
- **CORE-1312:** fix neutral button text dissapearing when pressed. ([5a85be2](http://bitbucket.org/thermsio/atalaya/commits/5a85be28488d9a41934ac14dc7773b1401abe392))
- **CORE-1312:** make alingn props optional ([a09be47](http://bitbucket.org/thermsio/atalaya/commits/a09be47d926d5a6e74e62cfbabaec1937c4b009c))
- **CORE-1312:** only render label if defined ([9d446c7](http://bitbucket.org/thermsio/atalaya/commits/9d446c7d0a0c3b89344104e3b452f60e22afd930))
- **CORE-1313:** add missing prop year key ([28c8671](http://bitbucket.org/thermsio/atalaya/commits/28c86716ad5fec819b807ffe9a6ed7161468f632))
- **CORE-1313:** divider being present on date input alone ([3fab07a](http://bitbucket.org/thermsio/atalaya/commits/3fab07ab51c5dd219eaad3f1993d510ee75871e3))
- **CORE-1313:** fix 24 hour time picker ([88a2094](http://bitbucket.org/thermsio/atalaya/commits/88a20941ddd155959d57b7fc6ada6ada7f2e7b62))
- **CORE-1313:** fix click on next month closing pop up ([2fa6c42](http://bitbucket.org/thermsio/atalaya/commits/2fa6c42953c01c0e3469d04774ec5f1953a2ed16))
- **CORE-1313:** remove arbitrary spacing on form fields ([e42a395](http://bitbucket.org/thermsio/atalaya/commits/e42a395d869144d2ca71b5cf27f748b851371ddb))
- **CORE-1313:** restore display flex inside buttons ([7c02ae7](http://bitbucket.org/thermsio/atalaya/commits/7c02ae7a95ef255812480c3ee4b29db19143debb))
- **CORE-1313:** Stop month picker from changin the year until it has picked a month ([1f630f4](http://bitbucket.org/thermsio/atalaya/commits/1f630f4b5713c4a844c2d0fb6b9e0119c2c7b0c1))
- **CORE-1319:** fix default value of useIsMobile.ts ([a2fa9de](http://bitbucket.org/thermsio/atalaya/commits/a2fa9dee90e1b72dbc6cd91a99a92162212fec92))
- **CORE-1319:** fix dismissing bottom sheet on click ([b78f0ba](http://bitbucket.org/thermsio/atalaya/commits/b78f0ba4b0edc771123c311b18a631d8a709884e))
- **CORE-1319:** fix timepicker look ugly on BottomSheet ([81ef5f4](http://bitbucket.org/thermsio/atalaya/commits/81ef5f48e9db539419ef5170b41d1ca3988cef22))
- activate dark theme by default ([926cb35](http://bitbucket.org/thermsio/atalaya/commits/926cb35196ce7d0896afe95b747b169799651b5d))
- add html body bg and text color to base.css ([e0ad579](http://bitbucket.org/thermsio/atalaya/commits/e0ad579131c11eef6052551bc19a73c1919bef28))
- build in pipeline skip-ci ([14fdc8b](http://bitbucket.org/thermsio/atalaya/commits/14fdc8bf9177aabb00065eacdc5bc6d9fdf506d3))
- es bundle output no process and no require ([1bbf552](http://bitbucket.org/thermsio/atalaya/commits/1bbf55215e0238ccac04373feafcbbb6777406bb))
- production build failing ([f86fc40](http://bitbucket.org/thermsio/atalaya/commits/f86fc409b44a47efb625964cf042791ed348289a))
- require to import ([7bda687](http://bitbucket.org/thermsio/atalaya/commits/7bda687ea50a944cdbf84efc8fc263843cd57b4d))
- typings path in package.json ([01239d4](http://bitbucket.org/thermsio/atalaya/commits/01239d4e6176b24a7beeed2746e0325a445af01b))
- useCSS work with vars without '--' ([a1be66e](http://bitbucket.org/thermsio/atalaya/commits/a1be66e145c781d9d2fc413b0fc10e4b833cc14c))
- wrong color value ([f1582f0](http://bitbucket.org/thermsio/atalaya/commits/f1582f04f2724c195821b99d559131e620a9fd18))
- z-index tailwind theme ([ff02eb1](http://bitbucket.org/thermsio/atalaya/commits/ff02eb1d1503fa182ff7060482e1c1d023002e04))
- **CORE-1321:** move iconify to dependencies ([b356707](http://bitbucket.org/thermsio/atalaya/commits/b35670756dfd2ddd852d99fc86d099215a99ec5a))

### Features

- Update exports ([b1c1489](http://bitbucket.org/thermsio/atalaya/commits/b1c14897b68bcf342ed2a85f2902ca1da21b09bf))
- **Button:** className spread ([1d24d37](http://bitbucket.org/thermsio/atalaya/commits/1d24d37b9280c898d0634f565de1980e4657f3d7))
- **Card:** props spread manual commit again ([25d7bac](http://bitbucket.org/thermsio/atalaya/commits/25d7bacaad31070b236aa21eba2bd1bbd0453a3e))
- **CORE-1279:** don't clone Column when used as child of Columns ([6efafc5](http://bitbucket.org/thermsio/atalaya/commits/6efafc5415260546a49eb4df9afe21c53fa0f18f))
- **CORE-1279:** implement ThemeSwitcher ([050c082](http://bitbucket.org/thermsio/atalaya/commits/050c082235a06376b54c5468897744e6a946e1fe))
- **CORE-1279:** improve performance when setting css props ([31dc1a0](http://bitbucket.org/thermsio/atalaya/commits/31dc1a0685b0ff1791f0c575f374a1699fc1fc1d))
- **CORE-1279:** rename Semantic Type to Variant ([3bf44e8](http://bitbucket.org/thermsio/atalaya/commits/3bf44e820494f088a2d0932002808bd0e0a36cc8))
- **CORE-1279:** update color documentation when theme changes ([31db1af](http://bitbucket.org/thermsio/atalaya/commits/31db1af432f3d976eb7cae7d181817f0ffafb750))
- **CORE-1279:** update columns docs ([d16d28c](http://bitbucket.org/thermsio/atalaya/commits/d16d28c2df1e31b478a4e8dedf085b406143a24a))
- **CORE-1285:** add Controlled mode ([3bf2c1a](http://bitbucket.org/thermsio/atalaya/commits/3bf2c1a3d38ef9639d9573d6fcc9345af6553552))
- **CORE-1285:** add custom positions ([f170c11](http://bitbucket.org/thermsio/atalaya/commits/f170c11ae47f16443b83b77b52c6d4360d65ac7c))
- **CORE-1285:** add Dropdown ([032388f](http://bitbucket.org/thermsio/atalaya/commits/032388f8dee6103df9b90c72ed571c23cfa2fb50))
- **CORE-1285:** add Dropdown examples to docs ([e9a5da8](http://bitbucket.org/thermsio/atalaya/commits/e9a5da86212cb2922f57ad5d1a94b6a2a7f3dc1e))
- **CORE-1285:** dismiss dropdown on click outside and on blur ([992d805](http://bitbucket.org/thermsio/atalaya/commits/992d8057c4225a29f125123fd003cf2c7457ff5b))
- **CORE-1285:** implement Dropdown on DateTimePicker ([8f3f43d](http://bitbucket.org/thermsio/atalaya/commits/8f3f43dda5acea175ae613afdc18752cce5ed2e7))
- **CORE-1285:** update DateTimePicker.tsx with Dropdown WIP ([542790b](http://bitbucket.org/thermsio/atalaya/commits/542790bf3eb93f0cf688cd692dcc22a5eb80fa57))
- **CORE-1304:** add Modal, ActionModal ([ecef29e](http://bitbucket.org/thermsio/atalaya/commits/ecef29e763eba906898a73ffc02b1e86f9f843bb))
- **CORE-1304:** center button content ([e7a5cdc](http://bitbucket.org/thermsio/atalaya/commits/e7a5cdc04437144e7aa6ac0c963c0c4d2008cbc3))
- **CORE-1305:** add faded semantic color ([e75fed4](http://bitbucket.org/thermsio/atalaya/commits/e75fed42cb984d44e39832ec9285ac2dbc26d0ae))
- **CORE-1305:** add outlined buttons ([df935c4](http://bitbucket.org/thermsio/atalaya/commits/df935c4c5b3a555605f861be02f48140022a7246))
- **CORE-1305:** add subtle button ([e3bdcfe](http://bitbucket.org/thermsio/atalaya/commits/e3bdcfee87397296835d9df527a74de9d4973edf))
- **CORE-1305:** tweak button stylings ([11f1024](http://bitbucket.org/thermsio/atalaya/commits/11f1024aa6702e792d4597c70f5d288eb6a336e6))
- **CORE-1305:** update neutral dark colors ([193021c](http://bitbucket.org/thermsio/atalaya/commits/193021c70e25bf936a8d380a4cc568acf7a5fc95))
- **CORE-1305:** update semantic text light color ([448072b](http://bitbucket.org/thermsio/atalaya/commits/448072b34231934b60ff25f9b21468601bc446e7))
- **CORE-1309:** add Icon documentation ([af9a66e](http://bitbucket.org/thermsio/atalaya/commits/af9a66e2246581ef0b028ddec531ccbbba4882ad))
- **CORE-1309:** expand text size up to 2xl ([cea4581](http://bitbucket.org/thermsio/atalaya/commits/cea4581174faf68ec94edf32e45b043c1faeb12a))
- **CORE-1309:** update Icon component ([9ab96bc](http://bitbucket.org/thermsio/atalaya/commits/9ab96bc8e2de1fd803ce83a3aa014f1362f02b9e))
- **CORE-1309:** update Icon size prop ([2e9ee17](http://bitbucket.org/thermsio/atalaya/commits/2e9ee17bf5fe373d1859332b3e5346e5e06bf962))
- **CORE-1312:** add password form control ([3d68768](http://bitbucket.org/thermsio/atalaya/commits/3d68768133e774dd55497b87efbda9748c474363))
- **CORE-1312:** add button active stylings ([b90d6b1](http://bitbucket.org/thermsio/atalaya/commits/b90d6b121f7455efd3d4089dc97b1388b85bbc7d))
- **CORE-1312:** add checkbox ([320514c](http://bitbucket.org/thermsio/atalaya/commits/320514c1274aa5c9a3f5b1113862bbe5fa81620f))
- **CORE-1312:** add disabled state ([3f5b762](http://bitbucket.org/thermsio/atalaya/commits/3f5b762fae6f87bf1e0317bf849cb2714e5ed9c1))
- **CORE-1312:** add disabled state to Button ([b34fbe9](http://bitbucket.org/thermsio/atalaya/commits/b34fbe9e4718544259c8f05f1272c8b6310aa8d4))
- **CORE-1312:** add email field ([0d5bd29](http://bitbucket.org/thermsio/atalaya/commits/0d5bd297c2bd4d62c704e9b45b0f1371b60cf7e9))
- **CORE-1312:** add error to FormControlWrapper ([9a92ccc](http://bitbucket.org/thermsio/atalaya/commits/9a92ccc8b10c8a784f05fed9cdb44f032deb9e1e))
- **CORE-1312:** add hint text component ([2f2b292](http://bitbucket.org/thermsio/atalaya/commits/2f2b29299daa6041cc0843715851168d2d18ae97))
- **CORE-1312:** add HTMLSelect ([97782f0](http://bitbucket.org/thermsio/atalaya/commits/97782f0a5ecf8faa66b6c50d2431b1596d47defa))
- **CORE-1312:** add number field ([82dcf63](http://bitbucket.org/thermsio/atalaya/commits/82dcf63c26154cbbc0055746fd2ddede6efc3637))
- **CORE-1312:** add PhoneField ([e1db44b](http://bitbucket.org/thermsio/atalaya/commits/e1db44b144d596ffbd3b17e04b919ebac5fa68ca))
- **CORE-1312:** add prop to display top row or not ([e2e5e08](http://bitbucket.org/thermsio/atalaya/commits/e2e5e08a92fde3fa3fabd1eedcf938f079d2df68))
- **CORE-1312:** add RadioField ([b31f368](http://bitbucket.org/thermsio/atalaya/commits/b31f368b42ee0501ca3b099c8048310c81117602))
- **CORE-1312:** add slider field ([663c493](http://bitbucket.org/thermsio/atalaya/commits/663c493bbedabfb1f7a6f837798e0f2e76d08e70))
- **CORE-1312:** add text semantic faded colors ([3bfb103](http://bitbucket.org/thermsio/atalaya/commits/3bfb10376e12e111b2cf54e047fea1ce181b0b0e))
- **CORE-1312:** add textarea ([3267a52](http://bitbucket.org/thermsio/atalaya/commits/3267a52ce71a7f3f058ee51d4839af79be77d967))
- **CORE-1312:** add TextField component ([91b3bb0](http://bitbucket.org/thermsio/atalaya/commits/91b3bb03a81537213538258ce57bb0a0ec09d70d))
- **CORE-1312:** add toggleSwitch ([6beda35](http://bitbucket.org/thermsio/atalaya/commits/6beda35d43996763edb5d16ce065f1a37a986d1b))
- **CORE-1312:** appease TS Compiler for onFocus events ([0ca30a6](http://bitbucket.org/thermsio/atalaya/commits/0ca30a670226fde4078b838c71cbd22ff97c6816))
- **CORE-1312:** implement FormFieldLabel component ([e335e09](http://bitbucket.org/thermsio/atalaya/commits/e335e09cb05f965919a96165041da9ec3ca1d6a2))
- **CORE-1312:** implement select field logic ([a4b6f3c](http://bitbucket.org/thermsio/atalaya/commits/a4b6f3c1dd6759b316e3ffb94344f1d24343d40d))
- **CORE-1312:** make faded semantic colors more solid ([df5e3c2](http://bitbucket.org/thermsio/atalaya/commits/df5e3c2a7f63002dbef59593e2c1df06117fdbe0))
- **CORE-1312:** make label props optional ([ea651ce](http://bitbucket.org/thermsio/atalaya/commits/ea651ce921b48926066dbeb37325ed313cc96075))
- **CORE-1312:** remove all evidence of preserveTopRow ([6f1f376](http://bitbucket.org/thermsio/atalaya/commits/6f1f376aca54db9f788dec70e2946c496e33d29f))
- **CORE-1312:** remove checkbox ring offset ([0965a4d](http://bitbucket.org/thermsio/atalaya/commits/0965a4d45cc51fd6020b8779c5a8f614741c9011))
- **CORE-1312:** style react-select ([806e98a](http://bitbucket.org/thermsio/atalaya/commits/806e98aa1ff49ff7ce5c1a07a70612ef3ae25869))
- **CORE-1312:** update slider ([a59d1ca](http://bitbucket.org/thermsio/atalaya/commits/a59d1ca0fb526b18077d3842e723e42ed2537223))
- **CORE-1313:** add additional props to Inline via divProps; ([d37b047](http://bitbucket.org/thermsio/atalaya/commits/d37b047da6dcdb982c69c283c8111e4caf18d03c))
- **CORE-1313:** add calendar month days logic ([666795c](http://bitbucket.org/thermsio/atalaya/commits/666795c47a1dab47fbfbca7c5a11907d809471f2))
- **CORE-1313:** add DatePicker stylings ([26fee4b](http://bitbucket.org/thermsio/atalaya/commits/26fee4b9a583449501754b3ac18fad40a0edbaa4))
- **CORE-1313:** add DateTimePicker input preview ([ad245cc](http://bitbucket.org/thermsio/atalaya/commits/ad245cc4be83dccdeb8eb8484d7254ddb1fdb72f))
- **CORE-1313:** add default alignment to icon ([07e24bd](http://bitbucket.org/thermsio/atalaya/commits/07e24bd0b7e26949d7d690f03aa03364a4ac514e))
- **CORE-1313:** add fullWidth prop ([566486d](http://bitbucket.org/thermsio/atalaya/commits/566486dc83081574752908a916a4568e6748a714))
- **CORE-1313:** add generic div props to Inline ([84b4424](http://bitbucket.org/thermsio/atalaya/commits/84b442465a8b5ae4e85423c12d24e38284864798))
- **CORE-1313:** add generic div props to Stack ([1a156d8](http://bitbucket.org/thermsio/atalaya/commits/1a156d82efa723c6fdf149cb92d97ad6eccdb06c))
- **CORE-1313:** add invoking and dismiss interaction to date picker ([4360d90](http://bitbucket.org/thermsio/atalaya/commits/4360d90fe900e42d2f7df025ab81b2e4e35dc826))
- **CORE-1313:** add logic to time picker ([402ba2f](http://bitbucket.org/thermsio/atalaya/commits/402ba2fe43ee7fbb4f72e5ab4d1ce428a9fa10f6))
- **CORE-1313:** add MonthPickerl ([e733120](http://bitbucket.org/thermsio/atalaya/commits/e7331201db7719cbc1ab9c764559b74272ca2308))
- **CORE-1313:** add time picking logic ([c022290](http://bitbucket.org/thermsio/atalaya/commits/c022290ddd5240eda77126a279053c24d3b8154c))
- **CORE-1313:** add year picker ([e2f7f60](http://bitbucket.org/thermsio/atalaya/commits/e2f7f6071db8e2d3a8fedfd2aad2bbce8de70edf))
- **CORE-1313:** change TimePicker logic to change timestamp on every action WIP ([1de6465](http://bitbucket.org/thermsio/atalaya/commits/1de64654e9c0c807135438ea0414146defca5047))
- **CORE-1313:** complete date setting implementation ([51dc6d9](http://bitbucket.org/thermsio/atalaya/commits/51dc6d9f669db32a41cb3fcdf016734c3d2dffda))
- **CORE-1313:** dateTime picker returns the picked value via onChangeProps ([35218b4](http://bitbucket.org/thermsio/atalaya/commits/35218b41c47aa07e963af45320e8854fce99f9e9))
- **CORE-1313:** dont alter the date from day picker; ([c8c4f42](http://bitbucket.org/thermsio/atalaya/commits/c8c4f422ab005cc0963ff14a33c4a64df3e3252b))
- **CORE-1313:** finsh calendar styling ([bb5a9b0](http://bitbucket.org/thermsio/atalaya/commits/bb5a9b065b3d323634f1436dd74e7fdded07126e))
- **CORE-1313:** fix time picker not rendering ([c615473](http://bitbucket.org/thermsio/atalaya/commits/c615473cfe970dc419e8fab9177f0d8230225153))
- **CORE-1313:** further tweaks on button stylings ([37a7fa6](http://bitbucket.org/thermsio/atalaya/commits/37a7fa6423c7b9432a24a49f161f781eed28761e))
- **CORE-1313:** made button outline a step bolder ([fa7ec05](http://bitbucket.org/thermsio/atalaya/commits/fa7ec05b2842da4a889e3fd0ff32b53da2aeedbb))
- **CORE-1313:** make number step controls optional ([d601ead](http://bitbucket.org/thermsio/atalaya/commits/d601ead4f82272844a979535044f2f6c5c4a51d4))
- **CORE-1313:** more netural color refinment ([f06f476](http://bitbucket.org/thermsio/atalaya/commits/f06f47619c73dbb23d9bdddacc853b09ae7f3f7c))
- **CORE-1313:** update date picker logic to update timestamp on every action ([9f90039](http://bitbucket.org/thermsio/atalaya/commits/9f9003948052367d51d4145c7622c298b4d4ca7a))
- **CORE-1313:** update neutral faded color ([24192ba](http://bitbucket.org/thermsio/atalaya/commits/24192ba17a8eb8954746aeaaa6918899b9f8486c))
- **CORE-1313:** update stylings ([7551afa](http://bitbucket.org/thermsio/atalaya/commits/7551aface0a206dac10a0c7f32de013a785c1e24))
- **CORE-1313:** update time picker stylings ([54da840](http://bitbucket.org/thermsio/atalaya/commits/54da8409753f620ee83c5675609bb7fb57ec4a05))
- **CORE-1313:** update time picking logic ([3f95bb1](http://bitbucket.org/thermsio/atalaya/commits/3f95bb155d1619c404f505c26f071c3cfdece063))
- **CORE-1313:** use more subtle buttons ([cb5d4f9](http://bitbucket.org/thermsio/atalaya/commits/cb5d4f95510c833281f1d0e09c793d94380c2ab3))
- **CORE-1318:** add extras to FormControls; ([7c8232c](http://bitbucket.org/thermsio/atalaya/commits/7c8232c7d6bedd79b7fc58229d8d044d447e4296))
- **CORE-1318:** add FormControls; ([08481b0](http://bitbucket.org/thermsio/atalaya/commits/08481b079f494e13fe547af86c88b9e45621b251))
- **CORE-1318:** add FormLayout and horizontal support; ([666ea36](http://bitbucket.org/thermsio/atalaya/commits/666ea369c1b2c93e7d5812a4da3d057a88941321))
- **CORE-1318:** add FormLayout.Title ([18b0e3f](http://bitbucket.org/thermsio/atalaya/commits/18b0e3f934b1bbe4c7c74d372643c22ea8fe4978))
- **CORE-1318:** add option to remove dividers ([5128f10](http://bitbucket.org/thermsio/atalaya/commits/5128f108a1205efc36cd5c08dc5492090bde3ae7))
- **CORE-1318:** add spacing between form fields; ([5d0b622](http://bitbucket.org/thermsio/atalaya/commits/5d0b6220850cc2a321bf7a4c639749d075817c44))
- **CORE-1318:** breakdown distribution in components; ([4cfba41](http://bitbucket.org/thermsio/atalaya/commits/4cfba41e7ea4cf39ceb8f2c6ab7f11f2bb1ed0e1))
- **CORE-1318:** export form components with their innate name ([aeb0f3a](http://bitbucket.org/thermsio/atalaya/commits/aeb0f3af1124ff61ea4120a64c9660077fa7c787))
- **CORE-1318:** export FormFields; ([dd3d237](http://bitbucket.org/thermsio/atalaya/commits/dd3d23752992599151c555d134b233a344a94935))
- **CORE-1319:** add UniversalDropdown ([58af8c3](http://bitbucket.org/thermsio/atalaya/commits/58af8c34a557672670917a77fe44a541276fdd3d))
- **CORE-1319:** change Dropdown to be more generic ([bcb0c44](http://bitbucket.org/thermsio/atalaya/commits/bcb0c44dc949468e097351e76792c5f1d46050dd))
- **CORE-1319:** create a proper isMobile hook ([f0f6504](http://bitbucket.org/thermsio/atalaya/commits/f0f6504e83d3c43b71915c5d2470bd2c733a09e3))
- **CORE-1319:** refine useIsMobile ([9c9a2f3](http://bitbucket.org/thermsio/atalaya/commits/9c9a2f358bb105a3acc88089db2137d2d8b5a4a5))
- **CORE-1320:** add zIndices tokens ([97627e7](http://bitbucket.org/thermsio/atalaya/commits/97627e795f2f91516dc56a0e4e4b155d8ec79aec))
- **CORE-1320:** export atalaya tailwind config as preset ([3d10131](http://bitbucket.org/thermsio/atalaya/commits/3d1013118b05204b69d145e7d85b2c6563a22301))
- update button component ([56beadb](http://bitbucket.org/thermsio/atalaya/commits/56beadbf288cecbea039b5aef36b5ab0974e8683))
- **CORE-1219:** simplify design tokens ([86770ac](http://bitbucket.org/thermsio/atalaya/commits/86770ac57bd13c6fbea617edc92b1c36d36f6ee4))
- **CORE-1219:** update badge with new semantic naming ([8ebd807](http://bitbucket.org/thermsio/atalaya/commits/8ebd807bcf5965fe59855b18b6fea7874dd99bff))
- **CORE-1219:** update color docs ([b9ce7ff](http://bitbucket.org/thermsio/atalaya/commits/b9ce7ff20443c0f21291249c9c662ad05f573761))
- **CORE-1219:** update color token list with new tokens ([c0ab33c](http://bitbucket.org/thermsio/atalaya/commits/c0ab33c55131f13c52d6cb411cd7323ba4ac2fd8))
- **CORE-1219:** update PrioritySquare with new semantics ([97106c0](http://bitbucket.org/thermsio/atalaya/commits/97106c070c66ee02482ae33f7f88a4e6e4b619fe))
- **CORE-1219:** update semantic color list ([74c3da9](http://bitbucket.org/thermsio/atalaya/commits/74c3da9675f876818e72a39700a73d07f99c5036))
- **CORE-1219:** update styleguide config with new semantic naming ([8b8d146](http://bitbucket.org/thermsio/atalaya/commits/8b8d146eb08e11dba34583f8efc22a61f3048230))
- **CORE-1219:** update tag to use new semantic names ([f36a87f](http://bitbucket.org/thermsio/atalaya/commits/f36a87fe20c4b49a5f29c63db3f7de6acd6b4bc2))
- **CORE-1244:** add background prop to Card ([bb0fe4c](http://bitbucket.org/thermsio/atalaya/commits/bb0fe4cb5a1a1d34827a9ace6adb270e7e6d5073))
- **CORE-1244:** add fillY prop to columns ([81b3b47](http://bitbucket.org/thermsio/atalaya/commits/81b3b470439b156f2cee55b42ffa9cddcf1e6afb))
- **CORE-1244:** add horizontal and vertical props to margin and padding ([cbfcf15](http://bitbucket.org/thermsio/atalaya/commits/cbfcf151c861b59ce37316a4b5f98f2505db1455))
- **CORE-1244:** add vertical alignment to stack ([d4de3ae](http://bitbucket.org/thermsio/atalaya/commits/d4de3ae8406c3a61526205ee6db9a47b2a3dcc41))
- **CORE-1247:** add content option to with prop on Column ([8ed478e](http://bitbucket.org/thermsio/atalaya/commits/8ed478e3ed422a27a2b445be85d941b28fa70887))
- **CORE-1247:** export PrioritySquare ([cc8654d](http://bitbucket.org/thermsio/atalaya/commits/cc8654d6b6566f2cfffe3e88250716d981dc71f3))
- **CORE-1249:** add child divers border props to layout components ([061e814](http://bitbucket.org/thermsio/atalaya/commits/061e8148819602dc55f287f785fc531d6537cfd3))
- **CORE-1249:** add DateTime component ([4669305](http://bitbucket.org/thermsio/atalaya/commits/4669305e8615f6f68da4cb3b345ac7be9effca6d))
- **CORE-1249:** add semantic colors to borders ([de8816a](http://bitbucket.org/thermsio/atalaya/commits/de8816a971ab1e55716661159a7aa286953dc1be))
- **CORE-1249:** add sm prop to ActivityType ([2f0f070](http://bitbucket.org/thermsio/atalaya/commits/2f0f070babd893f639ef746abd5b90fd5050eb5c))
- **CORE-1249:** enable ActivityType to accept margins ([4d06a50](http://bitbucket.org/thermsio/atalaya/commits/4d06a50a387856b07656777d2329aa1609708784))
- **CORE-1251:** add optional classNames prop to layout components ([ba5da84](http://bitbucket.org/thermsio/atalaya/commits/ba5da84f628c9181afc2994354d96e96f752b25e))
- **CORE-1251:** add sm prop to tagGroup ([84d156c](http://bitbucket.org/thermsio/atalaya/commits/84d156c333d5bfde3f89c80b02e429f916f3cbb9))
- **CORE-1251:** add TagGroup ([8ba2835](http://bitbucket.org/thermsio/atalaya/commits/8ba2835a3e69a2ca14e504d6460c4069a321065b))
- **CORE-1251:** fix Inline wrap spacing ([4bace58](http://bitbucket.org/thermsio/atalaya/commits/4bace58b0c11d95f870eb3e6cbe464b6ec8faa4c))
- **CORE-1251:** remove ActivityType ([07c5259](http://bitbucket.org/thermsio/atalaya/commits/07c525994b059fd6c4b9f5fbb5d1ab0c00ffd573))
- **CORE-1251:** remove wrapper prop from Inline ([9bd4885](http://bitbucket.org/thermsio/atalaya/commits/9bd4885b07af9b681aa556d0b6f8c59dda5a4881))
- **CORE-1251:** update inline docs placeholder ([9fa940c](http://bitbucket.org/thermsio/atalaya/commits/9fa940c08edcb318c2b1bc1d639521c8ea2aab4b))
- **CORE-1251:** update tag styling and add sm prop ([dcfd760](http://bitbucket.org/thermsio/atalaya/commits/dcfd760bdcd8e475f45a90a1ede943c643890c6b))
- **CORE-1252:** add fontSize and textColor to layout base props ([1e901a1](http://bitbucket.org/thermsio/atalaya/commits/1e901a14164be634eddd70783bb3230892a736b9))
- **CORE-1252:** add Icon component ([c3234b4](http://bitbucket.org/thermsio/atalaya/commits/c3234b4cd6a34189ff69294bf0226c372d032666))
- **CORE-1252:** center text in tags by default ([55cbe09](http://bitbucket.org/thermsio/atalaya/commits/55cbe09690f781850b0de74929d041c59b179749))
- **CORE-1252:** export general types ([f98c594](http://bitbucket.org/thermsio/atalaya/commits/f98c5949b8e2dd5ed1698bf15b5ad68b70afbc48))
- **CORE-1252:** implement Icon component ([a25a59d](http://bitbucket.org/thermsio/atalaya/commits/a25a59d4f24e27cbc2a8c0b52136a771b2a9d164))
- **CORE-1279:** tweak surface & caution dark colors ([71e3a57](http://bitbucket.org/thermsio/atalaya/commits/71e3a5723dfc8f5845201d02acc44b2d99227a39))
- **CORE-1330:** change Icon component to work with svg images ([81ce5a0](http://bitbucket.org/thermsio/atalaya/commits/81ce5a0937736b26a4d0e9272361ed630018f979))
- **useTheme:** add hook ([cc8f4df](http://bitbucket.org/thermsio/atalaya/commits/cc8f4df74ea9c3d0ac77757a4de362ea9ea486d9))
- tsconfig esModuleInterop and remove prettier eslint extend ([ceb1453](http://bitbucket.org/thermsio/atalaya/commits/ceb1453fb5746200834c4b98ac54f1d2f0d83298))

# [1.4.0-beta.49](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.48...v1.4.0-beta.49) (2021-05-09)

### Bug Fixes

- remove tsconfig target/module to fix es bundle output ([996bdb6](http://bitbucket.org/thermsio/atalaya/commits/996bdb6b76acc408e58d1ab5c67f358891d7ac87))

# [1.4.0-beta.48](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.47...v1.4.0-beta.48) (2021-05-07)

### Features

- Update exports ([b1c1489](http://bitbucket.org/thermsio/atalaya/commits/b1c14897b68bcf342ed2a85f2902ca1da21b09bf))

# [1.4.0-beta.47](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.46...v1.4.0-beta.47) (2021-05-07)

### Bug Fixes

- UniversalDropdown using outdated Icon ([ed3fa6d](http://bitbucket.org/thermsio/atalaya/commits/ed3fa6d35380241d5395285b5866e7822370013f))

# [1.4.0-beta.46](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.45...v1.4.0-beta.46) (2021-05-07)

### Bug Fixes

- move react-autosize-textarea to deps ([43b0361](http://bitbucket.org/thermsio/atalaya/commits/43b036115afb61722d2fce0b3be2885ecfad2409))

# [1.4.0-beta.45](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.44...v1.4.0-beta.45) (2021-05-07)

### Bug Fixes

- **CORE-1319:** fix default value of useIsMobile.ts ([a2fa9de](http://bitbucket.org/thermsio/atalaya/commits/a2fa9dee90e1b72dbc6cd91a99a92162212fec92))
- **CORE-1319:** fix dismissing bottom sheet on click ([b78f0ba](http://bitbucket.org/thermsio/atalaya/commits/b78f0ba4b0edc771123c311b18a631d8a709884e))
- **CORE-1319:** fix timepicker look ugly on BottomSheet ([81ef5f4](http://bitbucket.org/thermsio/atalaya/commits/81ef5f48e9db539419ef5170b41d1ca3988cef22))

### Features

- **CORE-1319:** add UniversalDropdown ([58af8c3](http://bitbucket.org/thermsio/atalaya/commits/58af8c34a557672670917a77fe44a541276fdd3d))
- **CORE-1319:** change Dropdown to be more generic ([bcb0c44](http://bitbucket.org/thermsio/atalaya/commits/bcb0c44dc949468e097351e76792c5f1d46050dd))
- **CORE-1319:** create a proper isMobile hook ([f0f6504](http://bitbucket.org/thermsio/atalaya/commits/f0f6504e83d3c43b71915c5d2470bd2c733a09e3))
- **CORE-1319:** refine useIsMobile ([9c9a2f3](http://bitbucket.org/thermsio/atalaya/commits/9c9a2f358bb105a3acc88089db2137d2d8b5a4a5))

# [1.4.0-beta.44](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.43...v1.4.0-beta.44) (2021-05-07)

### Features

- **CORE-1330:** change Icon component to work with svg images ([81ce5a0](http://bitbucket.org/thermsio/atalaya/commits/81ce5a0937736b26a4d0e9272361ed630018f979))

# [1.4.0-beta.43](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.42...v1.4.0-beta.43) (2021-05-05)

### Features

- **CORE-1285:** add Controlled mode ([3bf2c1a](http://bitbucket.org/thermsio/atalaya/commits/3bf2c1a3d38ef9639d9573d6fcc9345af6553552))
- **CORE-1285:** add custom positions ([f170c11](http://bitbucket.org/thermsio/atalaya/commits/f170c11ae47f16443b83b77b52c6d4360d65ac7c))
- **CORE-1285:** add Dropdown ([032388f](http://bitbucket.org/thermsio/atalaya/commits/032388f8dee6103df9b90c72ed571c23cfa2fb50))
- **CORE-1285:** add Dropdown examples to docs ([e9a5da8](http://bitbucket.org/thermsio/atalaya/commits/e9a5da86212cb2922f57ad5d1a94b6a2a7f3dc1e))
- **CORE-1285:** dismiss dropdown on click outside and on blur ([992d805](http://bitbucket.org/thermsio/atalaya/commits/992d8057c4225a29f125123fd003cf2c7457ff5b))
- **CORE-1285:** implement Dropdown on DateTimePicker ([8f3f43d](http://bitbucket.org/thermsio/atalaya/commits/8f3f43dda5acea175ae613afdc18752cce5ed2e7))
- **CORE-1285:** update DateTimePicker.tsx with Dropdown WIP ([542790b](http://bitbucket.org/thermsio/atalaya/commits/542790bf3eb93f0cf688cd692dcc22a5eb80fa57))

# [1.4.0-beta.42](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.41...v1.4.0-beta.42) (2021-05-04)

### Features

- **CORE-1318:** add extras to FormControls; ([7c8232c](http://bitbucket.org/thermsio/atalaya/commits/7c8232c7d6bedd79b7fc58229d8d044d447e4296))
- **CORE-1318:** add FormControls; ([08481b0](http://bitbucket.org/thermsio/atalaya/commits/08481b079f494e13fe547af86c88b9e45621b251))
- **CORE-1318:** add FormLayout and horizontal support; ([666ea36](http://bitbucket.org/thermsio/atalaya/commits/666ea369c1b2c93e7d5812a4da3d057a88941321))
- **CORE-1318:** add FormLayout.Title ([18b0e3f](http://bitbucket.org/thermsio/atalaya/commits/18b0e3f934b1bbe4c7c74d372643c22ea8fe4978))
- **CORE-1318:** add option to remove dividers ([5128f10](http://bitbucket.org/thermsio/atalaya/commits/5128f108a1205efc36cd5c08dc5492090bde3ae7))
- **CORE-1318:** add spacing between form fields; ([5d0b622](http://bitbucket.org/thermsio/atalaya/commits/5d0b6220850cc2a321bf7a4c639749d075817c44))
- **CORE-1318:** breakdown distribution in components; ([4cfba41](http://bitbucket.org/thermsio/atalaya/commits/4cfba41e7ea4cf39ceb8f2c6ab7f11f2bb1ed0e1))
- **CORE-1318:** export form components with their innate name ([aeb0f3a](http://bitbucket.org/thermsio/atalaya/commits/aeb0f3af1124ff61ea4120a64c9660077fa7c787))
- **CORE-1318:** export FormFields; ([dd3d237](http://bitbucket.org/thermsio/atalaya/commits/dd3d23752992599151c555d134b233a344a94935))

# [1.4.0-beta.41](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.40...v1.4.0-beta.41) (2021-05-03)

### Bug Fixes

- **CORE-1284:** add ButtonGroup ([db04d98](http://bitbucket.org/thermsio/atalaya/commits/db04d98476280f7f8aee341aee196fe3056959a9))
- **CORE-1284:** add Full width example ([247ec00](http://bitbucket.org/thermsio/atalaya/commits/247ec00a0cce00d7abd9f2edfc79df41e00492d0))

# [1.4.0-beta.40](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.39...v1.4.0-beta.40) (2021-05-03)

### Bug Fixes

- **CORE-1283:** fix pipeline ([4bb1fe6](http://bitbucket.org/thermsio/atalaya/commits/4bb1fe6f566e090838d883c83fd310297d542d26))

# [1.4.0-beta.39](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.38...v1.4.0-beta.39) (2021-04-29)

### Bug Fixes

- **CORE-1313:** add missing prop year key ([28c8671](http://bitbucket.org/thermsio/atalaya/commits/28c86716ad5fec819b807ffe9a6ed7161468f632))
- **CORE-1313:** divider being present on date input alone ([3fab07a](http://bitbucket.org/thermsio/atalaya/commits/3fab07ab51c5dd219eaad3f1993d510ee75871e3))
- **CORE-1313:** fix 24 hour time picker ([88a2094](http://bitbucket.org/thermsio/atalaya/commits/88a20941ddd155959d57b7fc6ada6ada7f2e7b62))
- **CORE-1313:** fix click on next month closing pop up ([2fa6c42](http://bitbucket.org/thermsio/atalaya/commits/2fa6c42953c01c0e3469d04774ec5f1953a2ed16))
- **CORE-1313:** remove arbitrary spacing on form fields ([e42a395](http://bitbucket.org/thermsio/atalaya/commits/e42a395d869144d2ca71b5cf27f748b851371ddb))
- **CORE-1313:** restore display flex inside buttons ([7c02ae7](http://bitbucket.org/thermsio/atalaya/commits/7c02ae7a95ef255812480c3ee4b29db19143debb))
- **CORE-1313:** Stop month picker from changin the year until it has picked a month ([1f630f4](http://bitbucket.org/thermsio/atalaya/commits/1f630f4b5713c4a844c2d0fb6b9e0119c2c7b0c1))

### Features

- **CORE-1313:** add additional props to Inline via divProps; ([d37b047](http://bitbucket.org/thermsio/atalaya/commits/d37b047da6dcdb982c69c283c8111e4caf18d03c))
- **CORE-1313:** add calendar month days logic ([666795c](http://bitbucket.org/thermsio/atalaya/commits/666795c47a1dab47fbfbca7c5a11907d809471f2))
- **CORE-1313:** add DatePicker stylings ([26fee4b](http://bitbucket.org/thermsio/atalaya/commits/26fee4b9a583449501754b3ac18fad40a0edbaa4))
- **CORE-1313:** add DateTimePicker input preview ([ad245cc](http://bitbucket.org/thermsio/atalaya/commits/ad245cc4be83dccdeb8eb8484d7254ddb1fdb72f))
- **CORE-1313:** add default alignment to icon ([07e24bd](http://bitbucket.org/thermsio/atalaya/commits/07e24bd0b7e26949d7d690f03aa03364a4ac514e))
- **CORE-1313:** add fullWidth prop ([566486d](http://bitbucket.org/thermsio/atalaya/commits/566486dc83081574752908a916a4568e6748a714))
- **CORE-1313:** add generic div props to Inline ([84b4424](http://bitbucket.org/thermsio/atalaya/commits/84b442465a8b5ae4e85423c12d24e38284864798))
- **CORE-1313:** add generic div props to Stack ([1a156d8](http://bitbucket.org/thermsio/atalaya/commits/1a156d82efa723c6fdf149cb92d97ad6eccdb06c))
- **CORE-1313:** add invoking and dismiss interaction to date picker ([4360d90](http://bitbucket.org/thermsio/atalaya/commits/4360d90fe900e42d2f7df025ab81b2e4e35dc826))
- **CORE-1313:** add logic to time picker ([402ba2f](http://bitbucket.org/thermsio/atalaya/commits/402ba2fe43ee7fbb4f72e5ab4d1ce428a9fa10f6))
- **CORE-1313:** add MonthPickerl ([e733120](http://bitbucket.org/thermsio/atalaya/commits/e7331201db7719cbc1ab9c764559b74272ca2308))
- **CORE-1313:** add time picking logic ([c022290](http://bitbucket.org/thermsio/atalaya/commits/c022290ddd5240eda77126a279053c24d3b8154c))
- **CORE-1313:** add year picker ([e2f7f60](http://bitbucket.org/thermsio/atalaya/commits/e2f7f6071db8e2d3a8fedfd2aad2bbce8de70edf))
- **CORE-1313:** change TimePicker logic to change timestamp on every action WIP ([1de6465](http://bitbucket.org/thermsio/atalaya/commits/1de64654e9c0c807135438ea0414146defca5047))
- **CORE-1313:** complete date setting implementation ([51dc6d9](http://bitbucket.org/thermsio/atalaya/commits/51dc6d9f669db32a41cb3fcdf016734c3d2dffda))
- **CORE-1313:** dateTime picker returns the picked value via onChangeProps ([35218b4](http://bitbucket.org/thermsio/atalaya/commits/35218b41c47aa07e963af45320e8854fce99f9e9))
- **CORE-1313:** dont alter the date from day picker; ([c8c4f42](http://bitbucket.org/thermsio/atalaya/commits/c8c4f422ab005cc0963ff14a33c4a64df3e3252b))
- **CORE-1313:** finsh calendar styling ([bb5a9b0](http://bitbucket.org/thermsio/atalaya/commits/bb5a9b065b3d323634f1436dd74e7fdded07126e))
- **CORE-1313:** fix time picker not rendering ([c615473](http://bitbucket.org/thermsio/atalaya/commits/c615473cfe970dc419e8fab9177f0d8230225153))
- **CORE-1313:** further tweaks on button stylings ([37a7fa6](http://bitbucket.org/thermsio/atalaya/commits/37a7fa6423c7b9432a24a49f161f781eed28761e))
- **CORE-1313:** made button outline a step bolder ([fa7ec05](http://bitbucket.org/thermsio/atalaya/commits/fa7ec05b2842da4a889e3fd0ff32b53da2aeedbb))
- **CORE-1313:** make number step controls optional ([d601ead](http://bitbucket.org/thermsio/atalaya/commits/d601ead4f82272844a979535044f2f6c5c4a51d4))
- **CORE-1313:** more netural color refinment ([f06f476](http://bitbucket.org/thermsio/atalaya/commits/f06f47619c73dbb23d9bdddacc853b09ae7f3f7c))
- **CORE-1313:** update date picker logic to update timestamp on every action ([9f90039](http://bitbucket.org/thermsio/atalaya/commits/9f9003948052367d51d4145c7622c298b4d4ca7a))
- **CORE-1313:** update neutral faded color ([24192ba](http://bitbucket.org/thermsio/atalaya/commits/24192ba17a8eb8954746aeaaa6918899b9f8486c))
- **CORE-1313:** update stylings ([7551afa](http://bitbucket.org/thermsio/atalaya/commits/7551aface0a206dac10a0c7f32de013a785c1e24))
- **CORE-1313:** update time picker stylings ([54da840](http://bitbucket.org/thermsio/atalaya/commits/54da8409753f620ee83c5675609bb7fb57ec4a05))
- **CORE-1313:** update time picking logic ([3f95bb1](http://bitbucket.org/thermsio/atalaya/commits/3f95bb155d1619c404f505c26f071c3cfdece063))
- **CORE-1313:** use more subtle buttons ([cb5d4f9](http://bitbucket.org/thermsio/atalaya/commits/cb5d4f95510c833281f1d0e09c793d94380c2ab3))

# [1.4.0-beta.38](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.37...v1.4.0-beta.38) (2021-04-24)

### Bug Fixes

- typings path in package.json ([01239d4](http://bitbucket.org/thermsio/atalaya/commits/01239d4e6176b24a7beeed2746e0325a445af01b))

# [1.4.0-beta.37](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.36...v1.4.0-beta.37) (2021-04-24)

### Features

- **Button:** className spread ([1d24d37](http://bitbucket.org/thermsio/atalaya/commits/1d24d37b9280c898d0634f565de1980e4657f3d7))

# [1.4.0-beta.36](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.35...v1.4.0-beta.36) (2021-04-23)

### Features

- **Card:** props spread manual commit again ([25d7bac](http://bitbucket.org/thermsio/atalaya/commits/25d7bacaad31070b236aa21eba2bd1bbd0453a3e))

# [1.4.0-beta.35](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.34...v1.4.0-beta.35) (2021-04-22)

### Bug Fixes

- **CORE-1312:** add hover pointer to form labels ([ef83f00](http://bitbucket.org/thermsio/atalaya/commits/ef83f005cd498869072dfda16d0520aade551322))
- **CORE-1312:** change transition animation ([e5713df](http://bitbucket.org/thermsio/atalaya/commits/e5713df46de495c46c3e56d7859bd5b6ee754cfb))
- **CORE-1312:** fix neutral button text dissapearing when pressed. ([5a85be2](http://bitbucket.org/thermsio/atalaya/commits/5a85be28488d9a41934ac14dc7773b1401abe392))
- **CORE-1312:** make alingn props optional ([a09be47](http://bitbucket.org/thermsio/atalaya/commits/a09be47d926d5a6e74e62cfbabaec1937c4b009c))
- **CORE-1312:** only render label if defined ([9d446c7](http://bitbucket.org/thermsio/atalaya/commits/9d446c7d0a0c3b89344104e3b452f60e22afd930))

### Features

- **CORE-1312:** add password form control ([3d68768](http://bitbucket.org/thermsio/atalaya/commits/3d68768133e774dd55497b87efbda9748c474363))
- **CORE-1312:** add button active stylings ([b90d6b1](http://bitbucket.org/thermsio/atalaya/commits/b90d6b121f7455efd3d4089dc97b1388b85bbc7d))
- **CORE-1312:** add checkbox ([320514c](http://bitbucket.org/thermsio/atalaya/commits/320514c1274aa5c9a3f5b1113862bbe5fa81620f))
- **CORE-1312:** add disabled state ([3f5b762](http://bitbucket.org/thermsio/atalaya/commits/3f5b762fae6f87bf1e0317bf849cb2714e5ed9c1))
- **CORE-1312:** add disabled state to Button ([b34fbe9](http://bitbucket.org/thermsio/atalaya/commits/b34fbe9e4718544259c8f05f1272c8b6310aa8d4))
- **CORE-1312:** add email field ([0d5bd29](http://bitbucket.org/thermsio/atalaya/commits/0d5bd297c2bd4d62c704e9b45b0f1371b60cf7e9))
- **CORE-1312:** add error to FormControlWrapper ([9a92ccc](http://bitbucket.org/thermsio/atalaya/commits/9a92ccc8b10c8a784f05fed9cdb44f032deb9e1e))
- **CORE-1312:** add hint text component ([2f2b292](http://bitbucket.org/thermsio/atalaya/commits/2f2b29299daa6041cc0843715851168d2d18ae97))
- **CORE-1312:** add HTMLSelect ([97782f0](http://bitbucket.org/thermsio/atalaya/commits/97782f0a5ecf8faa66b6c50d2431b1596d47defa))
- **CORE-1312:** add number field ([82dcf63](http://bitbucket.org/thermsio/atalaya/commits/82dcf63c26154cbbc0055746fd2ddede6efc3637))
- **CORE-1312:** add PhoneField ([e1db44b](http://bitbucket.org/thermsio/atalaya/commits/e1db44b144d596ffbd3b17e04b919ebac5fa68ca))
- **CORE-1312:** add prop to display top row or not ([e2e5e08](http://bitbucket.org/thermsio/atalaya/commits/e2e5e08a92fde3fa3fabd1eedcf938f079d2df68))
- **CORE-1312:** add RadioField ([b31f368](http://bitbucket.org/thermsio/atalaya/commits/b31f368b42ee0501ca3b099c8048310c81117602))
- **CORE-1312:** add slider field ([663c493](http://bitbucket.org/thermsio/atalaya/commits/663c493bbedabfb1f7a6f837798e0f2e76d08e70))
- **CORE-1312:** add text semantic faded colors ([3bfb103](http://bitbucket.org/thermsio/atalaya/commits/3bfb10376e12e111b2cf54e047fea1ce181b0b0e))
- **CORE-1312:** add textarea ([3267a52](http://bitbucket.org/thermsio/atalaya/commits/3267a52ce71a7f3f058ee51d4839af79be77d967))
- **CORE-1312:** add TextField component ([91b3bb0](http://bitbucket.org/thermsio/atalaya/commits/91b3bb03a81537213538258ce57bb0a0ec09d70d))
- **CORE-1312:** add toggleSwitch ([6beda35](http://bitbucket.org/thermsio/atalaya/commits/6beda35d43996763edb5d16ce065f1a37a986d1b))
- **CORE-1312:** appease TS Compiler for onFocus events ([0ca30a6](http://bitbucket.org/thermsio/atalaya/commits/0ca30a670226fde4078b838c71cbd22ff97c6816))
- **CORE-1312:** implement FormFieldLabel component ([e335e09](http://bitbucket.org/thermsio/atalaya/commits/e335e09cb05f965919a96165041da9ec3ca1d6a2))
- **CORE-1312:** implement select field logic ([a4b6f3c](http://bitbucket.org/thermsio/atalaya/commits/a4b6f3c1dd6759b316e3ffb94344f1d24343d40d))
- **CORE-1312:** make faded semantic colors more solid ([df5e3c2](http://bitbucket.org/thermsio/atalaya/commits/df5e3c2a7f63002dbef59593e2c1df06117fdbe0))
- **CORE-1312:** make label props optional ([ea651ce](http://bitbucket.org/thermsio/atalaya/commits/ea651ce921b48926066dbeb37325ed313cc96075))
- **CORE-1312:** remove all evidence of preserveTopRow ([6f1f376](http://bitbucket.org/thermsio/atalaya/commits/6f1f376aca54db9f788dec70e2946c496e33d29f))
- **CORE-1312:** remove checkbox ring offset ([0965a4d](http://bitbucket.org/thermsio/atalaya/commits/0965a4d45cc51fd6020b8779c5a8f614741c9011))
- **CORE-1312:** style react-select ([806e98a](http://bitbucket.org/thermsio/atalaya/commits/806e98aa1ff49ff7ce5c1a07a70612ef3ae25869))
- **CORE-1312:** update slider ([a59d1ca](http://bitbucket.org/thermsio/atalaya/commits/a59d1ca0fb526b18077d3842e723e42ed2537223))

# [1.4.0-beta.34](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.33...v1.4.0-beta.34) (2021-04-16)

### Bug Fixes

- z-index tailwind theme ([ff02eb1](http://bitbucket.org/thermsio/atalaya/commits/ff02eb1d1503fa182ff7060482e1c1d023002e04))

# [1.4.0-beta.33](https://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.32...v1.4.0-beta.33) (2021-04-16)

### Bug Fixes

- es bundle output no process and no require ([1bbf552](https://bitbucket.org/thermsio/atalaya/commits/1bbf55215e0238ccac04373feafcbbb6777406bb))

# [1.4.0-beta.32](https://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.31...v1.4.0-beta.32) (2021-04-16)

### Bug Fixes

- build in pipeline skip-ci ([14fdc8b](https://bitbucket.org/thermsio/atalaya/commits/14fdc8bf9177aabb00065eacdc5bc6d9fdf506d3))

# [1.4.0-beta.31](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.30...v1.4.0-beta.31) (2021-04-16)

### Features

- **CORE-1320:** add zIndices tokens ([97627e7](http://bitbucket.org/thermsio/atalaya/commits/97627e795f2f91516dc56a0e4e4b155d8ec79aec))
- **CORE-1320:** export atalaya tailwind config as preset ([3d10131](http://bitbucket.org/thermsio/atalaya/commits/3d1013118b05204b69d145e7d85b2c6563a22301))

# [1.4.0-beta.30](https://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.29...v1.4.0-beta.30) (2021-04-14)

### Bug Fixes

- require to import ([7bda687](https://bitbucket.org/thermsio/atalaya/commits/7bda687ea50a944cdbf84efc8fc263843cd57b4d))

# [1.4.0-beta.29](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.28...v1.4.0-beta.29) (2021-04-14)

### Bug Fixes

- **CORE-1321:** move iconify to dependencies ([b356707](http://bitbucket.org/thermsio/atalaya/commits/b35670756dfd2ddd852d99fc86d099215a99ec5a))

# [1.4.0-beta.28](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.27...v1.4.0-beta.28) (2021-04-13)

### Bug Fixes

- **CORE-13009:** prevent bug when using a 'default' value as the first item of responsiveClasses array ([ee67af0](http://bitbucket.org/thermsio/atalaya/commits/ee67af02c9b60cd00f67def4cd04ae1b15402627))
- **CORE-1309:** make code blocks easier to read on light mode ([94d6b91](http://bitbucket.org/thermsio/atalaya/commits/94d6b910c552241f2c554be61b69c622e4f05e43))

### Features

- **CORE-1309:** add Icon documentation ([af9a66e](http://bitbucket.org/thermsio/atalaya/commits/af9a66e2246581ef0b028ddec531ccbbba4882ad))
- **CORE-1309:** expand text size up to 2xl ([cea4581](http://bitbucket.org/thermsio/atalaya/commits/cea4581174faf68ec94edf32e45b043c1faeb12a))
- **CORE-1309:** update Icon component ([9ab96bc](http://bitbucket.org/thermsio/atalaya/commits/9ab96bc8e2de1fd803ce83a3aa014f1362f02b9e))
- **CORE-1309:** update Icon size prop ([2e9ee17](http://bitbucket.org/thermsio/atalaya/commits/2e9ee17bf5fe373d1859332b3e5346e5e06bf962))

# [1.4.0-beta.27](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.26...v1.4.0-beta.27) (2021-04-08)

### Bug Fixes

- add html body bg and text color to base.css ([e0ad579](http://bitbucket.org/thermsio/atalaya/commits/e0ad579131c11eef6052551bc19a73c1919bef28))

# [1.4.0-beta.26](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.25...v1.4.0-beta.26) (2021-04-08)

### Features

- **CORE-1304:** add Modal, ActionModal ([ecef29e](http://bitbucket.org/thermsio/atalaya/commits/ecef29e763eba906898a73ffc02b1e86f9f843bb))

# [1.4.0-beta.25](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.24...v1.4.0-beta.25) (2021-04-08)

### Bug Fixes

- activate dark theme by default ([926cb35](http://bitbucket.org/thermsio/atalaya/commits/926cb35196ce7d0896afe95b747b169799651b5d))

# [1.4.0-beta.24](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.23...v1.4.0-beta.24) (2021-04-02)

### Features

- **CORE-1304:** center button content ([e7a5cdc](http://bitbucket.org/thermsio/atalaya/commits/e7a5cdc04437144e7aa6ac0c963c0c4d2008cbc3))

# [1.4.0-beta.23](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.22...v1.4.0-beta.23) (2021-04-01)

### Bug Fixes

- useCSS work with vars without '--' ([a1be66e](http://bitbucket.org/thermsio/atalaya/commits/a1be66e145c781d9d2fc413b0fc10e4b833cc14c))

### Features

- **CORE-1305:** add subtle button ([e3bdcfe](http://bitbucket.org/thermsio/atalaya/commits/e3bdcfee87397296835d9df527a74de9d4973edf))
- **CORE-1305:** tweak button stylings ([11f1024](http://bitbucket.org/thermsio/atalaya/commits/11f1024aa6702e792d4597c70f5d288eb6a336e6))

# [1.4.0-beta.22](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.21...v1.4.0-beta.22) (2021-03-31)

### Bug Fixes

- **CORE-1305:** fix critical text color not working ([6abf30f](http://bitbucket.org/thermsio/atalaya/commits/6abf30f39c0fb4374a35098d10e19e28d973faab))

### Features

- **CORE-1305:** add faded semantic color ([e75fed4](http://bitbucket.org/thermsio/atalaya/commits/e75fed42cb984d44e39832ec9285ac2dbc26d0ae))
- **CORE-1305:** add outlined buttons ([df935c4](http://bitbucket.org/thermsio/atalaya/commits/df935c4c5b3a555605f861be02f48140022a7246))
- **CORE-1305:** update neutral dark colors ([193021c](http://bitbucket.org/thermsio/atalaya/commits/193021c70e25bf936a8d380a4cc568acf7a5fc95))
- **CORE-1305:** update semantic text light color ([448072b](http://bitbucket.org/thermsio/atalaya/commits/448072b34231934b60ff25f9b21468601bc446e7))
- update button component ([56beadb](http://bitbucket.org/thermsio/atalaya/commits/56beadbf288cecbea039b5aef36b5ab0974e8683))

# [1.4.0-beta.21](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.20...v1.4.0-beta.21) (2021-03-31)

### Bug Fixes

- **CORE-1279:** fix colors documentation ([b0c6823](http://bitbucket.org/thermsio/atalaya/commits/b0c68237724f36ce40ee8002f92c0f291cc2885c))
- **CORE-1279:** show just 2 decimals for rgb colors ([99c7230](http://bitbucket.org/thermsio/atalaya/commits/99c72307dfde96010c9ad8a64bbc72844c2e7009))
- wrong color value ([f1582f0](http://bitbucket.org/thermsio/atalaya/commits/f1582f04f2724c195821b99d559131e620a9fd18))

### Features

- **CORE-1279:** don't clone Column when used as child of Columns ([6efafc5](http://bitbucket.org/thermsio/atalaya/commits/6efafc5415260546a49eb4df9afe21c53fa0f18f))
- **CORE-1279:** implement ThemeSwitcher ([050c082](http://bitbucket.org/thermsio/atalaya/commits/050c082235a06376b54c5468897744e6a946e1fe))
- **CORE-1279:** improve performance when setting css props ([31dc1a0](http://bitbucket.org/thermsio/atalaya/commits/31dc1a0685b0ff1791f0c575f374a1699fc1fc1d))
- **CORE-1279:** update color documentation when theme changes ([31db1af](http://bitbucket.org/thermsio/atalaya/commits/31db1af432f3d976eb7cae7d181817f0ffafb750))
- **CORE-1279:** update columns docs ([d16d28c](http://bitbucket.org/thermsio/atalaya/commits/d16d28c2df1e31b478a4e8dedf085b406143a24a))

# [1.4.0-beta.20](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.19...v1.4.0-beta.20) (2021-03-29)

### Bug Fixes

- **CORE-1281:** fix stack children props not accepting multiple elements ([c6f87d0](http://bitbucket.org/thermsio/atalaya/commits/c6f87d073050fc8926fa0fc192c0b01d1a03d62a))

### Features

- **CORE-1279:** rename Semantic Type to Variant ([3bf44e8](http://bitbucket.org/thermsio/atalaya/commits/3bf44e820494f088a2d0932002808bd0e0a36cc8))

# [1.4.0-beta.19](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.18...v1.4.0-beta.19) (2021-03-12)

### Features

- **useTheme:** add hook ([cc8f4df](http://bitbucket.org/thermsio/atalaya/commits/cc8f4df74ea9c3d0ac77757a4de362ea9ea486d9))

# [1.4.0-beta.18](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.17...v1.4.0-beta.18) (2021-03-11)

### Features

- **CORE-1279:** tweak surface & caution dark colors ([71e3a57](http://bitbucket.org/thermsio/atalaya/commits/71e3a5723dfc8f5845201d02acc44b2d99227a39))

# [1.4.0-beta.17](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.16...v1.4.0-beta.17) (2021-03-09)

### Bug Fixes

- **CORE-1252:** remove rounded corners on Image thumbnail ([8ddf603](http://bitbucket.org/thermsio/atalaya/commits/8ddf603f9f13c4ccd49f6ddb7372d39fef3dddc4))

### Features

- **CORE-1252:** add fontSize and textColor to layout base props ([1e901a1](http://bitbucket.org/thermsio/atalaya/commits/1e901a14164be634eddd70783bb3230892a736b9))
- **CORE-1252:** center text in tags by default ([55cbe09](http://bitbucket.org/thermsio/atalaya/commits/55cbe09690f781850b0de74929d041c59b179749))
- **CORE-1252:** implement Icon component ([a25a59d](http://bitbucket.org/thermsio/atalaya/commits/a25a59d4f24e27cbc2a8c0b52136a771b2a9d164))

# [1.4.0-beta.16](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.15...v1.4.0-beta.16) (2021-03-08)

### Bug Fixes

- production build failing ([f86fc40](http://bitbucket.org/thermsio/atalaya/commits/f86fc409b44a47efb625964cf042791ed348289a))

### Features

- **CORE-1252:** add Icon component ([c3234b4](http://bitbucket.org/thermsio/atalaya/commits/c3234b4cd6a34189ff69294bf0226c372d032666))

# [1.4.0-beta.15](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.14...v1.4.0-beta.15) (2021-03-05)

### Bug Fixes

- **CORE-1219:** fix margin and padding classes being purged ([55c504f](http://bitbucket.org/thermsio/atalaya/commits/55c504fa6594c48989ba76c3318c33918aed98b2))

# [1.4.0-beta.14](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.13...v1.4.0-beta.14) (2021-03-05)

### Bug Fixes

- **CORE-1219:** fix purgeCSS ignoring ts files ([7e0de03](http://bitbucket.org/thermsio/atalaya/commits/7e0de030c81aa26b894adc7c9b615a845e59c704))

# [1.4.0-beta.13](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.12...v1.4.0-beta.13) (2021-03-05)

### Bug Fixes

- **CORE-1219:** update borderColor props with semantics ([e0e22b4](http://bitbucket.org/thermsio/atalaya/commits/e0e22b4c4cab6398b324ff0bae80b7d7c9ceefa0))

### Features

- **CORE-1219:** update PrioritySquare with new semantics ([97106c0](http://bitbucket.org/thermsio/atalaya/commits/97106c070c66ee02482ae33f7f88a4e6e4b619fe))

# [1.4.0-beta.12](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.11...v1.4.0-beta.12) (2021-03-05)

### Bug Fixes

- **CORE-1252:** rename Semantic type to SemanticType; add subset: PriorityType ([0ea6a2c](http://bitbucket.org/thermsio/atalaya/commits/0ea6a2ccff904f21cb0b273e91184750f8762519))

### Features

- **CORE-1252:** export general types ([f98c594](http://bitbucket.org/thermsio/atalaya/commits/f98c5949b8e2dd5ed1698bf15b5ad68b70afbc48))

# [1.4.0-beta.11](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.10...v1.4.0-beta.11) (2021-03-04)

### Bug Fixes

- **CORE-1219:** fix button not using new semantic classes ([edd55b7](http://bitbucket.org/thermsio/atalaya/commits/edd55b733e631b70aa22a2566a65652a06e43a54))
- **CORE-1219:** fix semantic colors not being displayed ([25d1358](http://bitbucket.org/thermsio/atalaya/commits/25d1358f2e8c9b673805ea4c0dcdc0b634fb647e))
- **CORE-1219:** fix Tag test fail ([9ea4af9](http://bitbucket.org/thermsio/atalaya/commits/9ea4af914c15e93270c7625ee103703d55ce2054))
- **CORE-1219:** make tag semantic background classes purgeable ([6fd7265](http://bitbucket.org/thermsio/atalaya/commits/6fd7265bc5031960dccf27b626ffcbddb752a7b0))
- **CORE-1219:** update Badge test ([572e9d3](http://bitbucket.org/thermsio/atalaya/commits/572e9d391fffa05462c0ab2a654c3a9cd242adf0))

### Features

- **CORE-1219:** simplify design tokens ([86770ac](http://bitbucket.org/thermsio/atalaya/commits/86770ac57bd13c6fbea617edc92b1c36d36f6ee4))
- **CORE-1219:** update badge with new semantic naming ([8ebd807](http://bitbucket.org/thermsio/atalaya/commits/8ebd807bcf5965fe59855b18b6fea7874dd99bff))
- **CORE-1219:** update color docs ([b9ce7ff](http://bitbucket.org/thermsio/atalaya/commits/b9ce7ff20443c0f21291249c9c662ad05f573761))
- **CORE-1219:** update color token list with new tokens ([c0ab33c](http://bitbucket.org/thermsio/atalaya/commits/c0ab33c55131f13c52d6cb411cd7323ba4ac2fd8))
- **CORE-1219:** update semantic color list ([74c3da9](http://bitbucket.org/thermsio/atalaya/commits/74c3da9675f876818e72a39700a73d07f99c5036))
- **CORE-1219:** update styleguide config with new semantic naming ([8b8d146](http://bitbucket.org/thermsio/atalaya/commits/8b8d146eb08e11dba34583f8efc22a61f3048230))
- **CORE-1219:** update tag to use new semantic names ([f36a87f](http://bitbucket.org/thermsio/atalaya/commits/f36a87fe20c4b49a5f29c63db3f7de6acd6b4bc2))

# [1.4.0-beta.10](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.9...v1.4.0-beta.10) (2021-03-03)

### Bug Fixes

- **CORE-1251:** update inline children type ([c0c7d21](http://bitbucket.org/thermsio/atalaya/commits/c0c7d212033b7944d82bddaee7fa0a0f330e91d9))

# [1.4.0-beta.9](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.8...v1.4.0-beta.9) (2021-03-03)

### Bug Fixes

- **CORE-1251:** make divide prop optional ([edb3d18](http://bitbucket.org/thermsio/atalaya/commits/edb3d1889d5c87d636d189ebad1bd8a2efe3229e))

### Features

- **CORE-1251:** add sm prop to tagGroup ([84d156c](http://bitbucket.org/thermsio/atalaya/commits/84d156c333d5bfde3f89c80b02e429f916f3cbb9))
- **CORE-1251:** add TagGroup ([8ba2835](http://bitbucket.org/thermsio/atalaya/commits/8ba2835a3e69a2ca14e504d6460c4069a321065b))
- **CORE-1251:** fix Inline wrap spacing ([4bace58](http://bitbucket.org/thermsio/atalaya/commits/4bace58b0c11d95f870eb3e6cbe464b6ec8faa4c))
- **CORE-1251:** remove ActivityType ([07c5259](http://bitbucket.org/thermsio/atalaya/commits/07c525994b059fd6c4b9f5fbb5d1ab0c00ffd573))
- **CORE-1251:** update tag styling and add sm prop ([dcfd760](http://bitbucket.org/thermsio/atalaya/commits/dcfd760bdcd8e475f45a90a1ede943c643890c6b))

# [1.4.0-beta.8](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.7...v1.4.0-beta.8) (2021-03-02)

### Bug Fixes

- **CORE-1251:** stop crash when Columns had a null child ([d0f18c8](http://bitbucket.org/thermsio/atalaya/commits/d0f18c8f169384b3a8f01a5e1d45699adb595d36))

### Features

- **CORE-1251:** update inline docs placeholder ([9fa940c](http://bitbucket.org/thermsio/atalaya/commits/9fa940c08edcb318c2b1bc1d639521c8ea2aab4b))

# [1.4.0-beta.7](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.6...v1.4.0-beta.7) (2021-03-02)

### Bug Fixes

- **CORE-1251:** stop double spacing between children when using Inline wrap prop ([a779b50](http://bitbucket.org/thermsio/atalaya/commits/a779b50d3cbfab8767a35e373e6e94ee508f1f60))

# [1.4.0-beta.6](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.5...v1.4.0-beta.6) (2021-03-01)

### Features

- **CORE-1251:** remove wrapper prop from Inline ([9bd4885](http://bitbucket.org/thermsio/atalaya/commits/9bd4885b07af9b681aa556d0b6f8c59dda5a4881))

# [1.4.0-beta.5](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.4...v1.4.0-beta.5) (2021-03-01)

### Bug Fixes

- **CORE-1251:** fix negative margin collapsing on Inline ([e9d61ad](http://bitbucket.org/thermsio/atalaya/commits/e9d61ad19a07e21c1d34de575bbd86dc09c73b5b))
- **CORE-1251:** remove className from accepted props ([2a7b5f8](http://bitbucket.org/thermsio/atalaya/commits/2a7b5f8368616726ac49fade0f57ddec6530f8db))

### Features

- **CORE-1251:** add optional classNames prop to layout components ([ba5da84](http://bitbucket.org/thermsio/atalaya/commits/ba5da84f628c9181afc2994354d96e96f752b25e))

# [1.4.0-beta.4](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.3...v1.4.0-beta.4) (2021-02-26)

### Bug Fixes

- **CORE-1249:** make PrioritySquare support margins ([4aba71c](http://bitbucket.org/thermsio/atalaya/commits/4aba71c4eed8d435a466aa6a555321dd29f477bb))
- **CORE-1249:** remove 'undefined' classes from final HTML ([6d2d08c](http://bitbucket.org/thermsio/atalaya/commits/6d2d08c20d1176e73438dbb07716b9fd4e5781dd))
- **CORE-1249:** remove DispatchUnassignedListItem ([940dbb6](http://bitbucket.org/thermsio/atalaya/commits/940dbb67b3b48247b2cdc063e00bb7d5577b22c8))
- **CORE-1249:** stop crash when providing an undefined class ([c84e635](http://bitbucket.org/thermsio/atalaya/commits/c84e6355bfc6f679b650931f5d68e99aa7af5270))
- **CORE-1249:** stop crash when rendering empty child on Inline ([abdd9f8](http://bitbucket.org/thermsio/atalaya/commits/abdd9f8990a650a48a98d3ecb3ae423494bf5394))

### Features

- **CORE-1249:** add semantic colors to borders ([de8816a](http://bitbucket.org/thermsio/atalaya/commits/de8816a971ab1e55716661159a7aa286953dc1be))
- **CORE-1249:** add sm prop to ActivityType ([2f0f070](http://bitbucket.org/thermsio/atalaya/commits/2f0f070babd893f639ef746abd5b90fd5050eb5c))
- **CORE-1249:** enable ActivityType to accept margins ([4d06a50](http://bitbucket.org/thermsio/atalaya/commits/4d06a50a387856b07656777d2329aa1609708784))

# [1.4.0-beta.3](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.2...v1.4.0-beta.3) (2021-02-26)

### Features

- **CORE-1247:** add content option to with prop on Column ([8ed478e](http://bitbucket.org/thermsio/atalaya/commits/8ed478e3ed422a27a2b445be85d941b28fa70887))
- **CORE-1247:** export PrioritySquare ([cc8654d](http://bitbucket.org/thermsio/atalaya/commits/cc8654d6b6566f2cfffe3e88250716d981dc71f3))
- **CORE-1249:** add child divers border props to layout components ([061e814](http://bitbucket.org/thermsio/atalaya/commits/061e8148819602dc55f287f785fc531d6537cfd3))
- **CORE-1249:** add DateTime component ([4669305](http://bitbucket.org/thermsio/atalaya/commits/4669305e8615f6f68da4cb3b345ac7be9effca6d))

# [1.4.0-beta.2](http://bitbucket.org/thermsio/atalaya/compare/v1.4.0-beta.1...v1.4.0-beta.2) (2021-02-25)

### Bug Fixes

- **CORE-1244:** make Card background optional prop ([3efc6a0](http://bitbucket.org/thermsio/atalaya/commits/3efc6a02af9524d382355eb5486d9f969f8bdb78))

### Features

- **CORE-1244:** add background prop to Card ([bb0fe4c](http://bitbucket.org/thermsio/atalaya/commits/bb0fe4cb5a1a1d34827a9ace6adb270e7e6d5073))
- **CORE-1244:** add fillY prop to columns ([81b3b47](http://bitbucket.org/thermsio/atalaya/commits/81b3b470439b156f2cee55b42ffa9cddcf1e6afb))
- **CORE-1244:** add horizontal and vertical props to margin and padding ([cbfcf15](http://bitbucket.org/thermsio/atalaya/commits/cbfcf151c861b59ce37316a4b5f98f2505db1455))
- **CORE-1244:** add vertical alignment to stack ([d4de3ae](http://bitbucket.org/thermsio/atalaya/commits/d4de3ae8406c3a61526205ee6db9a47b2a3dcc41))

# [1.4.0-beta.1](http://bitbucket.org/thermsio/atalaya/compare/v1.3.0...v1.4.0-beta.1) (2021-02-24)

### Features

- **CORE-1219:** update color description with current values ([948a717](http://bitbucket.org/thermsio/atalaya/commits/948a717fb5965e1130d8b211d25983dd4fdb996b))

# [1.3.0](http://bitbucket.org/thermsio/atalaya/compare/v1.2.0...v1.3.0) (2021-02-24)

### Features

- manual feat commit ([f4871a5](http://bitbucket.org/thermsio/atalaya/commits/f4871a5a3373f52667f00317d87e24045fa22790))

manually removed changelog for cleanup during development (cory)
