// Shared config used by Rollup, Cosmos and Styleguidist
module.exports = {
  minimize: true,
  plugins: [
    require('postcss-import'),
    require('tailwindcss/nesting'),
    require('tailwindcss'),
    require('postcss-combine-duplicated-selectors'),
    require('autoprefixer'),
  ],
}
