// This webpack config is used for react-cosmos dev tool
module.exports = {
  cache: false,
  mode: 'development',

  watch: true,

  devtool: 'source-map',

  stats: 'errors-only',

  module: {
    rules: [
      {
        test: /\.(png|gif|jpe?g)/,
        use: 'file-loader',
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader', 'postcss-loader'],
      },

      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'ts-loader',
          options: {
            transpileOnly: true,
          },
        },
      },
    ],
  },

  resolve: {
    extensions: ['.tsx', '.ts', '.js', 'jsx'],

    plugins: [],
  },
}
