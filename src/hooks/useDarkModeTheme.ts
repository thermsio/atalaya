import { useEffect, useMemo, useState } from 'react'
import { useCSS } from './useCSS'
import { useLocalStorage } from './useLocalStorage'
import { Constants } from '../constants'
import { themeType } from '../styles/context/StylesContext'

const LS_KEY = 'atalaya_dark_mode'

export function useDarkModeTheme(
  {
    dark = Constants.Color.Dark,
    light = Constants.Color.Light,
  }: themeType = {},
  defaultTheme: 'dark' | 'light' = 'dark',
): [boolean, (isDarkMode: boolean) => void] {
  const [isDarkMode, setIsDarkMode] = useLocalStorage<boolean>(
    LS_KEY,
    defaultTheme === 'dark',
  )

  const cssVars = useMemo(() => {
    const themes = { dark: {}, light: {} }

    for (const [key, cssVar] of Object.entries(Constants.Color.CSSVarMap)) {
      themes.dark[cssVar] = dark[key]
      themes.light[cssVar] = light[key]
    }

    return themes
  }, [dark, light])

  const [activeTheme, setActiveTheme] = useState(
    isDarkMode ? cssVars.dark : cssVars.light,
  )

  useCSS(activeTheme)

  useEffect(() => {
    setActiveTheme(isDarkMode ? cssVars.dark : cssVars.light)
  }, [isDarkMode])

  return [isDarkMode, setIsDarkMode]
}
