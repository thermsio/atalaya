import React, { useEffect, useReducer } from 'react'

type ElementsInitialState = { [key: string]: boolean }

interface ReducerAction {
  elementName: string
  overflowHidden: boolean
}

const reducer: React.Reducer<ElementsInitialState, ReducerAction> = (
  state,
  action,
) => {
  state[action.elementName] = action.overflowHidden
  return state
}

/**
 *
 * @param {boolean} shouldLock - When true, selected elements will be prevented from scrolling.
 * @param {string[]} [elementsToFreeze=['body', 'html']] - Array containing token strings for the elements to select to freeze.
 */
const usePageScrollLock = (shouldLock, elementsToFreeze = ['body', 'html']) => {
  const [elementsInitialState, dispatchElementInitialState] = useReducer(
    reducer,
    {},
  )

  const lockPageScroll = () =>
    elementsToFreeze.forEach((elementName) => {
      const element = document.querySelector(elementName)

      if (element && !elementsInitialState.hasOwnProperty(elementName)) {
        dispatchElementInitialState({
          elementName,
          overflowHidden: element.classList.contains('overflow-hidden'),
        })
      }

      element?.classList.add('overflow-hidden')
    })

  const releasePageScroll = () => {
    elementsToFreeze.forEach((elementName) => {
      if (
        elementsInitialState.hasOwnProperty(elementName) &&
        !elementsInitialState[elementName]
      ) {
        document.querySelector(elementName)?.classList.remove('overflow-hidden')
      }
    })
  }

  useEffect(() => {
    shouldLock ? lockPageScroll() : releasePageScroll()

    return () => releasePageScroll()
  }, [shouldLock])
}

export { usePageScrollLock }
