// Based on https://usehooks.com/useMedia/
import { useEffect } from 'react'
import { breakpoints } from '../tokens/breakpoints'
import { useContainerDimensionsContext } from '../layout/ContainerDimensions/useContainerDimensionsContext'
import { useStateDebounced } from './useStateDebounced'

const values = { xxl: 0, xl: 1, lg: 2, md: 3, sm: 4 }
const mediaQueries = [
  `(min-width: ${breakpoints['breakpoint-xxl']})`,
  `(min-width: ${breakpoints['breakpoint-xl']})`,
  `(min-width: ${breakpoints['breakpoint-lg']})`,
  `(min-width: ${breakpoints['breakpoint-md']})`,
  `(min-width: ${breakpoints['breakpoint-sm']})`,
]

/**
 * This hook will return `true` when the viewport width is at least the size of the specified breakpoint.
 *  Valid breakpoints: xxl, xl, lg, md, sm
 */
const useIsBreakpointActive = (
  breakpoint: 'xxl' | 'xl' | 'lg' | 'md' | 'sm',
): boolean => {
  // If this hook is used as a child of <ContainerDimensionsContextProvider /> then we respect it's "viewport" width instead of the window viewport
  const containerDimensionsCtx = useContainerDimensionsContext(false)

  const mediaQueryLists = mediaQueries.map((q) => {
    try {
      return window.matchMedia(q)
    } catch (e) {
      return undefined
    }
  })

  const getValue = () => {
    let index = mediaQueryLists?.findIndex((mql) => mql?.matches) ?? 0

    if (index === -1) index = 5

    return index - values[breakpoint] < 0
  }

  const [, value, setValue] = useStateDebounced(getValue)

  useEffect(() => {
    if (containerDimensionsCtx?.viewport?.width) {
      const isActive =
        containerDimensionsCtx.viewport.width >
        parseInt(breakpoints[`breakpoint-${breakpoint}`], 10)

      setValue(isActive)
    } else {
      const handler = () => setValue(getValue)

      mediaQueryLists?.forEach((mql) =>
        mql?.addEventListener('change', handler),
      )

      return () => {
        mediaQueryLists?.forEach((mql) =>
          mql?.removeEventListener('change', handler),
        )
      }
    }
  }, [])

  return value
}

export { useIsBreakpointActive }
