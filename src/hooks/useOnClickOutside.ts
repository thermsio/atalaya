import { RefObject, useEffect, useRef } from 'react'

export function useOnClickOutside(
  ref: RefObject<any>,
  handler?: (...args: any[]) => void,
  /** Optionally turn off listeners, ie: the component is not concerned with click events until some state is set in the parent */
  ignoreClicks?: boolean,
): void {
  const local = useRef(handler)

  local.current = handler

  useEffect(() => {
    if (!local.current || !ref.current || ignoreClicks) return

    const listener = (event: Event) => {
      // Do nothing if clicking ref's element or descendent elements
      if (!ref.current || ref.current?.contains(event.target)) return

      local.current?.(event)
    }

    document?.addEventListener('mousedown', listener, false)
    document?.addEventListener('touchstart', listener, false)

    return () => {
      document?.removeEventListener('mousedown', listener, false)
      document?.removeEventListener('touchstart', listener, false)
    }
  }, [ignoreClicks, ref.current])
}
