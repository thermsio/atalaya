import { useState, useLayoutEffect } from 'react'

export function useMemoDebounced<T = unknown>(
  value: () => T,
  deps: any[],
  delay: number,
) {
  const [debouncedValue, setDebouncedValue] = useState<T>(value)

  if (!deps) throw new Error('deps array is required')
  if (!delay) throw new Error('delay is required')

  useLayoutEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(value())
    }, delay)

    return () => {
      clearTimeout(handler)
    }
  }, deps)

  return debouncedValue
}
