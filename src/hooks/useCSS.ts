import { RefObject, useLayoutEffect } from 'react'

/*
  EXAMPLE:

// Usage
const theme = {
  'padding': '16px',
  'font-size': '14px',
  'border-radius': '4px',
  'border': 'none',
  'color': '#FFF',
  'background': '#6772e5',
  'hover-border': 'none',
  'hover-color': '#FFF'
};

function App() {
  const ref = useRef()

  useCSS(theme, { ref })
  // useCSS(theme, { element: document.querySelector('button') })
  // useCSS(theme)

  return (
    <div>
      <button className="button" ref={ref}>Button</button>
    </div>
  )
}


*/

export function useCSS(
  styles: { [key: string]: string | number },
  { element, ref }: { element?: Element; ref?: RefObject<any> } = {},
): void {
  useLayoutEffect(() => {
    try {
      const $el = element || ref?.current || document.documentElement

      for (const key in styles) {
        // allow passing with or without --
        const cssVar = key[0] != '-' ? `--${key}` : key

        $el.style.setProperty(cssVar, styles[key])
      }
    } catch (e: any) {
      console.error('useCss() error: ', e?.message || e)
    }
  }, [styles])
}
