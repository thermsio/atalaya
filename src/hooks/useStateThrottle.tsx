import { Dispatch, SetStateAction, useEffect, useRef, useState } from 'react'

export function useStateThrottle<S = unknown>(
  initialState: S | (() => S),
  interval?: number,
): [S, S, Dispatch<SetStateAction<S>>]

export function useStateThrottle<S = undefined>(
  undefined,
  interval?: number,
): [S | undefined, S | undefined, Dispatch<SetStateAction<S | undefined>>]

export function useStateThrottle(initState, interval = 500) {
  const [state, setState] = useState(initState)
  const [throttledValue, setThrottledValue] = useState(initState)
  const lastExecuted = useRef<number>(Date.now())

  useEffect(() => {
    if (Date.now() >= lastExecuted.current + interval) {
      lastExecuted.current = Date.now()
      setThrottledValue(state)
    } else {
      const timeoutId = setTimeout(() => {
        lastExecuted.current = Date.now()
        setThrottledValue(state)
      }, interval)

      return () => clearTimeout(timeoutId)
    }
  }, [state, interval])

  return [state, throttledValue, setState]
}
