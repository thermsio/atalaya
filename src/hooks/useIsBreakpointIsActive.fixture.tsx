import React, { useEffect, useState } from 'react'
import { useIsBreakpointActive } from './useIsBreakpointActive'

const base = () => {
  const [updateCount, setUpdateCount] = useState(0)
  const isBreakpointActive = useIsBreakpointActive('md')

  console.log('isBreakpointActive updated')

  useEffect(() => {
    setUpdateCount((_updateCount) => _updateCount + 1)
  }, [isBreakpointActive])

  return (
    <div>
      <div>
        is Md breakpoint active? {isBreakpointActive ? 'True' : 'False'}
      </div>
      <div>Update Count: {updateCount}</div>
    </div>
  )
}

export default { base }
