import { useState, useEffect, RefObject } from 'react'

/* // Usage
function App() {
  // Call our hook for each key that we'd like to monitor
  const happyPress = useKeyPress('h')
  const sadPress = useKeyPress('s')
  const robotPress = useKeyPress('r')
  const foxPress = useKeyPress('f')

  return (
    <div>
      <div>h, s, r, f</div>
      <div>
        {happyPress && '😊'}
        {sadPress && '😢'}
        {robotPress && '🤖'}
        {foxPress && '🦊'}
      </div>
    </div>
  )
} */

// Hook
const useKeyPress = (
  targetKey: string,
  ref?: RefObject<HTMLInputElement>,
): boolean => {
  // State for keeping track of whether key is pressed
  const [keyPressed, setKeyPressed] = useState(false)

  // If pressed key is our target key then set to true
  function downHandler(e: KeyboardEvent) {
    if (e.key === targetKey) {
      setKeyPressed(true)
    }
  }

  // If released key is our target key then set to false
  const upHandler = (e: KeyboardEvent) => {
    if (e.key === targetKey) {
      setKeyPressed(false)
    }
  }

  // Add event listeners
  useEffect(() => {
    if (ref) {
      ref.current?.addEventListener('keydown', downHandler)
      ref.current?.addEventListener('keyup', upHandler)
    } else {
      window.addEventListener('keydown', downHandler)
      window.addEventListener('keyup', upHandler)
    }
    // Remove event listeners on cleanup
    return () => {
      if (ref) {
        ref.current?.removeEventListener('keydown', downHandler)
        ref.current?.removeEventListener('keyup', upHandler)
      } else {
        window.removeEventListener('keydown', downHandler)
        window.removeEventListener('keyup', upHandler)
      }
    }
  }, []) // Empty array ensures that effect is only run on mount and unmount

  return keyPressed
}

export { useKeyPress }
