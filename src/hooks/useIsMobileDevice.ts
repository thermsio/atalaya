// https://medium.com/react-bootcamp/how-to-create-a-custom-usedevicedetect-react-hook-f5a1bfe64599
import React from 'react'

const useIsMobileDevice = (): boolean => {
  const [isMobile, setMobile] = React.useState(false)

  React.useEffect(() => {
    const userAgent =
      typeof window.navigator === 'undefined' ? '' : navigator.userAgent
    const mobile = Boolean(
      userAgent.match(
        /Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile|WPDesktop/i,
      ),
    )
    setMobile(mobile)
  }, [])

  return isMobile
}

export { useIsMobileDevice }
