import { useContext } from 'react'
import { StylesContext, StylesContextType } from '../context/StylesContext'

const useStylesContext = (): StylesContextType => {
  const stylesContext = useContext(StylesContext)

  if (stylesContext === undefined) {
    console.warn(
      'useAtalayaContext() must be a descendant of <AtalayaWrapper />',
    )
  }

  return stylesContext
}

export { useStylesContext }
