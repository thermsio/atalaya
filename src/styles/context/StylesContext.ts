import { createContext } from 'react'
import { Constants } from '../../constants'

export type themeType = {
  dark?: Partial<typeof Constants.Color.Dark>
  light?: Partial<typeof Constants.Color.Light>
}

export type StylesContextType = {
  isDarkMode?: boolean
  theme: themeType
  toggleDarkMode?: () => void
}

const StylesContext = createContext({
  isDarkMode: true,
  theme: Constants.Color,
  toggleDarkMode: undefined,
} as StylesContextType)

export { StylesContext }
