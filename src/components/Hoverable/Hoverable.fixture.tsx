import React from 'react'
import { Hoverable } from './index'

const simple = () => {
  return (
    <Hoverable>
      <div className="min-w-[300px]">Testing this</div>
      <Hoverable.HoverContent>Show me!</Hoverable.HoverContent>
    </Hoverable>
  )
}

const containerEl = () => {
  return (
    <div>
      <div>TITLE</div>

      <Hoverable containerElement={<span />}>
        <span className="p-base">Testing this</span>

        <Hoverable.HoverContent>Show me!</Hoverable.HoverContent>
      </Hoverable>

      <div>SOME TEXT STUFF</div>
    </div>
  )
}

const advancedExample = () => {
  return (
    <Hoverable containerElement={<div className="relative" />}>
      <div>A Hoverable w/ `containerElement`</div>

      <Hoverable.HoverContent>
        <div className="absolute left-0 top-0 rounded bg-main p-base">
          I am only shown when hovering!
        </div>
      </Hoverable.HoverContent>
    </Hoverable>
  )
}

export default { simple, containerEl, advancedExample }
