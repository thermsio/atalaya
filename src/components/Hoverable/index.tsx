import { useToggle, useUpdateEffect } from 'ahooks'
import React, { useContext, useRef } from 'react'

interface HoverableProps {
  children: React.ReactNode
  /** Default container element is a <div />, optionally override the container element */
  containerElement?: React.ReactElement
  /** If true then the Hoverable.Content will be shown when Hoverable is first mounted */
  defaultShow?: boolean
  /** Optionally provide a style to be applied to the container element when hovering */
  hoverStyle?: React.CSSProperties
  onHoverStateChange?: (args: { isHovering: boolean }) => void
}

const HoverContext = React.createContext({
  isHovering: false,
})

function useHoverableContext() {
  return useContext(HoverContext)
}

function Hoverable({
  children,
  containerElement,
  defaultShow,
  hoverStyle,
  onHoverStateChange,
}: HoverableProps) {
  const local = useRef({ onHoverStateChange })
  local.current.onHoverStateChange = onHoverStateChange

  const [show, toggle] = useToggle(!!defaultShow)

  useUpdateEffect(() => {
    if (local.current.onHoverStateChange)
      local.current.onHoverStateChange({ isHovering: show })
  }, [show])

  return (
    <HoverContext.Provider value={{ isHovering: show }}>
      {containerElement ? (
        React.cloneElement(containerElement, {
          children,
          onBlur: toggle.setLeft,
          onFocus: toggle.setRight,
          onMouseLeave: toggle.setLeft,
          onMouseOver: toggle.setRight,
          style: hoverStyle,
        })
      ) : (
        <div
          data-id="hoverable-container"
          onBlur={toggle.setLeft}
          onFocus={toggle.setRight}
          onMouseLeave={toggle.setLeft}
          onMouseOver={toggle.setRight}
          style={show ? hoverStyle : undefined}
        >
          {children}
        </div>
      )}
    </HoverContext.Provider>
  )
}

function HoverContent({ children }) {
  const hoverCtx = useContext(HoverContext)

  if (!hoverCtx.isHovering) return null

  return children
}

function TextEllipse({ text, ellipse }: { text?: string; ellipse: number }) {
  const hoverCtx = useContext(HoverContext)

  if (!text) return null

  if (!hoverCtx.isHovering) {
    return (
      <>
        {text?.substring(0, ellipse)}
        {text?.length > ellipse && '...'}
      </>
    )
  }

  return <>{text}</>
}

Hoverable.HoverContent = HoverContent
Hoverable.TextEllipse = TextEllipse

export { Hoverable, useHoverableContext }
