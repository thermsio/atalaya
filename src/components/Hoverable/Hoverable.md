### Simple

```jsx
<Hoverable>
  <div className="min-w-max rounded bg-positive p-base">A Hoverable!</div>

  <Hoverable.HoverContent>I'm only shown when hovering!</Hoverable.HoverContent>
</Hoverable>
```

### Text Ellipse

```jsx
import {Hoverable} from "./index";

;<Hoverable>
    <div className="min-w-max rounded bg-positive p-base">A Hoverable!</div>

    <div>
        This text is ellipsed when not hovering: <Hoverable.TextEllipse ellipse={7} text="Something really long" />
    </div>

    <Hoverable.HoverContent>I'm only shown when hovering!</Hoverable.HoverContent>
</Hoverable>
```

### With Container Element

```jsx
<Hoverable containerElement={<span className="bg-main p-base" />}>
  <div>A Hoverable w/ `containerElement`</div>

  <Hoverable.HoverContent>I'm only shown when hovering!</Hoverable.HoverContent>
</Hoverable>
```

### Advanced Example

```jsx
import { Hoverable } from './index'
;<Hoverable containerElement={<div className="relative" />}>
  <div>A Hoverable w/ `containerElement`</div>

  <Hoverable.HoverContent>
    <div className="absolute left-0 top-0 w-12 rounded bg-main">
      I'm only shown when hovering!
    </div>
  </Hoverable.HoverContent>
</Hoverable>
```

#### Context

If you need to drill into the _is hovering_ state you can use `useHoverableContext()`. This is useful for when you need to conditionally hide or render something based on the hover state.
and `<Hoverable.HoverContent />` doesn't solve your use-case. 

> This context is only available as a child of `<Hoverable />`

`const { isHovering } = useHoverableContext()`
