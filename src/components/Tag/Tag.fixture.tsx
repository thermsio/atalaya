import React from 'react'
import { Tag } from './index'

const base = <Tag value="Base Tag" />

const contrastText = (
  <Tag
    backgroundColor="#00ffc3"
    value="Background color is set, text should automatically contrast it"
  />
)

const contrastTextLight = (
  <Tag
    backgroundColor="#00503d"
    value="Background color is set, text should be light"
  />
)

const largeTag = <Tag value="Small Tag" lg />

const subtle = (
  <Tag
    backgroundColor="#00ffc3"
    subtle
    value="Background color is set, text should automatically contrast it"
  />
)

const subtleVariant = (
  <Tag
    variant="main"
    subtle
    value="Background color is set, text should automatically contrast it"
  />
)

const subtleVariantAndColor = (
  <Tag
    backgroundColor="#00ffc3"
    variant="positive"
    subtle
    value="Background color is set, text should automatically contrast it"
  />
)

const subtleBackground = (
  <Tag
    backgroundColor="#00ffc3"
    subtle
    subtleBackground="surface-strong"
    value="Background color is set, text should automatically contrast it"
  />
)

const subtleBackgroundCustom = (
  <Tag
    backgroundColor="#00ffc3"
    subtle
    subtleBackground="#421150"
    value="Background color is set, text should automatically contrast it"
  />
)

const subtleNoColor = (
  <Tag
    subtle
    value="Background color is set, text should automatically contrast it"
  />
)

export default {
  base,
  contrastText,
  contrastTextLight,
  largeTag,
  subtle,
  subtleVariant,
  subtleVariantAndColor,
  subtleBackground,
  subtleBackgroundCustom,
  subtleNoColor,
}
