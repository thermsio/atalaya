Used to label elements on your app. They only accept strings and should be kept as brief as possible

```jsx
import { TagGroup } from '../TagGroup'
;<TagGroup max={6}>
  <Tag variant="main" value="Main" />
  <Tag variant="neutral" value="Neutral" />
  <Tag variant="positive" value="Positive" />
  <Tag variant="caution" value="Caution" />
  <Tag variant="critical" value="Critical" />
  <Tag variant="info" value="Info" />
</TagGroup>
```

### Large

Using the `lg` prop will result in larger tags

```jsx
import { TagGroup } from '../TagGroup'
;<TagGroup max={6} lg>
  <Tag sm variant="main" value="Main" />
  <Tag sm variant="neutral" value="Neutral" />
  <Tag sm variant="positive" value="Positive" />
  <Tag sm variant="caution" value="Caution" />
  <Tag sm variant="critical" value="Critical" />
  <Tag sm variant="info" value="Info" />
</TagGroup>
```

## Custom Color

If you wish to set the tag's background color to other different from the semantic variants you may do so by using
the `backgroundColor` prop, it expects a CSS valid color value. If there is a semantic variant also specified, the
component will ignore it.
You may also use `textColor` to make sure the tag's text has good contrasts with its background.

```jsx
import { TagGroup } from '../TagGroup'
;<TagGroup alignX="end">
  <Tag backgroundColor="#d931cc" textColor="#ffe6e6" value="Example" />
  <Tag backgroundColor="#afffe5" textColor="#111111" value="Example" />
  <Tag backgroundColor="#80931c" textColor="#ffe6e6" value="Example" />
</TagGroup>
```

## Tag Group

Atalaya also exports a tag container component called `<TagGroup />`. The benefits of wrapping your tags in a tag group
are:

- Spacing and wrapping automatically configured
- Ability to set all tags to the same size using the `sm` prop
- Set the max number of tags to be displayed. `0` its the default value, it means there is no max.

```jsx
import { TagGroup } from '../TagGroup'
;<TagGroup max={3}>
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
</TagGroup>
```

```jsx
import { TagGroup } from '../TagGroup'
;<TagGroup max={18}>
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
</TagGroup>
```

**TagGroup's** `alignX` can be used to tweak the alignment.

```jsx
import { TagGroup } from '../TagGroup'
;<TagGroup alignX="end" max={18}>
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
  <Tag value="Example" />
</TagGroup>
```

### With tooltip

```jsx
import { TagGroup } from '../TagGroup'
;<TagGroup alignX="end">
  <Tag tooltip="Tag #1" value="Example" />
  <Tag tooltip="Tag #2" value="Example" />
  <Tag tooltip="Tag #3" value="Example" />
</TagGroup>
```
