import classNamesParser from 'classnames'
import { SemanticVariant } from '../../../constants'

export interface TagClassProps {
  /** If it should show the large version of the tag */
  lg?: boolean

  /** Semantic meaning of the tag */
  variant?: SemanticVariant
  subtle?: boolean
  subtleBackground?: 'surface' | 'surface-strong' | 'surface-subtle' | string
}

export const makeTagClasses = ({
  lg,
  variant = 'neutral',
  subtle,
  subtleBackground,
}: TagClassProps): string => {
  return classNamesParser(
    'capitalize inline-flex text-sm rounded text-color-semantic text-center',
    {
      'bg-main': !subtle && variant === 'main',
      'bg-neutral': !subtle && (!variant || variant === 'neutral'),
      'bg-positive': !subtle && variant === 'positive',
      'bg-caution': !subtle && variant === 'caution',
      'bg-critical': !subtle && variant === 'critical',
      'bg-info': !subtle && variant === 'info',
      'items-center gap-x-xxs': subtle,
      'bg-surface':
        subtle && (!subtleBackground || subtleBackground === 'surface'),
      'bg-surface-strong': subtle && subtleBackground === 'surface-strong',
      'bg-surface-subtle': subtle && subtleBackground === 'surface-subtle',
      'font-medium px-xxs': !lg,
      'font-bold p-xxs': lg,
    },
  )
}
