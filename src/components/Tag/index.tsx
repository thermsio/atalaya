import React, { useMemo } from 'react'
import { getColorContrast } from '../../utils/getColorContrast'
import { makeTagClasses, TagClassProps } from './utils'
import { Tooltip } from '../Tooltip'
import { ColorSquare } from '../ColorSquare'
import classNames from 'classnames'

export interface TagProps extends TagClassProps {
  backgroundColor?: string
  subtle?: boolean
  textColor?: string
  tooltip?: React.ReactNode
  value: React.ReactNode
}

const Tag = ({
  backgroundColor,
  lg,
  subtle,
  subtleBackground,
  textColor,
  tooltip,
  value,
  variant,
}: TagProps): React.ReactElement => {
  const classes = useMemo(
    () =>
      makeTagClasses({
        lg,
        subtle,
        subtleBackground,
        variant: backgroundColor ? undefined : variant,
      }),
    [backgroundColor, lg, subtle, subtleBackground, variant],
  )

  let color
  if (textColor) {
    color = textColor
  } else if (backgroundColor) {
    color = getColorContrast(backgroundColor)
  }

  const TagBody = useMemo(() => {
    if (subtle) {
      return (
        <div
          className={classNames(classes)}
          style={{
            color: textColor,
            backgroundColor:
              subtleBackground &&
              ['surface', 'surface-strong', 'surface-subtle'].includes(
                subtleBackground,
              )
                ? undefined
                : subtleBackground,
          }}
        >
          {Boolean(backgroundColor || variant) && (
            <ColorSquare variant={variant} color={backgroundColor} />
          )}
          {value}
        </div>
      )
    } else {
      return (
        <div
          className={classes}
          style={{
            backgroundColor: backgroundColor,
            color,
          }}
        >
          {value}
        </div>
      )
    }
  }, [
    backgroundColor,
    classes,
    color,
    subtle,
    subtleBackground,
    value,
    variant,
  ])

  if (tooltip) return <Tooltip tooltip={tooltip}>{TagBody}</Tooltip>

  return TagBody
}

export { Tag }
