### Sizes

When inside a ButtonGroup, Buttons size is overwritten by it.

```jsx
import {ButtonGroup} from './index';
import { Button } from '../Button';

<div className="space-y-base">
  <ButtonGroup size="sm">
    <Button variant="main">Main</Button>
    <Button variant="neutral">Neutral</Button>
    <Button variant="info">Info</Button>
    <Button variant="positive">Positive</Button>
    <Button variant="caution">Caution</Button>
    <Button variant="critical">Critical</Button>
  </ButtonGroup>

  <ButtonGroup>
    <Button variant="main">Main</Button>
    <Button variant="neutral">Neutral</Button>
    <Button variant="info">Info</Button>
    <Button variant="positive">Positive</Button>
    <Button variant="caution">Caution</Button>
    <Button variant="critical">Critical</Button>
  </ButtonGroup>

  <ButtonGroup size="lg">
    <Button variant="main">Main</Button>
    <Button variant="neutral">Neutral</Button>
    <Button variant="info">Info</Button>
    <Button variant="positive">Positive</Button>
    <Button variant="caution">Caution</Button>
    <Button variant="critical">Critical</Button>
  </ButtonGroup>
</div>
```

### Default Variant

Variant can be set on ButtonGroup to provide a default variant for all buttons in the group. If a Button has a
particular variant set it will overwrite the group's default.

```jsx
import {ButtonGroup} from './index';
import { Button } from '../Button';

<ButtonGroup variant="positive">
  <Button>Default</Button>
  <Button>Default</Button>
  <Button>Default</Button>
  <Button>Default</Button>
  <Button variant="caution">Caution</Button>
  <Button>Default</Button>
</ButtonGroup>
```

### Full Width

Space is evenly distributed to each button.

```jsx
import {ButtonGroup} from './index';
import { Button } from '../Button';

<ButtonGroup fullWidth>
  <Button variant="main">Main</Button>
  <Button variant="neutral">Neutral</Button>
  <Button variant="info">Info</Button>
  <Button variant="positive">Positive</Button>
  <Button variant="caution">Caution</Button>
  <Button variant="critical">Critical</Button>
</ButtonGroup>
```

### Outline

If a Button has a different type, it will be overwritten by ButtonGroup

```jsx
import {ButtonGroup} from './index';
import { Button } from '../Button';

<ButtonGroup outline>
  <Button subtle variant="main">Main</Button>
  <Button variant="neutral">Neutral</Button>
  <Button subtle variant="info">Info</Button>
  <Button variant="positive">Positive</Button>
  <Button subtle variant="caution">Caution</Button>
  <Button variant="critical">Critical</Button>
</ButtonGroup>
```

### Subtle

If a Button has a different type, it will be overwritten by ButtonGroup.

```jsx
import {ButtonGroup} from './index';
import { Button } from '../Button';

<ButtonGroup subtle>
  <Button outline variant="main">Main</Button>
  <Button variant="neutral">Neutral</Button>
  <Button outline variant="info">Info</Button>
  <Button variant="positive">Positive</Button>
  <Button outline variant="caution">Caution</Button>
  <Button variant="critical">Critical</Button>
</ButtonGroup>
```

### Disabled

Disabled state must be passed to each Button separately.

```jsx
import {ButtonGroup} from './index';
import { Button } from '../Button';

<ButtonGroup>
  <Button variant="main">Main</Button>
  <Button disabled variant="neutral">Neutral</Button>
  <Button disabled variant="info">Info</Button>
  <Button disabled variant="positive">Positive</Button>
  <Button disabled variant="caution">Caution</Button>
  <Button variant="critical">Critical</Button>
</ButtonGroup>
```
