import React from 'react'
import { ButtonGroup } from './index'
import { Button } from '../Button'

const nestedButton = (
  <ButtonGroup>
    <Button>First</Button>

    <div>
      <div>
        <Button>Double Nested Button</Button>
      </div>
    </div>

    <Button>Last</Button>
  </ButtonGroup>
)

export default { nestedButton }
