import React from 'react'
import { ButtonClassProps } from '../Button/utils'
import './ButtonGroup.css'

import classNameParser from 'classnames'

export interface ButtonGroupProps extends ButtonClassProps {
  children?: React.ReactNode
  disabled?: boolean
}

const ButtonGroup = ({
  children,
  className,
  fullWidth,
  outline,
  size,
  subtle,
  variant,
}: ButtonGroupProps): React.ReactElement => {
  return (
    <div
      className={classNameParser(
        {
          'btn-group-outline': outline,
        },
        'btn-group',
        className,
      )}
    >
      {React.Children.map(children, (child) => {
        if (React.isValidElement(child))
          return React.cloneElement(child, {
            ...child.props,
            fullWidth,
            outline: outline || child.props.outline,
            size,
            subtle: subtle || child.props.subtle,
            variant: child.props.variant || variant,
          })
      })}
    </div>
  )
}

export { ButtonGroup }
