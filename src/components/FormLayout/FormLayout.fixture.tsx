import React from 'react'
import { FormLayout } from './index'
import { Text } from '../FormControls/Text/Text'

const controls = (
  <FormLayout heading="Form Layout Title">
    <Text
      name="i"
      label="Label"
      hint="Hint"
      subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
      value="Text's value"
    />

    <FormLayout.Controls
      onCancel={() => console.log('cancel')}
      onSave={() => console.log('save')}
    />
  </FormLayout>
)

const withFragments = (
  <FormLayout dividers="lines" heading="Form Layout Title">
    <Text
      name="a"
      label="Label 1"
      hint="Hint"
      subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
      value="Text's value"
    />

    <>
      <Text
        name="b"
        label="Label 2"
        hint="Hint"
        subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
        value="Text's value"
      />

      <Text
        name="c"
        label="Label 3"
        hint="Hint"
        subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
        value="Text's value"
      />

      <>
        <Text
          name="d"
          label="Label 4"
          hint="Hint"
          subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
          value="Text's value"
        />

        <Text
          name="e"
          label="Label"
          hint="Hint"
          subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
          value="Text's value"
        />
      </>

      <Text
        name="f"
        label="Label"
        hint="Hint"
        subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
        value="Text's value"
      />

      <Text
        name="g"
        label="Label"
        hint="Hint"
        subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
        value="Text's value"
      />
    </>

    <Text
      name="h"
      label="Label"
      hint="Hint"
      subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
      value="Text's value"
    />

    <FormLayout.Controls
      onCancel={() => console.log('cancel')}
      onSave={() => console.log('save')}
    />
  </FormLayout>
)

const horizontalAndHintBug = (
  <div>
    <FormLayout heading="Form Layout Title" horizontal>
      <FormLayout.Section heading="Random Title" horizontal>
        <Text name="i" label="Label 1" value="Text's value" />

        <Text name="i" label="Label 2" hint="Required" value="Text's value" />

        <Text
          name="i"
          label="Label 3"
          hint="Hint Required "
          subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque nec nam aliquam sem et tortor."
          value="Text's value"
        />

        <Text
          name="i"
          label="Label 4"
          hint="Hint Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque nec nam aliquam sem et tortor."
          value="Text's value"
        />
      </FormLayout.Section>

      <FormLayout.Controls
        onCancel={() => console.log('cancel')}
        onSave={() => console.log('save')}
      />
    </FormLayout>
  </div>
)

export default {
  controls,
  withFragments,
  horizontalAndHintBug,
}
