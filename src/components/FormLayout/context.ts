import { createContext } from 'react'
import { Spacing } from '../../layout/types'

interface FormLayoutContextProps {
  dividers?: string
  isHorizontal?: boolean
  space?: Spacing
  width?: number
}

const defaultContext: FormLayoutContextProps = {
  dividers: undefined,
  isHorizontal: undefined,
  space: undefined,
  width: undefined,
}

interface FormControlLabelBehaviorContextProps {
  labelBehavior?: 'block' | 'floating' | 'floating-on-value'
}

const defaultFormControlLabelBehaviorContext: FormControlLabelBehaviorContextProps =
  { labelBehavior: undefined }

export const FormLayoutContext = createContext(defaultContext)
export const FormControlLabelBehaviorContext = createContext(
  defaultFormControlLabelBehaviorContext,
)
