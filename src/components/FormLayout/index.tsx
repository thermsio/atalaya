import React, { useLayoutEffect, useState } from 'react'
import classNameParser from 'classnames'
import { useResizeDetector } from 'react-resize-detector'
import { FormControls } from './components/FormControls'
import { FormDivider } from './components/FormDivider'
import { FormSection } from './components/FormSection'
import { Loading } from '../Loading'
import { FormControlLabelBehaviorContext, FormLayoutContext } from './context'
import { Spacing } from '../../layout/types'
import classNames from 'classnames'
import { FormElementLayoutWrapper } from './components/FormElementLayoutWrapper'
import { FormControlWrapper } from './components/FormControlWrapper'

export interface FormLayoutProps {
  children?: React.ReactNode | React.ReactNode[]
  /** Style of divider, if 'none' is selected there will be no spacing between elements. */
  dividers?: 'none' | 'lines' | 'space'
  /** Optionally force horizontal FormLayout */
  horizontal?: boolean
  /** Block: Label on top of input, Floating: label will not create a new line, will look like a tab title, FloatingOnValue: Same as floating, but it will only appear when a value is present */
  labelBehavior?: 'block' | 'floating' | 'floating-on-value'
  /** Will display a loading overlay if true. */
  loading?: boolean
  loadingMessage?: React.ReactNode
  /** Heading of the form. */
  heading?: React.ReactNode
  /** If container is relative, if undefined it will only be set to relative while it's loading. */
  relative?: boolean
  /** Amount of spacing between elements. */
  space?: Spacing
  /** Will be displayed below heading. */
  subTitle?: React.ReactNode
}

const FormLayout = ({
  children,
  dividers = 'space',
  heading,
  horizontal,
  labelBehavior,
  loading,
  loadingMessage,
  relative,
  space = 'sm',
  subTitle,
}: FormLayoutProps): React.ReactElement => {
  const { width, ref } = useResizeDetector({
    handleHeight: false,
    refreshMode: 'throttle',
    refreshRate: 150,
  })
  const [isHorizontal, setIsHorizontal] = useState(horizontal)
  const _dividers = dividers !== 'none' ? dividers : undefined

  useLayoutEffect(() => {
    if (typeof horizontal !== 'undefined') {
      setIsHorizontal(horizontal)
    } else if (width && width < 350) {
      setIsHorizontal(false)
    } else {
      setIsHorizontal(true)
    }
  }, [horizontal, width])

  return (
    <FormLayoutContext.Provider
      value={{ dividers: _dividers, isHorizontal, space, width }}
    >
      <FormControlLabelBehaviorContext.Provider value={{ labelBehavior }}>
        <div
          className={classNameParser({
            relative: relative === undefined ? loading : relative,
            'divide-y divide-border': dividers === 'lines',
          })}
          ref={ref}
        >
          {loading && (
            <Loading
              className="text-color-main"
              overlay
              size="xxl"
              message={loadingMessage}
            />
          )}

          {(!!heading || !!subTitle) && (
            <div
              className={classNames({
                'pb-xxs': space === 'xxs',
                'pb-xs': space === 'xs',
                'pb-sm': space === 'sm',
                'pb-base': space === 'base',
                'pb-lg': space === 'lg',
                'pb-xl': space === 'xl',
                'pb-xxl': space === 'xxl',
              })}
            >
              <div className="text-lg font-bold">{heading}</div>
              <div className="text-color-subtle">{subTitle}</div>
            </div>
          )}

          {children}
        </div>
      </FormControlLabelBehaviorContext.Provider>
    </FormLayoutContext.Provider>
  )
}

FormLayout.Controls = FormControls
FormLayout.ElementLayoutWrapper = FormElementLayoutWrapper
FormLayout.ControlWrapper = FormControlWrapper
FormLayout.Divider = FormDivider
FormLayout.Section = FormSection

export { FormLayout }
