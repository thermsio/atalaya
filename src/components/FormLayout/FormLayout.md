```jsx
import { Text } from '../FormControls/Text/Text'
;<FormLayout
  heading="Form Layout Title"
  subTitle="Id interdum velit laoreet id. Dignissim enim sit amet."
>
  <Text
    name="b"
    label="Label"
    hint="Hint"
    error="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  />
</FormLayout>
```

### With Sections

```jsx
import { Text } from '../FormControls/Text/Text'
;<FormLayout
  heading="Form Layout Title"
  subTitle="Id interdum velit laoreet id. Dignissim enim sit amet."
>
  <FormLayout.Section
    heading="Section 1"
    subTitle="Id interdum velit laoreet id. Dignissim enim sit amet."
  >
    <Text
      name="e"
      label="Label"
      hint="Hint"
      subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
    />

    <Text
      name="f"
      label="Label"
      hint="Hint"
      subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
    />

    <Text
      name="g"
      label="Label"
      hint="Hint"
      subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
    />
  </FormLayout.Section>

  <FormLayout.Section
    heading="Section 2"
    subTitle="Id interdum velit laoreet id. Dignissim enim sit amet."
  >
    <Text
      name="b"
      label="Label"
      hint="Hint"
      subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
    />

    <Text
      name="c"
      label="Label"
      hint="Hint"
      subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
    />

    <Text
      name="d"
      label="Label"
      hint="Hint"
      subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
    />
  </FormLayout.Section>
</FormLayout>
```

### With Controls

```jsx
import { Text } from '../FormControls/Text/Text'
;<FormLayout
  heading="Form Layout Title"
  subTitle="Id interdum velit laoreet id. Dignissim enim sit amet."
>
  <Text
    name="h"
    label="Label"
    hint="Hint"
    subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  />

  <FormLayout.Controls
    onCancel={() => 'cancel'}
    onDelete={() => 'delete'}
    onSave={() => 'save'}
  />
</FormLayout>
```

### With Controls and extras

```jsx
import { Text } from '../FormControls/Text/Text'
import { Button } from '../Button'
;<FormLayout heading="Form Layout Title">
  <Text
    name="i"
    label="Label"
    hint="Hint"
    subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  />

  <FormLayout.Controls
    extraEnd={
      <Button subtle variant="caution">
        Extra end
      </Button>
    }
    extraStart={<Button variant="positive">Extra start</Button>}
    onCancel={() => 'cancel'}
    onDelete={() => 'delete'}
    onSave={() => 'save'}
  />
</FormLayout>
```

### Default Dividers

```jsx
import { Text } from '../FormControls/Text/Text'
;<FormLayout
  heading="Form Layout Title"
  subTitle="Id interdum velit laoreet id. Dignissim enim sit amet."
>
  <Text
    name="i"
    label="Label"
    hint="Hint"
    subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  />

  <Text
    name="j"
    label="Label"
    hint="Hint"
    subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  />

  <Text
    name="k"
    label="Label"
    hint="Hint"
    subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  />

  <FormLayout.Controls
    onCancel={() => 'cancel'}
    onDelete={() => 'delete'}
    onSave={() => 'save'}
  />
</FormLayout>
```

### Dividers hidden

```jsx
import { Text } from '../FormControls/Text/Text'
;<FormLayout
  dividers="space"
  heading="Form Layout Title"
  subTitle="Id interdum velit laoreet id. Dignissim enim sit amet."
>
  <Text
    name="l"
    label="Label"
    hint="Hint"
    subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  />

  <Text
    name="m"
    label="Label"
    hint="Hint"
    subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  />

  <Text
    name="n"
    label="Label"
    hint="Hint"
    subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  />

  <FormLayout.Controls
    onCancel={() => 'cancel'}
    onDelete={() => 'delete'}
    onSave={() => 'save'}
  />
</FormLayout>
```

### No dividers

```jsx
import { Text } from '../FormControls/Text/Text'
;<FormLayout
  dividers="none"
  heading="Form Layout Title"
  subTitle="Id interdum velit laoreet id. Dignissim enim sit amet."
>
  <Text
    name="o"
    label="Label"
    hint="Hint"
    subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  />

  <Text
    name="p"
    label="Label"
    hint="Hint"
    subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  />

  <Text
    name="q"
    label="Label"
    hint="Hint"
    subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  />

  <FormLayout.Controls
    onCancel={() => 'cancel'}
    onDelete={() => 'delete'}
    onSave={() => 'save'}
  />
</FormLayout>
```

### Loading

```jsx
import { Text } from '../FormControls/Text/Text'
;<FormLayout
  heading="Form Layout Title"
  loading
  subTitle="Id interdum velit laoreet id. Dignissim enim sit amet."
>
  <Text
    name="r"
    label="Label"
    hint="Hint"
    subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  />

  <Text
    name="s"
    label="Label"
    hint="Hint"
    subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  />

  <Text
    name="t"
    label="Label"
    hint="Hint"
    subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  />

  <FormLayout.Controls
    onCancel={() => 'cancel'}
    onDelete={() => 'delete'}
    onSave={() => 'save'}
  />
</FormLayout>
```

# Customizing

If you want to use your own custom form controls you can use [FormLayout.ControlWrapper](#/ReactComponents/FormControlWrapper) make it fit naturally with the rest of controls.
If you however want to add an element that's not a Form Control, you may ue [FormLayout.ControlLayoutWrapper](#/ReactComponents/FormElementLayoutWrapper).
