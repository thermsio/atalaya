import React, { useState } from 'react'
import { FormLayout } from '../index'
import { Text } from '../../FormControls/Text/Text'

const base = () => {
  const [textValue, setTextValue] = useState('')
  return (
    <FormLayout
      heading="This is the Form's Title"
      horizontal
      subTitle="It has some information we thought you might enjoy"
    >
      <FormLayout.Section
        heading="Section Title"
        horizontal
        subTitle="And this is the section subtitle, aimed to contribute additional information."
      >
        <Text
          label="Text Field"
          placeholder="Lorem Ipsum"
          subText="Only latin characters allowed."
          value={textValue}
          onChangeValue={setTextValue}
        />
        <Text
          label="Lorem ipsum dolor sit amet"
          hint="Required"
          placeholder="Lorem Ipsum"
          subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
          value={textValue}
          onChangeValue={setTextValue}
        />
        <Text
          label="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
          hint="Required"
          placeholder="Lorem Ipsum"
          subText="Only latin characters allowed."
          value={textValue}
          onChangeValue={setTextValue}
        />
      </FormLayout.Section>

      <FormLayout.Section
        heading="Section Title"
        subTitle="And this is the section subtitle, aimed to contribute additional information."
      >
        <Text
          label="Text Field"
          placeholder="Lorem Ipsum"
          subText="Only latin characters allowed."
          value={textValue}
          onChangeValue={setTextValue}
        />
        <Text
          label="Text Field"
          hint="Required"
          placeholder="Lorem Ipsum"
          subText="Only latin characters allowed."
          value={textValue}
          onChangeValue={setTextValue}
        />
        <Text
          label="Text Field"
          hint="Required"
          placeholder="Lorem Ipsum"
          subText="Only latin characters allowed."
          value={textValue}
          onChangeValue={setTextValue}
        />
      </FormLayout.Section>
    </FormLayout>
  )
}

export default base
