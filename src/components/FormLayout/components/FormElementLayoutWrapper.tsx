import React, { useContext } from 'react'
import classNames from 'classnames'
import { FormLayoutContext } from '../context'

export interface FormElementLayoutWrapperProps {
  children: React.ReactNode
}

const FormElementLayoutWrapper = ({
  children,
}: FormElementLayoutWrapperProps) => {
  const { space } = useContext(FormLayoutContext)

  if (!children || !React.Children.count(children)) return null

  return (
    <div
      className={classNames({
        'py-xxs': space === 'xxs',
        'py-xs': space === 'xs',
        'py-sm': space === 'sm',
        'py-base': space === 'base',
        'py-lg': space === 'lg',
        'py-xl': space === 'xl',
        'py-xxl': space === 'xxl',
      })}
    >
      {children}
    </div>
  )
}

export { FormElementLayoutWrapper }
