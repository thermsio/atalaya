import React, { useContext, useEffect, useLayoutEffect, useState } from 'react'
import classNameParser from 'classnames'
import { Button } from '../../Button'
import { Icon } from '../../Icon'
import { Inline } from '../../../layout/Inline'
import { FormLayoutContext } from '../context'
import { deprecate } from '../../../utils/deprecate'

export interface FormControlsProps {
  /** Text that will be shown in cancel control. */
  cancelLabel?: React.ReactNode
  /** Disable the action buttons, does not affect the extra custom components */
  disabled?: boolean
  /** Extra component placed at the end of the controls. */
  extraEnd?: React.ReactNode
  /** Extra component placed at the start of the controls. */
  extraStart?: React.ReactNode
  /** If defined, it will override current form flow for this component */
  isHorizontal?: boolean
  /** *Deprecated, use submitting* */
  isSaving?: boolean | string
  /** Will disabled the controls and show loading indicators where applicable. If it's a string, it will be shown next to the loading indicator. */
  submitting?: boolean | string
  /** Callback fired when cancel button is pressed. If undefined cancel button won't be shown. */
  onCancel?: () => void
  /** Callback fired when delete button is pressed. If undefined the control won't be shown. */
  onDelete?: () => void
  /** *Deprecated, use onSubmit* Callback fired when submit button is pressed. If undefined the control won't be shown. */
  onSave?: () => void
  /** Callback fired when submit button is pressed. If undefined the control won't be shown. */
  onSubmit?: () => void
  /** Text that will be shown in cancel control. */
  submitLabel?: React.ReactNode
}

const FormControls = ({
  cancelLabel,
  disabled,
  extraEnd,
  extraStart,
  isHorizontal,
  isSaving,
  submitting,
  onCancel,
  onDelete,
  onSave,
  onSubmit,
  submitLabel,
}: FormControlsProps): React.ReactElement => {
  const { space, width } = useContext(FormLayoutContext)
  const _onSubmit = onSubmit || onSave
  const _submitting = submitting || isSaving
  const [confirmDelete, setConfirmDelete] = useState(false)

  let horizontalWidthLimit = 250
  if (extraEnd) horizontalWidthLimit = horizontalWidthLimit + 75
  if (extraStart) horizontalWidthLimit = horizontalWidthLimit + 75
  const _isHorizontal = isHorizontal ?? (width || 0) > horizontalWidthLimit

  useLayoutEffect(() => {
    if (onSave) {
      deprecate(
        'FormLayout.Controls',
        'Atalaya FormLayout.Controls: onSave is deprecated and will no longer be available on future versions, use onSubmit instead.',
      )
    }

    if (isSaving === true || isSaving === false) {
      deprecate(
        'FormLayout.Controls',
        'Atalaya FormLayout.Controls: isSaving is deprecated and will no longer be available on future versions, use submitting instead.',
      )
    }
  }, [])

  useEffect(() => {
    if (confirmDelete) {
      const t = setTimeout(() => {
        setConfirmDelete(false)
      }, 3000)

      return () => clearTimeout(t)
    }
  }, [confirmDelete])

  return (
    <div
      className={classNameParser(
        {
          'flex-col items-end space-y-xs': !_isHorizontal,
          'flex-row-reverse justify-between space-x-xs space-x-reverse':
            _isHorizontal,
          'pt-xxs': space === 'xxs',
          'pt-xs': space === 'xs',
          'pt-sm': space === 'sm',
          'pt-base': space === 'base',
          'pt-lg': space === 'lg',
          'pt-xl': space === 'xl',
          'pt-xxl': space === 'xxl',
        },
        'flex',
      )}
    >
      <div
        className={classNameParser(
          {
            'w-full flex-col space-y-xs': !_isHorizontal,
            'flex-row-reverse items-end space-x-xs space-x-reverse':
              _isHorizontal,
          },
          'flex',
        )}
      >
        {extraStart}

        {!!_onSubmit && (
          <Button
            disabled={disabled || !!_submitting}
            fullWidth={!_isHorizontal}
            loading={_submitting}
            onClick={_onSubmit}
            variant="main"
            type="submit"
          >
            {submitLabel || 'Submit'}
          </Button>
        )}

        {!!onCancel && (
          <Button
            disabled={!!_submitting}
            fullWidth={!_isHorizontal}
            onClick={onCancel}
          >
            {cancelLabel || 'Cancel'}
          </Button>
        )}
      </div>

      {(!!onDelete || !!extraEnd) && (
        <Inline space="xs">
          {!!onDelete && (
            <Button
              disabled={disabled || !!_submitting}
              onClick={() =>
                confirmDelete ? onDelete() : setConfirmDelete(true)
              }
              subtle
              variant="critical"
            >
              {confirmDelete ? (
                <span>Confirm</span>
              ) : (
                <Icon>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="currentColor"
                    viewBox="0 0 192 192"
                  >
                    <path d="M67.493 154.018h57.074c8.196 0 12.958-4.339 13.319-12.476l3.857-87.207h8.378c2.953 0 5.303-2.29 5.303-5.304 0-2.953-2.35-5.243-5.303-5.243h-26.217v-9.04c0-9.583-6.208-15.308-16.513-15.308H84.489c-10.306 0-16.453 5.725-16.453 15.308v9.04H41.879c-2.892 0-5.303 2.29-5.303 5.243 0 3.014 2.41 5.304 5.303 5.304h8.498l3.797 87.207c.362 8.137 5.123 12.476 13.32 12.476ZM80.451 35.29c0-2.712 1.868-4.52 4.882-4.52h21.274c3.074 0 4.942 1.808 4.942 4.52v8.498H80.451V35.29Zm-3.737 100.406c-2.41 0-4.098-1.567-4.158-3.977l-1.808-64.186c-.06-2.35 1.567-3.977 4.098-3.977 2.41 0 4.098 1.567 4.158 3.977l1.808 64.186c.06 2.35-1.566 3.977-4.098 3.977Zm19.286 0c-2.471 0-4.219-1.627-4.219-3.977V67.533c0-2.35 1.748-3.977 4.219-3.977 2.531 0 4.219 1.627 4.219 3.977v64.186c0 2.35-1.688 3.977-4.219 3.977Zm19.286 0c-2.532 0-4.159-1.627-4.098-3.977l1.808-64.186c.06-2.41 1.747-3.917 4.158-3.917 2.531 0 4.159 1.567 4.098 3.978l-1.808 64.125c-.06 2.41-1.748 3.977-4.158 3.977Z" />
                  </svg>
                </Icon>
              )}
            </Button>
          )}

          {extraEnd}
        </Inline>
      )}
    </div>
  )
}

export { FormControls }
