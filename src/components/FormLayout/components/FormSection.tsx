import React, { useContext, useLayoutEffect, useState } from 'react'
import classNameParser from 'classnames'
import { Constants } from '../../../constants'
import { FormLayoutContext } from '../context'
import { Spacing } from '../../../layout/types'

export interface FormSectionProps {
  children?: React.ReactNode
  /** Type of divider between input fields. Overrides the prop from FormLayout */
  dividers?: 'none' | 'lines' | 'space'
  heading?: React.ReactNode
  /** Places the heading horizontally on the same line as the content. Will fall back to vertical layout when there is not enough space */
  horizontal?: boolean
  space?: Spacing
  subTitle?: React.ReactNode
}

const FormSection = ({
  children,
  dividers,
  heading,
  horizontal = true,
  space,
  subTitle,
}: FormSectionProps): React.ReactElement => {
  const context = useContext(FormLayoutContext)
  const { width } = context
  const [isHorizontal, setIsHorizontal] = useState<boolean>(horizontal)
  const _dividers = dividers || context.dividers || 'space'
  const _space = space || context.space || 'sm'

  useLayoutEffect(() => {
    setIsHorizontal(
      horizontal && !!width && width >= Constants.Breakpoint.Md.width,
    )
  }, [horizontal, width])

  return (
    <FormLayoutContext.Provider
      value={{ ...context, dividers: _dividers, isHorizontal, space: _space }}
    >
      <div
        className={classNameParser({
          'grid grid-cols-3 gap-base': isHorizontal,
          'divide-y divide-border': _dividers === 'lines' && !isHorizontal,
        })}
      >
        <div
          className={classNameParser(
            {
              'py-xxs': _space === 'xxs',
              'py-xs': _space === 'xs',
              'py-sm': _space === 'sm',
              'py-base': _space === 'base',
              'py-lg': _space === 'lg',
              'py-xl': _space === 'xl',
              'py-xxl': _space === 'xxl',
            },
            'col-span-1',
          )}
        >
          <div className="font-bold">{heading}</div>
          <div className="text-sm text-color-subtle">{subTitle}</div>
        </div>

        <div
          className={classNameParser(
            { 'divide-y divide-border': _dividers === 'lines' },
            'col-span-2',
          )}
        >
          {children}
        </div>
      </div>
    </FormLayoutContext.Provider>
  )
}

export { FormSection }
