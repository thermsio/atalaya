import React, { useState } from 'react'

export interface FormControlDisplayEditProps {
  initialEditing?: boolean
  renderDisplay: ({ onEdit }: { onEdit: () => void }) => React.ReactElement
  renderEditing: ({
    onComplete,
  }: {
    onComplete: () => void
  }) => React.ReactElement
}

const FormControlDisplayEdit = ({
  initialEditing,
  renderDisplay,
  renderEditing,
}: FormControlDisplayEditProps) => {
  const [editing, setEditing] = useState(initialEditing)

  if (editing)
    return renderEditing({
      onComplete: () => setEditing(false),
    })

  return renderDisplay({ onEdit: () => setEditing(true) })
}

export { FormControlDisplayEdit }
