import React, { useState } from 'react'
import { FormLayout } from '../..'
import { FormControlDisplayEdit } from './index'
import { Text } from '../../../FormControls/Text/Text'
import { ReadOnlyFormControl } from './ReadOnlyFormControl'

const base = () => {
  const [value, setValue] = useState('')

  return (
    <FormLayout>
      <FormControlDisplayEdit
        initialEditing={false}
        renderDisplay={({ onEdit }) => (
          <ReadOnlyFormControl
            label="Text Label"
            subText="Text Component Subtext"
            onEdit={onEdit}
          >
            {value}
          </ReadOnlyFormControl>
        )}
        renderEditing={({ onComplete }) => (
          <Text
            label="Text Label"
            subText="Text Component subtext"
            onBlur={onComplete}
            value={value}
            onChangeValue={setValue}
          />
        )}
      />
    </FormLayout>
  )
}

export default { base }
