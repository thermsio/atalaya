import classNames from 'classnames'
import React from 'react'
import { Button } from '../../../Button'
import { Icon } from '../../../Icon'
import {
  FormControlWrapper,
  FormControlWrapperProps,
} from '../FormControlWrapper'

export interface ReadOnlyFormControlProps extends FormControlWrapperProps {
  children: React.ReactNode
  onEdit?: () => void
}

const ReadOnlyFormControl = ({
  children,
  onEdit,
  ...formControlWrapperProps
}: ReadOnlyFormControlProps) => {
  return (
    <FormControlWrapper {...formControlWrapperProps}>
      <div className={classNames('flex items-center')}>
        {children}

        {!!onEdit && (
          <Button className="ml-auto mr-sm" onClick={onEdit} subtle>
            <Icon size="xs">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="currentColor"
              >
                <path d="M0 0h24v24H0V0z" fill="none" />
                <path d="M3 17.46v3.04c0 .28.22.5.5.5h3.04c.13 0 .26-.05.35-.15L17.81 9.94l-3.75-3.75L3.15 17.1c-.1.1-.15.22-.15.36zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z" />
              </svg>
            </Icon>
          </Button>
        )}
      </div>
    </FormControlWrapper>
  )
}

export { ReadOnlyFormControl }
