import React from 'react'
import classNameParser from 'classnames'
import { Spacing } from '../../../layout/types'

export interface FormDividerProps {
  className?: string
  space?: Spacing
  withBorder?: boolean
}

const FormDivider = ({
  className,
  space,
  withBorder,
}: FormDividerProps): React.ReactElement => {
  return (
    <div
      className={classNameParser(
        {
          'bg-border': withBorder,
          'my-xxs': space === 'xxs',
          'my-xs': space === 'xs',
          'my-sm': space === 'sm',
          'my-base': space === 'base',
          'my-lg': space === 'lg',
          'my-xl': space === 'xl',
          'my-xxl': space === 'xxl',
        },
        'h-px w-full',
        className,
      )}
    />
  )
}

export { FormDivider }
