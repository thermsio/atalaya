import React, { useContext, useMemo } from 'react'
import Error from './components/Error'
import HintText from './components/HintText'
import Label from './components/Label'
import SubText from './components/SubText'
import {
  FormControlLabelBehaviorContext,
  FormLayoutContext,
} from '../../context'
import classNames from 'classnames'

export interface FormControlWrapperPublicProps {
  /** Error message */
  error?: React.ReactNode
  disabled?: boolean
  hasValue?: boolean
  /** Short helpful text */
  hint?: React.ReactNode
  /** Text the label should display */
  label?: React.ReactNode
  labelBehavior?: 'block' | 'floating' | 'floating-on-value'
  maxWidth?: number
  minWidth?: number
  showErrorMessage?: boolean
  /** Help description, can be more extensive than hint */
  subText?: React.ReactNode
}

export interface FormControlWrapperProps extends FormControlWrapperPublicProps {
  /** Input field which will be displayed */
  children: React.ReactNode
  /** Identifier of the input element our component it's labeling */
  inputName?: string
}

const FormControlWrapper = ({
  children,
  error,
  disabled,
  hasValue,
  hint,
  inputName,
  label,
  labelBehavior,
  maxWidth,
  minWidth,
  showErrorMessage = true,
  subText,
}: FormControlWrapperProps): React.ReactElement => {
  const formLayoutContext = useContext(FormLayoutContext)
  const { isHorizontal, space } = formLayoutContext
  const { labelBehavior: contextLabelBehavior } = useContext(
    FormControlLabelBehaviorContext,
  )

  const _labelBehavior = contextLabelBehavior || labelBehavior || 'block'

  const allowFloatingLabel =
    !formLayoutContext &&
    (_labelBehavior === 'floating' ||
      (_labelBehavior === 'floating-on-value' && hasValue))

  const showHorizontal = useMemo(() => {
    // Do not show horizontal if there is no FormLayoutContext parent
    if (!formLayoutContext) return false

    return Boolean(isHorizontal && (label || hint))
  }, [hint, isHorizontal, label])

  return (
    <div
      className={classNames({
        'grid grid-cols-3 items-start gap-base': showHorizontal,
        'space-y-xs': !showHorizontal,
        'py-xxs': space === 'xxs',
        'py-xs': space === 'xs',
        'py-sm': space === 'sm',
        'py-base': space === 'base',
        'py-lg': space === 'lg',
        'py-xl': space === 'xl',
        'py-xxl': space === 'xxl',
      })}
      style={{ maxWidth, minWidth }}
    >
      {!allowFloatingLabel && Boolean(label || hint) && (
        <div
          className={
            showHorizontal
              ? 'col-span-1 flex flex-col'
              : 'flex flex-wrap-reverse items-baseline justify-between'
          }
        >
          <Label inputName={inputName}>{label}</Label>
          <HintText>{hint}</HintText>
        </div>
      )}

      <div className="col-span-2 space-y-xs relative">
        {children}

        {!!label && allowFloatingLabel && (
          <div
            className={classNames(
              {
                'bg-input-background-disabled text-color-neutral-faded select-none cursor-not-allowed':
                  disabled,
                ' bg-input-background text-color-strong': !disabled,
              },
              'rounded px-sm absolute left-sm text-xs font-bold tracking-wide truncate max-w-[90%] -top-base transition-colors',
            )}
          >
            {label}
          </div>
        )}

        {!!error && showErrorMessage ? (
          <Error>
            {/* In some validation libs like Yup an object can be passed to a field and that causes render errors unexpectedly.*/}
            {React.isValidElement(error) || typeof error === 'string'
              ? error
              : 'There is an error'}
          </Error>
        ) : (
          !!subText && <SubText>{subText}</SubText>
        )}
      </div>
    </div>
  )
}

export { FormControlWrapper }
