import React from 'react'
import { FormControlWrapper } from './index'
import { Text } from '../../../FormControls/Text/Text'

const base = () => (
  <FormControlWrapper
    hint="optional"
    label="FromControl Field"
    subText="This is the subtext for the form control input, it will appear regardless of it being editable or not"
  >
    <div className="bg-input rounded p-sm">
      This is the editable form control
    </div>
  </FormControlWrapper>
)

const errorMessageHidden = () => (
  <Text
    hint="optional"
    label="FromControl Field"
    subText="This is the subtext for the form control input, it will appear regardless of it being editable or not"
    error="This is the error message"
    showErrorMessage={false}
  />
)

export default { base, errorMessageHidden }
