import React from 'react'

interface SubText {
  children?: React.ReactNode | string
}

const SubText = ({ children }: SubText): React.ReactElement | null => {
  if (!children) return null
  return <div className="text-sm text-color-subtle">{children}</div>
}

export default SubText
