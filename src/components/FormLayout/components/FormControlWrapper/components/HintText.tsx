import React from 'react'

interface HintTextProps {
  children?: React.ReactNode | string
}

const HintText = ({ children }: HintTextProps): React.ReactElement | null => {
  if (!children) return null

  return <div className="text-sm text-color-subtle">{children}</div>
}

export default HintText
