import React from 'react'
import classNameParser from 'classnames'

interface LabelProps {
  /** Text the label should display */
  children?: React.ReactNode | string
  className?: string
  /** Identifier of the input element the component it's labeling */
  inputName?: string
}

const Label = ({
  children,
  className,
  inputName,
}: LabelProps): React.ReactElement | null => {
  if (!children) return null

  return (
    <label
      htmlFor={inputName}
      className={classNameParser('block font-medium', className)}
    >
      {children}
    </label>
  )
}

export default Label
