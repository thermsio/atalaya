import React from 'react'

interface ErrorProps {
  children?: React.ReactNode | string
}

const Error = ({ children }: ErrorProps): React.ReactElement | null => {
  if (!children) return null

  return <div className="text-sm text-color-critical">{children}</div>
}

export default Error
