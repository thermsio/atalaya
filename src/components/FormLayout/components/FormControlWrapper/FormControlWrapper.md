This component is a wrapper around Atalaya's input fields and is to be used
for creating new input fields outside of Atalaya while maintaining the same
look and feel.

```jsx padded
<FormControlWrapper
  error="Something went very wrong"
  hint="optional"
  inputName="text"
  label="Text Field"
  subText="This is the sub text"
>
  <div className="rounded bg-input-background p-xs text-center text-color-subtle">
    -- Input field will be here --
  </div>
</FormControlWrapper>
```
