import React, { useEffect, useState } from 'react'
import { FormControls } from './FormControls'
import { FormLayout } from '../index'

const loadingTest = () => {
  const [loading, setLoading] = useState(false)
  const [submitted, setSubmitted] = useState(false)

  useEffect(() => {
    if (submitted) {
      setLoading(true)
      const stopLoadingTimeout = setTimeout(() => {
        setLoading(false)
        setSubmitted(false)
      }, 3000)

      return () => clearTimeout(stopLoadingTimeout)
    }
  }, [submitted])

  return (
    <FormLayout>
      <FormControls
        submitting={loading ? 'Submitting' : undefined}
        onCancel={() => null}
        onDelete={() => null}
        onSubmit={() => setSubmitted(true)}
      />
    </FormLayout>
  )
}

export default { loadingTest }
