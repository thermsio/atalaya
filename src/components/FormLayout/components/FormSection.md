You can subdivide a form using the `FormLayout.Section` component.

```jsx
import { FormLayout } from '../index'
import { Text } from '../../FormControls/Text/Text'

;<FormLayout>
  <FormLayout.Section
    heading="Section Title"
    subTitle="And this is the section subtitle, aimed to contribute additional information."
  >
    <Text
      label="Text Field"
      placeholder="Lorem Ipsum"
      subText="Only latin characters allowed."
    />
    <Text
      label="Text Field"
      hint="Required"
      placeholder="Lorem Ipsum"
      subText="Only latin characters allowed."
    />
    <Text
      label="Text Field"
      hint="Required"
      placeholder="Lorem Ipsum"
      subText="Only latin characters allowed."
    />
  </FormLayout.Section>

  <FormLayout.Section
    heading="Section Title"
    horizontal
    subTitle="And this is the section subtitle, aimed to contribute additional information."
  >
    <Text
      label="Text Field"
      placeholder="Lorem Ipsum"
      subText="Only latin characters allowed."
    />
    <Text
      label="Text Field"
      hint="Required"
      placeholder="Lorem Ipsum"
      subText="Only latin characters allowed."
    />
    <Text
      label="Text Field"
      hint="Required"
      placeholder="Lorem Ipsum"
      subText="Only latin characters allowed."
    />
  </FormLayout.Section>
</FormLayout>
```

### Dividers

Dividers prop will override the prop from FormLayout

```jsx
import { FormLayout } from '../index'
import { Text } from '../../FormControls/Text/Text'

;<FormLayout heading="Form has divider lines" dividers="lines">
  <FormLayout.Section
    dividers="space"
    heading="This Section does not"
    horizontal
  >
    <Text
      label="Text Field"
      placeholder="Lorem Ipsum"
      subText="Only latin characters allowed."
    />
    <Text
      label="Text Field"
      hint="Required"
      placeholder="Lorem Ipsum"
      subText="Only latin characters allowed."
    />
    <Text
      label="Text Field"
      hint="Required"
      placeholder="Lorem Ipsum"
      subText="Only latin characters allowed."
    />
  </FormLayout.Section>

  <FormLayout.Section
    heading="This section has not defined a divider prop"
    horizontal
    subTitle="So, it fallsback to whatever FormLayout dictates."
  >
    <Text
      label="Text Field"
      placeholder="Lorem Ipsum"
      subText="Only latin characters allowed."
    />
    <Text
      label="Text Field"
      hint="Required"
      placeholder="Lorem Ipsum"
      subText="Only latin characters allowed."
    />
    <Text
      label="Text Field"
      hint="Required"
      placeholder="Lorem Ipsum"
      subText="Only latin characters allowed."
    />
  </FormLayout.Section>
</FormLayout>
```
