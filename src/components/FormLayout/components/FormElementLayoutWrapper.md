This element serves as a wrapper to be used inside [FormLayout](#/ReactComponents/FormLayout) it provides the necessary styling to fit among other form elements.
