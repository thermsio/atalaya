BottomMenu documentation is in progress...

```jsx
const menuItems = new Array(10).fill(null).map((a, index) => (
  <span className="bg-surface p-xs px-lg" key={index}>
    {index}
  </span>
))

;<BottomMenu>
  <div className="space-x-base">{menuItems}</div>
</BottomMenu>
```
