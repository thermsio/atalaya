import React from 'react'
import { BottomMenu } from './index'

const menuItems = new Array(10)
  .fill(null)
  .map((a, index) => <BottomMenu.Item key={index}>{index}</BottomMenu.Item>)

const base = <BottomMenu>{menuItems}</BottomMenu>

export default { base }
