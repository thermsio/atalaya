import React from 'react'
import classNamesParser from 'classnames'

export interface BottomMenuItemProps extends React.HTMLProps<HTMLDivElement> {
  isActive?: boolean
}

const BottomMenuItem = ({
  className,
  children,
  isActive,
  ...divProps
}: BottomMenuItemProps) => {
  return (
    <div
      className={classNamesParser(
        'group flex grow basis-0 cursor-pointer flex-col items-center justify-center px-sm py-xs text-center font-medium text-color-subtle transition hover:bg-surface-strong',
        { 'bg-surface-subtle text-color-strong': isActive },
        className,
      )}
      {...divProps}
    >
      {children}
    </div>
  )
}

export { BottomMenuItem }
