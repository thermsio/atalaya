import React from 'react'
import './BottomMenu.css'
import classNamesParser from 'classnames'
import { BottomMenuItem } from './components/BottomMenuItem'

export interface BottomMenuProps {
  background?: 'background' | 'surface' | 'surface-strong' | 'surface-subtle'
  children: React.ReactNode
}

const BottomMenu = ({ children, background = 'surface' }: BottomMenuProps) => {
  return (
    <div
      className={classNamesParser(
        {
          'bg-background': background === 'background',
          'bg-surface': background === 'surface',
          'bg-surface-strong': background === 'surface-strong',
          'bg-surface-subtle': background === 'surface-subtle',
        },
        'bottom-menu-container sticky bottom-0 z-sticky border-t border-border',
      )}
    >
      <div
        className={classNamesParser(
          {
            'from-background': background === 'background',
            'from-surface': background === 'surface',
            'from-surface-strong': background === 'surface-strong',
            'from-surface-subtle': background === 'surface-subtle',
          },
          'absolute inset-y-0 left-0 w-base bg-gradient-to-r',
        )}
      />

      <div className="bottom-menu">{children}</div>

      <div
        className={classNamesParser(
          {
            'from-background': background === 'background',
            'from-surface': background === 'surface',
            'from-surface-strong': background === 'surface-strong',
            'from-surface-subtle': background === 'surface-subtle',
          },
          'absolute inset-y-0 right-0 w-base bg-gradient-to-l',
        )}
      />
    </div>
  )
}

BottomMenu.Item = BottomMenuItem

export { BottomMenu }
