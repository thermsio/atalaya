import React from 'react'
import { Card } from '../Card'
import { Label } from './index'

const inline = (
  <div>
    <Label>label </Label>
    <span>Value</span>
  </div>
)

const block = (
  <div>
    <Label block>label</Label>
    <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
  </div>
)

const inCard = (
  <Card>
    <div>
      <Label block>label</Label>
      <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
    </div>
  </Card>
)

const descriptionList = (
  <div className="grid grid-cols-5 gap-sm">
    <Label>full name</Label>
    <span className="col-span-4">John Doe</span>

    <Label>role description</Label>
    <span className="col-span-4">Placeholder for a Person</span>

    <Label>email address</Label>
    <span className="col-span-4">johnDoe@sample.com</span>

    <Label>phone number</Label>
    <span className="col-span-4">555 555 5555</span>

    <Label>about</Label>
    <span className="col-span-4">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
      veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
      commodo consequat.{' '}
    </span>
  </div>
)

export default {
  block,
  descriptionList,
  inline,
  inCard,
}
