import React from 'react'
import classNameParser from 'classnames'

export interface LabelProps {
  /** Render Label as a block level element */
  block?: boolean
  children?: React.ReactNode
  className?: string
  /** HTML Element to be created, `span` by default */
  element?: string
  htmlFor?: string
}

const Label = ({
  block,
  children,
  className,
  element,
  htmlFor,
}: LabelProps): React.ReactElement => {
  let _element = element
  if (!_element && typeof htmlFor === 'string') _element = 'label'

  return React.createElement(
    _element || 'span',
    {
      className: classNameParser(
        { block, 'inline-block': !block },
        'capitalize-first-letter text-color-subtle tracking-wide',
        className,
      ),
      htmlFor,
    },
    children,
  )
}

export { Label }
