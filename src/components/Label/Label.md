A label's is a short string of text that provides context about a value.

## When to use them?

Most of the time, it's not necessary to use a label at all since adjacent elements provide enough context as is.
However, sometimes that context is not enough, labels in these situations are the perfect solution to reduce ambiguity
for the users.

## How to use them?

Labels should be closely placed with their corresponding value to create a tight relation between them.

Labels are secondary information, users are interested primarily on the data the label is providing context for.

## What do they look like?

Our Label it's just a string with 3 distinct stylistic details:

- Uses a more subtle color than regular text.
- Has a slightly wider letter spacing
- It always capitalizes the first letter of the sentence

```jsx
<Label>the label</Label>
```

### Examples

#### Inline

```jsx
<div>
    <Label className="mr-1">label</Label>
    <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</span>
</div>
```

#### Datalist

```jsx
<div className="grid gap-sm grid-cols-5">
    <Label>Full name</Label>
    <span className="col-span-4">John Doe</span>

    <Label>Role description</Label>
    <span className="col-span-4">Placeholder for a Person</span>

    <Label>Email address</Label>
    <span className="col-span-4">johnDoe@sample.com</span>

    <Label>Phone number</Label>
    <span className="col-span-4">555 555 5555</span>

    <Label>About</Label>
    <span className="col-span-4">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
      veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
      commodo consequat.{' '}
    </span>
</div>
```

### Block

```jsx
<div>
    <Label block>label</Label>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
        magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat.</p>
</div>
```

### Big Text

```jsx
import {Stack} from "../../layout/Stack";

<Stack space="lg">
  <div>
    <Label>first item</Label>
    <div className="text-lg">Some Value</div>
  </div>

  <div>
    <Label>second item</Label>
    <div className="text-lg">Some Other Value</div>
  </div>

  <div>
    <Label>third item</Label>
    <div className="text-lg">Yet Another Value</div>
  </div>
</Stack>
```
