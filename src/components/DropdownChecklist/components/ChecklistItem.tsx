import React from 'react'
import { Checkbox } from '../../FormControls/Checkbox/Checkbox'
import { Inline } from '../../../layout/Inline'

interface DropdownOptionProps {
  active: React.ReactNode
  onClick?: () => void
  onClickLabel?: () => void
  onClickCheckBox?: (active?: boolean) => void
  label: React.ReactNode
}

function ChecklistItem({
  active,
  onClick,
  onClickCheckBox,
  label,
}: DropdownOptionProps) {
  return (
    <Inline width="full" className="hover:cursor-pointer" onClick={onClick}>
      <div
        onClick={(e) => {
          if (onClickCheckBox) {
            e.stopPropagation()
            onClickCheckBox()
          }
        }}
      >
        <Checkbox active={!!active} />
      </div>

      <div className="pl-xs">{label}</div>
    </Inline>
  )
}

export { ChecklistItem }
