import React from 'react'
import { Dropdown, DropdownProps } from '../Dropdown'
import { ChecklistItem } from './components/ChecklistItem'
import { Card } from '../Card'

export type DropdownChecklistProps = DropdownProps

export type DropdownChecklistType = React.FC<DropdownChecklistProps> & {
  ChecklistItem: typeof ChecklistItem
}

const DropdownChecklist: DropdownChecklistType = ({
  children,
  ...dropdownProps
}: DropdownChecklistProps) => {
  return (
    <Dropdown {...dropdownProps}>
      <Card space="xs" className="min-w-max">
        {children}
      </Card>
    </Dropdown>
  )
}

DropdownChecklist.ChecklistItem = ChecklistItem

export { DropdownChecklist }
