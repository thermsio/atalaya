Use this component to utilize a checklist inside a dropdown container.

> The parent component is responsible for controlling checklist items active state.

```jsx
import { Button } from '../Button'
import { DropdownChecklist } from './index'
;<DropdownChecklist controller={<Button>Show</Button>}>
  <DropdownChecklist.ChecklistItem active={true} label="Option 1" />
  <DropdownChecklist.ChecklistItem
    active={false}
    label="Option 2"
    onClick={() => alert('You clicked me')}
  />
</DropdownChecklist>
```
