Skeletons components can be used as placeholders for elements that has not yet being loaded.

In essence, they are just rectangles with a waving animations you can set their dimensions too. This simplicity allows
skeletons to be used as atomically as you need, you can use them to inside your components or create separate skeleton
views showing a placeholder of your whole app.

If your Skeletons are inside containers with set dimensions you can use the `fillContainer` prop to make them fill all
available space.

They also accept the `className` prop to further customization.

```jsx
import {Columns} from '../../layout/Columns';
import {Inline} from '../../layout/Inline';
import {Stack} from '../../layout/Stack';

<Stack space="base">
  <Skeleton height="3em"/>

  <Columns space="base">
    <Columns.Column width="3/12">
      <Skeleton fillContainer/>
    </Columns.Column>

    <Columns.Column>
      <Stack space="base">
        <Skeleton height="5em"/>
        <Inline space="base">
          <Skeleton className="flex-grow" height="5em"/>
          <Skeleton height="5em" width="5em"/>
        </Inline>
      </Stack>
    </Columns.Column>
  </Columns>
</Stack>
```

### Circle

`Skeleton.Circle` it's a specialized version of the component that provides an easy API to create circular skeletons
using the `size` prop.

Like the more general version, it also accepts the `fillContainer` prop. Just keep in mind the skeleton will stretch to
fill all the container. To ensure it keeps being a circle, you need to provide a square container.

Additional customization can be provided via `className`.

```jsx
import {Inline} from '../../layout/Inline';

;<Inline space="base" alignY="center">
  <Skeleton.Circle size="5em" />

  <div style={{ height: '5em', width: '10em' }}>
    <Skeleton.Circle fillContainer />
  </div>

  <Skeleton.Circle className="h-xxl w-xxl" />
</Inline>
```

### Text

`Skeleton.Text` it provides a convenient API to create Skeletons that simulate text.

By using the `chars` and `lines` props you quickly create different types of text representations.

- `chars` will display a single line with a width equivalent to _n_ characters.
- `inline` display the skeleton text inline instead of as blocks.
- `lines` will display _n_ lines of full width text.
- `chars` and `lines` at the same time, will first show the full width lines, then an extra final line with its width
  determined by `chars`.
- If no `chars` or `lines` are specified, component will display a single full width line.

You can set `fontSize` to change the size of the skeleton lines, they match the font size scale provided in our tokens.
Alternatively you can wrap the Skeleton in a container div and define the font size there.

```jsx
import {Stack} from '../../layout/Stack';

;<Stack space="lg">
  {/* A 35 Characters equivalent line with font size set to xl */}
  <Skeleton.Text chars={35} fontSize="xl" />
  {/* 3 Full width lines*/}
  <Skeleton.Text lines={3} />
  {/* Single full width line*/}
  <Skeleton.Text />
  {/* First 3 lines, then a 40 characters equivalent line*/}
  <Skeleton.Text chars={40} lines={3} />
  {/* A single line with font size set to sm */}
  <Skeleton.Text fontSize="sm" />
</Stack>
```

#### Inline

```jsx
;<SkeletonText chars={12} inline lines={4} />
```
