import React from 'react'
import classNameParser from 'classnames'
import { SkeletonCircle } from './components/SkeletonCircle'
import { SkeletonText } from './components/SkeletonText'

import './Skeleton.css'

export interface SkeletonProps {
  borderRadius?: 'sm' | 'default'
  className?: string
  fillContainer?: boolean
  height?: number | string
  width?: number | string
}

const Skeleton = ({
  borderRadius = 'default',
  className,
  fillContainer,
  height,
  width,
}: SkeletonProps): React.ReactElement => {
  return (
    <div
      className={classNameParser(
        'skeleton',
        {
          'h-full w-full': fillContainer,
          rounded: borderRadius === 'default',
          'rounded-sm': borderRadius === 'sm',
        },
        className,
      )}
      style={{ height, width }}
    />
  )
}

Skeleton.Circle = SkeletonCircle
Skeleton.Text = SkeletonText

export { Skeleton }
