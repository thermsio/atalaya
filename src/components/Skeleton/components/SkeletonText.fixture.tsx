import React from 'react'
import { SkeletonText } from './SkeletonText'
import { Stack } from '../../../layout/Stack'

// eslint-disable-next-line react/display-name
export default () => (
  <Stack>
    <div>
      <div>Multi-line</div>
      <SkeletonText chars={24} lines={3} />
    </div>

    <div>
      <div>Inline</div>
      <SkeletonText chars={12} inline lines={3} />
    </div>
  </Stack>
)
