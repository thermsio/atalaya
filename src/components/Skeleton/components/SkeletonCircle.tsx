import React from 'react'
import classNameParser from 'classnames'

export interface SkeletonCircleProps {
  className?: string
  fillContainer?: boolean
  size?: number | string
}

const SkeletonCircle = ({
  className,
  fillContainer,
  size,
}: SkeletonCircleProps): React.ReactElement => {
  return (
    <div
      className={classNameParser(
        'skeleton rounded-full',
        { 'h-full w-full': fillContainer },
        className,
      )}
      style={{ height: size, width: size }}
    />
  )
}

export { SkeletonCircle }
