import React, { useMemo } from 'react'
import { Stack } from '../../../layout/Stack'
import { FontSize } from '../../../layout/types'
import { Inline } from '../../../layout/Inline'

export interface SkeletonTextProps {
  chars?: number
  fontSize?: FontSize
  /* Defaults to false, if true, will display: inline using chars as the width of "em" */
  inline?: boolean
  lines?: number
}

const SkeletonText = ({
  chars,
  inline,
  lines,
  fontSize,
}: SkeletonTextProps): React.ReactElement => {
  const TextLines = useMemo(() => {
    if (!lines && !chars) lines = 1

    const textLines: React.ReactElement[] = []

    if (lines) {
      for (let i = 1; i <= lines; i++) {
        textLines.push(
          <div
            className="skeleton w-full rounded"
            key={i}
            style={{ width: inline ? `${chars}ch` : 'auto' }}
          >
            <span className="invisible" aria-hidden="true">
              &#42;
            </span>
          </div>,
        )
      }
    }

    if (!!chars && !inline) {
      textLines.push(
        <span
          className="skeleton rounded"
          style={{
            maxWidth: `${chars}ch`,
          }}
          key={`chars:${chars}`}
        >
          <span className="invisible" aria-hidden="true">
            &#42;
          </span>
        </span>,
      )
    }

    return textLines
  }, [chars, lines])

  if (inline) {
    return (
      <Inline space="xxs" fontSize={fontSize}>
        {TextLines}
      </Inline>
    )
  }

  return (
    <Stack space="sm" fontSize={fontSize}>
      {TextLines}
    </Stack>
  )
}

export { SkeletonText }
