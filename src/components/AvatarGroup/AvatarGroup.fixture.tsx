import React from 'react'
import { AvatarGroup } from './index'
import { Avatar } from '../Avatar'

const base = (
  <AvatarGroup>
    <Avatar
      statusVariant="main"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />
    <Avatar
      statusVariant="positive"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />
    <Avatar
      statusVariant="caution"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />
    <Avatar
      statusVariant="critical"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />
    <Avatar
      statusVariant="info"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />
    <Avatar
      statusVariant="neutral"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />
  </AvatarGroup>
)

const square = (
  <AvatarGroup circular={false}>
    <Avatar
      statusVariant="main"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />
    <Avatar
      statusVariant="positive"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />
    <Avatar
      statusVariant="caution"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />
    <Avatar
      statusVariant="critical"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />
    <Avatar
      statusVariant="info"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />
    <Avatar
      statusVariant="neutral"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />
  </AvatarGroup>
)

const withExtra = (
  <AvatarGroup max={3}>
    <Avatar
      statusVariant="main"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />
    <Avatar
      statusVariant="positive"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />
    <Avatar
      statusVariant="caution"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />
    <Avatar
      statusVariant="critical"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />
    <Avatar
      statusVariant="info"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />
    <Avatar
      statusVariant="neutral"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />
  </AvatarGroup>
)

export default {
  base,
  square,
  withExtra,
}
