import React, { useMemo } from 'react'

import { Avatar, AvatarProps } from '../Avatar'

export interface AvatarGroupProps extends Pick<AvatarProps, 'size'> {
  children: React.ReactNode
  circular?: boolean
  lightbox?: boolean
  max?: number
}

const AvatarGroup = ({
  max,
  size = 'base',
  circular = true,
  children,
  lightbox,
}: AvatarGroupProps): React.ReactElement => {
  const excessAvatars = max ? React.Children.count(children) - max : 0

  const avatarList = useMemo(
    () =>
      React.Children.map(children, (child, index) => {
        if ((max && index + 1 > max) || !React.isValidElement(child)) return

        if (child.type === Avatar)
          return React.cloneElement(child, {
            size,
            circular,
            lightbox,
          } as AvatarGroupProps)

        return child
      }),
    [children, circular, size],
  )

  return (
    <div className="flex -space-x-3">
      {avatarList}

      {excessAvatars > 0 && (
        <Avatar symbol={`+${excessAvatars}`} circular={circular} size={size} />
      )}
    </div>
  )
}

export { AvatarGroup }
