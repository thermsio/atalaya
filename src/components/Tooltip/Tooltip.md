```jsx
<div>
  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
  incididunt ut labore et dolore magna aliqua.{' '}
  <Tooltip tooltip="And this is the tooltip's description." underline>
    This is the tooltip&apos;s component.
  </Tooltip>{' '}
  Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
  eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt
  in culpa qui officia deserunt mollit anim id est laborum.
</div>
```

### Placement

```jsx
<div>
  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
  incididunt ut labore et dolore magna aliqua.{' '}
  <Tooltip
    placement="top"
    tooltip="And this is the tooltip's description."
    underline
  >
    This is the tooltip&apos;s component.
  </Tooltip>{' '}
  Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
  eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt
  in culpa qui officia deserunt mollit anim id est laborum.
</div>
```

### Custom Component

```jsx
import { Icon } from '../Icon'
;<Tooltip
  tooltip={
    <div className="flex items-center space-x-xxs">
      <Icon color="info">
        <svg
          className="h-6 w-6"
          fill="currentColor"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            d="M18 5v8a2 2 0 01-2 2h-5l-5 4v-4H4a2 2 0 01-2-2V5a2 2 0 012-2h12a2 2 0 012 2zM7 8H5v2h2V8zm2 0h2v2H9V8zm6 0h-2v2h2V8z"
            clipRule="evenodd"
          />
        </svg>
      </Icon>
      <span>This is a custom tooltip!</span>
    </div>
  }
  underline
>
  This is the tooltip&apos;s component.
</Tooltip>
```
