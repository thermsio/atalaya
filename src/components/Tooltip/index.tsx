import React, { useLayoutEffect, useRef } from 'react'
import { useHover } from '../../hooks/useHover'
import classNames from 'classnames'
import { useFocusWithin } from 'ahooks'
import {
  autoUpdate,
  flip,
  offset,
  shift,
  useFloating,
} from '@floating-ui/react-dom'
import { FLOATING_UI_PLACEMENT_MAP } from '../DropdownContainer/constants'

export type Placement =
  | 'bottom'
  | 'bottom-left'
  | 'bottom-right'
  | 'left'
  | 'right'
  | 'top'
  | 'top-left'
  | 'top-right'

export interface TooltipProps {
  className?: string
  /** Content that will display the tooltip when hovered over */
  children: React.ReactNode
  /** Tooltip can be externally controlled by this prop, it will only display it when it's true */
  isOpen?: boolean
  /** Preferred place for tooltip, will be overridden if there is not enough room to display it. */
  placement?: Placement
  /** Content that will be displayed inside the tooltip */
  tooltip?: React.ReactNode
  /** Will show a dotted underline below the text. */
  underline?: boolean
}

const Tooltip = ({
  className,
  children,
  isOpen,
  placement = 'top',
  tooltip,
  underline,
}: TooltipProps) => {
  const containerRef = useRef<HTMLDivElement>(null)

  const { x, y, refs, strategy } = useFloating({
    placement: FLOATING_UI_PLACEMENT_MAP[placement],
    whileElementsMounted: autoUpdate,
    middleware: [offset(5), shift(), flip()],
  })

  useLayoutEffect(() => {
    refs.setReference(containerRef.current)
  }, [containerRef.current])

  const isHovering = useHover(containerRef)
  const isFocused = useFocusWithin(containerRef)

  const showTooltip = !!tooltip && (isOpen ?? (isHovering || isFocused))

  return (
    <div
      className={classNames(
        {
          'underline decoration-info decoration-dotted underline-offset-2':
            underline,
        },
        'inline-block',
        className,
      )}
      ref={containerRef}
      tabIndex={0}
    >
      {typeof children === 'string' ? <span>{children}</span> : children}

      {showTooltip && (
        <div
          className="absolute z-tooltip max-w-max rounded border border-border bg-surface-subtle p-xxs text-sm shadow"
          style={{ position: strategy, top: y ?? 0, left: x ?? 0 }}
          ref={refs.setFloating}
        >
          {tooltip}
        </div>
      )}
    </div>
  )
}

export { Tooltip }
