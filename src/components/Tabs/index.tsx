import React, { useMemo } from 'react'
import {
  TabsContextProps,
  TabsContextProvider,
  useTabsContext,
} from './context/TabsContext'
import { TabsDesktop } from './components/TabsDesktop'
import { TabsMobile } from './components/TabsMobile'
import { TabsDesktopSelect } from './components/TabDesktopSelect'
import { Tab } from './components/Tab'
import { TabMonitor } from './components/TabMonitor'

export { TabsContextProps as TabsProps }

const TabsLayout = ({ children }): React.ReactElement | null => {
  const { displayedContent, mode } = useTabsContext()

  const SelectComponent = useMemo(() => {
    if (mode === 'select-mobile') {
      return TabsMobile
    } else if (mode === 'select-desktop') {
      return TabsDesktopSelect
    }

    return null
  }, [mode])

  // We mirror each child as a TabMonitor to inform TabsContext of any change independently of a Tab being rendered.
  const TabsMonitor = useMemo(
    () =>
      React.Children.map(children, (child) => {
        if (React.isValidElement(child) && child.props)
          return React.createElement(TabMonitor, child.props as any)
      }),
    [children],
  )

  return (
    <div className="min-w-0 grow">
      {TabsMonitor}

      {!!SelectComponent ? (
        <SelectComponent />
      ) : (
        <TabsDesktop>{children}</TabsDesktop>
      )}

      {!!displayedContent && displayedContent}
    </div>
  )
}

const Tabs = ({
  align = 'left',
  fullWidth,
  noBottomBorder,
  pills,
  currentKey,
  initialTabKey,
  onTabClick,
  children,
}: TabsContextProps): React.ReactElement => {
  return (
    <TabsContextProvider
      align={align}
      currentKey={currentKey}
      fullWidth={fullWidth}
      initialTabKey={initialTabKey}
      noBottomBorder={noBottomBorder}
      onTabClick={onTabClick}
      pills={pills}
    >
      <TabsLayout>{children}</TabsLayout>
    </TabsContextProvider>
  )
}

Tabs.Tab = Tab

export { Tabs }
