import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import { useIsMobileDevice } from '../../../hooks/useIsMobileDevice'
import { TabProps } from '../components/types'

export interface TabsContextExternalProps {
  /** Left | Right */
  align?: 'left' | 'right'
  /** Will stretch tabs to evenly fill available width */
  fullWidth?: boolean
  /** In uncontrolled mode, the tab with the specified key will start as active.  */
  noBottomBorder?: boolean
  /** Display tabs as pills */
  pills?: boolean
  /** Used when you want to take control of tabs state. Tabs will stop to internally manage which tab is active and will use this tabKey instead.  */
  currentKey?: string
  /** In uncontrolled mode, the tab with the specified key will start as active.  */
  initialTabKey?: string
  /** Callback triggered when a new tab has been picked, it receives the new index as its argument. */
  onTabClick?:
    | React.Dispatch<React.SetStateAction<string>>
    | ((newTabKey: string) => void)
}

type TabsMode = 'tabs' | 'select-desktop' | 'select-mobile'

export interface TabsContextValue {
  align?: 'left' | 'right'
  breakWidth?: number
  containerRef: React.RefObject<HTMLDivElement>
  currentKey?: string
  deRegisterTab: (key: string) => void
  displayedContent?: React.ReactNode
  fullWidth?: boolean
  handleTabClick: (newTabKey: string) => void
  mode?: TabsMode
  noBottomBorder?: boolean
  pills?: boolean
  registerTabProps: (key: string, children: any) => void
  setBreakWidth: (newBreakWidth: number) => void
  setWidth: (newWidth: number) => void
  tabsPropsByKey?: { [key: string]: any }
  width?: number
}

export interface TabsContextProps extends TabsContextExternalProps {
  /** The Tabs, you must use the <Tabs.Tab /> subcomponent for proper compatibility.  */
  children: React.ReactNode
}

const TabsContext = React.createContext<TabsContextValue>(undefined!)

export const TabsContextProvider = ({
  align,
  children,
  currentKey,
  fullWidth,
  initialTabKey,
  noBottomBorder,
  onTabClick,
  pills,
}: TabsContextProps): React.ReactElement => {
  const [breakWidth, setBreakWidth] = useState<number>(0)
  const [localCurrentKey, setLocalCurrentKey] = useState(initialTabKey)
  const isMobileDevice = useIsMobileDevice()
  const [mode, setMode] = useState<TabsMode>()
  const [tabsPropsByKey, setTabsPropsByKey] = useState<
    Record<string, TabProps>
  >({})
  const [width, setWidth] = useState<number>()

  const _currentKey = currentKey || localCurrentKey

  useEffect(() => {
    // todo: remove if satisfactory on mobile UI
    // if (isMobileDevice) {
    //   setMode('select-mobile')
    // } else
    if (width && width < breakWidth) {
      setMode('select-desktop')
    } else {
      setMode('tabs')
    }
  }, [breakWidth, children, isMobileDevice, width])

  const registerTabProps = useCallback((key: string, tabChildren: TabProps) => {
    setTabsPropsByKey((prevValue) => ({ ...prevValue, [key]: tabChildren }))
  }, [])

  const deRegisterTab = useCallback((key: string) => {
    setTabsPropsByKey((prevValue) => {
      const newValue = { ...prevValue }

      delete newValue[key]

      return newValue
    })
  }, [])

  const handleTabClick = useCallback(
    (tabKey: string) => {
      setLocalCurrentKey(tabKey)

      if (onTabClick) onTabClick(tabKey)
    },
    [onTabClick],
  )

  const displayedContent = useMemo(() => {
    if (_currentKey && tabsPropsByKey[_currentKey]) {
      return tabsPropsByKey[_currentKey].children
    }
  }, [_currentKey, tabsPropsByKey])

  const ctxValue = {
    align,
    breakWidth,
    currentKey: _currentKey,
    containerRef: useRef<HTMLDivElement>(null),
    deRegisterTab,
    displayedContent,
    fullWidth: fullWidth && !!breakWidth,
    handleTabClick,
    mode,
    noBottomBorder,
    pills,
    registerTabProps,
    setBreakWidth,
    setWidth,
    tabsPropsByKey,
    width,
  }

  return (
    <TabsContext.Provider value={ctxValue}>{children}</TabsContext.Provider>
  )
}

export const useTabsContext = (contextRequired = false): TabsContextValue => {
  const tabsCtx = React.useContext(TabsContext)

  if (contextRequired && tabsCtx === undefined) {
    throw new Error('useTabsContext must be a child below TabsProvider')
  }

  return tabsCtx
}
