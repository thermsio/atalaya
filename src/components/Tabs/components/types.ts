import React from 'react'

export interface TabProps {
  /** Children will be shown when tab is active.  */
  children?: React.ReactNode
  /** Disabled tabs can't be clicked and won't show any content */
  disabled?: boolean
  /** SVG that will be used as Icon. */
  icon?: React.ReactSVGElement | React.ReactElement
  /** Used for edge cases where more than the default max of 99 needs to be shown. */
  maxNotifications?: number
  /** Number to display inside the badge. */
  notifications?: number | string
  /** Unique identifier for this tab, will be passed onto Tabs onTabClick as an argument. Will use the "title" prop if undefined */
  tabKey?: string
  /** Text that will be displayed on tab. */
  title: string | React.ReactElement
}

export interface TabSelectProps {
  label: string
  value: TabProps
}
