import React, { useMemo } from 'react'
import { useTabsContext } from '../context/TabsContext'
import { Select } from '../../FormControls/Select/Select'
import { TabSelectOption } from './TabSelectOption'
import { TabSelectLabel } from './TabSelectLabel'
import { useEventListener } from '../../../hooks/useEventListener'
import { TabProps } from './types'

const TabsDesktopSelect = (): React.ReactElement => {
  const {
    breakWidth,
    setBreakWidth,
    containerRef,
    currentKey,
    tabsPropsByKey,
    handleTabClick,
    setWidth,
  } = useTabsContext()
  useEventListener('resize', () => {
    if (!breakWidth) setBreakWidth(containerRef.current?.scrollWidth || 0)
    const width = containerRef?.current?.clientWidth
    if (width) setWidth(width)
  })

  const options = useMemo<{ label: string; value: TabProps }[]>(() => {
    if (tabsPropsByKey) {
      return Object.values(tabsPropsByKey).map((tabProps) => {
        return { label: tabProps.title, value: tabProps }
      })
    } else {
      return []
    }
  }, [tabsPropsByKey])

  const selectedOption = useMemo(
    () => options.find((option) => option.value.tabKey === currentKey),
    [currentKey, options],
  )

  return (
    <div ref={containerRef} className="mb-base">
      <Select
        isClearable={false}
        isOptionDisabled={(option) => !!option.value.disabled}
        isSearchable={false}
        multi={false}
        options={options}
        value={selectedOption}
        optionLabelExtractor={({ value }) => value.title}
        optionValueExtractor={({ value }) =>
          value.tabKey || value.title.toString()
        }
        RenderListItem={TabSelectOption}
        RenderSelectedSingle={TabSelectLabel}
        onChangeOptions={(option) => handleTabClick(option.value.tabKey!)}
      />
    </div>
  )
}

export { TabsDesktopSelect }
