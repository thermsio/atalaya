import React from 'react'
import { Badge } from '../../Badge'
import { Columns } from '../../../layout/Columns'
import { Icon } from '../../Icon'
import { TabSelectProps } from './types'

const TabSelectOption = ({ value }: TabSelectProps): React.ReactElement => {
  const { disabled, icon, notifications, title } = value

  return (
    <Columns
      space="sm"
      fontSize="sm"
      textColor={disabled ? 'subtle' : undefined}
    >
      {!!icon && (
        <Columns.Column width="content">
          <Icon size="sm">{icon}</Icon>
        </Columns.Column>
      )}

      <Columns.Column>
        <span>{title}</span>
      </Columns.Column>

      {!!notifications && (
        <Columns.Column width="content">
          <Badge value={notifications} />
        </Columns.Column>
      )}
    </Columns>
  )
}

export { TabSelectOption }
