import React from 'react'
import { TabSelectProps } from './types'

import { Badge } from '../../Badge'
import { Columns } from '../../../layout/Columns'
import { Icon } from '../../Icon'

const TabSelectLabel = (option: TabSelectProps): React.ReactElement => {
  const { icon, title, notifications } = option.value

  return (
    <Columns space="sm" fontSize="sm">
      {!!icon && (
        <Columns.Column width="content">
          <Icon size="sm">{icon}</Icon>
        </Columns.Column>
      )}

      <Columns.Column>
        <span>{title}</span>
      </Columns.Column>

      {!!notifications && (
        <Columns.Column width="content">
          <Badge value={notifications} variant="main" />
        </Columns.Column>
      )}
    </Columns>
  )
}

export { TabSelectLabel }
