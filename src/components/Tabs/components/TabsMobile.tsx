import React, { useMemo } from 'react'
import { useTabsContext } from '../context/TabsContext'
import { HTMLSelect } from '../../FormControls/HTMLSelect/HTMLSelect'

const TabsMobile = (): React.ReactElement => {
  const { currentKey, tabsPropsByKey, handleTabClick } = useTabsContext()

  const options = useMemo(() => {
    if (tabsPropsByKey) {
      return Object.entries(tabsPropsByKey).map(([keyName, tabProps]) => ({
        label: tabProps.title,
        disabled: tabProps.disabled,
        value: keyName,
      }))
    }

    return []
  }, [tabsPropsByKey])

  return (
    <HTMLSelect
      options={options}
      value={currentKey ? options[currentKey] : undefined}
      onChangeValue={handleTabClick}
    />
  )
}

export { TabsMobile }
