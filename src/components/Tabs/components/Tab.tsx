import React, { useState } from 'react'
import { useTabsContext } from '../context/TabsContext'
import classNameParser from 'classnames'
import { Inline } from '../../../layout/Inline'
import { Icon } from '../../Icon'
import { Badge } from '../../Badge'

import './Tab.css'
import { TabProps } from './types'

const Tab = (props: TabProps): React.ReactElement | null => {
  // the tabKey is optional and the title can be an element, so we coerce it to a string as a fallback
  const [tabId] = useState(() => props.tabKey || props.title.toString())
  const { disabled, icon, maxNotifications, notifications, title } = props

  const { currentKey, fullWidth, handleTabClick, noBottomBorder, pills } =
    useTabsContext()

  const active = currentKey === tabId

  return (
    <button
      className={classNameParser('tab', {
        'tab-active': active,
        'tab-with-border': !noBottomBorder && !pills,
        'tab-pill': pills,
        'tab-disabled': disabled,
        'flex-grow': fullWidth,
      })}
      onClick={disabled ? undefined : () => handleTabClick(tabId)}
      aria-current={active ? 'page' : undefined}
    >
      <Inline alignY="start" space="sm">
        {!!icon && <Icon size="sm">{icon}</Icon>}

        <span className="whitespace-nowrap">{title}</span>

        {!!notifications && (
          <Badge
            max={maxNotifications}
            subtle={!active}
            value={notifications}
            variant={active ? 'main' : 'neutral'}
          />
        )}
      </Inline>
    </button>
  )
}

export { Tab }
