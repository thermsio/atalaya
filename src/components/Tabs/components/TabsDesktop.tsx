import React, { useEffect, useLayoutEffect, useState } from 'react'
import { useEventListener } from '../../../hooks/useEventListener'

import classNameParser from 'classnames'

import './Tab.css'
import { useTabsContext } from '../context/TabsContext'

const TabsDesktop = ({ children }) => {
  const {
    align,
    breakWidth,
    containerRef,
    fullWidth,
    noBottomBorder,
    pills,
    setBreakWidth,
    setWidth,
    width,
  } = useTabsContext()
  const [childrenSize, setChildrenSize] = useState<number>(
    React.Children.count(children),
  )

  useLayoutEffect(() => {
    if (containerRef?.current && !width && !breakWidth) {
      setBreakWidth(containerRef.current.scrollWidth)
      setWidth(containerRef.current.clientWidth)
    }
  }, [containerRef?.current, width, breakWidth])

  useEventListener('resize', () => {
    if (!breakWidth) setBreakWidth(containerRef.current?.scrollWidth || 0)
    const width = containerRef?.current?.clientWidth
    if (width) setWidth(width)
  })

  useEffect(() => {
    if (
      containerRef?.current &&
      childrenSize !== React.Children.count(children)
    ) {
      setChildrenSize(React.Children.count(children))
      setBreakWidth(containerRef.current.scrollWidth)
      setWidth(containerRef.current.clientWidth)
    }
  }, [children])

  return (
    <div
      className={classNameParser(
        {
          'border-b border-border': !noBottomBorder && !pills,
        },
        'mb-base grow',
      )}
    >
      <div
        className={classNameParser({
          'max-w-max': !fullWidth,
          'ml-auto': align === 'right',
          'mr-auto': align === 'left',
        })}
        ref={containerRef}
      >
        <div className="relative flex w-full space-x-base">{children}</div>
      </div>
    </div>
  )
}

export { TabsDesktop }
