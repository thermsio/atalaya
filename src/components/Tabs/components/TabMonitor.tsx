import { useEffect, useState } from 'react'
import { useTabsContext } from '../context/TabsContext'
import { TabProps } from './types'

import './Tab.css'

// This component it's in charge of reporting keeping the tab list updated on mobile views.
const TabMonitor = (props: TabProps): null => {
  // the tabKey is optional and the title can be an element, so we coerce it to a string as a fallback
  const [tabId] = useState(() => props.tabKey || props.title.toString())

  const { deRegisterTab, registerTabProps } = useTabsContext()

  useEffect(() => {
    if (tabId) registerTabProps(tabId, props)

    return () => {
      if (tabId) deRegisterTab(tabId)
    }
  }, [props])

  return null
}

export { TabMonitor }
