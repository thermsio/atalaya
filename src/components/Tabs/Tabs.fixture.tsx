import React, { useState } from 'react'
import { Tabs } from './index'
import { Button } from '../Button'
import { ButtonGroup } from '../ButtonGroup'

const pills = (
  <Tabs onTabClick={(index) => console.log(`Active tab index: ${index}`)} pills>
    <Tabs.Tab
      tabKey="one"
      title="Tab 1"
      notifications={5}
      icon={
        <svg
          className="h-6 w-6"
          fill="currentColor"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
            clipRule="evenodd"
          />
        </svg>
      }
    />
    <Tabs.Tab
      tabKey="two"
      title="Tab 2"
      icon={
        <svg
          className="h-6 w-6"
          fill="currentColor"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
            clipRule="evenodd"
          />
        </svg>
      }
    />
    <Tabs.Tab
      tabKey="three"
      title="Tab 3"
      icon={
        <svg
          className="h-6 w-6"
          fill="currentColor"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
            clipRule="evenodd"
          />
        </svg>
      }
    />
    <Tabs.Tab
      tabKey="four"
      title="Tab 4"
      icon={
        <svg
          className="h-6 w-6"
          fill="currentColor"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
            clipRule="evenodd"
          />
        </svg>
      }
    />
    <Tabs.Tab
      tabKey="five"
      title="Tab 5"
      icon={
        <svg
          className="h-6 w-6"
          fill="currentColor"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
            clipRule="evenodd"
          />
        </svg>
      }
    />
  </Tabs>
)

const fullWidthInsideFlexContainer = (
  <div className="flex">
    <Tabs>
      <Tabs.Tab tabKey="one" title="Tab 1">
        <div className="text-color-main">Tab 1 content</div>
      </Tabs.Tab>
      <Tabs.Tab tabKey="two" title="Tab 2">
        <div className="text-color-info">Tab 2 content</div>
      </Tabs.Tab>
      <Tabs.Tab tabKey="three" title="Tab 3" disabled>
        <div className="text-color-critical">
          Tab 3 content, we should not see this
        </div>
      </Tabs.Tab>
      <Tabs.Tab tabKey="four" title="Tab 4">
        <div className="text-color-caution">Tab 4 content</div>
      </Tabs.Tab>
    </Tabs>
  </div>
)

const dynamicChildrenInsideFlexContainer = () => {
  const [count, setCount] = useState(2)

  return (
    <div className="space-y-base">
      <ButtonGroup fullWidth>
        <Button onClick={() => setCount(count + 1)} variant="positive">
          Add Tab
        </Button>
        <Button onClick={() => setCount(count - 1)} variant="critical">
          Remove Tab
        </Button>
      </ButtonGroup>

      <div>Flex Parent</div>
      <div className="flex">
        <Tabs>
          {new Array(count).fill(null).map((_, i) => (
            <Tabs.Tab key={i} title={`Tab ${i + 1}`}>
              <div className="text-color-main">Tab {i + 1} content</div>
            </Tabs.Tab>
          ))}
        </Tabs>
      </div>

      <div>Non-Flex Parent</div>
      <div className="">
        <Tabs>
          {new Array(count).fill(null).map((_, i) => (
            <Tabs.Tab key={i} title={`Tab ${i + 1}`}>
              <div className="text-color-main">Tab {i + 1} content</div>
            </Tabs.Tab>
          ))}
        </Tabs>
      </div>
    </div>
  )
}

const initialKey = (
  <Tabs initialTabKey="two">
    <Tabs.Tab tabKey="one" title="Tab 1">
      <div className="text-color-main">Tab 1 content</div>
    </Tabs.Tab>
    <Tabs.Tab tabKey="two" title="Tab 2">
      <div className="text-color-info">Tab 2 content</div>
    </Tabs.Tab>
    <Tabs.Tab tabKey="three" title="Tab 3" disabled>
      <div className="text-color-critical">
        Tab 3 content, we should not see this
      </div>
    </Tabs.Tab>
    <Tabs.Tab tabKey="four" title="Tab 4">
      <div className="text-color-caution">Tab 4 content</div>
    </Tabs.Tab>
  </Tabs>
)

export default {
  dynamicChildrenInsideFlexContainer,
  fullWidthInsideFlexContainer,
  initialKey,
  pills,
}
