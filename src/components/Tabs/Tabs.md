`Tabs` actually consists in two components. `Tabs.Tab` are single tabs that you can customize as needed and `Tabs` is
the container that should wrap every tab, it provides state management. There are two ways to use them, the simplest one
is to provide the content you want to display on each individual tab as a children element.

```jsx
<Tabs initialTabKey="one">
  <Tabs.Tab tabKey="one" title="Tab 1">
    <div className="text-color-main">Tab 1 content</div>
  </Tabs.Tab>
  <Tabs.Tab tabKey="two" title="Tab 2">
    <div className="text-color-info">Tab 2 content</div>
  </Tabs.Tab>
  <Tabs.Tab tabKey="three" title="Tab 3" disabled>
    <div className="text-color-critical">
      Tab 3 content, we should not see this
    </div>
  </Tabs.Tab>
  <Tabs.Tab tabKey="four" title="Tab 4">
    <div className="text-color-caution">Tab 4 content</div>
  </Tabs.Tab>
  <Tabs.Tab tabKey="five" title="Tab 5">
    <div className="text-color-positive">Tab 5 content</div>
  </Tabs.Tab>
</Tabs>
```

### Controlled component

You may also use them as controlled components by providing an `activeTabKey`. If present, Tabs will no longer keep w
track of which tab is active, you are responsible for updating. To achieve this you need to provide to each one of your
tabs with a `tabKey` and a `onTabClick` callback to tabs. `onTabClick` will receive the key of the selected tab, and you
should update `activeTabKey` with that information.

```jsx
const [activeTabKey, setActiveTabKey] = React.useState('one');

<Tabs onTabClick={(key) => setActiveTabKey(key)} activeTabKey={activeTabKey}>
  <Tabs.Tab tabKey="one" title="Tab 1">
    <div className="mt-base">
      <h3 className="text-color-main">Tab 1</h3>
    </div>
  </Tabs.Tab>
  <Tabs.Tab tabKey="two" title="Tab 2">
    <div className="mt-base">
      <h3 className="text-color-info">Tab 2</h3>
    </div>
  </Tabs.Tab>
  <Tabs.Tab tabKey="three" title="Tab 3">
    <div className="mt-base">
      <h3 className="text-color-positive">Tab 3</h3>
    </div>
  </Tabs.Tab>
  <Tabs.Tab tabKey="four" title="Tab 4">
    <div className="mt-base">
      <h3 className="text-color-caution">Tab 4</h3>
    </div>
  </Tabs.Tab>
  <Tabs.Tab tabKey="five" title="Tab 5">
    <div className="mt-base">
      <h3 className="text-color-critical">Tab 5</h3>
    </div>
  </Tabs.Tab>
</Tabs>
```

Notice that while you can keep adding content as children of tabs, you don't need to do it. You can just as well ad tabs
without children and display that information in whatever other way you choose.

### Right Aligned

```jsx
<Tabs align="right" initialIndex={0} onTabClick={(index) => console.log(`Active tab index: ${index}`)}>
  <Tabs.Tab tabKey="one" title="Tab 1"/>
  <Tabs.Tab tabKey="two" title="Tab 2"/>
  <Tabs.Tab tabKey="three" title="Tab 3"/>
  <Tabs.Tab tabKey="four" title="Tab 4"/>
  <Tabs.Tab tabKey="five" title="Tab 5"/>
</Tabs>
```

### Full width

```jsx
<Tabs fullWidth initialIndex={0} onTabClick={(index) => console.log(`Active tab index: ${index}`)}>
  <Tabs.Tab tabKey="one" title="Tab 1"/>
  <Tabs.Tab tabKey="two" title="Tab 2"/>
  <Tabs.Tab tabKey="three" title="Tab 3"/>
  <Tabs.Tab tabKey="four" title="Tab 4"/>
  <Tabs.Tab tabKey="five" title="Tab 5"/>
</Tabs>
```

### With Icon

```jsx
<Tabs onTabClick={(index) => console.log(`Active tab index: ${index}`)}>
  <Tabs.Tab tabKey="one" title="Tab 1"
            icon={<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                    clipRule="evenodd"/>
            </svg>}/>
  <Tabs.Tab tabKey="two" title="Tab 2"
            icon={<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                    clipRule="evenodd"/>
            </svg>}/>
  <Tabs.Tab tabKey="three" title="Tab 3"
            icon={<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                    clipRule="evenodd"/>
            </svg>}/>
  <Tabs.Tab tabKey="four" title="Tab 4"
            icon={<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                    clipRule="evenodd"/>
            </svg>}/>
  <Tabs.Tab tabKey="five" title="Tab 5"
            icon={<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                    clipRule="evenodd"/>
            </svg>}/>
</Tabs>
```

### With Icon and Notification

```jsx
const [activeTabKey, setActiveTabKey] = React.useState(0);

<Tabs onTabClick={setActiveTabKey} activeTabKey={activeTabKey}>
  <Tabs.Tab tabKey="one" title="Tab 1"
            icon={<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                    clipRule="evenodd"/>
            </svg>} notifications={10}/>
  <Tabs.Tab tabKey="two" title="Tab 2"
            icon={<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                    clipRule="evenodd"/>
            </svg>} notifications={10}/>
  <Tabs.Tab tabKey="three" title="Tab 3"
            icon={<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                    clipRule="evenodd"/>
            </svg>} notifications={10}/>
  <Tabs.Tab tabKey="four" title="Tab 4"
            icon={<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                    clipRule="evenodd"/>
            </svg>} notifications={10}/>
  <Tabs.Tab tabKey="five" title="Tab 5"
            icon={<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                    clipRule="evenodd"/>
            </svg>} notifications={10}/>
</Tabs>
```

### Without bottom border

```jsx
<Tabs onTabClick={(index) => console.log(`Active tab index: ${index}`)} noBottomBorder>
  <Tabs.Tab tabKey="one" title="Tab 1"
            icon={<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                    clipRule="evenodd"/>
            </svg>}/>
  <Tabs.Tab tabKey="two" title="Tab 2"
            icon={<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                    clipRule="evenodd"/>
            </svg>}/>
  <Tabs.Tab tabKey="three" title="Tab 3"
            icon={<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                    clipRule="evenodd"/>
            </svg>}/>
  <Tabs.Tab tabKey="four" title="Tab 4"
            icon={<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                    clipRule="evenodd"/>
            </svg>}/>
  <Tabs.Tab tabKey="five" title="Tab 5"
            icon={<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                    clipRule="evenodd"/>
            </svg>}/>
</Tabs>
```

### As Pills

```jsx
<Tabs onTabClick={(index) => console.log(`Active tab index: ${index}`)} pills>
  <Tabs.Tab tabKey="one" title="Tab 1"
            icon={<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                    clipRule="evenodd"/>
            </svg>}/>
  <Tabs.Tab tabKey="two" title="Tab 2"
            icon={<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                    clipRule="evenodd"/>
            </svg>}/>
  <Tabs.Tab tabKey="three" title="Tab 3"
            icon={<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                    clipRule="evenodd"/>
            </svg>}/>
  <Tabs.Tab tabKey="four" title="Tab 4"
            icon={<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                    clipRule="evenodd"/>
            </svg>}/>
  <Tabs.Tab tabKey="five" title="Tab 5"
            icon={<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                    clipRule="evenodd"/>
            </svg>}/>
</Tabs>
```
