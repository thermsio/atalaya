import React from 'react'
import { Icon } from './index'

const async = (
  <div>
    <div>Icon should be right here</div>
    <div className="flex items-center">
      <div style={{ background: 'red', height: '2px', width: '100px' }} />
      <Icon url="https://unpkg.com/@therms/icons@2.6.0/svg/align_vertical_top.svg" />
      <div style={{ background: 'red', height: '2px', width: '100px' }} />
    </div>
  </div>
)

const saveSpace = (
  <div className="space-y-base">
    <p>Default size</p>

    <div className="flex">
      <div className="bg-main p-xl" />
      <div className="flex flex-col">
        <div className="flex-1 bg-main" />
        <Icon preserveSpace />
        <div className="flex-1 bg-main" />
      </div>
      <div className="bg-main p-xl" />
    </div>

    <p>Lg size</p>

    <div className="flex">
      <div className="bg-main p-xl" />
      <div className="flex flex-col">
        <div className="flex-1 bg-main" />
        <Icon preserveSpace size="lg" />
        <div className="flex-1 bg-main" />
      </div>
      <div className="bg-main p-xl" />
    </div>

    <p>The transparent square is the icon</p>
  </div>
)

export default { async, saveSpace }
