Provides a standardized way of displaying icons, ensuring maximum compatibility
with the rest of our components.

While it should support different formats, Icon expects to receive SVG images.
To ensure maximum compatibility, always provide SVG images as children of Icon.

### Colors
It supports, Text and Semantic colors and HEX values

```jsx
import {Inline} from '../../layout/Inline';
import {Stack} from '../../layout/Stack';
import Add from '@therms/icons/svg/ui/web/add.svg';

<
  Inline
  space="base"
  wrap>
  <Stack
    alignX="center"
    space="xs">
    <Icon>
      <Add/>
    </Icon>
    <div className="text-color-subtle font-bold">Default</div>
  </Stack>

  <Stack alignX="center" space="xs">
    <Icon color="subtle">
      <Add/>
    </Icon>
    <div className="text-color-subtle font-bold">Subtle</div>
  </Stack>

  <Stack alignX="center" space="xs">
    <Icon color="strong">
      <Add/>
    </Icon>
    <div className="text-color-subtle font-bold">Strong</div>
  </Stack>

  <Stack alignX="center" space="xs">
    <Icon color="semantic">
      <Add/>
    </Icon>
    <div className="text-color-subtle font-bold">Semantic</div>
  </Stack>

  <Stack alignX="center" space="xs">
    <Icon color="main">
      <Add/>
    </Icon>
    <div className="text-color-subtle font-bold">Main</div>
  </Stack>

  <Stack alignX="center" space="xs">
    <Icon color="neutral">
      <Add/>
    </Icon>
    <div className="text-color-subtle font-bold">Neutral</div>
  </Stack>

  <Stack alignX="center" space="xs">
    <Icon color="positive">
      <Add/>
    </Icon>
    <div className="text-color-subtle font-bold">Positive</div>
  </Stack>

  <Stack alignX="center" space="xs">
    <Icon color="caution">
      <Add/>
    </Icon>
    <div className="text-color-subtle font-bold">Caution</div>
  </Stack>

  <Stack alignX="center" space="xs">
    <Icon color="critical">
      <Add/>
    </Icon>
    <div className="text-color-subtle font-bold">Critical</div>
  </Stack>

  <Stack alignX="center" space="xs">
    <Icon color="info">
      <Add/>
    </Icon>
    <div className="text-color-subtle font-bold">Info</div>
  </Stack>

  <Stack alignX="center" space="xs">
    <Icon color="#fffd00">
      <Add/>
    </Icon>
    <div className="text-color-subtle font-bold">Custom</div>
  </Stack>
</Inline>
```

### Sizes

Icon sizes are meant to exactly match text lines height's. This allows you to
easily pair Icons and text without worrying about vertical alignment.

```jsx
import { Inline } from '../../layout/Inline';
import { Stack } from '../../layout/Stack';
import Notification from '@therms/icons/svg/ui/web/notification.svg';

<Stack space="base">
  <Inline>
    <Icon
      color="positive"
      size="sm">
      <Notification />
    </Icon>
    <span className="text-sm">Small Text</span>
  </Inline>

  <Inline>
    <Icon color="positive" size="base">
      <Notification />
    </Icon>
    <span className="text-base">Base Text</span>
  </Inline>

  <Inline>
    <Icon color="positive" size="lg">
      <Notification />
    </Icon>
    <span className="text-lg">Large Text</span>
  </Inline>

  <Inline>
    <Icon color="positive" size="xl">
      <Notification />
    </Icon>
    <span className="text-xl">Extra Large Text</span>
  </Inline>

  <Inline>
    <Icon color="positive" size="xxl">
      <Notification />
    </Icon>
    <span className="text-xxl">Display Text</span>
  </Inline>
</Stack>
```

### Flip

Change the direction an Icon is facing by using `flipX` and `flipY`
props. This is useful for icons that are not symmetric and rotating them to the
desired direction would cause them to be upside down.

```jsx
import { Inline } from '../../layout/Inline';
import { Stack } from '../../layout/Stack';

<Stack space="sm">
  <div className="text-color-subtle font-bold">flipX</div>

  <Inline space="sm">
    <Icon>
      <svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M3 6a3 3 0 013-3h10a1 1 0 01.8 1.6L14.25 8l2.55 3.4A1 1 0 0116 13H6a1 1 0 00-1 1v3a1 1 0 11-2 0V6z" clipRule="evenodd" /></svg>
    </Icon>

    <Icon flipX>
      <svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M3 6a3 3 0 013-3h10a1 1 0 01.8 1.6L14.25 8l2.55 3.4A1 1 0 0116 13H6a1 1 0 00-1 1v3a1 1 0 11-2 0V6z" clipRule="evenodd" /></svg>
    </Icon>
  </Inline>

  <div className="text-color-subtle font-bold">flipY</div>

  <Inline space="sm">
    <Icon>
      <svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M3 6a3 3 0 013-3h10a1 1 0 01.8 1.6L14.25 8l2.55 3.4A1 1 0 0116 13H6a1 1 0 00-1 1v3a1 1 0 11-2 0V6z" clipRule="evenodd" /></svg>
    </Icon>

    <Icon flipY>
      <svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M3 6a3 3 0 013-3h10a1 1 0 01.8 1.6L14.25 8l2.55 3.4A1 1 0 0116 13H6a1 1 0 00-1 1v3a1 1 0 11-2 0V6z" clipRule="evenodd" /></svg>
    </Icon>
  </Inline>

</Stack>

```

### Rotate
Turn the icon clockwise by 90° on each step.

```jsx
import { Inline } from '../../layout/Inline';

<Inline space="base">
  <Icon>
    <svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM7 9a1 1 0 100-2 1 1 0 000 2zm7-1a1 1 0 11-2 0 1 1 0 012 0zm-.464 5.535a1 1 0 10-1.415-1.414 3 3 0 01-4.242 0 1 1 0 00-1.415 1.414 5 5 0 007.072 0z" clipRule="evenodd" /></svg>
  </Icon>
  <Icon rotate="1/4">
    <svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM7 9a1 1 0 100-2 1 1 0 000 2zm7-1a1 1 0 11-2 0 1 1 0 012 0zm-.464 5.535a1 1 0 10-1.415-1.414 3 3 0 01-4.242 0 1 1 0 00-1.415 1.414 5 5 0 007.072 0z" clipRule="evenodd" /></svg>
  </Icon>
  <Icon rotate="2/4">
    <svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM7 9a1 1 0 100-2 1 1 0 000 2zm7-1a1 1 0 11-2 0 1 1 0 012 0zm-.464 5.535a1 1 0 10-1.415-1.414 3 3 0 01-4.242 0 1 1 0 00-1.415 1.414 5 5 0 007.072 0z" clipRule="evenodd" /></svg>
  </Icon>
  <Icon rotate="3/4">
    <svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM7 9a1 1 0 100-2 1 1 0 000 2zm7-1a1 1 0 11-2 0 1 1 0 012 0zm-.464 5.535a1 1 0 10-1.415-1.414 3 3 0 01-4.242 0 1 1 0 00-1.415 1.414 5 5 0 007.072 0z" clipRule="evenodd" /></svg>
  </Icon>
</Inline>
```

### Spin

The prop `spin` is a 0 configuration way to make any icon spin. Useful for
loading indicators.

Note: Spin will not work if any of `flipX, flipY, rotate` have also been used on
to the icon.

```jsx
import Update from '@therms/icons/svg/ui/web/update.svg';

<Icon size="lg" spin>
    <Update />
</Icon>
```
