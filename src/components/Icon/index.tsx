import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { IconClassProps, makeIconClasses } from './utils'
import parseToHTML from 'html-react-parser'
import domPurify from 'dompurify'

import { Constants } from '../../constants'
import { usePrevious } from '../../hooks/usePrevious'

const cachedSvgs = new Map<string, React.ReactSVGElement>()

const useAsyncSvg = (url?) => {
  const [svg, setSvg] = useState<React.ReactSVGElement | undefined>(undefined)
  const [loading, setLoading] = useState<boolean>(!!url)
  const [error, setError] = useState()
  const prevUrl = usePrevious(url)

  const getSvgFromUrl = useCallback(async (_url, abortSignal) => {
    setLoading(true)

    try {
      const svgData = await fetch(_url, {
        signal: abortSignal,
        cache: 'force-cache',
      })

      if (svgData.ok) {
        const svgText = await svgData.text()
        const sanitizedSvg = domPurify.sanitize(svgText)

        const svg = parseToHTML(sanitizedSvg)

        cachedSvgs.set(_url, svg as React.ReactSVGElement)

        return svg as React.ReactSVGElement
      }
    } catch (e: any) {
      if (!abortSignal.aborted) {
        setError(e)
      }
    } finally {
      setLoading(false)
    }
  }, [])

  useEffect(() => {
    if (url && url !== prevUrl) {
      if (cachedSvgs.has(url)) {
        setSvg(cachedSvgs.get(url) as React.ReactSVGElement)
        setLoading(false)
        return
      }

      const abortController = new AbortController()

      getSvgFromUrl(url, abortController.signal).then((svgElement) => {
        if (svgElement) setSvg(svgElement)
      })

      return () => {
        abortController.abort()
      }
    } else {
      setSvg(undefined)
    }
  }, [url])

  return { svg, loading, error }
}

export interface IconProps extends IconClassProps {
  children?: React.ReactElement | React.ReactSVGElement
  /** Occupy designated space even if no icon is provided */
  preserveSpace?: boolean
  /** It's size, matches text size */
  size?: 'xs' | 'sm' | 'base' | 'lg' | 'xl' | 'xxl'
  url?: string
}

const Icon = ({
  className,
  children,
  color,
  flipX,
  flipY,
  rotate,
  preserveSpace = true,
  size,
  spin,
  url,
}: IconProps): React.ReactElement | null => {
  const { svg: asyncSvg, error, loading } = useAsyncSvg(url)

  const classes = useMemo(
    () => makeIconClasses({ className, color, flipX, flipY, rotate, spin }),
    [className, color, flipX, flipY, rotate, spin],
  )

  const dimensions = useMemo(() => {
    if (size === 'xs') return Constants.IconSize.Xs
    if (size === 'sm') return Constants.IconSize.Sm
    if (size === 'lg') return Constants.IconSize.Lg
    if (size === 'xl') return Constants.IconSize.Xl
    if (size === 'xxl') return Constants.IconSize.Xxl
    return Constants.IconSize.Base
  }, [size])

  const icon = asyncSvg || children

  if (error) {
    console.warn(`Atalaya <Icon /> error when fetching from url: ${url}`, error)
  }

  if (loading || (!icon && preserveSpace))
    return <svg className={classes} height={dimensions} width={dimensions} />

  if (React.isValidElement(icon)) {
    return React.cloneElement(icon, {
      className: classes,
      fill: 'currentColor',
      height: dimensions,
      size: dimensions,
      // @ts-ignore
      stroke: icon.props?.stroke || 'currentColor',
      // @ts-ignore
      strokeWidth: icon.props?.strokeWidth || '0',
      width: dimensions,
      style: { color: color && color[0] === '#' ? color : undefined },
    } as IconProps)
  }

  return null
}

Icon.displayName = 'Icon'

export { Icon }
