import classNameParser from 'classnames'

import { TextColor } from '../../../layout/types'
import { SemanticVariant } from '../../../constants'

export interface IconClassProps {
  className?: string
  /** Icon color */
  color?: TextColor | SemanticVariant | string
  /** Flip icon horizontally */
  flipX?: boolean
  /** Flip icon Vertically */
  flipY?: boolean
  /** Rotate icon by 0°, 90°, 180° and 270° respectively */
  rotate?: '0/4' | '1/4' | '2/4' | '3/4'
  /** Icon will spin */
  spin?: boolean
}

export const makeIconClasses = ({
  className,
  color,
  flipX,
  flipY,
  rotate,
  spin,
}: IconClassProps): string | undefined => {
  let colorClass

  if (color === 'default') {
    colorClass = 'text-color'
  } else if (color && color[0] !== '#') {
    colorClass = `text-color-${color}`
  }

  let rotation
  if (rotate === '1/4') rotation = 'rotate-90'
  if (rotate === '2/4') rotation = 'rotate-180'
  if (rotate === '3/4') rotation = '-rotate-90'

  return classNameParser(
    'inline',
    className,
    colorClass,
    {
      'animate-spin': spin,
      'transform transform-gpu': rotation || flipX || flipY,
      flipX,
      flipY,
    },
    rotation,
  )
}

/* PurgeCSS:
text-color-subtle text-color text-color-strong text-color-semantic text-color-main text-color-neutral text-color-positive text-color-caution text-color-critical text-color-info
*/
