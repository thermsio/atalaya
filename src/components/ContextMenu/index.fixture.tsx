import React from 'react'
import { ContextMenu } from './index'

const selectOptions = new Array(5).fill(null).map((value, index) => ({
  value: index + 1,
  label: `Record Number ${index + 1}`,
}))

const base = (
  <ContextMenu>
    {selectOptions.map((option, index) => (
      <ContextMenu.Option
        disabled={index === 0}
        onClick={() => alert('onClick called')}
        key={option.value}
      >
        {option.label}
      </ContextMenu.Option>
    ))}
  </ContextMenu>
)

export default { base }
