import React from 'react'
import classnames from 'classnames'
import { useContextMenuContext } from '../context/ContextMenuContext'
import './MenuOptions.css'

export interface MenuOptionProps extends React.HTMLAttributes<HTMLDivElement> {
  children: React.ReactNode
  closeOnClick?: boolean
  disabled?: boolean
  onClick: () => void
}

const MenuOption = ({
  children,
  closeOnClick,
  disabled,
  onClick,
  ...props
}: MenuOptionProps): React.ReactElement => {
  const { closeOnClick: contextCloseOnClick, handleClose } =
    useContextMenuContext()
  const _closeOnClick = closeOnClick ?? contextCloseOnClick

  const handleClick = disabled
    ? undefined
    : () => {
        if (!disabled) {
          onClick()
          if (_closeOnClick) handleClose()
        }
      }

  return (
    <div
      {...props}
      className={classnames(`context-menu-option`, {
        'context-menu-disabled-option': disabled,
      })}
      onClick={handleClick}
      tabIndex={0}
    >
      {children}
    </div>
  )
}

export { MenuOption }
