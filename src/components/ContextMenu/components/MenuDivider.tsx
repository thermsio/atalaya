import React from 'react'

const MenuDivider = (): React.ReactElement => {
  return <div className="mx-sm my-xxs h-px bg-border" />
}

export { MenuDivider }
