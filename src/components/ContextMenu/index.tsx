import React, { useState } from 'react'
import { Button } from '../Button'
import { Icon } from '../Icon'
import { Dropdown } from '../Dropdown'
import { MenuDivider } from './components/MenuDivider'
import { MenuOption } from './components/MenuOption'
import { ContextMenuContextProvider } from './context/ContextMenuContext'
import './ContextMenu.css'

export interface ContextMenuProps {
  children?: React.ReactNode
  closeOnClick?: boolean
  controller?:
    | React.ReactNode
    | ((params: {
        close: () => void
        isOpen: boolean
        open: () => void
      }) => React.ReactNode)
  controllerPlace?: 'top-left' | 'top-right' | 'bottom-right' | 'bottom-left'
  dropdownPlace?: 'top-left' | 'top-right' | 'bottom-right' | 'bottom-left'
}

const ContextMenu = ({
  children,
  closeOnClick = true,
  controller,
  controllerPlace,
  dropdownPlace = 'bottom-left',
}: ContextMenuProps): React.ReactElement | null => {
  const [isOpen, setIsOpen] = useState(false)

  if (!React.Children.count(children)) return null

  return (
    <ContextMenuContextProvider
      closeOnClick={closeOnClick}
      handleClose={() => setIsOpen(false)}
    >
      <Dropdown
        controller={
          controller || (
            <Button subtle>
              <Icon>
                <svg
                  className="h-6 w-6"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M10 6a2 2 0 110-4 2 2 0 010 4zM10 12a2 2 0 110-4 2 2 0 010 4zM10 18a2 2 0 110-4 2 2 0 010 4z" />
                </svg>
              </Icon>
            </Button>
          )
        }
        controllerPlace={controllerPlace}
        dropdownContainerClass="context-menu-container"
        dropdownPlace={dropdownPlace}
        isOpen={isOpen}
        onClose={() => setIsOpen(false)}
        onOpen={() => setIsOpen(true)}
      >
        {children}
      </Dropdown>
    </ContextMenuContextProvider>
  )
}

ContextMenu.Divider = MenuDivider
ContextMenu.Option = MenuOption

export { ContextMenu }
