import React, { useContext } from 'react'

export interface ContextMenuContext {
  closeOnClick: boolean
  handleClose: () => void
}

export const ContextMenuContext = React.createContext<ContextMenuContext>({
  closeOnClick: true,
  handleClose: () => undefined,
})

interface ContextMenuContextProviderProps {
  children?: React.ReactNode
  closeOnClick?: boolean
  handleClose: () => void
}

export const ContextMenuContextProvider = ({
  children,
  closeOnClick = true,
  handleClose,
}: ContextMenuContextProviderProps) => {
  return (
    <ContextMenuContext.Provider value={{ closeOnClick, handleClose }}>
      {children}
    </ContextMenuContext.Provider>
  )
}

export const useContextMenuContext = (opts: { ctxRequired?: boolean } = {}) => {
  const ctx = useContext(ContextMenuContext)

  if (opts.ctxRequired && !ctx) {
    throw new Error('context is required for useContextMenuContext()')
  }

  return ctx
}
