### Simple, just options

```jsx
<ContextMenu>
  <ContextMenu.Option onClick={() => alert('clicked Option 1')}>
    Option 1
  </ContextMenu.Option>
  <ContextMenu.Option onClick={() => alert('clicked Option 2')}>
    Option 2
  </ContextMenu.Option>
  <ContextMenu.Option onClick={() => alert('clicked Option 3')}>
    Option 3
  </ContextMenu.Option>
  <ContextMenu.Option onClick={() => alert('clicked Option 4')}>
    Option 4
  </ContextMenu.Option>
  <ContextMenu.Option onClick={() => alert('clicked Option 5')}>
    Option 5
  </ContextMenu.Option>
</ContextMenu>
```

### Complex, options and dividers

```jsx
<ContextMenu>
  <ContextMenu.Option onClick={() => alert('clicked Option 1')}>
    <div className="px-base">
      <p className="text-sm">Signed in as</p>
      <p className="truncate text-sm font-medium text-color-subtle">
        tom@example.com
      </p>
    </div>
  </ContextMenu.Option>

  <ContextMenu.Divider />

  <ContextMenu.Option onClick={() => alert('clicked Option 2')}>
    <div className="flex items-center">
      <svg
        className="text-gray-400 group-hover:text-gray-500 mr-3 h-5 w-5"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 20 20"
        fill="currentColor"
        aria-hidden="true"
      >
        <path d="M17.414 2.586a2 2 0 00-2.828 0L7 10.172V13h2.828l7.586-7.586a2 2 0 000-2.828z" />
        <path
          fillRule="evenodd"
          d="M2 6a2 2 0 012-2h4a1 1 0 010 2H4v10h10v-4a1 1 0 112 0v4a2 2 0 01-2 2H4a2 2 0 01-2-2V6z"
          clipRule="evenodd"
        />
      </svg>
      <span>Edit</span>
    </div>
  </ContextMenu.Option>

  <ContextMenu.Option onClick={() => alert('clicked Option 3')}>
    <div className="flex items-center">
      <svg
        className="text-gray-400 group-hover:text-gray-500 mr-3 h-5 w-5"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 20 20"
        fill="currentColor"
        aria-hidden="true"
      >
        <path d="M7 9a2 2 0 012-2h6a2 2 0 012 2v6a2 2 0 01-2 2H9a2 2 0 01-2-2V9z" />
        <path d="M5 3a2 2 0 00-2 2v6a2 2 0 002 2V5h8a2 2 0 00-2-2H5z" />
      </svg>
      Duplicate
    </div>
  </ContextMenu.Option>

  <ContextMenu.Divider />

  <ContextMenu.Option onClick={() => alert('clicked Option 4')}>
    <div className="flex items-center">
      <svg
        className="text-gray-400 group-hover:text-gray-500 mr-3 h-5 w-5"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 20 20"
        fill="currentColor"
        aria-hidden="true"
      >
        <path d="M8 9a3 3 0 100-6 3 3 0 000 6zM8 11a6 6 0 016 6H2a6 6 0 016-6zM16 7a1 1 0 10-2 0v1h-1a1 1 0 100 2h1v1a1 1 0 102 0v-1h1a1 1 0 100-2h-1V7z" />
      </svg>
      Share
    </div>
  </ContextMenu.Option>

  <ContextMenu.Option onClick={() => alert('clicked Option 5')}>
    <div className="flex items-center">
      <svg
        className="text-gray-400 group-hover:text-gray-500 mr-3 h-5 w-5"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 20 20"
        fill="currentColor"
        aria-hidden="true"
      >
        <path
          fillRule="evenodd"
          d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
          clipRule="evenodd"
        />
      </svg>
      Add to favorites
    </div>
  </ContextMenu.Option>
</ContextMenu>
```

### Absolute Position

```jsx
<div className="relative h-36 w-full rounded bg-surface">
  <ContextMenu controllerPlace="top-left">
    <ContextMenu.Option onClick={() => alert('clicked Option 1')}>
      Option 1
    </ContextMenu.Option>
  </ContextMenu>

  <ContextMenu controllerPlace="top-right">
    <ContextMenu.Option onClick={() => alert('clicked Option 1')}>
      Option 1
    </ContextMenu.Option>
  </ContextMenu>

  <ContextMenu controllerPlace="bottom-right">
    <ContextMenu.Option onClick={() => alert('clicked Option 1')}>
      Option 1
    </ContextMenu.Option>
  </ContextMenu>

  <ContextMenu controllerPlace="bottom-left">
    <ContextMenu.Option onClick={() => alert('clicked Option 1')}>
      Option 1
    </ContextMenu.Option>
  </ContextMenu>
</div>
```

### Dropdown Position

```jsx
<div className="flex space-x-base">
  <ContextMenu dropdownPlace="top-left">
    <ContextMenu.Option onClick={() => alert('clicked Option 1')}>
      Option 1
    </ContextMenu.Option>
  </ContextMenu>

  <ContextMenu dropdownPlace="top-right">
    <ContextMenu.Option onClick={() => alert('clicked Option 1')}>
      Option 1
    </ContextMenu.Option>
  </ContextMenu>

  <ContextMenu dropdownPlace="bottom-right">
    <ContextMenu.Option onClick={() => alert('clicked Option 1')}>
      Option 1
    </ContextMenu.Option>
  </ContextMenu>

  <ContextMenu dropdownPlace="bottom-left">
    <ContextMenu.Option onClick={() => alert('clicked Option 1')}>
      Option 1
    </ContextMenu.Option>
  </ContextMenu>
</div>
```

### Close on Click

By default, after selecting an option context menu will close, you can prevent this behaviour by setting the prop `closeOnClick` to false. `ContextMenu.Option` also has an `closeOnClick` prop which will override the context menu option if specified.

```jsx
<ContextMenu closeOnClick={false}>
  <ContextMenu.Option onClick={() => alert('clicked Option 1')}>
    Clicking me won't close menu
  </ContextMenu.Option>
  <ContextMenu.Option onClick={() => alert('clicked Option 2')}>
    Clicking me won't close menu
  </ContextMenu.Option>
  <ContextMenu.Option
    closeOnClick={true}
    onClick={() => alert('clicked Option 3')}
  >
    Clicking me WILL close menu
  </ContextMenu.Option>
</ContextMenu>
```

### Custom Controller

By supplying an element to `controller` you can use a custom controller to open ContextMenu

```jsx
import { Button } from '../Button'
;<ContextMenu controller={<Button variant="main">Open Menu</Button>}>
  <ContextMenu.Option onClick={() => alert('clicked Option 1')}>
    Option 1
  </ContextMenu.Option>
  <ContextMenu.Option onClick={() => alert('clicked Option 2')}>
    Option 2
  </ContextMenu.Option>
  <ContextMenu.Option onClick={() => alert('clicked Option 3')}>
    Option 3
  </ContextMenu.Option>
  <ContextMenu.Option onClick={() => alert('clicked Option 4')}>
    Option 4
  </ContextMenu.Option>
  <ContextMenu.Option onClick={() => alert('clicked Option 5')}>
    Option 5
  </ContextMenu.Option>
</ContextMenu>
```
