A simple wrapper that provides a default styling that simulate a content card.

It provides sensible defaults for `background`, `border-radius`, `padding`
and `spacing` but they can also be overridden to suit your needs.

```jsx
<Card>
  <div className="text-lg font-bold">Lorem Ipsum</div>
  <div>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
    tempor incididunt ut
    labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
    exercitation ullamco
    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
    reprehenderit in
    voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
    occaecat cupidatat
    non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
  </div>
  <div className="italic text-color-subtle">
    Sapien et ligula ullamcorper malesuada proin libero nunc consequat interdum.
  </div>
</Card>
```
