import React, { SyntheticEvent } from 'react'
import {
  Background,
  BorderColor,
  BorderWidth,
  Radius,
  Spacing,
} from '../../layout/types'
import { Stack } from '../../layout/Stack'
import classNameParser from 'classnames'

export interface CardProps extends React.HTMLAttributes<HTMLDivElement> {
  /** `background | surface | surface-strong | surface-subtle` */
  background?: Background
  borderColor?: BorderColor
  border?: BorderWidth
  borderBottom?: BorderWidth
  borderLeft?: BorderWidth
  borderRight?: BorderWidth
  borderTop?: BorderWidth
  /** `none | sm | default | md | lg | xl | xxl | 3xl | full` */
  borderRadius?: Radius
  className?: string
  children: React.ReactNode
  onClick?: (e: SyntheticEvent) => void
  /** `xxs | xs | sm | base | lg | xl | xxl` */
  padding?: Spacing
  /**
   *  Space between elements<br>
   *  `xxs | xs | sm | base | lg | xl | xxl`
   * */
  space?: Spacing
}

const Card = ({
  background = 'surface',
  border,
  borderColor,
  borderBottom,
  borderLeft,
  borderRight,
  borderTop,
  borderRadius = 'default',
  children,
  className,
  onClick,
  padding = 'base',
  space = 'sm',
  ...props
}: CardProps): React.ReactElement => {
  return (
    <Stack
      background={background}
      border={border}
      borderBottom={borderBottom}
      borderColor={borderColor}
      borderLeft={borderLeft}
      borderRight={borderRight}
      borderTop={borderTop}
      borderRadius={borderRadius}
      className={classNameParser(
        {
          'cursor-pointer transition-colors hover:bg-surface-strong active:bg-surface-subtle':
            !!onClick,
        },
        className,
      )}
      onClick={onClick}
      padding={padding}
      space={space}
      {...props}
    >
      {children}
    </Stack>
  )
}

export { Card }
