import React from 'react'
import { TagGroup } from './index'
import { Tag } from '../Tag'

const tags = (
  <TagGroup>
    <Tag value="Example" />
    <Tag value="Example" />
    <Tag value="Example" />
    <Tag value="Example" />
  </TagGroup>
)

const largeTags = (
  <TagGroup lg>
    <Tag value="Example" />
    <Tag value="Example" />
    <Tag value="Example" />
    <Tag value="Example" />
  </TagGroup>
)

export default { largeTags, tags }
