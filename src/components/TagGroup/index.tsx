import React, { useMemo } from 'react'

import { Inline } from '../../layout/Inline'
import { Tag } from '../Tag'
import { Alignment, Justification, Spacing } from '../../layout/types'

export interface TagGroupProps {
  alignY?: Alignment
  alignX?: Justification | Justification[]
  max?: number
  lg?: boolean
  space?: Spacing
  children: React.ReactNode
  wrap?: boolean
}

const TagGroup = ({
  alignX,
  alignY = 'end',
  max = 0,
  lg,
  space = 'xxs',
  children,
  wrap = true,
}: TagGroupProps): React.ReactElement => {
  const excessTags = max ? React.Children.count(children) - max : 0
  const tagList = useMemo(
    () =>
      React.Children.map(children, (child, index) => {
        if ((max && index + 1 > max) || !React.isValidElement(child)) return
        return React.cloneElement(child, { lg } as any)
      }),
    [children, lg],
  )

  return (
    <Inline space={space} alignX={alignX} alignY={alignY} wrap={wrap}>
      {tagList}
      {excessTags > 0 && <Tag value={`+${excessTags}`} />}
    </Inline>
  )
}

export { TagGroup }
