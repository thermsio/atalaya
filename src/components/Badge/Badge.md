Badges are only meant to display numbers, essentially displaying the amount of
certain item. Since badges don't have any text, it's important to place them in
a context that makes it obvious what they are quantifying. Most of the time they
will be right beside the item name.

The other aspect of a badge is their semantic meaning. Picking the
correct `variant` will change it's background color to their respective semantic
color. This, paired with the item's name, gives the user enough information to
determine the urgency of any such item.

```jsx
<div className="flex space-x-xs text-color">
  <Badge variant="main" value={1} />
  <Badge variant="neutral" value={42} max={9} />
  <Badge variant="positive" value={42} />
  <Badge variant="caution" value={150} />
  <Badge variant="critical" value={10} />
  <Badge variant="info" value={42} max={9} />
</div>
```

`color` prop will let you add any CSS valid color value, it takes precedence over `variant` and `subtle`.

```jsx
<div className="flex space-x-xs text-color">
  <Badge color="rgb(148 219 226)" value={1} />
  <Badge color="hsl(139deg 77% 49%)" value={42} max={9} />
  <Badge color="#890312" value={42} />
  <Badge color="#fff" value={150} />
  <Badge color="#00111abf" value={10} />
</div>
```

### Subtle

```jsx
<div className="flex space-x-xs text-color">
  <Badge subtle variant="main" value={1} />
  <Badge subtle variant="neutral" value={42} max={9} />
  <Badge subtle variant="positive" value={42} />
  <Badge subtle variant="caution" value={150} />
  <Badge subtle variant="critical" value={10} />
  <Badge subtle variant="info" value={42} max={9} />
</div>
```
