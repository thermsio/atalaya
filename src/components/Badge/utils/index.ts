import classNames from 'classnames'
import { SemanticVariant } from '../../../constants'

export interface BadgeClassProps {
  subtle?: boolean
  /** Semantic meaning of the badge */
  variant?: SemanticVariant
}

export const makeBadgeClasses = ({
  subtle,
  variant,
}: BadgeClassProps): string => {
  return classNames('badge ', {
    'badge-main': variant === 'main',
    'badge-neutral': variant === 'neutral',
    'badge-positive': variant === 'positive',
    'badge-caution': variant === 'caution',
    'badge-critical': variant === 'critical',
    'badge-info': variant === 'info',
    'badge-subtle': subtle,
  })
}
