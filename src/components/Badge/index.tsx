import React, { useMemo } from 'react'
import { BadgeClassProps, makeBadgeClasses } from './utils'

import './Badge.css'
import { getColorContrast } from '../../utils/getColorContrast'

export interface BadgeProps extends BadgeClassProps {
  /** Background color of the badge, takes precedence over variant and subtle */
  color?: string
  /** Highest number to display, if value is higher than max a "+" will be prefixed  */
  max?: number
  /** Number to be displayed on the badge */
  value: number | string | undefined
}

const Badge = ({
  color,
  max = 99,
  subtle,
  variant = 'neutral',
  value,
}: BadgeProps): React.ReactElement | null => {
  const classes = useMemo(
    () => makeBadgeClasses({ subtle, variant }),
    [subtle, variant],
  )

  const displayValue =
    typeof value === 'number' && value > max ? `+${max}` : value

  if (!displayValue || displayValue === '0') return null

  if (!color) return <div className={classes}>{displayValue}</div>

  return (
    <div
      className="badge"
      style={{
        backgroundColor: color,
        color: getColorContrast(color),
      }}
    >
      {displayValue}
    </div>
  )
}

export { Badge }
