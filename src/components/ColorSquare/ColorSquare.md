`variant` prop to will let you select between semantic colors.

```jsx
<div className="space-x-xs">
  <ColorSquare variant="main" />
  <ColorSquare variant="neutral" />
  <ColorSquare variant="positive" />
  <ColorSquare variant="caution" />
  <ColorSquare variant="critical" />
  <ColorSquare variant="info" />
</div>
```

`color` prop will let you add any CSS valid color value, it takes precedence over `variant`.

```jsx
<div className="space-x-xs">
  <ColorSquare color="rgb(148 219 226)" />
  <ColorSquare color="hsl(139deg 77% 49%)" />
  <ColorSquare color="#890312" />
</div>
```

### Small

```jsx
<div className="space-x-xs">
  <ColorSquare size="sm" variant="main" />
  <ColorSquare size="sm" variant="neutral" />
  <ColorSquare size="sm" variant="positive" />
  <ColorSquare size="sm" variant="caution" />
  <ColorSquare size="sm" variant="critical" />
  <ColorSquare size="sm" variant="info" />
</div>
```
