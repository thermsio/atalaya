import React from 'react'
import classnames from 'classnames'
import { SemanticVariant } from '../../constants'

export interface ColorSquareProps {
  color?: string
  marginLeft?: boolean
  marginRight?: boolean
  /* If no color passed or is undefined it will still use the space */
  preserveSpace?: boolean
  size?: 'sm'
  variant?: SemanticVariant
}

const ColorSquare = ({
  color,
  marginLeft,
  marginRight,
  preserveSpace = true,
  size,
  variant,
}: ColorSquareProps): React.ReactElement | null => {
  if (!color && !variant && !preserveSpace) return null

  return (
    <span
      className={classnames(`inline-block`, {
        'ml-xs': marginLeft,
        'mr-xs': marginRight,
        'pb-sm pr-sm': !size,
        'pb-xs pr-xs': size === 'sm',
        [`bg-${variant}`]: !color && variant,
      })}
      style={{
        backgroundColor: color,
        borderRadius: '1px',
      }}
    />
  )
}

ColorSquare.displayName = 'ColorSquare'

export { ColorSquare }

// Purge CSS

// bg-main bg-neutral bg-caution bg-critical bg-info
