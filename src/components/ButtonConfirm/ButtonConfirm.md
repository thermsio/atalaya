```jsx
<ButtonConfirm onClick={() => console.log('You have confirmed')} variant="main">
  Take Action
</ButtonConfirm>
```

### Different variant confirmation

```jsx
<ButtonConfirm
  confirmationVariant="critical"
  onClick={() => console.log('You have confirmed')}
  variant="main"
>
  Take Action
</ButtonConfirm>
```
