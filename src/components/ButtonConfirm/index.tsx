import React, { useEffect, useState } from 'react'
import { Button, ButtonProps } from '../Button'
import { Icon } from '../Icon'
import { SemanticVariant } from '../../constants'

export interface ButtonConfirmProps extends ButtonProps {
  confirmationMessage?: React.ReactNode
  /** If you want the button's variant to change when is asking for confirmation */
  confirmationVariant?: SemanticVariant
  /** How long until the button closes the confirmation window */
  timeout?: number
}

const defaultMessage = (
  <div className="flex animate-pulse items-center">
    <Icon size="sm">
      <svg
        xmlns="http://www.w3.org/2000/svg"
        height="24px"
        viewBox="0 0 24 24"
        width="24px"
        fill="currentColor"
      >
        <path d="M0 0h24v24H0V0z" fill="none" />
        <circle cx="12" cy="19" r="2" />
        <path d="M10 3h4v12h-4z" />
      </svg>
    </Icon>
    <span>Click to confirm</span>
  </div>
)

const ButtonConfirm = ({
  children,
  confirmationMessage = defaultMessage,
  confirmationVariant,
  onClick,
  timeout = 5000,
  ...buttonProps
}: ButtonConfirmProps) => {
  const [isConfirming, setIsConfirming] = useState(false)

  const handleClick = (e) => {
    if (!isConfirming) {
      setIsConfirming(true)
    } else if (onClick) {
      onClick(e)
    }
  }

  useEffect(() => {
    if (isConfirming) {
      const confirmingTimeout = setTimeout(
        () => setIsConfirming(false),
        timeout,
      )
      return () => clearTimeout(confirmingTimeout)
    }
  }, [isConfirming, timeout])

  return (
    <Button
      {...buttonProps}
      onClick={handleClick}
      variant={
        confirmationVariant && isConfirming
          ? confirmationVariant
          : buttonProps.variant
      }
    >
      {isConfirming ? confirmationMessage : children}
    </Button>
  )
}

export { ButtonConfirm }
