This component is meant to be used as a wrapper around a list of elements. It will arrange them horizontally without
exceeding its container's max width if there is not enough room to display all items it will provide navigation arrows
for the user to scroll with. HorizontalScroll only hides the scrollbar, users are still able to scroll the container
using the mouse wheel, swipe gestures or trackpad in addition to using the arrows.

```jsx
const items = new Array(50).fill(null).map((value, index) => index + 1)

;<HorizontalScroll>
  {items.map((item) => (
    <div className="space-y-xs rounded bg-surface p-sm" key={item}>
      <p className="text-center text-xl text-color-subtle">{item}</p>
      <p className="text-center text-sm">
        This is the #{item} item on the list.
      </p>
    </div>
  ))}
</HorizontalScroll>
```

If `itemWidth` is defined, each item will be assigned that max width.

```jsx
const items = new Array(50).fill(null).map((value, index) => index + 1)

;<HorizontalScroll itemWidth={100}>
  {items.map((item) => (
    <div className="space-y-xs rounded bg-surface p-sm" key={item}>
      <p className="text-center text-xl text-color-subtle">{item}</p>
      <p className="text-center text-sm">
        This is the #{item} item on the list.
      </p>
    </div>
  ))}
</HorizontalScroll>
```
