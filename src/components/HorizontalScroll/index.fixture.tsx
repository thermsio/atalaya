import React from 'react'
import { HorizontalScroll } from './index'

const items = new Array(50).fill(null).map((value, index) => index + 1)

const base = (
  <HorizontalScroll>
    {items.map((item) => (
      <div className="space-y-xs rounded bg-surface p-sm" key={item}>
        <p className="text-center text-xl text-color-subtle">{item}</p>
        <p className="text-center text-sm">
          This is the #{item} item on the list.
        </p>
      </div>
    ))}
  </HorizontalScroll>
)

export default { base }
