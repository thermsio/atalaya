import React, { useEffect, useMemo } from 'react'
import './HorizontalScroll.css'
import { useElementSize } from '../../hooks/useElementSize'
import { Button } from '../Button'
import { Icon } from '../Icon'
import { useStateThrottle } from '../../hooks/useStateThrottle'
import classNames from 'classnames'
import { Spacing } from '../../layout/types'

interface HorizontalScrollProps {
  children?: React.ReactNode
  /** Divides available space to make fix X amount of items at the same time */
  itemsInView?: number
  /** Width of each item in the list */
  itemWidth?: string | number
  space?: Spacing
  alignX?: 'start' | 'center' | 'end'
}

const HorizontalScroll: React.FC<HorizontalScrollProps> = ({
  alignX = 'start',
  children,
  itemsInView = 5,
  itemWidth,
  space = 'base',
}) => {
  const [{ width }, setRef, ref] = useElementSize<HTMLDivElement>()

  const maxScrollLeft = useMemo(() => {
    if (ref) return ref.scrollWidth - ref.clientWidth
  }, [ref])

  const [, scrollLeftDebounced, setScrollLeft] = useStateThrottle(0, 300)

  useEffect(() => {
    if (ref) {
      const scrollHandler = (event) => {
        const eventTarget = event.target as HTMLElement
        setScrollLeft(eventTarget.scrollLeft)
      }

      ref.addEventListener('scroll', scrollHandler)

      // Remove event listener on cleanup
      return () => {
        ref.removeEventListener('scroll', scrollHandler)
      }
    }
  }, [setScrollLeft, ref])

  return (
    <div className="relative">
      <div
        className={classNames(
          {
            'mx-auto': alignX === 'center',
            'ml-auto': alignX === 'end',
          },
          `horizontal-scroll-container gap-${space} max-w-max`,
        )}
        style={{ gridAutoColumns: itemWidth || `${100 / itemsInView}%` }}
        ref={setRef}
      >
        {children}
      </div>

      <div
        className={classNames(
          {
            'pointer-events-none opacity-0':
              !maxScrollLeft || !scrollLeftDebounced,
          },
          'absolute inset-y-0 left-0 w-16 bg-gradient-to-r from-background from-30% transition-opacity duration-300',
        )}
      />
      <div
        className={classNames(
          {
            'pointer-events-none opacity-0':
              !maxScrollLeft || scrollLeftDebounced === maxScrollLeft,
          },
          '50% absolute inset-y-0 right-0 w-16 bg-gradient-to-l from-background from-30% transition-opacity duration-300',
        )}
      />

      <Button
        borderRadius="full"
        className={`absolute left-0 top-1/2 -translate-y-1/2 transition-opacity duration-300 hover:opacity-80 focus:opacity-80 ${
          scrollLeftDebounced ? 'opacity-60' : 'pointer-events-none opacity-0'
        } `}
        onClick={() => ref?.scrollBy(-width, 0)}
        size="lg"
      >
        <Icon>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            height="24px"
            viewBox="0 0 24 24"
            width="24px"
            fill="currentColor"
          >
            <path d="M0 0h24v24H0V0z" fill="none" />
            <path d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12l4.58-4.59z" />
          </svg>
        </Icon>
      </Button>

      <Button
        borderRadius="full"
        className={`absolute right-0 top-1/2 -translate-y-1/2 transition-opacity duration-300 hover:opacity-80 focus:opacity-80 ${
          maxScrollLeft === scrollLeftDebounced
            ? 'pointer-events-none opacity-0'
            : 'opacity-60'
        }`}
        size="lg"
        onClick={() => ref?.scrollBy(width, 0)}
      >
        <Icon>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            height="24px"
            viewBox="0 0 24 24"
            width="24px"
            fill="currentColor"
          >
            <path d="M0 0h24v24H0V0z" fill="none" />
            <path d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6-6-6z" />
          </svg>
        </Icon>
      </Button>
    </div>
  )
}

export { HorizontalScroll }

/* Purge CSS:
 gap-0 gap-2xs gap-xxs gap-xs gap-sm gap-base gap-lg gap-xl gap-xxl gap-2xl
*/
