import React, {
  useContext,
  useEffect,
  useMemo,
  useReducer,
  useRef,
} from 'react'

export interface ModalContextValue {
  addIdToStack: (modalId: string) => void
  modalId: string
  /** modalsIds will appear in the order they were opened, required for knowing which modal to close when esc key is pressed. */
  modalIdsLIFOStack: string[]
  closeHandler: () => void
  hasParentModal: boolean
  removeIdFromStack: (modalId: string) => void
  setHeader?: (header?: React.ReactNode) => void
}

export const ModalContext = React.createContext<ModalContextValue | undefined>(
  undefined,
)

interface ModalContextProviderProps {
  modalId: string
  children: React.ReactNode
  closeHandler?: () => void
  isParentModalCtx?: boolean
  setHeader?: ModalContextValue['setHeader']
}

const LIFOReducer = (
  state: string[],
  payload: { modalId: string; action: 'add' | 'remove' },
) => {
  if (payload.action === 'add') {
    return [...state, payload.modalId]
  } else if (payload.action === 'remove') {
    return state.filter((id) => id !== payload.modalId)
  } else {
    return state
  }
}

export const ModalContextProvider = ({
  modalId,
  children,
  closeHandler,
  setHeader,
}: ModalContextProviderProps) => {
  const local = useRef({ closeHandler })
  const [localModalIdsLIFOStack, dispatchLocalModalIdsLIFOStack] = useReducer(
    LIFOReducer,
    [modalId],
  )

  const parentModalCtx = useContext(ModalContext)

  useEffect(() => {
    if (parentModalCtx) {
      parentModalCtx.addIdToStack(modalId)
      return () => parentModalCtx.removeIdFromStack(modalId)
    }
  }, [])

  local.current.closeHandler = closeHandler || parentModalCtx?.closeHandler

  const value = useMemo(() => {
    return {
      addIdToStack:
        parentModalCtx?.addIdToStack ||
        ((_modalId) => {
          return dispatchLocalModalIdsLIFOStack({
            modalId: _modalId,
            action: 'add',
          })
        }),
      // prevent loops when an inline closeHandler func prop is passed
      closeHandler: () => local.current?.closeHandler?.(),
      hasParentModal: !!parentModalCtx,
      modalId,
      modalIdsLIFOStack:
        parentModalCtx?.modalIdsLIFOStack || localModalIdsLIFOStack,
      removeIdFromStack:
        parentModalCtx?.removeIdFromStack ||
        ((_modalId) => {
          return dispatchLocalModalIdsLIFOStack({
            modalId: _modalId,
            action: 'remove',
          })
        }),
      setHeader,
    }
  }, [
    closeHandler,
    modalId,
    localModalIdsLIFOStack,
    parentModalCtx?.addIdToStack,
    parentModalCtx?.modalIdsLIFOStack,
    parentModalCtx?.removeIdFromStack,
    setHeader,
  ])

  return <ModalContext.Provider value={value}>{children}</ModalContext.Provider>
}

export const useModalContext = (opts: { ctxRequired?: boolean } = {}) => {
  const ctx = useContext(ModalContext)

  if (opts.ctxRequired && !ctx) {
    throw new Error('context is required for useModalContext()')
  }

  return ctx
}
