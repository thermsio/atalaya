import React, { useEffect, useRef, useState } from 'react'
import { Modal } from './Modal'
import { Button } from '../Button'
import { ColorPicker } from '../FormControls/ColorPicker/ColorPicker'
import { Select } from '../FormControls/Select/Select'
import { Loading } from '../Loading'
import { Checkbox } from '../FormControls/Checkbox/Checkbox'
import { useModalContext } from './context/ModalContext'
import { Text } from '../FormControls/Text/Text'
import { ButtonWithModal } from '../ButtonWithModal/ButtonWithModal'

const Base = () => {
  const [show, setShow] = useState(false)
  const [color, setColor] = useState('')

  return (
    <div>
      <Button onClick={() => setShow(true)}>Show Modal</Button>

      {show && (
        <Modal
          closeHandler={() => setShow(false)}
          escPressClose
          outsideClickClose
          header="Lorem Ipsum"
        >
          <div>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat.
          </div>

          <ColorPicker value={color} onChangeValue={setColor} />

          <Select
            options={new Array(30).fill(null).map((value, index) => {
              return { label: index, value: index }
            })}
          />
        </Modal>
      )}
    </div>
  )
}

const LongContentScroll = () => {
  const [show, setShow] = useState(false)
  const [color, setColor] = useState('')

  return (
    <div>
      <Button onClick={() => setShow(true)}>Show Modal</Button>

      {show && (
        <Modal
          closeHandler={() => setShow(false)}
          escPressClose
          header="Lorem Ipsum"
        >
          {new Array(20).fill(null).map((_, i) => (
            <div key={i}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat.
            </div>
          ))}

          <ColorPicker value={color} onChangeValue={setColor} />
        </Modal>
      )}
    </div>
  )
}

const LongContentScrollNoHeader = () => {
  const [show, setShow] = useState(false)
  const [color, setColor] = useState('')

  return (
    <div>
      <Button onClick={() => setShow(true)}>Show Modal</Button>

      {show && (
        <Modal closeHandler={() => setShow(false)} showClose={false}>
          {new Array(20).fill(null).map((_, i) => (
            <div key={i}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat.
            </div>
          ))}

          <ColorPicker value={color} onChangeValue={setColor} />
        </Modal>
      )}
    </div>
  )
}

const ModalInsideModal = () => {
  const [showFirst, setShowFirst] = useState(false)
  const [showSecond, setShowSecond] = useState(false)
  const [showThird, setShowThird] = useState(false)

  const [color, setColor] = useState('')

  return (
    <div>
      <Button onClick={() => setShowFirst(true)}>Show Modal 1</Button>

      {showFirst && (
        <Modal
          closeHandler={() => setShowFirst(false)}
          escPressClose
          header="First Modal"
        >
          <Button onClick={() => setShowSecond(true)}>Show Modal 2</Button>

          {new Array(20).fill(null).map((_, i) => (
            <div key={i}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat.
            </div>
          ))}

          {showSecond && (
            <Modal
              closeHandler={() => setShowSecond(false)}
              escPressClose
              header="Second Modal"
            >
              <Button onClick={() => setShowThird(true)}>Show Modal 3</Button>

              <div>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat.
              </div>

              {showThird && (
                <Modal
                  closeHandler={() => setShowThird(false)}
                  header="Third Modal"
                >
                  <div>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco laboris nisi ut aliquip ex ea commodo consequat.
                  </div>
                </Modal>
              )}
            </Modal>
          )}

          <ColorPicker value={color} onChangeValue={setColor} />
        </Modal>
      )}
    </div>
  )
}

const OverflowSelect = () => {
  const [show, setShow] = useState(false)
  const [value, setValue] = useState([])

  return (
    <div>
      <Button onClick={() => setShow(true)}>Show Modal</Button>

      {show && (
        <Modal closeHandler={() => setShow(false)} escPressClose>
          <Select
            value={value}
            onChangeValue={setValue}
            options={new Array(30).fill(null).map((value, index) => {
              return { label: index, value: index }
            })}
          />

          {new Array(20).fill(null).map((_, i) => (
            <div key={i}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat.
            </div>
          ))}

          <Select
            value={value}
            onChangeValue={setValue}
            options={new Array(30).fill(null).map((value, index) => {
              return { label: index, value: index }
            })}
          />

          <div>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          </div>

          <Select
            value={value}
            onChangeValue={setValue}
            options={new Array(30).fill(null).map((value, index) => {
              return { label: index, value: index }
            })}
          />
        </Modal>
      )}
    </div>
  )
}

const TinyOverflowSelect = () => {
  const [show, setShow] = useState(false)
  const [value, setValue] = useState([])
  const [loading, setLoading] = useState(false)

  return (
    <div>
      <Button onClick={() => setShow(true)}>Show Modal</Button>
      <Checkbox
        active={loading}
        label="Set Loading"
        onChangeValue={setLoading}
      />

      {show && (
        <Modal closeHandler={() => setShow(false)} escPressClose>
          {loading && <Loading overlay />}

          <Select
            value={value}
            onChangeValue={setValue}
            options={new Array(30).fill(null).map((value, index) => {
              return { label: index, value: index }
            })}
          />

          <ColorPicker />
        </Modal>
      )}
    </div>
  )
}

const LoneOverflowSelect = () => {
  const [show, setShow] = useState(false)
  const [value, setValue] = useState([])

  return (
    <div>
      <Button onClick={() => setShow(true)}>Show Modal</Button>

      {show && (
        <Modal closeHandler={() => setShow(false)} escPressClose>
          {new Array(20).fill(null).map((_, i) => (
            <div key={i}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat.
            </div>
          ))}

          <Select
            value={value}
            onChangeValue={setValue}
            options={new Array(30).fill(null).map((value, index) => {
              return { label: index, value: index }
            })}
          />
        </Modal>
      )}
    </div>
  )
}

const InsideModalComponent = () => {
  const modalXCtx = useModalContext()
  const [header, setHeader] = useState('Changed Header by inside component')

  useEffect(() => {
    if (modalXCtx?.setHeader) {
      modalXCtx.setHeader(header)
    }
  }, [header])

  return (
    <div className="text-color-info">
      Inside modal component
      <div>
        <Text onChangeValue={setHeader} value={header} />
      </div>
    </div>
  )
}

const ModalContextUsage = () => {
  const [show, setShow] = useState(false)

  return (
    <div>
      <Button onClick={() => setShow(true)}>Show Modal</Button>

      {show && (
        <Modal
          closeHandler={() => setShow(false)}
          escPressClose
          header="Modal Context 1"
        >
          <div>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat.
          </div>

          <InsideModalComponent />
        </Modal>
      )}
    </div>
  )
}

const ModalHeaderAndContextSetHeaderUsage = () => {
  const [show, setShow] = useState(false)

  const [header, setHeader] = useState('1')

  useEffect(() => {
    const i = setInterval(() => {
      setHeader(Math.random().toString())
    }, 1000)

    return () => clearInterval(i)
  }, [])

  return (
    <div>
      <Button onClick={() => setShow(true)}>Show Modal</Button>

      {show && (
        <Modal
          closeHandler={() => setShow(false)}
          escPressClose
          header={header}
        >
          <div>`header` state value: {header}</div>

          <div>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat.
          </div>

          <InsideModalComponent />
        </Modal>
      )}
    </div>
  )
}

const customWidth = () => {
  const [show, setShow] = useState(false)
  const [color, setColor] = useState('')

  return (
    <div>
      <Button onClick={() => setShow(true)}>Show Modal</Button>

      {show && (
        <Modal
          closeHandler={() => setShow(false)}
          escPressClose
          header="Lorem Ipsum"
          width="200px"
        >
          <div>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat.
          </div>

          <ColorPicker value={color} onChangeValue={setColor} />
        </Modal>
      )}
    </div>
  )
}

const breakpointWidth = () => {
  const [show, setShow] = useState(false)
  const [color, setColor] = useState('')

  return (
    <div>
      <Button onClick={() => setShow(true)}>Show Modal</Button>

      {show && (
        <Modal
          closeHandler={() => setShow(false)}
          escPressClose
          header="Lorem Ipsum"
          width="sm"
        >
          <div>Lorem ipsum.</div>

          <ColorPicker value={color} onChangeValue={setColor} />
        </Modal>
      )}
    </div>
  )
}

const overLongContent = () => {
  const [show, setShow] = useState(false)
  const [color, setColor] = useState('')

  return (
    <div>
      <div className="fixed z-fixed">
        <Button onClick={() => setShow(true)}>Show Modal</Button>
      </div>

      {new Array(20).fill(null).map((value, index) => (
        <p
          className="mx-auto my-sm max-w-prose text-justify"
          key={value + index}
        >
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum.
        </p>
      ))}

      {show && (
        <Modal
          closeHandler={() => setShow(false)}
          escPressClose
          header="Lorem Ipsum"
          width="sm"
        >
          <div>Lorem ipsum.</div>

          <ColorPicker value={color} onChangeValue={setColor} />
        </Modal>
      )}
    </div>
  )
}

const NestedButtonWithModal = () => {
  const [show, setShow] = useState(false)

  return (
    <div>
      <Button onClick={() => setShow(true)}>Show Modal</Button>

      {show && (
        <Modal
          closeHandler={() => setShow(false)}
          escPressClose
          outsideClickClose
          header="Lorem Ipsum"
        >
          <div>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat.
          </div>

          <ButtonWithModal
            modalElement={<Modal escPressClose>This is the nested modal</Modal>}
          >
            Open Nested Button With Modal
          </ButtonWithModal>
        </Modal>
      )}
    </div>
  )
}

const ZIndexCheck = () => {
  const [show, setShow] = useState(false)

  return (
    <div>
      <Button onClick={() => setShow(true)}>Show Modal</Button>

      {show && (
        <Modal
          closeHandler={() => setShow(false)}
          escPressClose
          outsideClickClose
          header="Lorem Ipsum"
        >
          <div>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat.
          </div>
        </Modal>
      )}

      <div className="z-tooltip">
        This element has the tooltip z-index, it should not appear in front of
        an open modal
      </div>

      <div className="z-popover">
        This element has the popover z-index, popover index is higher but since
        it does not create a new stacking context it still should be behind
        modal
      </div>

      <div className="relative z-tooltip">
        This element has the tooltip z-index and creating a new stacking
        context, it should not appear in front of an open modal because
        it&apos;s z-index is lower than Modal&apos;s
      </div>

      <div className="relative z-popover">
        This element has the popover z-index and creating a new stacking
        context, it should appear in front of an open modal.
      </div>
    </div>
  )
}

const FullScreenInteraction = () => {
  const [show, setShow] = useState(false)
  const ref = useRef<HTMLDivElement>(null)

  return (
    <>
      <div className="rounded bg-surface p-xl" ref={ref}>
        <div className="text-lg">Element that will request fullscreen.</div>

        <Button variant="main" onClick={() => ref.current?.requestFullscreen()}>
          Request FullScreen
        </Button>

        <Button onClick={() => setShow(true)}>Show Modal</Button>

        {show && (
          <Modal
            closeHandler={() => setShow(false)}
            escPressClose
            outsideClickClose
            header="Lorem Ipsum"
          >
            <div>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat.
            </div>
          </Modal>
        )}
      </div>
    </>
  )
}

const OverlayLoading = () => {
  const [show, setShow] = useState(false)

  return (
    <div>
      <Button onClick={() => setShow(true)}>Show Modal</Button>

      {show && (
        <Modal
          closeHandler={() => setShow(false)}
          escPressClose
          outsideClickClose
          header="Lorem Ipsum"
        >
          <Loading overlay />
          <div>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat.
          </div>

          <div>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat.
          </div>

          <div>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat.
          </div>

          <div>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat.
          </div>
        </Modal>
      )}
    </div>
  )
}

export default {
  Base,
  LongContentScroll,
  LongContentScrollNoHeader,
  ModalContextUsage,
  ModalHeaderAndContextSetHeaderUsage,
  ModalInsideModal,
  OverflowSelect,
  TinyOverflowSelect,
  LoneOverflowSelect,
  customWidth,
  breakpointWidth,
  overLongContent,
  NestedButtonWithModal,
  ZIndexCheck,
  FullScreenInteraction,
  OverlayLoading,
}
