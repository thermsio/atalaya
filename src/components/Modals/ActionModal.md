A specialized modal to use when a user needs to take an immediate action.

The `variant` prop will affect button and icon color.

```jsx
import { Button } from '../Button'

const [show, setShow] = React.useState(false)

;<div>
  <Button onClick={() => setShow(true)}>Show Modal</Button>

  {show && (
    <ActionModal
      actionText="Take action!"
      body="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
      onCancel={() => setShow(false)}
      onAction={() => {
        alert('The action has been taken')
        setShow(false)
      }}
      title="Inminent Action!"
    />
  )}
</div>
```

### With Icon

```jsx
import { Button } from '../Button'
import { Icon } from '../Icon'

const [show, setShow] = React.useState(false)

;<div>
  <Button onClick={() => setShow(true)}>Show Modal</Button>
  {show && (
    <ActionModal
      actionText="Take action!"
      body="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
      icon={
        <Icon color="positive" size="xl">
          <svg
            className="h-6 w-6"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fillRule="evenodd"
              d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
              clipRule="evenodd"
            />
          </svg>
        </Icon>
      }
      onCancel={() => setShow(false)}
      onAction={() => {
        alert('The action has been taken')
        setShow(false)
      }}
      title="Inminent Action!"
      variant="positive"
    />
  )}
</div>
```

### With Icon, no title

```jsx
import { Button } from '../Button'
import { Icon } from '../Icon'

const [show, setShow] = React.useState(false)

;<div>
  <Button onClick={() => setShow(true)}>Show Modal</Button>
  {show && (
    <ActionModal
      actionText="Take action!"
      body="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
      icon={
        <Icon color="caution" size="xl">
          <svg
            className="h-6 w-6"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fillRule="evenodd"
              d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
              clipRule="evenodd"
            />
          </svg>
        </Icon>
      }
      onCancel={() => setShow(false)}
      onAction={() => {
        alert('The action has been taken')
        setShow(false)
      }}
      variant="caution"
    />
  )}
</div>
```

### `onAction()` Promise Loading

If the `onAction` prop is a `Promise` the `<ActionModal />` will _await_ for the Promise to resolve. If the Promise is taking a long time, a message will appear to let the user cancel the action. This is to allow a good UX so the user does not get trapped with a modal on the screen and no method for escaping.

When the `onAction` prop is a Promise, there are optional customizations available. We try to set sane defaults, however, there are these 2 optional props that can be used:

- `actionSlowTimeout`: The timeout to trigger the _slow loading message_.
- `actionSlowMsg`: The message that is shown after the `actionSlowTimeout` time has passed.

```jsx
import { Button } from '../Button'

const [show, setShow] = React.useState(false)

;<div>
  <Button onClick={() => setShow(true)}>Show Async Action Modal</Button>
  {!!show && (
    <ActionModal
      // set to 2sec for demonstration purposes
      actionSlowTimeout={2000}
      actionText="Run Async Action"
      body="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
      onCancel={() => setShow(false)}
      onAction={async () => {
        await new Promise((resolve) => setTimeout(resolve, 10000))

        setShow(false)
      }}
      title="Async Loading Action"
    />
  )}
</div>
```
