> A modal window is an element that sits on top of an application’s main window.
> It creates a mode that disables the main window but keeps it visible with the
> modal window as a child window in front of it. Users must interact with the
> modal window before they can return to the parent application.
>
> [Wikipedia](https://en.wikipedia.org/wiki/Modal_window)

The `<Modal />` it's the most generic and flexible modal we provide.

Modals are controlled components, they need to be told when they should be
displayed by setting their `show` prop to `true`. When a modal wants to be
dismissed, it will trigger the callback provided to `closeHandler`.

```jsx
import { Button } from '../Button'

const [show, setShow] = React.useState(false)

;<div>
  <Button onClick={() => setShow(true)}>Show Modal</Button>

  {show && (
    <Modal
      closeHandler={() => setShow(false)}
      escPressClose
      header="Lorem Ipsum"
    >
      <div>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
        commodo consequat.
      </div>
    </Modal>
  )}
</div>
```

### No close button

```jsx
import { Button } from '../Button'

const [show, setShow] = React.useState(false)

;<div>
  <Button onClick={() => setShow(true)}>Show Modal</Button>

  {show && (
    <Modal closeHandler={() => setShow(false)} escPressClose showClose={false}>
      <div>To dimiss this modal press Esc.</div>
    </Modal>
  )}
</div>
```

### Dismiss on click outside

```jsx
import { Button } from '../Button'

const [show, setShow] = React.useState(false)

;<div>
  <Button onClick={() => setShow(true)}>Show Modal</Button>

  {show && (
    <Modal
      closeHandler={() => setShow(false)}
      escPressClose
      header="Lorem Ipsum"
      outsideClickClose
      showClose={false}
    >
      <div>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
        commodo consequat.
      </div>
    </Modal>
  )}
</div>
```

# Modal Context

The Modal provides a context provider and a hook for accessing the context.
All Modal's wrap their children with a context provider and will pass any
parent Modal context through.

### ModalContextProvider

The context provider allows deeply nesting a Modal component and passing a `closeHandler`
via context.

```jsx static
import { ModalContextProvider } from './context/ModalContext'
import { Button } from '../Button'

const [show, setShow] = React.useState(false)

;<ModalContextProvider closeHandler={() => setShow(false)}>
  {show && <Modal>Modal body...</Modal>}

  <Button onClick={() => setShow(true)}>Show Modal</Button>
</ModalContextProvider>
```

### Using the Modal Context Hook

The context hook allows children to check if they're inside a modal and also
perform some utility.

```jsx static
import { useModalContext } from './context/ModalContext'
import { Button } from '../Button'

const ModalChildComponent = () => {
  const modalCtx = useModalContext()

  if (modalCtx) {
    return <div>I am inside of a Modal!</div>
  }

  return <div>I am not a child of a Modal</div>
}

;<Modal>
  <ModalChildComponent />
</Modal>
```

```jsx static
import { useModalContext } from './context/ModalContext'
import { Button } from '../Button'

const ModalChildComponent = () => {
  const modalCtx = useModalContext()

  React.useEffect(() => {
    if (modalCtx) {
      modalCtx.setHeader('The Child set this Title!')
    }
  }, [modalCtx])

  return <div>Modal child component body.</div>
}

;<div>
  {show && (
    <Modal>
      <ModalChildComponent />
    </Modal>
  )}

  <Button onClick={() => setShow(true)}>Show Modal</Button>
</div>
```
