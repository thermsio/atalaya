import React, { useCallback, useEffect, useState } from 'react'
import { Button } from '../Button'
import { Icon } from '../Icon'
import { Modal } from './Modal'
import { Loading } from '../Loading'
import { Overlay } from '../Overlay'
import { SemanticVariant } from '../../constants'

export interface ActionModalProps {
  /** Optional message override to show when the `onAction` prop has exceeded the `actionSlowTimeout` time. */
  actionSlowMsg?: React.ReactNode
  /** If the `onAction` prop is a Promise, this number will be the timeout to let the user press the cancel button, if `onCancel` prop is provided. The default is 7500 (7.5 sec). */
  actionSlowTimeout?: number
  /** Text to display on action button. */
  actionText?: React.ReactNode
  /** Main content of the modal. */
  body: React.ReactNode
  /** `true` by default, the `onCancel` prop will be called after the `onAction` function is called (and resolved if it's a Promise)   */
  cancelAfterAction?: boolean
  /** Text to display on cancel button. */
  cancelText?: React.ReactNode
  /** If true, modal will close when pressing the `Esc` key. */
  escPressClose?: boolean
  /** SVG Icon to go along with the body. */
  icon?: React.ReactElement | React.ReactSVGElement
  /** Callback to be trigger when action button has been pressed. If it's a Promise it will be awaited. */
  onAction: () => void | Promise<any>
  /** Callback to be triggered when cancel button has been pressed */
  onCancel?: () => void
  /** Title to be displayed on top of the body */
  title?: React.ReactNode
  /** Semantic variant of button */
  variant?: SemanticVariant
}

const ActionModal = ({
  actionSlowMsg,
  actionSlowTimeout = 7500,
  actionText,
  body,
  cancelAfterAction = true,
  cancelText,
  escPressClose,
  icon,
  onAction,
  onCancel,
  title,
  variant = 'main',
}: ActionModalProps): React.ReactElement => {
  const [actionLoading, setActionLoading] = useState(false)
  const [cancelLoading, setCancelLoading] = useState(false)

  const handleActionClicked = useCallback(async () => {
    setActionLoading(true)
    setCancelLoading(true)

    try {
      await onAction()

      if (cancelAfterAction && onCancel) {
        onCancel()
      }
    } catch (err: any) {
      throw err
    } finally {
      setActionLoading(false)
      setCancelLoading(false)
    }
  }, [onAction])

  useEffect(() => {
    /*
        If the `onAction()` gets stuck loading, we want to give the user some UI option to close
        the so that they aren't stuck with a modal blocking their UI
     */
    const t = setTimeout(() => {
      setCancelLoading(false)
    }, actionSlowTimeout)

    return () => clearTimeout(t)
  }, [cancelLoading])

  return (
    <Modal
      escPressClose={!!(escPressClose && onCancel)}
      closeHandler={onCancel}
      header={
        <h3
          className="mt-xs text-lg font-medium leading-6 text-color-subtle"
          id="modal-headline"
        >
          {title}
        </h3>
      }
      showClose={false}
    >
      {/* <Modal /> has "relative" class */}
      {cancelLoading && <Loading overlay />}
      {!cancelLoading && actionLoading && (
        <Overlay>
          <div className="mb-sm flex justify-center">
            <Loading />
          </div>

          <div className="flex items-center text-center text-sm text-color-caution">
            <Icon>
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                <path d="M4.47 21h15.06c1.54 0 2.5-1.67 1.73-3L13.73 4.99c-.77-1.33-2.69-1.33-3.46 0L2.74 18c-.77 1.33.19 3 1.73 3zM12 14c-.55 0-1-.45-1-1v-2c0-.55.45-1 1-1s1 .45 1 1v2c0 .55-.45 1-1 1zm1 4h-2v-2h2v2z" />
              </svg>
            </Icon>{' '}
            {actionSlowMsg || 'This is taking longer than expected'}
          </div>

          <div className="flex justify-center">
            <Button onClick={onCancel}>{cancelText || 'Cancel'}</Button>
          </div>
        </Overlay>
      )}

      <div className="sm:flex sm:items-start sm:justify-start">
        {!!icon && (
          <div className="flex justify-center p-sm px-base">{icon}</div>
        )}

        <div className="mt-xs text-center sm:mt-0 sm:text-left">
          <div className="mt-xs">
            {typeof body === 'string' ? (
              <p className="text-subtle">{body}</p>
            ) : (
              body
            )}
          </div>
        </div>
      </div>

      <div className="mt-lg flex flex-col space-y-xs sm:flex-row-reverse sm:space-x-xs sm:space-y-0 sm:space-x-reverse">
        <Button onClick={handleActionClicked} variant={variant}>
          {actionText || 'Accept'}
        </Button>

        {onCancel && (
          <Button onClick={onCancel}>{cancelText || 'Cancel'}</Button>
        )}
      </div>
    </Modal>
  )
}

export { ActionModal }
