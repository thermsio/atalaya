import React, { useEffect, useRef, useState } from 'react'
import { Button } from '../Button'
import { Icon } from '../Icon'
import { ModalContextProvider, useModalContext } from './context/ModalContext'
import { usePageScrollLock } from '../../hooks/usePageScrollLock'
import './Modal.css'
import { BREAKPOINT_NAMES, Breakpoints } from '../../constants/Breakpoint'
import classNames from 'classnames'

export interface ModalProps {
  children?: React.ReactNode
  className?: string
  /** Callback to be triggered when modal needs to close. */
  closeHandler?: () => void
  /** If true, modal will close when pressing the `Esc` key. */
  escPressClose?: boolean
  /** Title of the modal. */
  header?: React.ReactNode
  /** If true, will not render children's card wrapper. */
  overlayOnly?: boolean
  /** If true, modal will close when clicking the surrounding backdrop. */
  outsideClickClose?: boolean
  /** Show a close button in the header. */
  showClose?: boolean
  /** The max width of the modal, the default is "md" */
  width?: Breakpoints | string
}

const ModalComponent = ({
  children,
  className,
  closeHandler,
  escPressClose,
  header,
  overlayOnly,
  outsideClickClose,
  showClose = true,
  width = 'md',
}: ModalProps) => {
  const modalRef = useRef<HTMLDivElement>(null)
  const modalCtx = useModalContext({ ctxRequired: true })
  const isActiveModal =
    modalCtx?.modalIdsLIFOStack[modalCtx?.modalIdsLIFOStack.length - 1] ===
    modalCtx?.modalId
  const internalCloseHandler = closeHandler || modalCtx?.closeHandler

  const handleOverlayClick = (e) => {
    e.stopPropagation()

    if (outsideClickClose && internalCloseHandler && modalRef.current) {
      if (!modalRef.current.contains(e.target)) internalCloseHandler()
    }
  }

  usePageScrollLock(true)

  useEffect(() => {
    if (isActiveModal) {
      modalRef.current?.focus()
    }
  }, [isActiveModal])

  // FIXME: Needs to take into account two modal contexts existing independent of each other.
  // useEffect(() => {
  //   const handleBlur = (e) => {
  //     if (modalRef.current) {
  //       if (isActiveModal && !modalRef.current.contains(e.relatedTarget)) {
  //         e.stopPropagation()
  //         modalRef.current?.focus()
  //       }
  //     }
  //   }
  //
  //   modalRef.current?.addEventListener('focusout', handleBlur)
  //   return () => {
  //     modalRef.current?.removeEventListener('focusout', handleBlur)
  //   }
  // }, [modalRef.current, isActiveModal])

  useEffect(() => {
    const handleEscPress = (e) => {
      if (
        e.key === 'Escape' &&
        escPressClose &&
        internalCloseHandler &&
        modalRef.current &&
        isActiveModal
      ) {
        e.stopPropagation()
        internalCloseHandler?.()
      }
    }

    modalRef.current?.addEventListener('keydown', handleEscPress)
    return () => {
      modalRef.current?.removeEventListener('keydown', handleEscPress)
    }
  }, [
    modalCtx?.modalIdsLIFOStack,
    isActiveModal,
    escPressClose,
    internalCloseHandler,
    modalRef.current,
  ])

  // Casting due to TS limitations, see https://github.com/microsoft/TypeScript/issues/26255
  const isBreakpointWidth = (BREAKPOINT_NAMES as readonly string[]).includes(
    width,
  )

  if (overlayOnly) {
    return (
      <div
        className="modal-dialog outline-0"
        role="dialog"
        aria-modal="true"
        ref={modalRef}
        onClick={handleOverlayClick}
        tabIndex={0}
      >
        {children}
      </div>
    )
  }

  const isHeaderRowRendered = !!header || escPressClose || showClose

  return (
    <div
      className="modal-dialog"
      role="dialog"
      aria-modal="true"
      aria-labelledby="modal-headline"
      onClick={handleOverlayClick}
    >
      <div
        className={classNames(className, 'modal-element', {
          [`max-w-screen-${width}`]: isBreakpointWidth,
        })}
        style={{ maxWidth: isBreakpointWidth ? undefined : width }}
        ref={modalRef}
        tabIndex={0}
      >
        {isHeaderRowRendered && (
          <div className="sticky -top-base z-sticky flex w-full items-start justify-between rounded-t-lg bg-surface p-base md:-top-base">
            <div id="modal-headline" className="min-w-0 grow">
              {!!header &&
                (typeof header === 'string' ? (
                  <h3 className="mt-xs text-lg font-medium leading-6 text-color-subtle">
                    {header}
                  </h3>
                ) : (
                  header
                ))}
            </div>

            <div className="inline-flex items-center">
              {escPressClose && (
                <span className="px-2 py-1 text-xs text-color-subtle">esc</span>
              )}

              {showClose && (
                <Button subtle onClick={() => internalCloseHandler?.()}>
                  <span className="sr-only">Close</span>

                  <Icon>
                    <svg
                      className="h-6 w-6"
                      fill="currentColor"
                      viewBox="0 0 20 20"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fillRule="evenodd"
                        d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                        clipRule="evenodd"
                      />
                    </svg>
                  </Icon>
                </Button>
              )}
            </div>
          </div>
        )}

        <div
          className={`relative flex-1 overflow-y-auto ${
            isHeaderRowRendered ? 'px-base pb-base' : 'p-base'
          }`}
        >
          {children}
        </div>
      </div>
    </div>
  )
}

const Modal = (modalProps: ModalProps) => {
  const [instanceId] = useState(() => Math.random().toString())
  const [internalHeader, setInternalHeader] = useState<React.ReactNode>()

  return (
    <ModalContextProvider
      modalId={instanceId}
      setHeader={setInternalHeader}
      closeHandler={modalProps.closeHandler}
    >
      <ModalComponent
        {...modalProps}
        header={internalHeader || modalProps.header}
      />
    </ModalContextProvider>
  )
}

export { Modal }

// PurgeCSS
// max-w-screen-sm max-w-screen-md max-w-screen-lg max-w-screen-xl max-w-screen-xxl
