import React, { MouseEventHandler, useEffect, useRef, useState } from 'react'
import LazyLoad from 'react-lazy-load'
import { ConditionalWrapper } from '../../utils/components/ConditionalWrapper'
import { Skeleton } from '../Skeleton'

import { makeImageClasses, ImageClassProps } from './utils'
import classNames from 'classnames'
import { useLightboxContext } from '../Lightbox/context/LightboxContext'
import { LIGHTBOX_FILE_TYPES } from '../Lightbox/constants'

export interface ImageProps
  extends Omit<React.ImgHTMLAttributes<HTMLImageElement>, 'placeholder'>,
    ImageClassProps {
  /** Sets image shape as circular */
  circular?: boolean
  /** Img element additional classes */
  className?: string
  /** How should the image element be resized to fit its container */
  fit?: 'contain' | 'cover' | 'fill' | 'none' | 'scale-down'
  /** Image stretches to fit its container */
  fluid?: boolean
  height?: number | string
  /** Enable lazy loading. Defaults to false */
  lazy?: boolean
  /** If true will open image inside lightbox on click  */
  lightbox?: boolean
  /** The url that will be used when lightbox is open. If undefined, `src` will be used.   */
  lightboxSrc?: string
  maxHeight?: number | string
  maxWidth?: number | string
  /** Don't show skeleton when loading image */
  noSkeleton?: boolean
  onClick?: MouseEventHandler<HTMLImageElement>
  /** URL of the placeholder image */
  placeholder?: 'default' | 'preserveSpace' | 'none' | string
  /** Sets rounded corners on image */
  rounded?: boolean
  /** Img's element src attribute */
  src?: string
  /** Fix image to thumbnail size */
  thumbnail?: boolean
  width?: number | string
}

const IMAGE_ERROR =
  'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTkyIiBoZWlnaHQ9IjE5MiIgdmlld0JveD0iMCAwIDE5MiAxOTIiIGZpbGw9Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHJlY3Qgd2lkdGg9IjE5MiIgaGVpZ2h0PSIxOTIiIHJ4PSI4IiBmaWxsPSIjODAxMTAwIiBmaWxsLW9wYWNpdHk9Ii41Ii8+PHBhdGggZD0iTTE0NS43NzggMzJINDYuMjIyQzM4LjQgMzIgMzIgMzguNCAzMiA0Ni4yMjJ2OTkuNTU2QzMyIDE1My42IDM4LjQgMTYwIDQ2LjIyMiAxNjBoOTkuNTU2QzE1My42IDE2MCAxNjAgMTUzLjYgMTYwIDE0NS43NzhWNDYuMjIyQzE2MCAzOC40IDE1My42IDMyIDE0NS43NzggMzJabTAgMTEzLjc3OEg0Ni4yMjJ2LTMyLjU2OWw3LjA0IDcuMDQgMjguNDQ1LTI4LjQ0NSAyOC40NDQgMjguNDQ1IDI4LjQ0NS0yOC4zNzMgNy4xODIgNy4xODJ2NDYuNzJabTAtNjYuOTE2LTcuMTgyLTcuMTgyLTI4LjQ0NSAyOC41MTYtMjguNDQ0LTI4LjQ0NS0yOC40NDUgMjguNDQ1LTcuMDQtNy4xMTJWNDYuMjIyaDk5LjU1NnYzMi42NFoiIGZpbGw9IiNmZmYiIGZpbGwtb3BhY2l0eT0iLjgiLz48L3N2Zz4='
const IMAGE_PLACEHOLDER =
  'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTkyIiBoZWlnaHQ9IjE5MiIgdmlld0JveD0iMCAwIDE5MiAxOTIiIGZpbGw9Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHJlY3Qgd2lkdGg9IjE5MiIgaGVpZ2h0PSIxOTIiIHJ4PSI4IiBmaWxsPSIjM0Q0NDREIiBmaWxsLW9wYWNpdHk9Ii41Ii8+PHBhdGggZD0iTTE0NS43NzggNDYuMjIydjk5LjU1Nkg0Ni4yMjJWNDYuMjIyaDk5LjU1NlptMC0xNC4yMjJINDYuMjIyQzM4LjQgMzIgMzIgMzguNCAzMiA0Ni4yMjJ2OTkuNTU2QzMyIDE1My42IDM4LjQgMTYwIDQ2LjIyMiAxNjBoOTkuNTU2QzE1My42IDE2MCAxNjAgMTUzLjYgMTYwIDE0NS43NzhWNDYuMjIyQzE2MCAzOC40IDE1My42IDMyIDE0NS43NzggMzJabS0zNC41NiA2My4wMDQtMjEuMzM0IDI3LjUyLTE1LjIxNy0xOC40MTctMjEuMzM0IDI3LjQ0OWg4NS4zMzRsLTI3LjQ0OS0zNi41NTJaIiBmaWxsPSIjZmZmIiBmaWxsLW9wYWNpdHk9Ii44Ii8+PC9zdmc+'

const previouslyLoadedMap = new Map<string, boolean>()

const ImageComponent = ({
  alt,
  circular,
  className,
  fit = 'cover',
  fluid,
  height,
  lazy = false,
  lightbox,
  lightboxSrc,
  maxHeight,
  maxWidth,
  noSkeleton,
  onClick,
  placeholder = 'default',
  rounded,
  src,
  thumbnail,
  width,
  ...props
}: ImageProps): React.ReactElement | null => {
  const local = useRef<{ retryTimeout: any; retriesLeft: number }>({
    retryTimeout: 0,
    retriesLeft: 3,
  })
  const [retrying, setRetrying] = useState(false)
  const [error, setError] = useState(false)
  const [loaded, setLoaded] = useState(
    previouslyLoadedMap.get(src || '') || false,
  )
  const _height = fluid ? height : height || width
  const _width = fluid ? width : width || height

  const lightboxContext = useLightboxContext()

  let imgSource: string | undefined = undefined

  if (src) imgSource = src
  else if (placeholder === 'default') imgSource = IMAGE_PLACEHOLDER
  else if (typeof placeholder === 'string') imgSource = placeholder

  const handleOnClick =
    onClick ||
    (lightbox && lightboxContext?.onClickFile) ||
    (lightbox && lightboxContext?.open)
      ? (event) => {
          if (onClick) return onClick(event)
          if (lightboxContext?.onClickFile) {
            return lightboxContext.onClickFile(lightboxSrc || src || '')
          }
          if (lightboxContext?.open)
            return lightboxContext.open({
              src: lightboxSrc || src || '',
              type: LIGHTBOX_FILE_TYPES.IMAGE,
            })
        }
      : undefined

  const handleRetryImage = () => {
    if (!imgSource) return

    if (local.current.retriesLeft > 0) {
      setError(false)
      setRetrying(true)

      local.current.retryTimeout = setTimeout(() => {
        const img = new Image()

        img.onload = () => {
          setLoaded(true)
          setRetrying(false)
        }

        img.onerror = () => {
          local.current.retriesLeft--
          handleRetryImage()
        }
        // While this img it's not the <img /> that it's being displayed. Once an image is loaded, browsers update all
        // <img /> that have the same url.
        img.src = imgSource || ''
      }, 2000)
    } else {
      console.warn('Image, unable to fetch image after retrying')
      setLoaded(true)
      setRetrying(false)
    }
  }

  useEffect(() => {
    return () => {
      clearTimeout(local.current.retryTimeout)
    }
  }, [])
  const classes = makeImageClasses({
    circular,
    className,
    fit,
    fluid,
    rounded,
    thumbnail,
  })

  if (!src && placeholder === 'none') return null

  if (!src && placeholder === 'preserveSpace')
    return <div style={{ height: _height, width: _width }} />

  return (
    <ConditionalWrapper
      condition={lazy}
      wrapper={(children) => (
        <LazyLoad height={_height} width={_width}>
          {children}
        </LazyLoad>
      )}
    >
      <div
        className={
          handleOnClick
            ? 'cursor-pointer transition-[filter] hover:brightness-125 active:brightness-75'
            : undefined
        }
      >
        {!!src &&
          !noSkeleton &&
          !loaded &&
          (circular ? (
            <Skeleton.Circle className={classes} size={_height || _width} />
          ) : (
            <div>
              <Skeleton
                borderRadius="sm"
                className={classes}
                height={_height}
                width={_width}
              />
            </div>
          ))}

        {!retrying && (
          <img
            {...props}
            alt={alt}
            className={classNames(classes, { hidden: retrying || !loaded })}
            onClick={handleOnClick}
            onError={() => {
              setError(true)
              handleRetryImage()
            }}
            onLoad={() => {
              setLoaded(true)
              previouslyLoadedMap.set(imgSource || 'undefined', true)
            }}
            src={error ? IMAGE_ERROR : imgSource}
            height={_height}
            width={_width}
            style={{
              height: _height,
              maxHeight: maxHeight,
              maxWidth: maxWidth,
              width: _width,
            }}
          />
        )}
      </div>
    </ConditionalWrapper>
  )
}

export { ImageComponent, ImageComponent as Image }
