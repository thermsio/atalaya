import React from 'react'
import { Image } from './index'

const tallConstrained = (
  <Image src="https://via.placeholder.com/100x300" height={100} width={200} />
)

const wideConstrained = (
  <Image src="https://via.placeholder.com/300x100" height={200} width={100} />
)

const wideFluid = (
  <Image src="https://via.placeholder.com/100x300" height={200} fluid />
)

const noSource = <Image height={200} fluid />

const error = <Image src="www.error.com" height={40} />

const errorMixedDimensions = (
  <Image src="www.error.com" height={40} width={65} />
)

const errorNoDimensions = <Image src="www.error.com" />

const preserveSpace = <Image placeholder="preserveSpace" height={100} />

const placeholderNone = <Image placeholder="none" height={100} />

const fluidWithWidth = <Image src="https://via.placeholder.com/300x100" fluid />

const circular = (
  <>
    <Image
      src="https://therms-files.s3.amazonaws.com/62d9d01cde555031049d8bf7.png"
      circular
      height={70}
      width={70}
      onClick={() => {
        console.log('clicked')
      }}
      lightbox
    />
    <Image
      src="https://therms-files.s3.amazonaws.com/62d9d01cde555031049d8bf7.png"
      circular
      lightbox
    />
    <Image src="https://therms-files.s3.amazonaws.com/62d9d01cde555031049d8bf7.png" />
  </>
)

const loadingInFlexContainer = (
  <div className="flex space-x-base">
    <Image
      src="https://therms-files.s3.amazonaws.com/62d9d01cde555031049d8bf7.png"
      circular
      height={70}
      width={70}
    />

    <div className="h-40 w-4 bg-surface" />
  </div>
)

export default {
  circular,
  error,
  errorMixedDimensions,
  errorNoDimensions,
  noSource,
  tallConstrained,
  wideConstrained,
  wideFluid,
  preserveSpace,
  placeholderNone,
  fluidWithWidth,
  loadingInFlexContainer,
}
