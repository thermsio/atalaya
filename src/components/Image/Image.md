Provides some props to easily manage images

### Default

```jsx
<Image src="http://via.placeholder.com/500x250" />
```

### Rounded

```jsx
<Image src="http://via.placeholder.com/500x250" rounded />
```

### Circle

```jsx
<Image src="http://via.placeholder.com/500x250" circular fit="cover" />
```

### Thumbnail

```jsx
<Image src="http://via.placeholder.com/500x250" thumbnail />
```

### Fluid

```jsx
<Image src="http://via.placeholder.com/500x250" fluid />
```

### Placeholder

If Image does not have a `src` prop there are several ways you can choose to display the missing image.

#### Default

```jsx
<Image height={100} />
```

#### Preserve space

```jsx
<div className="flex items-center">
  <span>Image is not displayed but it's still taking space 👉</span>
  <Image placeholder="preserveSpace" height={100} />
  <span>👈</span>
</div>
```

#### None

Image won't be rendered if there is no `src`

```jsx
<Image placeholder="none" />
```

### Custom

You can also use a URL to a placeholder image of your choice.

```jsx
<Image placeholder="https://via.placeholder.com/150" />
```

### Fixed Dimensions

You can use `height` & `width` props to specify the dimensions of an image. If you only set one of those options, the
other will be automatically set to present a square image, unless the `fluid` property is set to true and `height` has
been set to some value. In that case `width` won't be fixed, so it can adapt to the container's width.

```jsx
<Image src="http://via.placeholder.com/300x300" height={500} width={250} />
```
