import classNames from 'classnames'

export interface ImageClassProps {
  /** Sets image shape as circular */
  circular?: boolean
  /** Img element additional classes */
  className?: string
  /** How should the image element be resized to fit its container */
  fit?: 'contain' | 'cover' | 'fill' | 'none' | 'scale-down'
  /** Image stretches to fit its container */
  fluid?: boolean
  /** Sets rounded corners on image */
  rounded?: boolean
  /** Fix image to thumbnail size */
  thumbnail?: boolean
}

export const makeImageClasses = ({
  circular,
  className,
  fit,
  fluid,
  rounded,
  thumbnail,
}: ImageClassProps): string => {
  return classNames(className, {
    rounded: rounded || (!rounded && thumbnail),
    'rounded-full': circular,
    'w-full h-full': fluid,
    'h-xxl w-xxl': thumbnail,
    'object-contain': fit === 'contain',
    'object-cover': fit === 'cover' || (!fit && thumbnail),
    'object-fill': fit === 'fill',
    'object-none': fit === 'none',
    'object-scale-down': fit === 'scale-down',
  })
}
