import React, { useEffect, useState } from 'react'
import { LightboxFile, LightboxItem } from './components/LightboxItem'
import { Button } from '../Button'
import { Icon } from '../Icon'
import { ButtonGroup } from '../ButtonGroup'
import { Modal } from '../Modals/Modal'

const handleDownload = ({ name, src }: LightboxFile) => {
  const a = document.createElement('a')
  a.href = src
  a.setAttribute('download', name || new Date().toISOString())
  document.body.appendChild(a)
  a.click()
  document.body.removeChild(a)
}

interface LightboxProps {
  files: LightboxFile[] | LightboxFile
  initialIndex?: number
  onClose: () => void
}

const Lightbox: React.FC<LightboxProps> = ({
  files,
  initialIndex = 0,
  onClose,
}) => {
  const [currentIndex, setCurrentIndex] = useState<number>(initialIndex)
  const isFileArray = Array.isArray(files)

  const handlePreviousFile = () => {
    if (isFileArray && currentIndex > 0) {
      setCurrentIndex(currentIndex - 1)
    }
  }

  const handleNextFile = () => {
    if (isFileArray && currentIndex < files.length - 1) {
      setCurrentIndex(currentIndex + 1)
    }
  }

  useEffect(() => {
    const handleArrowPress = (e) => {
      if (e.key === 'ArrowLeft' || e.key === 'ArrowRight') {
        e.stopPropagation()
        e.key === 'ArrowLeft' ? handlePreviousFile() : handleNextFile()
      }
    }

    document.addEventListener('keydown', handleArrowPress)

    return () => {
      document.removeEventListener('keydown', handleArrowPress)
    }
  }, [currentIndex, files])

  return (
    <Modal closeHandler={onClose} escPressClose overlayOnly>
      <div className="flex flex-col w-full">
        <div className="flex h-[50px] items-center justify-between p-xs w-full">
          <div className="truncate font-light tracking-wide text-color-neutral-light">
            {files[currentIndex]?.name && (
              <span>{files[currentIndex].name}</span>
            )}
          </div>

          <ButtonGroup subtle>
            <Button onClick={() => handleDownload(files[currentIndex])}>
              <Icon color="neutral-light">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 24 24"
                  fill="currentColor"
                >
                  <g>
                    <rect fill="none" height="24" width="24" />
                  </g>
                  <g>
                    <path d="M16.59,9H15V4c0-0.55-0.45-1-1-1h-4C9.45,3,9,3.45,9,4v5H7.41c-0.89,0-1.34,1.08-0.71,1.71l4.59,4.59 c0.39,0.39,1.02,0.39,1.41,0l4.59-4.59C17.92,10.08,17.48,9,16.59,9z M5,19c0,0.55,0.45,1,1,1h12c0.55,0,1-0.45,1-1s-0.45-1-1-1H6 C5.45,18,5,18.45,5,19z" />
                  </g>
                </svg>
              </Icon>
            </Button>

            <Button onClick={onClose}>
              <Icon color="neutral-light">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 24 24"
                  fill="currentColor"
                >
                  <path d="M0 0h24v24H0V0z" fill="none" />
                  <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12 19 6.41z" />
                </svg>
              </Icon>
            </Button>
          </ButtonGroup>
        </div>

        <div
          className="flex grow justify-between pb-xl"
          style={{ height: 'calc(100vh - 50px)' }}
        >
          {isFileArray && files.length > 1 && (
            <Button
              borderRadius="none"
              disabled={currentIndex === 0}
              onClick={handlePreviousFile}
              size="lg"
              subtle
            >
              <Icon>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  height="24px"
                  viewBox="0 0 24 24"
                  width="24px"
                  fill="currentColor"
                >
                  <path d="M0 0h24v24H0V0z" fill="none" />
                  <path d="M14.91 6.71c-.39-.39-1.02-.39-1.41 0L8.91 11.3c-.39.39-.39 1.02 0 1.41l4.59 4.59c.39.39 1.02.39 1.41 0 .39-.39.39-1.02 0-1.41L11.03 12l3.88-3.88c.38-.39.38-1.03 0-1.41z" />
                </svg>
              </Icon>
            </Button>
          )}

          <div className="relative flex grow items-center justify-center px-base">
            <LightboxItem
              src={isFileArray ? files[currentIndex]?.src : files.src}
              type={isFileArray ? files[currentIndex]?.type : files.type}
              onDownload={handleDownload}
            />
          </div>

          {isFileArray && files.length > 1 && (
            <Button
              borderRadius="none"
              disabled={currentIndex === files.length - 1}
              onClick={handleNextFile}
              size="lg"
              subtle
            >
              <Icon>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  height="24px"
                  viewBox="0 0 24 24"
                  width="24px"
                  fill="currentColor"
                >
                  <path d="M0 0h24v24H0V0z" fill="none" />
                  <path d="M9.31 6.71c-.39.39-.39 1.02 0 1.41L13.19 12l-3.88 3.88c-.39.39-.39 1.02 0 1.41.39.39 1.02.39 1.41 0l4.59-4.59c.39-.39.39-1.02 0-1.41L10.72 6.7c-.38-.38-1.02-.38-1.41.01z" />
                </svg>
              </Icon>
            </Button>
          )}
        </div>
      </div>
    </Modal>
  )
}

export { Lightbox, LightboxProps }
