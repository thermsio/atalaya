`Lightbox` accepts a files array to know which elements should display in the lightbox. In order to interact
with `Lightbox` and let it know which item needs to open, it provides an `openLightbox`, this callbacks may be called
with the file id as it's only argument to open the desired file. There are two ways you can access this callback.

### Accessing openLightbox via context

`Lightbox` provides to its children a `context` containing `openLightBox`. To access this context you can
use `Lightbox.useContext`.

```jsx
import { LightboxContext } from './context/LightboxContext'
import { Image } from '../Image'
import { Inline } from '../../layout/Inline'

const files = [
  {
    id: 'a',
    fileUrl:
      'https://images.unsplash.com/photo-1581894158358-5ecd2c518883?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1406&q=80',
    type: 'image',
  },
  {
    id: 'b',
    fileUrl:
      'https://images.unsplash.com/photo-1544550285-f813152fb2fd?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1400&q=80',
    type: 'image',
  },
  {
    id: 'c',
    fileUrl:
      'https://images.unsplash.com/photo-1584592740039-cddf0671f3d4?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1400&q=80',
    type: 'image',
  },
  {
    id: 'd',
    fileUrl: 'https://www.lightgalleryjs.com/videos/video1.mp4',
    thumbUrl:
      'https://www.lightgalleryjs.com/images/demo/html5-video-poster.jpg',
    posterUrl:
      'https://www.lightgalleryjs.com/images/demo/html5-video-poster.jpg',
    type: 'video/mp4',
  },
]

const FileListDisplay = ({ files }) => {
  const { openLightbox } = Lightbox.useContext()

  return files.map((file) => (
    <Image
      fit="cover"
      height={100}
      key={file.id}
      onClick={() => openLightbox(file.id)}
      rounded
      src={file.thumbUrl || file.fileUrl}
      width={100}
    />
  ))
}

;<LightboxContext files={files}>
  <Inline space="base">
    <FileListDisplay files={files} />
  </Inline>
</LightboxContext>
```

### Accessing openLightbox via render props

If you use a function as `Lightbox` children, `openLightbox` will be supplied as an argument to it. This allows you to
define all the JSX structure in a single component.

```jsx
import { LightboxContext } from './context/LightboxContext'
import { Image } from '../Image'
import { Inline } from '../../layout/Inline'

const files = [
  {
    id: 'a',
    fileUrl:
      'https://images.unsplash.com/photo-1581894158358-5ecd2c518883?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1406&q=80',
    type: 'image',
  },
  {
    id: 'b',
    fileUrl:
      'https://images.unsplash.com/photo-1544550285-f813152fb2fd?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1400&q=80',
    type: 'image',
  },
  {
    id: 'c',
    fileUrl:
      'https://images.unsplash.com/photo-1584592740039-cddf0671f3d4?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1400&q=80',
    type: 'image',
  },
  {
    id: 'd',
    fileUrl: 'https://www.lightgalleryjs.com/videos/video1.mp4',
    thumbUrl:
      'https://www.lightgalleryjs.com/images/demo/html5-video-poster.jpg',
    posterUrl:
      'https://www.lightgalleryjs.com/images/demo/html5-video-poster.jpg',
    type: 'video/mp4',
  },
]

;<LightboxContext files={files}>
  {(openLightbox) => (
    <Inline space="base">
      {files.map((file) => (
        <Image
          fit="cover"
          height={100}
          key={file.id}
          onClick={() => openLightbox(file.id)}
          rounded
          src={file.thumbUrl || file.fileUrl}
          width={100}
        />
      ))}
    </Inline>
  )}
</LightboxContext>
```

#### LightboxFiles

```ts static
interface LightboxFiles {
  /** alt text for img */
  alt?: string
  /** Url that wil be used when the download button is pressed */
  downloadUrl?: string
  /** String representing the file, will be used to select the file to open in lightbox. */
  id: string
  /** It will be used to name the file when it's downloaded. */
  fileName?: string
  /** Url of the file to be displayed on the lightbox */
  fileUrl: string
  /** For videos, image to display while the video is loading in lightbox. */
  posterUrl?: string
  /** Url for the image to be used on lightbox thumbnails. */
  thumbUrl?: string
  /** Tell Lightbox how to display the file. String must contain either image or video for it to be available on lightbox. */
  type: 'image' | 'video' | string
}
```

#### openLightbox

```ts static
type OpenLightbox = (itemId: string) => void
```
