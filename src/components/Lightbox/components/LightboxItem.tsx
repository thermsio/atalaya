import React, { useEffect, useState } from 'react'
import { Icon } from '../../Icon'
import { LIGHTBOX_FILE_TYPES } from '../constants'
import { Loading } from '../../Loading'
import { Button } from '../../Button'

export type LightboxFile = {
  src: string
  name?: string
  type?: LIGHTBOX_FILE_TYPES | string
}

export interface LightboxItemProps extends LightboxFile {
  onDownload?: (file: LightboxFile) => void
}

const LightboxItem: React.FC<LightboxItemProps> = ({ onDownload, ...file }) => {
  const { src, type } = file
  const [loadingImage, setLoadingImage] = useState<boolean | undefined>()

  useEffect(() => {
    if (type === LIGHTBOX_FILE_TYPES.IMAGE) setLoadingImage(true)
  }, [src])

  if (type === LIGHTBOX_FILE_TYPES.IMAGE) {
    return (
      <>
        <img
          className="object-fit-contain m-auto max-h-full max-w-full"
          src={src}
          alt=""
          onLoad={() => setLoadingImage(false)}
        />

        {loadingImage && <Loading overlay size="xl" />}
      </>
    )
  }

  if (type === LIGHTBOX_FILE_TYPES.VIDEO)
    return <video className="m-auto max-h-full max-w-full" controls src={src} />

  if (type === LIGHTBOX_FILE_TYPES.PDF)
    return <iframe className="h-full w-full rounded shadow-xl" src={src} />
  return (
    <div className="m-auto flex flex-col items-center space-y-xs">
      <Icon size="xxl" color="neutral-light">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 24 24"
          fill="currentColor"
        >
          <path d="M0 0h24v24H0V0z" fill="none" />
          <path d="M21 5v6.59l-2.29-2.3c-.39-.39-1.03-.39-1.42 0L14 12.59 10.71 9.3c-.39-.39-1.02-.39-1.41 0L6 12.59 3 9.58V5c0-1.1.9-2 2-2h14c1.1 0 2 .9 2 2zm-3 6.42l3 3.01V19c0 1.1-.9 2-2 2H5c-1.1 0-2-.9-2-2v-6.58l2.29 2.29c.39.39 1.02.39 1.41 0l3.3-3.3 3.29 3.29c.39.39 1.02.39 1.41 0l3.3-3.28z" />
        </svg>
      </Icon>
      <span className="text-lg font-light text-color-neutral-light">
        File type not supported
      </span>

      {!!onDownload && (
        <Button
          className="space-x-xxs"
          onClick={() => onDownload(file)}
          variant="main"
          subtle
        >
          <Icon>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 24 24"
              fill="currentColor"
            >
              <g>
                <rect fill="none" height="24" width="24" />
              </g>
              <g>
                <path d="M16.59,9H15V4c0-0.55-0.45-1-1-1h-4C9.45,3,9,3.45,9,4v5H7.41c-0.89,0-1.34,1.08-0.71,1.71l4.59,4.59 c0.39,0.39,1.02,0.39,1.41,0l4.59-4.59C17.92,10.08,17.48,9,16.59,9z M5,19c0,0.55,0.45,1,1,1h12c0.55,0,1-0.45,1-1s-0.45-1-1-1H6 C5.45,18,5,18.45,5,19z" />
              </g>
            </svg>
          </Icon>
          <span>Download File</span>
        </Button>
      )}
    </div>
  )
}

export { LightboxItem }
