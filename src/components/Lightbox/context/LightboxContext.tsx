import React, { useCallback, useContext, useEffect, useState } from 'react'
import { Lightbox } from '../index'
import { LightboxFile } from '../components/LightboxItem'
import {
  getFileTypeFromUrlExt,
  getFileTypeFromUrlServer,
} from '../utils/getFileTypeFromUrl'

type LightboxFiles = LightboxFile[]

export interface LightboxContextValue {
  close: () => void
  onClickFile?: (file: string) => void
  open: (files: LightboxFiles | LightboxFile, initialIndex?: number) => void
}

const LightboxContext = React.createContext<LightboxContextValue | null>(null)

interface LightboxContextProviderProps {
  children?: React.ReactNode
  onClickFile?: (fileUrl: string) => void
}

export const LightboxContextProvider = ({
  children,
  onClickFile,
}: LightboxContextProviderProps) => {
  const parentLightboxCtx = useContext(LightboxContext)

  const [isOpen, setIsOpen] = useState(false)
  const [files, setFiles] = useState<LightboxFiles | LightboxFile>([])
  const [initialIndex, setInitialIndex] = useState<number>()

  const handleLightboxClose = useCallback(() => {
    if (parentLightboxCtx) {
      parentLightboxCtx.close()
    } else {
      setFiles([])
      setInitialIndex(undefined)
      setIsOpen(false)
    }
  }, [])

  const handleLightboxOpen = useCallback(
    (newFiles: LightboxFiles | LightboxFile, initialIndex?: number) => {
      if (parentLightboxCtx) {
        parentLightboxCtx.open(newFiles, initialIndex)
      } else if (newFiles) {
        setFiles(Array.isArray(newFiles) ? newFiles : [newFiles])
        setInitialIndex(initialIndex || 0)
        setIsOpen(true)
      } else {
        handleLightboxClose()
      }
    },
    [],
  )

  return (
    <LightboxContext.Provider
      value={{
        close: handleLightboxClose,
        onClickFile,
        open: handleLightboxOpen,
      }}
    >
      {children}

      {!parentLightboxCtx && isOpen && (
        <Lightbox
          files={files}
          onClose={handleLightboxClose}
          initialIndex={initialIndex}
        />
      )}
    </LightboxContext.Provider>
  )
}

export const useLightboxContext = () => {
  return useContext(LightboxContext)
}

interface LightboxContextGroupProps {
  children: React.ReactNode
  files: string[]
}

export const LightboxContextGroup = ({
  children,
  files,
}: LightboxContextGroupProps) => {
  const lightboxCtx = useLightboxContext()
  const [parsedFiles, setParsedFiles] = useState<LightboxFiles>([])

  useEffect(() => {
    if (!lightboxCtx) {
      console.warn(
        'Atalaya <LightboxContextGroup /> mounted with no parent <LightboxContextProvider />',
      )
    }
  }, [])

  // @note: We need to know the type of resource each url leads to so Lightbox knows how to handle it, we first check for an url extension and if that fails we make a HEAD request in order to get the MIME type.
  useEffect(() => {
    const controller = new AbortController()
    const { signal } = controller

    if (files) {
      const urlsMissingType: string[] = []

      const filesMap = new Map<string, LightboxFile>(
        files.map((fileUrl) => {
          const fileType = getFileTypeFromUrlExt(fileUrl)

          if (!fileType) urlsMissingType.push(fileUrl)

          return [fileUrl, { src: fileUrl, type: fileType }]
        }),
      )

      Promise.all(
        urlsMissingType.map(async (urlMissingType) => {
          let file = parsedFiles.find((file) => file.src === urlMissingType)

          if (!file) {
            const fileType = await getFileTypeFromUrlServer(
              urlMissingType,
              signal,
            )

            file = {
              src: urlMissingType,
              type: fileType,
            }
          }

          return file
        }),
      ).then((filesWithType) => {
        filesWithType.forEach((fileWithType) =>
          filesMap.set(fileWithType.src, fileWithType),
        )
        setParsedFiles(Array.from(filesMap.values()))
      })

      return () => controller.abort()
    }
  }, [files])

  return (
    <LightboxContextProvider
      onClickFile={(fileUrl) => {
        if (!lightboxCtx?.open) return

        const fileIndex = files.indexOf(fileUrl)

        // This can happen if an Image is opened and the fileUrl doesn't exist in props.files
        if (fileIndex === -1) {
          lightboxCtx.open(
            [
              { src: fileUrl, type: getFileTypeFromUrlExt(fileUrl) },
              ...parsedFiles,
            ],
            0,
          )
        } else {
          lightboxCtx.open(parsedFiles, fileIndex)
        }
      }}
    >
      {children}
    </LightboxContextProvider>
  )
}
