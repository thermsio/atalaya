import React, { useState } from 'react'
import { Image } from '../Image'
import { LightboxContextGroup } from './context/LightboxContext'
import { Modal } from '../Modals/Modal'
import { Button } from '../Button'

const files = [
  'https://images.unsplash.com/photo-1662978857144-6c3cd4108ebe?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1936&q=80',
  'https://www.lightgalleryjs.com/videos/video1.mp4',
  'https://www.orimi.com/pdf-test.pdf',
  'https://filesamples.com/samples/document/xls/sample3.xls',
]

const context = (
  <LightboxContextGroup files={files}>
    {files.map((file) => (
      <Image lightbox src={file} key={file} />
    ))}
  </LightboxContextGroup>
)

const imageLightbox = (
  <Image
    src="https://images.unsplash.com/photo-1662978857144-6c3cd4108ebe?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1936&q=80"
    lightbox
  />
)

const fileUrls = [
  'https://therms-files.s3.amazonaws.com/6181fa9f6708c603a0ec417f.jpg',
  'https://therms-files.s3.amazonaws.com/6181fabd59784503883ceddd.jpg',
  'https://therms-files.s3.amazonaws.com/6181faed6708c603a0ec4197.jpg',
  'https://therms-files.s3.amazonaws.com/6181fb6b6708c603a0ec41be.jpg',
  'https://therms-files.s3.amazonaws.com/6255fb01de555031049d837b.docx',
]

const test = (
  <LightboxContextGroup files={fileUrls}>
    <div className="flex space-x-xs">
      {fileUrls.map((file) => (
        <Image lightbox src={file} key={file} thumbnail />
      ))}
    </div>
  </LightboxContextGroup>
)

const nestedContexts = () => {
  return (
    <LightboxContextGroup files={files}>
      <LightboxContextGroup files={fileUrls}>
        <div className="flex space-x-xs">
          {fileUrls.map((file) => (
            <Image lightbox src={file} key={file} thumbnail />
          ))}
        </div>
      </LightboxContextGroup>
    </LightboxContextGroup>
  )
}

const insideModal = () => {
  const [isModalOpen, setIsModalOpen] = useState(true)

  return (
    <>
      <Button onClick={() => setIsModalOpen(true)}>Open Modal</Button>

      {isModalOpen && (
        <Modal escPressClose closeHandler={() => setIsModalOpen(false)}>
          <LightboxContextGroup files={fileUrls}>
            <div className="flex space-x-xs">
              {fileUrls.map((file) => (
                <Image lightbox src={file} key={file} thumbnail />
              ))}
            </div>
          </LightboxContextGroup>
        </Modal>
      )}
    </>
  )
}

export default { context, imageLightbox, test, nestedContexts, insideModal }
