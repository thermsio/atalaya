import {
  LIGHTBOX_FILE_TYPE_BY_EXTENSION,
  LIGHTBOX_FILE_TYPES,
} from '../constants'

export const getFileTypeFromUrlExt = (url: string) => {
  const extension = url.split('.').pop()

  if (extension && extension.length <= 4) {
    const normalizedExtension = extension.toLowerCase()
    return (
      LIGHTBOX_FILE_TYPE_BY_EXTENSION[normalizedExtension] ||
      normalizedExtension
    )
  }
}

export const getFileTypeFromUrlServer = (
  url: string,
  abortSignal,
): Promise<LIGHTBOX_FILE_TYPES | string> => {
  return fetch(url, { method: 'HEAD', mode: 'cors', signal: abortSignal })
    .then((response) => {
      const contentType = response.headers.get('content-type')

      if (!contentType) return
      if (contentType?.includes('image')) return LIGHTBOX_FILE_TYPES.IMAGE
      if (contentType?.includes('video')) return LIGHTBOX_FILE_TYPES.VIDEO
      if (contentType?.includes('pdf')) return LIGHTBOX_FILE_TYPES.PDF
      return contentType
    })
    .catch(() => getFileTypeFromUrlExt(url))
}
