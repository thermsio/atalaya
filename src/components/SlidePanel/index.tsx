import React from 'react'
import classNameParser from 'classnames'
import { Transition } from 'react-transition-group'
import { Button } from '../Button'
import { Inline } from '../../layout/Inline'
import { Icon } from '../Icon'
import { usePageScrollLock } from '../../hooks/usePageScrollLock'

interface PanelProps {
  children?: React.ReactNode
  /** Custom close icon, if not provided a default will be used */
  closeIcon?: React.ReactNode
  /** Callback that will be fired when close button is pressed, should set `active` status to false. */
  onClose?: () => void
  /** Element that will be displayed alongside the close button  */
  title?: React.ReactNode
}

const Panel: React.FC<PanelProps> = ({
  children,
  closeIcon,
  onClose,
  title,
}) => {
  return (
    <div className="flex h-full flex-col bg-surface shadow-xl">
      {(!!onClose || !!title) && (
        <Inline
          alignY="start"
          alignX="between"
          padding="lg"
          space="base"
          width="full"
        >
          {typeof title === 'string' ? (
            <h2 className="text-lg font-medium" id="slide-over-title">
              {title}
            </h2>
          ) : (
            <div id="slide-over-title">{title}</div>
          )}

          {onClose && (
            <Button onClick={onClose} subtle>
              {closeIcon ? (
                closeIcon
              ) : (
                <Icon>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="currentColor"
                    viewBox="0 0 192 192"
                  >
                    <path d="M57.067 132.623c2.592 2.591 6.991 2.531 9.522 0L96 103.272l29.351 29.351c2.531 2.531 6.93 2.591 9.522 0 2.591-2.592 2.531-6.991 0-9.523l-29.351-29.41 29.351-29.35c2.531-2.532 2.591-6.932 0-9.523-2.592-2.592-6.991-2.531-9.522 0l-29.35 29.35-29.412-29.41c-2.53-2.471-6.93-2.532-9.522.06-2.592 2.591-2.531 6.991 0 9.522l29.35 29.35-29.35 29.411c-2.531 2.532-2.592 6.931 0 9.523Z" />
                  </svg>
                </Icon>
              )}
            </Button>
          )}
        </Inline>
      )}

      <div className="relative flex-1 overflow-y-auto overscroll-contain">
        {children}
      </div>
    </div>
  )
}

export interface SlidePanelProps extends PanelProps {
  /** When true, the side panel will be present.  */
  active: boolean
  /** Content that will be displayed in the panel.  */
  children: React.ReactNode
  /** Callback that should set `active` status to false. */
  onClose: () => void
  /** If true, the rest of the app will be dimmed out by an overlay. */
  overlay?: boolean
  /** On which side of the screen the panel will be anchored. */
  position?: 'left' | 'right'
  /** Whether to show the close button on top right, defaults to true */
  showCloseButton?: boolean
  /** Panel width */
  width?: string
}

const SlidePanel = ({
  active,
  closeIcon,
  children,
  onClose,
  overlay = true,
  position = 'right',
  showCloseButton = true,
  title,
  width = '450px',
}: SlidePanelProps): React.ReactElement => {
  usePageScrollLock(overlay && active)

  return (
    <Transition
      mountOnEnter
      unmountOnExit
      /* mountOnEnter breaks enter animation, by accessing an element attribute we cause
       the mount and start of css animation to happen on different ticks fixing the issue.
       https://github.com/reactjs/react-transition-group/issues/223 */
      onEnter={(node) => node.offsetHeight}
      in={active}
      timeout={500}
    >
      {(state: string) => (
        <div className="fixed inset-0 z-backdrop overscroll-contain">
          {overlay && (
            <div
              className={classNameParser(
                {
                  'opacity-50': state === 'entering' || state === 'entered',
                  'opacity-0': state === 'exiting' || state === 'exited',
                },
                'fixed inset-0 bg-black opacity-50 transition duration-500 ease-in-out',
              )}
              onClick={onClose}
            />
          )}
          <div
            className={`fixed inset-y-0 flex max-w-full ${
              position === 'right' ? 'right-0' : 'light-0'
            }`}
          >
            <div
              className={classNameParser(
                {
                  // 'translate-x-0': state === 'entering',
                  'translate-x-full':
                    position === 'right' &&
                    (state === 'exiting' || state === 'exited'),
                  '-translate-x-full':
                    position !== 'right' &&
                    (state === 'exiting' || state === 'exited'),
                },
                'transition duration-500 ease-in-out',
              )}
              style={{ width }}
            >
              <Panel
                onClose={showCloseButton ? onClose : undefined}
                closeIcon={closeIcon}
                title={title}
              >
                {children}
              </Panel>
            </div>
          </div>
        </div>
      )}
    </Transition>
  )
}

export { SlidePanel }
