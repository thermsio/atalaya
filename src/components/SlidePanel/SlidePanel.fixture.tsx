import React, { useEffect, useState } from 'react'
import { SlidePanel, SlidePanelProps } from './index'
import { Button } from '../Button'
import { Inline } from '../../layout/Inline'
import { Modal } from '../Modals/Modal'

const SlidePanelBase = (props: Partial<SlidePanelProps>) => {
  const [active, setActive] = useState(false)

  useEffect(() => {
    const timeout = setTimeout(() => {
      if (!active) setActive(true)
    }, 1000)

    return () => clearTimeout(timeout)
  }, [active])

  return (
    <SlidePanel active={active} onClose={() => setActive(false)} {...props}>
      {props.children || (
        <div className="p-lg">
          <h2 className="text-lg text-color-subtle">Lorem Ipsum</h2>
          <p className="mt-base text-justify text-md leading-relaxed">
            Dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
            incididunt ut labore et dolore magna aliqua. Ut enim ad minim
            veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
            ea commodo consequat. Duis aute irure dolor in reprehenderit in
            voluptate velit esse cillum dolore eu fugiat nulla pariatur.
            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
            officia deserunt mollit anim id est laborum.
          </p>
          <p className="mt-sm text-right italic">Lorem Ipsum</p>
        </div>
      )}
    </SlidePanel>
  )
}

const leftPanel = () => <SlidePanelBase position="left" title="Left Panel" />
const rightPanel = () => <SlidePanelBase position="right" title="Right Panel" />
const overlay = () => <SlidePanelBase overlay title="With Overlay" />
const noHeader = () => <SlidePanelBase showCloseButton={false} />
const customHeader = () => {
  const [active, setActive] = useState(false)

  useEffect(() => {
    const timeout = setTimeout(() => {
      if (!active) setActive(true)
    }, 1000)

    return () => clearTimeout(timeout)
  }, [active])

  return (
    <SlidePanelBase
      active={active}
      onClose={() => setActive(false)}
      showCloseButton={false}
    >
      <Inline
        alignY="start"
        alignX="between"
        padding="lg"
        space="base"
        width="full"
      >
        <div className="text-lg font-medium">Custom Header Title</div>

        <Button onClick={() => setActive(false)}>Custom Close button</Button>
        <Button variant="positive">
          Extra Button that doesn&apos;t do anything
        </Button>
      </Inline>

      <div className="p-lg">
        <h2 className="text-lg text-color-subtle">Lorem Ipsum</h2>
        <p className="mt-base text-justify text-md leading-relaxed">
          Dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
          incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
          commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
          velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
          occaecat cupidatat non proident, sunt in culpa qui officia deserunt
          mollit anim id est laborum.
        </p>
        <p className="mt-sm text-right italic">Lorem Ipsum</p>
      </div>

      <Button onClick={() => setActive(false)} variant="main" fullWidth>
        Nothing is stopping us from adding close button to the Slide Panel Body
      </Button>
    </SlidePanelBase>
  )
}
const withModal = () => {
  const [showModal, setShowModal] = useState(false)

  return (
    <SlidePanelBase>
      <div className="flex flex-col items-center p-base">
        <div>
          We had a bug in which <code>Modals</code> inside SlidePanel were being
          off centered.
        </div>

        <Button
          className="mt-lg"
          variant="main"
          onClick={() => setShowModal(true)}
        >
          Show Modal
        </Button>
      </div>

      {showModal && (
        <Modal closeHandler={() => setShowModal(false)} escPressClose>
          <div>
            If you can see the modal centered on the screen rather than inside
            the SlidePanel, the issue is solved. Else let Tom know.
          </div>
        </Modal>
      )}
    </SlidePanelBase>
  )
}

export default {
  leftPanel,
  overlay,
  rightPanel,
  noHeader,
  customHeader,
  withModal,
}
