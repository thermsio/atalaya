A panel that can appear from the sides of the screen.

It will appear when the prop `active` is true. It can be dismissed with the close button or by clicking on the area
outside the panel.

```jsx
import { Button } from '../Button'

const [active, setActive] = React.useState(false)

;<>
  <Button variant="main" onClick={() => setActive(true)}>
    Activate Panel
  </Button>

  <SlidePanel
    active={active}
    onClose={() => setActive(false)}
    title="Slide Panel"
  >
    <div className="p-lg">
      <h2 className="text-lg text-color-subtle">Lorem Ipsum</h2>
      <p className="text-md mt-base text-justify leading-relaxed">
        Dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit
        esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
        cupidatat non proident, sunt in culpa qui officia deserunt mollit anim
        id est laborum.
      </p>
      <p className="mt-sm text-right italic">Lorem Ipsum</p>
    </div>
  </SlidePanel>
</>
```

### No header

If you set the `showCloseButton` prop to `false` and do not provide a `title`, the header section will not be rendered.
This is useful if you want to provide a custom header as part of `children`.

```jsx
import { Button } from '../Button'
import { Inline } from '../../layout/Inline'

const [active, setActive] = React.useState(false)

;<>
  <Button variant="main" onClick={() => setActive(true)}>
    Activate Panel
  </Button>

  <SlidePanel
    active={active}
    onClose={() => setActive(false)}
    showCloseButton={false}
  >
    <Inline
      alignY="start"
      alignX="between"
      padding="lg"
      space="base"
      width="full"
    >
      <div className="text-lg font-medium text-color-caution">Custom Title</div>

      <Button onClick={() => setActive(false)} variant="critical">
        Custom Close
      </Button>
    </Inline>

    <div className="p-lg">
      <h2 className="text-lg text-color-subtle">Lorem Ipsum</h2>
      <p className="text-md mt-base text-justify leading-relaxed">
        Dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit
        esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
        cupidatat non proident, sunt in culpa qui officia deserunt mollit anim
        id est laborum.
      </p>
      <p className="mt-sm text-right italic">Lorem Ipsum</p>
    </div>
  </SlidePanel>
</>
```
