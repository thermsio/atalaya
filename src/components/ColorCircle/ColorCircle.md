`variant` prop to will let you select between semantic colors.

```jsx
<div className="space-x-xs">
  <ColorCircle variant="main" />
  <ColorCircle variant="neutral" />
  <ColorCircle variant="positive" />
  <ColorCircle variant="caution" />
  <ColorCircle variant="critical" />
  <ColorCircle variant="info" />
</div>
```

`color` prop will let you add any CSS valid color value, it takes precedence over `variant`.

```jsx
<div className="space-x-xs">
  <ColorCircle color="rgb(148 219 226)" />
  <ColorCircle color="hsl(139deg 77% 49%)" />
  <ColorCircle color="#890312" />
</div>
```

### Small

```jsx
<div className="space-x-xs">
  <ColorCircle size="sm" variant="main" />
  <ColorCircle size="sm" variant="neutral" />
  <ColorCircle size="sm" variant="positive" />
  <ColorCircle size="sm" variant="caution" />
  <ColorCircle size="sm" variant="critical" />
  <ColorCircle size="sm" variant="info" />
</div>
```
