import * as React from 'react'
import classnames from 'classnames'
import { SemanticVariant } from '../../constants'

export interface ColorCircleProps {
  color?: string
  marginLeft?: boolean
  marginRight?: boolean
  /* If no color passed or is undefined it will still use the space */
  preserveSpace?: boolean
  size?: 'sm'
  variant?: SemanticVariant
}

const ColorCircle = ({
  color,
  marginLeft,
  marginRight,
  preserveSpace = true,
  size,
  variant,
}: ColorCircleProps): React.ReactElement | null => {
  if (!color && !variant && !preserveSpace) return null

  return (
    <span
      className={classnames(`inline-block rounded-full`, {
        'ml-xs': marginLeft,
        'mr-xs': marginRight,
        'pb-sm pr-sm': !size,
        'pb-xs pr-xs': size === 'sm',
        [`bg-${variant}`]: !color && variant,
      })}
      style={{
        backgroundColor: color,
      }}
    />
  )
}

ColorCircle.displayName = 'ColorCircle'

export { ColorCircle }

// Purge CSS

// bg-main bg-neutral bg-caution bg-critical bg-info
