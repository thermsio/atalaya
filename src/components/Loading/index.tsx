import React, { useCallback, useMemo } from 'react'
import classNameParser from 'classnames'

import './Loading.css'

export interface LoadingProps {
  className?: string
  /** If `true` does not take up UI space */
  hidden?: boolean
  /** Loading message */
  message?: React.ReactNode
  /** Position of `message` relative to spinner. Defaults to `vertical` on overlay mode and `horizontal` in regular mode.  */
  messagePosition?: 'vertical' | 'horizontal'
  /** Create an overlay over the closest relatively positioned parent. */
  overlay?: boolean
  /** It's size, matches text size */
  size?: 'sm' | 'base' | 'lg' | 'xl' | 'xxl'
  /** If `true` will not show the loader but will take up the required UI space */
  visible?: boolean
}

const Loading = ({
  className,
  hidden,
  message,
  messagePosition,
  overlay,
  size,
  visible = true,
}: LoadingProps): React.ReactElement => {
  const spinnerSettings = useMemo(() => {
    // offset: half of stroke-width
    if (size === 'xxl') return { viewBox: 100, offset: 5, class: 'spinner-xxl' }
    if (size === 'xl') return { viewBox: 48, offset: 3.5, class: 'spinner-xl' }
    if (size === 'lg') return { viewBox: 24, offset: 2, class: 'spinner-lg' }
    if (size === 'sm') return { viewBox: 12, offset: 1.5, class: 'spinner-sm' }
    return { viewBox: 32, offset: 3 }
  }, [size])

  const SpinnerIcon = useCallback(
    () => (
      <svg
        className={classNameParser('spinner', spinnerSettings.class, className)}
        viewBox={`0 0 ${spinnerSettings.viewBox} ${spinnerSettings.viewBox}`}
        xmlns="http://www.w3.org/2000/svg"
      >
        <circle
          className="segment segment-background"
          cx={spinnerSettings.viewBox / 2}
          cy={spinnerSettings.viewBox / 2}
          r={spinnerSettings.viewBox / 2 - spinnerSettings.offset}
        />
        <circle
          className="segment segment-foreground"
          cx={spinnerSettings.viewBox / 2}
          cy={spinnerSettings.viewBox / 2}
          r={spinnerSettings.viewBox / 2 - spinnerSettings.offset}
        />
      </svg>
    ),
    [className, spinnerSettings],
  )

  if (overlay)
    return (
      <div
        className={classNameParser(
          {
            hidden,
            invisible: !visible,
            'flex-col': messagePosition !== 'horizontal',
          },
          'absolute inset-0 z-backdrop flex items-center justify-center',
        )}
      >
        <div className="absolute inset-0 bg-background opacity-80" />

        <SpinnerIcon />

        {!!message && (
          <div
            className={`relative overflow-hidden rounded-lg p-xxs ${
              messagePosition === 'horizontal' ? 'ml-xs' : 'mt-xs'
            }`}
          >
            <div className="relative z-backdrop">{message}</div>

            {typeof message === 'string' && (
              <div className="absolute inset-0 bg-background opacity-60" />
            )}
          </div>
        )}
      </div>
    )

  return (
    <span
      className={classNameParser(
        {
          hidden,
          invisible: !visible,
          'flex-col': messagePosition === 'vertical',
        },
        'inline-flex items-center',
      )}
    >
      <SpinnerIcon />

      {!!message && (
        <span className={messagePosition === 'vertical' ? 'mt-xs' : 'ml-xs'}>
          {message}
        </span>
      )}
    </span>
  )
}

export { Loading }
