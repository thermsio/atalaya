import React from 'react'
import { Loading } from './index'

const lorem =
  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas at blandit tortor. Mauris luctus nisi ut leo scelerisque, id lobortis turpis vestibulum. Donec nisi risus, consequat vitae feugiat sit amet, accumsan eget enim. Fusce eu nibh porta, vestibulum libero at, mattis nibh. Nam iaculis massa non velit tempus, et commodo ex fermentum. Maecenas sagittis sit amet nisi quis aliquam. Donec tincidunt sagittis ligula. Etiam id enim id enim ultrices convallis. Sed ac erat augue.'

const overlay = (
  <div className="relative rounded bg-surface p-base">
    <Loading
      overlay
      message="This seems to be taking longer than expected."
      size="xxl"
    />

    <p>{lorem}</p>
  </div>
)

const overlayHorizontal = (
  <div className="relative rounded bg-surface p-base">
    <Loading
      overlay
      message="This seems to be taking longer than expected."
      messagePosition="horizontal"
      size="xxl"
    />

    <p>{lorem}</p>
  </div>
)

const inline = (
  <Loading message="This seems to be taking longer than expected." size="xxl" />
)

const inlineVertical = (
  <Loading
    message="This seems to be taking longer than expected."
    messagePosition="vertical"
    size="xxl"
  />
)

export default { inline, inlineVertical, overlay, overlayHorizontal }
