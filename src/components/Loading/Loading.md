Spinner color will inherit current text color

```jsx
<div className="flex items-center justify-center space-x-lg">
  <Loading className="text-color-main" size="xxl" />
  <Loading className="text-color-info" size="xl" />
  <Loading className="text-color-positive" size="lg" />
  <Loading className="text-color-caution" />
  <Loading className="text-color-critical" size="sm" />
</div>
```

### Visibility

`hidden=true` will not render any loading element in the DOM where `visible=false` will not show the loading
icon/animation, but it will use up the UI space.

```jsx
<div className="flex items-center justify-center space-x-lg">
  ABC<Loading hidden={true}/>DEFG
</div>

<div className="flex items-center justify-center space-x-lg">
  ABC<Loading visible={true}/>DEFG
</div>
```

### Overlay

```jsx
import { Card } from '../Card'

;<Card className="relative">
  <Loading overlay size="xxl" />

  <div className="text-xl font-bold">Lorem Ipsum</div>
  <div className="mt-sm">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
    non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
  </div>
  <div className="italic mt-xs text-color-subtle">
    Sapien et ligula ullamcorper malesuada proin libero nunc consequat interdum.
  </div>
</Card>
```

### Message

You can add custom messages next to the spinner. If you use a string the component will automatically style it to make
it fit the surroundings.

```jsx
import { Card } from '../Card'

;<div className="space-y-base">
  {/* Overlay Vertical*/}
  <Card className="relative">
    <Loading
      overlay
      size="xxl"
      message="Loading in progress. Please stand by."
    />

    <div className="text-xl font-bold">Lorem Ipsum</div>
    <div className="mt-sm">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
      veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
      commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
      velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
      cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
      est laborum.
    </div>
    <div className="italic mt-xs text-color-subtle">
      Sapien et ligula ullamcorper malesuada proin libero nunc consequat
      interdum.
    </div>
  </Card>

  {/* Overlay Horizontal*/}
  <Card className="relative">
    <Loading
      overlay
      size="xxl"
      message="Loading in progress. Please stand by."
      messagePosition="horizontal"
    />

    <div className="text-xl font-bold">Lorem Ipsum</div>
    <div className="mt-sm">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
      veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
      commodo consequat.
    </div>
    <div className="italic mt-xs text-color-subtle">
      Sapien et ligula ullamcorper malesuada proin libero nunc consequat
      interdum.
    </div>
  </Card>

  {/* Inline Vertical*/}
  <div className="flex justify-center">
    <Loading
      message="Loading in progress. Please stand by."
      messagePosition="vertical"
    />
  </div>

  {/* Inline Vertical*/}
  <div className="flex justify-center">
    <Loading message="Loading in progress. Please stand by." />
  </div>
</div>
```

You can also add more custom components to create more complex messages.

```jsx
import { Card } from '../Card'
import { Button } from '../Button'

const [isShowingOverlay, setIsShowingOverlay] = React.useState(true)

;<Card className="relative">
  <Loading
    overlay
    size="xxl"
    hidden={!isShowingOverlay}
    message={
      <div className="flex flex-col justify-center space-y-base">
        <p className="text-center">
          Loading is taking more time than expected. <br />
          Do you want to cancel?
        </p>

        <Button onClick={() => setIsShowingOverlay(false)}>Cancel</Button>
      </div>
    }
  />

  <div className="text-xl font-bold">Lorem Ipsum</div>
  <div className="mt-sm">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
    non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
  </div>
  <div className="italic mt-xs text-color-subtle">
    Sapien et ligula ullamcorper malesuada proin libero nunc consequat interdum.
  </div>
</Card>
```
