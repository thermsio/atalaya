import React from 'react'
import classNameParser from 'classnames'
import { AvatarImage, AvatarImageProps } from './components/AvatarImage'
import { AvatarSymbol, AvatarSymbolProps } from './components/AvatarSymbol'
import { SemanticVariant } from '../../constants'

export interface AvatarCommonDisplayProps {
  /** Make avatar a circle */
  circular?: boolean
  size?: 'xxs' | 'xs' | 'sm' | 'base' | 'lg' | 'xl' | 'xxl'
  onClick?: () => void
}

export interface AvatarProps
  extends AvatarCommonDisplayProps,
    AvatarImageProps,
    AvatarSymbolProps {
  /** Shows a status indicator using a custom color value. Has precedence over statusVariant */
  statusColor?: string
  /** Shows a status indicator using a semantic color */
  statusVariant?: SemanticVariant
}

const Avatar = ({
  circular,
  lightbox,
  lightboxSrc,
  onClick,
  size = 'base',
  statusColor,
  statusVariant,
  symbol,
  url,
}: AvatarProps): React.ReactElement => (
  <div className="relative">
    <div
      className={`inline-block bg-surface ${
        circular ? 'rounded-full' : 'rounded'
      }`}
    >
      {url ? (
        <AvatarImage
          circular={circular}
          lightbox={lightbox}
          lightboxSrc={lightboxSrc}
          onClick={onClick}
          size={size}
          url={url}
        />
      ) : (
        <AvatarSymbol
          circular={circular}
          onClick={onClick}
          size={size}
          symbol={symbol || '?'}
        />
      )}
    </div>

    {(!!statusVariant || !!statusColor) && (
      <span
        className={classNameParser(
          {
            'bg-caution': statusVariant === 'caution',
            'bg-critical': statusVariant === 'critical',
            'bg-info': statusVariant === 'info',
            'bg-main': statusVariant === 'main',
            'bg-neutral': statusVariant === 'neutral',
            'bg-positive': statusVariant === 'positive',
            'h-3 w-3': size === 'xs',
            'h-4 w-4': size === 'sm' || size === 'base',
            'h-5 w-5': size === 'lg' || size === 'xl',
            'h-6 w-6': size === 'xxl',
            '-translate-x-1 -translate-y-1 transform': !circular,
          },
          'absolute left-0 top-0 block rounded-full ring-2 ring-inset ring-surface',
        )}
        style={statusColor ? { backgroundColor: statusColor } : undefined}
      />
    )}
  </div>
)

export { Avatar }
