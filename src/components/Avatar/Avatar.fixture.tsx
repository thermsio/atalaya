import React from 'react'
import { Avatar } from './index'

const base = (): React.ReactElement => {
  const avatars = (
    <div className="space-y-base p-lg">
      <div className="space-x-base">
        <Avatar
          size="xs"
          statusVariant="caution"
          url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
          circular
        />
        <Avatar
          size="sm"
          statusVariant="caution"
          url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
          circular
        />
        <Avatar
          statusVariant="caution"
          url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
          circular
        />
        <Avatar
          size="lg"
          statusVariant="caution"
          url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
          circular
        />
        <Avatar
          size="xl"
          statusVariant="caution"
          url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
          circular
        />
      </div>
      <div className="space-x-base">
        <Avatar
          size="xs"
          statusVariant="caution"
          url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
        />
        <Avatar
          size="sm"
          statusVariant="caution"
          url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
        />
        <Avatar
          statusVariant="caution"
          url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
        />
        <Avatar
          size="lg"
          statusVariant="caution"
          url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
        />
        <Avatar
          size="xl"
          statusVariant="caution"
          url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
        />
      </div>
    </div>
  )

  return (
    <div className="space-y-base">
      <div>{avatars}</div>
      <div className="bg-surface-subtle">{avatars}</div>
      <div className="bg-surface">{avatars}</div>
      <div className="bg-surface-strong">{avatars}</div>
    </div>
  )
}

const symbol = (
  <div className="bg-surface p-lg">
    <Avatar size="xs" statusVariant="caution" symbol="CR" circular />
    <Avatar size="sm" statusVariant="caution" symbol="CR" circular />
    <Avatar statusVariant="caution" symbol="CR" circular />
    <Avatar size="lg" statusVariant="caution" symbol="CR" circular />
    <Avatar size="xl" statusVariant="caution" symbol="CR" circular />
  </div>
)

const onClick = (
  <div className="space-y-base p-lg">
    <Avatar
      statusVariant="caution"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
      circular
      onClick={() => alert('avatar clicked')}
    />
    <Avatar
      statusVariant="caution"
      symbol="CR"
      circular
      onClick={() => alert('avatar clicked')}
    />
  </div>
)

export default {
  base,
  symbol,
  onClick,
}
