### Using Image

```jsx
  <div className="flex items-end space-x-lg">
  <Avatar
    size="xs"
    url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
  />
  <Avatar
    size="sm"
    url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
  />
  <Avatar
    size="base"
    url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU" />
  <Avatar
    size="lg"
    url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
  />
  <Avatar
    size="xl"
    url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
  />
</div>

<div className="flex items-end mt-base space-x-lg">
  <Avatar
    circular
    size="xs"
    url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
  />
  <Avatar
    circular
    size="sm"
    url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
  />
  <Avatar
    circular
    size="base"
    url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU" />
  <Avatar
    circular
    size="lg"
    url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
  />
  <Avatar
    circular
    size="xl"
    url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
  />
</div>
```

### Using Symbols

`symbol` prop will be ignored if `url` is present.

```jsx
<div>
  <div className="flex items-end space-x-lg">
    <Avatar size="xs" symbol="AT" />
    <Avatar size="sm" symbol="AT" />
    <Avatar size="base" symbol="AT" />
    <Avatar size="lg" symbol="AT" />
    <Avatar size="xl" symbol="AT" />
  </div>

  <div className="mt-base flex items-end space-x-lg">
    <Avatar circular size="xs" symbol="AT" />
    <Avatar circular size="sm" symbol="AT" />
    <Avatar circular size="base" symbol="AT" />
    <Avatar circular size="lg" symbol="AT" />
    <Avatar circular size="xl" symbol="AT" />
  </div>
</div>
```

### Semantic Variants

```jsx
<div>
  <div className="flex items-end space-x-lg">
    <Avatar
      statusVariant="main"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />

    <Avatar
      statusVariant="neutral"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />

    <Avatar
      statusVariant="info"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />

    <Avatar
      statusVariant="positive"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />

    <Avatar
      statusVariant="caution"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />

    <Avatar
      statusVariant="critical"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />
  </div>
</div>
```

### Custom Status Color

You can define a custom status color by using the `statusColor` prop. If both `statusColor` and `statusVariant` are
present on the component, `statusVariant` will be ignored.

You may use any valid CSS color format.

```jsx
<div>
  <div className="flex items-end space-x-lg">
    <Avatar
      statusColor="#36f443"
      statusVariant="main"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />

    <Avatar
      statusColor="slateblue"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />

    <Avatar
      statusColor="rgb(255, 150, 75)"
      url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
    />
  </div>
</div>
```
