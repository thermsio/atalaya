import React from 'react'
import { AvatarCommonDisplayProps } from '../index'
import { Image } from '../../Image'
import classNameParser from 'classnames'

export interface AvatarImageProps extends AvatarCommonDisplayProps {
  /** If true will open image inside lightbox on click  */
  lightbox?: boolean
  /** The url that will be used when lightbox is open. If undefined, `src` will be used.   */
  lightboxSrc?: string
  /** URL that will be used as the source of the image for the avatar. Has precedence over symbol. */
  url?: string
}

const AvatarImage = ({
  url,
  circular,
  lightbox,
  lightboxSrc,
  onClick,
  size,
}: AvatarImageProps): React.ReactElement | null => {
  if (!url) return null

  return (
    <Image
      src={url}
      fit="cover"
      circular={circular}
      lightbox={lightbox}
      lightboxSrc={lightboxSrc}
      rounded={!circular}
      className={classNameParser(
        {
          'h-6 w-6': size === 'xxs',
          'h-8 w-8': size === 'xs',
          'h-10 w-10': size === 'sm',
          'h-12 w-12': size === 'base',
          'h-14 w-14': size === 'lg',
          'h-16 w-16': size === 'xl',
        },
        'inline-block max-w-none',
      )}
      onClick={onClick}
    />
  )
}

export { AvatarImage }
