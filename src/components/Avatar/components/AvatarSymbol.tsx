import React from 'react'
import { AvatarCommonDisplayProps } from '../index'
import classNameParser from 'classnames'

export interface AvatarSymbolProps extends AvatarCommonDisplayProps {
  /** Short string to use instead of an image. To avoid clipping, don't use more than 3 characters.  */
  symbol?: string
}

const AvatarSymbol = ({
  symbol,
  circular,
  onClick,
  size,
}: AvatarSymbolProps): React.ReactElement => (
  <div
    className={classNameParser(
      {
        'rounded-full': circular,
        rounded: !circular,
        'h-6 w-6': size === 'xxs',
        'h-8 w-8 text-sm': size === 'xs',
        'h-10 w-10 text-sm': size === 'sm',
        'h-12 w-12': size === 'base',
        'h-14 w-14 text-lg': size === 'lg',
        'h-16 w-16 text-lg': size === 'xl',
        'cursor-pointer transition-[filter] hover:brightness-125 active:brightness-75':
          !!onClick,
      },
      'flex items-center justify-center overflow-hidden bg-neutral font-medium text-color-semantic',
    )}
    onClick={onClick}
  >
    <div>{symbol}</div>
  </div>
)

export { AvatarSymbol }
