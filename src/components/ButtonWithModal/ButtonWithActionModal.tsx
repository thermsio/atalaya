import React from 'react'
import { ActionModal, ActionModalProps } from '../Modals/ActionModal'
import { Button, ButtonProps } from '../Button'

export type ButtonWithActionModalProps = Pick<
  ActionModalProps,
  'body' | 'onAction' | 'title' | 'variant' | 'icon'
> &
  Omit<ButtonProps, 'children' | 'onClick' | 'title'> & {
    actionText: React.ReactNode
    className?: string
    children?: React.ReactNode
  }

function ButtonWithActionModal({
  actionText,
  body,
  children,
  icon,
  onAction,
  title,
  variant,
  ...buttonProps
}: ButtonWithActionModalProps) {
  const [show, setShow] = React.useState(false)

  return (
    <>
      <Button onClick={() => setShow(true)} variant={variant} {...buttonProps}>
        {children || actionText}
      </Button>

      {show && (
        <ActionModal
          actionText={actionText}
          body={body}
          icon={icon}
          escPressClose
          onCancel={() => setShow(false)}
          onAction={async () => {
            try {
              await onAction()
            } finally {
              setShow(false)
            }
          }}
          title={title}
          variant={variant}
        />
      )}
    </>
  )
}

export { ButtonWithActionModal }
