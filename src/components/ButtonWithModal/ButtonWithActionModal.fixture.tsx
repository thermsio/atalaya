import React from 'react'
import { ButtonWithActionModal } from './ButtonWithActionModal'
import { ButtonGroup } from '../ButtonGroup'

const base = (
  <ButtonWithActionModal
    actionText=""
    body={''}
    onAction={function (): void | Promise<any> {
      throw new Error('Function not implemented.')
    }}
  />
)

const withButtonGroup = (
  <ButtonGroup>
    <ButtonWithActionModal
      actionText=""
      body={''}
      onAction={function (): void | Promise<any> {
        throw new Error('Function not implemented.')
      }}
    >
      Start
    </ButtonWithActionModal>
    <ButtonWithActionModal
      actionText=""
      body={''}
      onAction={function (): void | Promise<any> {
        throw new Error('Function not implemented.')
      }}
    >
      Middle
    </ButtonWithActionModal>
    <ButtonWithActionModal
      actionText=""
      body={''}
      onAction={function (): void | Promise<any> {
        throw new Error('Function not implemented.')
      }}
    >
      Last
    </ButtonWithActionModal>
  </ButtonGroup>
)

export default { base, withButtonGroup }
