import React, { useCallback } from 'react'
import { useToggle } from 'ahooks'
import { Button, ButtonProps } from '../Button'
import { ModalProps } from '../Modals/Modal'
import { ActionModalProps } from '../Modals/ActionModal'

type WithElement = {
  modalComponent?: never
  /** You can either use one of our Modals or a function accepting the closeHandler. If you are using a Modal element, do not add `closeHandler` to it, it will be provided by ButtonWithModal.  */
  modalElement:
    | React.ReactElement<ModalProps | ActionModalProps>
    | (({ closeHandler }: { closeHandler: () => void }) => React.ReactNode)
}

type WithComponent = {
  modalComponent: React.ComponentType<{ closeHandler: () => void }>
  modalElement?: never
}

export type ButtonWithModalProps = ButtonProps & {
  /** Optional callback when clicked */
  onClick?: () => void
  /** Optional callback when modal closed */
  onClose?: () => void
} & (WithElement | WithComponent)

function ButtonWithModal({
  children,
  modalComponent: ModalComponent,
  modalElement,
  onClick,
  onClose,
  ...buttonProps
}: ButtonWithModalProps) {
  const [show, { toggle: toggleShow }] = useToggle()

  const closeHandler = useCallback(() => {
    onClose?.()
    toggleShow()
  }, [onClose])

  return (
    <>
      <Button
        {...buttonProps}
        onClick={() => {
          if (onClick) onClick()

          toggleShow()
        }}
      >
        {children}
      </Button>

      {show &&
        (ModalComponent ? (
          <ModalComponent closeHandler={closeHandler} />
        ) : typeof modalElement === 'function' ? (
          modalElement({ closeHandler })
        ) : modalElement ? (
          React.cloneElement(modalElement, { closeHandler })
        ) : null)}
    </>
  )
}

export { ButtonWithModal }
