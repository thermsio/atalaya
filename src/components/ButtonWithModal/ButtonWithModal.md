This component is a convenience for common use-cases where a button opens a modal when clicked.

The `<ButtonWithModal />` component handles passing the `closeHandler` prop to the `modalElement` prop.

```jsx padded
import { Modal } from '../Modals/Modal'
import { ButtonWithActionModal } from './ButtonWithActionModal'

;<div className="flex items-end space-x-sm">
  <ButtonWithModal modalElement={<Modal header="Test ButtonWithModal"></Modal>}>
    Open Modal
  </ButtonWithModal>

  <ButtonWithActionModal
    actionText="Save Something"
    body={'Are you sure you want to save something?'}
    onAction={function () {
      alert('Action approved by user...')
    }}
  >
    Save
  </ButtonWithActionModal>
</div>
```
