import React from 'react'
import { Button } from '../../Button'
import classNames from 'classnames'

interface WeekDayProps {
  isActive?: boolean
  label: string
  onClick?: () => void
}

function WeekDay({ isActive, label, onClick }: WeekDayProps) {
  if (onClick) {
    return (
      <Button
        active={isActive}
        outline
        onClick={onClick}
        variant={isActive ? 'main' : 'neutral'}
      >
        <div className="min-w-[2rem]">{label}</div>
      </Button>
    )
  }

  return (
    <div
      className={classNames(
        {
          'bg-main-faded text-color-main': isActive,
          'bg-neutral-faded text-color-neutral-faded': !isActive,
        },
        'min-w-[2rem] px-xs py-xxs text-center font-bold first:rounded-l last:rounded-r',
      )}
    >
      {label}
    </div>
  )
}

export { WeekDay }
