import React, { useState } from 'react'
import { WeekDays } from './index'
import { useSelect, useValue } from 'react-cosmos/client'

const defaultWeek = {
  sunday: false,
  monday: false,
  tuesday: false,
  wednesday: false,
  thursday: false,
  friday: false,
  saturday: false,
}

const base = () => {
  const [firstDayOfWeek] = useSelect('First Day of Week', {
    options: ['monday', 'sunday'],
    defaultValue: 'monday',
  })
  const [isInteractive] = useValue<boolean>('isInteractive', {
    defaultValue: true,
  })

  const [week, setWeek] = useState(defaultWeek)

  return (
    <WeekDays
      firstDayOfWeek={firstDayOfWeek}
      value={week}
      onChangeValue={isInteractive ? setWeek : undefined}
    />
  )
}

export default { base }
