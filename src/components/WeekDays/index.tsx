import React, { useCallback } from 'react'
import { WeekDay } from './components/WeekDay'
import { ButtonGroup } from '../ButtonGroup'

export type Week = {
  sunday: boolean
  monday: boolean
  tuesday: boolean
  wednesday: boolean
  thursday: boolean
  friday: boolean
  saturday: boolean
}

export interface WeekDaysProps {
  disabled?: boolean
  firstDayOfWeek?: 'monday' | 'sunday'
  fullWidth?: boolean
  onChangeValue?: (newWeekState: Week) => void
  value: Week
}

const WeekDays = ({
  disabled,
  firstDayOfWeek = 'monday',
  fullWidth,
  onChangeValue,
  value,
}: WeekDaysProps) => {
  const handleChange = useCallback(
    (
      dayKey:
        | 'sunday'
        | 'monday'
        | 'tuesday'
        | 'wednesday'
        | 'thursday'
        | 'friday'
        | 'saturday',
    ) => {
      if (!onChangeValue) return
      const newValue = { ...value, [dayKey]: !value[dayKey] }
      onChangeValue(newValue)
    },
    [value, onChangeValue],
  )

  if (!onChangeValue)
    return (
      <div className="flex gap-x-px">
        {firstDayOfWeek === 'sunday' && (
          <WeekDay isActive={value.sunday} label="Su" />
        )}

        <WeekDay isActive={value.monday} label="Mo" />

        <WeekDay isActive={value.tuesday} label="Tu" />

        <WeekDay isActive={value.wednesday} label="We" />

        <WeekDay isActive={value.thursday} label="Th" />

        <WeekDay isActive={value.friday} label="Fr" />

        <WeekDay isActive={value.saturday} label="Sa" />

        {firstDayOfWeek !== 'sunday' && (
          <WeekDay isActive={value.sunday} label="Su" />
        )}
      </div>
    )

  return (
    <ButtonGroup disabled={disabled} fullWidth={fullWidth}>
      {firstDayOfWeek === 'sunday' && (
        <WeekDay
          isActive={value.sunday}
          label="Su"
          onClick={() => handleChange('sunday')}
        />
      )}

      <WeekDay
        isActive={value.monday}
        label="Mo"
        onClick={() => handleChange('monday')}
      />

      <WeekDay
        isActive={value.tuesday}
        label="Tu"
        onClick={() => handleChange('tuesday')}
      />

      <WeekDay
        isActive={value.wednesday}
        label="We"
        onClick={() => handleChange('wednesday')}
      />

      <WeekDay
        isActive={value.thursday}
        label="Th"
        onClick={() => handleChange('thursday')}
      />

      <WeekDay
        isActive={value.friday}
        label="Fr"
        onClick={() => handleChange('friday')}
      />

      <WeekDay
        isActive={value.saturday}
        label="Sa"
        onClick={() => handleChange('saturday')}
      />

      {firstDayOfWeek !== 'sunday' && (
        <WeekDay
          isActive={value.sunday}
          label="Su"
          onClick={() => handleChange('sunday')}
        />
      )}
    </ButtonGroup>
  )
}

export { WeekDays }
