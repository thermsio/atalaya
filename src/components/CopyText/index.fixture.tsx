import React from 'react'
import { CopyText } from './index'

const base = <CopyText value="Copy this text">Copy this text</CopyText>

const alwaysShow = (
  <CopyText value="Copy this text" alwaysShow>
    Copy this text
  </CopyText>
)

const longText = (
  <CopyText value="Copy">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Non enim praesent
    elementum facilisis. Eget nunc lobortis mattis aliquam faucibus purus in.
    Iaculis eu non diam phasellus vestibulum lorem sed. A cras semper auctor
    neque vitae tempus quam pellentesque nec. Sit amet consectetur adipiscing
    elit duis tristique sollicitudin nibh sit. Ultrices sagittis orci a
    scelerisque purus. Elit pellentesque habitant morbi tristique senectus et.
    Iaculis urna id volutpat lacus laoreet non curabitur gravida. Netus et
    malesuada fames ac turpis egestas sed. Lorem donec massa sapien faucibus et
    molestie. Mattis ullamcorper velit sed ullamcorper. Neque convallis a cras
    semper auctor. Gravida neque convallis a cras semper auctor neque. Velit sed
    ullamcorper morbi tincidunt ornare massa. Et ligula ullamcorper malesuada
    proin libero.
  </CopyText>
)

const displayValue = (
  <CopyText
    displayValue
    value="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Non enim praesent
    elementum facilisis. Eget nunc lobortis mattis aliquam faucibus purus in.
    Iaculis eu non diam phasellus vestibulum lorem sed. A cras semper auctor
    neque vitae tempus quam pellentesque nec. Sit amet consectetur adipiscing
    elit duis tristique sollicitudin nibh sit. Ultrices sagittis orci a
    scelerisque purus. Elit pellentesque habitant morbi tristique senectus et.
    Iaculis urna id volutpat lacus laoreet non curabitur gravida. Netus et
    malesuada fames ac turpis egestas sed. Lorem donec massa sapien faucibus et
    molestie. Mattis ullamcorper velit sed ullamcorper. Neque convallis a cras
    semper auctor. Gravida neque convallis a cras semper auctor neque. Velit sed
    ullamcorper morbi tincidunt ornare massa. Et ligula ullamcorper malesuada
    proin libero."
  ></CopyText>
)

export default {
  base,
  displayValue,
  longText,
  alwaysShow,
}
