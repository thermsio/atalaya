CopyText can wrap around some content to provide a prompt to copy some arbitrary piece of text to the Clipboard

```jsx
<CopyText value="Some String to Copy">Some String to Copy</CopyText>
```

#### Always Show
Permanently show the clipboard icon by passing alwaysShow prop

```jsx
<CopyText value="Some String to Copy" alwaysShow>Some String to Copy</CopyText>
```
