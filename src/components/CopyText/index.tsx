import React, { ReactNode, useEffect } from 'react'
import classNameParser from 'classnames'
import useCopy from '@react-hook/copy'

import { Icon } from '../Icon'

import './CopyText.css'

export interface CopyTextProps {
  alwaysShow?: boolean
  children?: ReactNode | ReactNode[]
  copiedLabel?: string
  copyLabel?: string
  /** If true, the component will render the `value` prop as children text */
  displayValue?: boolean
  value?: string
}

const CopyText = ({
  alwaysShow,
  children,
  copiedLabel = 'Copied',
  copyLabel = 'Copy to Clipboard',
  displayValue,
  value = '',
}: CopyTextProps): React.ReactElement => {
  const { copied, copy, reset } = useCopy(value)

  useEffect(() => {
    if (copied) {
      const timeoutId = setTimeout(reset, 15000)
      return () => clearTimeout(timeoutId)
    }
  }, [copied, reset])

  if (displayValue) {
    if (
      (typeof value === 'string' || typeof value === 'number') &&
      `${value}`.length !== 0
    ) {
      children = <>{value}</>
    } else {
      return <></>
    }
  }

  if (!value && `${value}`.length === 0) {
    return <>{children}</>
  }

  return (
    <span
      className={classNameParser('copy-text-wrapper', {
        'copy-text-wrapper-only-hover': !alwaysShow,
      })}
    >
      {children}

      <span
        className="copy-text-button"
        onClick={(e) => {
          e.stopPropagation()
          copy()
        }}
      >
        <span className="copy-text-status">
          {copied ? copiedLabel : copyLabel}
        </span>

        <Icon
          className={classNameParser(
            { 'copy-text-icon-copied': copied },
            'copy-text-icon',
          )}
          color={copied ? 'positive' : 'subtle'}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            enableBackground="new 0 0 24 24"
            height="20px"
            viewBox="0 0 24 24"
            width="20px"
            fill="currentColor"
          >
            <g>
              <rect fill="none" height="24" width="24" />
            </g>
            <g>
              <path d="M15,20H5V7c0-0.55-0.45-1-1-1h0C3.45,6,3,6.45,3,7v13c0,1.1,0.9,2,2,2h10c0.55,0,1-0.45,1-1v0C16,20.45,15.55,20,15,20z M20,16V4c0-1.1-0.9-2-2-2H9C7.9,2,7,2.9,7,4v12c0,1.1,0.9,2,2,2h9C19.1,18,20,17.1,20,16z M18,16H9V4h9V16z" />
            </g>
          </svg>
        </Icon>
      </span>
    </span>
  )
}

export { CopyText }
