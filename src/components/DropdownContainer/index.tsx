import React, { useLayoutEffect } from 'react'
import { Placement } from '../Dropdown'
import classNames from 'classnames'
import {
  autoUpdate,
  flip,
  offset,
  shift,
  useFloating,
} from '@floating-ui/react-dom'
import { FLOATING_UI_PLACEMENT_MAP } from './constants'
import { useOnClickOutside } from '../../hooks/useOnClickOutside'

export interface DropdownContainerProps {
  children?: React.ReactNode
  className?: string
  containerRef: React.MutableRefObject<HTMLElement>
  inset?: boolean
  onClose?: () => void
  placement?: Placement
}

const DropdownContainer = ({
  children,
  className,
  containerRef,
  inset,
  onClose,
  placement = 'bottom-right',
}: DropdownContainerProps): React.ReactElement | null => {
  const { x, y, strategy, refs } = useFloating({
    placement: inset ? 'bottom' : FLOATING_UI_PLACEMENT_MAP[placement],
    whileElementsMounted: autoUpdate,
    middleware: [
      offset(({ rects }) => {
        if (inset) return -rects.reference.height
        return 5
      }),
      shift(),
      flip(),
    ],
    strategy: 'fixed',
  })

  useLayoutEffect(() => {
    refs.setReference(containerRef.current)
  }, [containerRef.current])

  useOnClickOutside(refs.floating, onClose)

  return (
    <div
      className={classNames(
        'z-tooltip w-max rounded border border-border bg-surface-subtle p-xxs text-sm shadow max-h-80 overflow-y-auto',
        className,
      )}
      style={{
        position: strategy,
        top: y ?? 0,
        left: x ?? 0,
        maxWidth: 'calc(100vw - 20px)',
        minWidth: inset
          ? containerRef.current?.getBoundingClientRect().width
          : undefined,
      }}
      ref={refs.setFloating}
    >
      {children}
    </div>
  )
}

export { DropdownContainer }
