import React, { useRef, useState } from 'react'
import { Button } from '../Button'
import { DropdownContainer } from './index'
import { useValue } from 'react-cosmos/client'

const base = () => {
  const [inset] = useValue('inset', { defaultValue: false })
  const [isOpen, setIsOpen] = useState(false)
  const containerRef = useRef<HTMLDivElement>(null)

  return (
    <div className="flex h-screen w-screen items-center justify-center">
      <div ref={containerRef}>
        <Button onClick={() => setIsOpen(true)}>Open Dropdown</Button>
      </div>

      {isOpen && (
        <DropdownContainer
          containerRef={containerRef as React.MutableRefObject<HTMLDivElement>}
          onClose={() => setIsOpen(false)}
          inset={inset}
        >
          <div className="w-[350px] min-w-max rounded bg-positive p-base">
            A Dropdown! A Dropdown! A Dropdown! A Dropdown! A Dropdown! A
            Dropdown!
          </div>
        </DropdownContainer>
      )}
    </div>
  )
}

const edgeOfScreen = () => {
  const [isOpen, setIsOpen] = useState(false)
  const containerRef = useRef<HTMLDivElement>(null)

  return (
    <>
      <div className="flex h-full w-full justify-end">
        <div className="bg-surface">
          <div className="ml-auto" ref={containerRef}>
            <Button onClick={() => setIsOpen(true)}>Open</Button>
          </div>
        </div>
      </div>

      {isOpen && (
        <DropdownContainer
          containerRef={containerRef as React.MutableRefObject<HTMLDivElement>}
          onClose={() => setIsOpen(false)}
        >
          <div className="min-w-max rounded bg-positive p-base">
            A Dropdown!
          </div>
        </DropdownContainer>
      )}
    </>
  )
}

export default {
  base,
  edgeOfScreen,
}
