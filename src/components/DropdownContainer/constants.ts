import { Placement } from '@floating-ui/react-dom'

export const FLOATING_UI_PLACEMENT_MAP: Record<string, Placement> = {
  bottom: 'bottom',
  'bottom-left': 'bottom-start',
  'bottom-right': 'bottom-end',
  left: 'left',
  right: 'right',
  top: 'top',
  'top-left': 'top-start',
  'top-right': 'top-end',
}
