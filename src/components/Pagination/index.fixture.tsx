import React from 'react'
import { Pagination } from './index'
import { useFixtureInput } from 'react-cosmos/client'

const base = () => {
  const [navPages] = useFixtureInput('navPages', 3)
  const [recordsPerPage] = useFixtureInput('recordsPerPage', 10)
  const [skippedRecords] = useFixtureInput('skippedRecords', 50)
  const [totalRecords] = useFixtureInput('totalRecords', 100)

  return (
    <Pagination
      recordsPerPage={recordsPerPage}
      sideNavPages={navPages}
      skippedRecords={skippedRecords}
      onPageClick={console.log}
      totalRecords={totalRecords}
    />
  )
}

export default base
