import React, { useMemo } from 'react'
import { Button } from '../Button'
import { ButtonGroup } from '../ButtonGroup'
import { Icon } from '../Icon'
import { Inline } from '../../layout/Inline'
import { Stack } from '../../layout/Stack'
import { HTMLSelect } from '../FormControls/HTMLSelect/HTMLSelect'
import classNames from 'classnames'

export interface PaginationProps {
  className?: string
  /** Callback receiving the new number of records that should be skipped */
  onPageClick: (newSkippedRecords) => void
  /** Callback receiving the new number of records that will be displayed. Causes a dropdown to appear. */
  onSetRecordsPerPage?: (newRecordsPerPage) => void
  /** Number of records that fit in each page.  */
  recordsPerPage?: number
  /** Number of page links to display on each side of the current page.
   *  If there are no available pages on some side,
   *  they will be moved to the other to keep the component's width consistent */
  sideNavPages?: number
  /** Extra information about which items are being displayed. */
  showDescription?: boolean
  /** Current page that's being displayed. */
  skippedRecords: number
  /** Total amount of records in the collection. */
  totalRecords: number
}

const Pagination = ({
  className,
  onPageClick,
  onSetRecordsPerPage,
  recordsPerPage = 25,
  sideNavPages = 3,
  showDescription = false,
  skippedRecords,
  totalRecords,
}: PaginationProps): React.ReactElement | null => {
  const showPerPagePicker = !!onSetRecordsPerPage && totalRecords > 10

  const pageCount = Math.ceil(totalRecords / recordsPerPage)
  const page = Math.ceil(skippedRecords / recordsPerPage + 1)

  const pages = useMemo(() => {
    const values: number[] = []
    let end = 1 + sideNavPages

    for (let i = -sideNavPages; i < end; i++) {
      const currentPage = page + i

      if (currentPage >= 1 && currentPage <= pageCount) {
        values.push(currentPage)
      } else if (currentPage < 1) {
        ++end
      } else if (currentPage > pageCount && values[0] > 1) {
        values.unshift(values[0] - 1)
      }
    }

    return values
  }, [sideNavPages, pageCount, page])

  if (pageCount < 1) return null

  const pagination = (
    <nav className={pageCount > 1 ? '' : 'hidden'}>
      <ButtonGroup size="sm" variant="main">
        <Button
          className="w-xl"
          disabled={page === 1}
          onClick={() => onPageClick(0)}
          subtle
        >
          <Icon>
            <svg
              className="h-6 w-6"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M15.707 15.707a1 1 0 01-1.414 0l-5-5a1 1 0 010-1.414l5-5a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 010 1.414zm-6 0a1 1 0 01-1.414 0l-5-5a1 1 0 010-1.414l5-5a1 1 0 011.414 1.414L5.414 10l4.293 4.293a1 1 0 010 1.414z"
                clipRule="evenodd"
              />
            </svg>
          </Icon>
        </Button>

        <Button
          className="w-xl"
          disabled={page === 1}
          onClick={() => onPageClick(skippedRecords - recordsPerPage)}
          subtle
        >
          <Icon>
            <svg
              className="h-6 w-6"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                clipRule="evenodd"
              />
            </svg>
          </Icon>
        </Button>

        {pages.map((pageNumber) => {
          const isActive = page === pageNumber

          return (
            <Button
              className="w-xl"
              disabled={isActive}
              onClick={() =>
                onPageClick(pageNumber * recordsPerPage - recordsPerPage)
              }
              subtle={!isActive}
              key={`page:${pageNumber}`}
            >
              {pageNumber}
            </Button>
          )
        })}

        <Button
          className="w-xl"
          disabled={page === pageCount}
          onClick={() => onPageClick(skippedRecords + recordsPerPage)}
          subtle
        >
          <Icon>
            <svg
              className="h-6 w-6"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clipRule="evenodd"
              />
            </svg>
          </Icon>
        </Button>

        <Button
          className="w-xl"
          disabled={page === pageCount}
          onClick={() =>
            onPageClick(
              totalRecords -
                recordsPerPage -
                (totalRecords - recordsPerPage * pageCount),
            )
          }
          subtle
        >
          <Icon>
            <svg
              className="h-6 w-6"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M10.293 15.707a1 1 0 010-1.414L14.586 10l-4.293-4.293a1 1 0 111.414-1.414l5 5a1 1 0 010 1.414l-5 5a1 1 0 01-1.414 0z"
                clipRule="evenodd"
              />
              <path
                fillRule="evenodd"
                d="M4.293 15.707a1 1 0 010-1.414L8.586 10 4.293 5.707a1 1 0 011.414-1.414l5 5a1 1 0 010 1.414l-5 5a1 1 0 01-1.414 0z"
                clipRule="evenodd"
              />
            </svg>
          </Icon>
        </Button>
      </ButtonGroup>
    </nav>
  )

  if (!showDescription && !showPerPagePicker)
    return (
      <Stack className={className} alignX="center">
        {pagination}
      </Stack>
    )

  const description =
    totalRecords > recordsPerPage ? (
      <p className="text-right text-sm text-color-subtle">
        Showing <span className="font-medium">{skippedRecords + 1}</span> to{' '}
        <span className="font-medium">
          {skippedRecords + recordsPerPage > totalRecords
            ? totalRecords
            : skippedRecords + recordsPerPage}
        </span>{' '}
        of <span className="font-medium">{totalRecords}</span> records
      </p>
    ) : (
      <p className="text-right text-sm text-color-subtle">
        <span className="font-medium">{totalRecords}</span>

        <span>{totalRecords > 1 ? ' records' : ' record'}</span>
      </p>
    )

  const perPagePicker = !!onSetRecordsPerPage && totalRecords > 10 && (
    <Inline alignY="center" space="sm" fontSize="sm">
      <span className="text-color-subtle">Records per page</span>
      <HTMLSelect
        options={[
          { value: 10 },
          { value: 25 },
          { value: 50 },
          { value: 100 },
          { value: 200 },
        ]}
        value={recordsPerPage}
        onChangeValue={(value) => onSetRecordsPerPage(parseInt(value))}
      />
    </Inline>
  )

  if (showDescription && onSetRecordsPerPage)
    return (
      <Stack className={className} space="sm">
        <Stack alignX="center">{pagination}</Stack>

        <Inline alignX="between" alignY="center" space="sm" width="full" wrap>
          {description}
          {perPagePicker}
        </Inline>
      </Stack>
    )

  return (
    <div
      className={classNames(
        className,
        'flex items-center justify-between gap-x-sm flex-wrap',
      )}
    >
      {pagination}

      <Inline alignX="end" className="min-w-content flex-grow">
        {showDescription && description}
        {showPerPagePicker && perPagePicker}
      </Inline>
    </div>
  )
}

export { Pagination }
