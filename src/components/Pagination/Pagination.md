### Simple

```jsx
const [skippedRecords, setSkippedRecords] = React.useState(0);

<Pagination alignX="start" recordsPerPage={10} skippedRecords={skippedRecords}
            onPageClick={setSkippedRecords}
            showDescription={false} totalRecords={123} />
```

### With description

```jsx
const [skippedRecords, setSkippedRecords] = React.useState(0);

<Pagination recordsPerPage={10} skippedRecords={skippedRecords}
            onPageClick={setSkippedRecords}
            showDescription totalRecords={123} />
```

### With records per pages control

```jsx
const [skippedRecords, setSkippedRecords] = React.useState(0);
const [recordsPerPage, setRecordsPerPage] = React.useState(10);

<Pagination recordsPerPage={recordsPerPage} skippedRecords={skippedRecords}
            onPageClick={setSkippedRecords}
            onSetRecordsPerPage={setRecordsPerPage} totalRecords={123} />
```

### With description and records per pages control

```jsx
const [skippedRecords, setSkippedRecords] = React.useState(0);
const [recordsPerPage, setRecordsPerPage] = React.useState(10);

<Pagination recordsPerPage={recordsPerPage} skippedRecords={skippedRecords}
            onPageClick={setSkippedRecords}
            showDescription
            onSetRecordsPerPage={setRecordsPerPage} totalRecords={123} />
```
