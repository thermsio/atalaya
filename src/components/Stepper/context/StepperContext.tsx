import React, { useState } from 'react'

type Step = {
  complete?: boolean
  error?: boolean
  label: string
}
type StepperContextValue = {
  activeStepIndex: number
  addStep: (newStep: Step) => void
  navigateNextDisabled?: boolean
  navigatePreviousDisabled?: boolean
  removeStep: (stepToRemove: Step) => void
  setActiveStep: ({ index, label }: { index: number; label: string }) => void
  steps: Step[]
  updateStep: (index: number, step: Step) => void
}

const StepperContext = React.createContext<StepperContextValue | undefined>(
  undefined,
)

interface StepperContextProviderProps {
  children: React.ReactNode
  navigateNextDisabled?: boolean
  navigatePreviousDisabled?: boolean
  onStepChange?: ({
    previous,
    next,
  }: {
    previous: { index: number; label: string }
    next: { index: number; label: string }
  }) => boolean | Promise<boolean>
}

export function StepperContextProvider({
  children,
  navigateNextDisabled,
  navigatePreviousDisabled,
  onStepChange,
}: StepperContextProviderProps) {
  const [steps, setSteps] = useState<Step[]>([])
  const [activeStepIndex, setActiveStepIndex] = useState<number>(0)

  return (
    <StepperContext.Provider
      value={{
        activeStepIndex,
        addStep: (newStep) => {
          setSteps((steps) => [...steps, newStep])
        },
        navigateNextDisabled,
        navigatePreviousDisabled,
        removeStep: (stepToRemove) =>
          setSteps((steps) =>
            steps.filter((step) => step.label !== stepToRemove.label),
          ),
        setActiveStep: (newStep) => {
          if (onStepChange) {
            const shouldProceed = onStepChange({
              previous: {
                index: activeStepIndex,
                label: steps[activeStepIndex].label,
              },
              next: newStep,
            })

            if (shouldProceed instanceof Promise) {
              shouldProceed.then((_shouldProceed) => {
                if (_shouldProceed) setActiveStepIndex(newStep.index)
              })
            } else {
              if (shouldProceed) setActiveStepIndex(newStep.index)
            }
          } else {
            setActiveStepIndex(newStep.index)
          }
        },
        steps,
        updateStep: (index, step) => {
          setSteps((steps) => {
            steps[index] = step
            return [...steps]
          })
        },
      }}
    >
      {children}
    </StepperContext.Provider>
  )
}

export function useStepperContext() {
  const context = React.useContext(StepperContext)

  if (!context) {
    throw new Error(
      'useStepperContext must be used within a StepperContext.Provider',
    )
  }

  return context as StepperContextValue
}
