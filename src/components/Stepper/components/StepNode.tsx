import React, { useEffect, useRef } from 'react'
import { useStepperContext } from '../context/StepperContext'
import classNames from 'classnames'

interface StepNodeProps {
  error?: boolean
  complete?: boolean
  index: number
  label: string
}

const StepNode = ({ error, complete, index, label }: StepNodeProps) => {
  const {
    steps,
    activeStepIndex,
    navigateNextDisabled,
    navigatePreviousDisabled,
    setActiveStep,
  } = useStepperContext()
  const nodeRef = useRef<HTMLDivElement | null>(null)

  let state = 'inactive'
  if (index === activeStepIndex) {
    state = 'active'
  } else if (error) {
    state = 'error'
  } else if (complete) {
    state = 'done'
  }

  let interactive = true
  if (
    state === 'active' ||
    (navigateNextDisabled && index > activeStepIndex) ||
    (navigatePreviousDisabled && index < activeStepIndex)
  ) {
    interactive = false
  }

  useEffect(() => {
    if (state === 'active' && nodeRef.current)
      nodeRef.current.scrollIntoView({ block: 'center', inline: 'center' })
  }, [state])

  return (
    <div
      onClick={interactive ? () => setActiveStep({ index, label }) : undefined}
      className={classNames({ 'group cursor-pointer': interactive })}
      ref={nodeRef}
    >
      <div className="flex items-center space-x-xs">
        <div
          className={classNames(
            {
              'bg-main': state === 'active',
              'bg-critical': state === 'error',
              'bg-positive': state === 'done',
              'bg-neutral': state === 'inactive',
              invisible: index === 0,
            },
            'h-0.5 flex-1 transition-colors duration-300',
          )}
        />

        <div
          className={classNames(
            {
              'bg-main': state === 'active',
              'bg-critical group-hover:bg-critical-light group-focus:bg-critical-light group-active:bg-critical-dark':
                state === 'error',
              'bg-positive group-hover:bg-positive-light group-focus:bg-positive-light group-active:bg-positive-dark':
                state === 'done',
              'bg-neutral group-hover:bg-neutral-light group-focus:bg-neutral-light group-active:bg-neutral-dark':
                state === 'inactive',
            },
            'flex h-7 w-7 items-center justify-center rounded-full text-color-semantic transition-colors duration-300',
          )}
        >
          {index + 1}
        </div>

        <div
          className={classNames(
            {
              'bg-main': state === 'active',
              'bg-critical': state === 'error',
              'bg-positive': state === 'done',
              'bg-neutral': state === 'inactive',
              invisible: steps.length === index + 1,
            },
            'h-0.5 flex-1 transition-colors duration-300',
          )}
        />
      </div>

      <div
        className={classNames(
          {
            'transition-colors duration-300 group-hover:text-color-strong group-focus:text-color-strong group-active:text-color-subtle':
              state !== 'active',
          },
          'mt-xs text-center',
        )}
      >
        {label}
      </div>
    </div>
  )
}

export { StepNode }
