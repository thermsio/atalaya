import React, { useEffect, useMemo } from 'react'
import { useStepperContext } from '../context/StepperContext'

export interface StepProps {
  /** Actual content of the step */
  children: React.ReactElement
  /** Used to represent a step that has already been visited and successfully completed. Navigation will display a `positive` backround for that step. */
  complete?: boolean
  /** Used to represent a step that has already been visited and unsuccessfully completed. Navigation will display a `critical` backround for that step. */
  error?: boolean
  /** Text will be used as the label on that step navigation. */
  label: string
}

const Step = ({ children, complete, error, label }: StepProps) => {
  const { activeStepIndex, addStep, removeStep, steps, updateStep } =
    useStepperContext()

  const stepIndex = useMemo(
    () => steps.findIndex((step) => step.label === label),
    [label, steps],
  )

  useEffect(() => {
    addStep({ complete, error, label })
    return () => removeStep({ complete, error, label })
  }, [])

  useEffect(() => {
    updateStep(stepIndex, { complete, error, label })
  }, [complete, error, label])

  if (activeStepIndex !== stepIndex) return null
  return children
}

export { Step }
