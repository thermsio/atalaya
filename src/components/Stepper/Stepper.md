A stepper component to create multiple step forms.

This component is divided into two parts.

## Stepper

The wrapper component. It accepts an `onStepChange` callback, which is used to react to step changes and let Stepper know if it should allow the change or not. It will be called anytime a step changes with the following signature.

```typescript static
type OnStepChange = ({
  // The Step that was selected before the change
  previous: { index: number, label: string },
  // The Step that will be selected after the change
  next: { index: number, label: string },
}) => void
```

If `OnStepChange` is provided it must return a boolean. This response determines if the change actual takes place. `true` will carry on the change, `false` will stop it.

## Step

The subcomponent Step should be used to wrap around the content you want to show on each step. Its job it's to hide or show its contents based on the current selected step and let the pass information to the Stepper navigation.

```typescript static
interface StepProps {
  // Actual content of the step.
  children: React.ReactElement
  // Used to represent a step that has already been visited and successfully completed. Navigation will display a `positive` backround for that step.
  complete?: boolean
  // Used to represent a step that has already been visited and unsuccessfully completed. Navigation will display a `critical` backround for that step.
  error?: boolean
  // Text will be used as the label on that step navigation.
  label: string
}
```

```jsx
const [completeSteps, setCompleteSteps] = React.useState(
  new Array(12).fill(false),
)

const handleStepChange = ({ previous }) => {
  setCompleteSteps((_completeSteps) => {
    _completeSteps[previous.index] = true
    return [..._completeSteps]
  })
  return true
}

return (
  <Stepper onStepChange={handleStepChange}>
    <Stepper.Step complete={completeSteps[0]} label="First Step">
      <div className="p-xl text-center">
        <div className="text-xxl font-bold text-color-main">1</div>
        <div className="mt-base text-lg">This is the first step</div>
      </div>
    </Stepper.Step>
    <Stepper.Step complete={completeSteps[1]} label="Second Step">
      <div className="p-xl text-center">
        <div className="text-xxl font-bold text-color-main">2</div>
        <div className="mt-base text-lg">This is the second step</div>
      </div>
    </Stepper.Step>
    <Stepper.Step complete={completeSteps[2]} label="Third Step">
      <div className="p-xl text-center">
        <div className="text-xxl font-bold text-color-main">3</div>
        <div className="mt-base text-lg">This is the third step</div>
      </div>
    </Stepper.Step>
    <Stepper.Step complete={completeSteps[3]} label="Forth Step">
      <div className="p-xl text-center">
        <div className="text-xxl font-bold text-color-main">4</div>
        <div className="mt-base text-lg">This is the Forth step</div>
      </div>
    </Stepper.Step>
    <Stepper.Step complete={completeSteps[4]} label="Fifth Step">
      <div className="p-xl text-center">
        <div className="text-xxl font-bold text-color-main">5</div>
        <div className="mt-base text-lg">This is the Fifth step</div>
      </div>
    </Stepper.Step>
    <Stepper.Step complete={completeSteps[5]} label="Sixth Step">
      <div className="p-xl text-center">
        <div className="text-xxl font-bold text-color-main">6</div>
        <div className="mt-base text-lg">This is the Sixth step</div>
      </div>
    </Stepper.Step>
    <Stepper.Step complete={completeSteps[6]} label="Seventh Step">
      <div className="p-xl text-center">
        <div className="text-xxl font-bold text-color-main">7</div>
        <div className="mt-base text-lg">This is the Seventh step</div>
      </div>
    </Stepper.Step>
    <Stepper.Step complete={completeSteps[7]} label="Eighth Step">
      <div className="p-xl text-center">
        <div className="text-xxl font-bold text-color-main">8</div>
        <div className="mt-base text-lg">This is the Eighth step</div>
      </div>
    </Stepper.Step>
    <Stepper.Step complete={completeSteps[8]} label="Ninth Step">
      <div className="p-xl text-center">
        <div className="text-xxl font-bold text-color-main">9</div>
        <div className="mt-base text-lg">This is the Ninth step</div>
      </div>
    </Stepper.Step>
    <Stepper.Step complete={completeSteps[9]} label="Tenth Step">
      <div className="p-xl text-center">
        <div className="text-xxl font-bold text-color-main">10</div>
        <div className="mt-base text-lg">This is the Tenth step</div>
      </div>
    </Stepper.Step>
    <Stepper.Step complete={completeSteps[10]} label="Eleventh Step">
      <div className="p-xl text-center">
        <div className="text-xxl font-bold text-color-main">11</div>
        <div className="mt-base text-lg">This is the Eleventh step</div>
      </div>
    </Stepper.Step>
    <Stepper.Step complete={completeSteps[11]} label="Twelfth Step">
      <div className="p-xl text-center">
        <div className="text-xxl font-bold text-color-main">12</div>
        <div className="mt-base text-lg">This is the Twelfth step</div>
      </div>
    </Stepper.Step>
  </Stepper>
)
```
