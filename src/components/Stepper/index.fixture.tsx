import React, { useState } from 'react'
import { Stepper } from './index'
import { useValue } from 'react-cosmos/client'

const base = () => {
  const [completeSteps, setCompleteSteps] = useState(new Array(12).fill(false))
  const [usePromise] = useValue('onStepChange Promise', {
    defaultValue: false,
  })
  const handleStepChange = async ({ previous }) => {
    setCompleteSteps((_completeSteps) => {
      _completeSteps[previous.index] = true
      return [..._completeSteps]
    })

    return usePromise
      ? new Promise<true>((resolve) => setTimeout(() => resolve(true), 2000))
      : true
  }

  return (
    <Stepper onStepChange={handleStepChange}>
      <Stepper.Step complete={completeSteps[0]} label="First Step">
        <div className="p-xl text-center">
          <div className="text-xxl font-bold text-color-main">1</div>
          <div className="mt-base text-lg">This is the first step</div>
        </div>
      </Stepper.Step>
      <Stepper.Step complete={completeSteps[1]} label="Second Step">
        <div className="p-xl text-center">
          <div className="text-xxl font-bold text-color-main">2</div>
          <div className="mt-base text-lg">This is the second step</div>
        </div>
      </Stepper.Step>
      <Stepper.Step complete={completeSteps[2]} label="Third Step">
        <div className="p-xl text-center">
          <div className="text-xxl font-bold text-color-main">3</div>
          <div className="mt-base text-lg">This is the third step</div>
        </div>
      </Stepper.Step>
      <Stepper.Step complete={completeSteps[3]} label="Forth Step">
        <div className="p-xl text-center">
          <div className="text-xxl font-bold text-color-main">4</div>
          <div className="mt-base text-lg">This is the Forth step</div>
        </div>
      </Stepper.Step>
      <Stepper.Step complete={completeSteps[4]} label="Fifth Step">
        <div className="p-xl text-center">
          <div className="text-xxl font-bold text-color-main">5</div>
          <div className="mt-base text-lg">This is the Fifth step</div>
        </div>
      </Stepper.Step>
      <Stepper.Step complete={completeSteps[5]} label="Sixth Step">
        <div className="p-xl text-center">
          <div className="text-xxl font-bold text-color-main">6</div>
          <div className="mt-base text-lg">This is the Sixth step</div>
        </div>
      </Stepper.Step>
      <Stepper.Step complete={completeSteps[6]} label="Seventh Step">
        <div className="p-xl text-center">
          <div className="text-xxl font-bold text-color-main">7</div>
          <div className="mt-base text-lg">This is the Seventh step</div>
        </div>
      </Stepper.Step>
      <Stepper.Step complete={completeSteps[7]} label="Eighth Step">
        <div className="p-xl text-center">
          <div className="text-xxl font-bold text-color-main">8</div>
          <div className="mt-base text-lg">This is the Eighth step</div>
        </div>
      </Stepper.Step>
      <Stepper.Step complete={completeSteps[8]} label="Ninth Step">
        <div className="p-xl text-center">
          <div className="text-xxl font-bold text-color-main">9</div>
          <div className="mt-base text-lg">This is the Ninth step</div>
        </div>
      </Stepper.Step>
      <Stepper.Step complete={completeSteps[9]} label="Tenth Step">
        <div className="p-xl text-center">
          <div className="text-xxl font-bold text-color-main">10</div>
          <div className="mt-base text-lg">This is the Tenth step</div>
        </div>
      </Stepper.Step>
      <Stepper.Step complete={completeSteps[10]} label="Eleventh Step">
        <div className="p-xl text-center">
          <div className="text-xxl font-bold text-color-main">11</div>
          <div className="mt-base text-lg">This is the Eleventh step</div>
        </div>
      </Stepper.Step>
      <Stepper.Step complete={completeSteps[11]} label="Twelfth Step">
        <div className="p-xl text-center">
          <div className="text-xxl font-bold text-color-main">12</div>
          <div className="mt-base text-lg">This is the Twelfth step</div>
        </div>
      </Stepper.Step>
    </Stepper>
  )
}

export default base
