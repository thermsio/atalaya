import React from 'react'
import { Step } from './components/Step'
import {
  StepperContextProvider,
  useStepperContext,
} from './context/StepperContext'
import { StepNode } from './components/StepNode'
import { Button } from '../Button'
import { ButtonGroup } from '../ButtonGroup'
import { Icon } from '../Icon'
import { HorizontalScroll } from '../HorizontalScroll'

interface StepperContentProps {
  children: React.ReactNode
  className?: string
}

const StepperContent = ({ children, className }: StepperContentProps) => {
  const {
    activeStepIndex,
    navigateNextDisabled,
    navigatePreviousDisabled,
    steps,
    setActiveStep,
  } = useStepperContext()

  return (
    <div className={className}>
      <HorizontalScroll alignX="center" itemWidth="8rem" space="0">
        {steps.map((step, index) => (
          <StepNode
            complete={step.complete}
            error={step.error}
            label={step.label}
            index={index}
            key={step.label}
          />
        ))}
      </HorizontalScroll>

      {children}

      <div className="flex justify-center">
        <ButtonGroup variant="main" subtle>
          <Button
            disabled={navigatePreviousDisabled || activeStepIndex <= 0}
            onClick={() => {
              const newStepIndex = activeStepIndex - 1
              setActiveStep({
                index: newStepIndex,
                label: steps[newStepIndex].label,
              })
            }}
          >
            <div className="flex w-28 items-center justify-start space-x-xs">
              <Icon>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 24 24"
                  fill="currentColor"
                >
                  <path d="M0 0h24v24H0V0z" fill="none" />
                  <path d="M14.91 6.71c-.39-.39-1.02-.39-1.41 0L8.91 11.3c-.39.39-.39 1.02 0 1.41l4.59 4.59c.39.39 1.02.39 1.41 0 .39-.39.39-1.02 0-1.41L11.03 12l3.88-3.88c.38-.39.38-1.03 0-1.41z" />
                </svg>
              </Icon>
              <span>Previous</span>
            </div>
          </Button>
          <Button
            disabled={
              navigateNextDisabled || activeStepIndex >= steps.length - 1
            }
            onClick={() => {
              const newStepIndex = activeStepIndex + 1
              setActiveStep({
                index: newStepIndex,
                label: steps[newStepIndex].label,
              })
            }}
          >
            <div className="flex w-28 items-center justify-end space-x-xs">
              <span>Next</span>
              <Icon>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 24 24"
                  fill="currentColor"
                >
                  <path d="M0 0h24v24H0V0z" fill="none" />
                  <path d="M9.31 6.71c-.39.39-.39 1.02 0 1.41L13.19 12l-3.88 3.88c-.39.39-.39 1.02 0 1.41.39.39 1.02.39 1.41 0l4.59-4.59c.39-.39.39-1.02 0-1.41L10.72 6.7c-.38-.38-1.02-.38-1.41.01z" />
                </svg>
              </Icon>
            </div>
          </Button>
        </ButtonGroup>
      </div>
    </div>
  )
}

export interface StepperProps extends StepperContentProps {
  /** Disabled all 'next' interaction */
  navigateNextDisabled?: boolean
  /** Disabled all 'previous' interactions */
  navigatePreviousDisabled?: boolean
  onStepChange?: (stepChange: {
    previous: { index: number; label: string }
    next: { index: number; label: string }
  }) => boolean | Promise<boolean>
}

const Stepper = ({
  children,
  className,
  navigateNextDisabled,
  navigatePreviousDisabled,
  onStepChange,
}: StepperProps) => {
  return (
    <StepperContextProvider
      navigateNextDisabled={navigateNextDisabled}
      navigatePreviousDisabled={navigatePreviousDisabled}
      onStepChange={onStepChange}
    >
      <StepperContent className={className}>{children}</StepperContent>
    </StepperContextProvider>
  )
}

Stepper.Step = Step

export { Stepper }
