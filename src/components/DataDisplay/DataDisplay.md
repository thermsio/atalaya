Provides the frame for displaying various types of information that can be described with labels

```jsx
<DataDisplay>
  <DataDisplay.Item label="Name">Lorem Ipsum</DataDisplay.Item>
  <DataDisplay.Item label="Phone">5555-5555</DataDisplay.Item>
  <DataDisplay.Item label="Bio">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
    non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
  </DataDisplay.Item>
</DataDisplay>
```

### Line dividers

```jsx
<DataDisplay stripes={false} space="lg" dividers="lines">
  <DataDisplay.Item label="Name">Lorem Ipsum</DataDisplay.Item>
  <DataDisplay.Item label="Phone">5555-5555</DataDisplay.Item>
  <DataDisplay.Item label="Bio">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
    non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
  </DataDisplay.Item>
</DataDisplay>
```
