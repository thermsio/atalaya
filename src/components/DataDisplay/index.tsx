import React from 'react'
import classNames from 'classnames'
import { DataItem, DataItemProps } from './components/DataItem'
import { Spacing } from '../../layout/types'

export interface DataDisplayProps {
  children?: React.ReactNode
  /** Style of divider, if 'none' is selected there will be no spacing between elements. */
  dividers?: 'none' | 'lines' | 'space'
  /** Amount of spacing between elements. */
  space?: Spacing
  /** Ads interlaced background colors on items. */
  stripes?: boolean
}

function DataDisplay({
  children,
  dividers = 'space',
  space = 'base',
  stripes = true,
}: DataDisplayProps): React.ReactElement | null {
  if (!children) return null

  if (dividers === 'none') return <>{children}</>

  return (
    <div>
      {React.Children.map(children, (child, index) => {
        if (!child) return null

        return (
          <>
            <div
              className={classNames({
                'odd:bg-surface-subtle': stripes,
                'py-xxs': stripes && space === 'xxs',
                'py-xs': stripes && space === 'xs',
                'py-sm': stripes && space === 'sm',
                'py-base': stripes && space === 'base',
                'py-lg': stripes && space === 'lg',
                'py-xl': stripes && space === 'xl',
                'py-xxl': stripes && space === 'xxl',
                'px-xxs': space === 'xxs',
                'px-xs': space === 'xs',
                'px-sm': space === 'sm',
                'px-base': space === 'base',
                'px-lg': space === 'lg',
                'px-xl': space === 'xl',
                'px-xxl': space === 'xxl',
              })}
            >
              {child}
            </div>

            {!stripes && index !== React.Children.count(children) - 1 && (
              <div
                className={classNames(`my-${space}`, {
                  'border-t border-border': dividers === 'lines',
                })}
              />
            )}
          </>
        )
      })}
    </div>
  )
}

DataDisplay.Item = DataItem

export { DataDisplay, DataItemProps }

// Tailwind Purge
// my-xxs my-xs my-sm my-base my-lg my-xl my-xxl
