import React from 'react'
import { Label } from '../../Label'

export interface DataItemProps {
  children?: React.ReactNode
  label?: React.ReactNode
}

function DataItem({ label, children }: DataItemProps) {
  return (
    <div className="flex flex-col md:flex-row">
      <div className="flex-1">
        {!!label && typeof label === 'string' ? (
          <Label className="block">{label}</Label>
        ) : (
          label
        )}
      </div>

      <div style={{ flex: '2 1 0%' }}>{children}</div>
    </div>
  )
}

export { DataItem }
