import React from 'react'
import { StylesWrapper, StylesWrapperProps } from './components/StylesWrapper'
import { LightboxContextProvider } from '../Lightbox/context/LightboxContext'
import { ToastContainer } from '../Toasts/ToastContainer'

export interface AtalayaWrapperProps extends StylesWrapperProps {
  children: React.ReactNode
  defaultTheme?: 'dark' | 'light'
}

const AtalayaWrapper: React.FC<AtalayaWrapperProps> = ({
  children,
  defaultTheme = 'dark',
  theme,
}) => {
  return (
    <StylesWrapper defaultTheme={defaultTheme} theme={theme}>
      <LightboxContextProvider>{children}</LightboxContextProvider>

      <ToastContainer />
    </StylesWrapper>
  )
}

export { AtalayaWrapper }
