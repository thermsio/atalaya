import React, { useCallback, useMemo } from 'react'
import merge from 'lodash-es/merge'

import { Constants } from '../../../constants'
import { StylesContext, themeType } from '../../../styles/context/StylesContext'
import { useDarkModeTheme } from '../../../hooks/useDarkModeTheme'

import '../../../styles/css/variables/base.css'
import '../../../styles/css/base.css'

export interface StylesWrapperProps {
  children: React.ReactNode
  defaultTheme?: 'dark' | 'light'
  theme?: themeType
}

const StylesWrapper = ({
  children,
  defaultTheme = 'dark',
  theme,
}: StylesWrapperProps): React.ReactElement => {
  const mergedTheme = useMemo(() => {
    const _theme: themeType = {
      dark: Constants.Color.Dark,
      light: Constants.Color.Light,
    }

    if (typeof theme === 'object' && !Array.isArray(theme)) merge(_theme, theme)

    return _theme
  }, [theme])

  const [isDarkMode, setIsDarkMode] = useDarkModeTheme(
    mergedTheme,
    defaultTheme,
  )

  const toggleDarkMode = useCallback(
    () => setIsDarkMode(!isDarkMode),
    [isDarkMode],
  )

  return (
    <StylesContext.Provider
      value={{
        isDarkMode,
        theme: mergedTheme,
        toggleDarkMode,
      }}
    >
      {children}
    </StylesContext.Provider>
  )
}

export { StylesWrapper }
