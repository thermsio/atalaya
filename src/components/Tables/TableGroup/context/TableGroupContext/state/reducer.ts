type State = {
  allRowsSelected: boolean
  selectedRowsData: Array<{ [key: string]: any }>
  selectedRowsKeys: string[]
  selectedRowsByKey: { [key: string]: boolean }
  hiddenColumnsByIndex: {
    [key: string]: boolean | undefined
  }
}

const getInitialState = (): Readonly<State> => ({
  allRowsSelected: false,
  selectedRowsData: [],
  selectedRowsKeys: [],
  selectedRowsByKey: {},
  hiddenColumnsByIndex: {},
})

const ACTIONS = {
  CLEAR_SELECTED_ROWS: 'clearSelectedRows',
  SET_HIDDEN_COLUMNS_BY_INDEX: 'setHiddenColumnsByIndex',
  TOGGLE_ROWS_SELECTED_BY_KEY: 'toggleRowSelectedByKey',
  TOGGLE_SELECT_ALL_ROWS: 'toggleSelectAllRows',
}

interface ReducerPayload {
  action: string
  payload?:
    | {
        data?: Array<{ [key: string]: any }>
        keyProp?: string
        rowKey?: string
      }
    | any
}

type StateReducerProps = (state: State, args: ReducerPayload) => State

const stateReducer: StateReducerProps = (state, { action, payload = {} }) => {
  const clearSelectedRows = () => {
    return {
      ...state,
      allRowsSelected: false,
      selectedRowsData: [],
      selectedRowsKeys: [],
      selectedRowsByKey: {},
    }
  }

  if (action === ACTIONS.CLEAR_SELECTED_ROWS) {
    return clearSelectedRows()
  }

  if (action === ACTIONS.TOGGLE_ROWS_SELECTED_BY_KEY) {
    const isSelected = !!state.selectedRowsByKey[payload.rowKey]

    const newSelectedRowsByKey = {
      ...state.selectedRowsByKey,
      [payload.rowKey]: !isSelected,
    }

    let newSelectedRowsData = Array.from(state.selectedRowsData)
    let newSelectedRowsKeys = Array.from(state.selectedRowsKeys)

    if (isSelected) {
      newSelectedRowsData = state.selectedRowsData.filter(
        (rowData) => rowData[payload.keyProp] !== payload.rowKey,
      )

      newSelectedRowsKeys = state.selectedRowsKeys.filter(
        (rowKey) => rowKey !== payload.rowKey,
      )
    } else {
      newSelectedRowsData.push(
        payload.data.find(
          (rowData) => rowData[payload.keyProp] === payload.rowKey,
        ),
      )

      newSelectedRowsKeys.push(payload.rowKey)
    }

    return {
      ...state,
      allRowsSelected: newSelectedRowsKeys.length === payload.data?.length,
      selectedRowsByKey: newSelectedRowsByKey,
      selectedRowsData: newSelectedRowsData,
      selectedRowsKeys: newSelectedRowsKeys,
    }
  }

  if (action === ACTIONS.TOGGLE_SELECT_ALL_ROWS) {
    if (state.allRowsSelected) {
      return clearSelectedRows()
    } else {
      const newState: Pick<
        State,
        | 'allRowsSelected'
        | 'selectedRowsData'
        | 'selectedRowsKeys'
        | 'selectedRowsByKey'
      > = {
        allRowsSelected: !state.allRowsSelected,
        selectedRowsData: [],
        selectedRowsKeys: [],
        selectedRowsByKey: {},
      }

      payload.data?.forEach((rowData) => {
        const keyVal =
          payload.keyProp !== undefined ? rowData[payload.keyProp] : null

        if (keyVal) {
          newState.selectedRowsByKey[keyVal] = true
          newState.selectedRowsData.push(rowData)
          newState.selectedRowsKeys.push(keyVal)
        }
      })

      return { ...state, ...newState }
    }
  }

  if (action === ACTIONS.SET_HIDDEN_COLUMNS_BY_INDEX) {
    return {
      ...state,
      hiddenColumnsByIndex: {
        ...state.hiddenColumnsByIndex,
        ...payload,
      },
    }
  }

  return state
}

export const TableGroupContextState = {
  ACTIONS,
  getInitialState,
  reducer: stateReducer,
}
