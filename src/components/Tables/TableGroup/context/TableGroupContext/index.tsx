import React, { useCallback, useReducer, useRef } from 'react'
import { TableGroupContextState } from './state/reducer'

interface TableGroupContext {
  allRowsSelected: boolean
  clearSelectedRows: () => void
  hiddenColumnsByIndex: {
    [key: string]: boolean | undefined
  }
  data: Array<{ [key: string]: any }>
  keyProp: string
  onClickRow?: (info: {
    rowData: { [key: string]: any }
    rowKey: string
    rowIndex: number
    data: Array<{ [key: string]: any }>
  }) => void
  selectedRowsData: Array<{ [key: string]: any }>
  selectedRowsKeys: Array<string>
  selectedRowsByKey: { [key: string]: boolean }
  setHiddenColumnsByIndex: (map) => void
  toggleRowSelectedByKey: (rowKey: string) => void | any
  toggleSelectAllRows: () => void | any
}

export const TableGroupContext = React.createContext<
  TableGroupContext | undefined
>(undefined)

type TableGroupContextProviderProps = {
  children: (tableGroupContextValue: TableGroupContext) => React.ReactNode
  data: Array<{ [key: string]: any }>
  keyProp: string
  onClickRow?: (info: {
    rowData: { [key: string]: any }
    rowKey: string
    rowIndex: number
    data: Array<{ [key: string]: any }>
  }) => void
}

export const TableGroupContextProvider = ({
  children,
  data,
  keyProp,
  onClickRow,
}: TableGroupContextProviderProps) => {
  const [state, dispatch] = useReducer(
    TableGroupContextState.reducer,
    TableGroupContextState.getInitialState(),
  )

  const local = useRef({ onClickRow })

  local.current.onClickRow = onClickRow

  // memoized callback so the reference doesn't change on every parent re-render
  const onClickRowMemo: TableGroupContextProviderProps['onClickRow'] =
    useCallback((info) => {
      local.current.onClickRow?.(info)
    }, [])

  const ctxValue = {
    allRowsSelected: state.allRowsSelected,
    clearSelectedRows: () =>
      dispatch({ action: TableGroupContextState.ACTIONS.CLEAR_SELECTED_ROWS }),
    hiddenColumnsByIndex: state.hiddenColumnsByIndex,
    data,
    keyProp,
    onClickRow: onClickRowMemo,
    selectedRowsData: state.selectedRowsData,
    selectedRowsKeys: state.selectedRowsKeys,
    selectedRowsByKey: state.selectedRowsByKey,
    setHiddenColumnsByIndex: (columns) =>
      dispatch({
        action: TableGroupContextState.ACTIONS.SET_HIDDEN_COLUMNS_BY_INDEX,
        payload: columns,
      }),
    toggleRowSelectedByKey: (rowKey) =>
      dispatch({
        action: TableGroupContextState.ACTIONS.TOGGLE_ROWS_SELECTED_BY_KEY,
        payload: { data, rowKey, keyProp },
      }),
    toggleSelectAllRows: () =>
      dispatch({
        action: TableGroupContextState.ACTIONS.TOGGLE_SELECT_ALL_ROWS,
        payload: { data, keyProp },
      }),
  }

  return (
    <TableGroupContext.Provider value={ctxValue}>
      {children(ctxValue)}
    </TableGroupContext.Provider>
  )
}

export const useTableGroupContext = () => {
  const context = React.useContext(TableGroupContext)

  if (context === undefined) {
    throw new Error(
      'useTableGroupContext() must be used inside a <TableGroup /> component',
    )
  }

  return context
}
