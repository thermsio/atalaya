import { Breakpoint } from '../../../../constants/Breakpoint'

export const selectCorrectOption = (
  visibleSize: string | undefined,
): string => {
  switch (visibleSize) {
    case 'sm':
      return `(max-width: ${Breakpoint.Sm.width}px)`
    case 'md':
      return `(max-width: ${Breakpoint.Md.width}px)`
    case 'lg':
      return `(max-width: ${Breakpoint.Lg.width}px)`
    case 'xl':
      return `(max-width: ${Breakpoint.Xl.width}px)`
    case 'xxl':
      return `(max-width: ${Breakpoint.Xxl.width}px)`
    default:
      return ''
  }
}
