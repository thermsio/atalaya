To hide any column depending on screen size, it is necessary to pass `visibleSize` prop with one of the following
breakpoint values: `'sm'`, `'md'`, `'lg'`, `'xl'`, `'xxl'`. If the screen size is less than passed value, the column
will hide.

```jsx inside Markdown
import { TableGroup } from './index'
import { Button } from '../../Button'

const HeadRow = ({
  // isSelected,
  data,
}) => (
  <TableGroup.HeadRow canSelect>
    <TableGroup.HeadCell canSort visibleSize="md">
      Name
    </TableGroup.HeadCell>

    <TableGroup.HeadCell>Contact</TableGroup.HeadCell>

    <TableGroup.HeadCell canSort>Status</TableGroup.HeadCell>
  </TableGroup.HeadRow>
)

const Row = ({
  rowData,
  rowKey,
  // rowIndex,
  // data,
}) => (
  <TableGroup.Row canSelect rowKey={rowKey}>
    <TableGroup.Cell>{rowData.displayName}</TableGroup.Cell>

    <TableGroup.Cell>
      <div className="font-light">{rowData.contactInfo.phone}</div>

      <div className="text-color-info">{rowData.contactInfo.email}</div>
    </TableGroup.Cell>

    <TableGroup.Cell>
      <span>{rowData.active ? 'Active' : 'Inactive'}</span>
    </TableGroup.Cell>
  </TableGroup.Row>
)

const Controls = ({ selectedRowsKeys, data }) => {
  if (!selectedRowsKeys.length) return null

  return (
    <div className="flex items-center justify-center">
      <div className="text-sm">{selectedRowsKeys.length} Selected</div>

      <div className="ml-2">
        <Button size="sm" subtle>
          Change Status
        </Button>

        <span className="mx-1"></span>

        <Button size="sm" subtle>
          Edit
        </Button>

        <span className="mx-1"></span>

        <Button size="sm" subtle variant="critical">
          Delete
        </Button>
      </div>
    </div>
  )
}

const TableGroupExample = () => {
  const data = [
    {
      active: true,
      displayName: 'Cory Robinson',
      id: '123',
      contactInfo: {
        email: 'cory@therms.io',
        phone: '208-874-9204',
      },
    },
    {
      active: false,
      displayName: 'Tom Corridoni',
      id: '345',
      contactInfo: {
        email: 'tom@therms.io',
        phone: '999-999-9999',
      },
    },
  ]

  return (
    <TableGroup
      data={data}
      keyProp="id"
      Controls={Controls}
      HeadRow={HeadRow}
      Row={Row}
      onSetRecordsPerPage={console.log}
      totalRecords={113}
    />
  )
}

;<TableGroupExample />
```
