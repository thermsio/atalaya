import React from 'react'

const TableEl = ({ children }) => {
  return (
    <div className="overflow-x-auto">
      <table className="min-w-full table-auto divide-y divide-surface-strong">
        {children}
      </table>
    </div>
  )
}

export { TableEl }
