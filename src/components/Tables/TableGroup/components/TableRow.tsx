import React, { Children } from 'react'
import TableCell from './TableCell'
import { Checkbox } from '../../../FormControls/Checkbox/Checkbox'
import { Stack } from '../../../../layout/Stack'
import { useTableGroupContext } from '../context/TableGroupContext'

type TableRowProps = React.PropsWithChildren<{
  canSelect?: boolean
  className?: string
  rowKey: string
}>

const TableRow: React.FC<
  TableRowProps & React.HTMLAttributes<HTMLTableRowElement>
> = ({ canSelect, children, className, rowKey, ...tableRowElementProps }) => {
  const {
    data,
    keyProp,
    onClickRow,
    selectedRowsByKey,
    toggleRowSelectedByKey,
  } = useTableGroupContext()

  if (!rowKey) {
    console.warn('<TableRow /> no "rowKey" prop provided', rowKey)
    return null
  }

  const handleOnClick = (e) => {
    if (tableRowElementProps.onClick) tableRowElementProps.onClick(e)

    if (onClickRow) {
      const rowIndex = data.findIndex((d) => d[keyProp] === rowKey)

      if (rowIndex !== -1) {
        const rowData = data[rowIndex]

        onClickRow({ rowData, rowKey, rowIndex, data })
      } else {
        console.warn(
          '<TableRow /> rowData not found in data',
          `rowKey: ${rowKey}`,
        )
      }
    }
  }

  return (
    <tr
      className={className}
      {...(tableRowElementProps || {})}
      onClick={handleOnClick}
    >
      {canSelect && (
        <TableCell>
          <Stack alignY="center">
            <div onClick={(e) => e.stopPropagation()}>
              <Checkbox
                value={1}
                active={selectedRowsByKey[rowKey]}
                onChangeValue={() => toggleRowSelectedByKey(rowKey)}
              />
            </div>
          </Stack>
        </TableCell>
      )}
      {Children.toArray(children).map((child: any, index) => {
        return React.cloneElement(child, {
          ...child.props,
          index,
        })
      })}
    </tr>
  )
}

export default TableRow
