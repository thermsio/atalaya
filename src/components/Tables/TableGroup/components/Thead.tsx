import React from 'react'

const Thead = ({ children }) => {
  return <thead className="bg-surface text-color-subtle">{children}</thead>
}

export default Thead
