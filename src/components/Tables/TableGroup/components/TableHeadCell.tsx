import React, { useCallback, useEffect, useLayoutEffect } from 'react'
import { ScreenSizeName } from '../../../../constants/Breakpoint'
import { selectCorrectOption } from '../utils'
import { useTableGroupContext } from '../context/TableGroupContext'
import classNameParser from 'classnames'

type TableHeadCellProps = React.PropsWithChildren<{
  canSort?: boolean
  colSpan?: number
  fitContent?: boolean
  index?: number
  visibleSize?: ScreenSizeName
  width?: number
}>

const TableHeadCell: React.FC<TableHeadCellProps> = ({
  children,
  colSpan,
  fitContent,
  index,
  width,
  visibleSize,
}) => {
  const { hiddenColumnsByIndex, setHiddenColumnsByIndex } =
    useTableGroupContext()
  const mediaQuery = window.matchMedia(selectCorrectOption(visibleSize))

  useLayoutEffect(() => {
    if (index !== undefined && visibleSize) {
      const payload = {}
      payload[index] = mediaQuery.matches
      setHiddenColumnsByIndex(payload)
    }
  }, [])

  const updateHiddenStatus = useCallback(
    (e) => {
      if (index !== undefined && visibleSize) {
        const payload = {}
        payload[index] = e.matches
        setHiddenColumnsByIndex(payload)
      }
    },
    [index],
  )

  useEffect(() => {
    mediaQuery.addEventListener('change', updateHiddenStatus)
    return () => {
      mediaQuery.removeEventListener('change', updateHiddenStatus)
    }
  }, [])

  if (index !== undefined && hiddenColumnsByIndex[index]) return null

  return (
    <th
      colSpan={colSpan || 1}
      scope="col"
      className={classNameParser(
        'subtle px-sm py-1 text-left text-xs font-medium uppercase tracking-wider',
        { 'w-0': fitContent },
      )}
      style={{ width }}
    >
      {children}
    </th>
  )
}

export default TableHeadCell
