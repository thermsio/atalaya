import React from 'react'

const Tbody = ({ children }) => (
  <tbody className="bg-surface-subtle text-color">{children}</tbody>
)

export default Tbody
