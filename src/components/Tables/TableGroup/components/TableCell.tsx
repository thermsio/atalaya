import React, { PropsWithChildren, useMemo } from 'react'
import classnames from 'classnames'
import { useTableGroupContext } from '../context/TableGroupContext'

const TableCell: React.FC<
  PropsWithChildren<{
    breakword?: boolean
    className?: string
    ellipse?: boolean
    index?: number
  }>
> = ({ breakword = true, children, className, ellipse, index }) => {
  const { hiddenColumnsByIndex } = useTableGroupContext()

  const style = useMemo<any>(() => {
    if (ellipse) {
      return {
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
        maxWidth: 0,
      }
    }

    return {}
  }, [ellipse])

  if (index !== undefined && hiddenColumnsByIndex[index]) return null

  return (
    <td
      className={classnames(
        'whitespace-nowrap px-sm py-xs text-sm font-medium',
        className,
        {
          breakword,
        },
      )}
      style={style}
    >
      {children}
    </td>
  )
}

export default TableCell
