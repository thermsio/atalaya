import React, { Children, cloneElement, isValidElement } from 'react'
import TableHeadCell from './TableHeadCell'
import { Checkbox } from '../../../FormControls/Checkbox/Checkbox'
import { useTableGroupContext } from '../context/TableGroupContext'

type TableHeadRowProps = React.PropsWithChildren<{
  canSelect?: boolean
}>

const TableHeadRow: React.FC<TableHeadRowProps> = ({ canSelect, children }) => {
  const { allRowsSelected, toggleSelectAllRows } = useTableGroupContext()

  return (
    <tr>
      {canSelect && (
        <TableHeadCell fitContent>
          <Checkbox
            value={1}
            active={allRowsSelected}
            onChangeValue={toggleSelectAllRows}
          />
        </TableHeadCell>
      )}

      {Children.map(children, (child, index) => {
        if (isValidElement(child)) {
          return cloneElement(child, { index } as any)
        }
      })}
    </tr>
  )
}

export default TableHeadRow
