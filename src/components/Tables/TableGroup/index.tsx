import React from 'react'

import { TableEl } from './components/TableEl'
import Tbody from './components/Tbody'
import TableCell from './components/TableCell'
import TableHeadCell from './components/TableHeadCell'
import Thead from './components/Thead'
import TableRow from './components/TableRow'
import TableHeadRow from './components/TableHeadRow'
import { Pagination } from '../../Pagination'
import { TableGroupContextProvider } from './context/TableGroupContext'

type ReactComponentOrElement<Props = any> =
  | React.FC<Props>
  | React.ReactElement<Props>

export type TableProps = React.PropsWithChildren<{
  // canSelectRows?: boolean
  data: Array<{ [key: string]: any }>
  keyProp: string
  onClickRow?: (info: {
    rowData: { [key: string]: any }
    rowKey: string
    rowIndex: number
    data: Array<{ [key: string]: any }>
  }) => void
  onSetRecordsPerPage?: (newRecordsPerPage: number) => void
  onSetSkippedRecords?: (skippedRecords: number) => void
  recordsPerPage?: number
  skippedRecords?: number
  totalRecords?: number
  Controls?: ReactComponentOrElement<{
    clearSelectedRows: () => void
    data: Array<{ [key: string]: any }>
    selectedRowsData: Array<{ [key: string]: any }>
    selectedRowsKeys: Array<string>
  }>
  HeadRow?: ReactComponentOrElement<{
    data: Array<{ [key: string]: any }>
  }>
  Row?: ReactComponentOrElement<{
    data: Array<{ [key: string]: any }>
    rowData: { [key: string]: any }
    rowIndex: number
    rowKey: string
  }>
}>

export type TableGroupType = React.FC<TableProps> & {
  HeadCell: typeof TableHeadCell
  HeadRow: typeof TableHeadRow
  Cell: typeof TableCell
  Row: typeof TableRow
}

const TableGroup: TableGroupType = ({
  data,
  keyProp,
  onClickRow,
  onSetRecordsPerPage,
  onSetSkippedRecords,
  recordsPerPage = 25,
  skippedRecords = 0,
  totalRecords,
  Controls,
  HeadRow,
  Row,
}: TableProps) => {
  return (
    <TableGroupContextProvider
      data={data}
      keyProp={keyProp}
      onClickRow={onClickRow}
    >
      {({ clearSelectedRows, selectedRowsData, selectedRowsKeys }) => (
        <div>
          {!!Controls && (
            <div className="flex flex-row">
              {React.isValidElement(Controls) ? (
                React.cloneElement(Controls, {
                  clearSelectedRows,
                  data,
                  selectedRowsData,
                  selectedRowsKeys,
                })
              ) : (
                <Controls
                  clearSelectedRows={clearSelectedRows}
                  data={data}
                  selectedRowsData={selectedRowsData}
                  selectedRowsKeys={selectedRowsKeys}
                />
              )}
            </div>
          )}

          <TableEl>
            {!!HeadRow && (
              <Thead>
                {React.isValidElement(HeadRow) ? (
                  React.cloneElement(HeadRow, { data })
                ) : (
                  <HeadRow data={data} />
                )}
              </Thead>
            )}

            <Tbody>
              {!!Row &&
                data?.map((rowData, rowIndex) => {
                  if (React.isValidElement(Row))
                    return React.cloneElement(Row, {
                      data,
                      key: rowData[keyProp],
                      rowData,
                      rowIndex,
                      rowKey: rowData[keyProp],
                    })

                  return (
                    <Row
                      data={data}
                      key={rowData[keyProp]}
                      rowData={rowData}
                      rowIndex={rowIndex}
                      rowKey={rowData[keyProp]}
                    />
                  )
                })}
            </Tbody>
          </TableEl>

          {!!onSetRecordsPerPage && !!onSetSkippedRecords && !!totalRecords && (
            <div className="mt-1 md:mt-3">
              <Pagination
                onPageClick={onSetSkippedRecords}
                onSetRecordsPerPage={onSetRecordsPerPage}
                recordsPerPage={recordsPerPage}
                showDescription
                skippedRecords={skippedRecords}
                totalRecords={totalRecords}
              />
            </div>
          )}
        </div>
      )}
    </TableGroupContextProvider>
  )
}

TableGroup.HeadCell = TableHeadCell
TableGroup.HeadRow = TableHeadRow

TableGroup.Cell = TableCell
TableGroup.Row = TableRow

export { TableGroup }
