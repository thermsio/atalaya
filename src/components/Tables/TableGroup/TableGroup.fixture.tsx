/* eslint-disable */
import React, { useEffect, useState } from 'react'
import { TableGroup } from './index'
import { Button } from '../../Button'

const HeadRow = ({
  // isSelected,
  data,
}) => (
  <TableGroup.HeadRow canSelect>
    <TableGroup.HeadCell canSort>Name</TableGroup.HeadCell>

    <TableGroup.HeadCell visibleSize="sm">Contact</TableGroup.HeadCell>

    <TableGroup.HeadCell canSort visibleSize="lg" fitContent>
      Status
    </TableGroup.HeadCell>
  </TableGroup.HeadRow>
)

const Row = ({ rowData, rowIndex, rowKey }) => (
  <TableGroup.Row canSelect rowKey={rowKey}>
    <TableGroup.Cell>{rowData.displayName}</TableGroup.Cell>

    <TableGroup.Cell>
      <div className="font-light">{rowData.contactInfo.phone}</div>

      <div className="text-color-info">{rowData.contactInfo.email}</div>
    </TableGroup.Cell>

    <TableGroup.Cell>
      <span>{rowData.active ? 'Active' : 'Inactive'}</span>
    </TableGroup.Cell>
  </TableGroup.Row>
)

const Controls = ({ selectedRowsKeys, selectedRowsData, data }) => {
  useEffect(() => {
    console.log(
      'selectedRowsKeys',
      selectedRowsKeys,
      'selectedRowsData',
      selectedRowsData,
    )
  }, [selectedRowsKeys, selectedRowsData])
  if (!selectedRowsKeys.length) return null

  return (
    <div className="flex items-center justify-center">
      <div className="text-sm">{selectedRowsKeys.length} Selected</div>

      <div className="ml-2">
        <Button size="sm" subtle>
          Change Status
        </Button>

        <span className="mx-1"></span>

        <Button size="sm" subtle>
          Edit
        </Button>

        <span className="mx-1"></span>

        <Button size="sm" subtle variant="critical">
          Delete
        </Button>
      </div>
    </div>
  )
}

export default () => {
  const [table, setTable] = useState('1')

  const data = [
    {
      active: true,
      displayName: 'Cory Robinson',
      id: '123',
      contactInfo: {
        email: 'cory@therms.io',
        phone: '208-874-9204',
      },
    },
    {
      active: false,
      displayName:
        'Tom Corridoni ldakjalksd lkajdlkas jkdljaskl jdlkasj dlkasjd lkjaskld jklasjd lasjd lajs lkajlk djl jalskjlkj',
      id: '345',
      contactInfo: {
        email: 'tom@therms.io',
        phone: '999-999-9999',
      },
    },
    {
      active: false,
      displayName: 'Andy',
      id: '567',
      contactInfo: {
        email: 'andy@therms.io',
        phone: '999-999-9999',
      },
    },
  ]

  return (
    <div>
      <div>
        <button onClick={() => setTable('1')}>Table 1</button>
        <button onClick={() => setTable('2')}>Table 2</button>
      </div>

      {table === '1' ? (
        <TableGroup
          data={data.reverse()}
          keyProp="id"
          Controls={Controls}
          HeadRow={HeadRow}
          Row={Row}
          onSetRecordsPerPage={console.log}
          totalRecords={113}
        />
      ) : (
        <TableGroup
          data={data}
          keyProp="id"
          Controls={Controls}
          HeadRow={HeadRow}
          Row={Row}
          onSetRecordsPerPage={console.log}
          totalRecords={113}
        />
      )}
    </div>
  )
}
