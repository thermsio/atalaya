import React, { ReactElement, MouseEvent } from 'react'
import ReactDOM from 'react-dom'
import {
  toast as toastifyManager,
  TypeOptions,
  ToastPosition,
  ToastOptions,
} from 'react-toastify'
import { ToastContainer, TOAST_CONTAINER_ID } from './ToastContainer'

interface ToastManagerOptions {
  /**
   * Set the delay in ms to close the toast automatically.
   * Use `false` to prevent the toast from closing.
   * `Default: 5000`
   */
  autoClose?: number | false
  /**
   * Hide or show the progress bar.
   * `Default: false`
   */
  hideProgressBar?: boolean
  /** Set the default position to use.
   * `Default: 'top-right'`
   */
  position?: ToastPosition
  /** Fired when clicking inside toaster */
  onClick?: (event: MouseEvent) => void
  /** Called when toast is mounted. */
  onOpen?: <T = Record<string, unknown>>(props: T) => void
  /** Called when toast is unmounted. */
  onClose?: <T = Record<string, unknown>>(props: T) => void
  /** Manually declare the toastId  */
  toastId?: string
  /** Set the toast variant. */
  variant?: 'info' | 'positive' | 'caution' | 'critical' | string

  /**
   * @INTERNAL
   */
  type?: TypeOptions
}

const toastManager = (
  toast: string | ReactElement,
  options?: ToastManagerOptions,
) => {
  if (!document.getElementById(TOAST_CONTAINER_ID)) {
    const rootNode =
      document.getElementById('app') ||
      document.getElementById('root') ||
      document.querySelector('body')

    const containerDiv = document.createElement('div')
    containerDiv.setAttribute('id', 'toast-container-container')
    rootNode?.appendChild(containerDiv)

    // eslint-disable-next-line react/no-deprecated
    ReactDOM.render(<ToastContainer />, containerDiv)
  }

  if (options?.variant) {
    let type
    if (options.variant === 'info') type = 'info'
    if (options.variant === 'positive') type = 'success'
    if (options.variant === 'caution') type = 'warning'
    if (options.variant === 'critical') type = 'error'
    options.type = type
    delete options.variant
  }

  return toastifyManager(toast, {
    containerId: TOAST_CONTAINER_ID,
    ...(options as ToastOptions),
  })
}

toastManager.dismiss = (toastId: string | number | undefined) =>
  toastifyManager.dismiss(toastId)

toastManager.POSITION = {
  TOP_LEFT: 'top-left',
  TOP_RIGHT: 'top-right',
  TOP_CENTER: 'top-center',
  BOTTOM_LEFT: 'bottom-left',
  BOTTOM_RIGHT: 'bottom-right',
  BOTTOM_CENTER: 'bottom-center',
} as const

toastManager.VARIANT = {
  INFO: 'info',
  POSITIVE: 'positive',
  CAUTION: 'caution',
  CRITICAL: 'critical',
}

export { toastManager, ToastManagerOptions }
