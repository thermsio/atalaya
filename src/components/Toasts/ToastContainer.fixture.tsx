import React, { useRef } from 'react'
import { toastManager } from './toastManager'
import { Modal } from '../Modals/Modal'
import { useState } from 'react'
import { Button } from '../Button'
import { ToastPosition } from 'react-toastify'
import { Toast } from './Toast'

const simpleToast = () => {
  return (
    <div>
      <Button
        variant="main"
        onClick={() =>
          toastManager('Toast Dispatched', {
            position: toastManager.POSITION.TOP_CENTER as ToastPosition,
          })
        }
      >
        Dispatch Toast
      </Button>
    </div>
  )
}

const toastOverModal = () => {
  const [isModalOpen, setIsModalOpen] = useState(false)

  return (
    <>
      <Button variant="main" onClick={() => setIsModalOpen(true)}>
        Open Modal
      </Button>

      {isModalOpen && (
        <Modal>
          <div className="mb-base">
            The point of this modal is to test if the toast appears behind or
            above the modal backdrop. It should appear above.
          </div>
          <Button
            variant="main"
            onClick={() =>
              toastManager(<Toast>Toast Dispatched</Toast>, {
                position: toastManager.POSITION.TOP_CENTER,
                autoClose: false,
              })
            }
          >
            Dispatch Toast
          </Button>
        </Modal>
      )}
    </>
  )
}

const toastOverFullScreen = () => {
  const ref = useRef<HTMLDivElement>(null)

  return (
    <>
      <div className="rounded bg-surface p-xl" ref={ref}>
        <div className="text-lg">Element that will request fullscreen.</div>
        <Button
          variant="main"
          onClick={() =>
            toastManager('Toast Dispatched', {
              position: toastManager.POSITION.TOP_CENTER as ToastPosition,
            })
          }
        >
          Dispatch Toast
        </Button>
      </div>

      <Button variant="main" onClick={() => ref.current?.requestFullscreen()}>
        Request FullScreen
      </Button>
    </>
  )
}

export default { simpleToast, toastOverModal, toastOverFullScreen }
