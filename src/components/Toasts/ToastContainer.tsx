import React from 'react'
import { ToastContainer as Container } from 'react-toastify'
import './toasts.css'

export const TOAST_CONTAINER_ID = 'atalaya__toast-container'

interface ToastContainerProps {
  /** Limits the amount of toasts that can be present any given time.  */
  limit?: number
}

export const ToastContainer = ({
  limit,
}: ToastContainerProps): React.ReactElement => {
  return (
    <Container
      containerId={TOAST_CONTAINER_ID}
      closeButton={false}
      closeOnClick={false}
      icon={false}
      limit={limit}
    />
  )
}
