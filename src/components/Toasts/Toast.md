The most flexible kind of toast provided. It provides a variety of optional
props that can be used to customize it to your needs.

```jsx
import {Button} from '../Button'
import {ButtonGroup} from '../ButtonGroup'
import {toastManager} from './toastManager'

const textToast = <Toast hideCloseButton>Nothing fancy</Toast>

const titleToast = (
  <Toast hideCloseButton title="Toast title">
    A bit more elaborated
  </Toast>
)

const closeToast = (
  <Toast title="Toast title">Things are starting to get interesting.</Toast>
)

const iconToast = (
  <Toast icon={<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
    <path fillRule="evenodd"
          d="M18 5v8a2 2 0 01-2 2h-5l-5 4v-4H4a2 2 0 01-2-2V5a2 2 0 012-2h12a2 2 0 012 2zM7 8H5v2h2V8zm2 0h2v2H9V8zm6 0h-2v2h2V8z"
          clipRule="evenodd"/>
  </svg>} title="Toast title">
    Starting to get crowded here...
  </Toast>
)

const actionToast = (
  <Toast
    icon={<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
      <path fillRule="evenodd"
            d="M18 5v8a2 2 0 01-2 2h-5l-5 4v-4H4a2 2 0 01-2-2V5a2 2 0 012-2h12a2 2 0 012 2zM7 8H5v2h2V8zm2 0h2v2H9V8zm6 0h-2v2h2V8z"
            clipRule="evenodd"/>
    </svg>}
    actionLabel="Take Action"
    onAction={() => {
    }}
    title="Toast title"
  >
    In case user needs to make a quick action.
  </Toast>
)

const cancelToast = (
  <Toast
    icon={<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
      <path fillRule="evenodd"
            d="M18 5v8a2 2 0 01-2 2h-5l-5 4v-4H4a2 2 0 01-2-2V5a2 2 0 012-2h12a2 2 0 012 2zM7 8H5v2h2V8zm2 0h2v2H9V8zm6 0h-2v2h2V8z"
            clipRule="evenodd"/>
    </svg>}
    actionLabel="Take Action"
    onAction={() => {
    }}
    cancelLabel="Cancel"
    onCancel={() => {
    }}
    title="Toast title"
  >
    In case user needs to make a quick action and might like to cancel
    something.
  </Toast>
)

const deleteToast = (
    <Toast
      icon={<svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
        <path fillRule="evenodd"
              d="M18 5v8a2 2 0 01-2 2h-5l-5 4v-4H4a2 2 0 01-2-2V5a2 2 0 012-2h12a2 2 0 012 2zM7 8H5v2h2V8zm2 0h2v2H9V8zm6 0h-2v2h2V8z"
              clipRule="evenodd"/>
      </svg>}
      actionLabel="Take Action"
      onAction={() => {
      }}
      cancelLabel="Cancel"
      onCancel={() => {
      }}
      deleteLabel="Delete"
      onDelete={() => {
      }}
      title="Toast title"
    >
      In case user needs to make a quick action and might like to cancel something
      or even do some destructive action.
    </Toast>
  )

;<ButtonGroup>
  <Button onClick={() => toastManager(textToast)}>Text</Button>
  <Button onClick={() => toastManager(titleToast)}>Title</Button>
  <Button onClick={() => toastManager(closeToast)}>Close button</Button>
  <Button onClick={() => toastManager(iconToast)}>Icon</Button>
  <Button onClick={() => toastManager(actionToast)}>Action</Button>
  <Button onClick={() => toastManager(cancelToast)}>Cancel</Button>
  <Button onClick={() => toastManager(deleteToast)}>Delete</Button>
</ButtonGroup>
```

Read the [ToastContainer](#/ReactComponents/Molecules/ToastContainer) docs for
more information on how toasts work.
