import React from 'react'
import { Button } from '../Button'
import { Icon } from '../Icon'
import { Inline } from '../../layout/Inline'
import { Stack } from '../../layout/Stack'
import { IconProps } from 'react-toastify'
import classNameParser from 'classnames'

export interface ToastProps {
  /** Label for action button */
  actionLabel?: React.ReactNode
  /** Label for cancel button */
  cancelLabel?: React.ReactNode
  /** Body of the toast */
  children?: React.ReactNode
  /** Internal. Sent by the toastManager */
  closeToast?: () => void
  /** Label for delete button */
  deleteLabel?: React.ReactNode
  /** Icon that will be displayed in the top left, accepts Icon component or svg */
  icon?: any | typeof Icon
  /** If true, X button won't be displayed */
  hideCloseButton?: boolean
  /** Callback when Action button is pressed, button will only be shown if this is provided */
  onAction?: () => void
  /** Callback when Cancel button is pressed, button will only be shown if this is provided */
  onCancel?: () => void
  /** Callback when Delete button is pressed, button will only be shown if this is provided */
  onDelete?: () => void
  /** Title of the toast */
  title?: React.ReactNode
  /** Internal. Sent by the toastManager */
  toastProps?: IconProps
}

const Toast = ({
  actionLabel = 'Agree',
  cancelLabel = 'Cancel',
  children,
  closeToast,
  deleteLabel = 'Delete',
  hideCloseButton,
  icon,
  onAction,
  onCancel,
  onDelete,
  title,
  toastProps,
}: ToastProps): React.ReactElement => {
  return (
    <Stack space="base" width="full">
      <div className="flex justify-between space-x-base">
        <div className="flex space-x-base w-full">
          {!!icon && (
            <div className="w-fit">
              <span
                className={classNameParser({
                  'text-color-info': toastProps?.type === 'info',
                  'text-color-positive': toastProps?.type === 'success',
                  'text-color-caution': toastProps?.type === 'warning',
                  'text-color-critical': toastProps?.type === 'error',
                })}
              >
                {icon}
              </span>
            </div>
          )}

          <div className="w-full">
            <div className="truncate font-bold">{title}</div>
            <div className="w-full text-color-subtle">{children}</div>
          </div>
        </div>

        {!hideCloseButton && (
          <div className="w-fit">
            <Button onClick={closeToast} size="sm" subtle>
              <Icon size="sm">
                <svg
                  className="h-6 w-6"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fillRule="evenodd"
                    d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  />
                </svg>
              </Icon>
            </Button>
          </div>
        )}
      </div>

      {(!!onAction || !!onCancel || !!onDelete) && (
        <Inline
          className="flex-row-reverse space-x-reverse"
          alignX="between"
          space="xxs"
        >
          <Inline
            className="flex-row-reverse space-x-reverse"
            alignX="end"
            space="xxs"
          >
            {!!onAction && (
              <Button onClick={onAction} subtle variant="main">
                {actionLabel}
              </Button>
            )}

            {!!onCancel && (
              <Button onClick={onCancel} subtle>
                {cancelLabel}
              </Button>
            )}
          </Inline>

          {!!onDelete && (
            <Button subtle variant="critical">
              {deleteLabel}
            </Button>
          )}
        </Inline>
      )}
    </Stack>
  )
}

export { Toast }
