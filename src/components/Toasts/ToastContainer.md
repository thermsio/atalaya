Toasts use [React-Toastify](https://github.com/fkhadra/react-toastify) under the hood. There are three different aspects
of making toasts works: The content, ToastContainer, toastManager.

### Content

Can be as simple as a string or a complex component. Atalaya provides some toast templates that should cover most of
your needs.

### Toast Container

It's the space where your toasts will get attached to. There only needs to be one `<ToastContainer />` in your app,
preferably near it's root.

Note: If you don't include a `ToastContainer` in your app the `toastManager` will append one for you.

```jsx static
import {ToastContainer} from './ToastContainer'

const App = () => {
  return (
    // ...
    // The rest of your app
    // ...

    <ToastContainer/>
  )
}

<App/>
```

### The Manager

The `toastManager` is what you will be using to control the toast in your app. It takes two arguments, first the content
of the toast that will be displayed, then an options object to configure it.

The simplest way to create a toast is to just pass a string to toast manager.

```jsx
import {Button} from '../Button'
import {toastManager} from './toastManager';

<Button onClick={() => toastManager('Simple, just a string')}>
  Dispatch Toast
</Button>
```

Available options are:

| Option          | Values         | Default     | Description                                                                                                                                         |
|-----------------|----------------|-------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
| autoClose       | number / false | 5000        | Set the delay in ms to close the toast automatically. Use `false` to prevent the toast from closing.                                                |
| hideProgressBar | boolean        | false       | Hide or show the progress bar                                                                                                                       |
| position        | string         | 'top-right' | Set the default position to use. One of: 'top-right', 'top-center', 'top-left', 'bottom-right', 'bottom-center', 'bottom-left' Default: 'top-right' |
| onClick         | function       |             | Fired when clicking inside toaster                                                                                                                  |
| onOpen          | function       |             | Called when toast is mounted.                                                                                                                       |
| onClose         | function       |             | Called when toast is unmounted.                                                                                                                     |
| toastId         | string         |             | Manually set the toast internal Id. Useful if you want to prevent the same toast from being fired multiple times.                                   |
| variant         | string         |             | Set the toast variant. One of: 'info', 'positive', 'caution', 'critical'                                                                            |

### autoClose

```jsx
import {Button} from '../Button'
import {ButtonGroup} from '../ButtonGroup'
import {Toast} from "./Toast";
import {toastManager} from './toastManager'
import {Icon} from '../Icon';

const longNotification = (
  <Toast title="Long"
         icon={<Icon>
           <svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
             <path fillRule="evenodd"
                   d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-12a1 1 0 10-2 0v4a1 1 0 00.293.707l2.828 2.829a1 1 0 101.415-1.415L11 9.586V6z"
                   clipRule="evenodd"/>
           </svg>
         </Icon>}>
    I will stay 10 seconds.
  </Toast>
)

const shortNotification = (
  <Toast title="Short"
         icon={<Icon>
           <svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
             <path fillRule="evenodd"
                   d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-12a1 1 0 10-2 0v4a1 1 0 00.293.707l2.828 2.829a1 1 0 101.415-1.415L11 9.586V6z"
                   clipRule="evenodd"/>
           </svg>
         </Icon>}>
    I will stay 2 seconds.
  </Toast>
)

const permanentNotification = (
    <Toast title="Permanent"
           icon={<Icon><svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
             <path fillRule="evenodd"
                   d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-12a1 1 0 10-2 0v4a1 1 0 00.293.707l2.828 2.829a1 1 0 101.415-1.415L11 9.586V6z"
                   clipRule="evenodd"/>
           </svg></Icon>}>
      I will never leave on my own.
    </Toast>
  )

;<ButtonGroup>
  <Button onClick={() => toastManager(longNotification, {autoClose: 10000})}>
    10 seconds
  </Button>
  <Button onClick={() => toastManager(shortNotification, {autoClose: 2000})}>
    2 seconds
  </Button>
  <Button
    onClick={() => toastManager(permanentNotification, {autoClose: false})}
  >
    No expiration
  </Button>
</ButtonGroup>
```

### hideProgressBar

```jsx
import {Button} from '../Button'
import {ButtonGroup} from '../ButtonGroup'
import {toastManager} from './toastManager'
import {Toast} from "./Toast";
import {Icon} from '../Icon';

const notification = (
    <Toast title="No Progress Bar"
           icon={
             <Icon>
               <svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                 <path fillRule="evenodd"
                       d="M18 5v8a2 2 0 01-2 2h-5l-5 4v-4H4a2 2 0 01-2-2V5a2 2 0 012-2h12a2 2 0 012 2zM7 8H5v2h2V8zm2 0h2v2H9V8zm6 0h-2v2h2V8z"
                       clipRule="evenodd"/>
               </svg>
             </Icon>}>
      I will leave enventually, you just don't know when.
    </Toast>
  )

;<Button onClick={() => toastManager(notification, {hideProgressBar: true})}>
  No progress bar
</Button>
```

### position

```jsx
import {Button} from '../Button'
import {ButtonGroup} from '../ButtonGroup'
import {toastManager} from './toastManager'
import {Toast} from "./Toast";
import {Icon} from '../Icon';

const notification = (
    <Toast title="Position"
           icon={
             <Icon>
               <svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                 <path fillRule="evenodd"
                       d="M18 5v8a2 2 0 01-2 2h-5l-5 4v-4H4a2 2 0 01-2-2V5a2 2 0 012-2h12a2 2 0 012 2zM7 8H5v2h2V8zm2 0h2v2H9V8zm6 0h-2v2h2V8z"
                       clipRule="evenodd"/>
               </svg>
             </Icon>}>
      This is a position example.
    </Toast>
  )

;<ButtonGroup>
  <Button
    onClick={() => toastManager(notification, {position: toastManager.POSITION.TOP_LEFT})}>
    Top left
  </Button>
  <Button
    onClick={() => toastManager(notification, {position: toastManager.POSITION.TOP_CENTER})}
  >
    Top center
  </Button>
  <Button
    onClick={() => toastManager(notification, {position: toastManager.POSITION.TOP_RIGHT})}>
    Top right
  </Button>
  <Button
    onClick={() => toastManager(notification, {position: toastManager.POSITION.BOTTOM_RIGHT})}
  >
    Bottom right
  </Button>
  <Button
    onClick={() => toastManager(notification, {position: toastManager.POSITION.BOTTOM_CENTER})}
  >
    Bottom center
  </Button>
  <Button
    onClick={() => toastManager(notification, {position: toastManager.POSITION.BOTTOM_LEFT})}
  >
    Bottom left
  </Button>
</ButtonGroup>
```

### variant

```jsx
import {Button} from '../Button'
import {ButtonGroup} from '../ButtonGroup'
import {toastManager} from './toastManager'
import {Toast} from "./Toast";
import {Icon} from '../Icon';

const notification = (
    <Toast title="Variants"
           icon={
             <Icon>
               <svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                 <path fillRule="evenodd"
                       d="M18 5v8a2 2 0 01-2 2h-5l-5 4v-4H4a2 2 0 01-2-2V5a2 2 0 012-2h12a2 2 0 012 2zM7 8H5v2h2V8zm2 0h2v2H9V8zm6 0h-2v2h2V8z"
                       clipRule="evenodd"/>
               </svg>
             </Icon>}
    >
      This is a semantic variant example.
    </Toast>
  )

;<ButtonGroup>
  <Button
    variant="info"
    onClick={() => toastManager(notification, {variant: toastManager.VARIANT.INFO})}
  >
    Info Notification
  </Button>
  <Button
    variant="positive"
    onClick={() => toastManager(notification, {variant: toastManager.VARIANT.POSITIVE})}
  >
    Positive Notification
  </Button>
  <Button
    variant="caution"
    onClick={() => toastManager(notification, {variant: toastManager.VARIANT.CAUTION})}
  >
    Caution Notification
  </Button>
  <Button
    variant="critical"
    onClick={() => toastManager(notification, {variant: toastManager.VARIANT.CRITICAL})}
  >
    Critical Notification
  </Button>
</ButtonGroup>
```

