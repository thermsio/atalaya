import React from 'react'
import { Avatar } from '../Avatar'
import { Button } from '../Button'

export interface ToastWithAvatarProps {
  /** Label for action button */
  actionLabel?: string
  avatarURL: string
  /** Body text of the toast, it will be truncated to one line */
  children?: string
  /** Callback when Action button is pressed, button will only be shown if this is provided */
  onAction?: () => void
  /** Title of the toast */
  title?: string
}

const ToastWithAvatar = ({
  actionLabel = 'Agree',
  avatarURL = 'https://therms.s3-us-west-2.amazonaws.com/images/avatar-placeholder.png',
  children,
  onAction,
  title,
}: ToastWithAvatarProps) => {
  return (
    <div className="flex items-center w-full space-x-xs">
      <div className="w-fit">
        <Avatar circular url={avatarURL} />
      </div>

      <div className="min-w-0">
        <p className="truncate font-bold">{title}</p>
        <p className="truncate">{children}</p>
      </div>

      {!!onAction && (
        <div className="min-w-fit">
          <Button onClick={onAction} subtle variant="main">
            {actionLabel}
          </Button>
        </div>
      )}
    </div>
  )
}

export { ToastWithAvatar }
