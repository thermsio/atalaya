Specialized Toast, meant to be used alongside the Avatar component to show notifications coming from other users.

Use cases: New Email notification, chat notifications, event notifications.

```jsx
import {ToastContainer} from './ToastContainer'
import {toastManager} from './toastManager'
import {Avatar} from '../Avatar'
import {Button} from '../Button'
import {ButtonGroup} from '../ButtonGroup'

const toast = (
  <ToastWithAvatar title="John Doe">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  </ToastWithAvatar>
)

const avatarToast = (
  <ToastWithAvatar
    avatar={
      <Avatar
        circular
        url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
        status="online"
      />
    }
    title="John Doe"
  >
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  </ToastWithAvatar>
);

const actionToast = (
  <ToastWithAvatar
    avatar={
      <Avatar
        circular
        url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU"
        status="online"
      />
    }
    actionLabel="Reply"
    onAction={() => {
    }}
    title="John Doe"
  >
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  </ToastWithAvatar>
);

<>
  <ButtonGroup>
    <Button onClick={() => toastManager(avatarToast)}>With Avatar</Button>
    <Button onClick={() => toastManager(toast)}>No Avatar</Button>
    <Button onClick={() => toastManager(actionToast)}>With Action</Button>
  </ButtonGroup>

  <ToastContainer/>
</>
```

Read the [ToastContainer](#/ReactComponents/Molecules/ToastContainer) docs for
more information on how toasts work.
