import React from 'react'
import { Button } from '../Button'

export interface BreadcrumbProps {
  children?: React.ReactNode
  clickable?: boolean
  onClick?: () => void
}

const Breadcrumb = ({
  children,
  clickable,
  onClick,
}: BreadcrumbProps): React.ReactElement => {
  if (clickable)
    return (
      <Button
        size="xs"
        variant="main"
        subtle
        onClick={onClick}
        className="truncate"
      >
        <div className="truncate" style={{ maxWidth: '150px' }}>
          {children}
        </div>
      </Button>
    )

  return (
    <div
      className="truncate text-sm font-bold text-color-subtle"
      style={{ maxWidth: '150px' }}
    >
      {children}
    </div>
  )
}

export { Breadcrumb }
