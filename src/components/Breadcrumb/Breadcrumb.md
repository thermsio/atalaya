Breadcrumb is meant to be used as a children of [Breadcrumbs](#/ReactComponents/Molecules/Breadcrumbs). If the
prop `clickable` is true it will respond to clicks and activate the `onClick` callback upon them. It will look
non-interactive text otherwise.

#### Clickable

```jsx
<Breadcrumb clickable onClick={() => alert('You clicked on me!')}>Clickable</Breadcrumb>
```

#### Not Clickable

```jsx
<Breadcrumb>Not Clickable</Breadcrumb>
```
