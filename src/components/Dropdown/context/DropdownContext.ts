import { createContext } from 'react'

export const DropdownContext = createContext({
  close: () => {
    /*void*/
  },
  isInsideDropdown: false,
  isOpen: false,
  open: () => {
    /*void*/
  },
})
