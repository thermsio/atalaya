### Simple

```jsx
<Dropdown>
  <div className="min-w-max rounded bg-positive p-base">A Dropdown!</div>
</Dropdown>
```

### Custom string on controller

```jsx
<Dropdown controller="Open Dropdown">
  <div className="min-w-max rounded bg-positive p-base">A Dropdown!</div>
</Dropdown>
```

### Custom controller

```jsx
import { Button } from '../Button'
;<Dropdown controller={<Button variant="positive">Custom Controller</Button>}>
  <div className="min-w-max rounded bg-positive p-base">A Dropdown!</div>
</Dropdown>
```

#### "Render Prop" controller

If you provide a function as the `controller` prop then you must handle the call to `open()` the dropdown.

```jsx
import { Button } from '../Button'
;<Dropdown
  controller={({ open }) => (
    <Button onClick={open} variant="positive">
      Open handler
    </Button>
  )}
>
  <div className="min-w-max rounded bg-positive p-base">A Dropdown!</div>
</Dropdown>
```

### Controlled mode

If you provide the isOpen prop, the component will work on controlled mode. To update the state of `isOpen` you may
use `onClose` and `onOpen` props. Since no controller will be rendered internally by Dropdown, there is no way for it to know which element should it attach itself to, you can let Dropdown know where it should attach to by providing a ref to `containerRef` prop.

```jsx
import { Button } from '../Button'

const [isOpen, setIsOpen] = React.useState(false)
const containerRef = React.useRef(null)

;<>
  <Button onClick={() => setIsOpen(!isOpen)}>Toggle Dropdown!</Button>

  <div
    className="relative ml-auto w-[300px] bg-caution-faded text-center"
    ref={containerRef}
  >
    <span className="italic tracking-wide text-color-subtle">
      Dropdown Container
    </span>

    <Dropdown
      containerRef={containerRef}
      isOpen={isOpen}
      onClose={() => setIsOpen(false)}
      onOpen={() => setIsOpen(true)}
    >
      <div className="min-w-max rounded bg-positive p-base">A Dropdown!</div>
    </Dropdown>
  </div>
</>
```

### Absolute Position

```jsx
<div className="relative h-36 w-full rounded bg-surface">
  <Dropdown controllerPlace="top-left">
    <div className="min-w-max rounded bg-positive p-base">A Dropdown!</div>
  </Dropdown>

  <Dropdown controllerPlace="top-right">
    <div className="min-w-max rounded bg-positive p-base">A Dropdown!</div>
  </Dropdown>

  <Dropdown controllerPlace="bottom-right">
    <div className="min-w-max rounded bg-positive p-base">A Dropdown!</div>
  </Dropdown>

  <Dropdown controllerPlace="bottom-left">
    <div className="min-w-max rounded bg-positive p-base">A Dropdown!</div>
  </Dropdown>
</div>
```

### Dropdown Position

```jsx
<div className="flex space-x-base">
  <Dropdown controller="bottom-left" dropdownPlace="bottom-left">
    <div className="min-w-max rounded bg-positive p-base">
      A Dropdown on the Bottom Left!
    </div>
  </Dropdown>

  <Dropdown controller="bottom-right" dropdownPlace="bottom-right">
    <div className="min-w-max rounded bg-positive p-base">
      A Dropdown on the Bottom Right!
    </div>
  </Dropdown>

  <Dropdown controller="top-left" dropdownPlace="top-left">
    <div className="min-w-max rounded bg-positive p-base">
      A Dropdown on the Top Left!
    </div>
  </Dropdown>

  <Dropdown controller="top-right" dropdownPlace="top-right">
    <div className="min-w-max rounded bg-positive p-base">
      A Dropdown on the Top Right!
    </div>
  </Dropdown>

  <Dropdown controller="top" dropdownPlace="top">
    <div className="min-w-max rounded bg-positive p-base">
      A Dropdown on the Top!
    </div>
  </Dropdown>

  <Dropdown controller="bottom" dropdownPlace="bottom">
    <div className="min-w-max rounded bg-positive p-base">
      A Dropdown on the Bottom!
    </div>
  </Dropdown>

  <Dropdown controller="left" dropdownPlace="left">
    <div className="min-w-max rounded bg-positive p-base">
      A Dropdown on the Left!
    </div>
  </Dropdown>

  <Dropdown controller="right" dropdownPlace="right">
    <div className="min-w-max rounded bg-positive p-base">
      A Dropdown on the Right!
    </div>
  </Dropdown>
</div>
```
