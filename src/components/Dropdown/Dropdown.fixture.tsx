import React from 'react'
import { Dropdown } from './index'
import { Text } from '../FormControls/Text/Text'
import { Button } from '../Button'
import { Inline } from '../../layout/Inline'
import { Stack } from '../../layout/Stack'
import { Select } from '../FormControls/Select/Select'

const nestedDropdown = () => {
  return (
    <Stack space="lg">
      <Dropdown>
        <div className="bg-surface p-base">
          This is a dropdown
          <Dropdown>This is a nested dropdown</Dropdown>
        </div>
      </Dropdown>

      <Inline space="sm">
        <Text />
        <Dropdown controller="Controller is a string">
          <div className="bg-surface p-base">
            This is a dropdown
            <Dropdown>This is a nested dropdown</Dropdown>
          </div>
        </Dropdown>
      </Inline>

      <Inline space="sm">
        <Text />
        <Dropdown
          controller={
            <Button size="base" subtle>
              <span>Controller is a Button</span>
              <span>+</span>
            </Button>
          }
        >
          <div className="bg-surface p-base">
            This is a dropdown
            <Dropdown>This is a nested dropdown</Dropdown>
          </div>
        </Dropdown>
      </Inline>
    </Stack>
  )
}

const withInput = () => {
  return (
    <Dropdown>
      <div className="min-w-[300px]">
        <Text />
      </div>
    </Dropdown>
  )
}

const selectOptions = new Array(50).fill(null).map((value, index) => ({
  value: index + 1,
  label: `Record Number ${index + 1}`,
}))

const withSelect = () => {
  return (
    <Dropdown>
      <Select options={selectOptions} />
    </Dropdown>
  )
}

const withLotsOfText = (
  <Dropdown>
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
      tempor incididunt ut labore et dolore magna aliqua. Odio morbi quis
      commodo odio. At lectus urna duis convallis convallis. Mauris sit amet
      massa vitae tortor condimentum. Egestas integer eget aliquet nibh.
      Sollicitudin aliquam ultrices sagittis orci a scelerisque. Erat
      pellentesque adipiscing commodo elit at imperdiet dui accumsan sit. Eget
      velit aliquet sagittis id consectetur purus. Vel elit scelerisque mauris
      pellentesque pulvinar pellentesque habitant. Elit duis tristique
      sollicitudin nibh sit amet commodo. Accumsan tortor posuere ac ut
      consequat semper viverra nam libero. Arcu dui vivamus arcu felis bibendum
      ut tristique. Odio facilisis mauris sit amet massa vitae tortor
      condimentum. Eget magna fermentum iaculis eu non. Venenatis lectus magna
      fringilla urna porttitor rhoncus dolor. Cras tincidunt lobortis feugiat
      vivamus at augue eget arcu. Netus et malesuada fames ac turpis egestas
      integer. Facilisis sed odio morbi quis.
    </p>

    <p>
      Donec enim diam vulputate ut pharetra sit amet aliquam id. Enim neque
      volutpat ac tincidunt vitae semper quis lectus nulla. Quis lectus nulla at
      volutpat diam ut venenatis tellus in. Faucibus nisl tincidunt eget nullam
      non nisi est. Sed libero enim sed faucibus turpis. Imperdiet massa
      tincidunt nunc pulvinar sapien. Feugiat in fermentum posuere urna nec.
      Rutrum quisque non tellus orci ac. Ullamcorper malesuada proin libero nunc
      consequat interdum varius. Lectus quam id leo in. Congue nisi vitae
      suscipit tellus mauris a diam maecenas sed. Tellus at urna condimentum
      mattis pellentesque id nibh tortor id. Elit ullamcorper dignissim cras
      tincidunt lobortis feugiat vivamus at. In fermentum posuere urna nec
      tincidunt. Facilisi cras fermentum odio eu feugiat pretium nibh ipsum
      consequat. Sapien eget mi proin sed libero enim sed faucibus. Proin sed
      libero enim sed faucibus. Enim neque volutpat ac tincidunt vitae semper
      quis. Tellus pellentesque eu tincidunt tortor aliquam nulla. Diam maecenas
      ultricies mi eget.
    </p>

    <p>
      Neque volutpat ac tincidunt vitae semper quis lectus nulla. Ante metus
      dictum at tempor. Lacus laoreet non curabitur gravida. Sollicitudin tempor
      id eu nisl nunc mi. Rutrum tellus pellentesque eu tincidunt tortor aliquam
      nulla. A arcu cursus vitae congue mauris rhoncus aenean vel elit. In
      hendrerit gravida rutrum quisque non tellus orci ac auctor. Amet nulla
      facilisi morbi tempus. Neque gravida in fermentum et sollicitudin ac orci
      phasellus egestas. Et leo duis ut diam quam nulla porttitor massa id.
      Risus quis varius quam quisque id diam. Turpis egestas maecenas pharetra
      convallis. Magna ac placerat vestibulum lectus mauris ultrices eros in
      cursus. Turpis cursus in hac habitasse. Sed arcu non odio euismod lacinia.
    </p>

    <p>
      Mauris ultrices eros in cursus. Aenean pharetra magna ac placerat
      vestibulum lectus mauris ultrices eros. Fermentum odio eu feugiat pretium
      nibh ipsum consequat nisl. Ullamcorper sit amet risus nullam eget felis
      eget nunc lobortis. Enim neque volutpat ac tincidunt vitae semper quis
      lectus nulla. Pharetra diam sit amet nisl suscipit adipiscing bibendum
      est. Est ultricies integer quis auctor. Et netus et malesuada fames ac
      turpis egestas. Arcu felis bibendum ut tristique et. Pellentesque habitant
      morbi tristique senectus et netus. Adipiscing elit duis tristique
      sollicitudin nibh sit. Sed pulvinar proin gravida hendrerit lectus a.
      Etiam tempor orci eu lobortis elementum. Fermentum leo vel orci porta non
      pulvinar neque. Mattis rhoncus urna neque viverra justo nec ultrices.
      Fermentum dui faucibus in ornare quam viverra. Scelerisque eleifend donec
      pretium vulputate sapien. Congue mauris rhoncus aenean vel elit. Ut tellus
      elementum sagittis vitae et leo. Pellentesque adipiscing commodo elit at
      imperdiet.
    </p>

    <p>
      Cras adipiscing enim eu turpis egestas. Tempor commodo ullamcorper a lacus
      vestibulum sed arcu non. Vel risus commodo viverra maecenas accumsan
      lacus. Cras adipiscing enim eu turpis. Porta lorem mollis aliquam ut
      porttitor leo a diam sollicitudin. Sed ullamcorper morbi tincidunt ornare
      massa. Ultricies leo integer malesuada nunc vel risus commodo. Tincidunt
      tortor aliquam nulla facilisi cras fermentum odio eu. Habitant morbi
      tristique senectus et netus et malesuada. A cras semper auctor neque vitae
      tempus. Mi ipsum faucibus vitae aliquet. Mi quis hendrerit dolor magna
      eget est lorem ipsum dolor. Donec adipiscing tristique risus nec feugiat.
      Leo vel orci porta non pulvinar neque laoreet.
    </p>
  </Dropdown>
)

export default { nestedDropdown, withInput, withSelect, withLotsOfText }
