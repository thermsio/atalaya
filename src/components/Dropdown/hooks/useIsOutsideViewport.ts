import React, { useLayoutEffect, useState } from 'react'
import { useWindowSize } from '../../../hooks/useWindowSize'

interface UseIsOutsideViewportProps<Element = HTMLElement> {
  (ref: React.RefObject<Element>): {
    bottom: boolean
    left: boolean
    right: boolean
    top: boolean
  }
}

const useIsOutsideViewport: UseIsOutsideViewportProps = (ref) => {
  const [hasWarned, setHasWarned] = useState(false)
  const [isOutsideOfViewport, setIsOutsideViewport] = useState({
    bottom: false,
    left: false,
    right: false,
    top: false,
  })

  const { height, width } = useWindowSize()

  useLayoutEffect(() => {
    if (!ref?.current) {
      if (!hasWarned) {
        console.warn('Atalaya/useIsOutsideViewport: no element provided')
        setHasWarned(true)
      }

      setIsOutsideViewport({
        bottom: false,
        left: false,
        right: false,
        top: false,
      })
    } else {
      const elementPosition = ref.current.getBoundingClientRect()

      setIsOutsideViewport({
        bottom: !!height && elementPosition.bottom > height,
        left: elementPosition.left < 0,
        right: !!width && elementPosition.right > width,
        top: elementPosition.top < 0,
      })
    }
  }, [ref?.current, height, width])

  return isOutsideOfViewport
}

export { useIsOutsideViewport }
