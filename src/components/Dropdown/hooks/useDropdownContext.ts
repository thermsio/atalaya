import { useContext } from 'react'
import { DropdownContext } from '../context/DropdownContext'

export function useDropdownContext({ ctxRequired } = { ctxRequired: false }) {
  const context = useContext(DropdownContext)

  if (!context && ctxRequired) {
    throw new Error('useDropdownContext must be used within a DropdownProvider')
  }

  return context
}
