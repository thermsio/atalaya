import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import { Button } from '../Button'
import { Icon } from '../Icon'
import classNameParser from 'classnames'
import { DropdownContainer } from '../DropdownContainer'
import { MobileBottomSheet } from './components/MobileBottomSheet'
import { useIsBreakpointActive } from '../../hooks/useIsBreakpointActive'
import classNames from 'classnames'
import { DropdownContext } from './context/DropdownContext'

export type Placement =
  | 'bottom'
  | 'bottom-left'
  | 'bottom-right'
  | 'left'
  | 'right'
  | 'top'
  | 'top-left'
  | 'top-right'

export interface DropdownProps {
  children?: React.ReactNode
  className?: string
  /** If the `controller` is a function,
   * then it is implied that the controller will handle the click event to open the dropdown container. */
  controller?:
    | React.ReactNode
    | ((params: {
        close: () => void
        isOpen: boolean
        open: () => void
      }) => React.ReactNode)
  containerRef?:
    | React.MutableRefObject<HTMLElement>
    | React.RefObject<HTMLElement>
  controllerPlace?: Placement
  disabled?: boolean
  dropdownContainerClass?: string
  dropdownInset?: boolean
  dropdownPlace?: Placement
  isOpen?: boolean
  onClose?: () => void
  onOpen?: () => void
}

const Dropdown = ({
  children,
  className,
  containerRef,
  controller,
  controllerPlace,
  disabled,
  dropdownContainerClass,
  dropdownInset,
  dropdownPlace,
  isOpen,
  onClose,
  onOpen,
}: DropdownProps): React.ReactElement | null => {
  const [isOpenLocal, setIsOpenLocal] = useState(false)
  const isControlled = isOpen !== undefined
  const _isOpen = isControlled ? isOpen : isOpenLocal
  const dropdownContainerRef = useRef(null)
  const _containerRef = containerRef?.current
    ? containerRef
    : dropdownContainerRef

  useEffect(() => {
    if (isOpenLocal && disabled) setIsOpenLocal(false)
  }, [disabled, isOpenLocal])

  const useBottomSheet = !useIsBreakpointActive('sm')

  const closeHandler = useCallback(() => {
    if (onClose) onClose()
    if (!isControlled) setIsOpenLocal(false)
  }, [isControlled, onClose])

  const openHandler = useCallback(() => {
    if (onOpen) onOpen()
    if (!isControlled) setIsOpenLocal(true)
  }, [isControlled, onOpen])

  const controllerWrapper = useMemo(() => {
    if (typeof controller === 'string') {
      return (
        <div
          className={classNames(
            { 'ring-2': _isOpen, 'ring-neutral-dark': _isOpen },
            'btn btn-subtle btn-neutral flex hover:cursor-pointer',
          )}
          onClick={openHandler}
        >
          {controller}
        </div>
      )
    } else if (typeof controller === 'function') {
      // we let the controller handle the clicks/open trigger
      return controller({
        close: closeHandler,
        isOpen: _isOpen,
        open: openHandler,
      })
    } else if (controller) {
      return (
        <div className="flex rounded" onClick={openHandler}>
          {controller}
        </div>
      )
    } else if (!isControlled) {
      return (
        <Button className="flex" onClick={openHandler} subtle>
          <Icon>
            <svg
              className="h-6 w-6"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="M10 6a2 2 0 110-4 2 2 0 010 4zM10 12a2 2 0 110-4 2 2 0 010 4zM10 18a2 2 0 110-4 2 2 0 010 4z" />
            </svg>
          </Icon>
        </Button>
      )
    }

    return null
  }, [openHandler, controller, _isOpen])

  if (!React.Children.count(children)) return null

  return (
    <DropdownContext.Provider
      value={{
        close: closeHandler,
        isInsideDropdown: true,
        isOpen: _isOpen,
        open: openHandler,
      }}
    >
      <div
        className={classNameParser(
          {
            absolute: controllerPlace,
            relative: !controllerPlace,
            'bottom-0 left-0': controllerPlace === 'bottom-left',
            'bottom-0 right-0': controllerPlace === 'bottom-right',
            'left-0 top-0': controllerPlace === 'top-left',
            'right-0 top-0': controllerPlace === 'top-right',
          },
          // reset the font, so it doesn't inherit from a parent, ie: parent has text-lg
          'w-fit text-base',
          className,
        )}
      >
        {controllerWrapper}

        {!useBottomSheet && _isOpen && (
          <div ref={dropdownContainerRef}>
            <DropdownContainer
              className={dropdownContainerClass}
              containerRef={
                _containerRef as React.MutableRefObject<HTMLDivElement>
              }
              inset={dropdownInset}
              onClose={closeHandler}
              placement={dropdownPlace}
            >
              {children}
            </DropdownContainer>
          </div>
        )}

        {useBottomSheet && (
          <MobileBottomSheet isOpen={_isOpen} onClose={closeHandler}>
            {children}
          </MobileBottomSheet>
        )}
      </div>
    </DropdownContext.Provider>
  )
}

export { Dropdown }
