import React from 'react'
import { BottomSheet } from 'react-spring-bottom-sheet'

import './MobileBottomSheet.css'

interface MobileBottomSheetProps {
  children?: React.ReactNode
  isOpen?: boolean
  onClose: () => void
}

const MobileBottomSheet = ({
  children,
  isOpen,
  onClose,
}: MobileBottomSheetProps): React.ReactElement => {
  return (
    <BottomSheet
      open={!!isOpen}
      onDismiss={onClose}
      snapPoints={({ minHeight, maxHeight }) => {
        const _snapPoints = [minHeight]

        if (minHeight < maxHeight / 2) _snapPoints.push(maxHeight / 2)

        _snapPoints.push(maxHeight)

        return _snapPoints
      }}
    >
      {children}
    </BottomSheet>
  )
}

export { MobileBottomSheet }
