import React from 'react'

import { Breadcrumb } from '../Breadcrumb'
import { Icon } from '../Icon'
import { Button } from '../Button'

export interface BreadcrumbsProps {
  backClickIcon?: React.ReactNode
  children?: React.ReactNode
  onBackClick?: () => void
  separatorIcon?: React.ReactNode
}

const Breadcrumbs = ({
  backClickIcon,
  children,
  onBackClick,
  separatorIcon,
}: BreadcrumbsProps): React.ReactElement | null => {
  if (!children || !React.Children.count(children)) return null

  return (
    <div className="flex w-full items-center space-x-xxs">
      {!!onBackClick && (
        <Button
          className="mr-xxs shrink-0"
          onClick={onBackClick}
          subtle
          size="xs"
          variant="main"
        >
          {backClickIcon || (
            <Icon className="shrink-0" size="sm">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="currentColor"
              >
                <path d="M0 0h24v24H0V0z" fill="none" />
                <path d="M14.91 6.71c-.39-.39-1.02-.39-1.41 0L8.91 11.3c-.39.39-.39 1.02 0 1.41l4.59 4.59c.39.39 1.02.39 1.41 0 .39-.39.39-1.02 0-1.41L11.03 12l3.88-3.88c.38-.39.38-1.03 0-1.41z" />
              </svg>
            </Icon>
          )}
        </Button>
      )}

      {React.Children.map(children, (child, index) => {
        return (
          <>
            {child}

            {Array.isArray(children) &&
              index + 1 < React.Children.count(children) &&
              (separatorIcon || (
                <Icon color="subtle" size="sm" className="shrink-0">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-5 w-5"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                      clipRule="evenodd"
                    />
                  </svg>
                </Icon>
              ))}
          </>
        )
      })}
    </div>
  )
}

Breadcrumbs.Crumb = Breadcrumb

export { Breadcrumbs }
