> Breadcrumbs are a secondary form of navigation that assists users in getting to content nearby in the hierarchical
> structure. They are especially useful when users arrive to the site through an external link and don’t start with the
> homepage.
>
> _[-NNgroup](https://www.nngroup.com/articles/breadcrumbs/)_

This is a two part component. `Breadcrumbs` is meant to be a wrapper for a list of [Breadcrumb](#/Atoms/Breadcrumb).
This child component is conveniently included as a subcomponent as `Breadcrumbs.Crumb`. Crumbs can be customized individually.

`Breadcrumbs` will automatically place a nest icon after each item.

If on `onBackClick` prop is present, will cause a back arrow to appear before Crumbs, this is meant to act as a back button. When its clicked it will fire up the callback.

```jsx
<Breadcrumbs onBackClick={() => alert('Back Clicked')}>
  <Breadcrumbs.Crumb>First Step</Breadcrumbs.Crumb>
  <Breadcrumbs.Crumb clickable onClick={() => alert('Crumb Clicked')}>
    Second Step
  </Breadcrumbs.Crumb>
  <Breadcrumbs.Crumb>Third Step</Breadcrumbs.Crumb>
</Breadcrumbs>
```
