import React from 'react'
import { Breadcrumbs } from './index'

const base = (
  <Breadcrumbs onBackClick={() => console.log('Back Clicked')}>
    <Breadcrumbs.Crumb>First Step</Breadcrumbs.Crumb>
    <Breadcrumbs.Crumb clickable>Second Step</Breadcrumbs.Crumb>
    <Breadcrumbs.Crumb>Third Step</Breadcrumbs.Crumb>
  </Breadcrumbs>
)

const widerThanContainer = (
  <Breadcrumbs onBackClick={() => console.log('Back Clicked')}>
    <Breadcrumbs.Crumb>First Step Long</Breadcrumbs.Crumb>
    <Breadcrumbs.Crumb clickable>
      Fifth Step Longest Longest Longest Longest Longest Longest
      Longeeeeeeeeeeest
    </Breadcrumbs.Crumb>
    <Breadcrumbs.Crumb clickable>Second Step</Breadcrumbs.Crumb>
    <Breadcrumbs.Crumb>Third Step</Breadcrumbs.Crumb>
    <Breadcrumbs.Crumb>Fourth Step Longer Longer</Breadcrumbs.Crumb>
  </Breadcrumbs>
)

const withBackground = (
  <div className="bg-surface-subtle">
    <Breadcrumbs onBackClick={() => console.log('Back Clicked')}>
      <Breadcrumbs.Crumb>First Step</Breadcrumbs.Crumb>
      <Breadcrumbs.Crumb clickable>Second Step</Breadcrumbs.Crumb>
      <Breadcrumbs.Crumb>Third Step</Breadcrumbs.Crumb>
    </Breadcrumbs>
  </div>
)

export default {
  base,
  widerThanContainer,
  withBackground,
}
