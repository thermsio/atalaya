import React from 'react'

export interface OverlayProps {
  children?: React.ReactNode
}

const Overlay = ({ children }: OverlayProps): React.ReactElement => {
  return (
    <div className="absolute inset-0 z-backdrop flex items-center justify-center">
      <div className="absolute inset-0 bg-background opacity-80" />

      {!!children && <div className="relative">{children}</div>}
    </div>
  )
}

export { Overlay }
