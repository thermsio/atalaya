```jsx inside Markdown
const data = [
  {
    activity: 'Left the warehouse',
    completed: true,
    displayName: 'Tom Usman',
    id: '123',
    subEvents: [
      {
        activity: 'crept down the block',
        displayName: 'Drizzy',
        timestamp: '10, Mar 2022 at 10:50pm',
      },
      {
        activity: 'made a right',
        displayName: 'Drake',
        timestamp: '10, Mar 2022 at 10:53pm',
      },
    ],
    timestamp: new Date(),
  },
  {
    activity: 'Approaching destination',
    completed: false,
    displayName: 'Cory Eldar',
    id: '123',

    subEvents: [
      {
        activity: 'cut the lights',
        displayName: 'Aubrey',
        timestamp: '10, Mar 2022 at 11:55pm',
      },
      {
        activity: 'paid the price',
        displayName: 'Graham',
        timestamp: '11, Mar 2022 at 12am',
      },
    ],
    timestamp: new Date(),
  },
]

const renderTimeline = (item) => (
  <>
    <h3 className="color-text-strong text-lg font-bold">{item.displayName}</h3>
    <p className="color-neutral mb-base text-base">{item.activity}</p>
  </>
)

return (
  <div>
    <Timeline
      data={data}
      render={renderTimeline}
      timestampExtractor={(item) => item.timestamp.toLocaleString()}
    />
  </div>
)
```
