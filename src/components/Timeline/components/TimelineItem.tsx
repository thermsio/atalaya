import React from 'react'
import { Icon } from '../../Icon'
import classnames from 'classnames'
import { getSemanticVariant } from '../../../utils/get-semantic-variant'
import { SemanticVariant } from '../../../constants'

type TimelineItemProps = React.PropsWithChildren<{
  className?: string
  horizontal?: boolean
  icon?: React.ReactNode
  iconBgColor?: SemanticVariant | string
  timestamp?: React.ReactNode
}>

const DefaultIcon = (
  <Icon color="semantic" size="sm">
    <svg
      fill="currentColor"
      viewBox="0 0 20 20"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z"
        clipRule="evenodd"
      ></path>
    </svg>
  </Icon>
)

const TimelineItem: React.FC<TimelineItemProps> = ({
  children,
  horizontal,
  icon,
  iconBgColor,
  timestamp,
}: TimelineItemProps) => {
  const bgClass = iconBgColor
    ? `bg-${getSemanticVariant(iconBgColor)}`
    : 'bg-info'
  const bgStyle = bgClass === 'bg-' ? iconBgColor : undefined

  if (horizontal) {
    return (
      <li className="group mb-lg sm:mb-0">
        <div className="flex items-center">
          <div className="rounded-full py-1 pr-1">
            <div
              className={classnames(
                'flex h-8 w-8 shrink-0 items-center justify-center rounded-full',
                { [bgClass]: bgClass },
              )}
              style={bgStyle ? { backgroundColor: bgStyle } : {}}
            >
              {icon || DefaultIcon}
            </div>
          </div>

          <div className="mr-1 hidden h-0.5 w-full rounded bg-border sm:flex sm:group-last:hidden" />
        </div>

        {!!timestamp && (
          <time className="mt-xs block text-sm text-color-neutral">
            {timestamp}
          </time>
        )}

        <div className="sm:pr-lg sm:group-last:pr-0">{children}</div>
      </li>
    )
  } else {
    return (
      <li className="group flex space-x-sm">
        <div className="relative flex flex-col items-center">
          <div className="rounded-full">
            <div
              className={classnames(
                'relative flex h-8 w-8 items-center justify-center rounded-full',
                { [bgClass]: bgClass },
              )}
              style={bgStyle ? { backgroundColor: bgStyle } : {}}
            >
              {icon || DefaultIcon}
            </div>
          </div>

          <div className="w-0.5 grow rounded bg-border group-last:hidden" />
        </div>

        <div>
          {!!timestamp && (
            <time className="mb-xxs text-sm text-color-neutral">
              {timestamp}
            </time>
          )}

          {children}
        </div>
      </li>
    )
  }
}

export { TimelineItem }
