/* eslint-disable react/prop-types */
import React from 'react'
import { createRoot } from 'react-dom/client'
import { Timeline } from './index'

const data = [
  {
    activity: 'Left the warehouse',
    completed: true,
    displayName: 'Tom Usman',
    id: '123',
    subEvents: [
      {
        activity: 'crept down the block',
        displayName: 'Drizzy',
        timestamp: '10, Mar 2022 at 10:50pm',
      },
      {
        activity: 'made a right',
        displayName: 'Drake',
        timestamp: '10, Mar 2022 at 10:53pm',
      },
    ],
    timestamp: new Date(),
  },
  {
    activity: 'Approaching destination',
    completed: false,
    displayName: 'Cory Eldar',
    id: '123',

    subEvents: [
      {
        activity: 'cut the lights',
        displayName: 'Aubrey',
        timestamp: '10, Mar 2022 at 11:55pm',
      },
      {
        activity: 'paid the price',
        displayName: 'Graham',
        timestamp: '11, Mar 2022 at 12am',
      },
    ],
    timestamp: new Date(),
  },
]

const renderTimeline = (item) => (
  <>
    <h3 className="color-text-strong text-lg font-bold">{item.displayName}</h3>
    <p className="color-neutral mb-base text-base">{item.activity}</p>
  </>
)

it('Timeline renders without crashing', () => {
  const root = createRoot(document.createElement('div'))
  root.render(
    <Timeline
      data={data}
      render={renderTimeline}
      timestampExtractor={(item: (typeof data)[number]) =>
        item.timestamp.toLocaleString()
      }
    />,
  )
})

it('Timeline accepts renderTimestamp prop', () => {
  const root = createRoot(document.createElement('div'))
  root.render(
    <Timeline
      data={data}
      render={renderTimeline}
      renderTimestamp={(item: (typeof data)[number]) => (
        <span>{new Date(item.timestamp).toLocaleString()}</span>
      )}
    />,
  )
})
