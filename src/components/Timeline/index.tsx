import React, { useEffect } from 'react'
import { TimelineItem } from './components/TimelineItem'
import classnames from 'classnames'
import { SemanticVariant } from '../../constants'

export interface TimelineProps<TimelineEvent> {
  /** The list of events */
  data: TimelineEvent[]
  /** If true, the timeline will be horizontal (stepper) */
  horizontal?: boolean
  /** Icon that will be used for each event */
  icon?: React.ReactNode
  /** Optionally provide a semantic-variant or valid HEX for the icon circle's background color */
  iconBgColor?: SemanticVariant | string
  /** Function to render each event */
  render: (timelineItem: TimelineEvent) => React.ReactNode
  /** Optional render prop to render the timestamp view */
  renderTimestamp?: (timelineItem: TimelineEvent) => React.ReactNode
  /** Optional if no `renderTimestamp` prop provided. Function to extract timestamp from each event */
  timestampExtractor?: (timelineItem: TimelineEvent) => string
}

const Timeline = <TimelineEvent,>({
  data,
  horizontal,
  icon,
  iconBgColor,
  render,
  renderTimestamp,
  timestampExtractor,
}: TimelineProps<TimelineEvent>) => {
  useEffect(() => {
    if (!renderTimestamp && !timestampExtractor) {
      console.error(
        '<Timeline /> component did not receive a prop for rendering or extracting the timestamp.',
      )
    }
  }, [])

  if (!data?.length) return null

  return (
    <ol
      className={classnames(
        {
          'items-center sm:flex': horizontal,
          relative: !horizontal,
        },
        'list-none pl-0',
      )}
    >
      {data.map((event, index) => {
        const timestamp = renderTimestamp
          ? renderTimestamp(event)
          : timestampExtractor?.(event)

        return (
          <TimelineItem
            horizontal={horizontal}
            icon={icon}
            iconBgColor={iconBgColor}
            timestamp={timestamp}
            key={`${timestamp}_${index}`}
          >
            {render(event)}
          </TimelineItem>
        )
      })}
    </ol>
  )
}

Timeline.Item = TimelineItem

export { Timeline }
