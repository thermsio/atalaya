/* eslint-disable */
import React from 'react'
import { Timeline } from './index'

const data = [
  {
    activity: 'Left the warehouse',
    completed: true,
    displayName: 'Tom Usman',
    id: '123',
    subEvents: [
      {
        activity: 'crept down the block',
        displayName: 'Drizzy',
        timestamp: '10, Mar 2022 at 10:50pm',
      },
      {
        activity: 'made a right',
        displayName: 'Drake',
        timestamp: '10, Mar 2022 at 10:53pm',
      },
    ],
    timestamp: new Date(),
  },
  {
    activity: 'Approaching destination',
    completed: false,
    displayName: 'Cory Eldar',
    id: '123',

    subEvents: [
      {
        activity: 'cut the lights',
        displayName: 'Aubrey',
        timestamp: '10, Mar 2022 at 11:55pm',
      },
      {
        activity: 'paid the price',
        displayName: 'Graham',
        timestamp: '11, Mar 2022 at 12am',
      },
    ],
    timestamp: new Date(),
  },
  {
    activity: 'Some other thing',
    completed: true,
    displayName: 'Tom Usman',
    id: '234',
    timestamp: new Date(),
  },
]

const renderInnerStepperTimeline = (item) => (
  <div className="w-48">
    <h3 className="color-text-strong text-base font-bold">
      {item.displayName}
    </h3>
    <p className="color-neutral mb-sm text-sm">{item.activity}</p>
  </div>
)

const renderTimeline = (item) => (
  <>
    <h3 className="color-text-strong text-lg font-bold">{item.displayName}</h3>
    <p className="color-neutral mb-base text-sm">{item.activity}</p>
    {!!item.subEvents && (
      <Timeline
        data={item.subEvents}
        horizontal
        render={renderInnerStepperTimeline}
        timestampExtractor={(item: { timestamp: string }) => item.timestamp}
      />
    )}
  </>
)

const renderSimpleTimeline = (item) => (
  <h3 className="color-text-strong text-lg font-bold">{item.displayName}</h3>
)

const nestedTimeline = (
  <div className="mx-lg">
    <Timeline
      data={data}
      render={renderTimeline}
      timestampExtractor={(item: { timestamp: Date }) =>
        item.timestamp.toLocaleString()
      }
    />
  </div>
)

const simpleTimeline = (
  <div className="mx-lg">
    <Timeline data={data} render={renderSimpleTimeline} />
  </div>
)

export default {
  nestedTimeline,
  simpleTimeline,
}
