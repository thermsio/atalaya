import React, { useState } from 'react'

import { DayPicker } from './components/DayPicker'
import { MonthPicker } from './components/MonthPicker'
import { YearPicker } from './components/YearPicker'

interface DatePickerBaseProps {
  /** Accepts an ISO string or the word 'today' */
  disableDatesAfterDate?: 'today' | string
  /** Accepts an ISO string or the word 'today' */
  disableDatesBeforeDate?: 'today' | string
  weekStart?: 'sunday' | 'monday'
  timezone?: string
  onTimeLauncherClick?: () => void
  pickDay?: boolean
  pickMonth?: boolean
  pickYear?: boolean
  timestamp?: string
  timestampEnd?: string
  timestampStart?: string
}

export interface DatePickerSingleProps {
  rangePick?: false
  onChangeValue: (newDate: string) => void
}

export interface DatePickerRangeProps {
  rangePick: true
  onChangeValue: (newDate: { start?: string; end?: string }) => void
}

export type DatePickerProps = DatePickerBaseProps &
  (DatePickerSingleProps | DatePickerRangeProps)

const DatePicker = ({
  disableDatesAfterDate,
  disableDatesBeforeDate,
  onTimeLauncherClick,
  onChangeValue,
  pickDay = true,
  pickMonth = true,
  pickYear = true,
  rangePick = false,
  timestamp,
  timestampEnd,
  timestampStart,
  weekStart = 'monday',
}: DatePickerProps): React.ReactElement => {
  const [activeView, setActiveView] = useState(() => {
    if (pickDay) return 'day'
    if (pickMonth) return 'month'
    if (pickYear) return 'year'
  })

  const [timestampActiveView, setTimestampActiveView] = useState(
    timestamp || timestampStart || timestampEnd || new Date().toISOString(),
  )

  if (activeView === 'month')
    return (
      <MonthPicker
        onBack={() => setActiveView('day')}
        onChangeTimestampActiveView={setTimestampActiveView}
        onYearPickerClick={() => setActiveView('year')}
        timestampActiveView={timestampActiveView}
      />
    )

  if (activeView === 'year')
    return (
      <YearPicker
        onBack={() => setActiveView('month')}
        onChangeTimestampActiveView={setTimestampActiveView}
        timestampActiveView={timestampActiveView}
      />
    )

  return (
    // Using ts-ignore because we are using two possible prop type shapes TS assumes rangePick is going to be a boolean, then it complains when 'true' can't be 'false'
    // @ts-ignore
    <DayPicker
      disableDatesAfterDate={disableDatesAfterDate}
      disableDatesBeforeDate={disableDatesBeforeDate}
      onChangeValue={onChangeValue}
      onMonthPickerClick={() => setActiveView('month')}
      onTimeLauncherClick={
        !!onTimeLauncherClick ? () => onTimeLauncherClick() : undefined
      }
      rangePick={rangePick}
      timestamp={timestamp}
      timestampActiveView={timestampActiveView}
      onChangeTimestampActiveView={setTimestampActiveView}
      timestampEnd={timestampEnd}
      timestampStart={timestampStart}
      weekStart={weekStart}
    />
  )
}

export { DatePicker }
