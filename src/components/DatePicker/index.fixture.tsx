import React, { useState } from 'react'
import { Dropdown } from '../Dropdown'
import { DatePicker } from './index'
import { useSelect } from 'react-cosmos/client'

const base = () => {
  const [value, setValue] = useState<string>()
  const [firstDayOfWeek] = useSelect('First Day of Week', {
    options: ['monday', 'sunday'],
    defaultValue: 'monday',
  })

  return (
    <Dropdown dropdownContainerClass="p-base">
      <DatePicker
        weekStart={firstDayOfWeek}
        timestamp={value}
        onChangeValue={setValue}
      />
    </Dropdown>
  )
}

export default { base }
