import React, { useCallback, useMemo, useState } from 'react'
import dayjs from 'dayjs'

import { Button } from '../../../Button'
import { Icon } from '../../../Icon'
import { Inline } from '../../../../layout/Inline'
import { CalendarDay } from './components/CalendarDay'
import {
  checkIsAfter,
  checkIsBefore,
  checkIsBetweenDates,
  checkIsSameDay,
  checkIsToday,
  getDateShort,
  getDayOfMonth,
  getMonth,
  getMonthWeekDays,
} from '../../../../utils/date'
import {
  calendarIcon,
  timeIcon,
} from '../../../FormControls/DateTimePicker/components/DateTimeIcons'
import { Tooltip } from '../../../Tooltip'
import { DatePickerRangeProps, DatePickerSingleProps } from '../../index'

interface DayPickerBaseProps {
  /** Accepts an ISO string or the word 'today' */
  disableDatesAfterDate?: 'today' | string
  /** Accepts an ISO string or the word 'today' */
  disableDatesBeforeDate?: 'today' | string
  onChangeTimestampActiveView: (newTimestamp: string) => void
  onMonthPickerClick: () => void
  onTimeLauncherClick?: () => void
  timestamp?: string
  timestampActiveView: string
  timestampEnd?: string
  timestampStart?: string
  weekStart: 'sunday' | 'monday'
}

type DayPickerProps = DayPickerBaseProps &
  (DatePickerSingleProps | DatePickerRangeProps)

const DayPicker = ({
  disableDatesAfterDate,
  disableDatesBeforeDate,
  onChangeTimestampActiveView,
  onChangeValue,
  onMonthPickerClick,
  onTimeLauncherClick,
  rangePick,
  timestamp,
  timestampActiveView,
  timestampEnd,
  timestampStart,
  weekStart,
}: DayPickerProps): React.ReactElement => {
  const [pickTurn, setPickTurn] = useState(() => {
    if (!timestampStart) return 'start'
    if (!timestampEnd) return 'end'
    return 'start'
  })

  const weekDays = useMemo(() => {
    return weekStart === 'sunday'
      ? ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa']
      : ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su']
  }, [weekStart])

  const calendarDays = useMemo(
    () =>
      getMonthWeekDays(timestampActiveView, weekStart, {
        disableDatesAfterDate:
          disableDatesAfterDate === 'today'
            ? new Date().toISOString()
            : disableDatesAfterDate,
        disableDatesBeforeDate:
          disableDatesBeforeDate === 'today'
            ? new Date().toISOString()
            : disableDatesBeforeDate,
      }),
    [
      disableDatesAfterDate,
      disableDatesBeforeDate,
      timestampActiveView,
      weekStart,
    ],
  )

  const formattedMonth = useMemo(
    () => getMonth(timestampActiveView),
    [timestampActiveView],
  )

  const handleDayPick = useCallback(
    (pickedDate) => {
      if (!rangePick) {
        return onChangeValue(pickedDate)
      }

      if (!timestampStart || checkIsBefore(pickedDate, timestampStart)) {
        setPickTurn('end')
        return onChangeValue({
          start: pickedDate,
          end: dayjs(timestampEnd).endOf('day').toISOString(),
        })
      }

      if (!timestampEnd || checkIsAfter(pickedDate, timestampEnd)) {
        setPickTurn('start')
        return onChangeValue({
          start: timestampStart,
          end: dayjs(pickedDate).endOf('day').toISOString(),
        })
      }

      if (pickTurn === 'start') {
        setPickTurn('end')
        return onChangeValue({
          start: pickedDate,
          end: dayjs(timestampEnd).endOf('day').toISOString(),
        })
      } else {
        setPickTurn('start')
        return onChangeValue({
          start: timestampStart,
          end: dayjs(pickedDate).endOf('day').toISOString(),
        })
      }
    },
    [rangePick, pickTurn, timestampStart, timestampEnd],
  )

  return (
    <div>
      <div className="flex justify-between">
        <div className="flex space-x-xs">
          <Tooltip tooltip="Go to today">
            <Button
              subtle
              onClick={() =>
                onChangeTimestampActiveView(new Date().toISOString())
              }
            >
              {calendarIcon}
            </Button>
          </Tooltip>

          <Button variant="main" subtle onClick={onMonthPickerClick}>
            {formattedMonth}
          </Button>
        </div>

        <Inline space="xs">
          <Button
            subtle
            onClick={() =>
              onChangeTimestampActiveView(
                dayjs(timestampActiveView).subtract(1, 'month').toISOString(),
              )
            }
          >
            <Icon>
              <svg
                className="h-6 w-6"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                  clipRule="evenodd"
                />
              </svg>
            </Icon>
          </Button>

          <Button
            subtle
            onClick={() =>
              onChangeTimestampActiveView(
                dayjs(timestampActiveView).add(1, 'month').toISOString(),
              )
            }
          >
            <Icon>
              <svg
                className="h-6 w-6"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                  clipRule="evenodd"
                />
              </svg>
            </Icon>
          </Button>
        </Inline>
      </div>

      <div className="mb-sm mt-xs border-t border-border" />

      <div className="grid min-w-max grid-cols-7">
        {weekDays.map((day) => (
          <div
            className="mb-xxs text-center font-medium text-color-subtle"
            key={day}
          >
            {day}
          </div>
        ))}

        {calendarDays.map(({ day, enabled }, index) => {
          const calendarDay = getDayOfMonth(day, timestampActiveView)
          let isToday = false
          let isSelected = false
          let isInSelectedRange = false

          if (enabled) {
            isToday = checkIsToday(calendarDay)
            isSelected = rangePick
              ? checkIsSameDay(calendarDay, timestampStart) ||
                checkIsSameDay(calendarDay, timestampEnd)
              : checkIsSameDay(calendarDay, timestamp)
            isInSelectedRange =
              !!rangePick &&
              checkIsBetweenDates(calendarDay, timestampStart, timestampEnd)
          }

          return (
            <CalendarDay
              enabled={enabled}
              isToday={isToday}
              isInSelectedRange={isInSelectedRange}
              isSelected={isSelected}
              onClick={() => handleDayPick(calendarDay)}
              key={`${day}/${index}`}
            >
              {day.toString()}
            </CalendarDay>
          )
        })}
      </div>

      {rangePick && (
        <>
          <div className="my-sm border-t border-border" />

          <div
            className="grid items-center gap-sm px-xxs text-sm text-color-subtle"
            style={{ gridTemplateColumns: '1fr max-content 1fr' }}
          >
            <Button
              disabled={!timestampStart}
              subtle
              onClick={
                timestampStart
                  ? () => onChangeTimestampActiveView(timestampStart)
                  : undefined
              }
            >
              {timestampStart ? getDateShort(timestampStart) : 'From Date'}
            </Button>

            <Icon color="neutral">
              <svg
                className="h-6 w-6"
                fill="currentColor"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M0 0h24v24H0V0z" fill="none" />
                <path d="M12 8V6.41c0-.89 1.08-1.34 1.71-.71l5.59 5.59c.39.39.39 1.02 0 1.41l-5.59 5.59c-.63.63-1.71.19-1.71-.7V16H5c-.55 0-1-.45-1-1V9c0-.55.45-1 1-1h7z" />
              </svg>
            </Icon>
            <Button
              disabled={!timestampEnd}
              onClick={
                timestampEnd
                  ? () => onChangeTimestampActiveView(timestampEnd)
                  : undefined
              }
              subtle
            >
              {timestampEnd ? getDateShort(timestampEnd) : 'To Date'}
            </Button>
          </div>
        </>
      )}

      {!!onTimeLauncherClick && (
        <div className="mt-sm rounded bg-surface">
          <Button fullWidth onClick={onTimeLauncherClick} subtle>
            {timeIcon}
          </Button>
        </div>
      )}
    </div>
  )
}

export { DayPicker }
