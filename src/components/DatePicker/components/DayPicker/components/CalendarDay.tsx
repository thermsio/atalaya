import React, { MouseEventHandler } from 'react'
import { Button } from '../../../../Button'
import classNames from 'classnames'

interface CalendarDayProps {
  children: string
  isInSelectedRange?: boolean
  isSelected: boolean
  isToday: boolean
  enabled?: boolean
  onClick: MouseEventHandler<HTMLButtonElement>
}

const CalendarDay = ({
  children,
  isInSelectedRange,
  isSelected,
  isToday,
  enabled,
  onClick,
}: CalendarDayProps): React.ReactElement => {
  return (
    <div
      className={classNames({
        'bg-main-faded': isInSelectedRange,
        'bg-main': isInSelectedRange,
      })}
    >
      <Button
        borderRadius="none"
        disabled={!enabled}
        onClick={onClick}
        subtle={!isToday && !isSelected}
        outline={isToday && !isSelected}
        variant={enabled ? 'main' : 'neutral'}
        className={classNames({ 'bg-main-dark': isSelected }, 'min-w-[2.5rem]')}
        fullWidth
      >
        {children}
      </Button>
    </div>
  )
}

export { CalendarDay }
