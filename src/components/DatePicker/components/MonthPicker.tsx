import React, { useMemo } from 'react'
import dayjs from 'dayjs'

import { Button } from '../../Button'
import { Icon } from '../../Icon'
import { Inline } from '../../../layout/Inline'
import { Stack } from '../../../layout/Stack'

const monthNames = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
]

interface MonthPickerProps {
  onBack: () => void
  onChangeTimestampActiveView: (newTimestamp: string) => void
  onYearPickerClick: () => void
  timestampActiveView: string
}

const MonthPicker = ({
  onBack,
  onChangeTimestampActiveView,
  onYearPickerClick,
  timestampActiveView,
}: MonthPickerProps): React.ReactElement => {
  const month = useMemo(
    () => dayjs(timestampActiveView).month(),
    [timestampActiveView],
  )
  const year = useMemo(
    () => dayjs(timestampActiveView).year(),
    [timestampActiveView],
  )

  return (
    <Stack space="sm">
      <Inline alignX="between">
        <Inline space="xs">
          <Button subtle onClick={onBack}>
            <Icon>
              <svg
                className="h-6 w-6"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M9.707 16.707a1 1 0 01-1.414 0l-6-6a1 1 0 010-1.414l6-6a1 1 0 011.414 1.414L5.414 9H17a1 1 0 110 2H5.414l4.293 4.293a1 1 0 010 1.414z"
                  clipRule="evenodd"
                />
              </svg>
            </Icon>
          </Button>

          <Button subtle variant="main" onClick={onYearPickerClick}>
            {year}
          </Button>
        </Inline>

        <Inline space="xs">
          <Button
            subtle
            onClick={() =>
              onChangeTimestampActiveView(
                dayjs(timestampActiveView).subtract(1, 'year').toISOString(),
              )
            }
          >
            <Icon>
              <svg
                className="h-6 w-6"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                  clipRule="evenodd"
                />
              </svg>
            </Icon>
          </Button>

          <Button
            subtle
            onClick={() =>
              onChangeTimestampActiveView(
                dayjs(timestampActiveView).add(1, 'year').toISOString(),
              )
            }
          >
            <Icon>
              <svg
                className="h-6 w-6"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                  clipRule="evenodd"
                />
              </svg>
            </Icon>
          </Button>
        </Inline>
      </Inline>

      <div className="my-base border-t border-border" />

      <div className="grid min-w-max grid-cols-3 gap-xs">
        {monthNames.map((monthName, index) => (
          <Button
            className="justify-start"
            subtle
            variant={index === month ? 'main' : 'neutral'}
            onClick={() => {
              onChangeTimestampActiveView(
                dayjs(timestampActiveView).month(index).toISOString(),
              )
              onBack()
            }}
            key={monthName}
          >
            {monthName}
          </Button>
        ))}
      </div>
    </Stack>
  )
}

export { MonthPicker }
