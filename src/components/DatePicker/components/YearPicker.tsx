import React, { useCallback, useMemo, useState } from 'react'
import dayjs from 'dayjs'

import { Button } from '../../Button'
import { Icon } from '../../Icon'
import { Inline } from '../../../layout/Inline'
import { Stack } from '../../../layout/Stack'

interface YearPickerProps {
  onBack: () => void
  onChangeTimestampActiveView: (newTimestamp: string) => void
  timestampActiveView: string
}

const YearPicker = ({
  onBack,
  onChangeTimestampActiveView,
  timestampActiveView,
}: YearPickerProps): React.ReactElement => {
  const [page, setPage] = useState(0)

  const year = useMemo(
    () => dayjs(timestampActiveView).year(),
    [timestampActiveView],
  )

  const handleYearPick = useCallback(
    (year) => {
      onChangeTimestampActiveView(
        dayjs(timestampActiveView).year(year).toISOString(),
      )
      setPage(0)
      onBack()
    },
    [timestampActiveView],
  )

  const YearList = useMemo(() => {
    const list: React.ReactNode[] = []

    if (page === 0) {
      for (let i = -9; i <= 6; i++) {
        const _year = year + i
        list.push(
          <Button
            subtle
            variant={i === 0 ? 'main' : 'neutral'}
            onClick={() => handleYearPick(_year)}
            key={_year}
          >
            {_year}
          </Button>,
        )
      }
    } else {
      for (let i = 0; i < 16; i++) {
        const _year = year - 9 + 16 * page + i
        list.push(
          <Button subtle onClick={() => handleYearPick(_year)} key={_year}>
            {_year}
          </Button>,
        )
      }
    }

    return list
  }, [year, page])

  return (
    <Stack space="sm">
      <Inline alignX="between" space="xs">
        <Button subtle onClick={onBack}>
          <Icon>
            <svg
              className="h-6 w-6"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M9.707 16.707a1 1 0 01-1.414 0l-6-6a1 1 0 010-1.414l6-6a1 1 0 011.414 1.414L5.414 9H17a1 1 0 110 2H5.414l4.293 4.293a1 1 0 010 1.414z"
                clipRule="evenodd"
              />
            </svg>
          </Icon>
        </Button>

        <Inline space="xs">
          <Button subtle onClick={() => setPage(page - 1)}>
            <Icon>
              <svg
                className="h-6 w-6"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                  clipRule="evenodd"
                />
              </svg>
            </Icon>
          </Button>

          <Button subtle onClick={() => setPage(page + 1)}>
            <Icon>
              <svg
                className="h-6 w-6"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                  clipRule="evenodd"
                />
              </svg>
            </Icon>
          </Button>
        </Inline>
      </Inline>

      <div className="my-base border-t border-border" />

      <div className="grid min-w-max grid-cols-4 gap-xs">{YearList}</div>
    </Stack>
  )
}

export { YearPicker }
