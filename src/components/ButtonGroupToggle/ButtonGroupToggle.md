# ButtonGroupToggle

`ButtonGroupToggle` is a component that allows you to toggle between different buttons.

```jsx
import { useState } from 'react'
import { ButtonGroupToggle } from './index'

const [activeKey, setActiveKey] = useState('1')

;<div>
  <div className="flex">
    <ButtonGroupToggle
      activeKey={activeKey}
      buttons={[
        {
          key: '1',
          label: 'All',
        },
        {
          key: '2',
          label: 'Active',
        },
        {
          key: '3',
          label: 'Inactive',
        },
      ]}
      onKeyPress={(key) => setActiveKey(key)}
      variant="main"
    />
  </div>
</div>
```
