import React, { ReactNode } from 'react'
import { ButtonClassProps } from '../Button/utils'
import { Button } from '../Button'
import classnames from 'classnames'

export interface ButtonGroupToggleProps<KeyType> {
  activeKey: KeyType
  activeVariant?: ButtonClassProps['variant']
  backgroundColor?: string
  buttons: {
    key: KeyType
    label: ReactNode
  }[]
  disabled?: boolean
  inactiveVariant?: ButtonClassProps['variant']
  onKeyPress?: (key: KeyType) => void
  variant?: ButtonClassProps['variant']
}

export function ButtonGroupToggle<KeyType>({
  activeKey,
  activeVariant,
  backgroundColor,
  buttons,
  disabled,
  inactiveVariant,
  onKeyPress,
  variant,
}: ButtonGroupToggleProps<KeyType>) {
  return (
    <div
      className={classnames(
        { 'bg-input-background': !backgroundColor },
        `${backgroundColor} p-1 space-x-xxs rounded flex w-min shadow-inner`,
      )}
    >
      {buttons.map((button, i) => (
        <Button
          // match the height of other form inputs
          className="p-0 px-xs py-0.5"
          disabled={disabled}
          key={button.label?.toString() ?? i}
          onClick={() => onKeyPress?.(button.key)}
          subtle={activeKey !== button.key}
          variant={
            activeKey === button.key
              ? activeVariant || variant
              : inactiveVariant || undefined
          }
        >
          {button.label}
        </Button>
      ))}
    </div>
  )
}
