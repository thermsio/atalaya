import React, { useState } from 'react'
import { ButtonGroupToggle } from './index'

const nestedButton = () => {
  const [activeKey, setActiveKey] = useState('1')

  return (
    <div className="flex">
      <ButtonGroupToggle
        activeKey={activeKey}
        backgroundColor="bg-white"
        buttons={[
          {
            key: '1',
            label: 'All',
          },
          {
            key: '2',
            label: 'Active',
          },
          {
            key: '3',
            label: 'Inactive',
          },
        ]}
        onKeyPress={(key) => setActiveKey(key)}
        variant="main"
      />
    </div>
  )
}

export default { nestedButton }
