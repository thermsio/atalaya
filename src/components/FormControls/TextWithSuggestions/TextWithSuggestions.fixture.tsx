import React, { useCallback, useRef, useState } from 'react'
import { TextWithSuggestions } from './TextWithSuggestions'

const data: { label: string; value: string; name: string }[] = new Array(40)
  .fill(null)
  .map((_, i) => ({
    label: 'Test ' + i,
    value: 'Test ' + i,
    name: 'name ' + i,
  }))

const basic = (): React.ReactElement => {
  const limit = 4
  const [suggestions, setSuggestions] = useState<typeof data>([])
  const [isLoading, setIsLoading] = useState(false)
  const timeoutRef = useRef<unknown>(null)

  const handleLoadOptions = useCallback(
    (text: string) => {
      setIsLoading(true)

      if (timeoutRef.current) clearTimeout(Number(timeoutRef.current))

      return new Promise<typeof data>((resolve) => {
        timeoutRef.current = setTimeout(() => {
          let newOptions: typeof data = []

          newOptions = data
            .filter((suggestion) => {
              return suggestion.label.toLowerCase().includes(text.toLowerCase())
            })
            .slice(0, limit)

          setSuggestions(newOptions)
          setIsLoading(false)
          resolve(newOptions)
        }, 1000)
      })
    },
    [suggestions],
  )

  return (
    <div className="mx-lg">
      <p className="mb-xxs ml-xxs">Input with text suggestions</p>
      <TextWithSuggestions
        isLoading={isLoading}
        loadSuggestions={handleLoadOptions}
        suggestions={suggestions}
        onSuggestionSelected={(suggestion) => {
          // TS autoComplete should work for ambient object props, here
          suggestion.name

          return suggestion.value
        }}
      />
    </div>
  )
}

export default {
  basic,
}
