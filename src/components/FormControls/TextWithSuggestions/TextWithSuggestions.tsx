import React, { useEffect, useRef, useState } from 'react'
import classNameParser from 'classnames'
import { useKeyPress } from '../../../hooks/useKeyPress'
import { Inline } from '../../../layout/Inline'
import { Loading } from '../../Loading'
import { TextProps } from '../Text/Text'
import { Stack } from '../../../layout/Stack'
import { StatusIcons } from '../components/StatusIcons'
import { FormControlWrapper } from '../../FormLayout/components/FormControlWrapper'

export interface Suggestion {
  label?: React.ReactNode
  value?: string
}

export type TextWithSuggestionsProps<
  Suggestions extends Array<unknown> = Suggestion[],
> = TextProps & {
  /** Will show loading indicators */
  isLoading?: boolean
  /** Callback fired to find suggestions when the value of the input changes. */
  loadSuggestions?: (text: string) => Promise<Suggestions>
  /** Resolves each suggestion to a string to be displayed as the label by components. */
  suggestionLabelExtractor?: (
    suggestion: Suggestions[number],
  ) => React.ReactNode
  /** Resolves each suggestion to a string that will replace the input text when selected. */
  suggestionValueExtractor?: (option: Suggestions[number]) => string
  /** Callback fired when a suggestion is selected from the list. */
  onSuggestionSelected?: (suggestion: Suggestions[number]) => string
  /** Component used to render each suggestion. */
  renderSuggestion?: (suggestion: Suggestions[number]) => React.ReactNode
  /** The list of suggestions to be displayed. */
  suggestions?: Suggestions
}

function TextWithSuggestions<Suggestions extends Array<Suggestion & object>>({
  autoComplete,
  error,
  disabled,
  hint,
  isLoading,
  label,
  labelBehavior,
  loadSuggestions,
  maxWidth,
  minWidth,
  name,
  onChange,
  onBlur,
  onChangeValue,
  onFocus,
  onSuggestionSelected,
  suggestionLabelExtractor = (suggestion) => suggestion.label,
  suggestionValueExtractor = (suggestion) => suggestion.value || '',
  placeholder,
  renderSuggestion,
  search,
  showErrorMessage,
  subText,
  suggestions,
  valid,
  value,
}: TextWithSuggestionsProps<Suggestions>) {
  const inputTextRef = useRef<HTMLInputElement>(null)
  const [showSuggestions, setShowSuggestions] = useState<boolean>(false)
  const [cursor, setCursor] = useState<number>(-1)
  const [hovered, setHovered] = useState<Suggestion | undefined>(undefined)
  const downPress = useKeyPress('ArrowDown', inputTextRef)
  const enterPress = useKeyPress('Enter', inputTextRef)
  const upPress = useKeyPress('ArrowUp', inputTextRef)

  useEffect(() => {
    if (suggestions && suggestions?.length > 0 && downPress) {
      if (!showSuggestions) {
        setShowSuggestions(true)
      }
      setCursor((prevState) =>
        prevState < suggestions?.length - 1 ? prevState + 1 : prevState,
      )
    }
  }, [downPress])

  useEffect(() => {
    if (suggestions && suggestions?.length > 0 && upPress) {
      if (!showSuggestions) {
        setShowSuggestions(true)
      }
      setCursor((prevState) => (prevState >= 0 ? prevState - 1 : prevState))
    }
  }, [upPress])

  useEffect(() => {
    if (suggestions && suggestions?.length > 0 && enterPress && cursor >= 0) {
      handleSelection(suggestions[cursor])
    }
  }, [cursor, enterPress])

  useEffect(() => {
    if (suggestions && suggestions?.length > 0 && hovered) {
      setCursor(suggestions?.indexOf(hovered))
    }
  }, [hovered])

  const handleSelection = (suggestion: Suggestion) => {
    let newValue: string
    if (onSuggestionSelected) {
      newValue = onSuggestionSelected(suggestion)
    } else {
      newValue = suggestionValueExtractor(suggestion)
    }
    if (inputTextRef.current) inputTextRef.current.value = newValue
    setShowSuggestions(false)
  }

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newValue = e.target.value

    if (newValue.length > 0) {
      if (loadSuggestions) {
        loadSuggestions(newValue).then(() => setShowSuggestions(true))
      } else if (suggestions) {
        setShowSuggestions(true)
      }
    } else {
      setShowSuggestions(false)
    }

    if (onChange) onChange(e)
    if (onChangeValue) onChangeValue(newValue)
  }

  const handleBlur = (e) => {
    setShowSuggestions(false)
    if (onBlur) onBlur(e)
  }

  const handleFocus = (e) => {
    if (
      suggestions &&
      suggestions?.length > 0 &&
      inputTextRef.current &&
      inputTextRef.current.value.length > 0
    ) {
      setShowSuggestions(true)
    }
    if (onFocus) onFocus(e)
  }

  const handleClick = () => {
    if (
      suggestions &&
      suggestions?.length > 0 &&
      inputTextRef.current &&
      inputTextRef.current.value.length > 0
    ) {
      setShowSuggestions(true)
    }
  }

  return (
    <FormControlWrapper
      error={error}
      disabled={disabled}
      hasValue={!!value}
      hint={hint}
      inputName={name}
      label={label}
      labelBehavior={labelBehavior}
      maxWidth={maxWidth}
      minWidth={minWidth}
      showErrorMessage={showErrorMessage}
      subText={subText}
    >
      <div
        className={classNameParser(
          {
            'form-field-disabled': disabled,
            'form-field-error': error,
            'form-field-success': valid,
          },
          'form-field-input-wrapper relative flex items-center justify-between',
        )}
      >
        <input
          autoComplete={
            !!autoComplete && autoComplete !== 'off'
              ? autoComplete
              : 'autoComplete-off'
          }
          className="form-field"
          name={name}
          id={name}
          disabled={disabled}
          onBlur={handleBlur}
          onChange={handleInputChange}
          onFocus={handleFocus}
          placeholder={placeholder}
          type={search ? 'search' : 'text'}
          value={value}
          onClick={handleClick}
        />

        {showSuggestions && (
          <Stack className="absolute inset-x-0 top-full z-dropdown mt-xs w-full overflow-hidden rounded bg-surface">
            {suggestions?.map((suggestion, index) => (
              <Inline
                alignX="between"
                alignY="center"
                className={classNameParser(
                  {
                    'bg-surface-strong': index === cursor,
                  },
                  'p-sm py-xs',
                )}
                key={`selected-suggestion-${suggestionValueExtractor(
                  suggestion,
                )}`}
                onClick={() => handleSelection(suggestion)}
                onMouseDown={(e) => e.preventDefault()}
                onMouseEnter={() => setHovered(suggestion)}
                onMouseLeave={() => {
                  setHovered(undefined)
                  setCursor(-1)
                }}
                width="full"
              >
                {renderSuggestion ? (
                  renderSuggestion(suggestion)
                ) : (
                  <span>{suggestionLabelExtractor(suggestion)}</span>
                )}
              </Inline>
            ))}
          </Stack>
        )}

        {(!!error || !!valid) && (
          <div className="py-xs pr-sm">
            <StatusIcons error={error} valid={valid} />
          </div>
        )}

        {isLoading && <Loading className="mr-sm" size="sm" />}
      </div>
    </FormControlWrapper>
  )
}

export { TextWithSuggestions }
