In order to use select you need to provide an array of objects to the `suggestions` prop. The shape of the suggestion is up to you.
Default shape:

- `label`: The string that will be used to represent the suggestion inside the component.
- `value`: The actual string value of the suggestion which will replace the input text if selected.

While you are not required to follow this shape, remember to provide the following extra props when not using the default shape:

- `suggestionLabelExtractor`: This function will be used to extract the string to be used as the label of each suggestion
- `suggestionValueExtractor`. This function will be used to extract the string to be used as the value of each suggestion

Note that these returned values will only be used internally by the component, and anytime a callback provides you
with a suggestion option, the option will be in its original shape.

```jsx
const suggestions = [
  {
    label: "Home",
    value: "610 Ravef View, North Carolina, USA."
  },
  {
    label: "Office",
    value: "1535 Uvawul Center, Manchester, UK."
  }
];


<TextWithSuggestions
  placeholder="Where are you going?"
  suggestions={suggestions}
/>
```
