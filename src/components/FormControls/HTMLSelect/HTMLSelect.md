```jsx
const [value, setValue] = React.useState('');
const options = [
  {label: 'Option 1',
   value: '1'
  },
  {label: 'Option 2',
   value: '2'
  },
  {label: 'Option 3',
   value: '3'
  },
];

<HTMLSelect options={options} placeholder="Select one option please" value={value} onChangeValue={setValue} />
```

### With labels

```jsx
const options = [
  {label: 'Option 1',
   value: '1'
  },
  {label: 'Option 2',
   value: '2'
  },
  {label: 'Option 3',
   value: '3'
  },
];

<HTMLSelect
  label="Select Field"
  hint="Required"
  options={options}
  placeholder="Select one option please"
  subText="Only latin characters allowed."
/>
```

### With Error

```jsx
const options = [
  {label: 'Option 1',
   value: '1'
  },
  {label: 'Option 2',
   value: '2'
  },
  {label: 'Option 3',
   value: '3'
  },
];

<HTMLSelect
  error="There is something very wrong with this input."
  label="Select Field"
  hint="Required"
  options={options}
  placeholder="Select one option please"
/>
```

### Valid

```jsx
const options = [
  {label: 'Option 1',
   value: '1'
  },
  {label: 'Option 2',
   value: '2'
  },
  {label: 'Option 3',
   value: '3'
  },
];

<HTMLSelect
  label="Select Field"
  hint="Required"
  options={options}
  placeholder="Select one option please"
  valid={true}
/>
```

### Disabled

```jsx
const options = [
  {label: 'Option 1',
   value: '1'
  },
  {label: 'Option 2',
   value: '2'
  },
  {label: 'Option 3',
   value: '3'
  },
];

<HTMLSelect
  disabled
  label="Select Field"
  hint="Required"
  options={options}
  placeholder="Select one option please"
/>
```
