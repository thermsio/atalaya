import React, { useState } from 'react'
import { Stack } from '../../../layout/Stack'
import { HTMLSelect } from './HTMLSelect'

// eslint-disable-next-line react/display-name
export default (): React.ReactElement => {
  const [value, setValue] = useState()

  return (
    <Stack space="base">
      <HTMLSelect
        onChangeValue={setValue}
        options={[
          { label: 'Test 1', value: 1 },
          { label: 'Test 2', value: 2 },
        ]}
        value={value}
      />

      <div className="mt-lg">Selected value: {value}</div>
    </Stack>
  )
}
