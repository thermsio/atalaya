import React, { useCallback } from 'react'
import classNameParser from 'classnames'
import {
  FormControlWrapper,
  FormControlWrapperPublicProps,
} from '../../FormLayout/components/FormControlWrapper'

import '../FormControls.css'
import { FormControlCommonProps } from '../types'
import { StatusIcons } from '../components/StatusIcons'

export type HTMLSelectOption = {
  disabled?: boolean
  label?: string
  value?: any
}

export interface HTMLSelectProps
  extends FormControlCommonProps,
    FormControlWrapperPublicProps {
  options: HTMLSelectOption[]
  value?: string | number
}

const HTMLSelect = ({
  error,
  disabled,
  hint,
  label,
  labelBehavior,
  name,
  onChange,
  onBlur,
  onChangeValue,
  onFocus,
  options,
  subText,
  showErrorMessage,
  valid,
  value,
}: HTMLSelectProps): React.ReactElement => {
  const handleOnChange = useCallback(
    (e) => {
      if (onChangeValue) onChangeValue(e.target.value)
      if (onChange) onChange(e)
    },
    [onChange, onChangeValue],
  )

  return (
    <FormControlWrapper
      error={error}
      disabled={disabled}
      hasValue={!!value}
      hint={hint}
      inputName={name}
      label={label}
      labelBehavior={labelBehavior}
      subText={subText}
      showErrorMessage={showErrorMessage}
    >
      <div
        className={classNameParser(
          {
            'form-field-disabled': disabled,
            'form-field-error': error,
            'form-field-success': valid,
          },
          'form-field-input-wrapper flex items-center justify-between',
        )}
      >
        <select
          className="form-field form-field-select"
          name={name}
          id={name}
          disabled={disabled}
          onBlur={onBlur}
          onChange={handleOnChange}
          onFocus={onFocus}
          value={value}
        >
          {options.map(({ disabled, label, value }, index) => {
            let _label
            if (label) {
              _label = label
            } else if (typeof value === 'string' || typeof value === 'number') {
              _label = value
            }

            return (
              <option
                disabled={disabled}
                value={value}
                key={`${_label}/${index}`}
              >
                {_label}
              </option>
            )
          })}
        </select>

        {(!!error || !!valid) && (
          <div className="py-xs pr-sm">
            <StatusIcons error={error} valid={valid} />
          </div>
        )}
      </div>
    </FormControlWrapper>
  )
}

export { HTMLSelect }
