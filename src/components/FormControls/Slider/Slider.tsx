import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { Range, getTrackBackground } from 'react-range'
import classNameParser from 'classnames'

import {
  FormControlWrapper,
  FormControlWrapperPublicProps,
} from '../../FormLayout/components/FormControlWrapper'

import { FormControlCommonProps } from '../types'
import '../FormControls.css'
import './Slider.css'
import { useUpdateEffect } from 'ahooks'

export interface SliderProps
  extends FormControlCommonProps,
    FormControlWrapperPublicProps {
  max?: number
  min?: number
  onBlur?: () => void
  /** The onChangeValue() prop is fired when the user stops sliding on the track, this prop is fired on every step as the user is sliding the thumb */
  onChangeValueImmediate?: (value: number) => void
  onFocus?: () => void
  renderThumb?: (data: { value: number }) => React.ReactElement
  step?: number
  value?: number
}

//TODO: move the slider value that's inside the thumb, above it when it's been dragged.
//People using a touchscreen won't be able to see the new value until they lift their
//finger otherwise.
const Slider = ({
  error,
  disabled,
  hint,
  label,
  labelBehavior,
  max = 100,
  maxWidth,
  min = 0,
  minWidth,
  name,
  onBlur,
  onChange,
  onChangeValue,
  onChangeValueImmediate,
  onFocus,
  renderThumb,
  showErrorMessage,
  subText,
  step = 1,
  value = 50,
}: SliderProps): React.ReactElement => {
  const [sliderValues, setSliderValues] = useState([value])

  useEffect(() => {
    if (onFocus) onFocus()
    setSliderValues([value])
    if (onBlur) onBlur()
  }, [value])

  const handleOnChange = useCallback(
    (sliderValues) => {
      if (onChangeValue) onChangeValue(sliderValues[0])
      if (onChange) onChange(sliderValues[0])
    },
    [onChange, onChangeValue],
  )

  useUpdateEffect(() => {
    onChangeValueImmediate?.(sliderValues[0])
  }, [sliderValues])

  const trackStyle = useMemo(() => {
    if (disabled) return { background: 'var(--color-neutral-faded)' }

    return {
      background: getTrackBackground({
        values: sliderValues,
        colors: ['var(--color-main)', 'var(--color-neutral)'],
        min,
        max,
      }),
    }
  }, [disabled, max, min, sliderValues])

  return (
    <FormControlWrapper
      error={error}
      disabled={disabled}
      hasValue={!!value}
      hint={hint}
      inputName={name}
      label={label}
      labelBehavior={labelBehavior}
      maxWidth={maxWidth}
      minWidth={minWidth}
      showErrorMessage={showErrorMessage}
      subText={subText}
    >
      <Range
        disabled={disabled}
        max={max}
        min={min}
        onChange={setSliderValues}
        onFinalChange={handleOnChange}
        step={step}
        values={sliderValues}
        renderTrack={({ props, children }) => (
          <div
            onMouseDown={props.onMouseDown}
            onTouchStart={props.onTouchStart}
            className={classNameParser(
              { 'form-slider-wrapper-disabled': disabled },
              // "relative" position is required for the thumb to stay relative to the track
              'form-slider-wrapper relative',
            )}
          >
            <div
              ref={props.ref}
              className={classNameParser(
                { 'form-slider-track-disabled': disabled },
                'form-field form-slider-track',
              )}
              style={trackStyle}
            >
              {children}
            </div>
          </div>
        )}
        renderThumb={({ props }) => (
          <div
            {...props}
            className={classNameParser(
              { 'form-slider-thumb-disabled': disabled },
              'form-slider-thumb',
            )}
          >
            {renderThumb
              ? renderThumb({ value: sliderValues[0] })
              : sliderValues[0]}
          </div>
        )}
      />
    </FormControlWrapper>
  )
}

export { Slider }
