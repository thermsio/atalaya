```jsx
const [value, setValue] = React.useState(50);

<Slider name="SliderA" value={value} onChangeValue={setValue} />
```

### With labels

```jsx
<Slider
  label="Slider Field"
  hint="Slide"
  name="SliderB"
  subText="Sliders are not meant to provide an exact value."
/>
```

### With Custom Thumb Element

```jsx
<Slider
  label="Slider Field"
  hint="Slide"
  name="SliderB"
  renderThumb={({ value }) => <span>My Thumb "{value}"</span>}
/>
```

### With Error

```jsx
<Slider
  error="While this might surprise you, your slider has an error."
  hint="Slide"
  label="Slider Field"
  name="SliderC"
/>
```

### Disabled

```jsx
<Slider
  disabled
  hint="Slide"
  label="Slider Field"
  name="SliderD"
  subText="Sliders are not meant to provide an exact value."
/>
```
