```jsx
<Text
  placeholder="Lorem Ipsum"

/>
```

### With labels
```jsx
<Text
  label="Text Field"
  hint="Required"
  placeholder="Lorem Ipsum"
  subText="Only latin characters allowed."

/>
```

### With Error

```jsx
<Text
  error="There is something very wrong with this input."
  label="Text Field"
  hint="Required"
  placeholder="Lorem Ipsum"
/>
```

### Valid

```jsx
<Text
  label="Text Field"
  hint="Required"
  placeholder="Lorem Ipsum"
  valid={true}
/>
```

### Disabled

```jsx
const [value, setValue] = React.useState('Input is disabled');

<Text
  disabled
  label="Text Field"
  hint="Required"
  placeholder="Lorem Ipsum"
  value={value}
/>
```
