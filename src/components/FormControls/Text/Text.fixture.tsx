import React, { useRef } from 'react'
import { Text } from './Text'
import { Button } from '../../Button'
import { FormControlLabelBehaviorContext } from '../../FormLayout/context'

const focus = () => {
  const textRef = useRef<HTMLInputElement>(null)

  return (
    <div>
      <Button
        onClick={() => {
          console.log(textRef?.current)
          textRef?.current?.focus()
        }}
      >
        Focus Text
      </Button>
      <Text value="This is fixed text" ref={textRef} />
    </div>
  )
}

const backgroundContrast = (
  <FormControlLabelBehaviorContext.Provider
    value={{ labelBehavior: 'floating' }}
  >
    <div className="p-lg space-y-base">
      <Text
        label="Text Field"
        hint="Required"
        placeholder="Lorem Ipsum"
        subText="Only latin characters allowed."
      />

      <Text
        label="Text Field"
        hint="Required"
        value="Some Actual Value"
        subText="Only latin characters allowed."
      />

      <Text
        disabled
        value="Disabled Value"
        label="Text Field"
        hint="Required"
        placeholder="Lorem Ipsum"
        subText="Only latin characters allowed."
      />
    </div>

    <div className="bg-surface-subtle space-y-base p-lg">
      <Text
        label="Text Field"
        hint="Required"
        placeholder="Lorem Ipsum"
        subText="Only latin characters allowed."
      />

      <Text
        label="Text Field"
        hint="Required"
        value="Some Actual Value"
        subText="Only latin characters allowed."
      />

      <Text
        disabled
        value="Disabled Value"
        label="Text Field"
        hint="Required"
        placeholder="Lorem Ipsum"
        subText="Only latin characters allowed."
      />
    </div>

    <div className="bg-surface p-lg space-y-base">
      <Text
        label="Text Field"
        hint="Required"
        placeholder="Lorem Ipsum"
        subText="Only latin characters allowed."
      />

      <Text
        label="Text Field"
        hint="Required"
        value="Some Actual Value"
        subText="Only latin characters allowed."
      />

      <Text
        disabled
        value="Disabled Value"
        label="Text Field"
        hint="Required"
        placeholder="Lorem Ipsum"
        subText="Only latin characters allowed."
      />
    </div>

    <div className="bg-surface-strong p-lg space-y-base">
      <Text
        label="Text Field"
        hint="Required"
        placeholder="Lorem Ipsum"
        subText="Only latin characters allowed."
      />

      <Text
        label="Text Field"
        hint="Required"
        value="Some Actual Value"
        subText="Only latin characters allowed."
      />

      <Text
        disabled
        value="Disabled Value"
        label="Text Field"
        hint="Required"
        placeholder="Lorem Ipsum"
        subText="Only latin characters allowed."
      />
    </div>
  </FormControlLabelBehaviorContext.Provider>
)

export default {
  focus,
  backgroundContrast,
}
