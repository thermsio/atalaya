import React, { forwardRef, useCallback } from 'react'
import classNameParser from 'classnames'

import {
  FormControlWrapper,
  FormControlWrapperPublicProps,
} from '../../FormLayout/components/FormControlWrapper'

import '../FormControls.css'
import { FormControlCommonProps } from '../types'
import { StatusIcons } from '../components/StatusIcons'

export interface TextProps
  extends FormControlCommonProps,
    FormControlWrapperPublicProps {
  /* If true will set type="search" on input element */
  search?: boolean
  value?: string
}

const Text = forwardRef<HTMLInputElement, TextProps>(
  (
    {
      autoComplete,
      error,
      disabled,
      hint,
      label,
      labelBehavior,
      maxWidth,
      minWidth,
      name,
      onChange,
      onBlur,
      onChangeValue,
      onFocus,
      placeholder,
      search,
      showErrorMessage,
      subText,
      valid,
      value,
    },
    ref,
  ): React.ReactElement => {
    const handleOnChange = useCallback(
      (e) => {
        if (onChangeValue) onChangeValue(e.target.value)
        if (onChange) onChange(e)
      },
      [onChange, onChangeValue],
    )

    return (
      <FormControlWrapper
        error={error}
        disabled={disabled}
        hasValue={!!value}
        hint={hint}
        inputName={name}
        label={label}
        labelBehavior={labelBehavior}
        maxWidth={maxWidth}
        minWidth={minWidth}
        showErrorMessage={showErrorMessage}
        subText={subText}
      >
        <div
          className={classNameParser(
            {
              'form-field-disabled': disabled,
              'form-field-error': error,
              'form-field-success': valid,
            },
            'form-field-input-wrapper flex items-center justify-between',
          )}
        >
          <input
            autoComplete={
              !!autoComplete && autoComplete !== 'off'
                ? autoComplete
                : 'autoComplete-off'
            }
            className="form-field"
            name={name}
            id={name}
            disabled={disabled}
            onBlur={onBlur}
            onChange={handleOnChange}
            onFocus={onFocus}
            placeholder={placeholder}
            ref={ref}
            type={search ? 'search' : 'text'}
            value={value}
          />

          {(!!error || !!valid) && (
            <div className="py-xs pr-sm">
              <StatusIcons error={error} valid={valid} />
            </div>
          )}
        </div>
      </FormControlWrapper>
    )
  },
)

Text.displayName = 'Text'

export { Text }
