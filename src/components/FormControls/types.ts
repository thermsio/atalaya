import React from 'react'
import { ReactNode } from 'react'

export interface FormControlCommonProps {
  autoComplete?: string | undefined
  /** If field is disabled */
  disabled?: boolean
  /** Identifier for the element */
  name?: string
  /** Fired when value changes, receives Event */
  onChange?: (e: React.ChangeEvent | { target: { value: unknown } }) => void
  /** Fired when value changes, receives Value */
  onChangeValue?: (value: any) => void
  /** Fired when element looses focus */
  onBlur?: (e: React.FocusEvent | React.MouseEvent) => void
  /** Fired when element gains focus */
  onFocus?: (e: React.FocusEvent | React.MouseEvent) => void
  /** Text element will display when empty  */
  placeholder?: string
  /** If element has been validated correctly */
  valid?: boolean
}

export interface SelectOption<Value = any> extends Record<any, any> {
  label?: ReactNode
  value?: Value
}
