import React, { useCallback, useMemo } from 'react'
import classNameParser from 'classnames'
import TextareaAutosize from 'react-autosize-textarea'

import { useWindowSize } from '../../../hooks/useWindowSize'

import {
  FormControlWrapper,
  FormControlWrapperPublicProps,
} from '../../FormLayout/components/FormControlWrapper'
import { FormControlCommonProps } from '../types'
import { StatusIcons } from '../components/StatusIcons'

import '../FormControls.css'

export interface TextareaProps
  extends FormControlCommonProps,
    FormControlWrapperPublicProps {
  maxRows?: number
  minRows?: number
  value?: string
}

const Textarea = ({
  error,
  disabled,
  hint,
  label,
  labelBehavior,
  maxRows,
  maxWidth,
  minRows = 2,
  minWidth,
  name,
  onChange,
  onBlur,
  onChangeValue,
  onFocus,
  placeholder,
  showErrorMessage,
  subText,
  valid,
  value,
}: TextareaProps): React.ReactElement => {
  const handleOnChange = useCallback(
    (e) => {
      if (onChangeValue) onChangeValue(e.target.value)
      if (onChange) onChange(e)
    },
    [onChange, onChangeValue],
  )

  const windowSize = useWindowSize()

  const _maxRows = useMemo(() => {
    if (maxRows) return maxRows
    if (windowSize?.width && windowSize.width < 768) return 5
    return 10
  }, [maxRows, windowSize.width])

  return (
    <FormControlWrapper
      error={error}
      disabled={disabled}
      hasValue={!!value}
      hint={hint}
      inputName={name}
      label={label}
      labelBehavior={labelBehavior}
      maxWidth={maxWidth}
      minWidth={minWidth}
      showErrorMessage={showErrorMessage}
      subText={subText}
    >
      <div
        className={classNameParser(
          {
            'form-field-disabled': disabled,
            'form-field-error': error,
            'form-field-success': valid,
          },
          'form-field-input-wrapper relative flex flex-col items-end',
        )}
      >
        <TextareaAutosize
          className="form-field"
          id={name}
          disabled={disabled}
          maxLength={20000}
          maxRows={_maxRows}
          name={name}
          onBlur={onBlur}
          onChange={handleOnChange}
          onFocus={onFocus}
          placeholder={placeholder}
          rows={minRows}
          value={value}
          // stupid ts error for "missing props", it's the 3rd part lib's fault for not making these optional:
          onPointerEnterCapture={undefined}
          onPointerLeaveCapture={undefined}
        />

        {(!!error || !!valid) && (
          <div className="absolute bottom-1 right-1 bg-input-background opacity-90">
            <StatusIcons error={error} valid={valid} />
          </div>
        )}
      </div>
    </FormControlWrapper>
  )
}

export { Textarea }
