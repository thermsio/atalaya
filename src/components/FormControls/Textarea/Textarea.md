Textarea auto resizes as needed to fit the content. Each time the text gets too large
for the field it will extend one extra row until it reaches the max allowed amount of rows.

```jsx
<Textarea placeholder="Write your message here..." />
```

### With labels

```jsx
<Textarea
  label="Textarea"
  hint="Required"
  placeholder="Write your message here..."
  subText="Only latin characters allowed."
/>
```

### With Error

```jsx
<Textarea
  error="There is something very wrong with this input."
  label="Textarea"
  hint="Required"
  placeholder="Write your message here..."
/>
```

### Valid

```jsx
<Textarea
  label="Textarea"
  hint="Required"
  placeholder="Write your message here..."
  valid={true}
/>
```

### Disabled

```jsx
<Textarea
  disabled
  label="Textarea"
  hint="Required"
  placeholder="Write your message here..."
/>
```
