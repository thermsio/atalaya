
```jsx
<Phone placeholder="(555) 555-5555" />
```

### With labels
```jsx
<Phone
  label="Phone Field"
  hint="Required"
  placeholder="(555) 555-5555"
  subText="Only numbers allowed."
/>
```

### With Error

```jsx
<Phone
  error="There is something very wrong with this input."
  label="Phone Field"
  hint="Required"
  placeholder="(555) 555-5555"
/>
```

### Valid

```jsx
<Phone
  label="Phone Field"
  hint="Required"
  placeholder="(555) 555-5555"
  valid={true}
/>
```

### Disabled

```jsx
<Phone
  disabled
  label="Phone Field"
  hint="Required"
  placeholder="(555) 555-5555"
/>
```
