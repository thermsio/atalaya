import React, { useCallback } from 'react'
import classNameParser from 'classnames'
import InputMask from 'react-input-mask'

import {
  FormControlWrapper,
  FormControlWrapperPublicProps,
} from '../../FormLayout/components/FormControlWrapper'

import { FormControlCommonProps } from '../types'
import { StatusIcons } from '../components/StatusIcons'

import '../FormControls.css'

export interface PhoneProps
  extends FormControlCommonProps,
    FormControlWrapperPublicProps {
  value?: string
}

const Phone = ({
  error,
  disabled,
  hint,
  label,
  labelBehavior,
  maxWidth,
  minWidth,
  name,
  onChange,
  onBlur,
  onChangeValue,
  onFocus,
  placeholder,
  showErrorMessage,
  subText,
  valid,
  value,
}: PhoneProps): React.ReactElement => {
  const handleOnChange = useCallback(
    (e) => {
      if (onChangeValue) onChangeValue(e.target.value)
      if (onChange) onChange(e)
    },
    [onChange, onChangeValue],
  )

  return (
    <FormControlWrapper
      error={error}
      disabled={disabled}
      hasValue={!!value}
      hint={hint}
      inputName={name}
      label={label}
      labelBehavior={labelBehavior}
      maxWidth={maxWidth}
      minWidth={minWidth}
      showErrorMessage={showErrorMessage}
      subText={subText}
    >
      <div
        className={classNameParser(
          {
            'form-field-disabled': disabled,
            'form-field-error': error,
            'form-field-success': valid,
          },
          'form-field-input-wrapper flex items-center justify-between',
        )}
      >
        <InputMask
          mask="(999) 999-9999"
          maskChar={null}
          className="form-field"
          type="tel"
          name={name}
          id={name}
          disabled={disabled}
          onBlur={onBlur}
          onChange={handleOnChange}
          onFocus={onFocus}
          placeholder={placeholder}
          value={value}
        />

        {(!!error || !!valid) && (
          <div className="py-xs pr-sm">
            <StatusIcons error={error} valid={valid} />
          </div>
        )}
      </div>
    </FormControlWrapper>
  )
}

export { Phone }
