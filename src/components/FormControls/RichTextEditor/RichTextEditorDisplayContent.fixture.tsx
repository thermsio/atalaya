import React from 'react'
import { RichTextEditorDisplayContent } from './RichTextEditorDisplayContent'

const children = (
  <RichTextEditorDisplayContent>
    This is the children content
  </RichTextEditorDisplayContent>
)

const childrenAndHtml = (
  <RichTextEditorDisplayContent html="<p>this is the html content</p> <p></p> <p></p>">
    This is the children content
  </RichTextEditorDisplayContent>
)

const html = (
  <RichTextEditorDisplayContent html="<p>this is the html content</p> <p></p> <p></p>" />
)

export default { children, html, childrenAndHtml }
