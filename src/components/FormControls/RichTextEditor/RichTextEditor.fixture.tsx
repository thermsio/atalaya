import React from 'react'
import { Stack } from '../../../layout/Stack'
import { RichTextEditor } from './index'

// eslint-disable-next-line react/display-name
export default (): React.ReactElement => {
  const uploadCallback = () =>
    new Promise<string>((resolve) => {
      setTimeout(() => {
        resolve(
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU',
        )
      }, 1500)
    })

  const [state, setState] = React.useState({ text: '', html: '' })

  return (
    <Stack space="base">
      <RichTextEditor
        autofocus
        onChangeValue={setState}
        onImageUpload={uploadCallback}
      />

      <div className="text-sm text-color-subtle">Returned values preview</div>

      <div className="text-lg">newState.text</div>
      <div className="rounded bg-surface-subtle p-sm">{state.text}</div>

      <div className="text-lg">newState.html</div>
      <div className="rounded bg-surface-subtle p-sm">{state.html}</div>

      <div className="text-sm text-color-subtle">
        Notice that they only update after 500ms from the last change.
      </div>
    </Stack>
  )
}
