Provides more advanced ways for users to style their texts.

### onChangeValue arguments

**html**: It's the fully enriched version of the current editor state.

**text**: Plain text fallback. Useful if you need to provide support to other environments that are not compatible with
html.

### Embedding Images

The editor supports two ways of adding images, pasting a URL or uploading the file. URL embedding it's always available,
for the upload option to be active a function in charge of uploading the image must be provided to `onImageUpload`
prop.

This function should take a [File object](https://developer.mozilla.org/en-US/docs/Web/API/File) as an argument and
return a Promise resolving on a string with the url where the file is located.

Note: for the purposes of this documentation, our function simply waits for a bit and returns a fixed string regardless
of the file that has been chosen.

```jsx
import { Stack } from '../../../layout/Stack'

const uploadCallback = () =>
  new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU',
      )
    }, 1500)
  })

const [state, setState] = React.useState({})

;<Stack space="base">
  <RichTextEditor
    autofocus
    onChangeValue={setState}
    onImageUpload={uploadCallback}
  />

  <div className="text-sm text-color-subtle">Returned values preview</div>

  <div className="text-lg">newState.text</div>
  <div className="rounded bg-surface-subtle p-sm">{state.text}</div>

  <div className="text-lg">newState.html</div>
  <div className="rounded bg-surface-subtle p-sm">{state.html}</div>

  <div className="text-sm text-color-subtle">
    Notice that they only update after 500ms from the last change.
  </div>
</Stack>
```

### With labels

```jsx
const [state, setState] = React.useState({})

const uploadCallback = () =>
  new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU',
      )
    }, 1500)
  })

;<RichTextEditor
  label="Rich Text Editor Label"
  hint="Required"
  placeholder="Write your message here..."
  onChangeValue={setState}
  onImageUpload={uploadCallback}
/>
```

### With Error

```jsx
const [state, setState] = React.useState({})

const uploadCallback = () =>
  new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU',
      )
    }, 1500)
  })

;<RichTextEditor
  error="There is something very wrong with this input."
  label="Rich Text Editor Label"
  hint="Required"
  placeholder="Write your message here..."
  onChangeValue={setState}
  onImageUpload={uploadCallback}
/>
```

### Valid

```jsx
const [state, setState] = React.useState({})

const uploadCallback = () =>
  new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU',
      )
    }, 1500)
  })

;<RichTextEditor
  valid
  label="Rich Text Editor Label"
  hint="Required"
  placeholder="Write your message here..."
  onChangeValue={setState}
  onImageUpload={uploadCallback}
/>
```

### Disabled

```jsx
const [state, setState] = React.useState({})

;<RichTextEditor
  disabled
  label="Rich Text Editor Label"
  hint="Required"
  placeholder="Write your message here..."
  onChangeValue={setState}
/>
```

### Displaying RichTextEditor Content (html)

The global styles in this project reset CSS styles for displaying of elements
such as `<ul>`, `<ol>` & `<li>`. This component can be used to display the RTE
html content, visually, the same as the RTE.

Given this html that is produced from the RTE: `<div><ol><li>One</li><li>Two</li><ol></div>`

```jsx
import { RichTextEditorDisplayContent } from './RichTextEditorDisplayContent'

;<RichTextEditorDisplayContent html='<p><del><strong><em><ins>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</ins></em></strong></del></p> <p><span style="font-size: 30px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></p> <p style="text-align:left;"><span style="font-size: 16px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></p> <p style="text-align:center;"><span style="font-size: 16px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></p> <p style="text-align:right;"><span style="color: rgb(247,218,100);font-size: 16px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></p> <p style="text-align:justify;"><a href="http://www.therms.io" target="_blank"><span style="font-size: 16px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></a>&nbsp;</p> <p>🤓🤔</p> <p></p> <div style="text-align:left;"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU" alt="undefined" style="height: auto;width: auto"/></div> <p></p> <div style="text-align:none;"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU" alt="undefined" style="height: auto;width: auto"/></div> <p></p> <div style="text-align:right;"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU" alt="undefined" style="height: auto;width: auto"/></div> <p></p> <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1-PlDs17nT662wlTCvuCRfxBJUI2cgSjklA&usqp=CAU" alt="undefined" style="height: 200px;width: 600px"/> <ol> <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li> <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li> <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li> <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li> </ol> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>' />
```
