import React, { useEffect, useMemo, useState } from 'react'
import { EditorState, convertToRaw, ContentState } from 'draft-js'
import { Editor } from 'react-draft-wysiwyg'
import draftToHtml from 'draftjs-to-html'
import htmlToDraft from 'html-to-draftjs'

import { useDebounce } from '../../../hooks/useDebounce'

import { editorConfig } from './editorConfig'

import './RichTextEditor.css'
import { StatusIcons } from '../components/StatusIcons'
import { FormControlCommonProps } from '../types'
import {
  FormControlWrapper,
  FormControlWrapperPublicProps,
} from '../../FormLayout/components/FormControlWrapper'

export interface RichTextEditorProps
  extends FormControlCommonProps,
    FormControlWrapperPublicProps {
  /** If true, Editor will be focused on load */
  autofocus?: boolean
  /** Initial state will be created from this. Useful for editing previously created content.   */
  initialHTML?: string
  /** Function receives object with props: html and text.
   *  Because exporting the state it's an expensive operation, it will only be triggered after 750ms of inactivity.
   * */
  onChangeValue: (newState: { html: string; text: string }) => void
  /** Callback to process an image to be uploaded. <br>
   * It takes a [File object](https://developer.mozilla.org/en-US/docs/Web/API/File) as it's argument and should return a Promise resolving on a string with the url where the file is located.
   * */
  onImageUpload?: (file: File) => Promise<string>
  placeholderText?: string
}

const RichTextEditor = ({
  autofocus,
  initialHTML,
  onChangeValue,
  onImageUpload,
  disabled,
  error,
  hint,
  label,
  labelBehavior,
  maxWidth,
  minWidth,
  name,
  onBlur,
  onFocus,
  placeholder,
  subText,
  showErrorMessage,
  valid,
}: RichTextEditorProps): React.ReactElement => {
  const initialState = useMemo(() => {
    if (initialHTML) {
      let html = initialHTML

      // CORE-3088 the initial HTML value must start w/ a <p> tag, an error occurs in a case where an image is added
      // and the initial HTML starts with <img .../> <p></p>
      if (html && !html.startsWith('<p>')) {
        html = `<p>${html}</p>`
      }

      const contentBlock = htmlToDraft(html)
      const contentState = ContentState.createFromBlockArray(
        contentBlock.contentBlocks,
      )

      return EditorState.createWithContent(contentState)
    }

    return EditorState.createEmpty()
  }, [])

  const [editorState, setEditorState] = useState(initialState)

  const debouncedValues = useDebounce(
    {
      html: draftToHtml(convertToRaw(editorState.getCurrentContent())),
      text: editorState.getCurrentContent().getPlainText(),
    },
    300,
  )

  const handleImageUpload = useMemo(() => {
    if (onImageUpload) {
      return async (file) => {
        const link = await onImageUpload(file)
        return { data: { link } }
      }
    }

    return undefined
  }, [onImageUpload])

  useEffect(() => {
    onChangeValue({
      html: debouncedValues.html,
      text: debouncedValues.text,
    })
  }, [debouncedValues.html, debouncedValues.text])

  const [editorRef, setEditorRef] = useState<{ focus?: () => void }>({})

  useEffect(() => {
    if (autofocus) editorRef?.focus?.()
  }, [autofocus, editorRef])

  return (
    <FormControlWrapper
      error={error}
      disabled={disabled}
      hasValue={!!debouncedValues.text}
      hint={hint}
      inputName={name}
      label={label}
      labelBehavior={labelBehavior}
      maxWidth={maxWidth}
      minWidth={minWidth}
      showErrorMessage={showErrorMessage}
      subText={subText}
    >
      <div className="relative">
        {disabled && (
          <div className="absolute inset-0 z-dropdown rounded bg-surface opacity-80" />
        )}

        <Editor
          tabIndex={disabled ? -1 : undefined}
          editorRef={setEditorRef}
          editorState={editorState}
          placeholder={placeholder}
          onBlur={onBlur}
          onEditorStateChange={setEditorState}
          onFocus={onFocus}
          toolbar={editorConfig}
          toolbarCustomButtons={[
            <div
              className="flex flex-grow items-center justify-end pb-xxs pr-xxs"
              key="statusIcons"
            >
              <StatusIcons error={error} valid={valid} />
            </div>,
          ]}
          uploadCallback={handleImageUpload}
          spellCheck
        />
      </div>
    </FormControlWrapper>
  )
}

export { RichTextEditor }
