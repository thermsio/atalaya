import React from 'react'
import classNames from 'classnames'

export interface RichTextEditorDisplayContentProps {
  /** Optional className for the div container */
  className?: string
  children?: React.ReactNode
  /** Will override children if both are provided */
  html?: string
}

function RichTextEditorDisplayContent({
  className,
  children,
  html,
}: RichTextEditorDisplayContentProps) {
  if (html) {
    return (
      <div
        className={classNames(
          'prose max-w-none prose-img:mx-auto prose-img:rounded',
          className,
        )}
        dangerouslySetInnerHTML={{ __html: html }}
      />
    )
  }

  if (children) {
    return (
      <div
        className={classNames(
          'prose max-w-none prose-img:mx-auto prose-img:rounded',
          className,
        )}
      >
        {children}
      </div>
    )
  }

  return null
}

export { RichTextEditorDisplayContent }
