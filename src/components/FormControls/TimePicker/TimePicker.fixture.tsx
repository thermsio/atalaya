import React, { useState } from 'react'
import { TimePicker } from './TimePicker'
import { useValue } from 'react-cosmos/client'

const base = () => {
  const [timestamp, setTimestamp] = useState<string | undefined>('')
  const [is24] = useValue('is24', { defaultValue: true })
  const [minuteStep] = useValue<number>('minuteStep', { defaultValue: 1 })

  return (
    <TimePicker
      value={timestamp}
      onChangeValue={(newValue) => {
        console.log('onChangeValue called', newValue)
        setTimestamp(newValue)
      }}
      is24={is24}
      minuteStep={minuteStep}
    />
  )
}

export default { base }
