import React from 'react'
import {
  FormControlWrapper,
  FormControlWrapperPublicProps,
} from '../../FormLayout/components/FormControlWrapper'
import classNameParser from 'classnames'
import { StatusIcons } from '../components/StatusIcons'
import { FormControlCommonProps } from '../types'
import { TimeBase } from './components/TimeBase'

export interface TimePickerProps
  extends Omit<FormControlCommonProps, 'onChange'>,
    FormControlWrapperPublicProps {
  is24?: boolean
  /** If this is true there will be no internal way of clearing the value. */
  hideClearButton?: boolean
  minuteStep?: number
  onChange?: (e: { target: { value?: string } }) => void
  onChangeValue?: (newTime?: string) => void
  value?: string
}

const TimePicker = ({
  disabled,
  error,
  hint,
  hideClearButton,
  is24 = false,
  label,
  labelBehavior,
  maxWidth,
  minWidth,
  minuteStep = 1,
  name,
  onBlur,
  onChange,
  onChangeValue,
  onFocus,
  showErrorMessage,
  subText,
  valid,
  value,
}: TimePickerProps): React.ReactElement => {
  return (
    <FormControlWrapper
      error={error}
      disabled={disabled}
      hasValue={!!value}
      hint={hint}
      inputName={name}
      label={label}
      labelBehavior={labelBehavior}
      maxWidth={maxWidth}
      minWidth={minWidth}
      subText={subText}
      showErrorMessage={showErrorMessage}
    >
      <div
        className={classNameParser(
          {
            'form-field-disabled': disabled,
            'form-field-error': error,
            'form-field-success': valid,
          },
          'form-field-input-wrapper flex items-center justify-between',
        )}
        onBlur={onBlur}
        onFocus={onFocus}
      >
        <TimeBase
          disabled={disabled}
          is24={is24}
          minuteStep={minuteStep}
          name={name}
          onChangeValue={onChangeValue}
          value={value}
          onChange={onChange}
          hideClearButton={hideClearButton}
        />

        {(error || valid) && (
          <>
            <div className="pl-xs" />
            <StatusIcons error={error} valid={valid} />
            <div className="pr-xs" />
          </>
        )}
      </div>
    </FormControlWrapper>
  )
}

TimePicker.displayName = 'TimePicker'

export { TimePicker }
