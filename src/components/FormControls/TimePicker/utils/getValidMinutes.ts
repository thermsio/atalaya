export const getValidMinutes = (minutes: number) => {
  let parsedMinutes: number = minutes

  if (parsedMinutes < 0) parsedMinutes = 0
  if (parsedMinutes > 59) parsedMinutes = 59

  return parsedMinutes
}
