export const getValidHour = (hour: number, suffix?: string) => {
  let parsedHour: number = hour

  if (suffix) {
    if (hour > 12) parsedHour = 12
    if (hour < 1) parsedHour = 1
  } else {
    if (hour > 23) parsedHour = 23
    if (hour < 0) parsedHour = 0
  }

  if (suffix && parsedHour === 12) {
    parsedHour -= 12
  }

  if (suffix === 'PM') {
    parsedHour = parsedHour + 12
  }

  return parsedHour
}
