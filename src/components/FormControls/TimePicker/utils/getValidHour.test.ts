import { getValidHour } from './getValidHour'

describe('formatHour', () => {
  it('should handle all possible input values for the hour parameter', () => {
    expect(getValidHour(0)).toBe(0)
    expect(getValidHour(5)).toBe(5)
    expect(getValidHour(12)).toBe(12)
    expect(getValidHour(23)).toBe(23)
    expect(getValidHour(1, 'PM')).toBe(13)
    expect(getValidHour(6, 'PM')).toBe(18)
    expect(getValidHour(12, 'PM')).toBe(12)
    expect(getValidHour(1, 'AM')).toBe(1)
    expect(getValidHour(6, 'AM')).toBe(6)
    expect(getValidHour(12, 'AM')).toBe(0)
  })

  it('should handle values that go above the max and minimum', () => {
    expect(getValidHour(25)).toBe(23)
    expect(getValidHour(-25)).toBe(0)
    expect(getValidHour(25, 'AM')).toBe(0)
    expect(getValidHour(-25, 'AM')).toBe(1)
    expect(getValidHour(25, 'PM')).toBe(12)
    expect(getValidHour(-25, 'PM')).toBe(13)
  })

  it('should handle invalid input values gracefully', () => {
    expect(getValidHour(NaN)).toBe(NaN)
    expect(getValidHour(Infinity)).toBe(23)
    expect(getValidHour(-Infinity)).toBe(0)
  })
})
