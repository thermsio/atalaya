import dayjs from 'dayjs'

export const getTimestampFromHoursAndMinutes = (
  newValue: { hour?: number; minute?: number } | null,
  currentTimestamp?: string,
) => {
  if (newValue === null) return ''

  let instance = dayjs(currentTimestamp || undefined)

  if (newValue.hour !== undefined) {
    instance = instance.hour(newValue.hour)
  }

  if (newValue.minute !== undefined) {
    instance = instance.minute(newValue.minute)
  }

  return instance.toISOString()
}
