import React, { useRef } from 'react'
import { getValidHour } from '../utils/getValidHour'
import { getValidMinutes } from '../utils/getValidMinutes'
import classNames from 'classnames'

interface HourMinutePickProps {
  className?: string
  disabled?: boolean
  hour?: string
  is24?: boolean
  minuteStep?: number
  minute?: string
  name?: string
  onChangeValue: (newValue: { hour?: number; minute?: number }) => void
  onSwitchSuffix?: () => void
  suffix?: string
}

const HourMinutePick = ({
  className,
  disabled,
  hour,
  minuteStep = 1,
  minute,
  name,
  onChangeValue,
  onSwitchSuffix,
  suffix,
}: HourMinutePickProps) => {
  const hourRef = useRef<HTMLInputElement>(null)
  const minuteRef = useRef<HTMLInputElement>(null)
  const suffixRef = useRef<HTMLSpanElement>(null)
  const isTimeEmpty = !hour && !minute

  return (
    <div className={className}>
      <input
        className="w-5 border-none bg-transparent px-0 text-right text-color hover:underline focus:underline focus:ring-0 inline text-sm"
        disabled={disabled}
        id={`${name}/hours`}
        onChange={(e) => {
          const newHour = parseInt(e.target.value || '0', 10)

          if (!Number.isNaN(newHour)) {
            onChangeValue({ hour: getValidHour(newHour, suffix) })
          }
        }}
        onKeyDown={(e) => {
          if (
            e.key === 'ArrowRight' &&
            // @ts-expect-error e.target.selectionStart is missing in Type
            e.target.selectionStart === e.target.value.length
          ) {
            return minuteRef.current?.focus()
          }

          let newHour: number | undefined

          if (e.key === 'ArrowDown') {
            newHour = parseInt(hour || '0', 10) - 1
          } else if (e.key === 'ArrowUp') {
            newHour = parseInt(hour || '0', 10) + 1
          }

          if (newHour !== undefined && !Number.isNaN(newHour)) {
            onChangeValue({ hour: getValidHour(newHour, suffix) })
          }
        }}
        name={`${name}/hours`}
        placeholder="--"
        ref={hourRef}
        type="text"
        value={hour}
      />

      <span className="px-px text-sm">:</span>

      <input
        className="w-5 border-none bg-transparent px-0 text-left text-color hover:underline focus:underline focus:ring-0 text-sm"
        placeholder="--"
        disabled={disabled}
        id={`${name}/minutes`}
        onChange={(e) => {
          const newMinutes = parseInt(e.target.value || '0', 10)

          if (!Number.isNaN(newMinutes)) {
            onChangeValue({ minute: getValidMinutes(newMinutes) })
          }
        }}
        onKeyDown={(e) => {
          // @ts-expect-error e.target.selectionStart is missing in Type
          if (e.key === 'ArrowLeft' && e.target.selectionStart === 0) {
            return hourRef.current?.focus()
          }

          if (
            e.key === 'ArrowRight' &&
            // @ts-expect-error e.target.selectionStart is missing in Type
            e.target.selectionStart === e.target.value.length
          ) {
            return suffixRef.current?.focus()
          }

          let newMinutes: number | undefined

          if (e.key === 'ArrowDown') {
            newMinutes = parseInt(minute || '0', 10) - minuteStep
          } else if (e.key === 'ArrowUp') {
            newMinutes = parseInt(minute || '0', 10) + minuteStep
          }

          if (newMinutes !== undefined && !Number.isNaN(newMinutes)) {
            onChangeValue({ minute: getValidMinutes(newMinutes) })
          }
        }}
        name={`${name}/minutes`}
        ref={minuteRef}
        type="text"
        value={minute}
      />

      {!!suffix && (
        <span
          className={classNames(
            {
              'text-color-subtle': isTimeEmpty,
              'cursor-pointer transition hover:underline focus-visible:outline-none focus-visible:ring-1 focus-visible:ring-main focus-visible:ring-offset-1 focus-visible:ring-offset-input-background':
                onSwitchSuffix,
            },
            'mr-xxs select-none rounded text-xs font-bold',
          )}
          onClick={onSwitchSuffix}
          onKeyDown={(e) => {
            if (e.key === 'ArrowLeft') {
              return minuteRef.current?.focus()
            }

            if (
              onSwitchSuffix &&
              (e.key === ' ' || e.key === 'ArrowDown' || e.key === 'ArrowUp')
            ) {
              onSwitchSuffix()
            }
          }}
          ref={suffixRef}
          tabIndex={0}
        >
          {suffix}
        </span>
      )}
    </div>
  )
}

export { HourMinutePick }
