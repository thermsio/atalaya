import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { HourMinutePick } from '../HourMinutePick'
import { Button } from '../../../../Button'
import { createMockReactSyntheticEvent } from '../../../../../utils/mock-react-synthetic-event'
import { Icon } from '../../../../Icon'
import { HourMinuteDropdown } from '../HourMinuteDropdown'
import dayjs from 'dayjs'
import { useDebounceFn } from 'ahooks'
import { getTimestampFromHoursAndMinutes } from '../../utils/getTimestampFromHoursAndMinutes'

export interface TimeBaseProps {
  disabled?: boolean
  is24?: boolean
  /** If this is true there will be no internal way of clearing the value. */
  hideClearButton?: boolean
  minuteStep?: number
  name?: string
  onChange?: (e: { target: { value?: string } }) => void
  onChangeValue?: (newTime?: string) => void
  value?: string
}

function TimeBase({
  disabled,
  is24,
  hideClearButton,
  minuteStep,
  name,
  onChange,
  onChangeValue,
  value,
}: TimeBaseProps) {
  const [localTimestamp, setLocalTimestamp] = useState<string>('')

  useEffect(() => {
    setLocalTimestamp(value || '')
  }, [value])

  const { formattedHour, formattedMinute, suffix } = useMemo(() => {
    if (!localTimestamp) {
      return {
        // We use '' instead of undefined to keep the underlying input element as controlled, making sure it re-renders even when the value is empty.
        formattedHour: '',
        formattedMinute: '',
        suffix: is24 ? '' : 'AM',
      }
    }

    const instance = dayjs(localTimestamp || undefined)

    return {
      formattedHour: instance.format(is24 ? 'HH' : 'h'),
      formattedMinute: instance.format('mm'),
      suffix: is24 ? '' : instance.format('A'),
    }
  }, [is24, localTimestamp])

  const updateLocalTimestamp = useCallback(
    (newValue: { hour?: number; minute?: number } | null) => {
      const newTimestamp = getTimestampFromHoursAndMinutes(
        newValue,
        localTimestamp,
      )
      setLocalTimestamp(newTimestamp)
    },
    [localTimestamp],
  )

  const handleOnChange = useCallback(
    (
      newValue: { hour?: number; minute?: number } | string | null,
      e?: { target: { value: string } },
    ) => {
      const newTimestamp =
        typeof newValue === 'string'
          ? newValue
          : getTimestampFromHoursAndMinutes(newValue, localTimestamp)

      if (onChangeValue) onChangeValue(newTimestamp)

      if (onChange) {
        const event =
          e || createMockReactSyntheticEvent({ value: newTimestamp })

        if (e) {
          event.target.value = newTimestamp
        }

        onChange(event)
      }
    },
    [onChange, onChangeValue, localTimestamp],
  )

  const { run: handleOnChangeDebounced } = useDebounceFn(handleOnChange, {
    wait: 500,
  })

  const switchSuffix = useCallback(() => {
    const currentHour = localTimestamp ? dayjs(localTimestamp).hour() : 0
    const newHour = currentHour + (suffix === 'AM' ? 12 : -12)
    updateLocalTimestamp({ hour: newHour })
    handleOnChange({ hour: newHour })
  }, [localTimestamp, suffix])

  return (
    <div className="flex items-center">
      <HourMinutePick
        disabled={disabled}
        hour={formattedHour}
        is24={is24}
        minuteStep={minuteStep}
        minute={formattedMinute}
        name={name}
        onChangeValue={(newValue) => {
          updateLocalTimestamp(newValue)
          handleOnChangeDebounced(newValue)
        }}
        onSwitchSuffix={switchSuffix}
        suffix={suffix}
      />

      {!!localTimestamp && !hideClearButton && (
        <Button
          className="align-text-bottom"
          onClick={(e) => {
            e.stopPropagation()

            updateLocalTimestamp(null)
            handleOnChange(null)
          }}
          subtle
          size="sm"
        >
          <Icon size="xs">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 24 24"
              fill="currentColor"
            >
              <path d="M0 0h24v24H0V0z" fill="none" />
              <path d="M18.3 5.71c-.39-.39-1.02-.39-1.41 0L12 10.59 7.11 5.7c-.39-.39-1.02-.39-1.41 0-.39.39-.39 1.02 0 1.41L10.59 12 5.7 16.89c-.39.39-.39 1.02 0 1.41.39.39 1.02.39 1.41 0L12 13.41l4.89 4.89c.39.39 1.02.39 1.41 0 .39-.39.39-1.02 0-1.41L13.41 12l4.89-4.89c.38-.38.38-1.02 0-1.4z" />
            </svg>
          </Icon>
        </Button>
      )}

      <HourMinuteDropdown
        disabled={disabled}
        hour={formattedHour}
        minuteStep={minuteStep}
        minute={formattedMinute}
        name={name}
        onChangeValue={updateLocalTimestamp}
        onDropdownClose={() => {
          handleOnChange(localTimestamp as string)
        }}
        onSwitchSuffix={switchSuffix}
        suffix={suffix}
      />
    </div>
  )
}

export { TimeBase }
