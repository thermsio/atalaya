import React from 'react'
import { Button } from '../../../Button'
import {
  stepDownIcon,
  stepUpIcon,
} from '../../DateTimePicker/components/DateTimeIcons'
import { getValidHour } from '../utils/getValidHour'
import { getValidMinutes } from '../utils/getValidMinutes'
import { ButtonTurbo } from '../../../Button/ButtonTurbo'
import classNames from 'classnames'
import { Icon } from '../../../Icon'
import { Dropdown } from '../../../Dropdown'

interface HourMinuteDropdownProps {
  disabled?: boolean
  hour?: string
  is24?: boolean
  minuteStep?: number
  minute?: string
  name?: string
  onChangeValue: (newValue: { hour?: number; minute?: number }) => void
  onDropdownClose?: () => void
  onSwitchSuffix?: () => void
  suffix?: string
}

function HourMinuteDropdown({
  disabled,
  hour = '',
  minuteStep = 1,
  minute = '',
  onChangeValue,
  onDropdownClose,
  onSwitchSuffix,
  suffix,
}: HourMinuteDropdownProps) {
  return (
    <Dropdown
      controller={({ close, isOpen, open }) => (
        <Button onClick={isOpen ? close : open} size="sm" subtle>
          <Icon size="xs">
            <svg
              fill="currentColor"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="M0 0h24v24H0V0z" fill="none" />
              <path d="M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8zm.5-13H11v6l5.25 3.15.75-1.23-4.5-2.67z" />
            </svg>
          </Icon>
        </Button>
      )}
      onClose={onDropdownClose}
    >
      <div className="max-w-[9rem]">
        <div className="flex items-center justify-center">
          <div className="space-y-xxs">
            <ButtonTurbo
              disabled={disabled}
              onHeld={() => {
                const newHour = parseInt(hour || '0', 10) + 1

                if (newHour !== undefined && !Number.isNaN(newHour)) {
                  const validHour = getValidHour(newHour, suffix)
                  onChangeValue({ hour: validHour })
                }
              }}
              subtle
            >
              {stepUpIcon}
            </ButtonTurbo>

            <div
              className={classNames(
                { 'text-color-subtle': !hour },
                'w-8 text-center text-md font-bold',
              )}
            >
              {hour || '--'}
            </div>

            <ButtonTurbo
              disabled={disabled}
              onHeld={() => {
                const newHour = parseInt(hour || '0', 10) - 1

                if (newHour !== undefined && !Number.isNaN(newHour)) {
                  const validHour = getValidHour(newHour, suffix)
                  onChangeValue({ hour: validHour })
                }
              }}
              subtle
            >
              {stepDownIcon}
            </ButtonTurbo>
          </div>

          <div className="text-md font-bold">:</div>

          <div className="space-y-xxs">
            <ButtonTurbo
              disabled={disabled}
              onHeld={() => {
                const newMinutes = parseInt(minute || '0', 10) + minuteStep

                if (newMinutes !== undefined && !Number.isNaN(newMinutes)) {
                  const formattedMinutes = getValidMinutes(newMinutes)
                  onChangeValue({ minute: formattedMinutes })
                }
              }}
              subtle
            >
              {stepUpIcon}
            </ButtonTurbo>

            <div
              className={classNames(
                { 'text-color-subtle': !hour },
                'w-8 text-center text-md font-bold',
              )}
            >
              {minute || '--'}
            </div>

            <ButtonTurbo
              disabled={disabled}
              onHeld={() => {
                const newMinutes = parseInt(minute || '0', 10) - minuteStep

                if (newMinutes !== undefined && !Number.isNaN(newMinutes)) {
                  const formattedMinutes = getValidMinutes(newMinutes)
                  onChangeValue({ minute: formattedMinutes })
                }
              }}
              subtle
            >
              {stepDownIcon}
            </ButtonTurbo>
          </div>
        </div>

        {!!suffix && (
          <div className="mt-xs rounded bg-surface">
            <Button fullWidth subtle onClick={onSwitchSuffix} variant="main">
              {suffix}
            </Button>
          </div>
        )}
      </div>
    </Dropdown>
  )
}

export { HourMinuteDropdown }
