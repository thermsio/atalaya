import React, { useState } from 'react'
import { Email } from './Email'

const base = () => {
  const [value, setValue] = useState('')

  return (
    <Email
      placeholder="johnDoe@atalaya.com"
      onBlur={(e) => console.log('blur event fired', e)}
      value={value}
      label="Email Field"
      labelBehavior="floating"
      onChangeValue={setValue}
    />
  )
}

export default base
