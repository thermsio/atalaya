Built in validation

```jsx
const [error, setError] = React.useState('');
const [valid, setValid] = React.useState();

const handleInvalid = () => {
  setValid(false)
  setError('There is something wrong here')
}

const handleValid = () => {
  setError('')
  setValid(true)
}

<Email
  placeholder="johnDoe@atalaya.com"
  error={error}
  valid={valid}
  onInvalid={handleInvalid}
  onValid={handleValid}
/>
```

### With labels
```jsx
const [error, setError] = React.useState('');
const [valid, setValid] = React.useState();

const handleInvalid = () => {
  setValid(false)
  setError('There is something wrong here')
}

const handleValid = () => {
  setError('')
  setValid(true)
}

<Email
  label="Email Field"
  hint="Required"
  placeholder="johnDoe@atalaya.com"
  subText="Please provide a valid address."
  error={error}
  valid={valid}
  onInvalid={handleInvalid}
  onValid={handleValid}
/>
```

### With Error

```jsx
const [error, setError] = React.useState('');
const [valid, setValid] = React.useState();

const handleInvalid = () => {
  setValid(false)
  setError('There is something wrong here')
}

const handleValid = () => {
  setError('')
  setValid(true)
}

<Email
  label="Email Field"
  hint="Required"
  placeholder="johnDoe@atalaya.com"
  error={error}
  valid={valid}
  onInvalid={handleInvalid}
  onValid={handleValid}
/>
```

### Valid

```jsx
const [error, setError] = React.useState('');
const [valid, setValid] = React.useState();

const handleInvalid = () => {
  setValid(false)
  setError('There is something wrong here')
}

const handleValid = () => {
  setError('')
  setValid(true)
}

<Email
  label="Email Field"
  hint="Required"
  placeholder="johnDoe@atalaya.com"
  error={error}
  valid={valid}
  onInvalid={handleInvalid}
  onValid={handleValid}
/>
```

### Disabled

```jsx
const [error, setError] = React.useState('');
const [valid, setValid] = React.useState();

const handleInvalid = () => {
  setValid(false)
  setError('There is something wrong here')
}

const handleValid = () => {
  setError('')
  setValid(true)
}

<Email
  disabled
  label="Email Field"
  hint="Required"
  placeholder="johnDoe@atalaya.com"
  value="johnDoe@atalaya.com"
  error={error}
  valid={valid}
  onInvalid={handleInvalid}
  onValid={handleValid}
/>
```
