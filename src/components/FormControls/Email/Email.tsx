import React, { useCallback, useState } from 'react'
import classNameParser from 'classnames'
import {
  FormControlWrapper,
  FormControlWrapperPublicProps,
} from '../../FormLayout/components/FormControlWrapper'
import { FormControlCommonProps } from '../types'
import { StatusIcons } from '../components/StatusIcons'
import '../FormControls.css'

const VALIDATION_PATTERN = '\\S+@\\S+\\.\\S+'

export interface EmailProps
  extends FormControlCommonProps,
    FormControlWrapperPublicProps {
  value?: string
  onInvalid?: (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => void
  onValid?: (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => void
}

const Email = ({
  autoComplete,
  error,
  disabled,
  hint,
  label,
  labelBehavior,
  maxWidth,
  minWidth,
  name,
  onChange,
  onBlur,
  onChangeValue,
  onFocus,
  onInvalid,
  onValid,
  placeholder,
  showErrorMessage,
  subText,
  valid,
  value,
}: EmailProps): React.ReactElement => {
  const [dirty, setDirty] = useState(false)

  const handleOnChange = useCallback(
    (e) => {
      if (onChangeValue) onChangeValue(e.target.value)
      if (onChange) onChange(e)

      if (dirty) {
        if (onInvalid && !e.target.reportValidity()) onInvalid(e)
        if (onValid && e.target.reportValidity()) onValid(e)
      }
    },
    [onChange, onChangeValue, onInvalid, onValid, dirty],
  )

  const handleBlur = useCallback(
    (e) => {
      if (!dirty) {
        setDirty(true)
        if (onInvalid && !e.target.reportValidity()) onInvalid(e)
        if (onValid && e.target.reportValidity()) onValid(e)
      }

      if (onBlur) onBlur(e)
    },
    [dirty],
  )

  return (
    <FormControlWrapper
      error={error}
      disabled={disabled}
      hasValue={!!value}
      hint={hint}
      inputName={name}
      label={label}
      labelBehavior={labelBehavior}
      maxWidth={maxWidth}
      minWidth={minWidth}
      subText={subText}
      showErrorMessage={showErrorMessage}
    >
      <div
        className={classNameParser(
          {
            'form-field-disabled': disabled,
            'form-field-error': error,
            'form-field-success': valid,
          },
          'form-field-input-wrapper flex items-center justify-between',
        )}
      >
        <input
          autoComplete={autoComplete || 'off'}
          className="form-field"
          type="email"
          name={name}
          id={name}
          disabled={disabled}
          onBlur={handleBlur}
          onChange={handleOnChange}
          onFocus={onFocus}
          pattern={VALIDATION_PATTERN}
          placeholder={placeholder}
          value={value}
        />

        {(error || valid) && (
          <div className="py-xs pr-sm">
            <StatusIcons error={error} valid={valid} />
          </div>
        )}
      </div>
    </FormControlWrapper>
  )
}

export { Email }
