This component is used for selection of multiple values - adding/removing.

```jsx
const [options, setOptions] = React.useState([
  {
    description: 'Lorem Ipsum',
    label: 'Oladosu Robertson',
    value: 'Oladosu Robertson',
  },
  {
    description: 'Lorem Ipsum',
    label: 'Gordon Johnston',
    value: 'Gordon Johnston',
  },
  {
    description:
      'Deserunt aute veniam ullamco mollit reprehenderit aliqua tempor non consectetur cillum laborum ipsum labore.',
    label: 'Owen Poole',
    value: 'Owen Poole',
  },
])

const [value, setValue] = React.useState([])

;<MultiSelect
  label="Example MultiSelect"
  maxHeight={200}
  options={options}
  onChangeValue={setValue}
  placeholder="Search for local and remote items"
  value={value}
/>
```

## Infinite Scrolling

This component can provide infinite scroll functionality with the use of `loadOptions` callback prop.

```jsx
const data = new Array(1000).fill(null).map((v,i) => ({ label: `Label ${i}`, value: i }))
const limit = 50

const [options, setOptions] = React.useState(data.slice(0, limit))
const [isLoading, setIsLoading] = React.useState(false)
const [value, setValue] = React.useState([])

const timeoutRef = React.useRef(null)

const handleLoadOptions = React.useCallback(
    ({ additional, text } = {}) => {
        setIsLoading(true)
        if (timeoutRef.current) {
            clearTimeout(timeoutRef.current)
        }
        if (text) {
            timeoutRef.current = setTimeout(() => {
                let newOptions = []

                if (additional) {
                    newOptions = [
                        ...options,
                        ...data
                            .filter((option) => option.label.includes(text))
                            .slice(options.length, options.length + limit),
                    ]
                } else {
                    newOptions = data
                        .filter((option) => option.label.includes(text))
                        .slice(0, limit)
                }

                setOptions(newOptions)
                setIsLoading(false)
            }, 2000)
        } else if (additional) {
            timeoutRef.current = setTimeout(() => {
                setOptions([
                    ...options,
                    ...data.slice(options.length, options.length + limit),
                ])
                setIsLoading(false)
            }, 1000)
        } else {
            setOptions(data.slice(0, limit))
            setIsLoading(false)
        }
    },
    [options],
)

;<div className="container mx-lg px-base">
    <MultiSelect
        maxHeight={200}
        label="Example MultiSelect Infinite Scroll"
        loading={isLoading}
        loadOptions={handleLoadOptions}
        onChangeValue={setValue}
        options={options}
        optionValueExtractor={(option) => option.value}
        value={value}
    />
</div>
```
