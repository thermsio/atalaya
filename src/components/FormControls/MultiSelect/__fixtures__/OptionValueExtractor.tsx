import React, { useState } from 'react'
import { MultiSelect } from '../MultiSelect'
import { Stack } from '../../../../layout/Stack'

const data = [
  {
    category: 'Incident',
    color: '#b82525',
    createdAt: '2022-11-17T20:02:55.157Z',
    formId: 'form--tmplt-entrytp-physaltcn-v1',
    id: 'entrytp-xQXJQi3Mu-1RuMo-221117sZt',
    name: 'Physical Altercation',
    organizationId: 'o-221117sZt',
    regionIds: ['rgn-'],
  },
  {
    category: 'Maintenance',
    color: '#6725b8',
    createdAt: '2022-11-17T20:02:55.158Z',
    formId: 'form--tmplt-entrytp-propdmg-v1',
    id: 'entrytp-FW86JwmLGz4aHIo-221117sZt',
    name: 'Property Damage',
    organizationId: 'o-221117sZt',
    regionIds: ['rgn-'],
  },
  {
    category: 'general',
    createdAt: '2023-01-09T23:32:39.058Z',
    formId: 'form-GByX7aFIjDd_FH',
    id: 'entrytp-2zaLLzjOWPhNkLo-221117sZt',
    name: 'QR Scan Log Entry ',
    organizationId: 'o-221117sZt',
    regionIds: ['rgn-fDtfcU-qjX-seKo-221117sZt'],
  },
  {
    category: 'general',
    createdAt: '2023-01-26T18:15:52.435Z',
    formId: 'form-mW9rP8vbiZF90e',
    id: 'entrytp-YyTZazN87uqDd7o-221117sZt',
    name: 'Test',
    organizationId: 'o-221117sZt',
    regionIds: ['rgn-fDtfcU-qjX-seKo-221117sZt'],
  },
  {
    category: 'General',
    color: '#d67f7f',
    createdAt: '2023-03-06T21:28:45.456Z',
    description: null,
    formId: 'form-I1PsxP7aAYMqoO',
    icon: null,
    id: 'entrytp-CKQXiMGFGqPByqo-221117sZt',
    name: 'Test Form',
    organizationId: 'o-221117sZt',
    regionIds: ['rgn-fDtfcU-qjX-seKo-221117sZt'],
  },
  {
    category: 'General',
    color: '#fafff2',
    createdAt: '2023-03-09T20:37:06.863Z',
    description: '',
    formId: 'form-RY04j6eJUElvUU',
    icon: null,
    id: 'entrytp-S0jjoUo49vJIqco-221117sZt',
    name: 'Test Form Edit',
    organizationId: 'o-221117sZt',
    regionIds: ['rgn-fDtfcU-qjX-seKo-221117sZt'],
  },
  {
    category: 'General',
    color: '#72c11b',
    createdAt: '2023-03-06T22:43:41.264Z',
    description: 'Testing',
    formId: 'form-lC3yDjxAu715PH',
    icon: null,
    id: 'entrytp-T0W_78Hg2iHsFHo-221117sZt',
    name: 'Test Log Entry Form',
    organizationId: 'o-221117sZt',
    regionIds: ['rgn-fDtfcU-qjX-seKo-221117sZt'],
  },
  {
    category: 'General',
    color: null,
    createdAt: '2023-03-09T20:29:33.609Z',
    description: null,
    formId: 'form-_KRV1WBnQPIO3U',
    icon: null,
    id: 'entrytp-XBDIEqAvgyaxq3o-221117sZt',
    name: 'Test QR Scan Entry Log',
    organizationId: 'o-221117sZt',
    regionIds: ['rgn-fDtfcU-qjX-seKo-221117sZt'],
  },
  {
    category: 'general',
    createdAt: '2022-11-30T23:58:26.920Z',
    formId: 'form-zXbgXOXaJV-hkd',
    id: 'entrytp-2eco7JpebKyScvo-221117sZt',
    name: 'Test this for date ',
    organizationId: 'o-221117sZt',
    regionIds: ['rgn-fDtfcU-qjX-seKo-221117sZt'],
  },
  {
    category: 'general',
    color: '#ff0329',
    createdAt: '2022-12-09T23:09:05.741Z',
    formId: 'form-mlon3OZ90keJUz',
    id: 'entrytp-dhBnPCAziSnoPro-221117sZt',
    name: 'Text Test ',
    organizationId: 'o-221117sZt',
    regionIds: [
      'rgn-XnjWWMv8HqtlVwo-221117sZt',
      'rgn-fDtfcU-qjX-seKo-221117sZt',
      'rgn-',
    ],
  },
]

// eslint-disable-next-line react/display-name
export default function () {
  const [value, setValue] = useState<string[]>([])

  return (
    <Stack space="lg">
      <div className="container mx-lg px-base">
        {JSON.stringify(value)}
        <MultiSelect
          maxHeight={200}
          label="Example MultiSelect with optionValueExtractor"
          onChangeValue={setValue}
          options={data}
          optionValueExtractor={(option) => option.id}
          optionLabelExtractor={(option) => option.name}
          renderOption={(option) => (
            <div className="ml-base">
              <p className="mb-0 pb-0 text-base font-bold ">{option.name}</p>
              <span className="text-sm text-neutral">
                {option.description as string}
              </span>
            </div>
          )}
          value={value}
          totalOptions={data.length}
        />
      </div>
    </Stack>
  )
}
