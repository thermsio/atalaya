import React, { useCallback, useRef, useState } from 'react'
import { MultiSelect } from '../MultiSelect'

const data = [
  {
    label: 'Oladosu Robertson',
    value: 'Oladosu Robertson',
    description: 'Lorem Ipsum',
  },
  {
    label: 'Gordon Johnston',
    value: 'Gordon Johnston',
    description: 'Lorem Ipsum',
  },
  {
    label: 'Owen Poole',
    value: 'Owen Poole',
    description:
      'Deserunt aute veniam ullamco mollit reprehenderit aliqua tempor non consectetur cillum laborum ipsum labore.',
  },
  {
    label: 'Cody Payne',
    value: 'Cody Payne',
    description: 'Incididunt consequat dolore ex qui.',
  },
  {
    label: 'Hannah Harper',
    value: 'Hannah Harper',
    description:
      'Anim labore nisi anim irure pariatur voluptate veniam dolore.',
  },
  {
    label: 'Blanche Moore',
    value: 'Blanche Moore',
    description: 'Laboris voluptate tempor aliquip tempor ea in veniam.',
  },
  {
    label: 'Jordan Hale',
    value: 'Jordan Hale',
    description:
      'Est magna do incididunt aliquip pariatur cupidatat magna incididunt commodo Lorem reprehenderit.',
  },
  {
    label: 'Manuel Stanley',
    value: 'Manuel Stanley',
    description:
      'Ut mollit proident fugiat veniam ex quis laborum reprehenderit velit aute.',
  },
  {
    label: 'Leila Lloyd',
    value: 'Leila Lloyd',
    description: 'Et ad anim laborum consequat in minim aliqua.',
  },
  {
    label: 'Carl McKenzie',
    value: 'Carl McKenzie',
    description: 'Ipsum velit sint sunt culpa et dolor.',
  },
  {
    label: 'Catherine Keller',
    value: 'Catherine Keller',
    description: 'Ipsum sunt ipsum ea nulla irure velit laborum.',
  },
  {
    label: 'Alvin Carr',
    value: 'Alvin Carr',
    description:
      'Non laboris laborum officia eu irure occaecat sit ex cupidatat sint nisi nostrud in fugiat.',
  },
  {
    label: 'Jessie Hawkins',
    value: 'Jessie Hawkins',
    description: 'Pariatur id occaecat amet sit ex eiusmod consequat pariatur.',
  },
  {
    label: 'Sean Reyes',
    value: 'Sean Reyes',
    description:
      'Lorem ipsum aute nostrud ut anim eu dolor culpa veniam anim eiusmod amet.',
  },
  {
    label: 'Marguerite Bishop',
    value: 'Marguerite Bishop',
    description:
      'Enim cupidatat nulla proident deserunt officia duis nisi duis elit eu ut deserunt.',
  },
  {
    label: 'Sara Massey',
    value: 'Sara Massey',
    description:
      'Elit cillum occaecat velit fugiat proident elit ipsum esse officia minim laboris.',
  },
  {
    label: 'Hilda Francis',
    value: 'Hilda Francis',
    description:
      'Pariatur occaecat tempor Lorem nulla eu labore cupidatat esse cupidatat id nostrud adipisicing nisi deserunt.',
  },
  {
    label: 'Harry Potter',
    value: 'Harry Potter',
    description: 'Qui ullamco amet non Lorem ut pariatur esse excepteur sunt.',
  },
  {
    label: 'Cordelia Luna',
    value: 'Cordelia Luna',
    description:
      'Sunt ullamco aute commodo ex sunt magna aliqua labore in et est esse.',
  },
  {
    label: 'Jesse Garner',
    value: 'Jesse Garner',
    description: 'Do tempor enim consequat est amet occaecat duis.',
  },
]

// eslint-disable-next-line react/display-name
export default function () {
  const limit = 5
  const [options, setOptions] = useState(data.slice(0, limit))
  const [isLoading, setIsLoading] = useState(false)
  const [value, setValue] = useState<string[]>([])

  const timeoutRef = useRef<any>(null)

  const handleLoadOptions = useCallback(
    ({ additional, text }: { additional?: boolean; text?: string } = {}) => {
      setIsLoading(true)
      if (timeoutRef.current) {
        clearTimeout(timeoutRef.current)
      }
      if (text) {
        timeoutRef.current = setTimeout(() => {
          let newOptions: any = []

          if (additional) {
            newOptions = [
              ...options,
              ...data
                .filter((option) => option.label.includes(text))
                .slice(options.length, options.length + limit),
            ]
          } else {
            newOptions = data
              .filter((option) => option.label.includes(text))
              .slice(0, limit)
          }

          setOptions(newOptions)
          setIsLoading(false)
        }, 2000)
      } else if (additional) {
        timeoutRef.current = setTimeout(() => {
          setOptions([
            ...options,
            ...data.slice(options.length, options.length + limit),
          ])
          setIsLoading(false)
        }, 1000)
      } else {
        setOptions(data.slice(0, limit))
        setIsLoading(false)
      }
    },
    [options],
  )

  return (
    <div className="container mx-lg px-base">
      <MultiSelect
        maxHeight={false}
        label="Example MultiSelect"
        loading={isLoading}
        loadOptions={handleLoadOptions}
        onChangeValue={setValue}
        options={options}
        optionValueExtractor={(option) => option.value}
        renderOption={(option) => (
          <div className="ml-base">
            <p className="mb-0 pb-0 text-base font-bold text-color-neutral-dark">
              {option.label}
            </p>
            <span className="text-sm text-neutral">
              {option.description as string}
            </span>
          </div>
        )}
        value={value}
      />
    </div>
  )
}
