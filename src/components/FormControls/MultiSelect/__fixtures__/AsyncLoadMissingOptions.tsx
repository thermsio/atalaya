import React, { useCallback, useRef, useState } from 'react'
import { MultiSelect } from '../MultiSelect'
import { SelectOption } from '../../types'

const data = new Array(1000).fill(null).map((_, i) => {
  return { label: `Item ${i}`, value: `Item ${i}` }
})

// eslint-disable-next-line react/display-name
export default function () {
  const limit = 250
  const [options, setOptions] = useState(data.slice(0, limit))
  const [isLoading, setIsLoading] = useState(false)
  const [value, setValue] = useState<string[]>([
    'Cordelia Luna', // does not exist in data list
    'Harry Potter', // does not exist in data list
    'Item 150',
    'Item 151',
  ])

  const timeoutRef = useRef<any>(null)

  const handleLoadOptions = useCallback(
    ({ additional, text }: { additional?: boolean; text?: string } = {}) => {
      setIsLoading(true)
      if (timeoutRef.current) {
        clearTimeout(timeoutRef.current)
      }
      if (text) {
        timeoutRef.current = setTimeout(() => {
          let newOptions: any = []

          if (additional) {
            newOptions = [
              ...options,
              ...data
                .filter((option) => option.label.includes(text))
                .slice(options.length, options.length + limit),
            ]
          } else {
            newOptions = data
              .filter((option) => option.label.includes(text))
              .slice(0, limit)
          }

          setOptions(newOptions)
          setIsLoading(false)
        }, 2000)
      } else if (additional) {
        timeoutRef.current = setTimeout(() => {
          setOptions([
            ...options,
            ...data.slice(options.length, options.length + limit),
          ])
          setIsLoading(false)
        }, 1000)
      } else {
        setOptions(data.slice(0, limit))
        setIsLoading(false)
      }
    },
    [options],
  )

  const handleLoadMissingOptions = (values: string[]) => {
    const missing: any = values
      .map((val) => {
        return data.find((option) => option.value === val)
      })
      .filter((x) => x !== undefined)
      .filter(
        (missingOption) =>
          !options.find((option) => option.value === missingOption?.value),
      )

    setOptions((_options) => [..._options, ...missing])
  }

  return (
    <div className="container mx-lg px-base">
      <MultiSelect
        loadAllOptions={() => {
          setOptions(data)
          return data
        }}
        maxHeight={200}
        label="Example MultiSelect"
        loading={isLoading}
        loadOptions={handleLoadOptions}
        loadMissingOptions={handleLoadMissingOptions}
        onChangeValue={setValue}
        options={options}
        optionValueExtractor={(option: SelectOption) => option.value}
        renderOption={(option: SelectOption) => (
          <div className="ml-base">
            <p className="mb-0 pb-0 text-base font-bold text-color-neutral-dark">
              {option.label}
            </p>
            <span className="text-sm text-neutral">
              {option.description as string}
            </span>
          </div>
        )}
        value={value}
        totalOptions={data.length}
      />
    </div>
  )
}
