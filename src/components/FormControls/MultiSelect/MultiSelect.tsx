import React, {
  ReactElement,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react'
import {
  FormControlWrapper,
  FormControlWrapperPublicProps,
} from '../../FormLayout/components/FormControlWrapper'
import { FormControlCommonProps, SelectOption } from '../types'
import { createMockReactSyntheticEvent } from '../../../utils/mock-react-synthetic-event'
import { Stack } from '../../../layout/Stack'
import { Button } from '../../Button'
import { Tabs } from '../../Tabs'
import { Text } from '../Text/Text'
import { SkeletonText } from '../../Skeleton/components/SkeletonText'
import { SelectedOptionsList } from './components/SelectedOptionsList'
import { OptionsList } from './components/OptionsList'
import { useStateDebounced } from '../../../hooks/useStateDebounced'
import { Loading } from '../../Loading'
import { Inline } from '../../../layout/Inline'
import { useUpdateEffect } from 'ahooks'
import { Icon } from '../../Icon'

export type InternalSelectOption<Option extends SelectOption = SelectOption> =
  Option & {
    /** Internal state if the Option is missing in the props.options list  */
    _missingOption?: boolean
    /** Internal state if the Option is in the selected list (ie: props.values) */
    _selected?: boolean
    /** Internal state of the "value" of each Option after running optional props.optionValueExtractor() */
    _value?: any
  }

export interface MultiSelectProps<Option extends SelectOption = SelectOption>
  extends FormControlCommonProps,
    FormControlWrapperPublicProps {
  /* Optional func for clearing current selection */
  onClearSelection?: () => void
  /** When true, will remove the 'Select All Options' button. Default is false */
  disableSelectAll?: boolean
  label?: string
  loading?: boolean
  /** Callback for fetching options for pagination (infinity scroll) or user input text search */
  loadOptions?: (options?: { additional?: boolean; text?: string }) => void
  /** Callback for fetching options that are included in the value prop but not found in the initial options list */
  loadMissingOptions?: (values: Option['value'][]) => void | Promise<Option[]>
  /** By default, the max-height is restricted with overflow-y-scroll. If `false` no max-height will be set, otherwise, a valid CSS height value can be passed, ie: "300px" */
  maxHeight?: false | number | string | true
  onChangeValue?: (value: string[]) => void
  /** Callback for fetching options with no pagination limit to get all possible options when Select All is clicked  */
  loadAllOptions?: () => Option[] | Promise<Option[]>
  /** This is an optional prop same as <Select /> */
  optionLabelExtractor?: (option) => string
  /** This is an optional prop same as <Select /> */
  optionValueExtractor?: (option: Option) => string
  options: Option[]
  /** The label to use for "options" description text, defaults to "Options" */
  optionsLabel?: ReactElement | string
  /* Placeholder text for the search bar */
  placeholder?: string
  /** This is an optional render func, it allows us to customize how each option looks inside the component. If this is not provided then `option.label` is used to display each option. */
  renderOption?: (option: Option, options?: Option[]) => React.ReactNode
  totalOptions?: number
  value: Option['value'][]
}

const MultiSelect = <Option extends SelectOption>({
  disabled,
  onClearSelection,
  error,
  disableSelectAll = false,
  hint,
  label,
  labelBehavior,
  loading,
  loadOptions,
  loadMissingOptions,
  maxHeight = true,
  name,
  onBlur,
  onChange,
  onChangeValue,
  onFocus,
  loadAllOptions,
  optionLabelExtractor,
  optionValueExtractor,
  options,
  optionsLabel,
  placeholder,
  renderOption,
  subText,
  showErrorMessage,
  totalOptions,
  value = [],
}: MultiSelectProps<Option>) => {
  const [previousOptions, setPreviousOptions] = useState<SelectOption[]>([])
  const local = useRef<{
    loadMissingOptionsTimeout?: any
    // track what "missing options" values have already been called
    // missedOptionsCalled: Set<SelectOption['value']>
    scrollTimeout?: any
  }>({
    // missedOptionsCalled: new Set()
  })

  const optionsContainerRef = useRef<HTMLDivElement>(null)
  const [textValue, text, setText] = useStateDebounced('', 300)
  const [tab, setTab] = useState<string | 'selected' | 'options'>('options')
  const [allOptionsLoaded, setAllOptionsLoaded] = useState(
    options?.length === totalOptions,
  )
  const allSelected = options.length === value.length

  const buildInternalData = (
    options: Option[],
    value?: Option['value'][],
  ): {
    internalOptions: InternalSelectOption<Option>[]
    optionsByValue: Record<string, InternalSelectOption<Option>>
    selectedOptions: InternalSelectOption<Option>[]
    missingOptionValues: any[]
  } => {
    const _internalOptionsByValue = options.reduce<
      Record<string, InternalSelectOption<Option>>
    >((acc, option) => {
      const _internalOption: InternalSelectOption<Option> = { ...option }

      _internalOption.label = optionLabelExtractor
        ? optionLabelExtractor(_internalOption)
        : _internalOption.label

      _internalOption._value = optionValueExtractor
        ? optionValueExtractor(_internalOption)
        : _internalOption.value

      acc[_internalOption._value] = _internalOption

      return acc
    }, {})

    const _selectedOptions: InternalSelectOption<any>[] = []
    const _missingOptionValues: SelectOption['value'][] = []

    value?.forEach((value) => {
      if (!_internalOptionsByValue[value]) {
        _missingOptionValues.push(value)

        _selectedOptions.push({
          label: '',
          missingOption: true,
          selected: true,
          value,
        })
      } else if (_internalOptionsByValue[value]) {
        _internalOptionsByValue[value]._selected = true

        _selectedOptions.push(_internalOptionsByValue[value])
      }
    })

    return {
      internalOptions: Object.values(_internalOptionsByValue),
      optionsByValue: _internalOptionsByValue,
      selectedOptions: _selectedOptions,
      missingOptionValues: _missingOptionValues,
    }
  }

  const [
    { internalOptions, selectedOptions, missingOptionValues },
    setInternalData,
  ] = useState<{
    internalOptions: InternalSelectOption<Option>[]
    optionsByValue: Record<string, InternalSelectOption<Option>>
    selectedOptions: InternalSelectOption<Option>[]
    missingOptionValues: any[]
  }>(() => buildInternalData(options, value))

  const handleMissedOptions = (values: Option['value'][]) => {
    if (loadMissingOptions && values.length) {
      clearTimeout(local.current.loadMissingOptionsTimeout)

      local.current.loadMissingOptionsTimeout = setTimeout(() => {
        // loadMissingOptions() can optionally return the missing options from a Promise that we can merge
        loadMissingOptions(values)?.then((options) => {
          setInternalData((prev) =>
            buildInternalData([...options, ...prev.internalOptions], value),
          )
        })
      }, 100)
    }
  }

  useEffect(() => {
    if (options?.length) {
      setAllOptionsLoaded(options.length === totalOptions)
    }
  }, [options?.length, totalOptions])

  useEffect(() => {
    if (missingOptionValues.length && tab === 'selected') {
      handleMissedOptions(missingOptionValues)
    }
  }, [missingOptionValues, tab])

  useUpdateEffect(() => {
    setInternalData(buildInternalData(options, value))
  }, [options, value])

  useUpdateEffect(() => {
    if (loadOptions) {
      loadOptions({ text })
    }
  }, [text])

  const handleLoadMoreOptions = () => {
    if (
      tab === 'options' &&
      loadOptions &&
      !allOptionsLoaded &&
      JSON.stringify(options) !== JSON.stringify(previousOptions)
    ) {
      setPreviousOptions(options)
      loadOptions({ additional: true, text })
    }
  }

  const handleScroll = (e) => {
    if (onFocus) onFocus(e)

    if (local.current) {
      clearTimeout(local.current.scrollTimeout)
    }

    local.current.scrollTimeout = {
      scrollTimeout: window.setTimeout(() => {
        if (onBlur) onBlur(e)
      }, 500),
    }

    const threshold = e.target.scrollHeight - e.target.clientHeight - 100

    const shouldLoadMoreOptions = e.target.scrollTop >= threshold

    if (shouldLoadMoreOptions) {
      handleLoadMoreOptions()
    }
  }

  const handleChange = (value) => {
    if (onChangeValue) {
      onChangeValue(value)
    }

    if (onChange) {
      onChange(
        // @ts-ignore
        createMockReactSyntheticEvent({
          value: value,
        }),
      )
    }
  }

  const handleSelectOption = (option: InternalSelectOption<Option>) => {
    let newValue: SelectOption['value'][] = []

    if (option._selected) {
      newValue = value.filter((_value) => _value !== option._value)

      if (!newValue.length) setTab('options')
    } else {
      newValue = [...value, option._value]
    }

    handleChange(newValue)
  }

  const handleSelectAll = async () => {
    const newValue: SelectOption['value'][] = []
    let _options = options

    if (loadAllOptions) {
      _options = await loadAllOptions()
    }

    _options.map((_option) => {
      if (optionValueExtractor) _option.value = optionValueExtractor(_option)

      newValue.push(_option.value)
    })

    handleChange(newValue)
  }

  const handleUnSelectAll = () => handleChange([])

  const showLoadMoreOptionsAtBottom = useMemo(() => {
    if (!optionsContainerRef.current) return true

    return (
      optionsContainerRef.current.clientHeight ===
      optionsContainerRef.current.scrollHeight
    )
  }, [options, optionsContainerRef])

  return (
    <FormControlWrapper
      error={error}
      disabled={disabled}
      hasValue={!!value}
      hint={hint}
      inputName={name}
      label={label}
      labelBehavior={labelBehavior}
      showErrorMessage={showErrorMessage}
      subText={subText}
    >
      <div>
        <div className="mb-sm flex gap-px">
          <Inline alignX="between" alignY="center" width="full">
            <div className="grow">
              <Text
                disabled={disabled}
                onBlur={onBlur}
                onFocus={(e) => {
                  if (local.current) {
                    clearTimeout(local.current.scrollTimeout)
                  }

                  onFocus?.(e)
                }}
                onChangeValue={setText}
                placeholder={placeholder}
                search
                subText={subText}
                value={textValue}
              />
            </div>

            {loading && (
              <div className="mx-xs">
                <Loading />
              </div>
            )}
          </Inline>

          {!!onClearSelection && (
            <Button
              disabled={disabled}
              onClick={(e) => {
                onFocus?.(e)
                onClearSelection()
                onBlur?.(e)
              }}
              size="sm"
              subtle
              variant="critical"
            >
              Clear selection
            </Button>
          )}
        </div>

        <Tabs
          currentKey={tab}
          fullWidth
          initialTabKey={tab}
          onTabClick={setTab}
          pills
        >
          <Tabs.Tab
            tabKey="options"
            title={
              <div>
                {optionsLabel || 'Options'} ({options?.length || 0}
                {!!totalOptions && ` of ${totalOptions}`})
              </div>
            }
          >
            <div className="mb-sm space-x-sm">
              {!disableSelectAll && !allSelected && (
                <Button
                  variant="neutral"
                  size="sm"
                  subtle
                  onClick={handleSelectAll}
                >
                  <Icon className="mr-xxs" size="sm">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      fill="currentColor"
                    >
                      <path d="M0 0h24v24H0V0z" fill="none" />
                      <path d="M3 5h2V3c-1.1 0-2 .9-2 2zm0 8h2v-2H3v2zm4 8h2v-2H7v2zM3 9h2V7H3v2zm10-6h-2v2h2V3zm6 0v2h2c0-1.1-.9-2-2-2zM5 21v-2H3c0 1.1.9 2 2 2zm-2-4h2v-2H3v2zM9 3H7v2h2V3zm2 18h2v-2h-2v2zm8-8h2v-2h-2v2zm0 8c1.1 0 2-.9 2-2h-2v2zm0-12h2V7h-2v2zm0 8h2v-2h-2v2zm-4 4h2v-2h-2v2zm0-16h2V3h-2v2zM8 17h8c.55 0 1-.45 1-1V8c0-.55-.45-1-1-1H8c-.55 0-1 .45-1 1v8c0 .55.45 1 1 1zm1-8h6v6H9V9z" />
                    </svg>
                  </Icon>
                  <span>Select All</span>
                </Button>
              )}

              {!!selectedOptions.length && (
                <Button
                  variant="neutral"
                  size="sm"
                  subtle
                  onClick={handleUnSelectAll}
                >
                  <Icon className="mr-xxs" size="sm">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      fill="currentColor"
                    >
                      <path d="M0 0h24v24H0V0z" fill="none" />
                      <path d="M18.3 5.71c-.39-.39-1.02-.39-1.41 0L12 10.59 7.11 5.7c-.39-.39-1.02-.39-1.41 0-.39.39-.39 1.02 0 1.41L10.59 12 5.7 16.89c-.39.39-.39 1.02 0 1.41.39.39 1.02.39 1.41 0L12 13.41l4.89 4.89c.39.39 1.02.39 1.41 0 .39-.39.39-1.02 0-1.41L13.41 12l4.89-4.89c.38-.38.38-1.02 0-1.4z" />
                    </svg>
                  </Icon>
                  <span>Clear Selected</span>
                </Button>
              )}
            </div>

            <div
              onScroll={handleScroll}
              className={`${
                maxHeight === true ? 'max-h-96' : ''
              } overflow-y-auto overflow-x-hidden`}
              ref={optionsContainerRef}
              style={typeof maxHeight !== 'boolean' ? { maxHeight } : {}}
            >
              <OptionsList
                disabled={disabled}
                internalOptions={internalOptions}
                onBlur={onBlur}
                onFocus={onFocus}
                allSelected={allSelected}
                onSelectOptionChange={handleSelectOption}
                renderOption={renderOption}
              />

              {showLoadMoreOptionsAtBottom &&
                !allOptionsLoaded &&
                previousOptions.length !== options.length &&
                (loading ? (
                  <div className="mt-sm w-full text-center">
                    <em className=" text-color-neutral-faded ">
                      Loading options <Loading size="sm" />
                    </em>
                  </div>
                ) : (
                  <div className="my-base">
                    <Button
                      fullWidth
                      onClick={handleLoadMoreOptions}
                      subtle
                      variant="neutral"
                    >
                      Load more options
                    </Button>
                  </div>
                ))}

              {loading && (
                <Stack paddingX="base" paddingY="xs">
                  <SkeletonText lines={3} />
                </Stack>
              )}
            </div>
          </Tabs.Tab>

          <Tabs.Tab
            disabled={!(Array.isArray(value) && value.length)}
            tabKey="selected"
            title={
              <div>
                Selected {value?.length > 0 && <span>({value.length})</span>}
              </div>
            }
          >
            <div
              onScroll={handleScroll}
              className={`${
                maxHeight === true ? 'max-h-96' : ''
              } overflow-y-auto overflow-x-hidden`}
              style={typeof maxHeight !== 'boolean' ? { maxHeight } : {}}
            >
              {Array.isArray(value) && !!value.length && (
                <SelectedOptionsList
                  disabled={disabled}
                  onUnSelectAll={() => {
                    handleUnSelectAll()
                    setTab('options')
                  }}
                  onSelectOptionChange={handleSelectOption}
                  options={options}
                  renderOption={renderOption}
                  selectedOptions={selectedOptions}
                  value={value}
                />
              )}
            </div>
          </Tabs.Tab>
        </Tabs>
      </div>
    </FormControlWrapper>
  )
}

export { MultiSelect }
