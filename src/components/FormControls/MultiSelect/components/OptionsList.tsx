import React from 'react'
import { InternalSelectOption } from '../MultiSelect'
import { Checkbox } from '../../Checkbox/Checkbox'
import { FormControlCommonProps } from '../../types'
import classNames from 'classnames'

type OptionsListProps<Option extends InternalSelectOption> = {
  allSelected: boolean
  disabled?: boolean
  onSelectOptionChange: (option: Option) => void
  internalOptions: InternalSelectOption<Option>[]
  renderOption?: (option: Option, options?: Option[]) => React.ReactNode
} & Pick<FormControlCommonProps, 'onBlur' | 'onFocus'>

const OptionsList = <Option extends InternalSelectOption>({
  allSelected,
  disabled,
  onBlur,
  onFocus,
  onSelectOptionChange,
  internalOptions,
  renderOption,
}: OptionsListProps<Option>) => {
  return (
    <>
      {internalOptions?.map((option) => {
        const onClick = () => {
          if (!disabled) {
            onSelectOptionChange(option)
          }
        }

        return (
          <div
            className={classNames(
              'flex  items-center px-sm py-0.5 pt-1 hover:bg-surface-subtle',
              {
                'cursor-pointer': !disabled,
                'cursor-not-allowed': disabled,
              },
            )}
            key={option._value}
            onClick={onClick}
          >
            <div className="mr-base">
              <Checkbox
                disabled={disabled}
                active={!!option._selected || allSelected}
                onBlur={onBlur}
                onFocus={onFocus}
              />
            </div>
            <div className="flex-grow overflow-hidden truncate text-ellipsis">
              {renderOption
                ? renderOption(option, internalOptions)
                : option.label}
            </div>
          </div>
        )
      })}
    </>
  )
}

export { OptionsList }
