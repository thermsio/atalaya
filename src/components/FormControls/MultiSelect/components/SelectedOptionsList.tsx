import React from 'react'
import { Button } from '../../../Button'
import { Icon } from '../../../Icon'
import { InternalSelectOption } from '../MultiSelect'
import { Skeleton } from '../../../Skeleton'
import { SelectOption } from '../../types'
import { Inline } from '../../../../layout/Inline'

interface SelectedOptionsListProps<Option extends InternalSelectOption> {
  disabled?: boolean
  onSelectOptionChange: (option: InternalSelectOption<Option>) => void
  onUnSelectAll?: () => void
  options: Option[]
  renderOption?: (option: Option, options?: Option[]) => React.ReactNode
  selectedOptions: SelectOption[]
  value: any[]
}

const SelectedOptionsList = <Option extends InternalSelectOption>({
  disabled,
  onSelectOptionChange,
  onUnSelectAll,
  options,
  renderOption,
  selectedOptions,
}: SelectedOptionsListProps<Option>) => {
  return (
    <>
      {onUnSelectAll && (
        <Inline alignX="center" padding="sm" width="full">
          <Button
            disabled={disabled}
            onClick={() => {
              if (onUnSelectAll) onUnSelectAll()
            }}
            size="sm"
            subtle
            variant="critical"
          >
            Unselect All Options
          </Button>
        </Inline>
      )}

      {selectedOptions?.map((option, i) => (
        <div
          className={'flex py-0.5 pt-1 hover:bg-surface-subtle'}
          key={`${option.value}-${i}-selected`}
        >
          <div className="flex-grow overflow-hidden truncate text-ellipsis">
            {option.missingOption && !option.label ? (
              <Skeleton.Text />
            ) : renderOption ? (
              renderOption(option as Option, options)
            ) : (
              option.label
            )}
          </div>

          <Button
            disabled={disabled}
            onClick={() => onSelectOptionChange(option as Option)}
            size="sm"
            subtle
            variant="critical"
          >
            <span className="sr-only">Remove</span>
            <Icon>
              <svg
                className="h-6 w-6"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                />
              </svg>
            </Icon>
          </Button>
        </div>
      ))}
    </>
  )
}

export { SelectedOptionsList }
