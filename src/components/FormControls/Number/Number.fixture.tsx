import React, { useState } from 'react'
import { Number } from './Number'

export default function NumberFixture() {
  const [value, setValue] = useState<number | undefined | string>('')
  console.log(value)
  return (
    <Number
      value={value}
      label="Number Field"
      onChangeInteger={(val) => setValue(val)}
      placeholder="XX"
      subText={typeof value + ' ' + value}
    />
  )
}
