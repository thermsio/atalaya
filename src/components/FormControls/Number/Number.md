```jsx
<NumberField placeholder="Add a number" />
```

### With labels

```jsx
<NumberField
  label="Number Field"
  hint="Required"
  placeholder="Add a number"
  subText="Only natural numbers allowed"
/>
```

### With Error

```jsx
<NumberField
  error="There was something wrong with your numbers"
  label="Number Field"
  hint="Required"
  placeholder="Add a number"
/>
```

### Valid

```jsx
<NumberField
  label="Number Field"
  hint="Required"
  placeholder="Add a number"
  valid={true}
/>
```

### Disabled

```jsx
<NumberField
  disabled
  label="Number Field"
  hint="Required"
  placeholder="Add a number"
/>
```
