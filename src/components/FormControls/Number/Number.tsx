import React, { useCallback, useRef } from 'react'
import classNameParser from 'classnames'

import {
  FormControlWrapper,
  FormControlWrapperPublicProps,
} from '../../FormLayout/components/FormControlWrapper'
import { Button } from '../../Button'
import { Icon } from '../../Icon'
import { StatusIcons } from '../components/StatusIcons'

import { FormControlCommonProps } from '../types'
import '../FormControls.css'
import './Number.css'

export interface NumberProps
  extends FormControlCommonProps,
    FormControlWrapperPublicProps {
  max?: number
  min?: number
  /* Type-safe callback */
  onChangeInteger?: (int: number | undefined) => void
  step?: number
  showControls?: boolean
  value?: number | string
}

const changeEvent = new Event('change', { bubbles: true })

// TODO: add onMouseDown functionality so a number can keep increasing or decreasing until button is released.
const NumberField = ({
  error,
  disabled,
  hint,
  label,
  labelBehavior,
  max,
  maxWidth,
  min,
  minWidth,
  name,
  onBlur,
  onChange,
  onChangeInteger,
  onChangeValue,
  onFocus,
  placeholder,
  showErrorMessage,
  subText,
  showControls = true,
  step,
  valid,
  value,
}: NumberProps): React.ReactElement => {
  const handleOnChange = useCallback(
    (e) => {
      const value = e.target.value

      if (onChangeInteger) {
        let safeIntegerOrUndefined: number | undefined = undefined

        if (value !== '' && !Number.isNaN(+value)) {
          safeIntegerOrUndefined = +value
        }

        onChangeInteger(safeIntegerOrUndefined)
      }
      if (onChangeValue) onChangeValue(value)
      if (onChange) onChange(e)
    },
    [onChange, onChangeInteger, onChangeValue],
  )

  const handleOnFocus = useCallback(
    (e) => {
      onFocus?.(e)
      e.target.select()
    },
    [onChange, onChangeValue],
  )

  const inputRef = useRef<HTMLInputElement>(null)

  return (
    <FormControlWrapper
      error={error}
      disabled={disabled}
      hasValue={!!value}
      hint={hint}
      inputName={name}
      label={label}
      labelBehavior={labelBehavior}
      maxWidth={maxWidth}
      minWidth={minWidth}
      showErrorMessage={showErrorMessage}
      subText={subText}
    >
      <div
        className={classNameParser(
          {
            'form-field-disabled': disabled,
            'form-field-error': error,
            'form-field-success': valid,
          },
          'form-field-input-wrapper flex items-center justify-between',
        )}
      >
        {showControls && (
          <Button
            disabled={disabled}
            subtle
            onClick={() => {
              if (inputRef.current) {
                inputRef.current.stepDown()
                inputRef.current.dispatchEvent(changeEvent)
              }
            }}
          >
            <Icon size="sm">
              <svg
                className="h-6 w-6"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                  clipRule="evenodd"
                />
              </svg>
            </Icon>
          </Button>
        )}

        <input
          type="number"
          className="form-field form-field-number"
          disabled={disabled}
          max={max}
          min={min}
          step={step}
          value={value || ''}
          onBlur={onBlur}
          onChange={handleOnChange}
          onFocus={handleOnFocus}
          placeholder={placeholder}
          ref={inputRef}
        />

        {showControls && (
          <Button
            disabled={disabled}
            subtle
            onClick={() => {
              if (inputRef.current) {
                inputRef.current?.stepUp()
                inputRef.current?.dispatchEvent(changeEvent)
              }
            }}
          >
            <Icon size="sm">
              <svg
                className="h-6 w-6"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z"
                  clipRule="evenodd"
                />
              </svg>
            </Icon>
          </Button>
        )}

        {(!!error || !!valid) && (
          <>
            <div className="pr-xxs" />
            <StatusIcons error={error} valid={valid} />
          </>
        )}
      </div>
    </FormControlWrapper>
  )
}

export { NumberField as Number }
