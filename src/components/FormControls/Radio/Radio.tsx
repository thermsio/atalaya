import React, { useCallback } from 'react'

import {
  FormControlWrapper,
  FormControlWrapperPublicProps,
} from '../../FormLayout/components/FormControlWrapper'

import RadioButton from './components/RadioButton'

import { FormControlCommonProps } from '../types'
import '../FormControls.css'
import classNameParser from 'classnames'

type RadioOption = {
  label: string
  subText?: string
  value?: string | number | boolean
}

export interface RadioProps
  extends FormControlCommonProps,
    FormControlWrapperPublicProps {
  /* If no option.value provided then the "value" will be the "label" prop for each RadioOption in the options list. */
  options: RadioOption[]
  value?: string | number | boolean
  vertical?: boolean
}

const Radio = ({
  error,
  disabled,
  hint,
  label,
  labelBehavior,
  name,
  onChange,
  onBlur,
  onChangeValue,
  onFocus,
  options,
  showErrorMessage,
  subText,
  valid,
  value,
  vertical,
}: RadioProps): React.ReactElement => {
  const handleOnChange = useCallback(
    (e, index) => {
      if (onChangeValue) onChangeValue(options[index].value)
      if (onChange) onChange(e)
    },
    [onChange, onChangeValue, options],
  )

  return (
    <FormControlWrapper
      error={error}
      disabled={disabled}
      hasValue={!!value}
      hint={hint}
      label={label}
      labelBehavior={labelBehavior}
      inputName={name}
      showErrorMessage={showErrorMessage}
      subText={subText}
    >
      <div
        className={classNameParser(
          {
            'flex-col space-y-base': vertical,
            'flex-wrap space-x-base': !vertical,
          },
          'flex py-xs',
        )}
      >
        {options.map(
          ({ label, subText: optionSubText, value: optionValue }, index) => (
            <RadioButton
              active={value === optionValue}
              disabled={disabled}
              error={!!error}
              label={label}
              name={`${name}-${index}`}
              onBlur={onBlur}
              onChange={(e) => handleOnChange(e, index)}
              onFocus={onFocus}
              subText={optionSubText}
              valid={valid}
              value={optionValue !== undefined ? optionValue : label}
              key={`${label}/${index}`}
            />
          ),
        )}
      </div>
    </FormControlWrapper>
  )
}

export { Radio }
