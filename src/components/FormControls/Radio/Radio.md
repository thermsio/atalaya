```jsx
const options = [
  { label: 'Option 1', value: '1' },
  { label: 'Option 2', value: '2' },
  { label: 'Option 3', value: '3' },
  { label: 'Option 4', value: '4' },
]

const [value, setValue] = React.useState(options[0].value)

;<Radio name="a" options={options} onChangeValue={setValue} value={value} />
```

### Vertical

```jsx
const options = [
  { label: 'Option 1', value: '1' },
  { label: 'Option 2', value: '2' },
  { label: 'Option 3', value: '3' },
  { label: 'Option 4', value: '4' },
]

const [value, setValue] = React.useState(options[0].value)

;<Radio
  name="b"
  onChangeValue={setValue}
  options={options}
  value={value}
  vertical
/>
```

### With labels

```jsx
const options = [
  { label: 'Option 1', value: '1' },
  { label: 'Option 2', value: '2' },
  { label: 'Option 3', value: '3' },
  { label: 'Option 4', value: '4' },
]

const [value, setValue] = React.useState(options[0].value)

;<Radio
  hint="Required"
  label="A Single Choice"
  name="c"
  onChangeValue={setValue}
  options={options}
  subText="You will only be able to pick a single option."
  value={value}
/>
```

### With Subtext

```jsx
const options = [
  {
    label: 'Option 1',
    subText: 'This is the description of option 1',
    value: '1',
  },
  {
    label: 'Option 2',
    subText: 'This is the description of option 2',
    value: '2',
  },
  {
    label: 'Option 3',
    subText: 'This is the description of option 3',
    value: '3',
  },
  {
    label: 'Option 4',
    subText: 'This is the description of option 4',
    value: '4',
  },
]

const [value, setValue] = React.useState(options[0].value)

;<Radio
  hint="Required"
  label="A Single Choice"
  name="c"
  onChangeValue={setValue}
  options={options}
  subText="You will only be able to pick a single option."
  value={value}
  // vertical
/>
```

### With Error

```jsx
const options = [
  { label: 'Option 1', value: '1' },
  { label: 'Option 2', value: '2' },
  { label: 'Option 3', value: '3' },
  { label: 'Option 4', value: '4' },
]

const [value, setValue] = React.useState(options[0].value)

;<Radio
  error="Something went very wrong here."
  hint="Required"
  label="A Single Choice"
  name="d"
  onChangeValue={setValue}
  options={options}
  value={value}
/>
```

### Valid

```jsx
const options = [
  { label: 'Option 1', value: '1' },
  { label: 'Option 2', value: '2' },
  { label: 'Option 3', value: '3' },
  { label: 'Option 4', value: '4' },
]

const [value, setValue] = React.useState(options[0].value)

;<Radio
  hint="Required"
  label="A Single Choice"
  name="e"
  onChangeValue={setValue}
  options={options}
  valid
  value={value}
/>
```

### Disabled

```jsx
const options = [
  { label: 'Option 1', value: '1' },
  { label: 'Option 2', value: '2' },
  { label: 'Option 3', value: '3' },
  { label: 'Option 4', value: '4' },
]

const [value, setValue] = React.useState(options[0].value)

;<Radio
  disabled
  hint="Required"
  label="A Single Choice"
  name="f"
  onChangeValue={setValue}
  options={options}
  value={value}
/>
```
