import React from 'react'
import classNameParser from 'classnames'

import { Inline } from '../../../../layout/Inline'

import './RadioButton.css'
import { FormControlCommonProps } from '../../types'

interface RadioButtonProps extends FormControlCommonProps {
  active?: boolean
  error?: boolean
  label?: string
  subText?: string
  value?: string | number | boolean
}

const RadioButton = ({
  active,
  disabled,
  error,
  label,
  name,
  onBlur,
  onChange,
  onFocus,
  subText,
  valid,
  value,
}: RadioButtonProps): React.ReactElement => {
  return (
    <Inline space="xxs">
      <input
        type="radio"
        checked={active}
        className={classNameParser(
          {
            'form-field-active': active,
            'form-field-disabled': disabled,
            'form-field-error': error,
            'form-field-valid': valid,
          },
          'form-field form-radio mt-xxs',
        )}
        disabled={disabled}
        id={name}
        name={name}
        onBlur={onBlur}
        onChange={onChange}
        onFocus={onFocus}
        value={typeof value === 'boolean' ? (value ? 1 : 0) : value}
      />

      <div>
        {!!label && (
          <label
            className={classNameParser(
              { 'cursor-not-allowed text-color-subtle': disabled },
              'select-none text-sm',
            )}
            htmlFor={name}
          >
            {label}
          </label>
        )}

        {!!subText && (
          <div className="max-w-xs text-sm text-color-subtle">{subText}</div>
        )}
      </div>
    </Inline>
  )
}

export default RadioButton
