import React, { useState } from 'react'
import { Radio } from './Radio'

const basic = () => (
  <div style={{ border: '1px solid red', width: '300px' }}>
    <Radio
      options={[
        'Yes',
        'No',
        'n/a',
        'REALLY REALLY REALLY long option',
        'REALLY REALLY REALLY long option',
      ].map((str) => ({ label: str, value: str }))}
    />
  </div>
)

const booleanValues = () => {
  const [value, setValue] = useState(false)

  return (
    <div style={{ border: '1px solid red', width: '300px' }}>
      <Radio
        options={[
          { label: 'True', value: true },
          { label: 'False', value: false },
        ]}
        subText={`Selected value: ${value}`}
        value={value}
        onChangeValue={setValue}
      />
    </div>
  )
}

export default {
  basic,
  booleanValues,
}
