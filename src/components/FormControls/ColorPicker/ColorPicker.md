```jsx
const [color, setColor] = React.useState('')

;<ColorPicker value={color} onChangeValue={setColor} />
```

### With labels

```jsx
const [color, setColor] = React.useState('')

;<ColorPicker
  label="Color Picker Field"
  hint="Colors!"
  subText="Any Color"
  value={color}
  onChangeValue={setColor}
/>
```

### With Error

```jsx
const [color, setColor] = React.useState('')

;<ColorPicker
  error="Wrong Color"
  label="Color Picker Field"
  hint="Colors!"
  value={color}
  onChangeValue={setColor}
/>
```

### Valid

```jsx
const [color, setColor] = React.useState('')

;<ColorPicker valid={true} value={color} onChangeValue={setColor} />
```

### Disabled

```jsx
const [color, setColor] = React.useState('')

;<ColorPicker
  disabled
  label="Color Picker"
  hint="Colors!"
  value={color}
  onChangeValue={setColor}
/>
```
