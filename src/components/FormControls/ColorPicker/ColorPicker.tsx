import React, { useCallback, useEffect, useRef, useState } from 'react'
import { SketchPicker } from 'react-color'
import { useOnClickOutside } from '../../../hooks/useOnClickOutside'
import { Button } from '../../Button'
import {
  FormControlWrapper,
  FormControlWrapperPublicProps,
} from '../../FormLayout/components/FormControlWrapper'
import { FormControlCommonProps } from '../types'
import '../FormControls.css'
import './ColorPicker.css'
import { usePrevious } from '../../../hooks/usePrevious'
import { mockReactSyntheticEvent } from '../../../utils/mock-react-synthetic-event'
import { DropdownContainer } from '../../DropdownContainer'

export interface ColorPickerProps
  extends FormControlCommonProps,
    FormControlWrapperPublicProps {
  onBlur?: (e: React.BaseSyntheticEvent) => void
  onFocus?: (e: React.BaseSyntheticEvent) => void
  value?: string
}

const ColorPicker = ({
  error,
  disabled,
  hint,
  label,
  labelBehavior,
  name,
  onBlur,
  onFocus,
  onChange,
  onChangeValue,
  showErrorMessage,
  subText,
  value,
}: ColorPickerProps): React.ReactElement => {
  const [isOpen, setIsOpen] = useState(false)
  const prevIsOpen = usePrevious(isOpen)

  const ref = useRef<HTMLDivElement>(null)

  const handleOnChange = useCallback(
    (color) => {
      if (onChangeValue) onChangeValue(color.hex)
      if (onChange)
        onChange({
          target: {
            value: color.hex,
          },
        } as React.ChangeEvent<HTMLInputElement>)
    },
    [onChange, onChangeValue],
  )

  useEffect(() => {
    if (isOpen && onFocus) {
      onFocus(mockReactSyntheticEvent)
    } else if (prevIsOpen && !isOpen && onBlur) {
      onBlur(mockReactSyntheticEvent)
    }
  }, [isOpen, onBlur, onFocus])

  const clickOutsideHandler = useCallback(() => setIsOpen(false), [])

  useOnClickOutside(ref, clickOutsideHandler)

  return (
    <FormControlWrapper
      error={error}
      disabled={disabled}
      hasValue={!!value}
      hint={hint}
      inputName={name}
      label={label}
      labelBehavior={labelBehavior}
      showErrorMessage={showErrorMessage}
      subText={subText}
    >
      <div className="relative" ref={ref}>
        <Button disabled={disabled} onClick={() => setIsOpen(!isOpen)} subtle>
          <div
            className="rounded p-sm"
            style={{ backgroundColor: value || '#798386' }}
          />
        </Button>

        {isOpen && (
          <DropdownContainer
            containerRef={ref as React.MutableRefObject<HTMLDivElement>}
            onClose={clickOutsideHandler}
          >
            <SketchPicker
              className="form-field-color-picker"
              // this is necessary because if a non-string value (ie: null) is passed then it throws an error
              color={typeof value === 'string' ? value : ''}
              disableAlpha
              onChange={handleOnChange}
            />
          </DropdownContainer>
        )}
      </div>
    </FormControlWrapper>
  )
}

export { ColorPicker }
