```jsx
<Password placeholder="Password" />
```

### With labels

```jsx
<Password
  label="Password Field"
  hint="Required"
  placeholder="Password"
  subText="Only latin characters allowed."
/>
```

### With Error

```jsx
<Password
  error="There is something very wrong with this input."
  label="Password Field"
  hint="Required"
  placeholder="Password"
/>
```

### Valid

```jsx
<Password
  label="Password Field"
  hint="Required"
  placeholder="Password"
  valid={true}
/>
```

### Disabled

```jsx
<Password
  disabled
  label="Password Field"
  hint="Required"
  placeholder="Password"
  value="This is my password"
/>
```
