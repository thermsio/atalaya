import React, { useCallback, useMemo, useRef } from 'react'
import { useElementSize } from '../../../hooks/useElementSize'
import ReactSelect, {
  ActionMeta,
  GroupBase,
  Props as ReactSelectProps,
  StylesConfig,
} from 'react-select'
import ReactSelectCreatable from 'react-select/creatable'
import {
  FormControlWrapper,
  FormControlWrapperPublicProps,
} from '../../FormLayout/components/FormControlWrapper'
import { FormControlCommonProps, SelectOption } from '../types'
import '../FormControls.css'
import './Select.css'
import { createMockReactSyntheticEvent } from '../../../utils/mock-react-synthetic-event'
import {
  ExtractorType,
  getCustomComponents,
  parseLoadingMessage,
} from './utils'
import { CustomMenuListProps } from './components/CustomMenuList'
import { FilterOptionOption } from 'react-select/dist/declarations/src/filters'
import {ZIndex} from "../../../constants/ZIndex";

type SelectPropsBase<Options extends Record<any, any>[]> =
  FormControlCommonProps &
    FormControlWrapperPublicProps & {
      /** Allow the user to create a new option that does not exist in the options list */
      allowCreate?: boolean
      /** close dropdown menu when selecting an option. Defaults to true on single and false on multi.   */
      closeMenuOnSelect?: boolean
      /** Show only the number of selected items. Selected items separated in list of options */
      combineSelected?: boolean
      /** Function that overrides default filtering logic when isSearchable is true, accepts an Option and should return either true or false to determine which options should be shown. Can also be set to null disable filtering. */
      filterOption?:
        | ((
            option: FilterOptionOption<Options[number]>,
            inputValue: string,
          ) => boolean)
        | null
      /** Hide selected options from the list of options. Default: true */
      hideSelectedOptions?: boolean
      /** Do not render selected values inside input. Default: false */
      hideValue?: boolean
      /** Allows to clear the selected value. */
      isClearable?: boolean
      /** Will show loading indicators */
      isLoading?: boolean
      /** Override the built-in logic to detect whether an option is disabled */
      isOptionDisabled?: (option: Options[number]) => boolean
      /** Lets you type on the select field to filter results */
      isSearchable?: boolean
      /** Callback for fetching options from remote */
      loadOptions?: (options?: { additional?: boolean; text?: string }) => void
      /** Message to display when loading options */
      loadingMessage?: ExtractorType
      /** The max height of options container */
      maxMenuHeight?: number
      /** Menu stays open at all times */
      menuIsOpen?: boolean
      /** Defaults to fixed for Modals compatibility. In cases where menu overlaps component, it should be set to absolute. */
      menuPosition?: 'absolute' | 'fixed'
      /** Custom & Tabbed Menu will be absolutely positioned. Default: false */
      menuOverlay?: boolean
      noOptionMessage?:
        | ((obj: { inputValue: string }) => React.ReactNode)
        | undefined
      /** Callback when a new option is created in the selected value, requires prop "allowCreate=true" */
      onCreateOption?: (option: SelectOption) => void
      /** Available options list */
      options: Options
      /** Resolves option data to a string to be displayed as the label by components */
      optionLabelExtractor?: (option: Options[number]) => React.ReactNode
      /** Resolves option data to a string to compare options */
      optionValueExtractor?: (option: Options[number]) => any
      /** Optionally provide a custom menu container for the Select options when the menu is open */
      RenderMenuList?: CustomMenuListProps['CustomComponent']
      /** The component will be used to render list items options */
      RenderGroupHeading?: (group: GroupBase<any>) => React.ReactNode
      /** The component will be used to render list items options.  */
      RenderListItem?: (
        option: Options[number],
        state?: {
          isDisabled?: boolean
          isFocused?: boolean
          isSelected?: boolean
        },
        selectOption?: (newValue: unknown) => void,
      ) => React.ReactNode
      /** The component will be used to render selected list items options. */
      RenderSelectedListItem?: (
        option: any,
        state?: {
          isDisabled?: boolean
          isFocused?: boolean
          isSelected?: boolean
        },
        selectOption?: (newValue: unknown) => void,
      ) => React.ReactNode
      /** The component will be used to render a selected option when `multi` is true */
      RenderSelectedMulti?: (
        option: Options[number],
        onRemove?: React.MouseEventHandler,
      ) => React.ReactElement
      /** The component will be used to render a single selected option */
      RenderSelectedSingle?: (option: Options[number]) => React.ReactNode
      /** The component will be used when there are no options available  */
      RenderNoOptions?: string | (() => React.ReactNode)
      /** Style modifier methods */
      styles?: StylesConfig
      /** Place all options and selected options in separate tabs */
      tabbed?: boolean
    }

export type SelectProps<Options extends Record<any, any>[] = any> =
  SelectPropsBase<Options> &
    (
      | {
          /** Allows for multiple value selection */
          multi?: false | never
          /** Callback when the selected options change */
          onChangeOptions?: (selectedOptions: Options[number]) => void
          /** Callback when the selected value changes */
          onChangeValue?: (selectedValue: any) => void
          /** Currently selected option */
          value?: any
        }
      | {
          /** Allows for multiple value selection */
          multi: true
          /** Callback when the selected options change */
          onChangeOptions?: (selectedOptions: Options) => void
          /** Callback when the selected value changes */
          onChangeValue?: (selectedValue: any) => void
          /** Currently selected option */
          value?: any
        }
    )

const Select = <Options extends Record<any, any>[]>({
  allowCreate,
  closeMenuOnSelect,
  combineSelected,
  error,
  disabled,
  filterOption,
  hideSelectedOptions,
  hideValue,
  hint,
  isClearable,
  isLoading,
  isOptionDisabled,
  isSearchable,
  label,
  labelBehavior,
  loadingMessage,
  loadOptions,
  maxMenuHeight,
  maxWidth,
  menuIsOpen,
  menuOverlay: _menuOverlay,
  menuPosition = 'fixed',
  minWidth,
  multi,
  name,
  noOptionMessage,
  onBlur,
  onChange,
  onChangeOptions,
  onChangeValue,
  onCreateOption,
  onFocus,
  options,
  optionLabelExtractor,
  optionValueExtractor,
  placeholder,
  RenderMenuList,
  RenderGroupHeading,
  RenderListItem,
  RenderSelectedListItem,
  RenderSelectedMulti,
  RenderSelectedSingle,
  RenderNoOptions,
  styles,
  showErrorMessage,
  subText,
  tabbed,
  value,
}: SelectProps<Options>): React.ReactElement => {
  const [{ width }, selectRef] = useElementSize()

  // hack-fix for <Select /> component light css styles being applied.
  // this will be removed when this task is implemented: https://americansoftware.atlassian.net/browse/CORE-2213
  const menuOverlay = menuIsOpen && combineSelected ? false : _menuOverlay
  closeMenuOnSelect ??= !multi

  const SelectComponents = useMemo(
    () =>
      getCustomComponents({
        combineSelected,
        ListItem: RenderListItem,
        NoOptions: RenderNoOptions,
        MenuList: RenderMenuList,
        SelectedListItem: RenderSelectedListItem,
        SelectedMulti: RenderSelectedMulti,
        SelectedSingle: RenderSelectedSingle,
        tabbed,
      }),

    [
      combineSelected,
      RenderListItem,
      RenderNoOptions,
      RenderMenuList,
      RenderSelectedMulti,
      RenderSelectedSingle,
      RenderSelectedListItem,
      tabbed,
    ],
  )

  const optionsByValue = useMemo(() => {
    const _optionsByValue = {}

    options?.forEach((option: any) => {
      if (Array.isArray(option.options)) {
        option.options.forEach((groupOption) => {
          const value = optionValueExtractor
            ? optionValueExtractor(groupOption)
            : groupOption.value

          _optionsByValue[value] = groupOption
        })
      } else {
        const value = optionValueExtractor
          ? optionValueExtractor(option)
          : option.value

        _optionsByValue[value] = option
      }
    })

    return _optionsByValue
  }, [options])

  const selectedValue = useMemo(() => {
    if (multi) {
      if (Array.isArray(value)) {
        return value.reduce((acc, _value) => {
          if (typeof _value === 'object') acc.push(_value)
          else {
            if (!optionsByValue[_value]) {
              // In a case where `allowCreate=true` the option might not exist in the `options` list
              acc.push({ label: _value, value: _value })
            } else {
              acc.push(optionsByValue[_value])
            }
          }

          return acc
        }, [])
      }
    } else if (typeof value === 'object') {
      return value
    } else if (allowCreate) {
      return { label: value, value }
    } else if (value !== undefined) {
      return optionsByValue[value] || null
    } else {
      return null
    }
  }, [optionsByValue, value])

  const handleOnChange: ReactSelectProps<any, any, any>['onChange'] = (
    option,
    actionMetadata: ActionMeta<any>,
  ) => {
    let value

    if (Array.isArray(option)) {
      value = option.map((opt) =>
        optionValueExtractor ? optionValueExtractor(opt) : opt.value,
      )
    } else if (option) {
      value = optionValueExtractor ? optionValueExtractor(option) : option.value
    } else {
      // when the Select is cleared we pass back an empty array if multi
      if (multi) {
        value = []
      } else {
        value = null
      }
    }

    if (
      actionMetadata.action === 'create-option' &&
      actionMetadata.option &&
      onCreateOption
    ) {
      onCreateOption({
        label: actionMetadata.option.label,
        value: actionMetadata.option.value,
      })

      return
    }

    // FIXME: if value is not in options list and no optionLabelExtractor exist, Select component will show the value as label, even if a label has been provided.
    if (onChangeValue) onChangeValue(value)

    if (onChangeOptions) onChangeOptions(option)

    if (onChange)
      onChange(
        // @ts-ignore
        createMockReactSyntheticEvent({
          value,
        }),
      )
  }

  // We pass any to the generic to avoid CSS type errors
  const customStyles: StylesConfig<any, any, any> = {
    menu: (provided) => ({
      ...provided,
      position:
        (RenderMenuList || tabbed) && !menuOverlay ? 'relative' : 'absolute',
      width: width || '100%',
      ...styles?.menu,
    }),
    menuPortal: (base) => ({ ...base, zIndex: ZIndex.Fixed }),
    ...styles,
  }

  const inputTextRef = useRef('')

  const handleSearchOptions = useCallback(
    (newValue, { prevInputValue }) => {
      if (loadOptions && newValue !== prevInputValue) {
        inputTextRef.current = newValue
        loadOptions({ text: newValue })
      }
    },
    [loadOptions],
  )

  const handleLoadMoreOptions = useCallback(() => {
    if (loadOptions)
      loadOptions({ additional: true, text: inputTextRef.current })
  }, [loadOptions])

  const SelectComponent = allowCreate ? ReactSelectCreatable : ReactSelect

  return (
    <FormControlWrapper
      error={error}
      disabled={disabled}
      hasValue={!!value}
      hint={hint}
      inputName={name}
      label={label}
      labelBehavior={labelBehavior}
      maxWidth={maxWidth}
      minWidth={minWidth || 100}
      showErrorMessage={showErrorMessage}
      subText={subText}
    >
      <div ref={selectRef}>
        <SelectComponent
          captureMenuScroll
          closeMenuOnSelect={closeMenuOnSelect}
          className="rs__container"
          classNamePrefix="rs"
          combineSelected={combineSelected}
          components={SelectComponents}
          filterOption={filterOption}
          // @ts-ignore
          getOptionLabel={optionLabelExtractor}
          getOptionValue={optionValueExtractor}
          hideSelectedOptions={hideSelectedOptions}
          controlShouldRenderValue={!hideValue}
          isClearable={isClearable}
          isDisabled={disabled}
          isLoading={isLoading}
          isOptionDisabled={isOptionDisabled}
          isMulti={multi}
          isSearchable={isSearchable}
          formatGroupLabel={RenderGroupHeading ? RenderGroupHeading : undefined}
          onBlur={onBlur}
          id={name}
          inputId={name}
          loadingMessage={(state) =>
            parseLoadingMessage(state.inputValue, loadingMessage)
          }
          maxMenuHeight={maxMenuHeight}
          menuIsOpen={menuIsOpen}
          menuPlacement="auto"
          menuPosition={menuPosition}
          menuShouldScrollIntoView={true}
          noOptionsMessage={noOptionMessage}
          onChange={handleOnChange}
          onFocus={onFocus}
          onInputChange={handleSearchOptions}
          onMenuScrollToBottom={handleLoadMoreOptions}
          options={options}
          placeholder={placeholder || (tabbed ? 'Search...' : undefined)}
          styles={customStyles}
          value={selectedValue}
        />
      </div>
    </FormControlWrapper>
  )
}

export { Select }
