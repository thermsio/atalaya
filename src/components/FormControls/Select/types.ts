import { MenuListProps } from 'react-select'

// MenuList doesn't seem to use `isLoading` but it gets passed as a prop by react-select anyway, we are taking advantage of this.
export interface MenuListWithLoadingProps extends MenuListProps {
  isLoading?: boolean
}
