In order to use select you need to provide an array to the `options` prop. Options can be of any type. If options are
objects the default props for them are:

- `label`: Must be a string, will be used to represent the option inside the component.
- 'value': The actual, raw, value of the option.

You are not forced to follow this shape however, you can use any kind of object you like. But if you do, in order for
Select to properly work you to provide 2 extra props: `optionLabelExtractor` and `optionValueExtractor`. These props
should be functions that accept an option object and returns a string that will represent their labels and values
respective. Note that these returned values will only be used internally by Select, any time a callback provides you
with an option, it will be in their original shape.

```jsx
const options = [
  { label: 'Option 1 ', value: '1' },
  { label: 'Option 2 ', value: '2' },
  { label: 'Option 3 ', value: '3' },
  { label: 'Option 4 ', value: '4' },
  { label: 'Option 5 ', value: '5' },
]
const [value, setValue] = React.useState()

;<Select
  onChangeValue={(value) => setValue(value)}
  options={options}
  value={value}
/>
```

### Create New Options

You can pass the prop `allowCreate` which will allow the user to create a new option in the list that does not exist in
the `options` array.

There is also an optional prop `onCreateOption(option: any)` that can be used to handle new options created.

```jsx
const [value, setValue] = React.useState()
const options = [
  { label: 'Option 1 ', value: '1' },
  { label: 'Option 2 ', value: '2' },
]

;<Select
  allowCreate
  hint="Options"
  label="Select Field"
  multi
  onChange={setValue}
  options={options}
  subText="Multiple Options can be picked"
  value={value}
/>
```

### Async

You may use `loadOptions` prop to fetch options remotely. This prop takes a callback that will be called when the user
needs to see more options. The callback will receive an object with two props:

- `additional`: Is a boolean parameter that will let you know if you need to concatenate the new options with the old
  ones. This will happen on events such as the user reaching the end of the options list. On the contrary, when the user
  is typing some search text it will be false so you now you need to discard your previous results.
- `text`: Is a string parameter that will contain the query the user has typed. You can use this to filter your results
  when fetching new options.

To let the user know when a request is taking place you can set Select's `isLoading` prop to true. This will add loading
indicators to provide feedback to the user.

```jsx
const rawOptions = [
  { label: 'Option 1', value: '1' },
  { label: 'Option 2', value: '2' },
  { label: 'Option 3', value: '3' },
  { label: 'Option 4', value: '4' },
  { label: 'Option 5', value: '5' },
  { label: 'Option 6', value: '6' },
  { label: 'Option 7', value: '7' },
  { label: 'Option 8', value: '8' },
  { label: 'Option 9', value: '9' },
  { label: 'Option 10', value: '10' },
  { label: 'Option 11', value: '11' },
  { label: 'Option 12', value: '12' },
  { label: 'Option 13', value: '13' },
  { label: 'Option 14', value: '14' },
  { label: 'Option 15', value: '15' },
]

const limit = 6
const [selected, setSelected] = React.useState([])
const [options, setOptions] = React.useState(rawOptions.slice(0, limit))
const [isLoading, setIsLoading] = React.useState(false)
const [searchText, setSearchText] = React.useState('')

const timeoutRef = React.useRef(null)

const handleLoadOptions = ({ additional, text }) => {
  setIsLoading(true)

  if (timeoutRef.current) {
    clearTimeout(timeoutRef.current)
  }

  if (text) {
    timeoutRef.current = setTimeout(() => {
      let newOptions = []

      if (additional) {
        newOptions = [
          ...options,
          ...rawOptions
            .filter((option) => option.label.includes(text))
            .slice(options.length, options.length + limit),
        ]
      } else {
        newOptions = rawOptions
          .filter((option) => option.label.includes(text))
          .slice(0, limit)
      }

      setOptions(newOptions)
      setIsLoading(false)
    }, 2000)
  } else if (additional) {
    timeoutRef.current = setTimeout(() => {
      setOptions([
        ...options,
        ...rawOptions.slice(options.length, options.length + limit),
      ])
      setIsLoading(false)
    }, 1000)
  } else {
    // initial "load" call
    setOptions(rawOptions.slice(0, limit))
    setIsLoading(false)
  }
}

;<Select
  isLoading={isLoading}
  loadOptions={handleLoadOptions}
  onChangeValue={setSelected}
  options={options}
  value={selected}
/>
```

### With labels

```jsx
const options = [
  { label: 'Option 1', value: '1' },
  { label: 'Option 2', value: '2' },
  { label: 'Option 3', value: '3' },
  { label: 'Option 4', value: '4' },
  { label: 'Option 5', value: '5' },
  { label: 'Option 6', value: '6' },
  { label: 'Option 7', value: '7' },
  { label: 'Option 8', value: '8' },
  { label: 'Option 9', value: '9' },
  { label: 'Option 10', value: '10' },
]
const [value, setValue] = React.useState(options[0])

;<Select
  hint="Options"
  label="Select Field"
  onChange={setValue}
  options={options}
  subText="Only a single value can be picked"
  value={value}
/>
```

### Error

```jsx
const [value, setValue] = React.useState()
const options = [
  { label: 'Option 1 ', value: '1' },
  { label: 'Option 2 ', value: '2' },
  { label: 'Option 3 ', value: '3' },
  { label: 'Option 4 ', value: '4' },
  { label: 'Option 5 ', value: '5' },
]

;<Select
  error="There was an error with this."
  hint="Options"
  label="Select Field"
  multi
  onChange={setValue}
  options={options}
  value={value}
/>
```

### Disabled

```jsx
const [value, setValue] = React.useState()
const options = [
  { label: 'Option 1 ', value: '1' },
  { label: 'Option 2 ', value: '2' },
  { label: 'Option 3 ', value: '3' },
  { label: 'Option 4 ', value: '4' },
  { label: 'Option 5 ', value: '5' },
]

;<Select
  disabled
  hint="Options"
  label="Select Field"
  multi
  onChange={setValue}
  options={options}
  subText="Multiple Options can be picked"
  value={value}
/>
```

### Multiple Options

```jsx
const options = [
  { label: 'Option 1 ', value: '1' },
  { label: 'Option 2 ', value: '2' },
  { label: 'Option 3 ', value: '3' },
  { label: 'Option 4 ', value: '4' },
  { label: 'Option 5 ', value: '5' },
]
const [value, setValue] = React.useState([options[0], options[1]])

;<Select
  hint="Options"
  label="Select Field"
  multi
  onChange={setValue}
  options={options}
  subText="Multiple Options can be picked"
  value={value}
/>
```

### Option Groups

You can group options in sections by changing the structure of the `options` array. Instead of placing the options
directly on the array, you can group them inside objects, each object should have two properties `label` and `options`.
Label should contain a string that will be displayed as the heading of the group. `options` should be an array of the
grouped options.

```json
[
  {
    "label": "Group A",
    "options": [
      {
        "label": "Option 1 A",
        "value": "1A"
      },
      {
        "label": "Option 2 A",
        "value": "2A"
      }
    ]
  },
  {
    "label": "Group B",
    "options": [
      {
        "label": "Option 1 B",
        "value": "1B"
      },
      {
        "label": "Option 2 B",
        "value": "2B"
      }
    ]
  }
]
```

```jsx
const groupedOptions = [
  {
    label: 'Group  A',
    options: [
      { label: 'Option 1', value: 1 },
      { label: 'Option 2', value: 2 },
      { label: 'Option 3', value: 3 },
      { label: 'Option 4', value: 4 },
      { label: 'Option 5', value: 5 },
    ],
  },
  {
    label: 'Group  B',
    options: [
      { label: 'Option 6', value: 6 },
      { label: 'Option 7', value: 7 },
      { label: 'Option 8', value: 8 },
      { label: 'Option 9', value: 9 },
      { label: 'Option 10', value: 10 },
    ],
  },
  {
    label: 'Group  C',
    options: [
      { label: 'Option 11', value: 11 },
      { label: 'Option 12', value: 12 },
      { label: 'Option 13', value: 13 },
      { label: 'Option 14', value: 14 },
      { label: 'Option 15', value: 15 },
    ],
  },
]

const [value, setValue] = React.useState()

;<div>
  <Select options={groupedOptions} onChangeValue={setValue} value={value} />
</div>
```

## Customizing Components

### RenderSelectedSingle

Using this prop you can change how a selected single value will look. It expects a function that accepts an `option`
parameters, this will contain all the selected option's data. It should return a React Element.

```jsx
const options = [
  { label: 'Option 1 ', value: '1' },
  { label: 'Option 2 ', value: '2' },
  { label: 'Option 3 ', value: '3' },
  { label: 'Option 4 ', value: '4' },
  { label: 'Option 5 ', value: '5' },
]

const [value, setValue] = React.useState([])

;<Select
  onChangeValue={setValue}
  options={options}
  RenderSelectedSingle={(option) => (
    <div className="font-size-lg text-color-caution">{option.label}</div>
  )}
  value={value}
/>
```

### RenderSelectedMulti

Using this prop you can change how a selected value will look when prop `multi` is true. It expects a function that
accepts the `option` and `onRemove`
parameters.

- `option` will contain all the selected option's data.
- `onRemove` is a function that should be called when the remove button is pressed this.

It should return a React Element.

```jsx
import { Button } from '../../Button'

const options = [
  { label: 'Option 1 ', value: '1' },
  { label: 'Option 2 ', value: '2' },
  { label: 'Option 3 ', value: '3' },
  { label: 'Option 4 ', value: '4' },
  { label: 'Option 5 ', value: '5' },
]

;<Select
  options={options}
  RenderSelectedMulti={(option, onRemove) => (
    <div className="m-xxs flex items-center rounded bg-surface">
      <div className="px-base text-sm">{option.label}</div>
      <Button subtle variant="critical" onClick={onRemove} size="sm">
        <svg
          className="h-4 w-4"
          fill="currentColor"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
            clipRule="evenodd"
          />
        </svg>
      </Button>
    </div>
  )}
  multi
/>
```

### RenderListItem

Using this prop you can change how a selected value will look when prop `multi` is true. It expects a function that
accepts the `option` and `state` parameters.

- `option` will contain all the selected option's data.
- `state` is an object that contains the props `isDisabled, isFocused, isSelected` that can be used to make your
  component react to state changes.

```jsx
const options = [
  { label: 'Option 1 ', value: '1' },
  { label: 'Option 2 ', value: '2' },
  { label: 'Option 3 ', value: '3' },
  { label: 'Option 4 ', value: '4' },
  { label: 'Option 5 ', value: '5' },
]

;<Select
  options={options}
  RenderListItem={(option) => {
    return <div className="text-color-caution">{option.label}</div>
  }}
/>
```

### RenderGroupHeading

Using this prop you can change how a group heading will look. It expects a function that accepts the `optionGroup`object
and returns a React element. `optionGroup` provides both `label` and `options` props to further customize the heading.

```jsx
import { Badge } from '../../Badge'

const groupedOptions = [
  {
    label: 'Group  A',
    options: [
      { label: 'Option 1', value: 1 },
      { label: 'Option 2', value: 2 },
      { label: 'Option 3', value: 3 },
      { label: 'Option 4', value: 4 },
      { label: 'Option 5', value: 5 },
    ],
  },
  {
    label: 'Group  B',
    options: [
      { label: 'Option 6', value: 6 },
      { label: 'Option 7', value: 7 },
      { label: 'Option 8', value: 8 },
      { label: 'Option 9', value: 9 },
      { label: 'Option 10', value: 10 },
    ],
  },
  {
    label: 'Group  C',
    options: [
      { label: 'Option 11', value: 11 },
      { label: 'Option 12', value: 12 },
      { label: 'Option 13', value: 13 },
      { label: 'Option 14', value: 14 },
      { label: 'Option 15', value: 15 },
    ],
  },
]

const [value, setValue] = React.useState()

;<div>
  <Select
    options={groupedOptions}
    onChangeValue={setValue}
    value={value}
    RenderGroupHeading={({ label, options }) => (
      <div className="space-between flex">
        <div className="flex-grow text-color-positive">{label}</div>
        <Badge value={options.length} variant="positive" />
      </div>
    )}
  />
  <div className="mt-base">Selected Value: {value}</div>
</div>
```

### RenderNoOptions

It will be displayed when menu is open and there are no options to show. It can take a `string` or `function`.

```jsx
<div className="space-y-base">
  <Select
    RenderNoOptions={
      <div className="bg-caution text-color-semantic">
        This is a custom noOptions message
      </div>
    }
    options={[]}
  />
  <Select RenderNoOptions="String also works" options={[]} />
</div>
```
