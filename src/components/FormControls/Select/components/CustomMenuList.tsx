import React from 'react'
import { MenuListWithLoadingProps } from '../types'
import { components } from 'react-select'

export interface CustomMenuListProps extends MenuListWithLoadingProps {
  CustomComponent: (props: { children: React.ReactNode }) => React.ReactElement
}

const CustomMenuList = ({
  CustomComponent,
  children,
  ...props
}: CustomMenuListProps): React.ReactElement => {
  return (
    <components.MenuList {...props}>
      <CustomComponent>{children}</CustomComponent>
    </components.MenuList>
  )
}

export { CustomMenuList }
