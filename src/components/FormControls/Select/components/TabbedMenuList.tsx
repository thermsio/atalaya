import React from 'react'
import { Options } from 'react-select'
import { Button } from '../../../Button'
import { FormDivider } from '../../../FormLayout/components/FormDivider'
import { Icon } from '../../../Icon'
import { Tabs } from '../../../Tabs'
import { Inline } from '../../../../layout/Inline'
import { MenuListWithLoadingProps } from '../types'
import { Loading } from '../../../Loading'

export interface TabbedMenuListProps extends MenuListWithLoadingProps {
  SelectedListItem?: (
    option: unknown,
    state?: { isDisabled?: boolean; isFocused?: boolean; isSelected?: boolean },
    selectOption?: (newValue: unknown) => void,
  ) => React.ReactNode
}

const TabbedMenuList = (props: TabbedMenuListProps): React.ReactElement => {
  const selection: Options<unknown> = props.getValue()

  let filteredSelection = [...selection]

  if (Array.isArray(selection) && selection.length > 0) {
    filteredSelection = filteredSelection.filter((item: unknown) => {
      const label = props.selectProps.getOptionLabel(item)

      if (typeof label === 'string') {
        return label
          .toLowerCase()
          .includes(props.selectProps.inputValue.toLowerCase())
      }

      // could be ReactNode
      return false
    })
  }

  const handleScroll = (e) => {
    const threshold = e.target.scrollHeight - e.target.clientHeight - 20

    const shouldLoadMoreOptions = e.target.scrollTop >= threshold

    if (shouldLoadMoreOptions && props.selectProps.onMenuScrollToBottom) {
      props.selectProps.onMenuScrollToBottom(e)
    }
  }

  return (
    <div className="rs__menu-list">
      {props.isLoading && <Loading overlay />}

      <div className="p-xxs">
        <Tabs fullWidth initialTabKey="one" pills>
          <Tabs.Tab tabKey="one" title="Options">
            {props.options.length > 0 && (
              <div className="mb-sm text-center text-sm text-color-neutral">
                <em>
                  {props.options.length} option{props.options.length > 1 && 's'}
                </em>
              </div>
            )}

            <FormDivider withBorder={true} />

            <div
              onScroll={handleScroll}
              className={'overflow-y-auto overflow-x-hidden'}
              style={{ maxHeight: props.selectProps.maxMenuHeight }}
            >
              {props.children}
            </div>
          </Tabs.Tab>

          <Tabs.Tab tabKey="two" title="Selected">
            {selection.length === 0 ? (
              <p className="mb-sm text-center text-sm text-color-neutral">
                No selected options
              </p>
            ) : (
              filteredSelection.length === 0 && (
                <p className="mb-sm text-center text-sm text-color-neutral">
                  No selection matches{' '}
                  <em>{"'" + props.selectProps.inputValue + "'"}</em>
                </p>
              )
            )}
            <div
              className={'overflow-y-auto overflow-x-hidden'}
              style={{ maxHeight: props.selectProps.maxMenuHeight }}
            >
              {filteredSelection.map((item: unknown) => {
                const label = props.selectProps.getOptionLabel(item)
                const value = props.selectProps.getOptionValue(item)

                return (
                  <Inline
                    alignX="between"
                    alignY="center"
                    className="px-sm py-1 hover:bg-critical-faded"
                    key={`selected-option-${value}`}
                    onClick={() => {
                      props.selectOption(item)
                    }}
                    width="full"
                  >
                    {props.SelectedListItem ? (
                      props.SelectedListItem(item)
                    ) : (
                      <span>{label}</span>
                    )}

                    <Button
                      onClick={() => {
                        props.selectOption(item)
                      }}
                      size="sm"
                      subtle
                      variant="critical"
                    >
                      <span className="sr-only">Remove</span>

                      <Icon>
                        <svg
                          className="h-6 w-6"
                          fill="currentColor"
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 20 20"
                        >
                          <path
                            clipRule="evenodd"
                            d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                            fillRule="evenodd"
                          />
                        </svg>
                      </Icon>
                    </Button>
                  </Inline>
                )
              })}
            </div>
          </Tabs.Tab>
        </Tabs>
      </div>
    </div>
  )
}

export { TabbedMenuList }
