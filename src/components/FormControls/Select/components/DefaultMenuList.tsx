import React from 'react'
import { components, Options } from 'react-select'
import { Inline } from '../../../../layout/Inline'
import { Button } from '../../../Button'
import { Icon } from '../../../Icon'
import { MenuListWithLoadingProps } from '../types'
import { Loading } from '../../../Loading'
import { Transition } from 'react-transition-group'
import classNameParser from 'classnames'

interface DefaultMenuListProps extends MenuListWithLoadingProps {
  combineSelected?: boolean
  SelectedListItem?: (props) => React.ReactNode
}

const DefaultMenuList = ({
  children,
  combineSelected,
  isLoading,
  options,
  SelectedListItem,
  ...restOfProps
}: DefaultMenuListProps) => {
  const selection: Options<unknown> = restOfProps.getValue()

  return (
    <>
      <components.MenuList options={options} {...restOfProps}>
        {combineSelected && selection.length > 0 && (
          <div className="rounded border border-neutral m-xxs ">
            {selection.map((item: unknown) => {
              const label = restOfProps.selectProps.getOptionLabel(item)
              const value = restOfProps.selectProps.getOptionValue(item)
              return (
                <Inline
                  alignX="between"
                  alignY="center"
                  className="px-sm py-1 hover:bg-critical-faded"
                  key={`selected-option-${value}`}
                  onClick={() => {
                    restOfProps.selectOption(item)
                  }}
                  width="full"
                >
                  {SelectedListItem ? (
                    SelectedListItem(item)
                  ) : (
                    <span>{label}</span>
                  )}

                  <Button
                    onClick={() => {
                      restOfProps.selectOption(item)
                    }}
                    size="sm"
                    subtle
                    variant="critical"
                  >
                    <span className="sr-only">Remove</span>

                    <Icon>
                      <svg
                        className="h-6 w-6"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          fillRule="evenodd"
                          d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                          clipRule="evenodd"
                        />
                      </svg>
                    </Icon>
                  </Button>
                </Inline>
              )
            })}
          </div>
        )}

        {children}
      </components.MenuList>

      <Transition
        mountOnEnter
        unmountOnExit
        /* mountOnEnter breaks enter animation. By accessing an element attribute we cause
         the mount and start of css animation to happen on different ticks, fixing the issue.
         https://github.com/reactjs/react-transition-group/issues/223 */
        onEnter={(node) => node.offsetHeight}
        in={isLoading && !!options.length}
        timeout={200}
      >
        {(state) => (
          <div
            className={classNameParser(
              {
                'opacity-100': state === 'entering' || state === 'entered',
                'opacity-0': state === 'exiting' || state === 'exited',
              },
              'pointer-events-none absolute inset-x-0 bottom-0 flex h-[100px] items-end justify-center bg-gradient-to-t from-input-background pb-base opacity-0 transition-opacity duration-200 ease-in-out',
            )}
          >
            <Loading />
          </div>
        )}
      </Transition>
    </>
  )
}

export { DefaultMenuList }
