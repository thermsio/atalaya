import React, { useCallback, useRef, useState } from 'react'
import { useValue } from 'react-cosmos/client'
import { Stack } from '../../../layout/Stack'
import { Select } from './Select'
import { Button } from '../../Button'
import { Badge } from '../../Badge'
import { Checkbox } from '../Checkbox/Checkbox'
import { Inline } from '../../../layout/Inline'
import { Text } from '../Text/Text'
import { SelectOption } from '../types'
import { RichTextEditor } from '../RichTextEditor'

const data: { label: string; value: any }[] = [
  { label: 'Test NULL value', value: null },
  { label: 'Test "" value', value: '' },
  ...new Array(40).fill(null).map((_, i) => ({
    label: 'Test ' + (i + 1),
    value: i + 1,
  })),
]

const groupedData = [
  {
    label: 'Group  A',
    options: [
      { label: 'Test 1', value: 1 },
      { label: 'Test 2', value: 2 },
      { label: 'Test 3', value: 3 },
      { label: 'Test 4', value: 4 },
      { label: 'Test 5', value: 5 },
    ],
  },
  {
    label: 'Group  B',
    options: [
      { label: 'Test 6', value: 6 },
      { label: 'Test 7', value: 7 },
      { label: 'Test 8', value: 8 },
      { label: 'Test 9', value: 9 },
      { label: 'Test 10', value: 10 },
    ],
  },
  {
    label: 'Group  C',
    options: [
      { label: 'Test 11', value: 11 },
      { label: 'Test 12', value: 12 },
      { label: 'Test 13', value: 13 },
      { label: 'Test 14', value: 14 },
      { label: 'Test 15', value: 15 },
    ],
  },
]

const base = (): React.ReactElement => {
  const [allowCreate] = useValue('allowCreate', { defaultValue: false })
  const [combined] = useValue('combined', { defaultValue: false })
  const [value, setValue] = useValue('value', { defaultValue: undefined })
  const [open] = useValue('menu open', { defaultValue: false })
  const [multi] = useValue('multi', { defaultValue: false })
  const [createdOption, setCreatedOption] = useState<SelectOption>()

  return (
    <Stack space="base">
      <Select
        label={`Label!! Value: ${value}`}
        allowCreate={allowCreate}
        combineSelected={combined}
        multi={multi}
        onChangeValue={setValue}
        onCreateOption={setCreatedOption}
        menuIsOpen={open}
        // optionValueExtractor={(option) => {
        //   console.log(option)
        //   return option.value
        // }}
        placeholder="here"
        options={data}
        value={value}
      />

      <div className="mt-lg">Selected value: {value}</div>
      {createdOption && (
        <div className="mt-lg">
          Created option: {JSON.stringify(createdOption)}
        </div>
      )}

      <div>
        <Button onClick={() => setValue(undefined)}>Reset Value</Button>
      </div>
    </Stack>
  )
}

const onChangeProps = (): React.ReactElement => {
  const [value, setValue] = useValue<any>('value', { defaultValue: undefined })
  const [multi] = useValue('multi', { defaultValue: false })

  return (
    <Stack space="base">
      <Select
        label={`Label!! Value: ${value}`}
        multi={multi}
        onChangeOptions={(selectedOptions) => {
          setValue(selectedOptions)
          console.log('selectedOptions', selectedOptions)
        }}
        onChangeValue={setValue}
        placeholder="here"
        options={data}
        value={value}
      />

      <div className="mt-lg">Selected value: {value}</div>

      <div>
        <Button onClick={() => setValue(undefined)}>Reset Value</Button>
      </div>
    </Stack>
  )
}

const async = (): React.ReactElement => {
  const limit = 10
  const [value, setValue] = useState()
  const [options, setOptions] = useState<SelectOption[]>(data.slice(0, limit))
  const [isLoading, setIsLoading] = useState(false)

  const timeoutRef = useRef<any>(null)

  const handleLoadOptions = useCallback(
    ({ additional, text }: { additional?: boolean; text?: string } = {}) => {
      setIsLoading(true)

      if (timeoutRef.current) {
        clearTimeout(timeoutRef.current)
      }

      if (text) {
        timeoutRef.current = setTimeout(() => {
          let newOptions: any = []

          if (additional) {
            newOptions = [
              ...options,
              ...data
                .filter((option) => option.label.includes(text))
                .slice(options.length, options.length + limit),
            ]
          } else {
            newOptions = data
              .filter((option) => option.label.includes(text))
              .slice(0, limit)
          }

          setOptions(newOptions)
          setIsLoading(false)
        }, 2000)
      } else if (additional) {
        timeoutRef.current = setTimeout(() => {
          setOptions([
            ...options,
            ...data.slice(options.length, options.length + limit),
          ])
          setIsLoading(false)
        }, 1000)
      } else {
        // initial "load" call
        setOptions(data.slice(0, limit))
        setIsLoading(false)
      }
    },
    [options],
  )

  return (
    <Select
      isLoading={isLoading}
      loadOptions={handleLoadOptions}
      options={options}
      value={value}
      onChangeValue={setValue}
    />
  )
}

const asyncCustomMenuList = (): React.ReactElement => {
  const limit = 10
  const [options, setOptions] = useState<SelectOption[]>(data.slice(0, limit))
  const [isLoading, setIsLoading] = useState(false)

  const timeoutRef = useRef<any>(null)

  const handleLoadOptions = useCallback(
    ({ additional, text }: { additional?: boolean; text?: string } = {}) => {
      setIsLoading(true)
      console.log('handleLoadOptions Fired')

      if (timeoutRef.current) {
        clearTimeout(timeoutRef.current)
      }

      if (text) {
        timeoutRef.current = setTimeout(() => {
          let newOptions: any = []

          if (additional) {
            newOptions = [
              ...options,
              ...data
                .filter((option) => option.label.includes(text))
                .slice(options.length, options.length + limit),
            ]
          } else {
            newOptions = data
              .filter((option) => option.label.includes(text))
              .slice(0, limit)
          }

          setOptions(newOptions)
          setIsLoading(false)
        }, 2000)
      } else if (additional) {
        timeoutRef.current = setTimeout(() => {
          setOptions([
            ...options,
            ...data.slice(options.length, options.length + limit),
          ])
          setIsLoading(false)
        }, 1000)
      } else {
        // initial "load" call
        setOptions(data.slice(0, limit))
        setIsLoading(false)
      }
    },
    [options],
  )

  return (
    <Select
      isLoading={isLoading}
      loadOptions={handleLoadOptions}
      multi
      options={options}
      RenderMenuList={(props) => (
        <div className="rounded bg-surface-subtle">
          <div className="sticky top-0">Custom Container!</div>

          {props.children}
        </div>
      )}
    />
  )
}

const asyncTabbed = (): React.ReactElement => {
  const limit = 7
  const [options, setOptions] = useState<SelectOption[]>(data.slice(0, limit))
  const [isLoading, setIsLoading] = useState(false)

  const timeoutRef = useRef<any>(null)

  const handleLoadOptions = useCallback(
    ({ additional, text }: { additional?: boolean; text?: string } = {}) => {
      setIsLoading(true)

      if (timeoutRef.current) {
        clearTimeout(timeoutRef.current)
      }

      if (text) {
        timeoutRef.current = setTimeout(() => {
          let newOptions: any = []

          if (additional) {
            newOptions = [
              ...options,
              ...data
                .filter((option) => option.label.includes(text))
                .slice(options.length, options.length + limit),
            ]
          } else {
            newOptions = data
              .filter((option) => option.label.includes(text))
              .slice(0, limit)
          }

          setOptions(newOptions)
          setIsLoading(false)
        }, 2000)
      } else if (additional) {
        timeoutRef.current = setTimeout(() => {
          setOptions([
            ...options,
            ...data.slice(options.length, options.length + limit),
          ])
          setIsLoading(false)
        }, 1000)
      } else {
        // initial "load" call
        setOptions(data.slice(0, limit))
        setIsLoading(false)
      }
    },
    [options],
  )

  return (
    <Select
      combineSelected
      hideSelectedOptions={false}
      isLoading={isLoading}
      loadOptions={handleLoadOptions}
      maxMenuHeight={200}
      menuIsOpen
      multi
      options={options}
      RenderListItem={(option, state, selectOption) => (
        <Inline alignY="center">
          <Checkbox
            active={state?.isSelected || false}
            onChange={() => {
              if (selectOption) {
                selectOption(option)
              }
            }}
          />
          <div className="ml-base flex-grow overflow-hidden truncate text-ellipsis">
            <p>{option.label}</p>
            <p className="text-sm">Some other text</p>
          </div>
        </Inline>
      )}
      RenderSelectedListItem={(option) => (
        <Inline alignY="center">
          <div className="ml-base flex-grow overflow-hidden truncate text-ellipsis">
            <p>{option.label}</p>
            <p className="text-sm">Some other text</p>
          </div>
        </Inline>
      )}
      tabbed
    />
  )
}

const customSingleValue = (): React.ReactElement => {
  return (
    <Select
      options={data}
      RenderSelectedSingle={(option: (typeof data)[0]) => (
        <div className="font-size-lg text-color-caution">{option.label}</div>
      )}
    />
  )
}

const dynamicValueChange = (): React.ReactElement => {
  const [value, setValue] = useState<number>()

  return (
    <div>
      <Text
        label="Enter 'data' array index"
        onChangeValue={(val) => setValue(data[val]?.value || undefined)}
      />

      <Select label={`Value: ${value}`} options={data} value={value} />
    </div>
  )
}

const customMultiValue = (): React.ReactElement => {
  return (
    <Select
      options={data}
      RenderSelectedMulti={(option, onRemove) => {
        return (
          <div className="flex items-center rounded bg-surface">
            <div className="px-base">{option.label}</div>
            <Button subtle variant="critical" onClick={onRemove}>
              <svg
                className="h-6 w-6"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <svg
                  className="h-6 w-6"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fillRule="evenodd"
                    d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  />
                </svg>
              </svg>
            </Button>
          </div>
        )
      }}
      multi
    />
  )
}

const customListItem = (): React.ReactElement => {
  return (
    <Select
      options={data}
      RenderListItem={(option) => {
        return <div className="text-color-caution">{option.label}</div>
      }}
    />
  )
}

const customNoOptions = (): React.ReactElement => (
  <Select RenderNoOptions="String also works" options={[]} />
)

const groupedOptions = (): React.ReactElement => {
  const [value, setValue] = useState()

  return (
    <div>
      <Select
        options={groupedData}
        onChangeValue={setValue}
        value={value}
        RenderGroupHeading={({ label, options }) => (
          <div className="space-between flex">
            <div className="flex-grow">{label}</div>
            <Badge value={options.length} />
          </div>
        )}
      />
      <div>Selected Value: {value}</div>
    </div>
  )
}

const extractorRequiredData: { name: string; id: number }[] = [
  { name: 'Test 1', id: 1 },
  { name: 'Test 2', id: 2 },
  { name: 'Test 3', id: 3 },
  { name: 'Test 4', id: 4 },
  { name: 'Test 5', id: 5 },
]

const usingExtractors = (): React.ReactElement => {
  const [value, setValue] = useState()

  return (
    <div>
      <Select
        options={extractorRequiredData}
        onChangeValue={setValue}
        value={value}
        optionLabelExtractor={(option) => option.name}
        optionValueExtractor={(option) => option.id.toString()}
      />
      <div>Selected Value: {value}</div>
    </div>
  )
}

const loadingNoOptions = <Select isLoading options={[]} />

const tabbed = (): React.ReactElement => {
  const [allowCreate] = useValue('allowCreate', { defaultValue: false })
  const [value, setValue] = useValue('value', { defaultValue: undefined })
  const [multi] = useValue('multi', { defaultValue: false })

  return (
    <Stack space="base">
      <Select
        label={`Label!! Value: ${value}`}
        allowCreate={allowCreate}
        multi={multi}
        onChangeValue={setValue}
        placeholder="here"
        options={data}
        menuIsOpen
        tabbed
        value={value}
      />

      <div className="mt-lg">Selected value: {value}</div>

      <div>
        <Button onClick={() => setValue(undefined)}>Reset Value</Button>
      </div>
    </Stack>
  )
}

const valueNotInOptions = () => {
  const [value, setValue] = useState([
    { label: 'Not in options List', value: 'Value not in options' },
  ])

  return (
    <Select
      allowCreate
      multi
      // @ts-ignore
      onChangeOptions={setValue}
      options={data}
      value={value}
    />
  )
}

const onTopOfRichText = () => {
  const [value, setValue] = useState()

  return (
    <div>
      <Select
        label={`Label!! Value: ${value}`}
        onChangeValue={setValue}
        placeholder="here"
        options={data}
        value={value}
      />

      <RichTextEditor onChangeValue={console.log} />
    </div>
  )
}

export default {
  base,
  async,
  asyncCustomMenuList,
  asyncTabbed,
  customListItem,
  customMultiValue,
  customNoOptions,
  customSingleValue,
  dynamicValueChange,
  groupedOptions,
  usingExtractors,
  loadingNoOptions,
  tabbed,
  onChangeProps,
  valueNotInOptions,
  onTopOfRichText,
}
