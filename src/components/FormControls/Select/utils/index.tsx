import React from 'react'
import {
  components,
  MultiValueProps,
  OptionProps,
  SelectComponentsConfig,
  SingleValueProps,
  ValueContainerProps,
} from 'react-select'
import { Inline } from '../../../../layout/Inline'
import { TabbedMenuList } from '../components/TabbedMenuList'
import { CustomMenuList } from '../components/CustomMenuList'
import { DefaultMenuList } from '../components/DefaultMenuList'

export function getCustomComponents({
  combineSelected,
  ListItem,
  NoOptions,
  MenuList,
  SelectedListItem,
  SelectedMulti,
  SelectedSingle,
  tabbed,
}): SelectComponentsConfig<any, any, any> {
  const customComponents = {} as SelectComponentsConfig<any, any, any>

  if (MenuList) {
    customComponents.MenuList = (props) =>
      CustomMenuList({ ...props, CustomComponent: MenuList })
  } else if (tabbed) {
    customComponents.MenuList = (props) =>
      TabbedMenuList({ ...props, SelectedListItem })
    customComponents.DropdownIndicator = function CustomDropdownIndicator() {
      return null
    }
  } else {
    customComponents.MenuList = (props) =>
      DefaultMenuList({ ...props, combineSelected, SelectedListItem })
  }

  if (SelectedSingle) {
    customComponents.SingleValue = function CustomSingleValue({
      data,
      ...restOfProps
    }: SingleValueProps) {
      return (
        <components.SingleValue data={data} {...restOfProps}>
          {SelectedSingle(data)}
        </components.SingleValue>
      )
    }
  }

  if (SelectedMulti) {
    customComponents.MultiValue = function CustomMultiValue({
      data,
      removeProps,
    }: MultiValueProps) {
      return (
        <div
          className="m-[2px] self-stretch"
          onMouseDown={(event) => {
            /* We stop propagation because we want to prevent the menu from being opened when clicking the remove button.*/
            event.stopPropagation()
          }}
        >
          {SelectedMulti(data, removeProps.onClick)}
        </div>
      )
    }
  }

  if (ListItem) {
    customComponents.Option = function CustomListItem({
      data,
      isDisabled,
      isFocused,
      isSelected,
      ...restOfProps
    }: OptionProps) {
      return (
        <components.Option
          data={data}
          isDisabled={isDisabled}
          isFocused={isFocused}
          isSelected={isSelected}
          {...restOfProps}
        >
          {ListItem(
            data,
            { isDisabled, isFocused, isSelected },
            restOfProps.selectOption,
          )}
        </components.Option>
      )
    }
  }

  if (combineSelected) {
    customComponents.ValueContainer = function CustomValueContainer(
      props: ValueContainerProps,
    ) {
      const itemCount = props.getValue().length
      const message = `${itemCount} item${itemCount > 1 ? 's' : ''} selected`
      return (
        <components.ValueContainer {...props}>
          <Inline alignY="center">
            {itemCount > 0 && (
              <span className="mr-sm inline-block text-xs text-color-subtle">
                {message}
              </span>
            )}
            {props.children}
          </Inline>
        </components.ValueContainer>
      )
    }
    customComponents.MultiValue = function MultiValue() {
      return null
    }
  }

  if (NoOptions) {
    customComponents.NoOptionsMessage = function CustomNoOptionsMessage(props) {
      return (
        <components.NoOptionsMessage {...props}>
          {typeof NoOptions === 'function' ? NoOptions() : NoOptions}
        </components.NoOptionsMessage>
      )
    }
  }

  return customComponents
}

export type ExtractorType =
  | null
  | string
  | ((searchValue: string) => React.ReactNode)

export function parseLoadingMessage(
  searchValue: string,
  extractor?: ExtractorType,
) {
  if (extractor === null) return null
  if (typeof extractor === 'string') return extractor
  if (typeof extractor === 'function') return extractor(searchValue)
  return undefined
}
