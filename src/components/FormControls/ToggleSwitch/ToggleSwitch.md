```jsx
const [active, setActive] = React.useState(true)

;<ToggleSwitch active={active} name="a" onChangeValue={setActive} />
```

### With labels

```jsx
const [active, setActive] = React.useState(true)

;<ToggleSwitch
  active={active}
  label="Toggle Switch Field"
  name="b"
  onChangeValue={setActive}
  subText="Switch on, switch off."
/>
```

### With labels and inline sub text

```jsx
const [active, setActive] = React.useState(true)

;<ToggleSwitch
  active={active}
  label="Toggle Switch Field"
  name="b"
  onChangeValue={setActive}
  subText="Switch on, switch off."
  subTextInline
/>
```

### With Error

```jsx
const [active, setActive] = React.useState(true)

;<ToggleSwitch
  active={active}
  label="Toggle Switch Field"
  name="c"
  onChangeValue={setActive}
  error="The toggle is not working."
/>
```

### Valid

```jsx
const [active, setActive] = React.useState(true)

;<ToggleSwitch
  active={active}
  label="Toggle Switch Field"
  name="c"
  onChangeValue={setActive}
  valid
/>
```

### Disabled

```jsx
<ToggleSwitch disabled active={false} label="Toggle Switch Field" name="d" />
```

### Toggle First

```jsx
const [active, setActive] = React.useState(true)

;<ToggleSwitch
  active={active}
  label="Toggle Switch Field"
  name="b"
  onChangeValue={setActive}
  subText="Lorem Ipsum dolor sit amet consectetur adipisicing elit adipisicing."
  toggleFirst
/>
```

### Toggle First and inline subText

```jsx
const [active, setActive] = React.useState(true)

;<ToggleSwitch
  active={active}
  label="Toggle Switch Field"
  name="b"
  onChangeValue={setActive}
  subText="Lorem Ipsum dolor sit amet consectetur adipisicing elit adipisicing."
  subTextInline
  toggleFirst
/>
```
