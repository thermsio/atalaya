import React, { useCallback } from 'react'
import classNameParser from 'classnames'
import {
  FormControlWrapper,
  FormControlWrapperPublicProps,
} from '../../FormLayout/components/FormControlWrapper'
import { FormControlCommonProps } from '../types'
import './ToggleSwitch.css'
import '../FormControls.css'
import Label from '../../FormLayout/components/FormControlWrapper/components/Label'
import SubText from '../../FormLayout/components/FormControlWrapper/components/SubText'

export interface ToggleSwitchProps
  extends FormControlCommonProps,
    FormControlWrapperPublicProps {
  active: boolean | undefined
  /** If true, label will appear right after toggle input.*/
  toggleFirst?: boolean
  /** If true, subText will be in the same line as Label.*/
  subTextInline?: boolean
}

const ToggleSwitch = ({
  active,
  disabled,
  error,
  label,
  labelBehavior,
  name,
  onBlur,
  onChange,
  onChangeValue,
  onFocus,
  showErrorMessage,
  subText,
  subTextInline,
  toggleFirst,
  valid,
}: ToggleSwitchProps): React.ReactElement => {
  const handleOnChange = useCallback(
    (e) => {
      if (onChangeValue) onChangeValue(!active)
      if (onChange) onChange(e)
    },
    [active, onChange, onChangeValue],
  )

  if (!label && !subTextInline)
    return (
      <FormControlWrapper
        error={error}
        inputName={name}
        showErrorMessage={showErrorMessage}
      >
        <button
          className={classNameParser(
            {
              'form-field-toggle-active': active,
              'form-field-disabled': disabled,
              'form-field-error': error,
              'form-field-valid': valid,
            },
            'form-field form-field-toggle',
          )}
          disabled={disabled}
          onBlur={onBlur}
          onClick={handleOnChange}
          onFocus={onFocus}
          type="button"
        >
          <span
            aria-hidden="true"
            className={classNameParser(
              {
                'form-field-toggle-handle-active': active,
              },
              'form-field-toggle-handle',
            )}
          />
        </button>
      </FormControlWrapper>
    )

  const showInlineSubtext = Boolean(subTextInline && !!subText)

  return (
    <FormControlWrapper
      error={error}
      inputName={name}
      showErrorMessage={showErrorMessage}
      subText={showInlineSubtext ? undefined : subText}
    >
      <div className="flex flex-wrap items-center gap-x-sm gap-y-xs">
        {!toggleFirst && (!!label || showInlineSubtext) && (
          <>
            {!!label && <Label inputName={name}>{label}</Label>}
            {showInlineSubtext && <SubText>{subText}</SubText>}
          </>
        )}

        <button
          className={classNameParser(
            {
              'form-field-toggle-active': active,
              'form-field-disabled': disabled,
              'form-field-error': error,
              'form-field-valid': valid,
              'ml-auto': !toggleFirst,
            },
            'form-field form-field-toggle',
          )}
          disabled={disabled}
          onBlur={onBlur}
          onClick={handleOnChange}
          onFocus={onFocus}
          type="button"
        >
          <span
            aria-hidden="true"
            className={classNameParser(
              {
                'form-field-toggle-handle-active': active,
              },
              'form-field-toggle-handle',
            )}
          />
        </button>

        {toggleFirst && (
          <>
            {!!label && <Label inputName={name}>{label}</Label>}
            {showInlineSubtext && <SubText>{subText}</SubText>}
          </>
        )}
      </div>
    </FormControlWrapper>
  )
}

export { ToggleSwitch }
