import React, { useCallback, useMemo } from 'react'
import classNameParser from 'classnames'

import {
  FormControlWrapper,
  FormControlWrapperPublicProps,
} from '../../FormLayout/components/FormControlWrapper'
import { Inline } from '../../../layout/Inline'

import { FormControlCommonProps } from '../types'
import '../FormControls.css'
import './Checkbox.css'
import HintText from '../../FormLayout/components/FormControlWrapper/components/HintText'

export interface CheckboxProps
  extends FormControlCommonProps,
    FormControlWrapperPublicProps {
  value?: string | number
  active: boolean
}

const Checkbox = ({
  active,
  error,
  disabled,
  hint,
  label,
  name,
  onChange,
  onBlur,
  onChangeValue,
  onFocus,
  showErrorMessage,
  subText,
  valid,
  value,
}: CheckboxProps): React.ReactElement => {
  const handleOnChange = useCallback(
    (e) => {
      if (onChangeValue) onChangeValue(!!e.target.checked)
      if (onChange) onChange(e)
    },
    [onChange, onChangeValue],
  )

  // An ID is needed for the label to be selectable and the "name" prop is not always provided
  const id = useMemo(() => {
    return name || Math.random().toString()
  }, [])

  return (
    <FormControlWrapper
      error={error}
      disabled={disabled}
      hasValue={!!value}
      inputName={name}
      showErrorMessage={showErrorMessage}
      subText={subText}
    >
      <Inline alignY="center" space="xs">
        <input
          type="checkbox"
          checked={active}
          className={classNameParser(
            {
              'form-field-disabled': disabled,
              'form-field-error': error,
              'form-field-success': valid,
            },
            'form-field form-checkbox',
          )}
          name={name}
          id={id}
          disabled={disabled}
          onBlur={onBlur}
          onChange={handleOnChange}
          onFocus={onFocus}
          value={value}
        />

        {!!label && (
          <label
            htmlFor={id}
            className={classNameParser(
              { 'cursor-not-allowed text-color-subtle': disabled },
              'select-none text-sm',
            )}
          >
            {label}
          </label>
        )}

        {!!hint && <HintText>{hint}</HintText>}
      </Inline>
    </FormControlWrapper>
  )
}

export { Checkbox }
