```jsx
const [active, setActive] = React.useState(true)

;<Checkbox
  active={active}
  name="a"
  onChange={(e) => setActive(e.target.active)}
/>
```

### With labels

```jsx
const [active, setActive] = React.useState(true)

;<Checkbox
  active={active}
  hint="Hint"
  label="Checkbox Field"
  name="b"
  onChange={(e) => setActive(e.target.active)}
  subText="Only latin characters allowed."
/>
```

### With Error

```jsx
const [active, setActive] = React.useState(true)

;<Checkbox
  active={active}
  error="There is something very wrong with this input."
  label="Checkbox Field"
  name="c"
  onChange={(e) => setActive(e.target.active)}
/>
```

### Valid

```jsx
const [active, setActive] = React.useState(true)

;<Checkbox
  active={active}
  label="Checkbox Field"
  name="d"
  onChange={(e) => setActive(e.target.active)}
  valid
/>
```

### Disabled

```jsx
<Checkbox disabled label="Checkbox Field" name="e" />
```
