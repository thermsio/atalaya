import React from 'react'
import { Button } from '../../Button'
import { DateTimePicker } from './DateTimePicker'
import dayjs from 'dayjs'

const basic = () => {
  const [date, setDate] = React.useState<string | undefined>()

  return (
    <div>
      <div className="mb-5">{date}</div>
      <DateTimePicker
        value={date}
        onChangeValue={(newDate) => {
          console.log(newDate)
          setDate(newDate)
        }}
      />
    </div>
  )
}

const dateOnly = () => {
  const [date, setDate] = React.useState<string | undefined>()

  return (
    <div>
      <div className="mb-5">{date}</div>
      <DateTimePicker mode="date" value={date} onChangeValue={setDate} />
    </div>
  )
}

const timeOnly = () => {
  const [date, setDate] = React.useState<string | undefined>()

  return (
    <div>
      <div className="mb-5">{date}</div>
      <DateTimePicker mode="time" value={date} onChangeValue={setDate} />
    </div>
  )
}

const is24 = () => {
  const [date, setDate] = React.useState<string | undefined>()

  return (
    <div>
      <div className="mb-5">{date}</div>
      <DateTimePicker is24 value={date} onChangeValue={setDate} />
    </div>
  )
}

const backgroundClash = () => {
  const [date, setDate] = React.useState<string | undefined>(
    '2021-05-31T17:56:39.975Z',
  )

  return (
    <div className="h-screen bg-surface p-xl">
      <DateTimePicker value={date} onChangeValue={setDate} />
    </div>
  )
}

const noInitialValue = () => {
  const [date, setDate] = React.useState<string | undefined>()

  return (
    <div className="h-screen bg-surface p-xl">
      <DateTimePicker value={date} noInitialValue onChangeValue={setDate} />
    </div>
  )
}

const changeValueOutsideOfPicker = () => {
  const [date, setDate] = React.useState<string | undefined>()

  return (
    <div className="h-screen bg-surface p-xl">
      <Button onClick={() => setDate(dayjs(date).add(1, 'day').toISOString())}>
        Add one day
      </Button>
      <DateTimePicker value={date} onChangeValue={setDate} />
    </div>
  )
}

const mondayStart = () => {
  const [date, setDate] = React.useState<string | undefined>()

  return (
    <div>
      <div className="mb-5">{date}</div>
      <DateTimePicker value={date} onChangeValue={setDate} weekStart="monday" />
    </div>
  )
}

const withError = () => {
  const [date, setDate] = React.useState<string | undefined>()

  return (
    <div>
      <div className="mb-5">{date}</div>
      <DateTimePicker
        value={date}
        onChangeValue={setDate}
        weekStart="monday"
        error="There is a problem"
      />
    </div>
  )
}

const clearButton = () => {
  const [date, setDate] = React.useState<string | undefined>()

  return (
    <div>
      <div className="mb-5">{date}</div>
      <DateTimePicker value={date} onChangeValue={setDate} />
      <Button onClick={() => setDate(undefined)}>Clear</Button>
    </div>
  )
}

const today = dayjs()
const disableDatesAfterDate = today.add(7, 'day').toISOString()
const disableDatesBeforeDate = today.subtract(7, 'day').toISOString()

const datesBlocked = () => {
  const [date, setDate] = React.useState<string | undefined>()

  return (
    <div>
      <div className="mb-5">{date}</div>
      <DateTimePicker
        disableDatesAfterDate={disableDatesAfterDate}
        disableDatesBeforeDate={disableDatesBeforeDate}
        value={date}
        onChangeValue={setDate}
      />
    </div>
  )
}

export default {
  backgroundClash,
  basic,
  dateOnly,
  timeOnly,
  is24,
  noInitialValue,
  changeValueOutsideOfPicker,
  mondayStart,
  withError,
  clearButton,
  datesBlocked,
}
