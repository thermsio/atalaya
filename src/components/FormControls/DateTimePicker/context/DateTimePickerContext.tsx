import React, { useEffect, useRef } from 'react'
import { proxy, useSnapshot } from 'valtio'
import { useDateTimeDisplayContext } from '../../../DateTime'

export type PickerModes =
  | 'rangeDate'
  | 'rangeDateTime'
  | 'rangeTime'
  | 'singleDate'
  | 'singleDateTime'
  | 'singleTime'

export const PICKER_MODES: Record<string, PickerModes> = {
  RANGE_DATE: 'rangeDate',
  RANGE_DATE_TIME: 'rangeDateTime',
  RANGE_TIME: 'rangeTime',
  SINGLE_DATE: 'singleDate',
  SINGLE_DATE_TIME: 'singleDateTime',
  SINGLE_TIME: 'singleTime',
}

export type DateTimePickerState = {
  /** Accepts an ISO string or the word 'today' */
  disableDatesAfterDate?: 'today' | string
  /** Accepts an ISO string or the word 'today' */
  disableDatesBeforeDate?: 'today' | string
  is24: boolean
  minuteStep: number
  mode: PickerModes
  timestampActiveView: string
  timestamp?: string
  timestampEnd?: string
  timestampStart?: string
  weekStart: 'sunday' | 'monday'
}

const defaultState: DateTimePickerState = {
  disableDatesAfterDate: undefined,
  disableDatesBeforeDate: undefined,
  is24: false,
  minuteStep: 1,
  mode: PICKER_MODES.SINGLE_DATE,
  timestampActiveView: new Date().toISOString(),
  timestamp: undefined,
  timestampEnd: undefined,
  timestampStart: undefined,
  weekStart: 'sunday',
}

interface DateTimePickerContextProps extends DateTimePickerState {
  isRangeMode: boolean
  setMode: (newMode: PickerModes) => void
  updateTimestamp: (payload?: string) => void
  updateTimestampActiveView: (timestampActiveView: string) => void
  updateTimestampEnd: (payload?: string) => void
  updateTimestampStart: (payload?: string) => void
}

export const DateTimePickerContext = React.createContext<
  DateTimePickerContextProps | undefined
>(undefined)

export interface DateTimePickerProviderProps
  extends Partial<DateTimePickerState> {
  children: React.ReactNode
}

export const DateTimePickerProvider: React.FC<DateTimePickerProviderProps> = ({
  children,
  ...props
}) => {
  const dateTimeDisplayCtx = useDateTimeDisplayContext()

  const { current: state } = useRef(
    proxy({
      disableDatesAfterDate: props.disableDatesAfterDate,
      disableDatesBeforeDate: props.disableDatesBeforeDate,
      is24:
        props.is24 ?? dateTimeDisplayCtx?.format?.is24hr ?? defaultState.is24,
      isRangeMode: !!props.mode?.includes('range'),
      minuteStep: props.minuteStep ?? defaultState.minuteStep,
      mode: props.mode ?? defaultState.mode,
      timestampActiveView:
        props.timestampActiveView ?? defaultState.timestampActiveView,
      timestamp: props.timestamp,
      timestampEnd: props.timestampEnd,
      timestampStart: props.timestampStart,
      weekStart: props.weekStart ?? defaultState.weekStart,
    }),
  )

  const snapshot = useSnapshot(state)

  useEffect(() => {
    if (props.timestamp !== state.timestamp) {
      state.timestamp = props.timestamp
    }
  }, [props.timestamp])

  useEffect(() => {
    if (props.timestampEnd !== state.timestampEnd) {
      state.timestampEnd = props.timestampEnd
    }
  }, [props.timestampEnd])

  useEffect(() => {
    if (props.timestampStart !== state.timestampStart) {
      state.timestampStart = props.timestampStart
    }
  }, [props.timestampStart])

  return (
    <DateTimePickerContext.Provider
      value={{
        ...snapshot,
        setMode: (mode) => (state.mode = mode),
        updateTimestamp: (timestamp: DateTimePickerState['timestamp']) =>
          (state.timestamp = timestamp),
        updateTimestampActiveView: (timestamp) =>
          (state.timestampActiveView = timestamp),
        updateTimestampEnd: (timestamp?) => (state.timestampEnd = timestamp),
        updateTimestampStart: (timestamp?) =>
          (state.timestampStart = timestamp),
      }}
    >
      {children}
    </DateTimePickerContext.Provider>
  )
}

export const useDateTimePickerContext = () => {
  const context = React.useContext(DateTimePickerContext)

  if (context === undefined) {
    throw new Error(
      'useDateTimePicker() must be used inside a <DateTimePicker /> component',
    )
  }

  return context
}
