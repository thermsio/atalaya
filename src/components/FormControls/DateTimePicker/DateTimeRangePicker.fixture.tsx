import React, { useRef, useState } from 'react'
import { DateTimeRangePicker } from './DateTimeRangePicker'
import { Button } from '../../Button'
import dayjs from 'dayjs'
import { Dropdown } from '../../Dropdown'

const datetime = () => {
  const [dateRange, setDateRange] = React.useState<{
    start?: string
    end?: string
  }>({
    start: undefined,
    end: undefined,
  })

  return (
    <DateTimeRangePicker
      mode="datetime"
      onChangeValue={(date) => {
        console.log('change value called', date)
        setDateRange(date)
      }}
      value={dateRange}
    />
  )
}

const date = () => {
  const [dateRange, setDateRange] = React.useState<{
    start?: string
    end?: string
  }>({
    start: undefined,
    end: undefined,
  })

  return (
    <DateTimeRangePicker
      mode="date"
      onChangeValue={(date) => {
        console.log('change value called', date)
        setDateRange(date)
      }}
      value={dateRange}
    />
  )
}

const time = () => {
  const [dateRange, setDateRange] = React.useState<{
    start?: string
    end?: string
  }>({
    start: undefined,
    end: undefined,
  })

  return (
    <DateTimeRangePicker
      mode="time"
      onChangeValue={(date) => {
        console.log('change value called', date)
        setDateRange(date)
      }}
      value={dateRange}
      minutesEnabled={false}
    />
  )
}

const time24 = () => {
  const [dateRange, setDateRange] = React.useState<{
    start?: string
    end?: string
  }>({
    start: undefined,
    end: undefined,
  })

  return (
    <DateTimeRangePicker
      is24
      mode="time"
      onChangeValue={(date) => {
        console.log('change value called', date)
        setDateRange(date)
      }}
      value={dateRange}
    />
  )
}

const updateTimestampsOutsidePicker = () => {
  const [dateRange, setDateRange] = React.useState<{
    start?: string
    end?: string
  }>({
    start: undefined,
    end: undefined,
  })

  return (
    <div>
      <Button
        onClick={() =>
          setDateRange({
            start: dayjs(dateRange.start).add(1, 'day').toISOString(),
            end: dateRange.end,
          })
        }
      >
        Add day to start day
      </Button>
      <Button
        onClick={() =>
          setDateRange({
            start: dateRange.start,
            end: dayjs(dateRange.end).add(1, 'day').toISOString(),
          })
        }
      >
        Add day to end day
      </Button>
      <DateTimeRangePicker
        is24
        onChangeValue={(date) => {
          console.log('change value called', date)
          setDateRange(date)
        }}
        value={dateRange}
      />
    </div>
  )
}

const noInitialValue = () => {
  const [dateRange, setDateRange] = React.useState<{
    start?: string
    end?: string
  }>({
    start: undefined,
    end: undefined,
  })

  return (
    <DateTimeRangePicker
      mode="datetime"
      onChangeValue={(date) => {
        console.log('change value called', date)
        setDateRange(date)
      }}
      value={dateRange}
    />
  )
}

const updateValueOnClickOutside = () => {
  const containerRef = useRef(null)
  const [isOpen, setIsOpen] = useState(false)

  const [dateRange, setDateRange] = React.useState<{
    start?: string
    end?: string
  }>({
    start: undefined,
    end: undefined,
  })

  return (
    <div>
      <div>
        Value: {dateRange.start || 'nostart '} {dateRange.start || 'noend'}
      </div>

      <div className="my-base">
        The expected behavior is that when you select a date range and click
        outside of the parent dropdown the DateTimeRangePicker will trigger the
        onChangeValue.
      </div>

      <Button onClick={() => setIsOpen(!isOpen)}>Toggle Dropdown!</Button>

      <div
        className="relative ml-auto w-[300px] bg-caution-faded text-center"
        ref={containerRef}
      >
        <span className="italic tracking-wide text-color-subtle">
          Dropdown Container
        </span>

        <Dropdown
          containerRef={containerRef}
          isOpen={isOpen}
          onClose={() => setIsOpen(false)}
          onOpen={() => setIsOpen(true)}
        >
          <div className="w-96">
            <DateTimeRangePicker
              mode="datetime"
              onChangeValue={(date) => {
                console.log('change value called', date)
                setDateRange(date)
              }}
              value={dateRange}
            />
          </div>
        </Dropdown>
      </div>
    </div>
  )
}

const dateEndTimePredefined = () => {
  const [dateRange, setDateRange] = React.useState<{
    start?: string
    end?: string
  }>({
    start: undefined,
    end: new Date().toISOString(),
  })

  return (
    <DateTimeRangePicker
      mode="date"
      onChangeValue={(date) => {
        console.log('change value called', date)
        setDateRange(date)
      }}
      value={dateRange}
    />
  )
}

const today = dayjs()
const disableDatesAfterDate = today.add(7, 'day').toISOString()
const disableDatesBeforeDate = today.subtract(7, 'day').toISOString()

const datesBlocked = () => {
  const [dateRange, setDateRange] = React.useState<{
    start?: string
    end?: string
  }>({
    start: undefined,
    end: new Date().toISOString(),
  })

  return (
    <DateTimeRangePicker
      disableDatesAfterDate={disableDatesAfterDate}
      disableDatesBeforeDate={disableDatesBeforeDate}
      onChangeValue={(date) => {
        console.log('change value called', date)
        setDateRange(date)
      }}
      value={dateRange}
    />
  )
}

export default {
  datetime,
  date,
  time,
  time24,
  updateTimestampsOutsidePicker,
  noInitialValue,
  updateValueOnClickOutside,
  dateEndTimePredefined,
  datesBlocked,
}
