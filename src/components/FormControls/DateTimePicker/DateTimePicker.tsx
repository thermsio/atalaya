import React, { useEffect, useMemo } from 'react'
import classNameParser from 'classnames'

import {
  FormControlWrapper,
  FormControlWrapperPublicProps,
} from '../../FormLayout/components/FormControlWrapper'

import { FormControlCommonProps } from '../types'
import { StatusIcons } from '../components/StatusIcons'
import { getDate } from '../../../utils/date'
import { DatePicker } from '../../DatePicker'
import { Dropdown } from '../../Dropdown'
import {
  DateTimePickerProvider,
  DateTimePickerProviderProps,
  PICKER_MODES,
  useDateTimePickerContext,
} from './context/DateTimePickerContext'
import { Icon } from '../../Icon'
import { Button } from '../../Button'
import { DateTimeDisplayContextProvider } from '../../DateTime'
import { DateTimeDisplayContextProviderProps } from '../../DateTime/DateTimeDisplayContext'
import '../FormControls.css'
import classNames from 'classnames'
import { TimeBase } from '../TimePicker/components/TimeBase'

export interface PickerProps
  extends FormControlCommonProps,
    FormControlWrapperPublicProps {
  onBlur?: (e: React.BaseSyntheticEvent) => void
  onFocus?: (e: React.BaseSyntheticEvent) => void
  onDateChange?: (date: string) => void
  /* If false, no initial date/time will be set */
  noInitialValue?: boolean
  placeholder?: string
  onChangeValue?: (date?: string) => void
  showClearValueButton?: boolean
  value?: string
}

const Picker = ({
  disabled,
  error,
  hint,
  label,
  labelBehavior,
  maxWidth,
  minWidth,
  name,
  noInitialValue,
  onBlur,
  onChangeValue,
  onFocus,
  showErrorMessage,
  showClearValueButton = true,
  subText,
  valid,
}: PickerProps): React.ReactElement => {
  const {
    disableDatesAfterDate,
    disableDatesBeforeDate,
    is24,
    mode,
    timestamp,
    updateTimestamp,
    weekStart,
  } = useDateTimePickerContext()

  useEffect(() => {
    if (!noInitialValue) {
      if (onChangeValue) {
        const now = new Date()
        now.setMilliseconds(0)
        now.setSeconds(0)

        onChangeValue(timestamp || now.toISOString())
      }
    }
  }, [])

  const readableDate = useMemo(() => {
    if (!timestamp) return '--- -, ----'

    return getDate(timestamp)
  }, [is24, mode, timestamp])

  return (
    <FormControlWrapper
      error={error}
      disabled={disabled}
      hasValue={!!timestamp}
      hint={hint}
      inputName={name}
      label={label}
      labelBehavior={labelBehavior}
      maxWidth={maxWidth}
      minWidth={minWidth}
      showErrorMessage={showErrorMessage}
      subText={subText}
    >
      <div
        className={classNameParser(
          {
            'form-field-disabled': disabled,
            'form-field-error': error,
            'form-field-success': valid,
          },
          'form-field-input-wrapper flex items-center',
        )}
        onBlur={onBlur}
        onFocus={onFocus}
      >
        {mode.match(/date/i) && (
          <Dropdown
            controller={({ close, isOpen, open }) => (
              <Button
                className="form-field"
                disabled={disabled}
                onClick={isOpen ? close : open}
                subtle
              >
                <div className="flex items-center font-normal text-color text-sm py-xxs">
                  <span
                    className={classNames({ 'text-color-subtle': !timestamp })}
                  >
                    {readableDate}
                  </span>

                  <Icon className="ml-xs" color="neutral" size="xs">
                    <svg
                      fill="currentColor"
                      viewBox="0 0 24 24"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M0 0h24v24H0V0z" fill="none" />
                      <path d="M19 3h-1V2c0-.55-.45-1-1-1s-1 .45-1 1v1H8V2c0-.55-.45-1-1-1s-1 .45-1 1v1H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-1 16H6c-.55 0-1-.45-1-1V8h14v10c0 .55-.45 1-1 1zM8 10h3c.55 0 1 .45 1 1v3c0 .55-.45 1-1 1H8c-.55 0-1-.45-1-1v-3c0-.55.45-1 1-1z" />
                    </svg>
                  </Icon>
                </div>
              </Button>
            )}
            disabled={disabled}
            dropdownPlace="bottom-left"
            dropdownContainerClass="shadow"
            onClose={() => {
              onChangeValue?.(timestamp)
            }}
          >
            <DatePicker
              disableDatesAfterDate={disableDatesAfterDate}
              disableDatesBeforeDate={disableDatesBeforeDate}
              onChangeValue={updateTimestamp}
              timestamp={timestamp}
              weekStart={weekStart}
            />
          </Dropdown>
        )}

        {mode.match(/time/i) && (
          <TimeBase
            hideClearButton
            is24={is24}
            name={`${name}/time`}
            onChangeValue={(newValue) => {
              updateTimestamp(newValue)
              onChangeValue?.(newValue)
            }}
            value={timestamp}
          />
        )}

        <div className="ml-auto flex items-center">
          {timestamp && !disabled && showClearValueButton && (
            <Button
              className="mr-xxs shrink-0"
              subtle
              onClick={() => {
                updateTimestamp(undefined)
                onChangeValue?.(undefined)
              }}
            >
              <Icon size="xs">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  height="24px"
                  viewBox="0 0 24 24"
                  width="24px"
                  fill="currentColor"
                >
                  <path d="M0 0h24v24H0V0z" fill="none" />
                  <path d="M18.3 5.71c-.39-.39-1.02-.39-1.41 0L12 10.59 7.11 5.7c-.39-.39-1.02-.39-1.41 0-.39.39-.39 1.02 0 1.41L10.59 12 5.7 16.89c-.39.39-.39 1.02 0 1.41.39.39 1.02.39 1.41 0L12 13.41l4.89 4.89c.39.39 1.02.39 1.41 0 .39-.39.39-1.02 0-1.41L13.41 12l4.89-4.89c.38-.38.38-1.02 0-1.4z" />
                </svg>
              </Icon>
            </Button>
          )}

          {(!!error || !!valid) && (
            <>
              <StatusIcons error={error} valid={valid} />
              <div className="mr-xs" />
            </>
          )}
        </div>
      </div>
    </FormControlWrapper>
  )
}

export interface DateTimePickerProps
  extends PickerProps,
    Omit<DateTimeDisplayContextProviderProps, 'children' | 'is24hr'>,
    Omit<DateTimePickerProviderProps, 'children' | 'mode'> {
  mode?: 'date' | 'datetime' | 'time'
  value?: string
}

const modesMap = {
  date: PICKER_MODES.SINGLE_DATE,
  datetime: PICKER_MODES.SINGLE_DATE_TIME,
  time: PICKER_MODES.SINGLE_TIME,
}

const DateTimePicker = ({
  disableDatesAfterDate,
  disableDatesBeforeDate,
  dateFormat = 'MM/DD/YY',
  timeFormat = 'h:mm A',
  is24 = false,
  minuteStep,
  mode = 'datetime',
  value,
  weekStart,
  timezone,
  ...PickerProps
}: DateTimePickerProps) => (
  <DateTimeDisplayContextProvider
    dateFormat={dateFormat}
    is24hr={is24}
    timeFormat={timeFormat}
    timezone={timezone}
  >
    <DateTimePickerProvider
      disableDatesAfterDate={disableDatesAfterDate}
      disableDatesBeforeDate={disableDatesBeforeDate}
      is24={is24}
      minuteStep={minuteStep}
      mode={modesMap[mode]}
      timestamp={value}
      timestampActiveView={value}
      weekStart={weekStart}
    >
      <Picker {...PickerProps} />
    </DateTimePickerProvider>
  </DateTimeDisplayContextProvider>
)

export { DateTimePicker }
