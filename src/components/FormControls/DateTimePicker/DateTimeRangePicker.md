```jsx
const [date, setDate] = React.useState({ start: undefined, end: undefined })

;<DateTimeRangePicker mode="datetime" value={date} onChangeValue={setDate} />
```

### 24-Hour Clock

```jsx
const [date, setDate] = React.useState({ start: undefined, end: undefined })

;<DateTimeRangePicker
  is24
  mode="datetime"
  value={date}
  onChangeValue={setDate}
/>
```

### Date Only

```jsx
const [date, setDate] = React.useState({ start: undefined, end: undefined })

;<DateTimeRangePicker mode="date" value={date} onChangeValue={setDate} />
```

### Time Only

```jsx
const [date, setDate] = React.useState({ start: undefined, end: undefined })

;<DateTimeRangePicker mode="time" value={date} onChangeValue={setDate} />
```

### Narrow Display

```jsx
const [date, setDate] = React.useState({ start: undefined, end: undefined })

;<div style={{ width: '350px' }}>
  <DateTimeRangePicker value={date} onChangeValue={setDate} />
</div>
```
