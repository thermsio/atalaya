```jsx
const [date, setDate] = React.useState('2021-05-31T17:56:39.975Z')

;<DateTimePicker value={date} onChangeValue={setDate} />
```

### 24-Hour Clock

```jsx
const [date, setDate] = React.useState('2021-05-31T17:56:39.975Z')

;<DateTimePicker is24 value={date} onChangeValue={setDate} />
```

### Date Only

```jsx
const [date, setDate] = React.useState('2021-05-31T17:56:39.975Z')

;<DateTimePicker mode="date" value={date} onChangeValue={setDate} />
```

### Time Only

```jsx
const [date, setDate] = React.useState('2021-05-31T17:56:39.975Z')

;<DateTimePicker mode="time" value={date} onChangeValue={setDate} />
```

### Narrow Display

```jsx
const [date, setDate] = React.useState('2021-05-31T17:56:39.975Z')

;<div style={{ width: '350px' }}>
  <DateTimePicker value={date} onChangeValue={setDate} />
</div>
```
