import React, { useMemo } from 'react'
import classNameParser from 'classnames'
import {
  FormControlWrapper,
  FormControlWrapperPublicProps,
} from '../../FormLayout/components/FormControlWrapper'
import { FormControlCommonProps } from '../types'
import { StatusIcons } from '../components/StatusIcons'
import { getDate } from '../../../utils/date'
import { Icon } from '../../Icon'
import { DatePicker } from '../../DatePicker'
import '../FormControls.css'
import { Dropdown } from '../../Dropdown'
import {
  DateTimePickerProvider,
  DateTimePickerProviderProps,
  PICKER_MODES,
  useDateTimePickerContext,
} from './context/DateTimePickerContext'
import { Button } from '../../Button'
import {
  DateTimeDisplayContextProvider,
  DateTimeDisplayContextProviderProps,
} from '../../DateTime/DateTimeDisplayContext'
import { useUpdateEffect } from 'ahooks'
import classNames from 'classnames'
import { TimeBase } from '../TimePicker/components/TimeBase'

export interface RangePickerProps
  extends FormControlCommonProps,
    FormControlWrapperPublicProps {
  onBlur?: (e: React.BaseSyntheticEvent) => void
  onFocus?: (e: React.BaseSyntheticEvent) => void
  onDateChange?: (date: string) => void
  placeholderStartDate?: string
  placeholderEndDate?: string
  onChangeValue?: (date: { start?: string; end?: string }) => void
  /** Control the behavior when onChangeValue() prop is called. Defaults to "StartAndEnd" required to be set before calling onChangeValue(). In some cases the start or end date are optional and the parent component needs to be notified of the start/end value when it changes.  */
  onChangeValueBehavior?: 'StartAndEnd' | 'StartOrEnd'
  onClearValue?: () => void
  showClearValueButton?: boolean
  value?: { start?: string; end?: string }
  hoursEnabled?: boolean
  minutesEnabled?: boolean
}

const RangePicker = ({
  disabled,
  error,
  hint,
  label,
  labelBehavior,
  maxWidth,
  minWidth,
  name,
  onBlur,
  onChangeValue,
  onChangeValueBehavior = 'StartAndEnd',
  onClearValue,
  onFocus,
  placeholderStartDate = '--- -, ----',
  placeholderEndDate = '--- -, ----',
  showErrorMessage,
  showClearValueButton = true,
  subText,
  valid,
}: RangePickerProps): React.ReactElement => {
  const {
    disableDatesAfterDate,
    disableDatesBeforeDate,
    is24,
    mode,
    timestampEnd,
    timestampStart,
    updateTimestampEnd,
    updateTimestampStart,
    weekStart,
  } = useDateTimePickerContext()

  useUpdateEffect(() => {
    if (!onChangeValue) return

    let call = false

    if (onChangeValueBehavior === 'StartAndEnd') {
      call = Boolean(timestampStart && timestampEnd)
    } else if (onChangeValueBehavior === 'StartOrEnd') {
      call = true
    } else {
      console.warn(
        `DateTimeRangePicker onChangeValueBehavior="${onChangeValueBehavior}" not supported`,
      )
    }

    if (call) {
      onChangeValue({ start: timestampStart, end: timestampEnd })
    }
  }, [timestampEnd, timestampStart])

  const endDateTime = useMemo(() => {
    if (!timestampEnd) return ''

    return getDate(timestampEnd)
  }, [timestampEnd])

  const startDateTime = useMemo(() => {
    if (!timestampStart) return ''

    return getDate(timestampStart)
  }, [timestampStart])

  return (
    <FormControlWrapper
      error={error}
      disabled={disabled}
      hasValue={Boolean(timestampStart || timestampEnd)}
      hint={hint}
      inputName={name}
      label={label}
      labelBehavior={labelBehavior}
      maxWidth={maxWidth}
      minWidth={minWidth}
      showErrorMessage={showErrorMessage}
      subText={subText}
    >
      <div
        className={classNameParser(
          {
            'form-field-disabled': disabled,
            'form-field-error': error,
            'form-field-success': valid,
          },
          'form-field-input-wrapper flex flex-wrap items-center',
        )}
        onBlurCapture={onBlur}
        onFocus={onFocus}
      >
        <span className="flex justify-center items-center min-w-fit">
          {mode.match(/date/i) && (
            <span className="min-w-fit">
              <Dropdown
                controller={({ close, isOpen, open }) => (
                  <Button
                    className="form-field"
                    disabled={disabled}
                    onClick={isOpen ? close : open}
                    subtle
                  >
                    <div className="flex items-center py-xxs text-sm font-normal text-color">
                      <span
                        className={classNames({
                          'text-color-subtle': !timestampStart,
                        })}
                      >
                        {timestampStart ? startDateTime : placeholderStartDate}
                      </span>

                      <Icon className="ml-xs" color="neutral" size="xs">
                        <svg
                          fill="currentColor"
                          viewBox="0 0 24 24"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path d="M0 0h24v24H0V0z" fill="none" />
                          <path d="M19 3h-1V2c0-.55-.45-1-1-1s-1 .45-1 1v1H8V2c0-.55-.45-1-1-1s-1 .45-1 1v1H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-1 16H6c-.55 0-1-.45-1-1V8h14v10c0 .55-.45 1-1 1zM8 10h3c.55 0 1 .45 1 1v3c0 .55-.45 1-1 1H8c-.55 0-1-.45-1-1v-3c0-.55.45-1 1-1z" />
                        </svg>
                      </Icon>
                    </div>
                  </Button>
                )}
                disabled={disabled}
                dropdownPlace="bottom-left"
                dropdownContainerClass="shadow"
              >
                <DatePicker
                  disableDatesAfterDate={disableDatesAfterDate}
                  disableDatesBeforeDate={disableDatesBeforeDate}
                  rangePick
                  timestampStart={timestampStart}
                  timestampEnd={timestampEnd}
                  onChangeValue={(newValue) => {
                    updateTimestampStart(newValue.start)
                    updateTimestampEnd(newValue.end)
                  }}
                  weekStart={weekStart}
                />
              </Dropdown>
            </span>
          )}

          {mode.match(/time/i) && (
            <span className="min-w-fit">
              <TimeBase
                hideClearButton
                is24={is24}
                name={`${name}/time`}
                onChangeValue={(newStart) => {
                  updateTimestampStart(newStart)
                }}
                value={timestampStart}
              />
            </span>
          )}

          <Icon className="" color="subtle" size="sm">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              enableBackground="new 0 0 24 24"
              height="24px"
              viewBox="0 0 24 24"
              width="24px"
              fill="currentColor"
            >
              <g>
                <rect fill="none" height="24" width="24" />
                <rect fill="none" height="24" width="24" />
              </g>
              <g>
                <g>
                  <path d="M20.08,11.42l-4.04-5.65C15.7,5.29,15.15,5,14.56,5h0c-1.49,0-2.35,1.68-1.49,2.89L16,12l-2.93,4.11 c-0.87,1.21,0,2.89,1.49,2.89h0c0.59,0,1.15-0.29,1.49-0.77l4.04-5.65C20.33,12.23,20.33,11.77,20.08,11.42z" />
                  <path d="M13.08,11.42L9.05,5.77C8.7,5.29,8.15,5,7.56,5h0C6.07,5,5.2,6.68,6.07,7.89L9,12l-2.93,4.11C5.2,17.32,6.07,19,7.56,19h0 c0.59,0,1.15-0.29,1.49-0.77l4.04-5.65C13.33,12.23,13.33,11.77,13.08,11.42z" />
                </g>
              </g>
            </svg>
          </Icon>
        </span>

        <span className="flex min-w-fit">
          {mode.match(/date/i) && (
            <span className="min-w-fit">
              <Dropdown
                controller={({ close, isOpen, open }) => (
                  <Button
                    className="form-field"
                    disabled={disabled}
                    onClick={isOpen ? close : open}
                    subtle
                  >
                    <div className="flex items-center py-xxs text-sm font-normal text-color">
                      <span
                        className={classNames({
                          'text-color-neutral-faded': !timestampEnd,
                        })}
                      >
                        {timestampEnd ? endDateTime : placeholderEndDate}
                      </span>

                      <Icon className="ml-xs" color="neutral" size="xs">
                        <svg
                          fill="currentColor"
                          viewBox="0 0 24 24"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path d="M0 0h24v24H0V0z" fill="none" />
                          <path d="M19 3h-1V2c0-.55-.45-1-1-1s-1 .45-1 1v1H8V2c0-.55-.45-1-1-1s-1 .45-1 1v1H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-1 16H6c-.55 0-1-.45-1-1V8h14v10c0 .55-.45 1-1 1zM8 10h3c.55 0 1 .45 1 1v3c0 .55-.45 1-1 1H8c-.55 0-1-.45-1-1v-3c0-.55.45-1 1-1z" />
                        </svg>
                      </Icon>
                    </div>
                  </Button>
                )}
                disabled={disabled}
                dropdownPlace="bottom-left"
                dropdownContainerClass="shadow"
              >
                <DatePicker
                  disableDatesAfterDate={disableDatesAfterDate}
                  disableDatesBeforeDate={disableDatesBeforeDate}
                  rangePick
                  timestampStart={timestampStart}
                  timestampEnd={timestampEnd}
                  onChangeValue={(newValue) => {
                    updateTimestampStart(newValue.start)
                    updateTimestampEnd(newValue.end)
                  }}
                  weekStart={weekStart}
                />
              </Dropdown>
            </span>
          )}

          {mode.match(/time/i) && (
            <span className="min-w-fit">
              <TimeBase
                hideClearButton
                is24={is24}
                name={`${name}/time`}
                onChangeValue={(newEnd) => {
                  updateTimestampEnd(newEnd)
                }}
                value={timestampEnd}
              />
            </span>
          )}

          <div className=" flex items-center">
            {!disabled &&
              showClearValueButton &&
              (timestampStart || timestampEnd) && (
                <Button
                  className="mr-xxs shrink-0"
                  subtle
                  onClick={() => {
                    updateTimestampStart('')
                    updateTimestampEnd('')
                    onClearValue?.()
                  }}
                >
                  <Icon size="xs">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      height="24px"
                      viewBox="0 0 24 24"
                      width="24px"
                      fill="currentColor"
                    >
                      <path d="M0 0h24v24H0V0z" fill="none" />
                      <path d="M18.3 5.71c-.39-.39-1.02-.39-1.41 0L12 10.59 7.11 5.7c-.39-.39-1.02-.39-1.41 0-.39.39-.39 1.02 0 1.41L10.59 12 5.7 16.89c-.39.39-.39 1.02 0 1.41.39.39 1.02.39 1.41 0L12 13.41l4.89 4.89c.39.39 1.02.39 1.41 0 .39-.39.39-1.02 0-1.41L13.41 12l4.89-4.89c.38-.38.38-1.02 0-1.4z" />
                    </svg>
                  </Icon>
                </Button>
              )}

            {(!!error || !!valid) && (
              <>
                <StatusIcons error={error} valid={valid} />
                <div className="mr-xs" />
              </>
            )}
          </div>
        </span>
      </div>
    </FormControlWrapper>
  )
}

export interface DateTimeRangePickerProps
  extends RangePickerProps,
    Omit<DateTimeDisplayContextProviderProps, 'children' | 'is24hr'>,
    Omit<DateTimePickerProviderProps, 'children' | 'mode'> {
  mode?: 'date' | 'datetime' | 'time'
  value?: { start?: string; end?: string }
}

const modesMap = {
  date: PICKER_MODES.RANGE_DATE,
  datetime: PICKER_MODES.RANGE_DATE_TIME,
  time: PICKER_MODES.RANGE_TIME,
}

const DateTimeRangePicker = ({
  disableDatesAfterDate,
  disableDatesBeforeDate,
  dateFormat = 'MM/DD/YY',
  timeFormat = 'h:mm A',
  is24 = false,
  minuteStep,
  mode = 'datetime',
  value,
  weekStart,
  timezone,
  ...RangePickerProps
}: DateTimeRangePickerProps) => (
  <DateTimeDisplayContextProvider
    dateFormat={dateFormat}
    is24hr={is24}
    timeFormat={timeFormat}
    timezone={timezone}
  >
    <DateTimePickerProvider
      disableDatesAfterDate={disableDatesAfterDate}
      disableDatesBeforeDate={disableDatesBeforeDate}
      is24={is24}
      minuteStep={minuteStep}
      mode={modesMap[mode]}
      timestampActiveView={value?.start}
      timestampStart={value?.start}
      timestampEnd={value?.end}
      weekStart={weekStart}
    >
      <RangePicker {...RangePickerProps} />
    </DateTimePickerProvider>
  </DateTimeDisplayContextProvider>
)

export { DateTimeRangePicker }
