```jsx
const [value, setValue] = React.useState()

;<WeekDaysPicker
  label="Week Days Picker"
  hint="This is the hint"
  subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
  fullWidth
  value={value}
  onChangeValue={setValue}
/>
```

The value used for this component must conform to this shape.

```jsx static
{
  sunday: boolean
  monday: boolean
  tuesday: boolean
  wednesday: boolean
  thursday: boolean
  friday: boolean
  saturday: boolean
}
```
