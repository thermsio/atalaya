import React from 'react'
import { FormLayout } from '../../FormLayout'
import { Number } from '../Number/Number'
import { WeekDaysPicker } from './WeekDaysPicker'

const sideBySide = (
  <FormLayout horizontal={false}>
    <div className="flex gap-x-xs">
      <Number
        label="Number"
        hint="Hint"
        subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
      />

      <WeekDaysPicker
        label="WeekDaysPicker"
        hint="Hint"
        subText="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        value={{
          monday: false,
          tuesday: true,
          wednesday: false,
          thursday: true,
          friday: false,
          saturday: true,
          sunday: true,
        }}
      />
    </div>
  </FormLayout>
)

export default { sideBySide }
