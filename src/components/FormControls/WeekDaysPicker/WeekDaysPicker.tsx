import React, { useCallback } from 'react'
import {
  FormControlWrapper,
  FormControlWrapperPublicProps,
} from '../../FormLayout/components/FormControlWrapper'
import { FormControlCommonProps } from '../types'
import '../FormControls.css'
import { WeekDays } from '../../WeekDays'
import { createMockReactSyntheticEvent } from '../../../utils/mock-react-synthetic-event'

const defaultWeek = {
  sunday: false,
  monday: false,
  tuesday: false,
  wednesday: false,
  thursday: false,
  friday: false,
  saturday: false,
}

type Week = {
  sunday: boolean
  monday: boolean
  tuesday: boolean
  wednesday: boolean
  thursday: boolean
  friday: boolean
  saturday: boolean
}

export interface WeekDaysPickerProps
  extends FormControlCommonProps,
    FormControlWrapperPublicProps {
  firstDayOfWeek?: 'monday' | 'sunday'
  fullWidth?: boolean
  value?: Week
}

const WeekDaysPicker = ({
  error,
  disabled,
  firstDayOfWeek,
  fullWidth,
  hint,
  label,
  labelBehavior,
  name,
  onChange,
  onChangeValue,
  showErrorMessage,
  subText,
  value = defaultWeek,
}: WeekDaysPickerProps): React.ReactElement => {
  const handleOnChange = useCallback(
    (newValue: Week) => {
      if (onChangeValue) onChangeValue(newValue)

      if (onChange)
        onChange(
          // @ts-ignore
          createMockReactSyntheticEvent({
            value: newValue,
          }),
        )
    },
    [onChange, onChangeValue],
  )

  return (
    <FormControlWrapper
      error={error}
      disabled={disabled}
      hasValue={!!value}
      hint={hint}
      label={label}
      labelBehavior={labelBehavior}
      inputName={name}
      showErrorMessage={showErrorMessage}
      subText={subText}
    >
      <WeekDays
        disabled={disabled}
        firstDayOfWeek={firstDayOfWeek}
        fullWidth={fullWidth}
        onChangeValue={handleOnChange}
        value={value}
      />
    </FormControlWrapper>
  )
}

export { WeekDaysPicker }
