This component is used for creating lists of values - adding/removing.

```jsx
const [options, setOptions] = React.useState(
  // new Array(15).fill(null).map((n, i) => ({ label: `Optiopn ${i}` })),
  [
    { label: 'Test 1' },
    { label: 'Testing 2' },
    { label: 'Some other option' },
    { label: 'And a final option' },
  ],
)

;<div style={{ width: '300px' }}>
  <ListCreator
    label="Example Options List Creator"
    onAddOption={(option) => {
      setOptions([...options, option])
    }}
    onRemoveOption={(option) => {
      const newOptions = options.filter((opt) => opt.label !== option.label)
      setOptions(newOptions)
    }}
    options={options}
  />
</div>
```
