# <ListCreator />

This component is used to display a list of rows and a text input 
at the top. The primary use for this component is for a user to 
be able to add/search simple string "options" in a list. The user 
can also remove "options".

![img.png](img.png)