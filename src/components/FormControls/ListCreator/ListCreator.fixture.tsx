import React, { useState } from 'react'
import { ListCreator } from './ListCreator'

// eslint-disable-next-line react/display-name
export default function () {
  const [options, setOptions] = useState(
    // new Array(15).fill(null).map((n, i) => ({ label: `Optiopn ${i}` })),
    [
      { label: 'cory' },
      { label: 'cory william' },
      { label: 'cory robinson' },
      { label: ' cory' },
    ],
  )

  return (
    <div style={{ width: '300px' }}>
      <ListCreator
        label="Example Options List Creator"
        onAddOption={(option) => {
          setOptions([...options, option])
        }}
        onOptionsOrderChange={setOptions}
        onRemoveOption={(option) => {
          const newOptions = options.filter((opt) => opt.label !== option.label)
          setOptions(newOptions)
        }}
        options={options}
      />
    </div>
  )
}
