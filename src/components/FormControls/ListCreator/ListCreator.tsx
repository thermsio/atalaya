import React, { useEffect, useMemo, useState } from 'react'
import SortableList, { SortableItem } from 'react-easy-sort'
import { arrayMoveImmutable } from 'array-move'
import { Text } from '../Text/Text'
import { Button } from '../../Button'
import {
  FormControlWrapper,
  FormControlWrapperPublicProps,
} from '../../FormLayout/components/FormControlWrapper'
import classNames from 'classnames'
import { FormControlCommonProps } from '../types'
import { useKeyPress } from '../../../hooks/useKeyPress'
import { escapeRegExp } from '../../../utils/escape-regex-string'

type ListOption = { label: string; value?: string; [key: string]: any }
type InternalListOption = ListOption & { subtle?: boolean }

export interface ListCreatorProps
  extends FormControlCommonProps,
    FormControlWrapperPublicProps {
  label?: string
  /* By default, the max-height is restricted with overflow-y-scroll. If `false` no max-height will be set, otherwise, a valid CSS height value can be passed, ie: "300px" */
  maxHeight?: false | number | string
  onAddOption: (option: ListOption) => void
  /* Optional callback, if provided the options list can be sorted by drag-and-drop */
  onOptionsOrderChange?: (options: ListOption[]) => void
  onRemoveOption: (option: ListOption, index: number) => void
  options: ListOption[]
}

const ListCreator: React.FC<ListCreatorProps> = ({
  error,
  disabled,
  hint,
  label,
  labelBehavior,
  maxHeight = true,
  name,
  onAddOption,
  onBlur,
  onFocus,
  onOptionsOrderChange,
  onRemoveOption,
  options,
  subText,
  showErrorMessage,
}) => {
  const [text, setText] = useState('')

  const canAdd = useMemo(() => {
    // we don't allow duplicates in the options list
    return !!text && !options?.find(({ label }) => label === text)
  }, [options, text])

  const enterPressed = useKeyPress('Enter')

  useEffect(() => {
    if (enterPressed && canAdd) {
      onAddOption({ label: text })
      setText('')
    }
  }, [enterPressed])

  const internalOptionsList = useMemo(() => {
    if (text?.length === 0) return options

    return options
      .map((opt) => {
        const newOpt: InternalListOption = { ...opt }

        newOpt.subtle = !opt.label.match(escapeRegExp(text))

        return newOpt
      })
      .sort((o1, o2) => (o1.label < o2.label ? 1 : -1))
      .sort((o1, o2) => {
        if (o1.subtle && !o2.subtle) return 1
        else return -1
      })
  }, [options, text])

  const onSortEnd = (oldIndex: number, newIndex: number) => {
    if (onOptionsOrderChange) {
      onOptionsOrderChange(arrayMoveImmutable(options, oldIndex, newIndex))
    }
  }

  return (
    <FormControlWrapper
      error={error}
      disabled={disabled}
      hasValue={Boolean(options?.length)}
      hint={hint}
      inputName={name}
      label={label}
      labelBehavior={labelBehavior}
      subText={subText}
      showErrorMessage={showErrorMessage}
    >
      <div>
        <div className="flex">
          <div className="flex-grow">
            <Text
              onBlur={onBlur}
              onFocus={onFocus}
              onChangeValue={setText}
              placeholder="Add or find option..."
              search
              value={text}
            />
          </div>

          <div className="flex flex-col justify-end">
            <Button
              className="ml-0.5"
              disabled={!canAdd}
              onClick={() => {
                onAddOption({ label: text })
                setText('')
              }}
              size="sm"
              subtle
              variant="positive"
            >
              Add
            </Button>
          </div>
        </div>

        {options.length > 10 && (
          <div className="text-center text-sm text-color-subtle">
            <em>{options.length} options</em>
          </div>
        )}

        <div
          className={`${
            maxHeight ? 'max-h-96' : ''
          } overflow-y-auto overflow-x-hidden`}
          // allow a raw css maxHeight value to be passed
          style={typeof maxHeight !== 'boolean' ? { maxHeight } : {}}
        >
          <SortableList
            onSortEnd={onSortEnd}
            draggedItemClassName="hover:cursor-grabbing"
          >
            {internalOptionsList?.map((option: InternalListOption, i) => (
              <SortableItem key={option.label + option.value}>
                <div
                  className={classNames(
                    `flex py-0.5 pt-1 hover:bg-surface-subtle`,
                    { 'text-color-subtle': option.subtle },
                    { 'hover:cursor-grab': !!onOptionsOrderChange },
                  )}
                  key={option.label + option.value}
                >
                  <div className="flex-grow overflow-hidden truncate text-ellipsis">
                    {option.label}
                  </div>

                  <Button
                    onClick={() => onRemoveOption(option, i)}
                    size="sm"
                    subtle
                    variant="critical"
                  >
                    Remove
                  </Button>
                </div>
              </SortableItem>
            ))}
          </SortableList>
        </div>
      </div>
    </FormControlWrapper>
  )
}

export { ListCreator }
