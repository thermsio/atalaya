import React, { useEffect, useRef, useState } from 'react'
import { Button, ButtonProps } from './index'

export interface ButtonTurboProps extends Omit<ButtonProps, 'onClick'> {
  interval?: number
  onHeld: () => void
}

/** Instead of calling a function onClick, it will repeatedly call the function defined in onHeld */
function ButtonTurbo({
  children,
  interval = 50,
  onHeld,
  ...buttonProps
}: ButtonTurboProps) {
  const [isPressing, setIsPressing] = useState(false)
  const [ticksCount, setTicksCount] = useState(0)

  const savedCallback = useRef<() => void>()

  useEffect(() => {
    savedCallback.current = onHeld
  }, [onHeld])

  useEffect(() => {
    const tick = () => {
      if (ticksCount <= 20) {
        setTicksCount((previousCount) => previousCount + 1)
      }
      savedCallback.current?.()
    }

    let _interval = 400
    if (ticksCount > 0) {
      _interval = interval
    }

    if (isPressing) {
      const id = setInterval(tick, isPressing ? _interval : undefined)
      return () => clearInterval(id)
    } else {
      setTicksCount(0)
    }
  }, [isPressing, ticksCount])

  return (
    <Button
      {...buttonProps}
      onKeyDown={(e) => {
        if (e.key === ' ') {
          onHeld()
          setIsPressing(true)
        }
      }}
      onKeyUp={(e) => {
        if (e.key === ' ') {
          setIsPressing(false)
        }
      }}
      onMouseDown={() => {
        onHeld()
        setIsPressing(true)
      }}
      onMouseUp={() => setIsPressing(false)}
    >
      {children}
    </Button>
  )
}

export { ButtonTurbo }
