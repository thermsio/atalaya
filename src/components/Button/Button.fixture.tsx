import React from 'react'
import { Button } from './index'
import { ButtonGroup } from '../ButtonGroup'

const subtleSolid = (
  <ButtonGroup>
    <Button subtle>Subtle</Button>
    <Button>Solid</Button>
  </ButtonGroup>
)

const outlineSolid = (
  <ButtonGroup>
    <Button outline>Outline</Button>
    <Button>Solid</Button>
  </ButtonGroup>
)

const nonButtonGroup = (
  <div className="flex space-x-xs">
    <Button outline>Outline</Button>
    <Button>Solid</Button>
  </div>
)

const activeButtons = (
  <div className="flex space-x-xs">
    <Button active>Solid</Button>
    <Button active outline>
      Outline
    </Button>
    <Button active subtle>
      Subtle
    </Button>
  </div>
)

export default {
  subtleSolid,
  outlineSolid,
  nonButtonGroup,
  activeButtons,
}
