import classNameParser from 'classnames'
import { SemanticVariant } from '../../../constants'

export interface ButtonClassProps {
  /** Will show button as pressed. */
  active?: boolean
  borderRadius?: 'none' | 'default' | 'full'
  className?: string

  // FIXME: only works in flex-row container for now.
  fullWidth?: boolean

  /** Show loading spinner, if a string it's provided it will be shown next to the spinner */
  loading?: string | boolean
  loadingMessage?: React.ReactNode

  outline?: boolean

  size?: 'base' | 'lg' | 'sm' | 'xs'

  subtle?: boolean

  variant?: SemanticVariant
}

export const makeButtonClasses = ({
  active,
  borderRadius,
  className,
  fullWidth,
  loading,
  outline,
  size,
  subtle,
  variant,
}: ButtonClassProps): string => {
  return classNameParser(
    'btn',
    {
      'btn-active': active,
      'btn-caution': variant === 'caution',
      'btn-critical': variant === 'critical',
      'btn-full-width': fullWidth,
      'btn-info': variant === 'info',
      'btn-lg': size === 'lg',
      'btn-loading': loading,
      'btn-main': variant === 'main',
      'btn-neutral': variant === 'neutral',
      'btn-outline': outline,
      'btn-positive': variant === 'positive',
      'btn-rounded-none': borderRadius === 'none',
      'btn-rounded-full': borderRadius === 'full',
      'btn-sm': size === 'sm',
      'btn-solid': !outline && !subtle,
      'btn-subtle': subtle,
      'btn-xs': size === 'xs',
    },
    className,
  )
}
