#### Main

```jsx padded
<div className="flex items-end space-x-sm">
  <Button size="lg" variant="main">
    Large
  </Button>
  <Button variant="main">Normal</Button>
  <Button active variant="main">
    Active
  </Button>
  <Button disabled variant="main">
    Disabled
  </Button>
  <Button size="sm" variant="main">
    Small
  </Button>
  <Button size="xs" variant="main">
    ExtraSmall
  </Button>
  <Button fullWidth variant="main">
    Full Width
  </Button>
</div>
```

#### Main Outlined

```jsx padded
<div className="flex items-end space-x-sm">
  <Button outline size="lg" variant="main">
    Large
  </Button>
  <Button outline variant="main">
    Normal
  </Button>
  <Button active outline variant="main">
    Active
  </Button>
  <Button disabled outline variant="main">
    Disabled
  </Button>
  <Button outline size="sm" variant="main">
    Small
  </Button>
  <Button outline size="xs" variant="main">
    ExtraSmall
  </Button>
  <Button fullWidth outline variant="main">
    Full Width
  </Button>
</div>
```

#### Main Subtle

```jsx padded
<div className="flex items-end space-x-sm">
  <Button subtle size="lg" variant="main">
    Large
  </Button>
  <Button subtle variant="main">
    Normal
  </Button>
  <Button active subtle variant="main">
    Active
  </Button>
  <Button disabled subtle variant="main">
    Disabled
  </Button>
  <Button subtle size="sm" variant="main">
    Small
  </Button>
  <Button subtle size="xs" variant="main">
    ExtraSmall
  </Button>
  <Button fullWidth subtle variant="main">
    Full Width
  </Button>
</div>
```

#### Main Loading

```jsx padded
<div className="space-y-sm">
  <div className="flex items-end space-x-sm">
    <Button size="lg" variant="main" loading="Loading">
      Normal
    </Button>
    <Button variant="main" loading>
      Normal
    </Button>
    <Button variant="main" loading="Loading">
      Normal
    </Button>
    <Button size="sm" variant="main" loading="Loading">
      Small
    </Button>
    <Button loading="Loading" size="xs" variant="main">
      ExtraSmall
    </Button>
  </div>

  <div className="flex items-end space-x-sm">
    <Button outline size="lg" variant="main" loading="Loading">
      Normal
    </Button>
    <Button outline variant="main" loading>
      Normal
    </Button>
    <Button outline variant="main" loading="Loading">
      Normal
    </Button>
    <Button outline size="sm" variant="main" loading="Loading">
      Small
    </Button>
    <Button outline loading="Loading" size="xs" variant="main">
      ExtraSmall
    </Button>
  </div>

  <div className="flex items-end space-x-sm">
    <Button subtle size="lg" variant="main" loading="Loading">
      Normal
    </Button>
    <Button subtle variant="main" loading>
      Normal
    </Button>
    <Button subtle variant="main" loading="Loading">
      Normal
    </Button>
    <Button subtle size="sm" variant="main" loading="Loading">
      Small
    </Button>
    <Button subtle size="xs" variant="main">
      ExtraSmall
    </Button>
  </div>
</div>
```

#### Neutral

```jsx padded
<div className="flex items-end space-x-sm">
  <Button size="lg" variant="neutral">
    Large
  </Button>
  <Button variant="neutral">Normal</Button>
  <Button active variant="neutral">
    Active
  </Button>
  <Button disabled variant="neutral">
    Disabled
  </Button>
  <Button size="sm" variant="neutral">
    Small
  </Button>
  <Button size="xs" variant="neutral">
    ExtraSmall
  </Button>
  <Button fullWidth variant="neutral">
    Full Width
  </Button>
</div>
```

#### Neutral Outlined

```jsx padded
<div className="flex items-end space-x-sm">
  <Button outline size="lg" variant="neutral">
    Large
  </Button>
  <Button outline variant="neutral">
    Normal
  </Button>
  <Button active outline variant="neutral">
    Active
  </Button>
  <Button disabled outline variant="neutral">
    Disabled
  </Button>
  <Button outline size="sm" variant="neutral">
    Small
  </Button>
  <Button fullWidth outline variant="neutral">
    Full Width
  </Button>
</div>
```

#### Neutral Subtle

```jsx padded
<div className="flex items-end space-x-sm">
  <Button subtle size="lg" variant="neutral">
    Large
  </Button>
  <Button subtle variant="neutral">
    Normal
  </Button>
  <Button active subtle variant="neutral">
    Active
  </Button>
  <Button disabled subtle variant="neutral">
    Disabled
  </Button>
  <Button subtle size="sm" variant="neutral">
    Small
  </Button>
  <Button subtle size="xs" variant="neutral">
    ExtraSmall
  </Button>
  <Button fullWidth subtle variant="neutral">
    Full Width
  </Button>
</div>
```

#### Neutral Loading

```jsx padded
<div className="space-y-sm">
  <div className="flex items-end space-x-sm">
    <Button size="lg" variant="neutral" loading="Loading">
      Normal
    </Button>
    <Button variant="neutral" loading>
      Normal
    </Button>
    <Button variant="neutral" loading="Loading">
      Normal
    </Button>
    <Button size="sm" variant="neutral" loading="Loading">
      Small
    </Button>
    <Button size="xs" variant="neutral" loading="Loading">
      ExtraSmall
    </Button>
  </div>

  <div className="flex items-end space-x-sm">
    <Button outline size="lg" variant="neutral" loading="Loading">
      Normal
    </Button>
    <Button outline variant="neutral" loading>
      Normal
    </Button>
    <Button outline variant="neutral" loading="Loading">
      Normal
    </Button>
    <Button outline size="sm" variant="neutral" loading="Loading">
      Small
    </Button>
    <Button outline subtle size="xs" variant="neutral" loading="Loading">
      ExtraSmall
    </Button>
  </div>

  <div className="flex items-end space-x-sm">
    <Button subtle size="lg" variant="neutral" loading="Loading">
      Normal
    </Button>
    <Button subtle variant="neutral" loading>
      Normal
    </Button>
    <Button subtle variant="neutral" loading="Loading">
      Normal
    </Button>
    <Button subtle size="sm" variant="neutral" loading="Loading">
      Small
    </Button>
    <Button subtle size="xs" variant="neutral" loading="Loading">
      ExtraSmall
    </Button>
  </div>
</div>
```

#### Info

```jsx padded
<div className="flex items-end space-x-sm">
  <Button size="lg" variant="info">
    Large
  </Button>
  <Button variant="info">Normal</Button>
  <Button active variant="info">
    Active
  </Button>
  <Button disabled variant="info">
    Disabled
  </Button>
  <Button size="sm" variant="info">
    Small
  </Button>
  <Button size="xs" variant="info">
    ExtraSmall
  </Button>
  <Button fullWidth variant="info">
    Full Width
  </Button>
</div>
```

#### Info Outlined

```jsx padded
<div className="flex items-end space-x-sm">
  <Button outline size="lg" variant="info">
    Large
  </Button>
  <Button outline variant="info">
    Normal
  </Button>
  <Button active outline variant="info">
    Active
  </Button>
  <Button disabled outline variant="info">
    Disabled
  </Button>
  <Button outline size="sm" variant="info">
    Small
  </Button>
  <Button outline size="xs" variant="info">
    ExtraSmall
  </Button>
</div>
```

#### Info Subtle

```jsx padded
<div className="flex items-end space-x-sm">
  <Button subtle size="lg" variant="info">
    Large
  </Button>
  <Button subtle variant="info">
    Normal
  </Button>
  <Button active subtle variant="info">
    Active
  </Button>
  <Button disabled subtle variant="info">
    Disabled
  </Button>
  <Button subtle size="sm" variant="info">
    Small
  </Button>
  <Button subtle size="xs" variant="info">
    ExtraSmall
  </Button>
</div>
```

#### Info Loading

```jsx padded
<div className="space-y-sm">
  <div className="flex items-end space-x-sm">
    <Button size="lg" variant="info" loading="Loading">
      Normal
    </Button>
    <Button variant="info" loading>
      Normal
    </Button>
    <Button variant="info" loading="Loading">
      Normal
    </Button>
    <Button size="sm" variant="info" loading="Loading">
      Small
    </Button>
    <Button size="xs" variant="info" loading="Loading">
      ExtraSmall
    </Button>
  </div>

  <div className="flex items-end space-x-sm">
    <Button outline size="lg" variant="info" loading="Loading">
      Normal
    </Button>
    <Button outline variant="info" loading>
      Normal
    </Button>
    <Button outline variant="info" loading="Loading">
      Normal
    </Button>
    <Button outline size="sm" variant="info" loading="Loading">
      Small
    </Button>
    <Button outline size="xs" variant="info" loading="Loading">
      ExtraSmall
    </Button>
  </div>

  <div className="flex items-end space-x-sm">
    <Button subtle size="lg" variant="info" loading="Loading">
      Normal
    </Button>
    <Button subtle variant="info" loading>
      Normal
    </Button>
    <Button subtle variant="info" loading="Loading">
      Normal
    </Button>
    <Button subtle size="sm" variant="info" loading="Loading">
      Small
    </Button>
    <Button subtle size="xs" variant="info" loading="Loading">
      ExtraSmall
    </Button>
  </div>
</div>
```

#### Positive

```jsx padded
<div className="flex items-end space-x-sm">
  <Button size="lg" variant="positive">
    Large
  </Button>
  <Button variant="positive">Normal</Button>
  <Button active variant="positive">
    Active
  </Button>
  <Button disabled variant="positive">
    Disabled
  </Button>
  <Button size="sm" variant="positive">
    Small
  </Button>
  <Button size="xs" variant="positive">
    ExtraSmall
  </Button>
  <Button fullWidth variant="positive">
    Full Width
  </Button>
</div>
```

#### Positive Outlined

```jsx padded
<div className="flex items-end space-x-sm">
  <Button outline size="lg" variant="positive">
    Large
  </Button>
  <Button outline variant="positive">
    Normal
  </Button>
  <Button active outline variant="positive">
    Active
  </Button>
  <Button disabled outline variant="positive">
    Disabled
  </Button>
  <Button outline size="sm" variant="positive">
    Small
  </Button>
  <Button outline size="xs" variant="positive">
    ExtraSmall
  </Button>
  <Button fullWidth outline variant="positive">
    Full Width
  </Button>
</div>
```

#### Positive Subtle

```jsx padded
<div className="flex items-end space-x-sm">
  <Button subtle size="lg" variant="positive">
    Large
  </Button>
  <Button subtle variant="positive">
    Normal
  </Button>
  <Button active subtle variant="positive">
    Active
  </Button>
  <Button disabled subtle variant="positive">
    Disabled
  </Button>
  <Button subtle size="sm" variant="positive">
    Small
  </Button>
  <Button subtle size="xs" variant="positive">
    ExtraSmall
  </Button>
  <Button fullWidth subtle variant="positive">
    Full Width
  </Button>
</div>
```

#### Positive Loading

```jsx padded
<div className="space-y-sm">
  <div className="flex items-end space-x-sm">
    <Button size="lg" variant="positive" loading="Loading">
      Normal
    </Button>
    <Button variant="positive" loading>
      Normal
    </Button>
    <Button variant="positive" loading="Loading">
      Normal
    </Button>
    <Button size="sm" variant="positive" loading="Loading">
      Small
    </Button>
    <Button size="xs" variant="positive" loading="Loading">
      ExtraSmall
    </Button>
  </div>

  <div className="flex items-end space-x-sm">
    <Button outline size="lg" variant="positive" loading="Loading">
      Normal
    </Button>
    <Button outline variant="positive" loading>
      Normal
    </Button>
    <Button outline variant="positive" loading="Loading">
      Normal
    </Button>
    <Button outline size="sm" variant="positive" loading="Loading">
      Small
    </Button>
    <Button outline size="xs" variant="positive" loading="Loading">
      ExtraSmall
    </Button>
  </div>

  <div className="flex items-end space-x-sm">
    <Button subtle size="lg" variant="positive" loading="Loading">
      Normal
    </Button>
    <Button subtle variant="positive" loading>
      Normal
    </Button>
    <Button subtle variant="positive" loading="Loading">
      Normal
    </Button>
    <Button subtle size="sm" variant="positive" loading="Loading">
      Small
    </Button>
    <Button subtle size="xs" variant="positive" loading="Loading">
      ExtraSmall
    </Button>
  </div>
</div>
```

#### Caution

```jsx padded
<div className="flex items-end space-x-sm">
  <Button size="lg" variant="caution">
    Large
  </Button>
  <Button variant="caution">Normal</Button>
  <Button active variant="caution">
    Active
  </Button>
  <Button disabled variant="caution">
    Disabled
  </Button>
  <Button size="sm" variant="caution">
    Small
  </Button>
  <Button size="xs" variant="caution">
    ExtraSmall
  </Button>
  <Button fullWidth variant="caution">
    Full Width
  </Button>
</div>
```

#### Caution Outlined

```jsx padded
<div className="flex items-end space-x-sm">
  <Button outline size="lg" variant="caution">
    Large
  </Button>
  <Button outline variant="caution">
    Normal
  </Button>
  <Button active outline variant="caution">
    Active
  </Button>
  <Button disabled outline variant="caution">
    Disabled
  </Button>
  <Button outline size="sm" variant="caution">
    Small
  </Button>
  <Button outline size="xs" variant="caution">
    ExtraSmall
  </Button>
  <Button fullWidth outline variant="caution">
    Full Width
  </Button>
</div>
```

#### Caution Subtle

```jsx padded
<div className="flex items-end space-x-sm">
  <Button subtle size="lg" variant="caution">
    Large
  </Button>
  <Button subtle variant="caution">
    Normal
  </Button>
  <Button active subtle variant="caution">
    Active
  </Button>
  <Button disabled subtle variant="caution">
    Disabled
  </Button>
  <Button subtle size="sm" variant="caution">
    Small
  </Button>
  <Button subtle size="xs" variant="caution">
    ExtraSmall
  </Button>
  <Button fullWidth subtle variant="caution">
    Full Width
  </Button>
</div>
```

#### Caution Loading

```jsx padded
<div className="space-y-sm">
  <div className="flex items-end space-x-sm">
    <Button size="lg" variant="caution" loading="Loading">
      Normal
    </Button>
    <Button variant="caution" loading>
      Normal
    </Button>
    <Button variant="caution" loading="Loading">
      Normal
    </Button>
    <Button size="sm" variant="caution" loading="Loading">
      Small
    </Button>
    <Button size="xs" variant="caution" loading="Loading">
      ExtraSmall
    </Button>
  </div>

  <div className="flex items-end space-x-sm">
    <Button outline size="lg" variant="caution" loading="Loading">
      Normal
    </Button>
    <Button outline variant="caution" loading>
      Normal
    </Button>
    <Button outline variant="caution" loading="Loading">
      Normal
    </Button>
    <Button outline size="sm" variant="caution" loading="Loading">
      Small
    </Button>
    <Button outline size="xs" variant="caution" loading="Loading">
      ExtraSmall
    </Button>
  </div>

  <div className="flex items-end space-x-sm">
    <Button subtle size="lg" variant="caution" loading="Loading">
      Normal
    </Button>
    <Button subtle variant="caution" loading>
      Normal
    </Button>
    <Button subtle variant="caution" loading="Loading">
      Normal
    </Button>
    <Button subtle size="sm" variant="caution" loading="Loading">
      Small
    </Button>
    <Button subtle size="xs" variant="caution" loading="Loading">
      ExtraSmall
    </Button>
  </div>
</div>
```

#### Critical

```jsx padded
<div className="flex items-end space-x-sm">
  <Button size="lg" variant="critical">
    Large
  </Button>
  <Button variant="critical">Normal</Button>
  <Button active variant="critical">
    Active
  </Button>
  <Button disabled variant="critical">
    Disabled
  </Button>
  <Button size="sm" variant="critical">
    Small
  </Button>
  <Button size="xs" variant="critical">
    ExtraSmall
  </Button>
  <Button fullWidth variant="critical">
    Full Width
  </Button>
</div>
```

#### Critical Outlined

```jsx padded
<div className="flex items-end space-x-sm">
  <Button outline size="lg" variant="critical">
    Large
  </Button>
  <Button outline variant="critical">
    Normal
  </Button>
  <Button active outline variant="critical">
    Active
  </Button>
  <Button disabled outline variant="critical">
    Disabled
  </Button>
  <Button outline size="sm" variant="critical">
    Small
  </Button>
  <Button outline size="xs" variant="critical">
    ExtraSmall
  </Button>
  <Button fullWidth outline variant="critical">
    Full Width
  </Button>
</div>
```

#### Critical Subtle

```jsx padded
<div className="flex items-end space-x-sm">
  <Button subtle size="lg" variant="critical">
    Large
  </Button>
  <Button subtle variant="critical">
    Normal
  </Button>
  <Button active subtle variant="critical">
    Active
  </Button>
  <Button disabled subtle variant="critical">
    Disabled
  </Button>
  <Button subtle size="sm" variant="critical">
    Small
  </Button>
  <Button subtle size="xs" variant="critical">
    ExtraSmall
  </Button>
  <Button fullWidth subtle variant="critical">
    Full Width
  </Button>
</div>
```

#### Critical Loading

```jsx padded
<div className="space-y-sm">
  <div className="flex items-end space-x-sm">
    <Button size="lg" variant="critical" loading="Loading">
      Normal
    </Button>
    <Button variant="critical" loading>
      Normal
    </Button>
    <Button variant="critical" loading="Loading">
      Normal
    </Button>
    <Button size="sm" variant="critical" loading="Loading">
      Small
    </Button>
    <Button size="xs" variant="critical" loading="Loading">
      ExtraSmall
    </Button>
  </div>

  <div className="flex items-end space-x-sm">
    <Button outline size="lg" variant="critical" loading="Loading">
      Normal
    </Button>
    <Button outline variant="critical" loading>
      Normal
    </Button>
    <Button outline variant="critical" loading="Loading">
      Normal
    </Button>
    <Button outline size="sm" variant="critical" loading="Loading">
      Small
    </Button>
    <Button outline size="xs" variant="critical" loading="Loading">
      ExtraSmall
    </Button>
  </div>

  <div className="flex items-end space-x-sm">
    <Button subtle size="lg" variant="critical" loading="Loading">
      Normal
    </Button>
    <Button subtle variant="critical" loading>
      Normal
    </Button>
    <Button subtle variant="critical" loading="Loading">
      Normal
    </Button>
    <Button subtle size="sm" variant="critical" loading="Loading">
      Small
    </Button>
    <Button subtle size="xs" variant="critical" loading="Loading">
      ExtraSmall
    </Button>
  </div>
</div>
```
