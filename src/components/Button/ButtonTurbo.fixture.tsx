import React from 'react'
import { ButtonTurbo } from './ButtonTurbo'

const base = (
  <div>
    <ButtonTurbo onHeld={() => console.log(new Date().getSeconds())}>
      Keep pressed to console log the amount of seconds on each tick.
    </ButtonTurbo>
  </div>
)

export default base
