import React, { useCallback, useMemo } from 'react'

import { Inline } from '../../layout/Inline'
import { Loading } from '../Loading'

import { makeButtonClasses, ButtonClassProps } from './utils'
import './Button.css'

export interface ButtonProps
  extends ButtonClassProps,
    React.ButtonHTMLAttributes<HTMLButtonElement> {
  children: React.ReactNode
}

const Button = ({
  active,
  borderRadius = 'default',
  children,
  className,
  disabled,
  fullWidth,
  loading,
  loadingMessage,
  onClick,
  outline,
  size = 'base',
  subtle,
  type = 'button',
  variant = 'neutral',
  ...props
}: ButtonProps): React.ReactElement => {
  const LoadingComponent = useMemo(() => {
    const loadingSize = size === 'xs' ? 'sm' : size

    if (typeof loading === 'string')
      return (
        <Inline alignY="center" space="xs">
          <Loading size={loadingSize} />
          <span>{loading}</span>
        </Inline>
      )

    if (loading)
      return (
        <Loading
          size={loadingSize}
          message={loadingMessage}
          messagePosition="horizontal"
        />
      )

    return null
  }, [loading, size])

  const isDisabled = disabled || !!loading

  const handleOnClick = useCallback(
    (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
      if (!isDisabled && onClick) onClick(e)
    },
    [isDisabled, onClick],
  )

  const classes = useMemo(
    () =>
      makeButtonClasses({
        active,
        borderRadius,
        className,
        fullWidth,
        loading,
        outline,
        subtle,
        size,
        variant,
      }),
    [
      active,
      borderRadius,
      className,
      fullWidth,
      loading,
      outline,
      subtle,
      size,
      variant,
    ],
  )

  return (
    <button
      type={type}
      {...props}
      disabled={isDisabled}
      className={classes}
      onClick={handleOnClick}
    >
      {LoadingComponent || children}
    </button>
  )
}

export { Button }
