import dayjs from 'dayjs'
import { DateTimeDisplayContextValue } from '../DateTimeDisplayContext'
import relativeTime from 'dayjs/plugin/relativeTime'
import utc from 'dayjs/plugin/utc'
import timezone from 'dayjs/plugin/timezone'

dayjs.extend(relativeTime)
dayjs.extend(utc)
dayjs.extend(timezone)

export const getRelativeTime = (
  timestamp: string,
  opts?: { timezone?: string },
): string => {
  const now = new Date().toISOString()

  if (timestamp >= now) return dayjs(timestamp).tz(opts?.timezone).fromNow()
  else return dayjs(timestamp).tz(opts?.timezone).toNow()
}

export const getTime = (
  timestamp: string,
  opts?: Pick<DateTimeDisplayContextValue, 'format'> & {
    mode?: 'date' | 'datetime' | 'time'
    timezone?: string
  },
): string => {
  const { date, is24hr, time } = opts?.format || {}

  const dateFormat = date || 'MM/DD/YY'
  const timeFormat = time || (is24hr ? 'H:mm' : 'h:mm A')

  let format = ''

  if (opts?.mode === 'date') format = dateFormat
  else if (opts?.mode === 'time') format = timeFormat
  else format = dateFormat + ' ' + timeFormat

  return dayjs(timestamp).tz(opts?.timezone).format(format)
}
