import React, { useContext, useLayoutEffect } from 'react'
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'
import timezone from 'dayjs/plugin/timezone'

dayjs.extend(utc)
dayjs.extend(timezone)

export type DateTimeDisplayContextValue = {
  format?: {
    // dayjs/moment lib format strings
    date?: string
    is24hr?: boolean
    time?: string
  }
  timezone: string
}

const DateTimeDisplayContext = React.createContext<
  DateTimeDisplayContextValue | undefined
>(undefined)

const browserTimezone = Intl.DateTimeFormat().resolvedOptions().timeZone

export interface DateTimeDisplayContextProviderProps {
  children: any
  dateFormat?: string
  is24hr?: boolean
  timeFormat?: string
  timezone?: string
}

export function DateTimeDisplayContextProvider({
  children,
  dateFormat,
  is24hr,
  timeFormat,
  timezone,
}: DateTimeDisplayContextProviderProps) {
  const parentCtx = useContext(DateTimeDisplayContext)

  useLayoutEffect(() => {
    // when the timezone is passed we set the dayjs GLOBAL timezone.
    // this is important in server rendering environments, however, depending on the client use-cases, it may break
    // some UI where the timezone is expected to be isolated by context (very edge case).
    if (timezone && !parentCtx?.timezone) {
      console.log('DateTimeDisplayContextProvider setting default TZ', timezone)
      dayjs.tz.setDefault(timezone)
    }
  }, [parentCtx, timezone])

  return (
    <DateTimeDisplayContext.Provider
      value={{
        format: {
          date: dateFormat,
          is24hr,
          time: timeFormat,
        },
        timezone: timezone || browserTimezone,
      }}
    >
      {children}
    </DateTimeDisplayContext.Provider>
  )
}

export function useDateTimeDisplayContext():
  | DateTimeDisplayContextValue
  | undefined {
  return useContext(DateTimeDisplayContext)
}
