import React, { useEffect, useMemo, useState } from 'react'
import { getRelativeTime, getTime } from './utils'
import {
  DateTimeDisplayContextValue,
  useDateTimeDisplayContext,
  DateTimeDisplayContextProvider,
} from './DateTimeDisplayContext'

interface DateTimeProps {
  format?: DateTimeDisplayContextValue['format']
  mode?: 'date' | 'datetime' | 'time'
  timestamp: string
  relativeTime?: boolean
}

const DateTime = ({
  format,
  mode = 'datetime',
  timestamp,
  relativeTime = false,
}: DateTimeProps): React.ReactElement => {
  const dateTimeDisplayCtx = useDateTimeDisplayContext()
  const [, force] = useState(0)

  const _format = format || dateTimeDisplayCtx?.format

  useEffect(() => {
    // force the date/time value to be updated
    if (relativeTime || !timestamp) {
      const i = setInterval(() => {
        force(Math.random())
      }, 5000)

      return () => clearInterval(i)
    }
  }, [relativeTime])

  const date = useMemo(() => {
    if (relativeTime) return getRelativeTime(timestamp)

    return getTime(timestamp, {
      format: _format,
      mode,
      timezone: dateTimeDisplayCtx?.timezone,
    })
  }, [mode, relativeTime, timestamp])

  return <span>{date}</span>
}

export {
  DateTime,
  DateTimeProps,
  DateTimeDisplayContextProvider,
  DateTimeDisplayContextValue,
  useDateTimeDisplayContext,
}
