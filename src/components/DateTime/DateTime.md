### Display Date/Time

> Atalaya uses ISOString for timestamps & dates

```jsx
import { Stack } from '../../layout/Stack'
;<Stack>
  <DateTime timestamp={new Date().toISOString()} />
  <DateTime relative timestamp={new Date().toISOString()} />
  <DateTime mode="date" timestamp={new Date().toISOString()} />
  <DateTime mode="time" timestamp={new Date().toISOString()} />
</Stack>
```

### DateTimeDisplayContext

When your application needs consistency to display formatting such as
date, time or 24hr clock or to simply override the timezone used to display dates,
then you must use `<DateTimeDisplayContextProvider />`

> Note: when passing a `timezone` prop it will set the timezone globally for `dayjs`

```jsx
import { DateTimeDisplayContextProvider } from './DateTimeDisplayContext'
;<DateTimeDisplayContextProvider
  // all props are optional
  dateFormat={'MMM D, YYYY'}
  is24hr={false}
  timeFormat={'h:mm A'}
  timezone={'America/Chicago'}
>
  <div>
    <DateTime timestamp={new Date().toISOString()} />
  </div>
</DateTimeDisplayContextProvider>
```

#### DateTimeDisplayContext Hook

You can access the `DateTimeDisplayContextValue` via hook:

`useDateTimeDisplayContext()`
