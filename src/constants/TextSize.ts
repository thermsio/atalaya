export const TextSize = {
  Base: '1rem',
  Lg: '1.5rem',
  Md: '1.25rem',
  Sm: '0.875rem',
  Xl: '2.25rem',
  Xxl: '3.75rem',
}
