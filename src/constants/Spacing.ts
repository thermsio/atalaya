export const Spacing = {
  Xxs: '0.25rem',
  Xs: '0.5rem',
  Sm: '0.75rem',
  Base: '1rem',
  Lg: '1.25rem',
  Xl: '2rem',
  Xxl: '3rem',
}
