export const LineHeight = {
  Base: '1.5rem',
  Lg: '2rem',
  Md: '1.75rem',
  Sm: '1.25rem',
  Xs: '1rem',
  Xl: '2.75rem',
  Xxl: '3.75rem',
}
