export const ZIndex = {
  Sticky: 1000,
  Fixed: 1010,
  Dropdown: 1020,
  OffCanvas: 1030,
  ToolTip: 1040,
  Backdrop: 1050,
  ModalBackdrop: 1050,
  Modal: 1060,
  PopOver: 1070,
}
