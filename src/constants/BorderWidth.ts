export const BorderWidth = {
  Base: '1px',
  Lg: '4px',
  Md: '2px',
  None: 0,
  Xl: '8px',
}
