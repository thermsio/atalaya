export const Breakpoint = {
  Lg: { width: 1024 },
  Md: { width: 768 },
  Sm: { width: 640 },
  Xl: { width: 1280 },
  Xxl: {
    width: 1536,
  },
}

export const BREAKPOINT_NAMES = ['sm', 'md', 'lg', 'xl', 'xxl'] as const

export type Breakpoints = keyof typeof Breakpoint
export type ScreenSizeName = 'sm' | 'md' | 'lg' | 'xl' | 'xxl'
