import { LineHeight } from './LineHeight'

export const IconSize = {
  Base: LineHeight.Base,
  Lg: LineHeight.Lg,
  Sm: LineHeight.Sm,
  Xs: LineHeight.Xs,
  Xl: LineHeight.Xl,
  Xxl: LineHeight.Xxl,
}
