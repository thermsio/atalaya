export type PriorityVariant = 'positive' | 'caution' | 'critical' | 'info'

export type SemanticVariant =
  | 'positive'
  | 'caution'
  | 'critical'
  | 'info'
  | 'main'
  | 'neutral'

export const SemanticVariants: SemanticVariant[] = [
  'positive',
  'caution',
  'critical',
  'info',
  'main',
  'neutral',
]
