export const BorderRadius = {
  None: 0,
  Sm: '0.07rem',
  Base: '0.125rem',
  Md: '0.25rem',
  Lg: '0.365rem',
  Xl: '0.5rem',
  Xxl: '0.75rem',
}
