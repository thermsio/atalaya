import getResponsiveClasses from '../../utils/getResponsiveClasses'
import { BaseLayoutProps } from '../types'
import { Constants } from '../../constants'

const tailwindClassMap = {
  background: 'bg',
  borderColor: 'border',
  border: 'border',
  borderBottom: 'border-b',
  borderLeft: 'border-l',
  borderRight: 'border-r',
  borderTop: 'border-t',
  borderRadius: 'rounded',
  borderBottomRadius: 'rounded-b',
  borderLeftRadius: 'rounded-l',
  borderRightRadius: 'rounded-r',
  borderTopRadius: 'rounded-t',
  divideX: 'divide-x',
  divideY: 'divide-y',
  divideColor: 'divide',
  fontSize: 'text',
  height: 'h',
  margin: 'm',
  marginBottom: 'mb',
  marginLeft: 'ml',
  marginRight: 'mr',
  marginTop: 'mt',
  marginX: 'mx',
  marginY: 'my',
  padding: 'p',
  paddingBottom: 'pb',
  paddingLeft: 'pl',
  paddingRight: 'pr',
  paddingTop: 'pt',
  paddingX: 'px',
  paddingY: 'py',
  textColor: 'text-color',
  width: 'w',
}

export const makeBaseLayoutClasses = (props: BaseLayoutProps): string => {
  let classes = ''

  for (const [name, values] of Object.entries(props)) {
    if (values) {
      classes = classes.concat(
        ' ',
        getResponsiveClasses({ className: tailwindClassMap[name], values }),
      )
    }
  }

  return classes.trim()
}

export function getWidthBreakpoint(
  width: number,
  opt?: { defaultBreakpoint?: 'sm' | 'md' | 'lg' | 'xl' | 'xxl' },
): 'sm' | 'md' | 'lg' | 'xl' | 'xxl' {
  let breakpoint = opt?.defaultBreakpoint || 'sm'

  if (width < Constants.Breakpoint.Sm.width) {
    breakpoint = 'sm'
  } else if (width < Constants.Breakpoint.Md.width) {
    breakpoint = 'md'
  } else if (width < Constants.Breakpoint.Lg.width) {
    breakpoint = 'lg'
  } else if (width < Constants.Breakpoint.Xl.width) {
    breakpoint = 'xl'
  } else if (width < Constants.Breakpoint.Xxl.width) {
    breakpoint = 'xxl'
  }

  return breakpoint
}
/*
In order to prevent tailwind from purging compiled classes we write all
possible outputs here for PurgeCSS to pick them up

bg-background bg-surface bg-surface-strong bg-surface-subtle
sm:bg-background sm:bg-surface sm:bg-surface-strong sm:bg-surface-subtle
md:bg-background md:bg-surface md:bg-surface-strong md:bg-surface-subtle
lg:bg-background lg:bg-surface lg:bg-surface-strong lg:bg-surface-subtle
xl:bg-background xl:bg-surface xl:bg-surface-strong xl:bg-surface-subtle
xxl:bg-background xxl:bg-surface xxl:bg-surface-strong xxl:bg-surface-subtle

border-border border-current border-transparent border-main border-neutral border-positive border-caution border-critical border-info
sm:border-border sm:border-current sm:border-transparent sm:border-main sm:border-neutral sm:border-positive sm:border-caution sm:border-critical sm:border-info
md:border-border md:border-current md:border-transparent md:border-main md:border-neutral md:border-positive md:border-caution md:border-critical md:border-info
lg:border-border lg:border-current lg:border-transparent lg:border-main lg:border-neutral lg:border-positive lg:border-caution lg:border-critical lg:border-info
xl:border-border xl:border-current xl:border-transparent xl:border-main xl:border-neutral xl:border-positive xl:border-caution xl:border-critical xl:border-info
xxl:border-border xxl:border-current xxl:border-transparent xxl:border-main xxl:border-neutral xxl:border-positive xxl:border-caution xxl:border-critical xxl:border-info

border border-none border-md border-lg border-xl
sm:border sm:border-none sm:border-md sm:border-lg sm:border-xl
md:border md:border-none md:border-md md:border-lg md:border-xl
lg:border lg:border-none lg:border-md lg:border-lg lg:border-xl
xl:border xl:border-none xl:border-md xl:border-lg xl:border-xl
xxl:border xxl:border-none xxl:border-md xxl:border-lg xxl:border-xl

border-b border-b-none border-b-md border-b-lg border-b-xl
sm:border-b sm:border-b-none sm:border-b-md sm:border-b-lg sm:border-b-xl
md:border-b md:border-b-none md:border-b-md md:border-b-lg md:border-b-xl
lg:border-b lg:border-b-none lg:border-b-md lg:border-b-lg lg:border-b-xl
xl:border-b xl:border-b-none xl:border-b-md xl:border-b-lg xl:border-b-xl
xxl:border-b xxl:border-b-none xxl:border-b-md xxl:border-b-lg xxl:border-b-xl

border-l border-l-none border-l-md border-l-lg border-l-xl
sm:border-l sm:border-l-none sm:border-l-md sm:border-l-lg sm:border-l-xl
md:border-l md:border-l-none md:border-l-md md:border-l-lg md:border-l-xl
lg:border-l lg:border-l-none lg:border-l-md lg:border-l-lg lg:border-l-xl
xl:border-l xl:border-l-none xl:border-l-md xl:border-l-lg xl:border-l-xl
xxl:border-l xxl:border-l-none xxl:border-l-md xxl:border-l-lg xxl:border-l-xl

border-r border-r-none border-r-md border-r-lg border-r-xl
sm:border-r sm:border-r-none sm:border-r-md sm:border-r-lg sm:border-r-xl
md:border-r md:border-r-none md:border-r-md md:border-r-lg md:border-r-xl
lg:border-r lg:border-r-none lg:border-r-md lg:border-r-lg lg:border-r-xl
xl:border-r xl:border-r-none xl:border-r-md xl:border-r-lg xl:border-r-xl
xxl:border-r xxl:border-r-none xxl:border-r-md xxl:border-r-lg xxl:border-r-xl

border-t border-t-none border-t-md border-t-lg border-t-xl
sm:border-t sm:border-t-none sm:border-t-md sm:border-t-lg sm:border-t-xl
md:border-t md:border-t-none md:border-t-md md:border-t-lg md:border-t-xl
lg:border-t lg:border-t-none lg:border-t-md lg:border-t-lg lg:border-t-xl
xl:border-t xl:border-t-none xl:border-t-md xl:border-t-lg xl:border-t-xl
xxl:border-t xxl:border-t-none xxl:border-t-md xxl:border-t-lg xxl:border-t-xl

rounded-none rounded-sm rounded-default rounded-md rounded-lg rounded-xl rounded-xxl rounded-3xl rounded-full
sm:rounded-none sm:rounded-sm sm:rounded-default sm:rounded-md sm:rounded-lg sm:rounded-xl sm:rounded-xxl sm:rounded-3xl sm:rounded-full
md:rounded-none md:rounded-md md:rounded-default md:rounded-md md:rounded-lg md:rounded-xl md:rounded-xxl md:rounded-3xl md:rounded-full
lg:rounded-none lg:rounded-lg lg:rounded-default lg:rounded-md lg:rounded-lg lg:rounded-xl lg:rounded-xxl lg:rounded-3xl lg:rounded-full
xl:rounded-none xl:rounded-xl xl:rounded-default xl:rounded-md xl:rounded-lg xl:rounded-xl xl:rounded-xxl xl:rounded-3xl xl:rounded-full
xxl:rounded-none xxl:rounded-xxl xxl:rounded-default xxl:rounded-md xxl:rounded-lg xxl:rounded-xl xxl:rounded-xxl xxl:rounded-3xl xxl:rounded-full

rounded-b-none rounded-b-sm rounded-b-default rounded-b-md rounded-b-lg rounded-b-xl rounded-b-xxl rounded-b-3xl rounded-b-full
sm:rounded-b-none sm:rounded-b-sm sm:rounded-b-default sm:rounded-b-md sm:rounded-b-lg sm:rounded-b-xl sm:rounded-b-xxl sm:rounded-b-3xl sm:rounded-b-full
md:rounded-b-none md:rounded-b-md md:rounded-b-default md:rounded-b-md md:rounded-b-lg md:rounded-b-xl md:rounded-b-xxl md:rounded-b-3xl md:rounded-b-full
lg:rounded-b-none lg:rounded-b-lg lg:rounded-b-default lg:rounded-b-md lg:rounded-b-lg lg:rounded-b-xl lg:rounded-b-xxl lg:rounded-b-3xl lg:rounded-b-full
xl:rounded-b-none xl:rounded-b-xl xl:rounded-b-default xl:rounded-b-md xl:rounded-b-lg xl:rounded-b-xl xl:rounded-b-xxl xl:rounded-b-3xl xl:rounded-b-full
xxl:rounded-b-none xxl:rounded-b-xxl xxl:rounded-b-default xxl:rounded-b-md xxl:rounded-b-lg xxl:rounded-b-xl xxl:rounded-b-xxl xxl:rounded-b-3xl xxl:rounded-b-full

rounded-l-none rounded-l-sm rounded-l-default rounded-l-md rounded-l-lg rounded-l-xl rounded-l-xxl rounded-l-3xl rounded-l-full
sm:rounded-l-none sm:rounded-l-sm sm:rounded-l-default sm:rounded-l-md sm:rounded-l-lg sm:rounded-l-xl sm:rounded-l-xxl sm:rounded-l-3xl sm:rounded-l-full
md:rounded-l-none md:rounded-l-md md:rounded-l-default md:rounded-l-md md:rounded-l-lg md:rounded-l-xl md:rounded-l-xxl md:rounded-l-3xl md:rounded-l-full
lg:rounded-l-none lg:rounded-l-lg lg:rounded-l-default lg:rounded-l-md lg:rounded-l-lg lg:rounded-l-xl lg:rounded-l-xxl lg:rounded-l-3xl lg:rounded-l-full
xl:rounded-l-none xl:rounded-l-xl xl:rounded-l-default xl:rounded-l-md xl:rounded-l-lg xl:rounded-l-xl xl:rounded-l-xxl xl:rounded-l-3xl xl:rounded-l-full
xxl:rounded-l-none xxl:rounded-l-xxl xxl:rounded-l-default xxl:rounded-l-md xxl:rounded-l-lg xxl:rounded-l-xl xxl:rounded-l-xxl xxl:rounded-l-3xl xxl:rounded-l-full

rounded-r-none rounded-r-sm rounded-r-default rounded-r-md rounded-r-lg rounded-r-xl rounded-r-xxl rounded-r-3xl rounded-r-full
sm:rounded-r-none sm:rounded-r-sm sm:rounded-r-default sm:rounded-r-md sm:rounded-r-lg sm:rounded-r-xl sm:rounded-r-xxl sm:rounded-r-3xl sm:rounded-r-full
md:rounded-r-none md:rounded-r-md md:rounded-r-default md:rounded-r-md md:rounded-r-lg md:rounded-r-xl md:rounded-r-xxl md:rounded-r-3xl md:rounded-r-full
lg:rounded-r-none lg:rounded-r-lg lg:rounded-r-default lg:rounded-r-md lg:rounded-r-lg lg:rounded-r-xl lg:rounded-r-xxl lg:rounded-r-3xl lg:rounded-r-full
xl:rounded-r-none xl:rounded-r-xl xl:rounded-r-default xl:rounded-r-md xl:rounded-r-lg xl:rounded-r-xl xl:rounded-r-xxl xl:rounded-r-3xl xl:rounded-r-full
xxl:rounded-r-none xxl:rounded-r-xxl xxl:rounded-r-default xxl:rounded-r-md xxl:rounded-r-lg xxl:rounded-r-xl xxl:rounded-r-xxl xxl:rounded-r-3xl xxl:rounded-r-full

rounded-t-none rounded-t-sm rounded-t-default rounded-t-md rounded-t-lg rounded-t-xl rounded-t-xxl rounded-t-3xl rounded-t-full
sm:rounded-t-none sm:rounded-t-sm sm:rounded-t-default sm:rounded-t-md sm:rounded-t-lg sm:rounded-t-xl sm:rounded-t-xxl sm:rounded-t-3xl sm:rounded-t-full
md:rounded-t-none md:rounded-t-md md:rounded-t-default md:rounded-t-md md:rounded-t-lg md:rounded-t-xl md:rounded-t-xxl md:rounded-t-3xl md:rounded-t-full
lg:rounded-t-none lg:rounded-t-lg lg:rounded-t-default lg:rounded-t-md lg:rounded-t-lg lg:rounded-t-xl lg:rounded-t-xxl lg:rounded-t-3xl lg:rounded-t-full
xl:rounded-t-none xl:rounded-t-xl xl:rounded-t-default xl:rounded-t-md xl:rounded-t-lg xl:rounded-t-xl xl:rounded-t-xxl xl:rounded-t-3xl xl:rounded-t-full
xxl:rounded-t-none xxl:rounded-t-xxl xxl:rounded-t-default xxl:rounded-t-md xxl:rounded-t-lg xxl:rounded-t-xl xxl:rounded-t-xxl xxl:rounded-t-3xl xxl:rounded-t-full

divide-divide divide-current divide-transparent
sm:divide-divide sm:divide-current sm:divide-transparent
md:divide-divide md:divide-current md:divide-transparent
lg:divide-divide lg:divide-current lg:divide-transparent
xl:divide-divide xl:divide-current xl:divide-transparent
xxl:divide-divide xxl:divide-current xxl:divide-transparent

divide-x divide-x-none divide-x-md divide-x-lg divide-x-xl
sm:divide-x sm:divide-x-none sm:divide-x-md sm:divide-x-lg sm:divide-x-xl
md:divide-x md:divide-x-none md:divide-x-md md:divide-x-lg md:divide-x-xl
lg:divide-x lg:divide-x-none lg:divide-x-md lg:divide-x-lg lg:divide-x-xl
xl:divide-x xl:divide-x-none xl:divide-x-md xl:divide-x-lg xl:divide-x-xl
xxl:divide-x xxl:divide-x-none xxl:divide-x-md xxl:divide-x-lg xxl:divide-x-xl

divide-y divide-y-none divide-y-md divide-y-lg divide-y-xl
sm:divide-y sm:divide-y-none sm:divide-y-md sm:divide-y-lg sm:divide-y-xl
md:divide-y md:divide-y-none md:divide-y-md md:divide-y-lg md:divide-y-xl
lg:divide-y lg:divide-y-none lg:divide-y-md lg:divide-y-lg lg:divide-y-xl
xl:divide-y xl:divide-y-none xl:divide-y-md xl:divide-y-lg xl:divide-y-xl
xxl:divide-y xxl:divide-y-none xxl:divide-y-md xxl:divide-y-lg xxl:divide-y-xl

h-auto h-full h-screen
sm:h-auto sm:h-full sm:h-screen
md:h-auto md:h-full md:h-screen
lg:h-auto lg:h-full lg:h-screen
xl:h-auto xl:h-full xl:h-screen
xxl:h-auto xxl:h-full xxl:h-screen

m-xxs m-xs m-sm m-base m-lg m-xl m-xxl
m-xxs sm:m-xs sm:m-sm sm:m-base sm:m-lg sm:m-xl sm:m-xxl
m-xxs md:m-xs md:m-md md:m-base md:m-lg md:m-xl md:m-xxl
m-xxs lg:m-xs lg:m-lg lg:m-base lg:m-lg lg:m-xl lg:m-xxl
m-xxs xl:m-xs xl:m-lg xl:m-base xl:m-lg xl:m-xl xl:m-xxl
m-xxs xxl:m-xs xxl:m-xxl xxl:m-base xxl:m-lg xxl:m-xl xxl:m-xxl

mb-xxs mb-xs mb-sm mb-base mb-lg mb-xl mb-xxl
mb-xxs sm:mb-xs sm:mb-sm sm:mb-base sm:mb-lg sm:mb-xl sm:mb-xxl
mb-xxs md:mb-xs md:mb-md md:mb-base md:mb-lg md:mb-xl md:mb-xxl
mb-xxs lg:mb-xs lg:mb-lg lg:mb-base lg:mb-lg lg:mb-xl lg:mb-xxl
mb-xxs xl:mb-xs xl:mb-lg xl:mb-base xl:mb-lg xl:mb-xl xl:mb-xxl
mb-xxs xxl:mb-xs xxl:mb-xxl xxl:mb-base xxl:mb-lg xxl:mb-xl xxl:mb-xxl

ml-xxs ml-xs ml-sm ml-base ml-lg ml-xl ml-xxl
ml-xxs sm:ml-xs sm:ml-sm sm:ml-base sm:ml-lg sm:ml-xl sm:ml-xxl
ml-xxs md:ml-xs md:ml-md md:ml-base md:ml-lg md:ml-xl md:ml-xxl
ml-xxs lg:ml-xs lg:ml-lg lg:ml-base lg:ml-lg lg:ml-xl lg:ml-xxl
ml-xxs xl:ml-xs xl:ml-lg xl:ml-base xl:ml-lg xl:ml-xl xl:ml-xxl
ml-xxs xxl:ml-xs xxl:ml-xxl xxl:ml-base xxl:ml-lg xxl:ml-xl xxl:ml-xxl

mr-xxs mr-xs mr-sm mr-base mr-lg mr-xl mr-xxl
mr-xxs sm:mr-xs sm:mr-sm sm:mr-base sm:mr-lg sm:mr-xl sm:mr-xxl
mr-xxs md:mr-xs md:mr-md md:mr-base md:mr-lg md:mr-xl md:mr-xxl
mr-xxs lg:mr-xs lg:mr-lg lg:mr-base lg:mr-lg lg:mr-xl lg:mr-xxl
mr-xxs xl:mr-xs xl:mr-lg xl:mr-base xl:mr-lg xl:mr-xl xl:mr-xxl
mr-xxs xxl:mr-xs xxl:mr-xxl xxl:mr-base xxl:mr-lg xxl:mr-xl xxl:mr-xxl

mt-xxs mt-xs mt-sm mt-base mt-lg mt-xl mt-xxl
mt-xxs sm:mt-xs sm:mt-sm sm:mt-base sm:mt-lg sm:mt-xl sm:mt-xxl
mt-xxs md:mt-xs md:mt-md md:mt-base md:mt-lg md:mt-xl md:mt-xxl
mt-xxs lg:mt-xs lg:mt-lg lg:mt-base lg:mt-lg lg:mt-xl lg:mt-xxl
mt-xxs xl:mt-xs xl:mt-lg xl:mt-base xl:mt-lg xl:mt-xl xl:mt-xxl
mt-xxs xxl:mt-xs xxl:mt-xxl xxl:mt-base xxl:mt-lg xxl:mt-xl xxl:mt-xxl

mx-xxs mx-xs mx-sm mx-base mx-lg mx-xl mx-xxl
mx-xxs sm:mx-xs sm:mx-sm sm:mx-base sm:mx-lg sm:mx-xl sm:mx-xxl
mx-xxs md:mx-xs md:mx-md md:mx-base md:mx-lg md:mx-xl md:mx-xxl
mx-xxs lg:mx-xs lg:mx-lg lg:mx-base lg:mx-lg lg:mx-xl lg:mx-xxl
mx-xxs xl:mx-xs xl:mx-lg xl:mx-base xl:mx-lg xl:mx-xl xl:mx-xxl
mx-xxs xxl:mx-xs xxl:mx-xxl xxl:mx-base xxl:mx-lg xxl:mx-xl xxl:mx-xxl

my-xxs my-xs my-sm my-base my-lg my-xl my-xxl
my-xxs sm:my-xs sm:my-sm sm:my-base sm:my-lg sm:my-xl sm:my-xxl
my-xxs md:my-xs md:my-md md:my-base md:my-lg md:my-xl md:my-xxl
my-xxs lg:my-xs lg:my-lg lg:my-base lg:my-lg lg:my-xl lg:my-xxl
my-xxs xl:my-xs xl:my-lg xl:my-base xl:my-lg xl:my-xl xl:my-xxl
my-xxs xxl:my-xs xxl:my-xxl xxl:my-base xxl:my-lg xxl:my-xl xxl:my-xxl

p-xxs p-xs p-sm p-base p-lg p-xl p-xxl
p-xxs sm:p-xs sm:p-sm sm:p-base sm:p-lg sm:p-xl sm:p-xxl
p-xxs md:p-xs md:p-md md:p-base md:p-lg md:p-xl md:p-xxl
p-xxs lg:p-xs lg:p-lg lg:p-base lg:p-lg lg:p-xl lg:p-xxl
p-xxs xl:p-xs xl:p-lg xl:p-base xl:p-lg xl:p-xl xl:p-xxl
p-xxs xxl:p-xs xxl:p-xxl xxl:p-base xxl:p-lg xxl:p-xl xxl:p-xxl

pb-xxs pb-xs pb-sm pb-base pb-lg pb-xl pb-xxl
pb-xxs sm:pb-xs sm:pb-sm sm:pb-base sm:pb-lg sm:pb-xl sm:pb-xxl
pb-xxs md:pb-xs md:pb-md md:pb-base md:pb-lg md:pb-xl md:pb-xxl
pb-xxs lg:pb-xs lg:pb-lg lg:pb-base lg:pb-lg lg:pb-xl lg:pb-xxl
pb-xxs xl:pb-xs xl:pb-lg xl:pb-base xl:pb-lg xl:pb-xl xl:pb-xxl
pb-xxs xxl:pb-xs xxl:pb-xxl xxl:pb-base xxl:pb-lg xxl:pb-xl xxl:pb-xxl

pl-xxs pl-xs pl-sm pl-base pl-lg pl-xl pl-xxl
pl-xxs sm:pl-xs sm:pl-sm sm:pl-base sm:pl-lg sm:pl-xl sm:pl-xxl
pl-xxs md:pl-xs md:pl-md md:pl-base md:pl-lg md:pl-xl md:pl-xxl
pl-xxs lg:pl-xs lg:pl-lg lg:pl-base lg:pl-lg lg:pl-xl lg:pl-xxl
pl-xxs xl:pl-xs xl:pl-lg xl:pl-base xl:pl-lg xl:pl-xl xl:pl-xxl
pl-xxs xxl:pl-xs xxl:pl-xxl xxl:pl-base xxl:pl-lg xxl:pl-xl xxl:pl-xxl

pr-xxs pr-xs pr-sm pr-base pr-lg pr-xl pr-xxl
pr-xxs sm:pr-xs sm:pr-sm sm:pr-base sm:pr-lg sm:pr-xl sm:pr-xxl
pr-xxs md:pr-xs md:pr-md md:pr-base md:pr-lg md:pr-xl md:pr-xxl
pr-xxs lg:pr-xs lg:pr-lg lg:pr-base lg:pr-lg lg:pr-xl lg:pr-xxl
pr-xxs xl:pr-xs xl:pr-lg xl:pr-base xl:pr-lg xl:pr-xl xl:pr-xxl
pr-xxs xxl:pr-xs xxl:pr-xxl xxl:pr-base xxl:pr-lg xxl:pr-xl xxl:pr-xxl

pt-xxs pt-xs pt-sm pt-base pt-lg pt-xl pt-xxl
pt-xxs sm:pt-xs sm:pt-sm sm:pt-base sm:pt-lg sm:pt-xl sm:pt-xxl
pt-xxs md:pt-xs md:pt-md md:pt-base md:pt-lg md:pt-xl md:pt-xxl
pt-xxs lg:pt-xs lg:pt-lg lg:pt-base lg:pt-lg lg:pt-xl lg:pt-xxl
pt-xxs xl:pt-xs xl:pt-lg xl:pt-base xl:pt-lg xl:pt-xl xl:pt-xxl
pt-xxs xxl:pt-xs xxl:pt-xxl xxl:pt-base xxl:pt-lg xxl:pt-xl xxl:pt-xxl

px-xxs px-xs px-sm px-base px-lg px-xl px-xxl
px-xxs sm:px-xs sm:px-sm sm:px-base sm:px-lg sm:px-xl sm:px-xxl
px-xxs md:px-xs md:px-md md:px-base md:px-lg md:px-xl md:px-xxl
px-xxs lg:px-xs lg:px-lg lg:px-base lg:px-lg lg:px-xl lg:px-xxl
px-xxs xl:px-xs xl:px-lg xl:px-base xl:px-lg xl:px-xl xl:px-xxl
px-xxs xxl:px-xs xxl:px-xxl xxl:px-base xxl:px-lg xxl:px-xl xxl:px-xxl

py-xxs py-xs py-sm py-base py-lg py-xl py-xxl
py-xxs sm:py-xs sm:py-sm sm:py-base sm:py-lg sm:py-xl sm:py-xxl
py-xxs md:py-xs md:py-md md:py-base md:py-lg md:py-xl md:py-xxl
py-xxs lg:py-xs lg:py-lg lg:py-base lg:py-lg lg:py-xl lg:py-xxl
py-xxs xl:py-xs xl:py-lg xl:py-base xl:py-lg xl:py-xl xl:py-xxl
py-xxs xxl:py-xs xxl:py-xxl xxl:py-base xxl:py-lg xxl:py-xl xxl:py-xxl

text-color-subtle text-color text-color-strong text-color-semantic text-color-main text-color-neutral text-color-positive text-color-caution text-color-critical text-color-info
sm:text-color-subtle sm:text-color sm:text-color-strong sm:text-color-semantic sm:text-color-main sm:text-color-neutral sm:text-color-positive sm:text-color-caution sm:text-color-critical sm:text-color-info
md:text-color-subtle md:text-color md:text-color-strong md:text-color-semantic md:text-color-main md:text-color-neutral md:text-color-positive md:text-color-caution md:text-color-critical md:text-color-info
lg:text-color-subtle lg:text-color lg:text-color-strong lg:text-color-semantic lg:text-color-main lg:text-color-neutral lg:text-color-positive lg:text-color-caution lg:text-color-critical lg:text-color-info
xl:text-color-subtle xl:text-color xl:text-color-strong xl:text-color-semantic xl:text-color-main xl:text-color-neutral xl:text-color-positive xl:text-color-caution xl:text-color-critical xl:text-color-info
xxl:text-color-subtle xxl:text-color xxl:text-color-strong xxl:text-color-semantic xxl:text-color-main xxl:text-color-neutral xxl:text-color-positive xxl:text-color-caution xxl:text-color-critical xxl:text-color-info

text-sm text-base text-lg
sm:text-sm sm:text-base sm:text-lg
md:text-sm md:text-base md:text-lg
lg:text-sm lg:text-base lg:text-lg
xl:text-sm xl:text-base xl:text-lg
xxl:text-sm xxl:text-base xxl:text-lg

w-auto w-full w-min w-max w-screen
sm:w-auto sm:w-full sm:w-min sm:w-max sm:w-screen
md:w-auto md:w-full md:w-min md:w-max md:w-screen
lg:w-auto lg:w-full lg:w-min lg:w-max lg:w-screen
xl:w-auto xl:w-full xl:w-min xl:w-max xl:w-screen
xxl:w-auto xxl:w-full xxl:w-min xxl:w-max xxl:w-screen
 */
