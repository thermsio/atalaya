import classNameParser from 'classnames'
import getResponsiveClasses from '../../../utils/getResponsiveClasses'
import { makeBaseLayoutClasses } from '../../utils'
import { Alignment, BaseLayoutProps, Justification, Spacing } from '../../types'
import { StackProps } from '../index'

export interface StackClassProps extends BaseLayoutProps {
  /**
   *  Horizontal alignment<br>
   *  `around | between | center | end | evenly | start`
   * */
  alignX?: Array<Alignment> | Alignment

  /**
   *  Vertical alignment<br>
   *  `baseline | center | end | start | stretch`
   * */
  alignY?: Array<Justification> | Justification

  /** Additional classes */
  className?: string

  /**
   *  Space between children<br>
   *  `xxs | xs | sm | base | lg | xl | xxl`
   * */
  space?: Array<Spacing> | Spacing
}

export const makeStackClasses = ({
  alignX,
  alignY,
  className,
  space,
  ...restOfProps
}: StackClassProps): string => {
  const generalClasses = makeBaseLayoutClasses(restOfProps)
  const alignXClasses =
    alignX && getResponsiveClasses({ className: 'items', values: alignX })
  const alignYClasses =
    alignY && getResponsiveClasses({ className: 'justify', values: alignY })
  const spaceClasses =
    space && getResponsiveClasses({ className: 'space-y', values: space })

  return classNameParser(
    'flex flex-col',
    generalClasses,
    alignXClasses,
    alignYClasses,
    spaceClasses,
    className,
  )
}

export const classifyProps = (props: StackProps) => {
  const { children, ...restOfProps } = props
  const {
    alignX,
    alignY,
    space,
    className,
    background,
    borderColor,
    border,
    borderBottom,
    borderLeft,
    borderRight,
    borderTop,
    borderRadius,
    borderBottomRadius,
    borderLeftRadius,
    borderRightRadius,
    borderTopRadius,
    divideX,
    divideY,
    divideColor,
    fontSize,
    height,
    margin,
    marginBottom,
    marginLeft,
    marginRight,
    marginTop,
    marginX,
    marginY,
    padding,
    paddingBottom,
    paddingLeft,
    paddingRight,
    paddingTop,
    paddingX,
    paddingY,
    textColor,
    width,
    ...divProps
  } = restOfProps

  const classProps = {
    alignX,
    alignY,
    space,
    className,
    background,
    borderColor,
    border,
    borderBottom,
    borderLeft,
    borderRight,
    borderTop,
    borderRadius,
    borderBottomRadius,
    borderLeftRadius,
    borderRightRadius,
    borderTopRadius,
    divideX,
    divideY,
    divideColor,
    fontSize,
    height,
    margin,
    marginBottom,
    marginLeft,
    marginRight,
    marginTop,
    marginX,
    marginY,
    padding,
    paddingBottom,
    paddingLeft,
    paddingRight,
    paddingTop,
    paddingX,
    paddingY,
    textColor,
    width,
  }

  return { children, classProps, divProps }
}

/* Purge CSS:
items-baseline items-center items-end items-start items-stretch
sm:items-baseline sm:items-center sm:items-end sm:items-start sm:items-stretch
md:items-baseline md:items-center md:items-end md:items-start md:items-stretch
lg:items-baseline lg:items-center lg:items-end lg:items-start lg:items-stretch
xl:items-baseline xl:items-center xl:items-end xl:items-start xl:items-stretch
xxl:items-baseline xxl:items-center xxl:items-end xxl:items-start xxl:items-stretch

justify-around justify-between justify-center justify-end justify-evenly justify-start
sm:justify-around sm:justify-between sm:justify-center sm:justify-end sm:justify-evenly sm:justify-start
md:justify-around md:justify-between md:justify-center md:justify-end md:justify-evenly md:justify-start
lg:justify-around lg:justify-between lg:justify-center lg:justify-end lg:justify-evenly lg:justify-start
xl:justify-around xl:justify-between xl:justify-center xl:justify-end xl:justify-evenly xl:justify-start
xxl:justify-around xxl:justify-between xxl:justify-center xxl:justify-end xxl:justify-evenly xxl:justify-start

space-y-2xs space-y-xxs space-y-xs space-y-sm space-y-base space-y-lg space-y-xl space-y-xxl
sm:space-y-2xs sm:space-y-xxs sm:space-y-xs sm:space-y-sm sm:space-y-base sm:space-y-lg sm:space-y-xl sm:space-y-xxl
md:space-y-2xs md:space-y-xxs md:space-y-xs md:space-y-md md:space-y-base md:space-y-lg md:space-y-xl md:space-y-xxl
lg:space-y-2xs lg:space-y-xxs lg:space-y-xs lg:space-y-lg lg:space-y-base lg:space-y-lg lg:space-y-xl lg:space-y-xxl
xl:space-y-2xs xl:space-y-xxs xl:space-y-xs xl:space-y-xl xl:space-y-base xl:space-y-lg xl:space-y-xl xl:space-y-xxl
xxl:space-y-2xs xxl:space-y-xxs xxl:space-y-xs xxl:space-y-xxl xxl:space-y-base xxl:space-y-lg xxl:space-y-xl xxl:space-y-xxl
*/
