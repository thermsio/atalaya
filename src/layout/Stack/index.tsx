import React, { forwardRef, useMemo } from 'react'
import { classifyProps, makeStackClasses, StackClassProps } from './utils'

export interface StackProps
  extends StackClassProps,
    React.HTMLAttributes<HTMLDivElement> {
  children: React.ReactNode
}

const Stack = forwardRef<HTMLDivElement, StackProps>(
  (props: StackProps, ref) => {
    const { children, classProps, divProps } = useMemo(
      () => classifyProps(props),
      [props],
    )

    const classes = useMemo(() => makeStackClasses(classProps), [classProps])

    return (
      <div data-stack className={classes} {...divProps} ref={ref}>
        {children}
      </div>
    )
  },
)

Stack.displayName = 'Stack'

export { Stack }
