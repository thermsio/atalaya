The Stack it's a layout component meant to streamline the task of vertically
placing elements together.

## Spacing

Using the spacing prop you can adjust the separation between elements. You can
use either a string or an array for responsive values.

```jsx
<Stack background="surface" padding="base" rounded="default" space="sm">
  <div className="bg-neutral p-base rounded" />
  <div className="bg-neutral p-base rounded" />
  <div className="bg-neutral p-base rounded" />
</Stack>
```

## Horizontal alignment

Items can be aligned horizontally using align prop. Responsive values are
supported.

### Start

```jsx
<Stack background="surface" alignX="start" padding="base" rounded="default" space="sm">
  <div className="bg-neutral p-base rounded w-64" />
  <div className="bg-neutral p-base rounded w-64" />
  <div className="bg-neutral p-base rounded w-64" />
</Stack>
```

### Center

```jsx
<Stack background="surface" alignX="center" padding="base" rounded="default" space="sm">
  <div className="bg-neutral p-base rounded w-64" />
  <div className="bg-neutral p-base rounded w-64" />
  <div className="bg-neutral p-base rounded w-64" />
</Stack>
```

### End

```jsx
<Stack background="surface" alignX="end" padding="base" rounded="default" space="sm">
  <div className="bg-neutral p-base rounded w-64" />
  <div className="bg-neutral p-base rounded w-64" />
  <div className="bg-neutral p-base rounded w-64" />
</Stack>
```

## Vertical alignment

### Start

```jsx
<div className="bg-surface h-32 p-base rounded">
  <Stack alignY="start" height="full" space="sm">
    <div className="bg-neutral p-base rounded w" />
  </Stack>
</div>
```
### Center

```jsx
<div className="bg-surface h-32 p-base rounded">
  <Stack alignY="center" height="full" space="sm">
    <div className="bg-neutral p-base rounded w" />
  </Stack>
</div>
```

### End

```jsx
<div className="bg-surface h-32 p-base rounded">
  <Stack alignY="end" height="full" space="sm">
    <div className="bg-neutral p-base rounded w" />
  </Stack>
</div>
```
