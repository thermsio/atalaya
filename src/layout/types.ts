import { SemanticVariant } from '../constants'

export type Alignment = 'baseline' | 'center' | 'end' | 'start' | 'stretch'
export type Background =
  | 'background'
  | 'surface'
  | 'surface-strong'
  | 'surface-subtle'
export type BorderColor = 'border' | 'current' | 'transparent' | SemanticVariant
export type BorderWidth = 'none' | 'default' | 'md' | 'lg' | 'xl'
export type ColumnWidth =
  | '1/12'
  | '2/12'
  | '3/12'
  | '4/12'
  | '5/12'
  | '6/12'
  | '7/12'
  | '8/12'
  | '9/12'
  | '10/12'
  | '11/12'
  | 'content'
export type FontSize = 'sm' | 'default' | 'lg' | 'xl' | 'xxl'
export type Height = 'auto' | 'full' | 'screen'
export type Justification =
  | 'around'
  | 'between'
  | 'center'
  | 'end'
  | 'evenly'
  | 'start'
export type Radius =
  | 'none'
  | 'sm'
  | 'default'
  | 'md'
  | 'lg'
  | 'xl'
  | 'xxl'
  | 'xxxl'
  | 'full'
export type Spacing = '0' | 'xxs' | 'xs' | 'sm' | 'base' | 'lg' | 'xl' | 'xxl'
export type TextColor = 'subtle' | 'default' | 'strong' | 'semantic'
export type Width = 'auto' | 'full' | 'min' | 'max' | 'screen'

export interface BaseLayoutProps {
  /** `background | surface | surface-strong | surface-subtle` */
  background?: Array<Background> | Background

  /** `border | current | transparent | main | neutral | info | positive | caution | critical` */
  borderColor?: Array<BorderColor> | BorderColor

  /** `none | default |  md |  lg |  xl` */
  border?: Array<BorderWidth> | BorderWidth
  /** `none | default |  md |  lg |  xl` */
  borderBottom?: Array<BorderWidth> | BorderWidth
  /** `none | default |  md |  lg |  xl` */
  borderLeft?: Array<BorderWidth> | BorderWidth
  /** `none | default |  md |  lg |  xl` */
  borderRight?: Array<BorderWidth> | BorderWidth
  /** `none | default |  md |  lg |  xl` */
  borderTop?: Array<BorderWidth> | BorderWidth

  /** `none | sm | default | md | lg | xl | xxl | 3xl | full` */
  borderRadius?: Array<Radius> | Radius
  /** `none | sm | default | md | lg | xl | xxl | 3xl | full` */
  borderBottomRadius?: Array<Radius> | Radius
  /** `none | sm | default | md | lg | xl | xxl | 3xl | full` */
  borderLeftRadius?: Array<Radius> | Radius
  /** `none | sm | default | md | lg | xl | xxl | 3xl | full` */
  borderRightRadius?: Array<Radius> | Radius
  /** `none | sm | default | md | lg | xl | xxl | 3xl | full` */
  borderTopRadius?: Array<Radius> | Radius
  /**
   * Border color between items.<br>
   * `border | current | transparent | main | neutral | info | positive | caution | critical`
   * */
  divideColor?: Array<BorderColor> | BorderColor
  /**
   * Border thickness between horizontal items.<br>
   * `none | default |  md |  lg |  xl`
   * */
  divideX?: Array<BorderWidth> | BorderWidth
  /**
   * Border thickness between vertical items.<br>
   * `none | default |  md |  lg |  xl`
   * */
  divideY?: Array<BorderWidth> | BorderWidth

  /** `sm | base | lg | xl | xxl` */
  fontSize?: Array<FontSize> | FontSize

  /** `auto` | `full`: 100% | `min/max-content` keyword | `screen`: 100vh */
  height?: Array<Height> | Height

  /** `2xs | xxs | xs | sm | base | lg | xl | xxl` */
  margin?: Array<Spacing> | Spacing
  /** `2xs | xxs | xs | sm | base | lg | xl | xxl` */
  marginBottom?: Array<Spacing> | Spacing
  /** `2xs | xxs | xs | sm | base | lg | xl | xxl` */
  marginLeft?: Array<Spacing> | Spacing
  /** `2xs | xxs | xs | sm | base | lg | xl | xxl` */
  marginRight?: Array<Spacing> | Spacing
  /** `2xs | xxs | xs | sm | base | lg | xl | xxl` */
  marginTop?: Array<Spacing> | Spacing
  /** `2xs | xxs | xs | sm | base | lg | xl | xxl` */
  marginX?: Array<Spacing> | Spacing
  /** `2xs | xxs | xs | sm | base | lg | xl | xxl` */
  marginY?: Array<Spacing> | Spacing

  /** `2xs | xxs | xs | sm | base | lg | xl | xxl` */
  padding?: Array<Spacing> | Spacing
  /** `2xs | xxs | xs | sm | base | lg | xl | xxl` */
  paddingBottom?: Array<Spacing> | Spacing
  /** `2xs | xxs | xs | sm | base | lg | xl | xxl` */
  paddingLeft?: Array<Spacing> | Spacing
  /** `2xs | xxs | xs | sm | base | lg | xl | xxl` */
  paddingRight?: Array<Spacing> | Spacing
  /** `2xs | xxs | xs | sm | base | lg | xl | xxl` */
  paddingTop?: Array<Spacing> | Spacing
  /** `2xs | xxs | xs | sm | base | lg | xl | xxl` */
  paddingX?: Array<Spacing> | Spacing
  /** `2xs | xxs | xs | sm | base | lg | xl | xxl` */
  paddingY?: Array<Spacing> | Spacing

  /** `subtle | default | strong | semantic | main | neutral | info | positive | caution | critical` */
  textColor?: TextColor | SemanticVariant | Array<TextColor | SemanticVariant>

  /** `auto` | `full`: 100% | `min/max-content` keyword | `screen`: 100vw */
  width?: Array<Width> | Width
}
