import React, {
  MutableRefObject,
  useContext,
  useEffect,
  useLayoutEffect,
  useMemo,
  useRef,
  useState,
} from 'react'
import { getWidthBreakpoint } from '../utils'
import { useWindowSize } from '../../hooks/useWindowSize'

type DimensionsValue = {
  currentBreakpoint: 'sm' | 'md' | 'lg' | 'xl' | 'xxl'
  // Pixels
  height: number
  /* This number (Pixels) is the height that the container would have to be if it wanted to go to the bottom of the screen */
  heightForBottomOfScreen: number
  // Pixels
  width: number
}

export type ContainerDimensionsContextValue = DimensionsValue & {
  viewport: DimensionsValue
}

export const ContainerDimensionsContext = React.createContext<
  ContainerDimensionsContextValue | undefined
>(undefined)

export function ContainerDimensionsContextProvider<
  Element extends HTMLElement = HTMLDivElement,
>({
  asViewport,
  children,
  containerRef,
}: {
  /** If `true` then this "container" is considered the main viewport by children, useful for nested navigation/views in a complex SPA UI */
  asViewport?: boolean
  children: any
  /** Optionally provide a ref to the child "container", if not provided then children will be wrapped in a <div> as a container */
  containerRef?: MutableRefObject<Element | null | undefined>
}) {
  const { height: windowHeight, width: windowWidth } = useWindowSize()
  const localRef = useRef<HTMLDivElement>(null)
  const _containerRef = containerRef || localRef
  const [dimensions, setDimensions] = useState<{
    currentBreakpoint: 'sm' | 'md' | 'lg' | 'xl' | 'xxl'
    height: number
    heightForBottomOfScreen: number
    width: number
  }>({
    currentBreakpoint: 'sm',
    height: 0,
    heightForBottomOfScreen: 0,
    width: 0,
  })

  const parentContext = useContext(ContainerDimensionsContext)

  const viewport = useMemo(() => {
    return (
      parentContext?.viewport || {
        currentBreakpoint: getWidthBreakpoint(windowWidth),
        height: windowHeight,
        heightForBottomOfScreen:
          windowHeight -
          (_containerRef?.current?.getBoundingClientRect()?.y ?? 0),
        width: windowWidth,
      }
    )
  }, [parentContext, windowHeight, windowWidth])

  useLayoutEffect(() => {
    if (_containerRef?.current) {
      const { height, width, y } = _containerRef.current.getBoundingClientRect()

      setDimensions({
        currentBreakpoint: getWidthBreakpoint(width),
        height,
        heightForBottomOfScreen: windowHeight - y,
        width,
      })
    }
  }, [windowHeight])

  useEffect(() => {
    if (_containerRef?.current) {
      const resizeObserver = new ResizeObserver((entries) => {
        // We wrap it in requestAnimationFrame to avoid this error - ResizeObserver loop limit exceeded
        window.requestAnimationFrame(() => {
          if (!Array.isArray(entries) || !entries.length) {
            return
          }

          if (_containerRef?.current) {
            const { height, width, y } =
              _containerRef.current.getBoundingClientRect()
            if (dimensions.height !== height || dimensions.width !== width) {
              setDimensions({
                currentBreakpoint: getWidthBreakpoint(width),
                height,
                heightForBottomOfScreen: windowHeight - y,
                width,
              })
            }
          }
        })
      })

      resizeObserver.observe(_containerRef.current)

      return () => {
        try {
          resizeObserver?.disconnect()
        } catch {
          // Do nothing, loop limit exceeded
        }
      }
    }
  }, [_containerRef.current, windowHeight])

  if (containerRef)
    return (
      <ContainerDimensionsContext.Provider
        value={{
          currentBreakpoint: dimensions.currentBreakpoint,
          height: dimensions.height,
          heightForBottomOfScreen: dimensions.heightForBottomOfScreen,
          viewport: asViewport ? dimensions : viewport,
          width: dimensions.width,
        }}
      >
        {children}
      </ContainerDimensionsContext.Provider>
    )

  return (
    <div ref={localRef}>
      <ContainerDimensionsContext.Provider
        value={{
          currentBreakpoint: dimensions.currentBreakpoint,
          height: dimensions.height,
          heightForBottomOfScreen: dimensions.heightForBottomOfScreen,
          viewport: asViewport ? dimensions : viewport,
          width: dimensions.width,
        }}
      >
        {children}
      </ContainerDimensionsContext.Provider>
    </div>
  )
}
