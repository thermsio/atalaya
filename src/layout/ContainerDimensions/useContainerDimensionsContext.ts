import { useContext } from 'react'
import {
  ContainerDimensionsContext,
  ContainerDimensionsContextValue,
} from './ContainerDimensionsContext'

export function useContainerDimensionsContext(ctxRequired = true) {
  const context = useContext(ContainerDimensionsContext)

  if (!context && ctxRequired) {
    throw new Error(
      'useContainerDimensionsContext must be used within a ContainerDimensionsContext.Provider',
    )
  }

  return context as ContainerDimensionsContextValue
}
