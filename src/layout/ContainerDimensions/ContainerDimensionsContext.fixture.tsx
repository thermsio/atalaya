import React, { useRef } from 'react'
import { ContainerDimensionsContextProvider } from './ContainerDimensionsContext'
import { useContainerDimensionsContext } from './useContainerDimensionsContext'

const ContainerDimensionsReporter = ({
  children,
}: {
  children?: React.ReactNode
}) => {
  const { width, height, viewport } = useContainerDimensionsContext()

  return (
    <div>
      <p>{children}</p>

      <div className="mt-sm">
        <div>
          <span className="font-medium">Container Dimensions:</span>
          <span>
            Width: <span className="text-color-info">{width}</span> / Height:
            <span className="text-color-info">{height}</span>
          </span>
        </div>
        <div>
          <span className="font-medium">Viewport Dimensions:</span>
          <span>
            Width: <span className="text-color-info">{viewport.width}</span> /
            Height: <span className="text-color-info">{viewport.height}</span>
          </span>
        </div>
      </div>
    </div>
  )
}

const base = () => (
  <ContainerDimensionsContextProvider asViewport>
    <ContainerDimensionsReporter>
      This div is right next to the root viewport
    </ContainerDimensionsReporter>
    <div className="m-base bg-surface-subtle p-base">
      <ContainerDimensionsContextProvider>
        <ContainerDimensionsReporter>
          This div is inside a nested Container Dimensions context provider, the
          container size should be smaller
        </ContainerDimensionsReporter>
        <div className="m-base bg-surface p-base">
          <ContainerDimensionsContextProvider asViewport>
            <ContainerDimensionsReporter>
              This is another nested ContainerDimensions but its considered a
              new viewport so dimensions should be the same
            </ContainerDimensionsReporter>
          </ContainerDimensionsContextProvider>
        </div>
      </ContainerDimensionsContextProvider>
    </div>
  </ContainerDimensionsContextProvider>
)

const withRef = () => {
  const ref = useRef<HTMLDivElement>(null)

  return (
    <div>
      We will have 2 different ContainerDimensionsProviders, the one with a ref
      passed to it should not have an extra div created.
      <ContainerDimensionsContextProvider>
        <div>This should had an extra div </div>
        <ContainerDimensionsReporter />
      </ContainerDimensionsContextProvider>
      <ContainerDimensionsContextProvider containerRef={ref}>
        <div ref={ref}>
          This should have not have the extra div.
          <ContainerDimensionsReporter />
        </div>
      </ContainerDimensionsContextProvider>
    </div>
  )
}

const ignoringScrollWidth = () => {
  const ref = useRef<HTMLDivElement>(null)

  return (
    <ContainerDimensionsContextProvider containerRef={ref}>
      <div
        className="overflow-y-scroll bg-info-faded"
        style={{ width: 500, height: 500 }}
        ref={ref}
      >
        Width should be exactly 500px. If there is a slight variation it&apos;s
        the scrollbar doing funny stuff.
        <ContainerDimensionsReporter />
      </div>
    </ContainerDimensionsContextProvider>
  )
}

export default { base, withRef, ignoringScrollWidth }
