import React, { useMemo } from 'react'
import { classifyProps, makeColumnClasses, ColumnsClassProps } from './utils'
import Column from './components/Column'

export { ColumnProps } from './components/Column/'

export interface ColumnsProps
  extends ColumnsClassProps,
    React.HTMLAttributes<HTMLDivElement> {
  children: React.ReactNode
}

const Columns = (props: ColumnsProps): React.ReactElement => {
  const { children, classProps, divProps } = useMemo(
    () => classifyProps(props),
    [props],
  )

  const classes = useMemo(() => makeColumnClasses(classProps), [classProps])

  return (
    <div data-column className={classes} {...divProps}>
      {React.Children.map(children, (child) => {
        if (!child || !React.isValidElement(child)) return

        if (child.type === Column) return child

        return React.cloneElement(child, {
          ...child.props,
          className: `flex-1 w-0 ${child.props.className}`,
        })
      })}
    </div>
  )
}

Columns.Column = Column

export { Columns }
