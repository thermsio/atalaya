import classNames from 'classnames'
import { makeBaseLayoutClasses } from '../../utils'
import getResponsiveClasses from '../../../utils/getResponsiveClasses'
import { Alignment, BaseLayoutProps, Justification, Spacing } from '../../types'
import { ColumnsProps } from '../index'

export interface ColumnsClassProps extends BaseLayoutProps {
  /**
   *  Horizontal alignment<br>
   *  `'around' | 'between' | 'center' | 'end' | 'evenly' | 'start'`
   * */
  alignX?: Array<Justification> | Justification

  /**
   *  Vertical alignment<br>
   *  `'baseline' | 'center' | 'end' | 'start' | 'stretch'`
   * */
  alignY?: Array<Alignment> | Alignment

  /** Additional classes */
  className?: string

  /**
   *  Space between children<br>
   *  `xxs | xs | sm | base | lg | xl | xxl`
   * */
  space?: Array<Spacing> | Spacing
}

export const makeColumnClasses = ({
  alignX,
  alignY,
  className,
  space,
  ...restOfProps
}: ColumnsClassProps): string => {
  const generalClasses = makeBaseLayoutClasses(restOfProps)
  const alignXClasses =
    alignX && getResponsiveClasses({ className: 'justify', values: alignX })
  const alignYClasses =
    alignY && getResponsiveClasses({ className: 'items', values: alignY })
  const spaceClasses =
    space && getResponsiveClasses({ className: 'space-x', values: space })

  return classNames(
    'flex',
    alignXClasses,
    alignYClasses,
    generalClasses,
    spaceClasses,
    className,
  )
}

export const classifyProps = (props: ColumnsProps) => {
  const { children, ...restOfProps } = props
  const {
    alignX,
    alignY,
    className,
    space,
    background,
    borderColor,
    border,
    borderBottom,
    borderLeft,
    borderRight,
    borderTop,
    borderRadius,
    borderBottomRadius,
    borderLeftRadius,
    borderRightRadius,
    borderTopRadius,
    divideX,
    divideY,
    divideColor,
    fontSize,
    height,
    margin,
    marginBottom,
    marginLeft,
    marginRight,
    marginTop,
    marginX,
    marginY,
    padding,
    paddingBottom,
    paddingLeft,
    paddingRight,
    paddingTop,
    paddingX,
    paddingY,
    textColor,
    width,
    ...divProps
  } = restOfProps

  const classProps = {
    alignX,
    alignY,
    className,
    space,
    background,
    borderColor,
    border,
    borderBottom,
    borderLeft,
    borderRight,
    borderTop,
    borderRadius,
    borderBottomRadius,
    borderLeftRadius,
    borderRightRadius,
    borderTopRadius,
    divideX,
    divideY,
    divideColor,
    fontSize,
    height,
    margin,
    marginBottom,
    marginLeft,
    marginRight,
    marginTop,
    marginX,
    marginY,
    padding,
    paddingBottom,
    paddingLeft,
    paddingRight,
    paddingTop,
    paddingX,
    paddingY,
    textColor,
    width,
  }

  return { children, classProps, divProps }
}

/*
Purge CSS:
items-baseline items-center items-end items-start items-stretch
sm:items-baseline sm:items-center sm:items-end sm:items-start sm:items-stretch
md:items-baseline md:items-center md:items-end md:items-start md:items-stretch
lg:items-baseline lg:items-center lg:items-end lg:items-start lg:items-stretch
xl:items-baseline xl:items-center xl:items-end xl:items-start xl:items-stretch
xxl:items-baseline xxl:items-center xxl:items-end xxl:items-start xxl:items-stretch

justify-around justify-between justify-center justify-end justify-evenly justify-start
sm:justify-around sm:justify-between sm:justify-center sm:justify-end sm:justify-evenly sm:justify-start
md:justify-around md:justify-between md:justify-center md:justify-end md:justify-evenly md:justify-start
lg:justify-around lg:justify-between lg:justify-center lg:justify-end lg:justify-evenly lg:justify-start
xl:justify-around xl:justify-between xl:justify-center xl:justify-end xl:justify-evenly xl:justify-start
xxl:justify-around xxl:justify-between xxl:justify-center xxl:justify-end xxl:justify-evenly xxl:justify-start

space-x-2xs space-x-xxs space-x-xs space-x-sm space-x-base space-x-lg space-x-xl space-x-xxl space-x-2xl
sm:space-x-2xs space-x-xxs sm:space-x-xs sm:space-x-sm sm:space-x-base sm:space-x-lg sm:space-x-xl sm:space-x-xxl space-x-2xl
md:space-x-2xs space-x-xxs md:space-x-xs md:space-x-md md:space-x-base md:space-x-lg md:space-x-xl md:space-x-xxl space-x-2xl
lg:space-x-2xs space-x-xxs lg:space-x-xs lg:space-x-lg lg:space-x-base lg:space-x-lg lg:space-x-xl lg:space-x-xxl space-x-2xl
xl:space-x-2xs space-x-xxs xl:space-x-xs xl:space-x-xl xl:space-x-base xl:space-x-lg xl:space-x-xl xl:space-x-xxl space-x-2xl
xxl:space-x-2xs space-x-xxs xxl:space-x-xs xxl:space-x-sm xxl:space-x-base xxl:space-x-lg xxl:space-x-xl xxl:space-x-xxl space-x-2xl
 */
