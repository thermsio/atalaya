import getResponsiveClasses from '../../../../../utils/getResponsiveClasses'

export const makeColumnClasses = (
  width: string | Array<string> | undefined,
): string | undefined => {
  if (!width) return 'flex-1 w-0'
  if (width === 'content') return

  let widthValues: string | Array<string>
  if (Array.isArray(width)) {
    widthValues = width.map((value) => (value === 'content' ? '' : value))
  } else {
    widthValues = width
  }

  const widthClass =
    width && getResponsiveClasses({ className: 'w', values: widthValues })

  return widthClass
}

/*
In order to prevent tailwind from purging compiled classes we write all
possible outputs here for PurgeCss to pick them up
w-1/12 w-2/12 w-3/12 w-4/12 w-5/12 w-6/12 w-7/12 w-8/12 w-9/12 w-10/12 w-11/12
sm:w-1/12 sm:w-2/12 sm:w-3/12 sm:w-4/12 sm:w-5/12 sm:w-6/12 sm:w-7/12 sm:w-8/12 sm:w-9/12 sm:w-10/12 sm:w-11/12
md:w-1/12 md:w-2/12 md:w-3/12 md:w-4/12 md:w-5/12 md:w-6/12 md:w-7/12 md:w-8/12 md:w-9/12 md:w-10/12 md:w-11/12
lg:w-1/12 lg:w-2/12 lg:w-3/12 lg:w-4/12 lg:w-5/12 lg:w-6/12 lg:w-7/12 lg:w-8/12 lg:w-9/12 lg:w-10/12 lg:w-11/12
xl:w-1/12 xl:w-2/12 xl:w-3/12 xl:w-4/12 xl:w-5/12 xl:w-6/12 xl:w-7/12 xl:w-8/12 xl:w-9/12 xl:w-10/12 xl:w-11/12
xxl:w-1/12 xxl:w-2/12 xxl:w-3/12 xxl:w-4/12 xxl:w-5/12 xxl:w-6/12 xxl:w-7/12 xxl:w-8/12 xxl:w-9/12 xxl:w-10/12 xxl:w-11/12
 */
