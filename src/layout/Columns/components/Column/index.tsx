import React, { useMemo } from 'react'
import { ColumnWidth } from '../../../types'

import { makeColumnClasses } from './utils'

export interface ColumnProps {
  children: React.ReactNode
  width?: ColumnWidth | Array<ColumnWidth>
}

const Column = ({ children, width }: ColumnProps): React.ReactElement => {
  const classes = useMemo(() => makeColumnClasses(width), [width])

  return <div className={classes}>{children}</div>
}

export default Column
