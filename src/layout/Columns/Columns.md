Its main objective it's to space children horizontally. The recommended way of
using it is to wrap every direct child of `Columns` with the `Columns.Column`
subcomponent. Doing so helps reduce layout bugs and enables advanced options.

You are not forced to do it though, you can use any element as a
child, `Columns` will just space its children evenly.

## How it works

`Columns` divides available space in 12 equal parts. By using `Columns.Column`'s
width property you can specify the fraction of space you want any given column to take.

If no Column has width set, space will be equally allocated to children

```jsx
<Columns space="base">
  <Columns.Column>
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      No Width Set
    </div>
  </Columns.Column>
  <Columns.Column>
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      No Width Set
    </div>
  </Columns.Column>
  <Columns.Column>
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      No Width Set
    </div>
  </Columns.Column>
</Columns>
```

If some Columns have a specified with and others do not. The required space will
be assigned, then remaining space will be distributed among the children who
didn't have width set.

```jsx
<Columns space="base">
  <Columns.Column>
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      No Width Set
    </div>
  </Columns.Column>
  <Columns.Column width="6/12">
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      6/12
    </div>
  </Columns.Column>
  <Columns.Column>
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      No Width Set
    </div>
  </Columns.Column>
</Columns>
```

If you want a column to be as small as possible, you can use the
keyword `content` as width's value. This ensures that it’s only as wide as the
content within it.

```jsx
<Columns space="base">
  <Columns.Column width="content">
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      Width it's determined by content length
    </div>
  </Columns.Column>
  <Columns.Column width="content">
    <div className="h-full rounded bg-surface-strong p-sm">
      <div className="h-full w-xxl rounded bg-neutral" />
    </div>
  </Columns.Column>
  <Columns.Column>
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      No Width Set
    </div>
  </Columns.Column>
</Columns>
```

If after setting every child's width there is left over space you may
use `Columns's alignX` prop to control it.

#### **Start** (default)

```jsx
<Columns space="base">
  <Columns.Column width="2/12">
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      2/12
    </div>
  </Columns.Column>
  <Columns.Column width="2/12">
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      2/12
    </div>
  </Columns.Column>
  <Columns.Column width="2/12">
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      2/12
    </div>
  </Columns.Column>
</Columns>
```

#### **Center**

```jsx
<Columns alignX="center" space="base">
  <Columns.Column width="2/12">
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      2/12
    </div>
  </Columns.Column>
  <Columns.Column width="2/12">
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      2/12
    </div>
  </Columns.Column>
  <Columns.Column width="2/12">
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      2/12
    </div>
  </Columns.Column>
</Columns>
```

#### **End**

```jsx
<Columns alignX="end" space="base">
  <Columns.Column width="2/12">
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      2/12
    </div>
  </Columns.Column>
  <Columns.Column width="2/12">
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      2/12
    </div>
  </Columns.Column>
  <Columns.Column width="2/12">
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      2/12
    </div>
  </Columns.Column>
</Columns>
```

#### **Between**

```jsx
<Columns alignX="between" space="base">
  <Columns.Column width="2/12">
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      2/12
    </div>
  </Columns.Column>
  <Columns.Column width="2/12">
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      2/12
    </div>
  </Columns.Column>
  <Columns.Column width="2/12">
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      2/12
    </div>
  </Columns.Column>
</Columns>
```

#### **Around**

```jsx
<Columns alignX="around" space="base">
  <Columns.Column width="2/12">
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      2/12
    </div>
  </Columns.Column>
  <Columns.Column width="2/12">
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      2/12
    </div>
  </Columns.Column>
  <Columns.Column width="2/12">
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      2/12
    </div>
  </Columns.Column>
</Columns>
```

#### **Evenly**

```jsx
<Columns alignX="evenly" space="base">
  <Columns.Column width="2/12">
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      2/12
    </div>
  </Columns.Column>
  <Columns.Column width="2/12">
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      2/12
    </div>
  </Columns.Column>
  <Columns.Column width="2/12">
    <div className="flex justify-center rounded bg-surface-strong p-sm font-bold">
      2/12
    </div>
  </Columns.Column>
</Columns>
```
