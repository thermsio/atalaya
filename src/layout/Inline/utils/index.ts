import classNameParser from 'classnames'
import { makeBaseLayoutClasses } from '../../utils'
import getResponsiveClasses from '../../../utils/getResponsiveClasses'
import { Alignment, BaseLayoutProps, Justification, Spacing } from '../../types'
import { InlineProps } from '../index'

export interface InlineClassProps extends BaseLayoutProps {
  /**
   *  Horizontal alignment<br>
   *  `'around' | 'between' | 'center' | 'end' | 'evenly' | 'start'`
   * */
  alignX?: Array<Justification> | Justification

  /**
   *  Vertical alignment<br>
   *  `'baseline' | 'center' | 'end' | 'start' | 'stretch'`
   * */
  alignY?: Array<Alignment> | Alignment

  /** Additional classes */
  className?: string

  /**
   *  Space between children<br>
   *  `xxs | xs | sm | base | lg | xl | xxl`
   * */
  space?: Array<Spacing> | Spacing

  /** If elements should wrap to a new line */
  wrap?: boolean
}

export const makeInlineClasses = ({
  alignX,
  alignY,
  className,
  space,
  wrap,
  ...restOfProps
}: InlineClassProps): {
  all?: string
  container?: string
  content?: string
} => {
  const alignXClasses =
    alignX && getResponsiveClasses({ className: 'justify', values: alignX })
  const alignYClasses =
    alignY && getResponsiveClasses({ className: 'items', values: alignY })

  if (wrap) {
    const { containerProps, contentProps } = classifyBaseClasses(restOfProps)

    const contentClasses = makeBaseLayoutClasses(contentProps)
    const containerClasses = makeBaseLayoutClasses(containerProps)

    let spaceClassName = 'space-wrap-left'
    if (alignX === 'end' || alignX === 'between') {
      spaceClassName = 'space-wrap-right'
    }

    const spaceClasses =
      space &&
      getResponsiveClasses({
        className: spaceClassName,
        values: space,
      })

    const content = classNameParser(
      'inline-flex flex-wrap',
      alignXClasses,
      alignYClasses,
      contentClasses,
      spaceClasses,
      className,
    )

    const container = classNameParser(
      'inline-flex',
      alignXClasses,
      alignYClasses,
      containerClasses,
    )

    return { container, content }
  }

  const baseClasses = makeBaseLayoutClasses(restOfProps)

  const spaceClasses =
    space &&
    getResponsiveClasses({
      className: 'space-x',
      values: space,
    })

  const classes = classNameParser(
    'inline-flex',
    alignXClasses,
    alignYClasses,
    baseClasses,
    spaceClasses,
    className,
  )

  return { all: classes }
}

export const classifyProps = (props: InlineProps) => {
  const { children, ...restOfProps } = props
  const {
    alignX,
    alignY,
    className,
    space,
    wrap,
    background,
    borderColor,
    border,
    borderBottom,
    borderLeft,
    borderRight,
    borderTop,
    borderRadius,
    borderBottomRadius,
    borderLeftRadius,
    borderRightRadius,
    borderTopRadius,
    divideX,
    divideY,
    divideColor,
    fontSize,
    height,
    margin,
    marginBottom,
    marginLeft,
    marginRight,
    marginTop,
    marginX,
    marginY,
    padding,
    paddingBottom,
    paddingLeft,
    paddingRight,
    paddingTop,
    paddingX,
    paddingY,
    textColor,
    width,
    ...divProps
  } = restOfProps

  const classProps = {
    alignX,
    alignY,
    className,
    space,
    wrap,
    background,
    borderColor,
    border,
    borderBottom,
    borderLeft,
    borderRight,
    borderTop,
    borderRadius,
    borderBottomRadius,
    borderLeftRadius,
    borderRightRadius,
    borderTopRadius,
    divideX,
    divideY,
    divideColor,
    fontSize,
    height,
    margin,
    marginBottom,
    marginLeft,
    marginRight,
    marginTop,
    marginX,
    marginY,
    padding,
    paddingBottom,
    paddingLeft,
    paddingRight,
    paddingTop,
    paddingX,
    paddingY,
    textColor,
    width,
  }

  return { children, classProps, divProps }
}

const classifyBaseClasses = (props: InlineClassProps) => {
  const {
    alignX,
    alignY,
    className,
    divideX,
    divideY,
    divideColor,
    fontSize,
    space,
    textColor,
    wrap,
    width,
    ...containerProps
  } = props

  const contentProps = {
    alignX,
    alignY,
    className,
    divideX,
    divideY,
    divideColor,
    fontSize,
    space,
    textColor,
    width,
    wrap,
  }

  return { containerProps: { ...containerProps, width }, contentProps }
}

/*
Purge CSS:
justify-around justify-between justify-center justify-end justify-evenly justify-start
sm:justify-around sm:justify-between sm:justify-center sm:justify-end sm:justify-evenly sm:justify-start
md:justify-around md:justify-between md:justify-center md:justify-end md:justify-evenly md:justify-start
lg:justify-around lg:justify-between lg:justify-center lg:justify-end lg:justify-evenly lg:justify-start
xl:justify-around xl:justify-between xl:justify-center xl:justify-end xl:justify-evenly xl:justify-start
xxl:justify-around xxl:justify-between xxl:justify-center xxl:justify-end xxl:justify-evenly xxl:justify-start

items-baseline items-center items-end items-start items-stretch
sm:items-baseline sm:items-center sm:items-end sm:items-start sm:items-stretch
md:items-baseline md:items-center md:items-end md:items-start md:items-stretch
lg:items-baseline lg:items-center lg:items-end lg:items-start lg:items-stretch
xl:items-baseline xl:items-center xl:items-end xl:items-start xl:items-stretch
xxl:items-baseline xxl:items-center xxl:items-end xxl:items-start xxl:items-stretch

space-x-2xs space-x-xxs space-x-xs space-x-sm space-x-base space-x-lg space-x-xl space-x-xxl xl space-x-2xl
sm:space-x-2xs sm:space-x-xxs sm:space-x-xs sm:space-x-sm sm:space-x-base sm:space-x-lg sm:space-x-xl sm:space-x-xxl sm:space-x-2xl
md:space-x-2xs md:space-x-xxs md:space-x-xs md:space-x-md md:space-x-base md:space-x-lg md:space-x-xl md:space-x-xxl md:space-x-2xl
lg:space-x-2xs lg:space-x-xxs lg:space-x-xs lg:space-x-lg lg:space-x-base lg:space-x-lg lg:space-x-xl lg:space-x-xxl lg:space-x-2xl
xl:space-x-2xs xl:space-x-xxs xl:space-x-xs xl:space-x-xl xl:space-x-base xl:space-x-lg xl:space-x-xl xl:space-x-xxl xl:space-x-2xl
xxl:space-x-2xs xxl:space-x-xxs xxl:space-x-xs xxl:space-x-sm xxl:space-x-base xxl:space-x-lg xxl:space-x-xl xxl:space-x-xxl xl:space-x-2xl

space-wrap-left-2xs space-wrap-left-xxs space-wrap-left-xs space-wrap-left-sm space-wrap-left-base space-wrap-left-lg space-wrap-left-xl space-wrap-left-xxl space-wrap-left-2xl
sm:space-wrap-left-2xs sm:space-wrap-left-xxs sm:space-wrap-left-xs sm:space-wrap-left-sm sm:space-wrap-left-base sm:space-wrap-left-lg sm:space-wrap-left-xl sm:space-wrap-left-xxl sm:space-wrap-left-2xl
md:space-wrap-left-2xs md:space-wrap-left-xxs md:space-wrap-left-xs md:space-wrap-left-md md:space-wrap-left-base md:space-wrap-left-lg md:space-wrap-left-xl md:space-wrap-left-xxl md:space-wrap-left-2xl
lg:space-wrap-left-2xs lg:space-wrap-left-xxs lg:space-wrap-left-xs lg:space-wrap-left-lg lg:space-wrap-left-base lg:space-wrap-left-lg lg:space-wrap-left-xl lg:space-wrap-left-xxl lg:space-wrap-left-2xl
xl:space-wrap-left-2xs xl:space-wrap-left-xxs xl:space-wrap-left-xs xl:space-wrap-left-xl xl:space-wrap-left-base xl:space-wrap-left-lg xl:space-wrap-left-xl xl:space-wrap-left-xxl xl:space-wrap-left-2xl
xxl:space-wrap-left-2xs xxl:space-wrap-left-xxs xxl:space-wrap-left-xs xxl:space-wrap-left-sm xxl:space-wrap-left-base xxl:space-wrap-left-lg xxl:space-wrap-left-xl xxl:space-wrap-left-xxl xxl:space-wrap-left-2xl

space-wrap-right-2xs space-wrap-right-xxs space-wrap-right-xs space-wrap-right-sm space-wrap-right-base space-wrap-right-lg space-wrap-right-xl space-wrap-right-xxl space-wrap-right-2xl
sm:space-wrap-right-2xs sm:space-wrap-right-xxs sm:space-wrap-right-xs sm:space-wrap-right-sm sm:space-wrap-right-base sm:space-wrap-right-lg sm:space-wrap-right-xl sm:space-wrap-right-xxl sm:space-wrap-right-2xl
md:space-wrap-right-2xs md:space-wrap-right-xxs md:space-wrap-right-xs md:space-wrap-right-md md:space-wrap-right-base md:space-wrap-right-lg md:space-wrap-right-xl md:space-wrap-right-xxl md:space-wrap-right-2xl
lg:space-wrap-right-2xs lg:space-wrap-right-xxs lg:space-wrap-right-xs lg:space-wrap-right-lg lg:space-wrap-right-base lg:space-wrap-right-lg lg:space-wrap-right-xl lg:space-wrap-right-xxl lg:space-wrap-right-2xl
xl:space-wrap-right-2xs xl:space-wrap-right-xxs xl:space-wrap-right-xs xl:space-wrap-right-xl xl:space-wrap-right-base xl:space-wrap-right-lg xl:space-wrap-right-xl xl:space-wrap-right-xxl xl:space-wrap-right-2xl
xxl:space-wrap-right-2xs xxl:space-wrap-right-xxs xxl:space-wrap-right-xs xxl:space-wrap-right-sm xxl:space-wrap-right-base xxl:space-wrap-right-lg xxl:space-wrap-right-xl xxl:space-wrap-right-xxl xxl:space-wrap-right-2xl
 */
