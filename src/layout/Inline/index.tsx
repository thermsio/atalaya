import React, { useMemo } from 'react'
import { classifyProps, makeInlineClasses, InlineClassProps } from './utils'

export interface InlineProps
  extends InlineClassProps,
    React.HTMLAttributes<HTMLDivElement> {
  children: React.ReactNode
}

const Inline = (props: InlineProps): React.ReactElement => {
  const { children, classProps, divProps } = useMemo(
    () => classifyProps(props),
    [props],
  )

  const classes = useMemo(() => makeInlineClasses(classProps), [classProps])

  // Since wrap uses a whole different method to space children evenly
  // we add an empty wrapper to prevent potential clashing with
  // parent and child margins.
  if (props.wrap)
    return (
      <div data-inline className={classes.container} {...divProps}>
        <div className={classes.content}>{children}</div>
      </div>
    )

  return (
    <div data-inline className={classes.all} {...divProps}>
      {children}
    </div>
  )
}

export { Inline }
