This layout component it's meant to render a set of elements with equal spacing
around them. Elements will wrap around to a new line when necessary.

```jsx
<Inline background="surface" padding="base" borderRadius="default" space="base">
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
</Inline>
```

## Wrapping

By adding the `wrap` prop elements will automatically wrap to a new line when
they are out of space.

Implementing the `space` and `wrap` prop at the same time required some
workaround because Inline uses flexbox. Flexbox support for `gap` property it's
not great yet, so we relied on workarounds using positive and negative `margins`
. In these cases we add an extra div wrapping Inline to prevent possible margin
collapses with the rest of elements in the app. This means when you use
the `className` and `wrap` prop simultaneously your classes will be added to
container closer to Inline's children.

### Start

```jsx
<Inline
  background="surface"
  borderRadius="default"
  padding="base"
  space="base"
  wrap
>
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
</Inline>
```

### End

```jsx
<Inline
  background="surface"
  borderRadius="default"
  padding="base"
  space="base"
  alignX="end"
  wrap
>
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
</Inline>
```

### Between

```jsx
<Inline
  background="surface"
  borderRadius="default"
  padding="base"
  space="base"
  alignX="between"
  wrap
>
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
</Inline>
```

## Horizontal Alignment

### Start

```jsx
<Inline
  alignX="start"
  background="surface"
  borderRadius="default"
  padding="base"
  space="xs"
  width="full"
>
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
</Inline>
```

### Center

```jsx
<Inline
  alignX="center"
  background="surface"
  borderRadius="default"
  padding="base"
  space="xs"
  width="full"
>
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
</Inline>
```

### End

```jsx
<Inline
  alignX="end"
  background="surface"
  borderRadius="default"
  padding="base"
  space="xs"
  width="full"
>
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
</Inline>
```

### Between

```jsx
<Inline
  alignX="between"
  background="surface"
  borderRadius="default"
  padding="base"
  space="xs"
  width="full"
>
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
</Inline>
```

### Around

```jsx
<Inline
  alignX="around"
  background="surface"
  borderRadius="default"
  padding="base"
  space="xs"
  width="full"
>
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
</Inline>
```

### Evenly

```jsx
<Inline
  alignX="evenly"
  background="surface"
  borderRadius="default"
  padding="base"
  space="xs"
  width="full"
>
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg" />
</Inline>
```

## Vertical Alignment

### Start

```jsx
<Inline
  alignX="center"
  alignY="start"
  background="surface"
  borderRadius="default"
  padding="base"
  space="xs"
>
  <div className="rounded bg-neutral p-lg py-sm" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg py-sm" />
</Inline>
```

### Center

```jsx
<Inline
  alignX="center"
  alignY="center"
  background="surface"
  borderRadius="default"
  padding="base"
  space="xs"
>
  <div className="rounded bg-neutral p-lg py-sm" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg py-sm" />
</Inline>
```

### End

```jsx
<Inline
  alignX="center"
  alignY="end"
  background="surface"
  borderRadius="default"
  padding="base"
  space="xs"
>
  <div className="rounded bg-neutral p-lg py-sm" />
  <div className="rounded bg-neutral p-lg" />
  <div className="rounded bg-neutral p-lg py-sm" />
</Inline>
```
