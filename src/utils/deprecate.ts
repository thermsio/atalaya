const hasLoggedByKey: { [key: string]: boolean } = {}

export function deprecate(key, ...msg) {
  if (hasLoggedByKey[key]) return

  hasLoggedByKey[key] = true
  console.warn('⚠️ deprecated: ', ...msg)
}
