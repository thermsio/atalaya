import dayjs from 'dayjs'
import isBetween from 'dayjs/plugin/isBetween'

dayjs.extend(isBetween)

const DATE_FORMAT = 'MMM D, YYYY'
const DATE_FORMAT_SHORT = 'MM/DD/YY'
const DATE_TIME_FORMAT = 'MM/DD/YY - h:mm A'
const DATE_TIME_FORMAT_24 = 'MM/DD/YY - H:mm A'
const MONTH_FORMAT = 'MMMM YYYY'
const TIME_FORMAT = 'h:mm A'
const TIME_FORMAT_24 = 'H:mm'

export const getDate = (timestamp?: string): string => {
  return dayjs(timestamp).format(DATE_FORMAT)
}

export const getDateShort = (timestamp: string): string => {
  return dayjs(timestamp).format(DATE_FORMAT_SHORT)
}

export const getDateTime = (timestamp: string, is24?: boolean): string => {
  return dayjs(timestamp).format(is24 ? DATE_TIME_FORMAT_24 : DATE_TIME_FORMAT)
}

export const getTime = (timestamp: string, is24?: boolean): string => {
  return dayjs(timestamp).format(is24 ? TIME_FORMAT_24 : TIME_FORMAT)
}

export const getHours = (timestamp?: string, is24?: boolean): number => {
  return parseInt(dayjs(timestamp).format(is24 ? 'H' : 'h'))
}

export const getMinutes = (timestamp?: string): number => {
  return parseInt(dayjs(timestamp).format('mm'))
}

export const getMonth = (timestamp: string): string => {
  return dayjs(timestamp).format(MONTH_FORMAT)
}

export const getSuffix = (timestamp?: string, is24?: boolean): string => {
  if (is24) return ''
  return dayjs(timestamp).format('A')
}

const getLastMonthTrailingDays = (monthTimestamp, weekStart) => {
  const month = dayjs(monthTimestamp)

  let firstDayOfMonth = month.date(1).day() as number
  if (weekStart === 'monday') {
    firstDayOfMonth = firstDayOfMonth === 0 ? 6 : firstDayOfMonth - 1
  }

  let lastMonthDaysCount = dayjs(monthTimestamp)
    .subtract(1, 'month')
    .daysInMonth()

  const trailingDays: number[] = []

  for (let index = firstDayOfMonth; index > 0; index--) {
    trailingDays.push(lastMonthDaysCount)
    --lastMonthDaysCount
  }

  return trailingDays.reverse()
}

const getNextMonthTrailingDays = (monthTimestamp, weekStart) => {
  const month = dayjs(monthTimestamp)
  const monthDays = month.daysInMonth()
  let lastDayOfMonth = month.date(monthDays).day() as number
  if (weekStart === 'monday') {
    lastDayOfMonth = lastDayOfMonth === 0 ? 6 : lastDayOfMonth - 1
  }

  const trailingDays: number[] = []
  let nextMonthCount = 1
  for (let index = lastDayOfMonth; index < 6; index++) {
    trailingDays.push(nextMonthCount)
    ++nextMonthCount
  }

  return trailingDays
}

type CalendarDay = { day: number; enabled: boolean }

export const getMonthWeekDays = (
  monthTimestamp: string,
  weekStart: 'sunday' | 'monday',
  options?: {
    disableDatesAfterDate?: string
    disableDatesBeforeDate?: string
  },
): CalendarDay[] => {
  const monthDays = dayjs(monthTimestamp).daysInMonth()

  const lastMonthTrailingDays = getLastMonthTrailingDays(
    monthTimestamp,
    weekStart,
  )

  const nextMonthTrailingDays = getNextMonthTrailingDays(
    monthTimestamp,
    weekStart,
  )

  const calendarDays: CalendarDay[] = []

  lastMonthTrailingDays.forEach((day) => {
    calendarDays.push({ day, enabled: false })
  })

  for (let monthDay = 1; monthDay <= monthDays; monthDay++) {
    const day = dayjs(monthTimestamp).date(monthDay)

    let enabled = true

    if (
      (options?.disableDatesAfterDate &&
        day.isAfter(options.disableDatesAfterDate)) ||
      (options?.disableDatesBeforeDate &&
        day.isBefore(options.disableDatesBeforeDate))
    ) {
      enabled = false
    }

    calendarDays.push({ day: monthDay, enabled })
  }

  nextMonthTrailingDays.forEach((day) => {
    calendarDays.push({ day, enabled: false })
  })

  return calendarDays
}

export const addHours = (timestamp?: string, amount = 1): string => {
  return dayjs(timestamp).add(amount, 'hour').toISOString()
}

export const addMinutes = (timestamp?: string, amount = 1): string => {
  return dayjs(timestamp).add(amount, 'minute').toISOString()
}

export const setHours = (
  timestamp?: string,
  amount = 0,
  is24?: boolean,
  isPM?: boolean,
): string => {
  // When you submit an overflowing value (i.e. 25 hours) to dayjs, it will
  // bubble up the change to the next unit of time (i.e 1 day, 1 hour).
  // This represents a problem when you want to manually set the hours
  // because when you set times like 12 AM it will change the day;
  // these hour manipulations are meant to prevent that.
  const hour = dayjs(timestamp).hour()

  let value = isPM ? amount + 12 : amount
  value = value - hour
  if (!is24 && amount === 12) value = value - 12

  return addHours(timestamp, value)
}

export const setMinutes = (timestamp?: string, amount = 0): string => {
  return dayjs(timestamp).minute(amount).toISOString()
}

export const checkIsAfter = (date, dateToCheck) => {
  if (!date || !dateToCheck) return false
  return dayjs(date).isAfter(dateToCheck)
}

export const checkIsBefore = (date, dateToCheck) => {
  if (!date || !dateToCheck) return false
  return dayjs(date).isBefore(dateToCheck)
}

export const checkIsBetweenDates = (
  timestamp,
  startRange?: string,
  endRange?: string,
) => {
  if (!timestamp || !startRange || !endRange) return false
  return dayjs(timestamp).isBetween(startRange, endRange, 'day')
}

export const checkIsSameDay = (date, dateToCheck) => {
  if (!date || !dateToCheck) return false
  return dayjs(date).isSame(dateToCheck, 'day')
}

export const checkIsToday = (date) => {
  return dayjs().isSame(date, 'day')
}

export const getDayOfMonth = (day, timestamp) => {
  return dayjs(timestamp).date(day).toISOString()
}
