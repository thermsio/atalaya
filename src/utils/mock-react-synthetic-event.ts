/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
export const createMockReactSyntheticEvent = ({
  id,
  name,
  value,
}: { id?: string; name?: string; value?: any } = {}) => ({
  nativeEvent: {},
  currentTarget: { id, name, value },
  target: { id, name, value },
  bubbles: false,
  cancelable: false,
  defaultPrevented: false,
  eventPhase: 0,
  isTrusted: false,
  preventDefault: () => undefined,
  isDefaultPrevented: () => true,
  stopPropagation: () => undefined,
  isPropagationStopped: () => true,
  persist: () => undefined,
  timeStamp: 0,
  type: '',
})

export const mockReactSyntheticEvent = createMockReactSyntheticEvent()
