import React from 'react'

export function isReactFragment(node) {
  if (node?.type) {
    return node.type === React.Fragment
  }

  return node === React.Fragment
}
