import React from 'react'

export interface ConditionalWrapperProps {
  children: React.ReactElement | null
  condition: boolean
  wrapper: (x: React.ReactNode) => React.ReactElement | null
}

const ConditionalWrapper = ({
  condition,
  wrapper,
  children,
}: ConditionalWrapperProps) => (condition ? wrapper(children) : children)

export { ConditionalWrapper }
