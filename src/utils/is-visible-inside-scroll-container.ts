const isVisibleInsideScrollContainer = function (
  targetElement: Element,
  container: Element,
) {
  const { bottom, height, top } = targetElement.getBoundingClientRect()
  const containerRect = container.getBoundingClientRect()

  return top <= containerRect.top
    ? containerRect.top - top <= height
    : bottom - containerRect.bottom <= height
}
