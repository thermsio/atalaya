import {
  SemanticVariant,
  SemanticVariants,
} from '../constants/SemanticVariants'

const semanticVariantsMap = SemanticVariants.reduce((acc, s) => {
  acc[s] = s
  return acc
}, {})

/**
 * A util to return a semantic variant or empty string
 * @param str
 */
export function getSemanticVariant(
  str: SemanticVariant | string | undefined,
): SemanticVariant | string {
  return semanticVariantsMap[str || ''] || ''
}
