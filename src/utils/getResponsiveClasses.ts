interface Props {
  className: string
  values: Array<string | number> | string | number
}

const breakpoints = ['sm', 'md', 'lg', 'xl', 'xxl', 'xxl']

const getResponsiveClasses = ({ className, values }: Props): string => {
  if (typeof values === 'string' || typeof values === 'number') {
    if (values === 'default') return className
    return `${className}-${values}`
  }

  if (Array.isArray(values)) {
    if (values.length > breakpoints.length + 1) {
      console.error(
        'getResponsiveClasses() more values than available breakpoints provided',
        {
          className,
          values,
        },
      )
    }

    // We manually push items into the array instead of using map() to avoid possible `undefined` items
    // that need an additional filter() loop to be removed.
    const classes: Array<string> = []

    values.forEach((value, index) => {
      if (value === '') return

      const declaration =
        value === 'default' ? className : `${className}-${value}`

      if (index === 0) {
        classes.push(declaration)
      } else {
        classes.push(`${breakpoints[index - 1]}:${declaration}`)
      }
    })

    return classes.join(' ')
  }

  console.error(
    `getResponsiveClasses() values are of incorrect type. Expected string, number or Array[string|number]; got ${typeof values}`,
  )

  return ''
}

export default getResponsiveClasses
