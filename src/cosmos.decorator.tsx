import React, { useContext } from 'react'
import DarkModeToggle from 'react-dark-mode-toggle'
import { AtalayaWrapper, ToastContainer } from './index'
import { StylesContext } from './styles/context/StylesContext'

import './styles/css/all.css'

const ComponentsWrapperContent = ({ children }) => {
  const { isDarkMode, toggleDarkMode } = useContext(StylesContext)

  return (
    <>
      <div className="m-4 flex justify-end">
        <DarkModeToggle
          onChange={toggleDarkMode}
          checked={isDarkMode}
          size={50}
          speed={3}
        />
      </div>

      {children}
    </>
  )
}

const ComponentsWrapper = ({ children }) => (
  <AtalayaWrapper>
    <ComponentsWrapperContent>{children}</ComponentsWrapperContent>

    <ToastContainer />
  </AtalayaWrapper>
)

export default ComponentsWrapper
