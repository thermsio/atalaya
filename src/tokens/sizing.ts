import { TokenMap } from './types'

export const borderWidth: TokenMap = {
  none: '0px',
  base: '1px',
  md: '2px',
  lg: '4px',
  xl: '8px',
}

export const textSizing: TokenMap = {
  'size-text-sm': '0.875rem',
  'size-text': '1rem',
  'size-text-md': '1.25rem',
  'size-text-lg': '1.5rem',
  'size-text-xl': '2.25rem',
  'size-text-xxl': '3.75rem',
}

export const lineHeight: TokenMap = {
  'line-height-text-sm': '1.25rem',
  'line-height-text': '1.5rem',
  'line-height-text-md': '1.75rem',
  'line-height-text-lg': '2rem',
  'line-height-text-xl': '2.5rem',
  'line-height-text-xxl': '3,75rem',
}

export const iconSizing: TokenMap = {
  'size-icon-sm': lineHeight['line-height-text-sm'],
  'size-icon': lineHeight['line-height-text'],
  'size-icon-md': lineHeight['line-height-md'],
  'size-icon-lg': lineHeight['line-height-text-lg'],
  'size-icon-xl': lineHeight['line-height-text-xl'],
  'size-icon-xxl': lineHeight['line-height-text-xxl'],
}
