export interface TokenMap {
  [key: string]: string
}
