/* eslint-disable prettier/prettier */
import { TokenMap } from './types'

export const darkColors: TokenMap = {
  /* Background --- */
  'color-background': '#14191F',

  /* Border --- */
  'color-border': '#393F46',

  /* Overlay --- */
  'color-overlay': '#0C1114BF',

  /* Surface --- */
  'color-surface-subtle': '#1C2126',
  'color-surface': '#23282E',
  'color-surface-strong': '#32383E',

  /* Text --- */
  'color-text-subtle': '#ffffff73',
  'color-text': '#ffffffb3',
  'color-text-strong': '#fffffff2',
  'color-text-semantic': '#fffffff2',

  /* Semantic --- */
  'color-main-dark': '#2D5286',
  'color-main': '#4075BF',
  'color-main-light': '#799ED2',
  'color-main-faded': '#4075BF4D',

  'color-neutral-dark': '#393f46',
  'color-neutral': '#787e87',
  'color-neutral-light': '#94989e',
  'color-neutral-faded': '#787e8780',

  'color-positive-dark': '#0D5938',
  'color-positive': '#1F9361',
  'color-positive-light': '#2DD28A',
  'color-positive-faded': '#1F93614D',

  'color-caution-dark': '#936110',
  'color-caution': '#D68F1F',
  'color-caution-light': '#F6B955',
  'color-caution-faded': '#D68F1F4D',

  'color-critical-dark': '#451008',
  'color-critical': '#BD3C28',
  'color-critical-light': '#E5614C',
  'color-critical-faded': '#BD3C284D',

  'color-info-dark': '#084545',
  'color-info': '#24A8A8',
  'color-info-light': '#4CE5E5',
  'color-info-faded': '#24A8A84D',
}

export const lightColors: TokenMap = {
  /* Background --- */
  'color-background': '#FFFFFF',

  /* Border --- */
  'color-border': '#d4dadd',

  /* Overlay --- */
  'color-overlay': '#0C1114BF',

  /* Surface --- */
  'color-surface-subtle': '#F7F8F8',
  'color-surface': '#F1F3F4',
  'color-surface-strong': '#E6E9EA',

  /* Text --- */
  'color-text-subtle': '#00111A73',
  'color-text': '#00111abf',
  'color-text-strong': '#00111af2',
  'color-text-semantic': '#ffffff',

  /* Semantic --- */
  'color-main-dark': '#3373CC',
  'color-main': '#4D8CE5',
  'color-main-light': '#99C2FF',
  'color-main-faded': '#4D8CE54D',

  'color-neutral-dark': '#8FA0A3',
  'color-neutral': '#ABB8BA',
  'color-neutral-light': '#D5DBDD',
  'color-neutral-faded': '#ABB8BA4D',

  'color-positive-dark': '#196645',
  'color-positive': '#2EB87C',
  'color-positive-light': '#70DBAD',
  'color-positive-faded': '#2EB87C4D',

  'color-caution-dark': '#BD7F00',
  'color-caution': '#F9B21F',
  'color-caution-light': '#FFC857',
  'color-caution-faded': '#F9B21F4D',

  'color-critical-dark': '#B83314',
  'color-critical': '#E5401A',
  'color-critical-light': '#FF8366',
  'color-critical-faded': '#E5401A4D',

  'color-info-dark': '#1C9C9C',
  'color-info': '#26D9D9',
  'color-info-light': '#7DE8E8',
  'color-info-faded': '#26D9D94d',
}
