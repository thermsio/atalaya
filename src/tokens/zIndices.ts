import { TokenMap } from './types'

export const zIndices: TokenMap = {
  'z-dropdown': '1000',
  'z-sticky': '1010',
  'z-fixed': '1020',
  'z-offcanvas': '1030',
  'z-tooltip': '1040',
  'z-backdrop': '1050',
  'z-modal-backdrop': '1050',
  'z-modal': '1060',
  'z-popover': '1070',
}

export default zIndices
