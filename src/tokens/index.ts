import { breakpoints } from './breakpoints'
import { darkColors, lightColors } from './colors'
import { borderWidth, iconSizing, lineHeight, textSizing } from './sizing'
import { spacing } from './spacing'
import { zIndices } from './zIndices'

const measures = {
  ...breakpoints,
  ...borderWidth,
  ...iconSizing,
  ...lineHeight,
  ...textSizing,
  ...spacing,
  ...zIndices,
}

const darkTheme = {
  ...darkColors,
  ...measures,
}

const lightTheme = {
  ...lightColors,
  ...measures,
}

const tokens = { darkColors, darkTheme, lightColors, lightTheme, measures }

export { tokens }
