import forms from '@tailwindcss/forms'
import rootTailwindConfig from '../../tailwind.config'

const AtalayaTailwindConfig = {
  plugins: [forms],

  theme: rootTailwindConfig.theme,

  variants: rootTailwindConfig.variants,
}

export default AtalayaTailwindConfig
