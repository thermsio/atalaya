import tailwind from './tailwind'

const handler = {
  get(target, property) {
    if (target === 'tailwind') {
      console.log('*********************')
      console.log(
        'DEPRECATION WARNING - importing/requiring @therms/atalaya tailwind configs wil be removed in the next release',
      )
      console.log(
        '   The new method for requiring the tailwind configs is: @therms/atalaya/tailwind.config.js',
      )
      console.log('*********************')
    }
    return target[property]
  },
}

export const configs = new Proxy({ tailwind }, handler)
