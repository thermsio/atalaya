module.exports = {
  Code: {
    code: {
      backgroundColor: 'var(--color-surface)',
      borderRadius: '2px',
      color: 'var(--color-text-subtle)',
      padding: '2px',
    },
  },

  Playground: {
    preview: {
      color: 'var(--color-text)',
    },
  },

  Table: {
    table: {
      color: 'var(--color-text-strong)',
    },
  },
}
