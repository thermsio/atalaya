export const spaceFactor = 8
export const space = [
  spaceFactor / 2, // 4
  spaceFactor, // 8
  spaceFactor * 2, // 16
  spaceFactor * 3, // 24
  spaceFactor * 4, // 32
  spaceFactor * 5, // 40
  spaceFactor * 6, // 48
]

export const color = {
  base: 'var(--color-text)',
  light: 'var(--color-text-subtle)',
  lightest: 'var(--color-text-subtle)',
  link: 'var(--color-main)',
  linkHover: 'var(--color-main-light)',
  focus: 'var(--color-text-strong)',
  border: 'var(--color-surface-subtle)',
  name: 'var(--color-text)',
  type: 'var(--color-info)',
  error: 'var(--color-critical)',
  baseBackground: 'var(--color-background)',
  codeBackground: '#23282E',
  sidebarBackground: 'var(--color-surface)',
  ribbonBackground: 'var(--color-caution)',
  ribbonText: '#fff',
  // Based on Material Oceanic theme
  codeBase: '#c3cee3',
  codeComment: '#546e7a',
  codePunctuation: '#89ddff',
  codeProperty: '#80cbc4',
  codeDeleted: '#f07178',
  codeString: '#c3e88d',
  codeInserted: '#80cbc4',
  codeOperator: '#89ddff',
  codeKeyword: '#c792ea',
  codeFunction: '#c792ea',
  codeVariable: '#f07178',
}

export const fontFamily = {
  base: [
    '-apple-system',
    'BlinkMacSystemFont',
    '"Segoe UI"',
    '"Roboto"',
    '"Oxygen"',
    '"Ubuntu"',
    '"Cantarell"',
    '"Fira Sans"',
    '"Droid Sans"',
    '"Helvetica Neue"',
    'sans-serif',
  ],
  monospace: ['Consolas', '"Liberation Mono"', 'Menlo', 'monospace'],
}

export const fontSize = {
  base: 15,
  text: 16,
  small: 13,
  h1: 48,
  h2: 36,
  h3: 24,
  h4: 18,
  h5: 16,
  h6: 16,
}

export const mq = {
  small: '@media (max-width: 600px)',
}

export const borderRadius = 3
export const maxWidth = 1000
export const sidebarWidth = 200

export const buttonTextTransform = 'uppercase'
