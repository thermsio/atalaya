export interface TypeScaleDetailsInterface {
  cssName: string
  lineHeight: string
  name: string
  size: string
  tailwindName: string
  tokenName: string
  usageNotes: string
}

export const typeScalesDetailsData: Array<TypeScaleDetailsInterface> = [
  {
    cssName: '--size-text-sm / --line-height-text-sm',
    lineHeight: '1.25rem / 20px',
    name: 'Small',
    size: '0.875rem / 14px',
    tailwindName: 'text-sm',
    tokenName: 'size-text-sm / line-height-text-sm',
    usageNotes:
      'Great for secondary text such as tooltips, input fields hints and aside text. Some elements like buttons and badges also make extensive use of this scale',
  },

  {
    cssName: '--size-text / --line-height-text',
    lineHeight: '1.5rem / 24px',
    name: 'Base',
    size: '1rem / 16px',
    tokenName: 'size-text / line-height-text',
    tailwindName: 'text-base',
    usageNotes: 'The default scale, should be the predominant text size.',
  },

  {
    cssName: '--size-text-md / --line-height-text-md',
    lineHeight: '1.75rem / 28px',
    name: 'Medium',
    size: '1.25rem / 20px',
    tokenName: 'size-text-md / line-height-text-md',
    tailwindName: 'text-md',
    usageNotes: 'Sub-titles and important sentences.',
  },

  {
    cssName: '--size-text-lg / --line-height-text-lg',
    lineHeight: '2rem / 32px',
    name: 'Large',
    size: '1.5rem / 24px',
    tokenName: 'size-text-lg / line-height-text-lg',
    tailwindName: 'text-lg',
    usageNotes: 'Titles or other short text lines.',
  },

  {
    cssName: '--size-text-xl / --line-height-text-xl',
    lineHeight: '2.75rem / 44px',
    name: 'Extra Large',
    size: '2.25rem / 36px',
    tokenName: 'size-text-xl / line-height-text-xl',
    tailwindName: 'text-xl',
    usageNotes: 'Rare. Recommended for short texts when space is plenty.',
  },

  {
    cssName: '--size-text-xxl / --line-height-text-xxl',
    lineHeight: '3.75rem / 60px',
    name: 'Display',
    size: '3.75rem / 60px',
    tokenName: 'size-text-xxl / line-height-text-xxl',
    tailwindName: 'text-xxl',
    usageNotes: 'Only for very special cases.',
  },
]
