export const structuralColors = [
  {
    displayName: 'Background',
    constantName: 'Background',
  },
  {
    displayName: 'Surface Subtle',
    constantName: 'SurfaceSubtle',
  },
  {
    displayName: 'Surface',
    constantName: 'Surface',
  },
  {
    displayName: 'Surface Strong',
    constantName: 'SurfaceStrong',
  },
  {
    displayName: 'Border',
    constantName: 'Border',
  },
  {
    displayName: 'Input Background',
    constantName: 'InputBackground',
  },
  {
    displayName: 'Scrollbar Track',
    constantName: 'ScrollbarTrack',
  },
  {
    displayName: 'Scrollbar Thumb',
    constantName: 'ScrollbarThumb',
  },
  {
    displayName: 'Scrollbar Thumb Highlight',
    constantName: 'ScrollbarThumbHighlight',
  },
  {
    displayName: 'Scrollbar Track',
    constantName: 'ScrollbarTrack',
  },
]

export const textColors = [
  {
    displayName: 'Text Subtle',
    constantName: 'TextSubtle',
  },
  {
    displayName: 'Text',
    constantName: 'Text',
  },
  {
    displayName: 'Text Strong',
    constantName: 'TextStrong',
  },
  {
    displayName: 'Text Semantic',
    constantName: 'TextSemantic',
  },
]

export const mainColors = [
  {
    displayName: 'Main Dark',
    constantName: 'MainDark',
  },
  {
    displayName: 'Main',
    constantName: 'Main',
  },
  {
    displayName: 'Main Light',
    constantName: 'MainLight',
  },
  {
    displayName: 'Main Faded',
    constantName: 'MainFaded',
  },
]

export const neutralColors = [
  {
    displayName: 'Neutral Dark',
    constantName: 'NeutralDark',
  },
  {
    displayName: 'Neutral',
    constantName: 'Neutral',
  },
  {
    displayName: 'Neutral Light',
    constantName: 'NeutralLight',
  },
  {
    displayName: 'Neutral Faded',
    constantName: 'NeutralFaded',
  },
]

export const positiveColors = [
  {
    displayName: 'Positive Dark',
    constantName: 'PositiveDark',
  },
  {
    displayName: 'Positive',
    constantName: 'Positive',
  },
  {
    displayName: 'Positive Light',
    constantName: 'PositiveLight',
  },
  {
    displayName: 'Positive Faded',
    constantName: 'PositiveFaded',
  },
]

export const cautionColors = [
  {
    displayName: 'Caution Dark',
    constantName: 'CautionDark',
  },
  {
    displayName: 'Caution',
    constantName: 'Caution',
  },
  {
    displayName: 'Caution Light',
    constantName: 'CautionLight',
  },
  {
    displayName: 'Caution Faded',
    constantName: 'CautionFaded',
  },
]

export const criticalColors = [
  {
    displayName: 'Critical Dark',
    constantName: 'CriticalDark',
  },
  {
    displayName: 'Critical',
    constantName: 'Critical',
  },
  {
    displayName: 'Critical Light',
    constantName: 'CriticalLight',
  },
  {
    displayName: 'Critical Faded',
    constantName: 'CriticalFaded',
  },
]

export const infoColors = [
  {
    displayName: 'Info Dark',
    constantName: 'InfoDark',
  },
  {
    displayName: 'Info',
    constantName: 'Info',
  },
  {
    displayName: 'Info Light',
    constantName: 'InfoLight',
  },
  {
    displayName: 'Info Faded',
    constantName: 'InfoFaded',
  },
]
