import React from 'react'

import SectionsRenderer from 'react-styleguidist/lib/client/rsg-components/Sections/SectionsRenderer'
import ThemeSwitcher from '../components/colors/ThemeSwitcher'

const SectionsRendererWrapper = (props): React.ReactElement => {
  return (
    <div>
      <div className="sticky right-0 top-0 flex justify-end bg-background py-sm">
        <ThemeSwitcher />
      </div>

      <SectionsRenderer {...props} />
    </div>
  )
}

export default SectionsRendererWrapper
