import React from 'react'
import StyleGuideRenderer from 'react-styleguidist/lib/client/rsg-components/StyleGuide/StyleGuideRenderer'
import { AtalayaWrapper } from '../../src'

const StyleGuideRendererWrapper = (props) => {
  return (
    <AtalayaWrapper>
      <StyleGuideRenderer {...props} />
    </AtalayaWrapper>
  )
}

export default StyleGuideRendererWrapper
