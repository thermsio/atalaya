import React from 'react'
import PlaygroundRenderer from 'react-styleguidist/lib/client/rsg-components/Playground/PlaygroundRenderer'
import '../../src/styles/css/all.css'

const PlaygroundRendererWrapper = (props) => <PlaygroundRenderer {...props} />

export default PlaygroundRendererWrapper
