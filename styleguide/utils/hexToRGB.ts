const hexToRGB = (hex: string): string => {
  let r = 0,
    g = 0,
    b = 0,
    a = 1

  r = parseInt(hex[1] + hex[2], 16)
  g = parseInt(hex[3] + hex[4], 16)
  b = parseInt(hex[5] + hex[6], 16)

  if (hex.length == 9) {
    a = parseInt(hex[7] + hex[8], 16)
    a = +(a / 255).toFixed(2)

    return `rgba(${+r},${+g},${+b},${a})`
  }

  return `rgb(${+r},${+g},${+b})`
}

export default hexToRGB
