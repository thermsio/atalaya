import React from 'react'

import { TypeScaleDetailsInterface as Props } from '../../constants/typography'

function TypeScaleDetails({
  cssName,
  lineHeight,
  name,
  size,
  tailwindName,
  tokenName,
  usageNotes,
}: Props) {
  return (
    <div className="rounded-lg bg-surface shadow">
      <div className="p-base sm:px-sm">
        <h3 className="text-lg font-medium leading-6 text-color">{name}</h3>
        <p className={`${tailwindName} mt-1 truncate text-color-subtle`}>
          The quick brown fox jumps over the lazy dog.
        </p>
      </div>
      <div className="bg-surface-subtle px-sm sm:px-0">
        <dl className="sm:divide-y sm:divide-border">
          <div className="py-sm sm:grid sm:grid-cols-3 sm:gap-xxs sm:px-sm">
            <dt className="text-sm font-medium text-color-subtle">
              Token Name
            </dt>
            <dd className="mt-xxs text-sm text-color sm:col-span-2 sm:mt-0">
              {tokenName}
            </dd>
          </div>
          <div className="py-sm sm:grid sm:grid-cols-3 sm:gap-xxs sm:px-sm">
            <dt className="text-sm font-medium text-color-subtle">
              CSS Variable
            </dt>
            <dd className="mt-xxs text-sm text-color sm:col-span-2 sm:mt-0">
              {cssName}
            </dd>
          </div>
          <div className="py-sm sm:grid sm:grid-cols-3 sm:gap-xxs sm:px-sm">
            <dt className="text-sm font-medium text-color-subtle">
              Tailwind Class
            </dt>
            <dd className="mt-xxs text-sm text-color sm:col-span-2 sm:mt-0">
              {tailwindName}
            </dd>
          </div>
          <div className="py-sm sm:grid sm:grid-cols-3 sm:gap-xxs sm:px-sm">
            <dt className="text-sm font-medium text-color-subtle">Size</dt>
            <dd className="mt-xxs text-sm text-color sm:col-span-2 sm:mt-0">
              {size}
            </dd>
          </div>
          <div className="py-sm sm:grid sm:grid-cols-3 sm:gap-xxs sm:px-sm">
            <dt className="text-sm font-medium text-color-subtle">
              Line Height
            </dt>
            <dd className="mt-xxs text-sm text-color sm:col-span-2 sm:mt-0">
              {lineHeight}
            </dd>
          </div>
          <div className="py-sm sm:grid sm:grid-cols-3 sm:gap-xxs sm:px-sm">
            <dt className="text-sm font-medium text-color-subtle">Usage</dt>
            <dd className="mt-xxs text-sm text-color sm:col-span-2 sm:mt-0">
              {usageNotes}
            </dd>
          </div>
        </dl>
      </div>
    </div>
  )
}

export default TypeScaleDetails
