import React from 'react'

import TypeScaleDetails from './TypeScaleDetails'
import {
  typeScalesDetailsData,
  TypeScaleDetailsInterface,
} from '../../constants/typography'

const TypeScalesList = (): React.ReactElement => {
  return (
    <div className="space-y-lg">
      {typeScalesDetailsData.map(
        ({
          cssName,
          lineHeight,
          name,
          size,
          tailwindName,
          tokenName,
          usageNotes,
        }: TypeScaleDetailsInterface) => (
          <TypeScaleDetails
            cssName={cssName}
            lineHeight={lineHeight}
            name={name}
            size={size}
            tailwindName={tailwindName}
            tokenName={tokenName}
            usageNotes={usageNotes}
            key={tokenName}
          />
        ),
      )}
    </div>
  )
}

export default TypeScalesList
