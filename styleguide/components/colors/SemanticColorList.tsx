import React from 'react'

import { Tag } from '../../../src/index'

const SemanticColorList = (): React.ReactElement => {
  return (
    <div className="flex justify-center rounded-md p-lg">
      <div className="grid grid-cols-7 grid-rows-5 items-end gap-lg">
        <div />
        <div className="text-main">Main</div>
        <div className="text-neutral">Neutral</div>
        <div className="text-positive">Positive</div>
        <div className="text-caution">Caution</div>
        <div className="text-critical">Critical</div>
        <div className="text-info">Info</div>

        <div className="text-right">Action</div>
        <Tag variant="main" value="Primary" />
        <Tag variant="neutral" value="Secondary" />
        <Tag variant="positive" value="Confirm" />
        <Tag variant="caution" value="Hold" />
        <Tag variant="critical" value="Remove" />
        <Tag variant="info" value="Help" />

        <div className="text-right">Message</div>
        <div />
        <div />
        <Tag variant="positive" value="Success" />
        <Tag variant="caution" value="Warning" />
        <Tag variant="critical" value="Danger" />
        <Tag variant="info" value="Info" />

        <div className="text-right">Presence</div>
        <div />
        <Tag variant="neutral" value="Off Duty" />
        <Tag variant="positive" value="On Duty" />
        <Tag variant="caution" value="On Call" />
        <Tag variant="critical" value="Busy" />
        <div />

        <div className="text-right">Priority</div>
        <div />
        <div />
        <Tag variant="positive" value="Low" />
        <Tag variant="caution" value="Medium" />
        <Tag variant="critical" value="High" />
        <Tag variant="info" value="Info" />

        <div className="text-right">Severity</div>
        <div />
        <div />
        <Tag variant="positive" value="Low" />
        <Tag variant="caution" value="Medium" />
        <Tag variant="critical" value="High" />
        <Tag variant="info" value="None" />

        <div className="text-right">Status</div>
        <Tag variant="main" value="Active" />
        <Tag variant="neutral" value="Default" />
        <Tag variant="positive" value="Complete" />
        <Tag variant="caution" value="Attention" />
        <Tag variant="critical" value="Problem" />
        <Tag variant="info" value="New" />
      </div>
    </div>
  )
}

export default SemanticColorList
