import React from 'react'

import ColorCard, { ColorCardProps } from './ColorCard'

type Props = { colors: Array<ColorCardProps> }

function ColorCardGroup({ colors }: Props) {
  return (
    <div className="flex justify-center rounded-md p-lg">
      <ul>
        {colors.map(({ displayName, constantName }: ColorCardProps) => (
          <li key={displayName}>
            <ColorCard displayName={displayName} constantName={constantName} />
          </li>
        ))}
      </ul>
    </div>
  )
}

export default ColorCardGroup
