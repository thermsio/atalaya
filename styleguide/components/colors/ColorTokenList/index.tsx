import React from 'react'

import SectionTitle from './components/SectionTitle'
import ColorToken from './components/ColorToken'
import ColorTokenGroup from './components/ColorTokenGroup'

const ColorTokens: React.FC = () => {
  return (
    <div>
      <SectionTitle>Background</SectionTitle>
      <div className="mt-xs">
        <ColorToken
          colorVal="var(--color-background)"
          tokenName="color-background"
        />
      </div>

      <SectionTitle>Border</SectionTitle>
      <div className="mt-xs">
        <ColorToken colorVal="var(--color-border)" tokenName="color-border" />
      </div>

      <SectionTitle>Surface</SectionTitle>
      <ColorTokenGroup
        tokens={[
          {
            colorVal: 'var(--color-surface-subtle)',
            tokenName: 'color-surface-subtle',
          },
          { colorVal: 'var(--color-surface)', tokenName: 'color-surface' },
          {
            colorVal: 'var(--color-surface-strong)',
            tokenName: 'color-surface-strong',
          },
        ]}
      />

      <SectionTitle>Text</SectionTitle>
      <ColorTokenGroup
        tokens={[
          {
            colorVal: 'var(--color-text-subtle)',
            isText: true,
            tokenName: 'color-text-subtle',
          },
          {
            colorVal: 'var(--color-text)',
            isText: true,
            tokenName: 'color-text',
          },
          {
            colorVal: 'var(--color-text-strong)',
            isText: true,
            tokenName: 'color-text-strong',
          },
          {
            colorVal: 'var(--color-text-semantic)',
            isText: true,
            tokenName: 'color-text-semantic',
          },
        ]}
      />

      <SectionTitle>Main</SectionTitle>
      <ColorTokenGroup
        tokens={[
          {
            colorVal: 'var(--color-main-dark)',
            tokenName: 'color-main-dark',
          },
          {
            colorVal: 'var(--color-main)',
            tokenName: 'color-main',
          },
          {
            colorVal: 'var(--color-main-light)',
            tokenName: 'color-main-light',
          },
          {
            colorVal: 'var(--color-main-faded)',
            tokenName: 'color-main-faded',
          },
        ]}
      />

      <SectionTitle>Neutral</SectionTitle>
      <ColorTokenGroup
        tokens={[
          {
            colorVal: 'var(--color-neutral-dark)',
            tokenName: 'color-neutral-dark',
          },
          {
            colorVal: 'var(--color-neutral)',
            tokenName: 'color-neutral',
          },
          {
            colorVal: 'var(--color-neutral-light)',
            tokenName: 'color-neutral-light',
          },
          {
            colorVal: 'var(--color-neutral-faded)',
            tokenName: 'color-neutral-faded',
          },
        ]}
      />

      <SectionTitle>Positive</SectionTitle>
      <ColorTokenGroup
        tokens={[
          {
            colorVal: 'var(--color-positive-dark)',
            tokenName: 'color-positive-dark',
          },
          {
            colorVal: 'var(--color-positive)',
            tokenName: 'color-positive',
          },
          {
            colorVal: 'var(--color-positive-light)',
            tokenName: 'color-positive-light',
          },
          {
            colorVal: 'var(--color-positive-faded)',
            tokenName: 'color-positive-faded',
          },
        ]}
      />

      <SectionTitle>Caution</SectionTitle>
      <ColorTokenGroup
        tokens={[
          {
            colorVal: 'var(--color-caution-dark)',
            tokenName: 'color-caution-dark',
          },
          {
            colorVal: 'var(--color-caution)',
            tokenName: 'color-caution',
          },
          {
            colorVal: 'var(--color-caution-light)',
            tokenName: 'color-caution-light',
          },
          {
            colorVal: 'var(--color-caution-faded)',
            tokenName: 'color-caution-faded',
          },
        ]}
      />

      <SectionTitle>Critical</SectionTitle>
      <ColorTokenGroup
        tokens={[
          {
            colorVal: 'var(--color-critical-dark)',
            tokenName: 'color-critical-dark',
          },
          {
            colorVal: 'var(--color-critical)',
            tokenName: 'color-critical',
          },
          {
            colorVal: 'var(--color-critical-light)',
            tokenName: 'color-critical-light',
          },
          {
            colorVal: 'var(--color-critical-faded)',
            tokenName: 'color-critical-faded',
          },
        ]}
      />

      <SectionTitle>Info</SectionTitle>
      <ColorTokenGroup
        tokens={[
          {
            colorVal: 'var(--color-info-dark)',
            tokenName: 'color-info-dark',
          },
          {
            colorVal: 'var(--color-info)',
            tokenName: 'color-info',
          },
          {
            colorVal: 'var(--color-info-light)',
            tokenName: 'color-info-light',
          },
          {
            colorVal: 'var(--color-info-faded)',
            tokenName: 'color-info-faded',
          },
        ]}
      />
    </div>
  )
}

export default ColorTokens
