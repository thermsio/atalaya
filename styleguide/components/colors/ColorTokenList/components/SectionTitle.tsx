import React, { ReactNode } from 'react'

interface SectionTitleProps {
  children: ReactNode
}

function SectionTitle({ children }: SectionTitleProps) {
  return (
    <div className="mt-base font-bold uppercase text-color-subtle">
      {children}
    </div>
  )
}

export default SectionTitle
