import React from 'react'

import ColorToken, { ColorTokenProps } from './ColorToken'

type Props = {
  tokens: Array<ColorTokenProps>
}

function ColorTokenGroup({ tokens }: Props) {
  return (
    <div className="mt-xs grid grid-cols-1 gap-xs sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
      {tokens.map(({ colorVal, isText, tokenName }: ColorTokenProps) => (
        <ColorToken
          colorVal={colorVal}
          isText={isText}
          tokenName={tokenName}
          key={tokenName}
        />
      ))}
    </div>
  )
}

export default ColorTokenGroup
