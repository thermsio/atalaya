import React, { useMemo } from 'react'

export type ColorTokenProps = {
  colorVal: string
  isText?: boolean
  tokenName: string
}

function ColorToken({ colorVal, isText = false, tokenName }: ColorTokenProps) {
  const thumbnail = useMemo(() => {
    if (isText)
      return (
        <div className="text-lg font-bold" style={{ color: colorVal }}>
          Aa
        </div>
      )

    return (
      <div
        className="h-lg w-lg rounded shadow"
        style={{ backgroundColor: colorVal }}
      />
    )
  }, [colorVal, isText])

  return (
    <div className="flex h-full items-center space-x-sm rounded bg-surface p-sm">
      {thumbnail}
      <div>{tokenName}</div>
    </div>
  )
}

export default ColorToken
