import React from 'react'

type Props = {
  colorClass: string
  cssName: string
  tailwindName: string
  tokenName: string
}

function TokenTableRow({
  colorClass,
  cssName,
  tailwindName,
  tokenName,
}: Props) {
  return (
    <tr>
      <td className="whitespace-nowrap p-sm text-sm font-bold text-color">
        <div className="flex space-x-sm">
          <div className={`${colorClass} h-lg w-lg rounded`} />
          <div>{tokenName}</div>
        </div>
      </td>
      <td className="whitespace-nowrap p-sm text-sm text-color-subtle">
        {cssName}
      </td>
      <td className="whitespace-nowrap p-sm text-sm text-color-subtle">
        {tailwindName}
      </td>
    </tr>
  )
}

export default TokenTableRow
