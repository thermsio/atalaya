import React from 'react'
import DarkModeToggle from 'react-dark-mode-toggle'
import { useAtalayaContext } from '../../../src'

const ThemeSwitcher = (): React.ReactElement => {
  const { isDarkMode, toggleDarkMode } = useAtalayaContext()

  return (
    <DarkModeToggle
      onChange={toggleDarkMode}
      checked={isDarkMode}
      size={50}
      speed={3}
    />
  )
}

export default ThemeSwitcher
