import React, { useMemo } from 'react'
import { Inline, Stack, useDarkModeTheme } from '../../../src'
import { Constants } from '../../../src'
import hexToRGB from '../../utils/hexToRGB'

export interface ColorCardProps {
  displayName: string
  constantName: string
}

const Color = Constants.Color

const ColorCard = ({
  displayName,
  constantName,
}: ColorCardProps): React.ReactElement => {
  // We use useDarkModeTheme instead of useAtalayaWrapper because Styleguidist examples are in a separate tree outside
  // AtalayaContext. useDarkModeTheme works because it's value is based on a localStorage property.
  const [isDarkMode] = useDarkModeTheme()
  const hexValue = useMemo(
    () => Color[isDarkMode ? 'Dark' : 'Light'][constantName],
    [isDarkMode, constantName],
  )

  return (
    <Inline alignY="center" space="base">
      <div
        className="h-24 w-24 shadow-md"
        style={{ backgroundColor: `var(${Color.CSSVarMap[constantName]})` }}
      />

      <Stack
        className="flex-grow"
        background="surface"
        padding="lg"
        borderRadius="default"
      >
        <div className="truncate font-bold uppercase">{displayName}</div>

        <Inline space="sm" className="truncate font-mono text-color-subtle">
          <p>{hexValue}</p>
          <p>|</p>
          <p>{hexToRGB(hexValue)}</p>
        </Inline>
      </Stack>
    </Inline>
  )
}

export default ColorCard
