import React from 'react'

export interface TokenTableRowProps {
  pixel?: string
  rem?: string
  tokenName?: string
}

function TokenTableRow({ pixel, rem, tokenName }: TokenTableRowProps) {
  return (
    <tr className="bg-surface-subtle">
      <td className="whitespace-nowrap p-sm text-sm font-bold text-color">
        <div className="flex space-x-sm">
          <div>{tokenName}</div>
        </div>
      </td>
      <td className="whitespace-nowrap p-sm text-sm text-color-subtle">
        {rem}
      </td>
      <td className="whitespace-nowrap p-sm text-sm text-color-subtle">
        {pixel}
      </td>
    </tr>
  )
}

export default TokenTableRow
