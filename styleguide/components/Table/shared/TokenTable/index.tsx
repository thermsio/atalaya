import React from 'react'
import TokenTableRow, { TokenTableRowProps } from './atoms/TokenTableRow'
import Table from '../../Table'

interface TokenTableProps {
  rows: TokenTableRowProps[]
}

function TokenTable({ rows }: TokenTableProps) {
  return (
    <Table headers={['Token', 'REM Size', 'Pixel Size']}>
      {rows.map(({ pixel, rem, tokenName }) => (
        <TokenTableRow
          pixel={pixel}
          rem={rem}
          tokenName={tokenName}
          key={tokenName}
        />
      ))}
    </Table>
  )
}

export { TokenTable }
