import React, { ReactNode } from 'react'

interface Props {
  children: ReactNode
}

function TH({ children }: Props) {
  return (
    <th
      scope="col"
      className="p-sm text-left text-xs font-bold uppercase tracking-wider text-color-subtle"
    >
      {children}
    </th>
  )
}

export default TH
