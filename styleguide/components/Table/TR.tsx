import React, { ReactElement, ReactNode } from 'react'

interface Props {
  children: ReactNode
}

const TR = ({ children }: Props): ReactElement => {
  return <tr className="border-transparent bg-surface-subtle">{children}</tr>
}

export default TR
