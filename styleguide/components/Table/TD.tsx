import React, { ReactElement, ReactNode } from 'react'

interface Props {
  children: ReactNode
}

const TD = ({ children }: Props): ReactElement => {
  return <td className="p-sm">{children}</td>
}

export default TD
