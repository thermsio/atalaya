import React, { ReactElement, ReactNode } from 'react'

import TH from './TH'

interface Props {
  headers: Array<string>
  children: ReactNode
}

const Table = ({ headers, children }: Props): ReactElement => {
  return (
    <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
      <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
        <div className="overflow-hidden border-b border-surface-strong shadow sm:rounded-lg">
          <table className="min-w-full divide-y divide-border">
            <thead className="bg-surface">
              <tr>
                {headers.map((header, index) => (
                  <TH key={`${header}/${index}`}>{header}</TH>
                ))}
              </tr>
            </thead>

            <tbody className="bg-surface-subtle">{children}</tbody>
          </table>
        </div>
      </div>
    </div>
  )
}

export default Table
