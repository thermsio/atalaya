The goal of Layout components is to distribute their children elements in an
area.

Their goal is to provide a standard way in which to achieve your desired
layouts. In other words, to stop you from having to apply to same set of rules
every time you want to have a particular layout.

We provide several components, each specializing in a particular kind of layout,
that accept different props in order to achieve the desired look.

## Responsive Design

You may use Arrays in component's props to provide different values for the
different screen breakpoints.

The values will be assigned starting from the smallest screen size breakpoint to
the widest.

For example, let's say you have a layout component that accepts a `padding`
prop, you have several options:

- `padding="base"` will add base padding to that element for every breakpoint
- `padding={['xs', 'sm', 'base']}` will use xs padding for smallest screens, sm
  padding for screens after **sm breakpoint** and base padding for anything
  later than **md breakpoint**
- `padding={['xs', 'sm', '', '', 'base']}` will skip **md & lg breakpoints** and
  will start applying base padding after **xl breakpoint** has been reached.
