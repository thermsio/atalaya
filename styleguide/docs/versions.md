[![Generic badge](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

![npm (scoped with tag)](https://img.shields.io/npm/v/@therms/atalaya/latest?label=%40therms%2Fatalaya&style=flat-square)

Git `master` branch, the latest production/stable release, docs: [@therms/atalaya](/)

```bash
npm i @therms/atalaya
```

---

![npm (scoped with tag)](https://img.shields.io/npm/v/@therms/atalaya/next?label=%40therms%2Fatalaya&style=flat-square)

Git `next` branch, the next major version stable release, docs: [@therms/atalaya@next](/@next)

```bash
npm i @therms/atalaya@next
```

Development and features on the `next` branch is primarily for breaking changes (major release version change).
This branch/tag should be considered mostly stable and typically published alongside of a lower @latest major release
in order to give packages time to migrate from a previous version.

---

![npm (scoped with tag)](https://img.shields.io/npm/v/@therms/atalaya/beta?label=%40therms%2Fatalaya&style=flat-square)

Git `x.x.x-beta.x` branch, development effort release, docs: [@therms/atalaya@beta](/@beta)

```bash
npm i @therms/atalaya@beta
```

Development, refactoring, additions, improvements, etc. are done on the `beta` branch.
