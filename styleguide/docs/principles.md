In order to define our design principles we need to know who is using our products.

So who are they? People working on the private security industry, the vast majority being security officers and, the rest being supervisors, administrative personnel and business owners. While this people may perform different tasks around the app, we can define two distinct type of users: _officers_ and _administrators_ (it's worth keeping in mind that in many cases a person can be both acting as an officer and administrator)

**Officers** are working out in the field and often use our products on their phone. They usually need to complete the same task multiple times and need to do it as fast as possible. As such they primarily value ease of use, simplicity and responsiveness.

**Administrators** are in charge of supervising the officers and configuring the app. They can afford working with a computer and often need more advanced controls. They value clarity of information and customization.

Our design principles cater to these users.

### Keep it simple

Officers are usually on the clock, on the move and need to complete the same task multiple times. Everything we can do to reduce unnecessary interactions it's a boon for them. Administrators will naturally require more complexity on their areas and keeping things as simple as possible here will reduce the cognitive noise that comes with that complexity.

### Be consistent

Both for brand identity and for establishing use patterns that makes easier for our users to find their way in our products

### Be thoughtful about the details

Think about who will be consuming your component. Officers usually need to know just a few main facts about an issue while administrators have time for the details.
