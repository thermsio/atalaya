We use icons to reinforce ideas. Usually paired with text, these icons provide
easily recognizable queues that our users learn to associate with different
aspects of our products.

### Pair them with text

Icons don't convey meaning by themselves, to avoid confusion you should
generally use them as visual aids right next to their labels. The only exception
to this are universally known symbols that will be easily identified by anyone,
like using a right arrow in a pagination element to convey 'next page', or an X
to convey 'close'.

### Don't overuse them

Be mindful of not being overwhelming by adding too many icons; it can be
detrimental for the user experience. I.e. when space is limited and there are
too many options to fit in.

Keep in mind that due to the modularity of our products, some users might have
many more modules enabled than others and as a result their screens will get
much crowded than people with fewer active modules.

### Keep them the same color as the text

It further enhances the relation between this two elements.

## When to use them

There are several places where icons can be used:

**Navigation** Icons can be used by themselves to depict universally understood
actions such as a right pointing arrow signifying 'next page'. Aside from this
special cases, icons can be used right next from their text counterparts on nav
bars or tabs to aid navigation.

**Actions** Icons can be paired with their labels on buttons. Normally you
should never use a button with just an icon and no label; there are exceptions,
like the contextual menu button (the one with the 3 dots).

**File Type** Adding an icon to a file card greatly helps users identify the
file they are looking for. You can choose to either show general file icons (
video, file, audio) or more detailed versions that specify the file type (xls,
txt, doc). Whenever possible instead of showing an icon for an image file, opt
to show the actual image thumbnail.

## Size

Icon size scale is made to work out of the box with their corresponding text
size. That is, if you have small text and small icons, both should have the same
height and be vertically centered with no extra work from your part.

### Design Tokens

```jsx noeditor
import { TokenTable } from "../components/Table/shared/TokenTable";

<TokenTable rows={[
  { pixel: '20px', rem: "1.25rem", tokenName: "size-icon-sm" },
  { pixel: '24px', rem: "1.5rem", tokenName: "size-icon" },
  { pixel: '32px', rem: "2rem", tokenName: "size-icon-lg" },
  { pixel: '40px', rem: "2.5rem", tokenName: "size-icon-xl" },
  { pixel: '60px', rem: "3.75rem", tokenName: "size-icon-xxl" },
]} />
```
