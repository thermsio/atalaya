## Line Length

It's important to keep text lines from becoming too wide as this makes it harder for users to read them. Ideally you would want to be fitting between 80 and 100 characters on each line of text.

Excluding some edge cases like very big display text, body containers should have a max-width of **768px** to prevent them from getting too wide.

## Emphasis

Usually you will be in situations where you have a lot of different text bodies on the same view, each body having different levels of importance. There are a couple of different ways to show your text's hierarchy:

- Use text with different contrast levels
- Use different font wights
- Use different text sizes

This list is ordered from the most subtle hints to the least subtle ones, try going for the more subtle option first. Ideally you should identify what's your main text and decide the rest of the content's hierarchy around that. In many cases just a distinction between base and soft text does the trick.

```jsx noeditor
import TypeScalesList from '../components/typography/TypeScalesList'

;<TypeScalesList />
```
