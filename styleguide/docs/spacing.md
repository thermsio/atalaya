Inconsistent use of spacing give your layouts a sense of disorder and messiness.
Users might not identify exactly that an app has inconsistent spacing, but they
can sense something is wrong about it.

Spacing can also be used to express the proximity of the relation between
elements, the closer elements are, the stronger their relation is. For instance,
you may have a `heading`, `sub-heading` and a `paragraph`. `heading`
and `sub-heading` would stand closer to each other compared to `paragraph` and
the 3 of them would be closer to each other compared to the rest of the page.

To help maintain consistency we defined a set of spacing units that should be
enough to create any layout.

To ensure dimensions stay proportional, even if users change font size in their
system settings, we base our sizing in *rem* units.

## Design Tokens

```js noeditor
import { TokenTable } from "../components/Table/shared/TokenTable";

<TokenTable rows={[
  { pixel: '4px', rem: "0.25rem", tokenName: "spacing-xxs" },
  { pixel: '8px', rem: "0.5rem", tokenName: "spacing-xs" },
  { pixel: '12px', rem: "0.75rem", tokenName: "spacing-sm" },
  { pixel: '16px', rem: "1rem", tokenName: "spacing-base" },
  { pixel: '20px', rem: "1.25rem", tokenName: "spacing-lg" },
  { pixel: '32px', rem: "2rem", tokenName: "spacing-xl" },
  { pixel: '48px', rem: "3rem", tokenName: "spacing-xxl" },
]}/>
```

Note: pixels values are assuming an `1rem === 16px` relation, which is the
default for most modern browsers.
