Our default theme is dark because most of the officers work is carried out at
night. A dark theme keeps their dark vision from spoiling.

We divide our colors into three categories:

**Structural** colors are the ones you will be using to give depth and
separation to different elements. Specifically, they are used as background,
border or surface colors.

**Text** colors are meant to only be used on text elements, specially tweaked to
ensure readability.

**Semantic** colors are meant to convey specific meaning to the user, like
priority levels.

## Structural

```jsx noeditor
import ColorCardGroup from '../components/colors/ColorCardGroup';
import { structuralColors } from '../constants/colors';

<div className="my-xl">
  <ColorCardGroup colors={structuralColors} />
</div>
```

These are your neutral filler colors. They are intentionally monochromatic in
order to exalt our semantic colors. We named them to reflect their intended
uses:

- **Background** should be used as the app's base color, it provides the best
  contrast with text.
- **Border** is meant to be used as a border or divider color.
- **Surface** is for background colors of elements that are above background.
  It's usually best to place elements directly on the background but there are
  many cases where a creating a visual subdivision makes the most sense, like
  Cards or Modals. Surface comes in 3 shades that can be used to create
  different contrast ratios.

## Text

```jsx noeditor
import ColorCardGroup from '../components/colors/ColorCardGroup';
import { textColors } from '../constants/colors';

<div className="my-xl">
  <ColorCardGroup colors={textColors} />
</div>
```

Body text uses an exclusive color palette especially designed to stand out
against the structural colors. If you want to make a text body less important
you would use the subtle color or if you wanted it to grab people's attention
you would use the strong color. Note that changing text color it's not the only
way to exalt or subdue elements, changing text size or font weight are also
valid options. Details about text hierarchy can be found on
the [Typography section](#/Foundations/Typography.md).

## Semantic

Lastly we have a **Semantic** text color which is not as self-explanatory as the
rest. This color is meant to be used exclusively in contrast with semantic color
backgrounds. We support both light and dark themes, this basically means our
color palette gets inverted (when background becomes light text becomes dark to
provide contrast), except for semantic colors, which stay more or less the same.
Having a dedicated color ensures good contrast for these semantic elements like
buttons, tags, badges.

```js noeditor
import ColorCardGroup from '../components/colors/ColorCardGroup';
import {
  mainColors,
  neutralColors,
  positiveColors,
  cautionColors,
  criticalColors,
  infoColors,
} from '../constants/colors';


<div
  className="grid grid-cols-1 xl:grid-cols-2 gap-lg my-xl justify-items-start">
  <ColorCardGroup colors={mainColors}/>
  <ColorCardGroup colors={neutralColors}/>
  <ColorCardGroup colors={positiveColors}/>
  <ColorCardGroup colors={cautionColors}/>
  <ColorCardGroup colors={criticalColors}/>
  <ColorCardGroup colors={infoColors}/>
</div>
```

We refer to the rest of the colors as semantic because they are used to convey
meaning, such as priority levels, message importance, or primary actions. Once
users have seen the color and meaning paired a few times it's easy fot them to
make the association between the two.

In most cases you will only ever need to use the base color from each category.
Dark and Light variants are meant to give a little flexibility for some edge
cases like button states. When a button is pressed or active you would use the
dark variant of the color and when it's being hovered on you would use the light
variant.

These colors all have high saturation and produce high contrast against the
structural colors. This is by design to catch the user's attention, but it also
means special care must be put into not overusing them. Too much color can
overload the user, producing confusion.

You might have noticed that as opposed to Background and Text colors, their
names don't suggest any intended use case. That's because the same color may be
used for different meanings. In the next table you will see all the possible
meanings we can convey and what color should be used for each of them.

```js noeditor
import SemanticColorList from '../components/colors/SemanticColorList';

<div className="my-xl">
  <SemanticColorList/>
</div>
```

## Design Tokens

For each of these colors we have created a token that represents its HEX value.

The `tokens` object we provide, contains all of these value pairs related to
design tokens. You can simply import it and start adding them into whichever
styling technology your app relies on.

```jsx noeditor
import ColorTokenList from '../components/colors/ColorTokenList';

<ColorTokenList />
```
