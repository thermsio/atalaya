This section lays out the different components you may use in your React
applications.

### Props & Methods

Inside each component page you will find a "Props & Methods" button you can
click to see all the possible props any given component takes and what are their
accepted values.

### View Code

By clicking you on the 'View Code' button you can see how each example it's
composed. Examples are fully interactive too, you are free to edit the code in
there, the example will automatically update with your changes.
