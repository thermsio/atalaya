# @therms/atalaya

React components and style guide for THERMS.

[![Generic badge](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

## Issue Tracker [https://bitbucket.org/thermsio/atalaya/issues](https://bitbucket.org/thermsio/atalaya/issues)

## Install

[![Generic badge](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

![npm (scoped with tag)](https://img.shields.io/npm/v/@therms/atalaya/latest?label=%40therms%2Fatalaya&style=flat-square)

Git `master` branch, the latest production/stable release, docs: [@therms/atalaya](/)

```bash
npm i @therms/atalaya
```

---

![npm (scoped with tag)](https://img.shields.io/npm/v/@therms/atalaya/next?label=%40therms%2Fatalaya&style=flat-square)

Git `next` branch, the next major stable release in development and testing, docs: [@therms/atalaya@next](/@next)

```bash
npm i @therms/atalaya@next
```

Development and features on the `next` branch is primarily for breaking changes (major release version change). This
branch/tag should be considered mostly stable and typically published alongside of a lower @latest major release in
order to give packages time to migrate from a previous version.

---

![npm (scoped with tag)](https://img.shields.io/npm/v/@therms/atalaya/beta?label=%40therms%2Fatalaya&style=flat-square)

Git `x.x.x-beta.x` branch, development effort release, docs: [@therms/atalaya@beta](/@beta)

```bash
npm i @therms/atalaya@beta
```

Development, refactoring, additions, improvements, etc. are done on the `beta` branch.

### Setup

We provide several ways to import the design system tokens:

- CSS Stylesheets.
- Tailwind.
- JS Tokens / Constants.
- JS Constants.

#### CSS stylesheet

CSS files ready to import can be found in the `css/` folder. Since Atalaya provides support dark/light themes, the
tokens had to be broken up into 3 files:

- base.css: contains all the default variables that are always used. Should always be imported before the rest of
  Atalaya stylesheets.
- color-light, color-dark: share the same CSS variables keys, each one have the corresponding values for their color
  themes. You should import one or the other depending on the color theme you want to display.

After importing this files, the only thing you need to do is to use the tokens on your css declarations.

#### Tailwind

If you are using tailwind we provide a preset that will automatically integrate our design tokens to tailwind classes.
To enable this, in tailwind's configuration add `tailwind.js` as a preset.

**Important note: If you use Tailwind and our React Components, we recommended that you add Atalaya's folder to the
purge configuration. It can prevent specificity errors that cause some components to don't look as expected.**

#### JS Tokens / Constants

We provide some JS objects that provide a more convenient API to define styling on JS based environments.
Inside `lib/index.js` you will find two object `tokens` and `Constants`.
`tokens` uses token names as keys, useful for situations like creating css variables programmatically. `Constants`
contains the same information, but it's shape it's more convenient to use on pure JS environments like React.

### React Components

Based on Atalaya's foundations, we have created React components that can be imported from `lib/index.js`. For them to
work you will need to wrap you app with `<AtalayaWrapper />` this component provides the rest of Atalaya's components
with the styling they need. Plus, it adds support for theme toggling.

## Docs

This project uses Styleguidist for documenting components. Components are automatically documented and can additionally
have `[component name].md` files to add additional documentation

Running the docs:

```
npm run docs
```

Building the docs:

```
npm run docs:build
```

## Development

This project uses React Cosmos for developing components with quick UI feedback/reloading on the developers local
machine. Run `npm run cosmos`.

To run this project and develop components that are used by another project locally on the same machine, use `npm link`
command in this project and in the other project run `npm link @therms/atalaya`.

In order for your changes to reflect while you develop, run:

```
npm run build:dev
```

## Publishing

We use `semantic-release` for automatic versioning. Every time a new commit has been pushed to master with the `feat`
or `fix` keyword it will create a new version and publish it to `npm`.
If the commit introduces a breaking change you should add `BREAKING CHANGE` to the commit message with an explanation of
what has been broken. This will let `semantic-release` know it should publish a new major version instead of a minor
one.

If you want to deploy a new version but don't want to push your commits you can run `npm run polish`.

### Known Issues

- Conflict between Tailwind and React Components if `node_modules/@therms/atalaya/**/*.js` is not included in purge
  config.

