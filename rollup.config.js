// import analyze from 'rollup-plugin-analyzer'
import postcss from 'rollup-plugin-postcss'
import commonjs from '@rollup/plugin-commonjs'
import autoExternal from 'rollup-plugin-auto-external'
import json from '@rollup/plugin-json'
import typescript from '@rollup/plugin-typescript'
import pkg from './package.json'

const postcssConfig = {
  minimize: true,
  plugins: [
    require('postcss-import'),
    require('tailwindcss/nesting'),
    require('tailwindcss'),
    require('postcss-combine-duplicated-selectors'),
    require('autoprefixer'),
  ],
}

const cssExtractConfig = { ...postcssConfig, extract: true }

export default [
  {
    input: 'src/index.ts',
    output: [
      {
        file: pkg.main, // CommonJS format for Node.js
        format: 'cjs',
      },
      {
        dir: 'lib', // ES module format for bundlers
        format: 'es',
      },
    ],

    external: [...Object.keys(pkg.peerDependencies || {})],

    plugins: [
      typescript(),
      commonjs(),
      autoExternal(),
      postcss(postcssConfig),
      json(),
    ],
  },

  {
    input: 'src/styles/css/variables/colors-light.css',
    output: { file: 'lib/css/variables-colors-light.css' },
    plugins: [postcss(cssExtractConfig)],
  },
  {
    input: 'src/styles/css/variables/colors-dark.css',
    output: { file: 'lib/css/variables-colors-dark.css' },
    plugins: [postcss(cssExtractConfig)],
  },
  {
    input: 'src/styles/css/variables/base.css',
    output: { file: 'lib/css/variables-base.css' },
    plugins: [postcss(cssExtractConfig)],
  },
  {
    input: 'src/styles/css/all.css',
    output: { file: 'lib/css/styles.css' },
    plugins: [postcss(cssExtractConfig)],
  },
]
